---
layout: post
title: "Hi all, I'm new to the group;"
date: December 21, 2015 17:44
category: "Hardware and Laser settings"
author: "Jim Bilodeau"
---
Hi all,



I'm new to the group; just got a K40 laser cutter. On my first run I could hear the laser tube arcing to the chassis. I have found a few posts in the group on this issue, but I have not seen a direct solution. (Arcing at about 0:15 in video) 



I do not have the external ground connector attached to a ground pin outside. I did remove paint from around the ground terminal inside the chassis so that the wire terminal was actually grounded to the enclosure.



Will adding a ground pin fix this? Do I need to add some of that silicone provided to around that red wire where it is arcing? Or is it a bad laser tube? (not planning to touch this without a possible solution at this point..)



![images/fe1c973b703305549d0c49a8524ee158.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe1c973b703305549d0c49a8524ee158.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-iUXhpgSPZkU/Vng6LZxG3WI/AAAAAAAAAP4/byt37yF2-k8/s0/1219151804.mp4.gif**
![images/e1aab468b2af0a194956d306376ce563.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/e1aab468b2af0a194956d306376ce563.gif)

**"Jim Bilodeau"**

---
---
**Scott Marshall** *December 21, 2015 18:32*

Although not directly with lasers, I have a fair amount of High voltage experience.



Arcing (assuming everything is in it's place and the distances are adequate for the voltage) is usually caused my contamination and/or moisture. 



POWER OFF and safe, Clean the area well using a non water based cleaner 



WD-40 or equal moisture displacing spray on a papertowel works great. 

That will to do it 99% of the time. 



Leaving a little WD-40 or oil based residue is not a problem, in fact it will discourage moisture condensation. 



IF you have constant/reoccurring problems here are solutions I've used sucessfully in the past. (verify proper voltage 1st)



If you run your chiller too cold in a humid enviroment, condensation will be a constant battle (if you are running chilled or sub ambient water as coolant)



If you have a constant problem due to high humidty, the old faithful insulation is RTV. Clean the are of oil with MEK, lacquer thinner, or similar solvent (Not alcohol with water in it) and cover the HV connection with a thin layer of RTV (100% Clear RTV). Make sure yo seal back the the wire insulation so there is no path out for the arc to form.

 Let cure for about 24 hours, although it will work as an insulator even before it cures. Leave the cover open as the most common RTV released acetic acid (vinegar type acid) while curing and it's a mild corrosive. Once cured the RTV is inert. The reason I say keep it thin is because it's tough to remove for servicing. It also cures slowly in thick sections so keep it < 1/4" (6mm) 

Years ago we used to get some for the CRT industry that had an additive in it like colloidal silica so that it was removable. The filler weakened it to where you could just pull on it and it would split and break away. I do't know if it's available anymore as CRTs are going the way of the Dodo. (I was a TV service tech in college in the late 70 and we got it from Zenith and Chemtronics).



Another solution we used back in the TV days was Kyrlon Crystal Clear (any pure clear lacquer ought to work). After cleaning the glass, spray 5 - 10 light coats and it helps keeps moisture from condensing on the area. It's an electrial insulator, but it's thermal insulation property is what helps most I believe. 

Distance, dry and clean are your friends



Hope it helps, Scott




---
**Jim Bilodeau** *December 21, 2015 23:17*

Thanks Scott!

I will give that all a try, hopefully it works!

And thank you for all of the detailed information!


---
**Timothy “Mike” McGuire** *December 22, 2015 04:24*

Jim make sure there is no charge left in the power supply before you touch it 25,000 V very unforgiving

It looks like the red wire has found a path to ground after making sure everything is safe check your connections and re-silicone but be safe rubber boots and gloves remove all conductive materials from your self  hope this helps and welcome to the Group happy cutting and Merry Christmas


---
**Jim Bilodeau** *December 23, 2015 21:36*

I rotated the tube 90 degrees such that the wire was sticking straight up (I had it towards the front wall before) and cleaned the area with WD-40. I have ran 2x jobs so far with no arcing! Thanks again for the tips! 


---
**Chris** *March 12, 2016 10:18*

Hello everyone, I just got my K40 and same problem as Jim an electric arc out at the red laser and file against by all the time and no laser beam in the tube. Thank you


---
*Imported from [Google+](https://plus.google.com/113851648493299517561/posts/NmFdQDe8Gxu) &mdash; content and formatting may not be reliable*
