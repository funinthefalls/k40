---
layout: post
title: "Looking for someone who has done an upgrade to their system from light object and replaced it with a new controller???"
date: March 06, 2016 04:34
category: "Modification"
author: "Jerry Martin"
---
Looking for someone who has done an upgrade to their system from light object and replaced it with a new controller???





**"Jerry Martin"**

---
---
**Gee Willikers** *March 06, 2016 04:37*

You mean upgrade to a Lightobject dsp? I have.


---
**Daniel Wood** *March 06, 2016 08:33*

I also use a lightobject dsp 


---
**Jerry Martin** *March 07, 2016 01:27*

Which Controller do you have?



Looking for someone who has the x7 DSP Controller and my power supply?



Looking for a wiring diagram to the power supply? MYGJW40


---
**Gee Willikers** *March 07, 2016 01:53*

X7, yes. That power supply, no idea. Have a photo? Typical dsp connections are GROUND to ground, PWM to center of potentiometer, TTL to where the test fire button connected opposite the ground.


---
**Jerry Martin** *March 07, 2016 04:03*





Here is a photo of the power supply, still trying to figure out the wiring diagram


---
**Gee Willikers** *March 07, 2016 04:21*

G+ doesn't allow photos on comments like facebook does. You'll need to make another post with the photo here in the community or tag me in a private post.


---
**Stephane Buisson** *March 07, 2016 13:41*

please not too much posts (hard to find anything later), please use external solution like dropbox for photos in comments.


---
*Imported from [Google+](https://plus.google.com/111904744658622469563/posts/j9xm3ueDwfw) &mdash; content and formatting may not be reliable*
