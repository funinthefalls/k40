---
layout: post
title: "K40 laser cutters rock,finaly after lots of mods and new focal lens my cutter is cutting 3mm mdf very well.the cut is so fine i cannot push a nornmal thi cknes a4 sheet cut into a strip through the cut-it is that fine.ordered"
date: March 02, 2016 08:58
category: "Hardware and Laser settings"
author: "Phillip Conroy"
---
K40 laser cutters rock,finaly after lots of mods and new focal lens my cutter is cutting 3mm mdf very well.the cut is so fine i cannot push a nornmal thi cknes a4 sheet cut into a strip through the cut-it is that fine.ordered feeler gauges to measure cut width better -hoping for a cut width of 0.03mm



![images/cc8db3de1104f3c14ac20f5e57f6aab2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc8db3de1104f3c14ac20f5e57f6aab2.jpeg)
![images/db28507f98574700f52f954cd7290e44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db28507f98574700f52f954cd7290e44.jpeg)
![images/756a100772ac23ee07c379801afe8d1d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/756a100772ac23ee07c379801afe8d1d.jpeg)
![images/0253a130e6b4ba2bed4626a5812e308c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0253a130e6b4ba2bed4626a5812e308c.jpeg)

**"Phillip Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 02, 2016 10:38*

That's cool :) I like how you've done the "top" side of the piece is actually the back, so the charring is kind of hidden on the final product. Smart move.


---
**Phillip Conroy** *March 02, 2016 14:59*

I  stiltl loke the way it looks on burnt side,getting to the limit to how fine i can cut the mdf-1-2 mm ,if les distance between cut would leave burn marks on both sides.



The laser beam has an hour glass shape to it,and with the way i have got the work distance from the lasers focal lenes  i am only using the top halfof it ,so it cuts a v shape into the mdf with the narrowerst part of the beam beeni g at the back of the work peace.[back of work peace is 50mm vrom focal lenes] if i droped my 3mm mdf 1.5mm to mzke the thinest part of the beam  the middle of the mdf  it would not look as clean on the back side.so for what i use my laser cutter fo i have it set up very well.


---
**Phillip Conroy** *March 05, 2016 01:01*

just measured with feeler gauges and my cut is 0.1mm i am very happy with this 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 05, 2016 04:56*

That's nice and thin. I'd be very happy with that too.


---
**Manuel Maria Organvídez Rodríguez** *March 09, 2016 20:05*

hello, 

good job¡¡¡

What parameters you use to cut 3 mm of mdf? i have some problems to cut it


---
**Phillip Conroy** *March 10, 2016 07:23*

7.5 mm/sec and 15ma power ,10psi air assist-if it doesnt cut you have problems-check mirrors and focal lens and alignemt of mirrors


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/jnveeCMy8mV) &mdash; content and formatting may not be reliable*
