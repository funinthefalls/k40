---
layout: post
title: "I have a k40 I just bought and plan to get it up and running this weekend"
date: September 29, 2017 01:44
category: "Discussion"
author: "Space Ronin"
---
I have a k40 I just bought and plan to get it up and running this weekend. My question I'm sure has been asked before but I'm over in the USA. I just installed a brand new grounding rods to my breaker. Box. That being said do I really need another separate ground rod somewhere else just to attach to the back of my machine? Any experience would be appreciated. And iis there's some sort of test I could do to check if I have a proper ground in my outlet for this laser to work properly. 





**"Space Ronin"**

---
---
**Gee Willikers** *September 29, 2017 01:59*

As long as your household wiring meets current nec rules you're fine with just the grounded outlet. One of those plug-in outlet testers will tell you both if the outlet is properly grounded and if neutral and hot are in the proper phase.


---
**Space Ronin** *September 29, 2017 02:09*

ill pick one up tomorrow thank you for the info. That what i was thinking as well. that my current ground should suffice since i just did 2 8 ft ground bars and oversized my ground system for my breaker box. Not to mention the outlet is brand new wiring directly to the breaker box since its a remodeled room. 


---
**Phillip Conroy** *September 29, 2017 02:42*

Just check inside the power supply area that they have removed some paintbefore screwing in the case earth


---
**Space Ronin** *September 29, 2017 02:47*

i planned on just using the 3 prong plug to ground my system to the house. the resistance from the plug to the case is around 1 ohm.


---
**ki ki** *September 29, 2017 09:10*

I used the continuity tester on a cheap multimeter to make sure all the metal parts are grounded tot he third prong on the power plug. 


---
**Space Ronin** *September 30, 2017 01:35*

So my ground looks good but the 3 lids that open aren't grounded from factory. Anyone recommend bonding them all together to the main chassis ?


---
**Adrian Godwin** *September 30, 2017 11:07*

Good electrical practice would suggest that you do (and its probably illegal to sell them without in some jurisdictions) but I haven't bothered and don't see a fault that would make them live as very likely.




---
**Space Ronin** *September 30, 2017 13:29*

Haha fair enough thanks for the input 


---
*Imported from [Google+](https://plus.google.com/103130365677659082439/posts/eaNSmaBDSyS) &mdash; content and formatting may not be reliable*
