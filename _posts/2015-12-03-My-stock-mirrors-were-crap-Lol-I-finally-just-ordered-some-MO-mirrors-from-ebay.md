---
layout: post
title: "My stock mirrors were crap! Lol I finally just ordered some MO mirrors from ebay"
date: December 03, 2015 20:38
category: "Modification"
author: "David Cook"
---
My stock mirrors were crap! Lol I finally just ordered some MO mirrors from ebay. Should have them next week



![images/17a85b81d4d7e11da33bd1593018a95d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/17a85b81d4d7e11da33bd1593018a95d.jpeg)
![images/a48409e5767325f63a9479bc177feab8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a48409e5767325f63a9479bc177feab8.png)

**"David Cook"**

---
---
**Gary McKinnon** *December 03, 2015 21:01*

Thanks for that, just ordered the same after seeing your old mirror in the photo! Haven't looked closely at mine yet but if they are okay then at least i'll have some spares.


---
**David Cook** *December 03, 2015 21:02*

no prob, that's why I posted.  Check your mirrors, even if they are not as bad as mine they are still not as good as a good mirror.  We want as much power as possible reaching our work piece.  specially when we are already limiting our power to preserve the tube life.


---
**Gary McKinnon** *December 03, 2015 21:57*

Ye i've seen the tubes going for £140 ($212).


---
**Coherent** *December 03, 2015 22:47*

Keep the mirrors clean it'll help them last longer. As a side note I've seen tutorials on making your own mirrors by cutting them from old hard disk drive platters. They are supposed to work/cut better than the original mirrors. Don't know if that is a statement about the DIY mirrors or the ones that come in these imported machines.


---
**David Cook** *December 03, 2015 22:48*

Absolutely,  this is how my mirrors came though.   I'm still converting my control board etc  before I start cutting


---
**I Laser** *December 04, 2015 00:18*

Wow, that's shocking! My lense and mirrors had slight scratching but seem okay, but nothing like the deposit of crap burnt into yours!


---
**Ashley M. Kirchner [Norym]** *December 04, 2015 04:40*

One of the things that was rather surprising to me was that the mirrors that came with my machine are not glass coated. They are actually metal discs. I don't know what kind, but they are metal none the less. I keep hearing people say theirs were glass coated and they're crap. But when I took all of mine out for cleaning, that's when I noticed they were metal. I guess for $366, I lucked out.


---
**David Cook** *December 07, 2015 16:50*

Mirrors arrived on Saturday amazing how reflective they are. They are thicker but they still fit.  Molybdenum is heavy!


---
**Victor Hurtado** *December 13, 2015 11:43*

saludos cual es el costo con envío a Ecuador Sudamerica


---
**David Cook** *December 13, 2015 18:15*

Today I started mirror alignment, lol what a pain in the ass without being able to have a red dot laser in the path lol.   too bad these CO2 tubes do not have a HeNe function as well with a 2nd set of terminals lol.  ( I know just funny to ponder)


---
**RL Rain** *January 10, 2016 02:22*

I made aluminum sleeves to cover the mirrors as I alight them using post its or tape.  The heavy duty aluminum from a flat disposable cookie sheet worked prefect for this and prevented the mirrors from being damaged.  Got the Idea from a youtube video on a home made laser. 


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/Tkt48fdTcmr) &mdash; content and formatting may not be reliable*
