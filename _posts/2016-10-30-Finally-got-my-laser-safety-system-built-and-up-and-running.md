---
layout: post
title: "Finally got my laser safety system built and up and running!"
date: October 30, 2016 16:15
category: "Modification"
author: "Ulf Stahmer"
---
Finally got my laser safety system built and up and running! Now to install in my k40! Code and parts details are here: [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)







![images/a17c4ffb9a7042f41c8d3892bcabc6cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a17c4ffb9a7042f41c8d3892bcabc6cd.jpeg)
![images/8657601a85648bcbb5095dc8d6b6bfa0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8657601a85648bcbb5095dc8d6b6bfa0.jpeg)
![images/15dbc47389333e463113de53b4108d3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15dbc47389333e463113de53b4108d3a.jpeg)
![images/ba71e5072f3d56beacd3f99fc5d98616.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba71e5072f3d56beacd3f99fc5d98616.jpeg)

**"Ulf Stahmer"**

---


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/LufoUSkA5pH) &mdash; content and formatting may not be reliable*
