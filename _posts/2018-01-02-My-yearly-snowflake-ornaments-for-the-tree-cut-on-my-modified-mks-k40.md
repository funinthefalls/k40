---
layout: post
title: "My yearly snowflake ornaments for the tree, cut on my modified mks k40"
date: January 02, 2018 19:34
category: "Object produced with laser"
author: "Ivan Iakimenko"
---
My yearly snowflake ornaments for the tree, cut on my modified mks k40. Video to the entire process here: 
{% include youtubePlayer.html id="R9rMZxua3n4" %}
[https://youtu.be/R9rMZxua3n4](https://youtu.be/R9rMZxua3n4)

![images/968449cfceaf641e60162d69dc227e2d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/968449cfceaf641e60162d69dc227e2d.jpeg)



**"Ivan Iakimenko"**

---
---
**Ned Hill** *January 02, 2018 20:51*

Those look awesome!  Great video to, thanks for sharing.


---
**Anthony Bolgar** *January 02, 2018 22:10*

Beautiful job!




---
**Steve Clark** *January 02, 2018 22:16*

Looking good!


---
**LightBurn Software** *January 02, 2018 23:16*

Those are beautiful. Well done!


---
**Paul Mott** *January 03, 2018 08:35*

Excellent work and excellent tutorial video. My congrats.


---
**Jeff Harbert** *January 07, 2018 14:05*

Beautiful work.


---
*Imported from [Google+](https://plus.google.com/+IvanIakimenko/posts/4tSbsZtkACx) &mdash; content and formatting may not be reliable*
