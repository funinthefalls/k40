---
layout: post
title: "What are the rules????"
date: September 09, 2016 20:11
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
What are the rules????  #yearofthelaser #laserweb

![images/469e67c1ee5561489373de915da29d60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/469e67c1ee5561489373de915da29d60.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**greg greene** *September 09, 2016 20:19*

Rule One:  It's a process, perfection comes with Practice so HAVE FUN !


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 20:22*

Keep calm & laser with remaining eye!


---
**greg greene** *September 09, 2016 20:29*

Yarrr Matey! It's Ok the parrot has both eyes !!! :)


---
**Alex Krause** *September 10, 2016 02:31*

Hotel soap?


---
**Ariel Yahni (UniKpty)** *September 10, 2016 02:36*

my ivory shower soap. Did a black image witl white letter, should have tried the opposit


---
**Alex Krause** *September 10, 2016 02:37*

Let me see if I have an extra bar of soap laying around ;)


---
**Ariel Yahni (UniKpty)** *September 10, 2016 02:52*

File here [https://cl.ly/2C2e0W1e0y2V](https://cl.ly/2C2e0W1e0y2V)


---
**Alex Krause** *September 10, 2016 04:13*

![images/e927f776b901fe08205e73095c4fc7ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e927f776b901fe08205e73095c4fc7ac.jpeg)


---
**Alex Krause** *September 10, 2016 04:21*

**+Ariel Yahni**​ **+Peter van der Walt**​ ^^^^^^^


---
**Ariel Yahni (UniKpty)** *September 10, 2016 04:21*

YEAHHH baby


---
**HalfNormal** *September 13, 2016 01:02*

Looks like some clean fun!


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/QRp8NnaC7fY) &mdash; content and formatting may not be reliable*
