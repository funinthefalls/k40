---
layout: post
title: "Did a test on standard USA safety glasses 3 second test..."
date: August 13, 2016 03:43
category: "Discussion"
author: "Alex Krause"
---
Did a test on standard USA safety glasses 3 second test... didn't penetrate all the way thru the lens with an un focused beam... the focused beam cut right thru it with less than a second burst 

![images/6fe0c67683063712ba1a756f17271a04.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6fe0c67683063712ba1a756f17271a04.jpeg)



**"Alex Krause"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 13, 2016 03:49*

Now take them back to Home Depot and say they didn't work. 


---
**Alex Krause** *August 13, 2016 03:50*

Lol **+Ray Kholodovsky**​ I have a box full of safety glasses and ear plugs :) I work in a factory


---
**Jim Hatch** *August 13, 2016 03:53*

Not pretty. Reflected beams can be convergent over significant distances. Do you have any Z136.1 glasses you could try the same experiment for. (BTW, the standard I'd for 10 sec which is where OD comes in.)


---
**Eric Flynn** *August 13, 2016 03:58*

They DID work!!  Obviously.  If they didnt, they wouldnt have burned, it would have transmitted the energy.  Now try it with "laser safety" glasses.  The result will be the same.  Glasses are not intended at any rate to protect you from an extended direct beam exposure.  That goes for ANY wavelength, at any power.



Practically all plastics are opaque to C02 wavelengths.  The fact they burn is a testament to this fact as it is absorbing, as opposed to transmitting the energy. 



Read below for any info you need to know about C02 lasers (or any laser for that matter).  Sams is highly regarded as THE definitive source for everything laser.  Even the big boy pros use his info as reference.



[http://www.repairfaq.org/sam/laserco2.htm](http://www.repairfaq.org/sam/laserco2.htm)


---
**Alex Krause** *August 13, 2016 04:00*

**+Jim Hatch**​ Only glasses i have to test on are z87+u6... which is the highest UV protection from impact rated glasses


---
**Jim Hatch** *August 13, 2016 04:10*

**+Eric Flynn**​ they "worked" only if you assume it's unfocused. The focused beam burnt thru before you'd hear your eyeball sizzle. The problem with reflected beams is that they can have the power of the original focused source (which is noted in your reference - although that's also the source that says "A 100 W unit can cut a reasonable thickness of sheet steel" in the CO2 laser section which would be news to pretty much everyone running those lasers).



Yes plastic is opaque and will absorb the light. The point of the standard has to do with how long. Alex's test may be satisfactory if you never cross a coherent reflected beam. Since all reflections are <b>not</b> unfocused, there is risk in relying on cheap Z81 glasses vs spending a few bucks on Z136 ones.



But people have to decide for themselves what their risk cost benefit calculation is that they're comfortable with. I'm thinking spending the equivalent of a tank of gas for purpose built specific protection is reasonable. 



YMMV


---
**Eric Flynn** *August 13, 2016 04:34*

I am not disagreeing with you, and you are correct.  However, my point is, normal safety glasses are adequate.  Use them if you dont have anything else, or have to wait who knows how long to get a decent pair.



The 100W for "sheet" steel is in fact correct.  His definition of "reasonable" is unstated.  A good 100W flowing gas (undefined there as well) C02 with oxygen assist can in fact cut a "relatively" reasonable thickness of sheet steel.  The mode of operation is also unstated. In superpulse mode, extremely high power densities are attainable many many magnitudes higher than any of the cheap sealed tubes are capable of.



It seems your experience is limited to cheap sealed tubes,(as is 99% of the info on internet), in which case you would be correct, and I see where you would get that impression.



I have a 70W Coherent flowing gas tube, and it is capable of cutting..010 stainless.  My 120W LEI sealed tube is capable of cutting .010 mild steel and stainless in superpulse mode.  Again, this isnt a cheapo tube the internet is used to.  Its a very high end $10k tube that was pulled from industrial sheet metal processing equipment.  



A decent tube, with good optics is far more capable than you may think.



Not news to those whose experience goes beyond the typical 40W chinese sealed tubes.



In fact, you can probably find a video showing a 100W system cutting steel in less than a minute if you look!!


---
**Whosa whatsis** *August 13, 2016 07:05*

That energy has to go somewhere, and the glasses aren't mirrors, so of course they're going to absorb the energy and burn. The faster the laser can cut through them (for a given thickness), the more effectively they're absorbing the energy from the laser. Obviously, if you're in a situation where the laser's power is focused enough to cut through them, they're not sufficient protection, and in that case, if it misses the glasses it's going to burn the shit out of your face anyway. The point of safety glasses (as opposed to a full face mask) is to make sure that anything that's not going to injure your exposed face doesn't injure your more easily-damaged eyes either. They don't make laser safety face masks because you're not supposed to use lasers in situations where your face might end up in front of a focused beam.


---
**Jim Hatch** *August 13, 2016 11:55*

**+Eric Flynn**​ I am aware of other non-cheap Chinese lasers but those aren't the ones these folks (or the OP) are dealing with. Someone will take these comments out of context and start wondering why they can't cut steel when they "should" because  they read something here that said they can. 


---
**Eric Flynn** *August 13, 2016 15:38*

I was just responding to your comment on the Sams link.  I was just explaining why that comment was untrue.  



If they have a concern about their laser, and why it will not cut steel, the themselves can spend a few minutes doing their own research and learn why.  If thats too much to ask of them, they shouldnt be playing around with these things anyway.



Nothing here said they can either. Quite the opposite.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/BNThC31oZBL) &mdash; content and formatting may not be reliable*
