---
layout: post
title: "Due to my job I have contact with coolant companies"
date: March 08, 2017 14:47
category: "Hardware and Laser settings"
author: "MA Lopez"
---
Due to my job I have contact with coolant companies. Today I have been talking with the chemical guy of one international company. I have asked about using coolant with the CO2 laser. He told what other mates from here did, don't use it. I asked about distilled water and he told me to use better osmotized water with some drops os chlorine. What do you thing?





**"MA Lopez"**

---
---
**Don Kleinschnitz Jr.** *March 08, 2017 16:03*

testing why I cant post here.


---
**Don Kleinschnitz Jr.** *March 08, 2017 16:03*

Interesting you should ask: 

We have been working on a formula for coolant after my machine started misbehaving  



We have reasonably well proven that there is a relationship between coolant conductivity and arching problems. 

+HP Persson has been warning us for some time....



The post captures the thread and the link to my blog post on the subject.



HOWEVER: Last night I replaced my water with the 1oz of Clorox and 5 gal of water and the reading was 281 uS, more than the <10 uS as I had calculated. 

Turns out the source of conductivity data is bogus. REBOOT!

<s>---------------</s>

+Ned Hill, our resident chemist re-calculated and we are now suggesting 1/4 oz of Clorox for 5gal of water may produce a level in the range of 30uS in 5 gallons.



Remember that we don't know what the conductivity threshold is but we want to get as close to distilled and still keep algae preventative characteristics.



I will be doing some tests on Clorox concentrations to insure our above formula is sound.  

[plus.google.com - COOLING WATER "IT MATTERS" After flailing for a few days with what I thought...](https://plus.google.com/113684285877323403487/posts/jDRGVhd6zqy)


---
**Phillip Conroy** *March 08, 2017 19:10*

algae need a food source and light to live. I lived on a farm and had only tank water .the tanks where never cleaned out.the only time I have seen algae grow in water is when a food source has been added ie cooks adding crumbs of food while drinking,


---
*Imported from [Google+](https://plus.google.com/109180025053447503546/posts/CjjL42F7xKE) &mdash; content and formatting may not be reliable*
