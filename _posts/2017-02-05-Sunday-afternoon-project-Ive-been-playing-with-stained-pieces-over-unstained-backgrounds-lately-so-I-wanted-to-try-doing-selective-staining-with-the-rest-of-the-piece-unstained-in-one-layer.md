---
layout: post
title: "Sunday afternoon project. I've been playing with stained pieces over unstained backgrounds lately so I wanted to try doing selective staining with the rest of the piece unstained in one layer"
date: February 05, 2017 23:29
category: "Object produced with laser"
author: "Ned Hill"
---
Sunday afternoon project.  I've been playing with stained pieces over unstained backgrounds lately so I wanted to try doing selective staining with the rest of the piece unstained in one layer.  Had this sea turtle graphic that I thought might work well.  The order of operations switched from what I had been doing for some of my recent pieces.  In this case I masked the wood (went over it really well to ensure a good seal), etched/cut, painted the shell etching black, removed the masking on the shell, stained the shell, removed the rest of the mask and sealed.  Came out pretty well.   I think it might look better if I painted all the etching black.

![images/e4952fadc8c108d3ec41e1ced42f343e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e4952fadc8c108d3ec41e1ced42f343e.png)



**"Ned Hill"**

---
---
**timb12957** *February 06, 2017 11:43*

Wow very nice! I have friends that live on an island in the  Bahamas called Green Turtle Cay. As such she loves turtles. I would like to try one of these for her. Would you share the graphic you used?


---
**Ned Hill** *February 06, 2017 13:01*

Sure thing **+timb12957** I sent it to you.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 07, 2017 00:04*

Would be interesting to play with multiple different stain colours for even more varied effect. Imagine all the different sections on the turtle's back being gradient stained (i.e. dark on edges, light in centre of shell). But, as is, this looks pretty great.



I'm curious, if you don't paint the etching black, but you are staining that region, is it not dark enough for the contrast?


---
**Ned Hill** *February 07, 2017 00:45*

Ahhh **+Yuusuf Sallahuddin** I knew I could count on your artistic input :)  I like your idea about the different stain colors.  I actually have a selection of different color I used for a project pre laser.  Will definitely have to give that a go.  

Yes, without paint the etched lines would defiently be a darker brown because they would soak up more stain.  So that is another option to play with.   Thanks  :D


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/2VC1FTSVJBM) &mdash; content and formatting may not be reliable*
