---
layout: post
title: "What version of Windows do you use?"
date: December 09, 2017 12:57
category: "Software"
author: "krystle phillips"
---
What version of Windows do you use?



Corel Laser and the K40 seems to indicate that I need Windows 8.



Someone help because I cannot get Windows 8.







**"krystle phillips"**

---
---
**Ned Hill** *December 09, 2017 14:45*

I'm using windows 10.


---
**krystle phillips** *December 09, 2017 14:47*

And the Corel Laser program works?


---
**Ned Hill** *December 09, 2017 14:54*

If you are using the corelDraw version that came with the machine then there could be some issues.  I was originally using the stock version (ver 12, really old) but I was having too many issues and upgraded to CD X8.  If you can't get it to work then I would suggest you check out the  K40 Whisper program.  It's a nice program that works with the stock controller and uses the open source inkscape drawing program. [scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**krystle phillips** *December 09, 2017 14:57*

**+Ned Hill** Really. I initially had 10 and I got issues after reading up I realised that I need 8.



8 is impossible to get.





So You tried Whisper and it worked well for you?

That's the consensus?


---
**Ned Hill** *December 09, 2017 15:09*

You can upgrade to coreldraw X8 but it's pricey (home version works but student version will not).  That will work with Windows 10.  I've played with K40 whisperer and it's a good program.  **+Scorch Works** has done a really good job with the install instructions (skip the dual load option).  Just read everything on that page and watch the videos.


---
**krystle phillips** *December 12, 2017 21:46*

**+Ned Hill** It isn't installing...i tried all the options and nada...zip...zilch.



All  of my anti-viriuses are flagging and removing it.

Windows and Norton 


---
**Ned Hill** *December 12, 2017 21:51*

**+krystle phillips** Are you talking about K40 whisperer? 


---
**krystle phillips** *December 12, 2017 21:54*

yes, whisperer. Should i deactivate them temp and allow the install?


---
**Ned Hill** *December 12, 2017 21:55*

Yes allow it to install. It’s a false positive for anti virus. 


---
*Imported from [Google+](https://plus.google.com/+krystlephillips/posts/MJvq9axeDxb) &mdash; content and formatting may not be reliable*
