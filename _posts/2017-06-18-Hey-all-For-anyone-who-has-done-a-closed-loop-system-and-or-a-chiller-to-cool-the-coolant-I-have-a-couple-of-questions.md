---
layout: post
title: "Hey all. For anyone who has done a closed loop system and or a chiller to cool the coolant I have a couple of questions"
date: June 18, 2017 03:06
category: "Modification"
author: "Sl33pydog"
---
Hey all.  For anyone who has done a closed loop system and or a chiller to cool the coolant I have a couple of questions.

1. To cool the fluid would I put a radiator on the inlet or outlet of the laser tube

2. Same question as number 1 but for a peltier cooler (it'll have a controller to keep a temp range)

3. If I put a temp probe in the system should I put it coming out of the reservoir?  My thoughts were that would be the most accurate temp.

4. Has anyone used pc coolant to cool the tubes?

5. What size tubing have you guys successfully replaced the coolant tubes from the laser?  I was thinking 3/8 might be a tad too big.

Thanks for your help guys.





**"Sl33pydog"**

---
---
**Joe Alexander** *June 18, 2017 05:02*

couple thoughts on this: 1 I don't think 3/8" would be too big just dont put a super strong pump on it in case it causes vibrations or something. And caution on coolant: some coolants increase electrical conductivity. It is widely recommended to use pure distilled water and at most a little algaecide or small amount of bleach to keep down growth.


---
**Joe Alexander** *June 18, 2017 05:04*

and if using a peltier i'd put the temp probe near the element, otherwise I would think that in the reservoir or on exit would be best to see if its cold enough/not gonna cause condensation.


---
**HP Persson** *June 18, 2017 09:20*

I have both radiator and peltiers. On fall/winter/spring the radiator is enough if i put it outside the window to cool. But summer is peltier needed :)



1: Radiator should be on tube inlet. The amount of cooling are identical, but if you have it on the outlet you have to wait longer as you have to cool down the tank before delivering it to the tube on startup. (this evens out after a while, depending on size of the tank)



2. I run my peltier on it´s own loop, cooling the tank, to prevent too cold water hitting a hot tube. Not a big issue, i just wanted to be extra safe and already had the tubing and pumps at home.



3. Use multiple probes :) , i have one on outlet from tube aswell as one in the tank.

The important value you want to know is what temperature you feed the tube with. Less than 22c is perfect.



4. Yes, i do it all day long. Distilled water, Water Wetter and PC cooling blitz (anti algea stuff). Water Wetter is to lower the surface tension, removing the ability for bubbles to form at all. Use same mix in my PC´s

Make sure the additive you buy is copper free!



5. I use original tubing to the laser tube, 6mm ID, 8mm OD

You do not gain anything (very little) with bigger tubes or more pressure.

When the radiator is used outside my window it has 10/13mm hoses with PC compression fittings.



Too much pressure is dangerous though, as the tube is very brittle you should stay at fish tank pump pressures.

Or about 15L/min max. (suggestion, not 100% fact it will brake on 16 ;) )

I did a flow test, above 12L/h there is no more gain of heat pickup from the tube. This can be calculated too with the thermal properties of water. 



This is just how i do it not how you have to do it, take what you need and apply to your setup :)


---
**Joe Alexander** *June 18, 2017 09:57*

**+HP Persson** I'd love to see your cooling setup, post us some pics? I'm currently using a cheapo pump with a 10liter steel reservoir(an old pressure pot from work that could handle 90pis) with a bottom outlet. I have another with a stainless tubing loop within that I could use to make a closed loop peltier loop. Just need to setup the peltier controller(already have 6 12706 peltiers with cooling blocks).


---
**Sl33pydog** *June 18, 2017 15:28*

**+HP Persson** So if I had to guess the attached picture is what your setup looks like?  If so I have a couple of questions:

1.  What tells your second pump and peltier cooler to turn on?  Do you have a controller on Temp Sense 1 that turns on/off that loop with a relay to keep the temperature of the reservoir in a certain range? or do you sense from a third temp sensor that directly measures reservoir temperature?

2.  It sounds like the cooling system is portable.  Did you make this like a portable chiller so you can as you said have it outside?

3.  If I went with pc liquid cooling pumps should I put them on a reastat so I can vary the speed of the pump to bring the pressure/flow rate down?  With this how are you measuring flow rate?  Did you put in a flow rate sensor into the loop?



My thoughts were to buy all PC liquid cooling parts and with a couple of reastats I could vary a lot of this and a relay controller to turn on and off the peltier cooler when I need some extra cooling.

![images/ccf185b3238bfc072e424ce2dd0b0e45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ccf185b3238bfc072e424ce2dd0b0e45.jpeg)


---
**Sl33pydog** *June 18, 2017 15:30*

**+Joe Alexander** I guess if the tube is just a tad too big I could put a hose clamp on it and then cover it with RTV.  It sounds like from the both of you guys that water pressure needs to be low since the tube is fragile.


---
**HP Persson** *June 18, 2017 16:18*

**+Sl33pydog** Almost, i edited your pic. One winter setup when the air outside is lower than inside and using the radiator. And one summer-setup where the peltiers work. I also changed the peltier-loop a bit, as it´s separate from the other loop. Two pumps in the tank, 2 hoses out, 2 in. One pair for the tube, one pair to the peltiers.



A radiator at the same time as a peltier, you will actually heat up the water as a rad. only can cool to ambient temps.

If the peltier pulls down the tank to 18c, and your ambient is 22c, you will be "heating" the 18c water with 22c air ;)

Take that into concideration depending on your temps.



I have a W1209 thermostat controller with a temp-probe, it turns on at 18c and turns off at 14c, well, never turns off on heavy work :)

At 24c ambient i have 16-17c for 20-30min with six 60w peltiers.

I´m thinking of watercooling the hot-side of the peltiers too, just to press some extra cooling out of them :)



My setup is not portable, not actually 100% closed either as i have a small vent for pressure differences. 

25 litres of water so it´s very heavy :)

On your idea you do not need one, just make sure to bleed it from air and make sure warm water can expand some inside the loop. 



Computer cooling pumps works good, i used both D5 and Laing DDC  pumps earlier. Now i use 12v bilge pumps for boats rated 14L/min as i can have them inside the tank. (25L plastic tank i seal when filled)



Having pumps on reastats is a good idea, you can tune it to whatever you want.

I use a arduino with a safety system on mine, it controls water temp, water flow, mirror temps (to notice me when to clean them) and in the winter it controls the fan speed on the radiator.



Just moved my machine to another room so my setup is in pieces, but i can shoot some pics on different parts to show what i use.



Edit: i forgot one pump on the summer setup :P Should have one per loop :)

![images/186c69bfaf9eb78d1fe4ec9d734e0065.png](https://gitlab.com/funinthefalls/k40/raw/master/images/186c69bfaf9eb78d1fe4ec9d734e0065.png)


---
**Sl33pydog** *June 19, 2017 12:18*

**+HP Persson** yah that would be awesome if you could post pics.  Your diagrams make a lot more sense since you aren't disturbing the flow going into the tube when the peltier loop is on.  You just want to get the reservoir temp down to a point.



With that I'm going to look for some pc cooling reservoirs with some extra inlet and outlet ports.  I'll start doing some diagrams for electrical and flow.



I was wondering about your lockout safety setup.  Would you mind expanding more on that?  Does it give an audible warning if something is outside your desired specs?  Does it control the laser switch to turn it off when something is outside your desired specs?  Are you running a Pwm flow sensor into the Arduino to measure water flow or is it simply one of those contact switches?


---
**HP Persson** *June 19, 2017 12:48*

**+Sl33pydog** Try to find a container with 5-25L volume, that´s the best option. More is better, longer time to heat up the water. The peltier will not keep up with a 0.25L pc cooling reservoar.

A solid one is better, transparent reservoars let UV and sun light trough helping growth of algea and stuff, algaecides will not stop it, just slow it down.



The lockout is both audible and visible, summer and LED + display.

Checkout github for different repo´s (search for laser safety system).



The flow sensor is a pwm/hall effect sensor feeding the system with data, both volume and if there is no flow att all = laser shuts down.

Water temp just beeps when it goes above 22c, and shuts down at 25c. 

Temp sensor is a DS18B20 water proof model, the flow sensor is a YF-S201.

Regular 20x4 display on a I2C and a Arduino Uno, can use a pro micro too. But i needed more pins so i used a Uno.

You can add whatever inputs you want to trigger alarm or shutdown, that´s what i love with the safety system :)



Next up in my project is to add the peltier to the safety system to control on/off trough the temp-data, just because i can :)

Probably not needed in the summer, but if you forget it on 100% power in the winter, you will make a big ice block of your reservoar... i tried it when i forgot it over night once :P


---
**Sl33pydog** *June 20, 2017 12:37*

**+HP Persson** definitely need a laser safety system like that.  Plus it'll get me to learn more coding.  Software is my bane.



Yah please post pics if you can.



Looks like I'll go with covered reservoirs and tubing with maybe a passive flow indicator for a small visual window on the side besides the Pwm one on the outlet for the Arduino system.



Does the reservoir need to be above the laser tube or will the bubbles eventually work themselves back into the reservoir tank?  



And I guess I could make it with two or three reservoir tanks even though cooling them will be a little more difficult with the peltier loop.


---
**HP Persson** *June 20, 2017 22:04*

**+Sl33pydog** If you use additives, so called surfactants to remove the surface tension - no need to have the res higher than the tube. With that bubbles cannot form at all and you can have it below the bench or similar. Check "pressure height" on the pumps you want to use, keep it at least at 3 meters. Don´t mind the other data, pressure height at least 3M and 10-15L/min is perfect.



I use Water Wetter for that and it works good.

Keeping the return-hose below the coolant level also helps alot preventing bubbles to form. You want as little turbulence of the water inside the res. as possible :)



Having more than 1 reservoar is no problem, i suggest to make sure the peltier is not cooling the tube directly - if you get a air bubble the cold cold water can crack the tube. Risk is slim - i just want to be extra safe.


---
*Imported from [Google+](https://plus.google.com/107481433958750959959/posts/YyQ8RJodQoy) &mdash; content and formatting may not be reliable*
