---
layout: post
title: "I have a K40 and i am VERY new to this laser game..."
date: December 30, 2015 17:49
category: "Discussion"
author: "Lataina Arnold"
---
I have a K40 and i am VERY new to this laser game... i need your help!  my laser seems weak... worked well for a week or so then went weak.  It will "cut" but it actually only marks, and wont engrave at all...  I changed the power supply and the laser tube and it is doing the SAME thing UGH!!! mA reading look low for cutting and barely read for engraving... any suggestions????





**"Lataina Arnold"**

---
---
**Scott Thorne** *December 30, 2015 17:53*

How is the alignment....that's one thing to check...or check the grounding.


---
**Lataina Arnold** *December 30, 2015 17:57*

alignment is not perfect but its close...


---
**Stephane Buisson** *December 30, 2015 18:28*

check your lens and mirror, if dirty your laser power would be lost.


---
**Scott Thorne** *December 30, 2015 18:40*

What are the mAs when you are trying to cut?


---
**Lataina Arnold** *December 30, 2015 20:48*

10 mAs on cut mode is giving me a light engraving.. i did just notice that when I ran the job the laser tube was not constantly glowing during the cut  it more like blinking... it was burn move burn move almost as if the the cut image was dotted instead of a solid line.... i hope this makes sense.... 


---
**Scott Thorne** *December 31, 2015 10:47*

Check to make sure that the main board is set to m2 not m1 in your coral laser and laser draw....check all connections on the pot.


---
**Lataina Arnold** *December 31, 2015 18:16*

yes it was Set to M2... i finally got it to engrave... I am really not sure how... however its not engraving with any depth... mirrors are clean and aligned....


---
**Scott Thorne** *December 31, 2015 20:39*

I'm at a loss...sorry man....hope someone else might have more insight than me.


---
**Stephane Buisson** *January 01, 2016 10:39*

if you never got a normal intensity, I would look into possibilities of any abnormal resistance in the laser line, my first idea would be to check the contact on the tube (under the silicone) and redo it. any loose connection is suspicious. 


---
**Dheeraj Arora** *January 03, 2016 11:47*

Hey do you have any video or manual for alignment?


---
**Lataina Arnold** *January 05, 2016 13:59*

re did connections and still only getting a bare minimal engraving regardless of settings.  However I can cut 2mm acrylic at a speed of 2.50 

The engravings look good but there is no depth to them; very shallow.....


---
**Lataina Arnold** *January 05, 2016 13:59*

I do not, check you tube


---
**Tomas Krnavek** *January 11, 2016 21:50*

try change the direction you engrave. My laser is weak as well, but does better job if I run the process from 'bottom to top' rather than from 'top to bottom' as I did before. Try, if this doesn't work try 'from right to left' or 'from left to right'. 


---
*Imported from [Google+](https://plus.google.com/110626599066810238227/posts/ZkNbATdhyU8) &mdash; content and formatting may not be reliable*
