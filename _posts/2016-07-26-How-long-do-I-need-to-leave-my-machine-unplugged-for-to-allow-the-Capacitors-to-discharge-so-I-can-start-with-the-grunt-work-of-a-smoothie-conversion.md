---
layout: post
title: "How long do I need to leave my machine unplugged for to allow the Capacitors to discharge so I can start with the grunt work of a smoothie conversion"
date: July 26, 2016 03:51
category: "Smoothieboard Modification"
author: "Alex Krause"
---
How long do I need to leave my machine unplugged for to allow the Capacitors to discharge so I can start with the grunt work of a smoothie conversion 





**"Alex Krause"**

---
---
**Anthony Bolgar** *July 26, 2016 04:04*

Depends on how much of a shock you wish to receive...lol! I have worked on mine as soon as 5 minutes after powering down, but to be safe, I would give it at least an hour.


---
**Brandon Smith** *July 26, 2016 04:38*

If it's a flux capacitor, be careful. I touched it too soon and got sent back to 1955.


---
**Anthony Bolgar** *July 26, 2016 04:53*

Don't worry if you get zapped back in time, Doc will save you.


---
**Alex Krause** *July 26, 2016 04:56*

Sadly I can't generate 1.21 gigawatts on a k40 maybe if I give it a TARDIS paint job I can do it with the 30-35w available 


---
**Anthony Bolgar** *July 26, 2016 04:58*

Only if you set it correctly with your sonic....


---
**Derek Schuetz** *July 26, 2016 05:26*

Just discharge it as you would a PC. Your technically aren't messing with any of the wires going to your tube so it shouldnt be an issue when swapping out your board


---
**Coherent** *July 26, 2016 18:43*

Depending on the circuitry it may take a long time for a capacitor to naturally discharge. All you need to do is bridge the capacitor terminals with a resistor (any size will work) for a couple seconds will discharge it. Large capacitors like those used in power supplies and CRT monitors easily have enough voltage to kill and if you don't know what you're doing, don't mess with them. At the very least check it with a volt meter before messing with the circuitry. Do a search online for how to discharge a capacitor... lots of info out there.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/QNkJ94JkWnz) &mdash; content and formatting may not be reliable*
