---
layout: post
title: "Yard dice made on white cedar 4x4s"
date: September 05, 2016 00:57
category: "Object produced with laser"
author: "Jeremy Hill"
---
Yard dice made on white cedar 4x4s. Laser engraved with 350 mm/sec. 



![images/2af25f04a6eafca5f46f7ea1bd5e60c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2af25f04a6eafca5f46f7ea1bd5e60c1.jpeg)
![images/9cb87b1c4238f4b2e4cf1136245e078e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9cb87b1c4238f4b2e4cf1136245e078e.jpeg)
![images/ba1c2077e1389924c4ca43162b63133f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ba1c2077e1389924c4ca43162b63133f.jpeg)

**"Jeremy Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 05, 2016 02:26*

Very nice. You'd have to give them a fair hoiking to get them to roll I'd imagine.


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/CuZpxbEvkEj) &mdash; content and formatting may not be reliable*
