---
layout: post
title: "I tested the new laser I bought with my power meter and it measured 51W"
date: September 14, 2017 15:21
category: "Discussion"
author: "Anthony Bolgar"
---
I tested the new laser I bought with my power meter and it measured 51W. Nice to get a laser where the advertised and actual power are equal. But I have a 60W tube that I will be transplanting it into and then using the 50W tube in a K40 with a tube extension box on it.





**"Anthony Bolgar"**

---
---
**Adrian Godwin** *September 15, 2017 01:03*

I'm interested to know what power meter you're using.


---
**Anthony Bolgar** *September 15, 2017 01:11*

I am using this one:

[https://www.bell-laser.com/product-page/mahoney-co2-laser-power-meter-probe-0-100-watts](https://www.bell-laser.com/product-page/mahoney-co2-laser-power-meter-probe-0-100-watts)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/LUgKtgjAj1J) &mdash; content and formatting may not be reliable*
