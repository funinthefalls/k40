---
layout: post
title: "The pump is off. What would cause the bubble build up in tube over night ?"
date: September 08, 2016 17:25
category: "Discussion"
author: "Robert Selvey"
---
The pump is off. What would cause the bubble build up in tube over night ?

![images/beb6638720f4a076836f2638067543e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/beb6638720f4a076836f2638067543e5.jpeg)



**"Robert Selvey"**

---
---
**Scott Marshall** *September 08, 2016 17:53*

Dissolved air coming out of the water. No biggie.


---
**Thor Johnson** *September 08, 2016 18:20*

Yep.  The bubbles should move when you turn the pump back  on.




---
**greg greene** *September 08, 2016 19:13*

no worries


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/ME3jvxsouMs) &mdash; content and formatting may not be reliable*
