---
layout: post
title: "Looking for a smoothieware config.txt already setup and being used in an K40"
date: August 27, 2016 21:32
category: "Hardware and Laser settings"
author: "Robi Akerley-McKee"
---
Looking for a smoothieware config.txt already setup and being used in an K40.  So I can compare my entries to what your running.





**"Robi Akerley-McKee"**

---
---
**Ariel Yahni (UniKpty)** *August 27, 2016 21:55*

your email and ill sent it to you


---
**Ariel Yahni (UniKpty)** *August 27, 2016 21:57*

better yet, find it here [https://gist.github.com/cojarbi/907b483f18278380c8c5805c9955aefb](https://gist.github.com/cojarbi/907b483f18278380c8c5805c9955aefb)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 27, 2016 23:43*

I have stripped all the extruder/temperature control/etc lines from the file (since we don't need & it just confuses the issue when trying to find other settings to tweak).


---
**Robi Akerley-McKee** *August 28, 2016 19:05*

very nice thanks


---
**Jon Bruno** *August 28, 2016 23:43*

Pwm duty is 200uS in that config. Is that accurate?


---
*Imported from [Google+](https://plus.google.com/103866996421752978311/posts/B4tTP35mPmN) &mdash; content and formatting may not be reliable*
