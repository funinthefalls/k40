---
layout: post
title: "I have had a K40 for a few years"
date: October 11, 2016 18:18
category: "Modification"
author: "Trenton"
---
I have had a K40 for a few years.  It has served me well, but I wanted something that could engrave a little faster.  Recently I purchased this: [http://www.ebay.com/itm/High-Precise-USB-Port-Laser-Engraving-Cutting-Machine-Engraver-Cutter-60W-CO2-/262600263802?hash=item3d2432087a:g:rEIAAOSwgmJXxPA7](http://www.ebay.com/itm/High-Precise-USB-Port-Laser-Engraving-Cutting-Machine-Engraver-Cutter-60W-CO2-/262600263802?hash=item3d2432087a:g:rEIAAOSwgmJXxPA7)



The seller promised me that I would still be able to use the CorelLaser plugin with CorelDraw.  However, when I received it, you clearly have to use something like RDWorks to operate the laser. 



Can anyone here tell me if it is possible to upgrade the K40 so it engraves faster, but I would also like to still be able to use the CorelLaser plugin with CorelDraw?  



The smoothieboard seems to be very popular here.  Does that have a plugin with CorelDraw, so I could easily run the laser from CorelDraw?



Thanks!









**"Trenton"**

---
---
**Ariel Yahni (UniKpty)** *October 11, 2016 18:23*

What do you consider "faster"? Do you have an example of something slow that you think should be much faster? 


---
**Trenton** *October 11, 2016 18:34*

Currently, my K40 will vector cut at a max of about 150 mm/s or so.  I use this to "engrave" text onto wood, so if it could be at least 500 mm/s, that would be great.  It is currently set at 500mm/s, but I know it doesn't actually go that fast. If I try to set it to go faster, it starts going crazy(the laser head moves really fast, but is really jerky, and makes a grinding noise) 



I'm not sure how fast I can raster engrave, but would like that speed to increase too.  Currently, it takes about 5 minutes to engrave a 1.5"x1.5" square with a letter in it.


---
**Ariel Yahni (UniKpty)** *October 11, 2016 18:50*

Interesting. Can you post that exact example, maybe me or someone can corroborate on the speed by 5 min on that small square seems to much. With smoothie you can indeed increase the acceleration plus you get much better engrave but the stock hardware itself can be a limitation. 


---
**Trenton** *October 11, 2016 18:59*

Yeah that'd be great.  Do you just want a picture of the engraved item and exactly how long it took, or a video? What info would be the most helpful for you?


---
**Ariel Yahni (UniKpty)** *October 11, 2016 19:13*

The file itself that took so long for me to try it


---
**Trenton** *October 11, 2016 22:07*

Here's the link to the corel file for the raster engraving: [https://drive.google.com/file/d/0B66PTjPZU6pnRExSLUpOeVd5YVE/view?usp=sharing](https://drive.google.com/file/d/0B66PTjPZU6pnRExSLUpOeVd5YVE/view?usp=sharing)



I messed with the settings a little and was able to engrave it in 2:35, so that's much better than 5.  Curious to see what yours can do it in though.



I would really like to improve my vector cutting too.  I can do this file in 45 seconds.  Is that decent?  I have my speed set to 500mm/s.  It doesn't do it any faster if I increase it, so I'm guessing it might be maxed out : [https://drive.google.com/file/d/0B66PTjPZU6pnSnNHT0VNQVVrWW8/view?usp=sharing](https://drive.google.com/file/d/0B66PTjPZU6pnSnNHT0VNQVVrWW8/view?usp=sharing) 



Thanks for your help!



[drive.google.com - Test.cdr - Google Drive](https://drive.google.com/file/d/0B66PTjPZU6pnRExSLUpOeVd5YVE/view?usp=sharing)


---
**Trenton** *October 11, 2016 22:09*

Would changing these settings have any effect on the speed?

![images/7ab55d013ef468908f7085e4709b2390.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ab55d013ef468908f7085e4709b2390.jpeg)


---
**Ariel Yahni (UniKpty)** *October 11, 2016 22:20*

**+Yuusuf Sallahuddin**​ or **+Alex Krause**​ are the experts on those setting. Regarding cutting, are you expecting to cut at 150mms? What type of material? 


---
**Alex Krause** *October 11, 2016 22:21*

Pixel size will... that is basically your scan gap... the distance between Y moves as the carriage moves to the front of the machine


---
**Trenton** *October 11, 2016 22:23*

No, I use the vector cutting to "engrave" letters.  I know that I'm not going to cut through much at that speed, but to simply use it as a wood burner, a little more speed would be nice.  



If I upgraded to a smoothie board, would I still be able to use the CorelLaser toolbar?


---
**Alex Krause** *October 11, 2016 22:26*

You can use corel to preprocess the image but corel laser is a closed source proprietary plugin used for the stock control boards that come with the machine


---
**Trenton** *October 11, 2016 22:27*

Ok thanks!  So what do most people use? Rd works?


---
**Ariel Yahni (UniKpty)** *October 11, 2016 22:28*

**+Trenton Hovland**​ I get it. The faster you go,  the high the tube power must be. If you are not getting the desired burn at 150mms full power on the k40 then what you need is higher power


---
**Trenton** *October 11, 2016 22:28*

**+Alex Krause** could you verify that the settings in the above photo are ok?


---
**Alex Krause** *October 11, 2016 22:30*

The k40 stock hardware inside (gantry/linear guides/mirror mounts) aren't sturdy enough to go very fast when moving on both axis at the same time. I see a noticeable decrease in quality of vector cutting after about 40mm/sec


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 11, 2016 23:52*

**+Trenton Hovland** When I was still running the stock hardware & CorelLaser I was using WMF for both those Engrave & Cut settings.



edit: not sure if it effects speed however.


---
**Trenton** *October 12, 2016 00:01*

**+Yuusuf Sallahuddin** Ok, thanks!


---
**Trenton** *October 12, 2016 00:43*

one last question (for now, haha)



What does it mean when my laser does this?  It does this for high (>500mm/s) speed settings for engraving.  It also does it for some cutting speed settings too:



[https://drive.google.com/file/d/0B66PTjPZU6pnYUpocjl1RVNlVkk/view?usp=sharing](https://drive.google.com/file/d/0B66PTjPZU6pnYUpocjl1RVNlVkk/view?usp=sharing)

[drive.google.com - IMG_2047.MOV - Google Drive](https://drive.google.com/file/d/0B66PTjPZU6pnYUpocjl1RVNlVkk/view?usp=sharing)


---
**Alex Krause** *October 12, 2016 00:45*

You are moving way to fast


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 12, 2016 00:57*

I second that, too fast. Also can happen when your belt tension is not right, but >500mm/s is definitely too fast. Max I could ever get with decent tension on the belt was 500mm/s. Anything more & would cause issues like your video.


---
**Ariel Yahni (UniKpty)** *October 12, 2016 00:59*

That video also seems like you are trying to go over the max area and its hitting and coming back


---
*Imported from [Google+](https://plus.google.com/108213621154298105694/posts/MZ2kt4bptMd) &mdash; content and formatting may not be reliable*
