---
layout: post
title: "Just Joined. Looking to get either a K40 or 5w Blue laser machine do etch a logo in black anodized aluminum"
date: September 19, 2016 16:13
category: "Discussion"
author: "Cloudbase Engineering"
---
Just Joined.  Looking to get either a K40 or 5w Blue laser machine do etch a logo in black anodized aluminum.  Parts are roughly 1.5" square.  Is there enough Z adjustment to get a part this thick under the laser and still be in focus?  I know there are motorized tables and DSP mods and such, but my main need is just logos and possibly to etch a design in leather so would prefer to use this machine unmodified.  For larger work, I will likely buy the 50w or 60w at a later date but don't have the room now for the larger machine.  I have read so much regarding poor and dangerous wiring, but these machines have been around a while and many now have an updated panel and curious if the new ones with the updated panel are any better.



I use to do all my cutting on a Morn 80w laser but I n longer have access to that machine due to a job change.  Anyone interested in my parts can visit my website to see what I want to engrave as well as my logo.



Best regards,



Marc

[www.cloudbaseengineering.com](http://www.cloudbaseengineering.com)









**"Cloudbase Engineering"**

---


---
*Imported from [Google+](https://plus.google.com/115143003847620794726/posts/J37SMV7k1zd) &mdash; content and formatting may not be reliable*
