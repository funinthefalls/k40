---
layout: post
title: "An update on the LPS HV lab"
date: April 02, 2017 18:36
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
An update on the LPS HV lab.

Got motivated after finishing all my honey do's for the weekend :)



These are the LPS labs V1.0 it features:



Measurements..................

1. 24v & 5v DVMs

2. HV current analog meter

4. HV current scope input

5. High voltage DVM (40,000v)

6. High voltage scope input attenuator (yikes)



Controls.............................

1. Current adjustment

3. Enable and Fire

4. HV dummy load

4. Main power and fuse



Safety.................................

1. Momentary Fire and Enable

5. Two handed HV dead man switches (RED/GRN). (you have to hold them both so that assumes you are not holding on to any HV :) )

6. Cover interlock on HV compartment

7. Acrylic frame and covers

8. HV wiring isolated with 2" acrylic standoffs

9. All HV connections in enclosed compartment

10. Rubber gloves



To do .............................

1. PWM input jack

2. Adjustable PWM generator

3. Low voltage load test

4. Dummy flyback load

5. Flyback tester

6. More HV compartment covers

7. Cooling



![images/7bb9af619e743c56206a23444d2f619f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bb9af619e743c56206a23444d2f619f.jpeg)
![images/98c86afaab39afe04274147d8bf952cb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98c86afaab39afe04274147d8bf952cb.jpeg)
![images/ab9cb00ae86309ef997163e071a75689.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab9cb00ae86309ef997163e071a75689.jpeg)
![images/c6827f38bc9eab0ae0f3a59b15799c52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c6827f38bc9eab0ae0f3a59b15799c52.jpeg)
![images/8dbb32320dd1843fa8209ab78143ce7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8dbb32320dd1843fa8209ab78143ce7c.jpeg)
![images/ec84c4a5d42e19c09e53e2f77c2f2b64.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ec84c4a5d42e19c09e53e2f77c2f2b64.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *April 02, 2017 18:52*

Evil mad scientist 


---
**greg greene** *April 02, 2017 18:56*

But does it allow it to work better?


---
**Cesar Tolentino** *April 02, 2017 18:59*

Don. Did you salvaged the original power supply? Or did you start from scratch?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2017 19:00*

I like the idea of the "dead man's switches". Although, maybe in this case they should be referred to as "alive man's switches" since they are stopping you from being dead.


---
**Alex Krause** *April 02, 2017 19:53*

Anti tie down device is what I would call it...


---
**Don Kleinschnitz Jr.** *April 02, 2017 19:59*

**+greg greene** I hope to learn enough to improve their reliability


---
**Don Kleinschnitz Jr.** *April 02, 2017 20:00*

**+Cesar Tolentino** this is a dead donated supply. Hopefully soon it will be alive again. 


---
**greg greene** *April 02, 2017 20:03*

good luck - improved reliability for these machines would be a great thing !


---
**Claudio Prezzi** *April 02, 2017 20:19*

Very nice build!


---
**Nate Caine** *April 02, 2017 21:23*

Don, are the Laser Power Supplies you are experimenting with roughly the same as the schematic you posted a few weeks back from Paul deGroot?  



[Looking at the circuit I have some questions about the schematic itself, and a few missing component values...especially around the control inputs at the front end of the TL494.  It's unclear where the connector terminals are, and what is external to the power supply.]



On your impressive test setup, what are you using as a stand-in for the laser tube itself?  A resistor might give you the steady-state response, but with the ignition breakdown of the laser when it fires, I doubt you'll get a valid dynamic response (considering the negative resistance region of the tube).



Is your stand-in load good for high-voltages such that it won't arc over?  Do you have sufficient wattage there to run it without it burning up?  






---
**Don Kleinschnitz Jr.** *April 02, 2017 22:13*

**+Nate Caine** 



 Inline <b>*</b> below:



"Don, are the Laser Power Supplies you are experimenting with roughly the same as the schematic you posted a few weeks back from Paul deGroot? " 

<b>**YES, less some missing stuff from the input as you suggest.</b>

<b>.................</b>

<b>"[Looking at the circuit I have some questions about the schematic itself, and a few missing component values...especially around the control inputs at the front end of the TL494.  It's unclear where the connector terminals are, and what is external to the power supply.]"</b>



<b>***You are right, the inputs terminals are missing on Paul's drawing. I never updated it (That was supposed to be my part :( ). Also there is an error as to where the pot and diodes are connected. I plan to update the schematic as I repair my first supply.</b>

<b>However the equivalent circuits that are here: </b>

[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)<b> </b>

<b>... are very close to functionally correct.</b>

<b>I will release a new schematic after I rebirth one of these supplies</b>

<b>................</b>

<b>"On your impressive test setup, what are you using as a stand-in for the laser tube itself?  A resistor might give you the steady-state response, but with the ignition breakdown of the laser when it fires, I doubt you'll get a valid dynamic response (considering the negative resistance region of the tube)."</b>

<b>**</b> Excellent question, one I am asking myself regularly. 

I am using the static resistance just to test basic HV function and validate repairs. I am troubleshooting with the fly-back removed until I verify all but the final HSwitch. Certainly a resistor will not simulate the negative resistance of ionization.



I am thinking about using a spark gap (although I have heard from manuf. that these supplies do not like arcs). I will know better as I look at the real driving circuits more.  I am not sure why an arc in open air would be a problem it seems like that would draw less current than the ionizing tube. Perhaps a spark gap with a high voltage resistor.

Of course there is always the option to hook up a tube :(. All ideas are welcome.



"Is your stand-in load good for high-voltages such that it won't arc over?" 

<b>*</b> Marginal but I think so. These resistors are high voltage rated and the spacing exceeds what I calculated is necessary.

...............

 "Do you have sufficient wattage there to run it without it burning up? 

<b>*</b> Certainly not continuous & full load, but these were the highest wattage/highest voltage I could find without breaking the budget. 

There could be smoke while I find better ones. 

I know some laser cutter manuf. ship a power resistor with the machine to test supplies but I have never found a source for one of those.

I plan to run the static load at lowest current and for very short (pulse) short periods of time. 


---
**Paul de Groot** *April 03, 2017 00:45*

**+Don Kleinschnitz**​ i really like the look of it and amazing how fast you got to do this. Unfortunately I haven't had any chance to open my psu to update the schematic since I am using the machine a lot for the makerfaire.  Just too scared i break it again😊


---
**Don Kleinschnitz Jr.** *April 03, 2017 01:37*

**+Paul de Groot** **+Nate Caine**

Here is an update after tonight's troubleshooting.

Just a sketch and missing labels but it is the correct interconnections.

![images/cef991bff3b04871cf1496772acc8017.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cef991bff3b04871cf1496772acc8017.jpeg)


---
**Paul de Groot** *April 03, 2017 06:57*

Pin 15 and 16 floating is really not a good design. Maybe you can find something in the ua494 datasheet how to disable them from picking up noise signals. Great schematic. Looking forward how this is coming together. Thanks! 


---
**Claudio Prezzi** *April 03, 2017 07:58*

**+Don Kleinschnitz** Nice schematic. 

Where is the L-pin connected? P+ or Fire?

There is one pot in your schematics, that seems to be for oscilator frequency. Where are the other two pots?


---
**Don Kleinschnitz Jr.** *April 03, 2017 10:15*

**+Claudio Prezzi** The L pin and the Fire pin on this supply are connected together.

This is just a supplement to the main schematic with a check and more detail on the PWM control schema. 

The other two pots are: one in the HV sense to adjust laser current and the other in the 24V switcher to adjust the 24V. They are already in the main schematic. 



This supply does not have a P+.


---
**Claudio Prezzi** *April 03, 2017 10:29*

Ah ok. Did you plan to combine the two schmatics to one complete schematic?


---
**Don Kleinschnitz Jr.** *April 03, 2017 10:38*

**+Claudio Prezzi** 

is this where I am supposed to say "RTFM" .... just kidding...seriously just kidding :). I hate that term....cause stuff is hard to find on this community :(.



The main schematic (original by **+Paul de Groot**) will be refreshed after I verify everything with this supply. 



BTW your supply (white connectors) control pins are  different than this one but internally works mostly the same. I have a dead supply like yours and its next :).



Links: ..........................



[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)



[https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view](https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view)






---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Fzchn6VHHDe) &mdash; content and formatting may not be reliable*
