---
layout: post
title: "Tilt your PSUs boys! (and girls) Did show this a couple of months ago, got some more data now ;) No1 killer of the PSUs are heat, and long runs will make it HOT, and with the PSU inside another chamber its getting awful"
date: January 03, 2017 00:42
category: "Modification"
author: "HP Persson"
---
Tilt your PSU´s boys! (and girls)



Did show this a couple of months ago, got some more data now ;)

No1 killer of the PSU´s are heat, and long runs will make it HOT, and with the PSU inside another chamber it´s getting awful hot, it cools itself with the air it already heated.



I 3D-printed a holder for my PSU, tilted it so the air it cools itself with is ambient room temp. And added a small fan to take care of the radiated heat inside the small chamber.

For me it made a 30c+ change when running.

I did a test with and without cooling it with ambient temps, did five runs and the runs with the PSU in the chamber showed less power (2-4mA less) as the breathing version did, with the PSU taking roomtemp to cool itself.

Just did a bunch of lines for 10 minutes, same ambient, same watertemp on all tests.

This small mod works, and it works good!



You do not need to 3D print anything, just use double sided tape and a extra ground wire to metal and flip the PSU standing ;)

Link to the 3D-model if you want it: [http://www.thingiverse.com/thing:1728665](http://www.thingiverse.com/thing:1728665)

![images/b9ffb0da0ffe23007061a783fd5cd751.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9ffb0da0ffe23007061a783fd5cd751.jpeg)



**"HP Persson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 03, 2017 01:43*

That's interesting to know. Thanks for sharing your results.


---
**Don Kleinschnitz Jr.** *January 03, 2017 04:03*

Confused. Are you drawing air from the right side of the cabinet IE punched holes in the wall.

I have a fan in the PS and in the back of the cabinet directly behind the supply does yours?


---
**HP Persson** *January 03, 2017 07:06*

On my machine there is ventilation holes in the right wall.

And the PSU fan is on top, tilting the PSU makes it breathe from the room air, not the heated air inside the case :)



Here is my PSU and you can see the ventilation holes.

![images/a51d4f58d14f03de007757f28dfb5397.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a51d4f58d14f03de007757f28dfb5397.jpeg)


---
**Don Kleinschnitz Jr.** *January 03, 2017 13:31*

Yes mine has the same vents but I worried that it being small slots would reduce some of the fan area, evidently not.

Do you also have a fan in the back of that cabinet, as I do. I am surprised at this difference in temp considering that two fans are employed. 

I am interested in this change as it's clear that these supplies are not very reliable. A 30c change is huge, were and how did you measure it.


---
**HP Persson** *January 03, 2017 16:50*

**+Don Kleinschnitz** Yeah, got a 90mm fan venting some of the radiated heat. That also helps!



I had a DS18B20 temp sensor inside the PSU, controlled by a arduino, and one in the water tank so every test was made with exact water temp.

Sensor can be seen on a white tape on the pic above, but it was inside when i did theese tests :)



And measured the mA on the machine while running.

2mA+ difference with PSU tilted VS not tilted, on the same settings, same water temp, same ambient temp.



Can probably be made more scientific, i just wanted to see if there was any difference. 

mA was measured with a digital amp-meter, and a claw amp-meter.



Been doing advanced computer cooling builds for years, and i just applied the knowledge from that to my machine (cool with ambient air, not heated air).


---
**Don Kleinschnitz Jr.** *January 03, 2017 17:09*

**+HP Persson** seems to be a profound find.... I moved my supply back to fit my new DC supplies perhaps I should do this as well.


---
**HalfNormal** *January 03, 2017 21:58*

**+Don Kleinschnitz** **+HP Persson**  I was worried about drawing the air out of the electronics area with the rear fan would also draw contaminates from the working area into the electronics area. I turned the fan around to draw air into the electronics area which would also push air into the working area. This has worked very well for me so far.


---
**Don Kleinschnitz Jr.** *January 03, 2017 23:55*

another stellar and easy to do  idea..


---
**Don Kleinschnitz Jr.** *January 04, 2017 04:11*

**+HalfNormal** While looking at the PS this PM I realized another thing that may not help the supplies reliability. These supplies are wrapped in a thin plastic film (blue) that I think is just to protect the surface . I would imagine that it inhibits heat radiation and should be removed. The high power components are connected to the case.


---
**HP Persson** *January 04, 2017 08:17*

All solutions where the air is exchanged is better than original :)



Also seen some K40 has a built in fan in the smaller chamber, not mine though.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2017 03:24*

My K40 has a built in fan in the electronics chamber, but I've never once seen or hear it moving. I think it's a dud (or bad wiring). I also pulled off the thin blue film of plastic **+Don Kleinschnitz** as it looked unsafe to me.


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/5Da6pPkhuno) &mdash; content and formatting may not be reliable*
