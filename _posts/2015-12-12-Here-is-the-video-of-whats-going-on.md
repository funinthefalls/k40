---
layout: post
title: "Here is the video of what's going on"
date: December 12, 2015 23:19
category: "Discussion"
author: "ChiRag Chaudhari"
---
Here is the video of what's going on. Am I not supposed to keep firing laser continually?



Is that why the power I'd dropping?  


**Video content missing for image https://lh3.googleusercontent.com/-4qJU1n5jVVo/VmyrlamV5sI/AAAAAAAAgvk/zbbcMC_QAuM/s0/20151212_170220.mp4.gif**
![images/9619d25ea0d4e2ba1cc311eb378603a0.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/9619d25ea0d4e2ba1cc311eb378603a0.gif)



**"ChiRag Chaudhari"**

---
---
**Scott Thorne** *December 12, 2015 23:42*

Check all your grounding, the outer shouldn't drop like that even at 100%.


---
**ChiRag Chaudhari** *December 13, 2015 00:47*

**+Scott Thorne** Yeah I checked everything again. Also made sure the machine ground wire is properly attached. But its still the same. However if I run the laser at 43 power with ppm value seems like it stays up there instead of dropping. 



Also do you what is the max value for PPM ?

And on the home screen there is : fR 100% and you can change value with knob from 10 to 999. what does that do ?


---
**Anthony Bolgar** *December 13, 2015 02:55*

The knob changes the feed rate...100% is what the gcode file sets, you can basically speed it up to 10X the set speed.


---
**Andrew ONeal (Andy-drew)** *December 17, 2015 05:37*

Why are you pushing the test fire button?


---
**ChiRag Chaudhari** *December 19, 2015 04:28*

**+Andrew ONeal** To fire the laser without running any G-Code file.


---
**Andrew ONeal (Andy-drew)** *December 19, 2015 05:13*

Mine is test fired via LCD


---
**Andrew ONeal (Andy-drew)** *December 19, 2015 05:20*

Also, previously I had based my build on weistek engineering modification which has me invert my Y axis and change other  home parameters. After playing in the code I found that I didn't need to invert at all now laser homes perfectly, now just need to make my first cut. I'm going to attempt both inkskape and sd card, then Corel via visicut. Just want so bad to make something after all this work. I have a friend who owns a botfarm of mainly 3dprinters that suggests strongly that I make the change to DSP electronics. 


---
**Scott Marshall** *December 19, 2015 05:20*

Is the discharge in the tube changing appearance as the power fades? That will tell you if it's indeed changing. The color can be an indicator of a leaky/contaminated tube as well. I'm not sure of the specifics, There's details on that on the CNC forum.


---
**ChiRag Chaudhari** *December 19, 2015 05:38*

**+Scott Marshall** yeas when it outputs full power ie 9-10mA I see bright purple lights and as soon as power drops it gets dull and less dense. 


---
**Scott Marshall** *December 19, 2015 05:39*

I think the bright purple is bad. Let me see if I can find the details for you.


---
**ChiRag Chaudhari** *December 19, 2015 05:41*

Tomorrow I will post video is the tube in action. Thanks a lot.


---
**Scott Marshall** *December 19, 2015 05:55*

Theres useful info over on CNC zone, although I can't find the post o the color of the discharge when failing. 

I do remember pink around the positive electrode is bad, Yellow is a warning your pushing it too hard, and something about blue vs violet, but can't remember the details, Are links allowed here?


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/8KfcGXiNC19) &mdash; content and formatting may not be reliable*
