---
layout: post
title: "very interesting"
date: September 17, 2016 03:47
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
very interesting





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 04:29*

That is interesting. Seems to speed up the process between iterations a significant amount.


---
**Don Kleinschnitz Jr.** *September 17, 2016 11:48*

Cool software but I guess you can't get a copy?


---
**Anthony Bolgar** *September 17, 2016 15:21*

I emailed the proffesor in charge of the project, maybe we will get luck and he will give us a copy.


---
**Anthony Bolgar** *September 17, 2016 22:27*

Update: The professor in charge of this project has put me on the Beta tester list for when the software is closer to being finished, much of what the video shows is a mock up.


---
**Don Kleinschnitz Jr.** *September 18, 2016 01:54*

**+Anthony Bolgar** lol professor doing some marketing ....


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/GA4c15hTiHf) &mdash; content and formatting may not be reliable*
