---
layout: post
title: "I wanted to ask if anyone has had trouble with the laser not matching location of software"
date: November 24, 2017 13:14
category: "Hardware and Laser settings"
author: "james blake"
---
I wanted to ask if anyone has had trouble with the laser not matching location of software.  I have set the home location and cut a stop board so that the edge is on the zero line in both x and y but when I cut or engrave it is off 5mm in the y axis and off 3mm in the x axis.  Is this normal and is there a way to fix or just make the corrections in where I want the project to start?





**"james blake"**

---
---
**Ned Hill** *November 25, 2017 04:37*

Assuming you are using corellaser, or laserdraw, can you post screenshots of your settings?


---
**james blake** *November 25, 2017 12:55*

I am using Moshidraw.  It is an older machine 2013 or 14.  I have a screen shot of page but it will not let me upload it into post.


---
*Imported from [Google+](https://plus.google.com/115955612231558356912/posts/XT8FCDHoi4S) &mdash; content and formatting may not be reliable*
