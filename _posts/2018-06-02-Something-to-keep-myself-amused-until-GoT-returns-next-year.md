---
layout: post
title: "Something to keep myself amused until GoT returns next year"
date: June 02, 2018 00:19
category: "Object produced with laser"
author: "Tony Sobczak"
---
Something to keep myself amused until GoT returns next year.



![images/5b67411e0ffceb5c955fb1b57e12629e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5b67411e0ffceb5c955fb1b57e12629e.png)
![images/7f6d3e939dd57c32214abcf2d703cdb5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7f6d3e939dd57c32214abcf2d703cdb5.png)
![images/1be7b96f3ded4531db7662ff3313bece.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1be7b96f3ded4531db7662ff3313bece.png)
![images/f43d2a5aa7ebef7d32d3c070b4676993.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f43d2a5aa7ebef7d32d3c070b4676993.png)
![images/4e6c87252b46cade443b8379b4678113.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4e6c87252b46cade443b8379b4678113.png)

**"Tony Sobczak"**

---
---
**Joe Alexander** *June 02, 2018 01:42*

very nice!


---
**Fook INGSOC** *June 04, 2018 01:36*

WoW!


---
**Joe Alexander** *June 04, 2018 02:12*

did you pull these patterns off a site or share them anywhere like thingiverse??


---
**Tony Sobczak** *June 05, 2018 05:52*

Made them myself.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/1Yp9rsVCeh8) &mdash; content and formatting may not be reliable*
