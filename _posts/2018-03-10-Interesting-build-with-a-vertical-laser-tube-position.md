---
layout: post
title: "Interesting build with a vertical laser tube position"
date: March 10, 2018 16:13
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Interesting build with a vertical laser tube position.



I wonder about the chalenges of:

...Wiring the High voltage through x-y moving axis

...Mechanical shock to the tube with a significant vertical cantilever.



Interesting none-the-less ......




{% include youtubePlayer.html id="EHOpS2NP4Zs" %}
[https://www.youtube.com/watch?v=EHOpS2NP4Zs&feature=push-sd&attr_tag=uHt-IffWjXN6_8_U-6](https://www.youtube.com/watch?v=EHOpS2NP4Zs&feature=push-sd&attr_tag=uHt-IffWjXN6_8_U-6)





**"Don Kleinschnitz Jr."**

---
---
**Cris Hawkins** *March 10, 2018 17:29*

The mechanical mount for the laser looks pretty stout to me. I wouldn't worry about 'shock' to the tube. Also, Daniel has been building laser cutters for years (having several products) and is likely very familiar with what works. In my experience, he is a very sharp guy. He is also an electronics guy (I believe), so he would be able to handle the high voltage stuff better than most.



But I agree your concerns in the hands of the inexperienced. There are definitely things that can sneak up and bite!


---
**Cris Hawkins** *March 10, 2018 17:44*

OOPS! My mistake. The person's name is Patrick. His last name is Hood-Daniel


---
**Don Kleinschnitz Jr.** *March 10, 2018 21:20*

**+Cris Hawkins** don't get me wrong I am not doubting anyone's ability simply recognizing the size of challenge. 



I just know that running HV wiring over a long distance is a challenge in itself. Adding the need for it to move in a drag chain of some sort adds to that challenge.  



As far as the laser mount is concerned rigid mounting may not prevent the internal optics from shifting under the inertial forces. I don't know anything about the internal stability of the internal optics when the laser is moved. Then again I believe the Fab Pro moves the tube :).



It would be nice to have one machine that cuts with bits and light .....



 


---
**Cris Hawkins** *March 10, 2018 23:44*

**+Don Kleinschnitz** it was my intention to acknowledge Patrick's abilities along with agreeing with your assessment of the challenges. I do not intend to diminish your remarks in any way. My apologies if I was not clear.


---
**Steve Clark** *March 12, 2018 17:39*

I agree with both of you. It would be interesting to see how he deals with the the HV and fast moves. I've also done work with Patrick (and Cris) in the past. 


---
**Timothy Rothman** *March 15, 2018 16:03*

I love the idea.  I think I need at least 2 of those routers though. :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/AuBY9cR1RUU) &mdash; content and formatting may not be reliable*
