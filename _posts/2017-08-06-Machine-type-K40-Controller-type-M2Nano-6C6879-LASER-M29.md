---
layout: post
title: ".Machine type: K40 .. Controller type: M2Nano 6C6879-LASER-M2:9 .."
date: August 06, 2017 04:27
category: "Original software and hardware issues"
author: "Yosman Ca\u00f1izalez"
---




#K40brokeXAxis 



.Machine type: K40

.. Controller type: M2Nano 6C6879-LASER-M2:9

.. Firmware:

.. Firmware config: 

.. Problem tenacity: new problem



Help! 



Hello! After engraving , I am having this problem: The stepper motor of the X axis, is failing, can be seen on the [video.It](http://video.It) sounds weird while moving and jump.



I have test the X motor on the conection of the Y Axis and it works OK. Limit Switch works OK.  



Could be a controller problem, Should I remplaced it? 



Thanks in anticipation!

![images/c4b3bf98ffeffdabb6577bcb37713164.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4b3bf98ffeffdabb6577bcb37713164.jpeg)



**"Yosman Ca\u00f1izalez"**

---
---
**Phillip Conroy** *August 06, 2017 10:34*

Can you swap x moter conection wit the y on on the controler board and see if fault follows


---
**Yosman Cañizalez** *August 06, 2017 11:26*

**+Phillip Conroy** yes! I did 

, and the fault continues 


---
**Martin Dillon** *August 06, 2017 19:41*

I have had similar problems and it has been a bad wire or connection.  Maybe disconnect motor and check with ohm meter and wiggle all the wires.


---
**Yosman Cañizalez** *August 06, 2017 21:39*

**+Martin Dillon** I did and woks ok in the Y axis. Motor Y on X axis same problem. 


---
**Fabiano Ramos** *February 27, 2018 01:36*

**+Yosman Cañizalez** Did you solve your problem? I think I'm in the same problem as yours.



[plus.google.com - Hello Friends, thx for attention! I have this problem on my K40. She came wi...](https://plus.google.com/111567065741989535353/posts/a16wy1K32GP)


---
**Yosman Cañizalez** *February 27, 2018 01:45*

**+Fabiano Cardoso Ramos** Hello! Yes, I did, I changed the mainboard and it solved the problem. The seller sent me a new mainboard.


---
*Imported from [Google+](https://plus.google.com/114977756769245164578/posts/W6oFQPdaWJH) &mdash; content and formatting may not be reliable*
