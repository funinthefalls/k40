---
layout: post
title: "Thought this might be of interest to some"
date: January 15, 2018 18:01
category: "Material suppliers"
author: "David Davidson"
---
Thought this might be of interest to some.



[http://www.mpja.com/01-11-18.asp?r=311310&s=2](http://www.mpja.com/01-11-18.asp?r=311310&s=2)



Don't know if it's any good but on sale for $6.95.





**"David Davidson"**

---
---
**Steve Clark** *January 16, 2018 01:56*

Interesting it should work ...I think...I can't find anything that tells you what the output trigger voltage and current is.  



For those not understanding the use, it could be used to turn on and off a cooling system like Pelters or some other refrigeration system but you need an appropriate relay down line for it to trigger. 



Also, I suppose just an alarm to let you know the coolant is too warm or even too cold for the current humidity. 




---
**HP Persson** *January 16, 2018 09:59*

There is another one, similar to this named "w1209" to be found almost anywhere, good documentation on youtube and other places.

I use them to turn my peltiers on/off :)


---
*Imported from [Google+](https://plus.google.com/+DavidDavidson-tristar500/posts/QW1mKwLndsH) &mdash; content and formatting may not be reliable*
