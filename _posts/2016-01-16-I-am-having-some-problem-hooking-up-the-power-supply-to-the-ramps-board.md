---
layout: post
title: "I am having some problem hooking up the power supply to the ramps board"
date: January 16, 2016 21:49
category: "Modification"
author: "Pedro Flores"
---
I am having some problem hooking up the power supply to the ramps board. The power supply is myjg40w. Where does D5 and D6 go on the power supply. I looked at the other drawings but they have a different power supply. This is getting connected to a ramps board.

![images/702123d5f6cd1a41ee5f5a57bbc02572.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/702123d5f6cd1a41ee5f5a57bbc02572.jpeg)



**"Pedro Flores"**

---
---
**Anthony Bolgar** *January 16, 2016 23:13*

D6 goes to in, D5 should go to K+(That should be the trigger on high pin)


---
**Pedro Flores** *January 16, 2016 23:17*

Does L get used?




---
**ChiRag Chaudhari** *January 17, 2016 00:34*

**+Pedro Flores** 

[https://plus.google.com/u/0/+ChiragChaudhari/posts/gW49XQXTqNi](https://plus.google.com/u/0/+ChiragChaudhari/posts/gW49XQXTqNi)



Check out pic 4 an 5. It should solve your issue for sure.


---
**Pedro Flores** *January 17, 2016 02:01*

Thanks. I got it to fire now. Y axis seems to only go in one direction if I hit +or-. X axis is not working. Any suggestions?

 


---
**ChiRag Chaudhari** *January 17, 2016 02:25*

You mean + and - in laser web Machine Control?


---
**Pedro Flores** *January 17, 2016 03:10*

yes




---
**ChiRag Chaudhari** *January 17, 2016 03:36*

In laserweb  x and y are inverted, so dont worry about that. I use LW to send quick commands like M5, G28 or G0 X0 Y0 F5000 etc. But I use Turnkey inkscape plugin to generate gcodes and run it via SD card. You can use LW to generate gcodes, but running it from the SD card it necessary. When you sent large gcode file via LW , because of lower speed and/or low memory it messes up the raster/cutting. The laser head wonder around while firing laser ruins the material. 


---
**ChiRag Chaudhari** *January 17, 2016 05:27*

**+Peter van der Walt** I can not wait for Smoothiebrainz to come out. Matter of fact I want to be US dealer if I can! 



But You think if I lower the baud rate it may work? But hey we really need to talk about some something I have planned in my head.


---
**Anthony Bolgar** *January 17, 2016 08:22*

Do you have a Canadian re-seller?


---
**Pete OConnell** *January 17, 2016 11:46*

or a UK? :)


---
**Pete OConnell** *January 17, 2016 11:46*

and by UK I mean EU :)


---
*Imported from [Google+](https://plus.google.com/116137110819103096995/posts/gJv6qp1Cjwc) &mdash; content and formatting may not be reliable*
