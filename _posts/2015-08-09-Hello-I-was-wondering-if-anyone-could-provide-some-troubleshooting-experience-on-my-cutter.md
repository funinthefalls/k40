---
layout: post
title: "Hello, I was wondering if anyone could provide some troubleshooting experience on my cutter"
date: August 09, 2015 02:01
category: "Hardware and Laser settings"
author: "Andy Trimpe"
---
Hello, I was wondering if anyone could provide some troubleshooting experience on my cutter. My pump failed in the middle of a job and the overheat cracked the tube. I ordered a new 40w tube from the same supplier that I bought the machine from originally. I installed the new tube and it would not fire. They have me check it for cracks (none) and make sure that the HV wire at the far end from the mirror would arc if you got it near a ground and hit the test fire button (arced fine). They send me a replacement and the new one exhibits the same symptoms. Any advice?





**"Andy Trimpe"**

---
---
**Joey Fitzpatrick** *August 09, 2015 22:58*

Does the laser tube glow or make any noise when you test Fire?


---
**Andy Trimpe** *August 09, 2015 23:04*

No glow or noise. When I turn it on the stepper motors do their normal thing and the chassis fan comes on.


---
**Joey Fitzpatrick** *August 10, 2015 14:43*

It sounds like you may have a problem with your Power supply(High Voltage side)  Even though the HV wire will arc near ground, it may not have enough current to work properly. Here is a link to an instructable that shows how to check your  power supply's current output.  [http://www.instructables.com/id/How-to-test-and-set-a-Chinese-China-made-CO2-Laser/](http://www.instructables.com/id/How-to-test-and-set-a-Chinese-China-made-CO2-Laser/)                                                              Keep in mind that your maximum current should be 18 ma on a K40(For the best tube life)


---
*Imported from [Google+](https://plus.google.com/101551676776164811030/posts/MLfDfPRaJYX) &mdash; content and formatting may not be reliable*
