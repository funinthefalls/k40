---
layout: post
title: "Engraved something new today.. Our campus safety officer asked if I could engrave the top slide on his Glock service pistol.."
date: September 15, 2016 21:33
category: "Hardware and Laser settings"
author: "Chris Boggs"
---
Engraved something new today..  Our campus safety officer asked if I could engrave the top slide on his Glock service pistol..  I told him I didn't think it would but we tried it anyway..  It turned out amazing.. ﻿

![images/72202e7442510e5a5ebe4370877014ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/72202e7442510e5a5ebe4370877014ee.jpeg)



**"Chris Boggs"**

---
---
**Ariel Yahni (UniKpty)** *September 16, 2016 00:16*

Cermark, moly or direct to part? 


---
**Chris Boggs** *September 16, 2016 00:17*

Direct to part..  200mm/s 90% power.. 50w blue Chinese laser.. 


---
*Imported from [Google+](https://plus.google.com/101316203748949165087/posts/gFv5FphP4jz) &mdash; content and formatting may not be reliable*
