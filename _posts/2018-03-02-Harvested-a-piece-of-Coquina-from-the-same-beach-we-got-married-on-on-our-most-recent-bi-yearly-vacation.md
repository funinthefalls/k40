---
layout: post
title: "Harvested a piece of \"Coquina\" from the same beach we got married on, on our most recent bi-yearly vacation"
date: March 02, 2018 00:07
category: "Object produced with laser"
author: "Madyn3D CNC, LLC"
---
Harvested a piece of "Coquina" from the same beach we got married on, on our most recent bi-yearly vacation. Coquina is a stone unique to Florida and can be found exclusively on South Florida beaches, sometimes in abundance after storms and high surfs. It is a limestone-like rock, formed from years of compressed sea shells and other organic ocean elements. 



I used my K40 laser to cut the cast acrylic letters for "FLORIDA" which were mounted on the coquina rock with concrete epoxy resin. The acrylic is a beautiful 1/8" teal cast sheet from Delvie's Plastics. They have some really exotic colors and materials for lasing. 



If you want to see the details of the build process for this project, including a short video of the laser cutter in action, feel free to check it out here- 



[https://www.facebook.com/Madyn3Dcnc/posts/1004655069683693?pnref=story](https://www.facebook.com/Madyn3Dcnc/posts/1004655069683693?pnref=story)



![images/4274b7e91d24c87b9ef13d49c966a643.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4274b7e91d24c87b9ef13d49c966a643.jpeg)
![images/e1b75eb5b0ebce552b6bb19df099babe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1b75eb5b0ebce552b6bb19df099babe.jpeg)
![images/b68bc669e312c689b41ca796751e1fd8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b68bc669e312c689b41ca796751e1fd8.jpeg)
![images/aafdab86d6d29709203977d0f5a82f82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aafdab86d6d29709203977d0f5a82f82.jpeg)

**"Madyn3D CNC, LLC"**

---
---
**Fabiano Ramos** *March 02, 2018 14:33*

Congratz Dude, it's fantastic. 


---
**Madyn3D CNC, LLC** *March 02, 2018 16:08*

**+Fabiano Cardoso Ramos** Thank you! 


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/W5AfsGAAEF5) &mdash; content and formatting may not be reliable*
