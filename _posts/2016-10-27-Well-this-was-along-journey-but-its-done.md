---
layout: post
title: "Well this was along journey but it's done"
date: October 27, 2016 03:35
category: "Object produced with laser"
author: "Jeff Johnson"
---
Well this was along journey but it's done. I couldn't find a good original USS Enterprise model so I made one.I've still got to tidy up the files then I'll share on here and thingiverse. 

![images/05cf2af0cb1d53a3ff5bf7e4d04a1603.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05cf2af0cb1d53a3ff5bf7e4d04a1603.jpeg)



**"Jeff Johnson"**

---
---
**Alex Krause** *October 27, 2016 03:54*

Did you use 123dmake?


---
**Jeff Johnson** *October 27, 2016 04:01*

**+Alex Krause** No, I use Corel Draw.


---
**Phillip Conroy** *October 27, 2016 05:44*

Nice ,i for one will be making one


---
**Kris Sturgess** *October 27, 2016 06:07*

Nice work!.... Following


---
**Gunnar Stefansson** *October 27, 2016 08:10*

**+Phillip Conroy** Me 2 :D best looking Laser cut Enterprise I've seen sofar! Nice work there **+Jeff Johnson**




---
**Stephane Buisson** *October 27, 2016 12:07*

good start for a desk lamp ;-))


---
**Harvey Benner** *October 27, 2016 16:03*

Very nice!


---
**Kris Sturgess** *October 27, 2016 21:06*

Inlay some EL wire and add some LEDS this would rock! 


---
**Jeff Johnson** *October 27, 2016 21:20*

Thanks guys! Maybe another weak to clean up the files and tweak a few more things and this will be ready to share.


---
**Jeff Johnson** *November 02, 2016 16:27*

For those waiting, I'm almost done tweaking the model. 


---
**Jeff Johnson** *November 18, 2016 23:13*

Here are the files and crude instructions. [http://www.thingiverse.com/thing:1902315](http://www.thingiverse.com/thing:1902315)

[thingiverse.com - USS Enterprise Laser Cut by JoeSnuffie](http://www.thingiverse.com/thing:1902315)


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/NAuWtKRVm4j) &mdash; content and formatting may not be reliable*
