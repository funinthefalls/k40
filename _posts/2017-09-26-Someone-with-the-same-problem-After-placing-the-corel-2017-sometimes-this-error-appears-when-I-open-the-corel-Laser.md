---
layout: post
title: "Someone with the same problem? After placing the corel 2017, sometimes this error appears when I open the corel Laser"
date: September 26, 2017 10:37
category: "Software"
author: "drwhy leiria"
---
Someone with the same problem?

After placing the corel 2017, sometimes this error appears when I open the corel Laser. then closes and opens normally. Now do not leave here. Is there a solution to open the corel and then the pulgin?



![images/84b38d4066436b3fa9fa7dcc0eaab29c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84b38d4066436b3fa9fa7dcc0eaab29c.jpeg)



**"drwhy leiria"**

---
---
**HalfNormal** *September 26, 2017 12:52*

If you installed CorelDraw after installing CorelLaser, try reinstalling CorelLaser. The error looks like it is looking for CorelDraw Ver 11.


---
**BEN 3D** *September 26, 2017 15:42*

Yeah, good Idea from halfnormal, if that do not help you could use dbugview from Microsoft Sysinternals Suite to may get some more detailed informations. But it is a mystery that this Message does not occurs in any way. So it could also a good Idea to keep this in mind while starting the app, may be you already find an work arround without new a installation.


---
*Imported from [Google+](https://plus.google.com/105409245888003963852/posts/Fdj81tMSrpf) &mdash; content and formatting may not be reliable*
