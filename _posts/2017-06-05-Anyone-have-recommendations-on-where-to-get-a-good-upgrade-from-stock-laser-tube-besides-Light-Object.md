---
layout: post
title: "Anyone have recommendations on where to get a good/upgrade from stock laser tube besides Light Object?"
date: June 05, 2017 18:40
category: "Material suppliers"
author: "Nathan Thomas"
---
Anyone have recommendations on where to get a good/upgrade from stock laser tube besides Light Object? If eBay, which seller?



I appreciate any answers...





**"Nathan Thomas"**

---
---
**Joe Alexander** *June 05, 2017 19:57*

got mine off of ebay from this seller:



[ebay.com - Details about  40W Laser Tube For CO2 Laser Engraving Cutting Machine Engraver 700mm x50mm BEST](http://www.ebay.com/itm/132151582195?_trksid=p2060353.m2749.l2649&ssPageName=STRK:MEBIDX:IT)



Arrived well packaged and intact but most sellers do.


---
**Nathan Thomas** *June 05, 2017 20:03*

**+Joe Alexander** Yeah I see that one....how is it running so far? About the same or better than what you replaced?


---
**Joe Alexander** *June 05, 2017 22:32*

i bought it as a backup but it tested fine. still waiting on the one i have to die/me do decide if im gifting it to my friend :)


---
**Nathan Thomas** *June 05, 2017 22:42*

Gotcha, yeah that's a good idea


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/atgmpFMBzuB) &mdash; content and formatting may not be reliable*
