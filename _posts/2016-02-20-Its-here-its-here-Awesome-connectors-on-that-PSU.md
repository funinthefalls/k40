---
layout: post
title: "It's here it's here! Awesome connectors on that PSU"
date: February 20, 2016 00:32
category: "Discussion"
author: "ThantiK"
---
It's here it's here!  Awesome connectors on that PSU.  Haven't had a chance to test it out yet - it fit perfectly in my del sol trunk, I didn't think I was going to get it home until tomorrow.  Though, tomorrow is going to be fraught with line-running, and bucket filling, and I think I saw something about this software not running on Windows 10 (certainly not linux!) so I gotta work that out, etc.



![images/d3353b03b8209f57bb70fd09e24ecaf4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d3353b03b8209f57bb70fd09e24ecaf4.jpeg)
![images/d90240e27c2b7086bbc249092aec7811.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d90240e27c2b7086bbc249092aec7811.jpeg)

**"ThantiK"**

---
---
**I Laser** *February 20, 2016 00:48*

Yuusuf has corellaser running on Win10, so you should be okay.


---
**Ariel Yahni (UniKpty)** *February 20, 2016 00:49*

What's the shipping weight on that? I'm thinking on bringing one from the US next week as luggage 


---
**Jim Hatch** *February 20, 2016 01:00*

I run LaserDRW on my Windows 10 machine. I don't run the Corel that shipped with mine because it has a virus in the install package.


---
**ThantiK** *February 20, 2016 01:04*

**+Ariel Yahni**, surprisingly light!  I don't think it is luggage-able though, our airline loaders are <i>notoriously</i> rough on luggage.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 20, 2016 01:05*

Yeah, I had no dramas installing on Win10 Pro & running CorelDraw/CorelLaser. Not too sure if there is a virus in the install package, but I use Win10 for nothing other than LaserCutting so it doesn't really bother me.


---
**Gee Willikers** *February 20, 2016 04:18*

Some of these custom laser softwares trip antivirus software. I'm not sure exactly why but I'm betting it has to do with the way they access the i/o ports.﻿


---
**Scott Thorne** *February 20, 2016 13:35*

No problems here with Windows 10...when I had the k40 I have no issues with it.


---
**Jim Hatch** *February 20, 2016 14:04*

I have BitDefender as my AV software. It gets tripped on one of the DLLs in the install for Corel. Since that's pirated software in the first place I wouldn't be surprised if it were a real virus either by the folks who hacked Corel to create the install or by whoever created the software tool that was used to do the same. Keygen software used to create fake license keys is known for containing malware used to enable remote access to a PC and any others running on the same network. Unless the machine I was using wasn't attached to the Internet or my home wireless network and I never used it for anything but driving the laser and designing projects I'm not risking a data breach just so I can go direct from Corel to the laser.



I just have the extra step now of going from AI to LaserDRW to the laser and don't have to worry much about getting hacked.


---
**ThantiK** *February 20, 2016 21:40*

Okie dokie; works on Windows 10 -- I just managed to test out the top left 20mm * 20mm of my laser.  It works great :)



I loaded up my water with [http://www.sidewindercomputers.com/peptpcobi1.html](http://www.sidewindercomputers.com/peptpcobi1.html) -- biocide that I use for my PC water cooling systems.  This should keep the water clear and free from needed changing.


---
**Jim Hatch** *February 21, 2016 00:34*

**+ThantiK**​ if it freezes where you are some antifreeze is warranted if you keep the laser in the garage like me :-) Seems to keep things from growing in the water too.


---
**ThantiK** *February 21, 2016 00:43*

**+Jim Hatch**, it's gonna go in the laundry room - but I think I'm going to be adding some aux heat to that room to keep it at 15C minimum.


---
**Gee Willikers** *February 21, 2016 00:46*

Antifreeze will help prevent thing growing in the lines. RV antifreeze is recommended if you have young kids or pets. Some people use granular pool chlorine. Never use bleach - it will etch the glass of the tube. 


---
**Ariel Yahni (UniKpty)** *February 21, 2016 02:32*

**+ThantiK**​ have you tested it yet? I'm battling with myself on either getting a K40 or buying the parts to make something bigger


---
**ThantiK** *February 21, 2016 03:00*

Tested it.  It came with mirrors aligned, already has optical endstops.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2016 13:50*

**+Jim Hatch** I wasn't aware that LaserDrw allowed importing from AI. I use AI > CorelDraw/CorelLaser > Laser.



For anyone using it, is the LaserDrw software better than the CorelDraw/CorelLaser plugin?


---
**ThantiK** *February 21, 2016 16:33*

Something that I keep running into is the laser plugin just disappearing on CorelLaser -- I often have to restart the damn thing.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2016 23:41*

**+ThantiK** I have the same issue with it disappearing from the top right corner, however if you check your System Tray, there will be an icon down there for the plugin that you can run all the commands from by right clicking it.


---
**Tony Sobczak** *February 22, 2016 22:31*

Same issue disappearing on win 10 64 with corel draw x7 64bit. Not all the time though. It 


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/9hPy4QV5Jh2) &mdash; content and formatting may not be reliable*
