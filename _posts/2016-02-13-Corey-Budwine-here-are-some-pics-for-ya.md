---
layout: post
title: "Corey Budwine here are some pics for ya"
date: February 13, 2016 02:07
category: "Modification"
author: "ChiRag Chaudhari"
---
**+Corey Budwine**​ here are some pics for ya.



First remove the black plate, then two nuts hiding under that. And the other two are on opposite side. If course you will need to remove the stock bed as well.



![images/5a63ec08898c23b6693fc83fc14cf633.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a63ec08898c23b6693fc83fc14cf633.jpeg)
![images/0a0e4c2a2e535dc578e6ada7c24661eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a0e4c2a2e535dc578e6ada7c24661eb.jpeg)
![images/f7b431c345ea8b347048aca66716f0c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7b431c345ea8b347048aca66716f0c0.jpeg)

**"ChiRag Chaudhari"**

---
---
**Stephane Buisson** *February 13, 2016 10:28*

All the K40 are not build the same. at least 2 types of bed table. (that one based on extruded profile (like Chirag), or bend metal frame (like mine, see my mod), the belt tensioner could be locate on front or back of the machine, the motor are not at the same place too.

like here the bed table is screwed on the profile and easy replaced by honeycomb. the other one, more stiff, it's more difficult to change as you need to cut into. But my bed table is hold by 4 screws underneath, the air exhaust by 4 screws at the back, simple.


---
**3D Laser** *February 14, 2016 03:52*

I think I'm going to do this I have to take mine apart to square it all up anyway.  I'm not looking forward to rebuilding this thing


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/8RrpWm7QgHg) &mdash; content and formatting may not be reliable*
