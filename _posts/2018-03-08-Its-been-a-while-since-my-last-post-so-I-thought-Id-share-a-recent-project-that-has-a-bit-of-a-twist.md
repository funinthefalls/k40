---
layout: post
title: "It's been a while since my last post, so I thought I'd share a recent project that has a bit of a twist"
date: March 08, 2018 16:24
category: "Object produced with laser"
author: "Ulf Stahmer"
---
It's been a while since my last post, so I thought I'd share a recent project that has a bit of a twist. Recently, I volunteered to print the poster for the 40th anniversary of a local(ish) book arts fair, Wayzgoose held in Grimsby, Ontario. [http://www.grimsby.ca/Art-Gallery/Wayzgoose/](http://www.grimsby.ca/Art-Gallery/Wayzgoose/)



As my selection of lead type is limited to small font sizes, I thought that I would use my K40 to cut the text for the poster into lino blocks and print them on my press. All things considered, it went very smoothly. Initially, I wanted all the text to be white (like at the top and bottom of the poster), but I discovered that while peeling out the inside shapes (after a laser cut of the outline) it was difficult to not to remove the floating shapes in the letters (such as the center of the o). I didn't really want to raster-engrave out the letters, because if the engraving wasn't deep enough, any remaining linoleum would cause artifacts in the print. So I decided to try a test print with just the laser cut outline on the smaller fonts, and I liked it! As can be seen in the photos, where required, I removed some excess linoleum by hand.



Overall, I'm very happy with the way the K40 cuts lino. I made sure that the small text was cut with an unfocused laser beam to give the outline some thickness. I will definitely use my K40 on these projects in the future!



![images/0cdccde6c70e9a21921b81504b8b8ecd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0cdccde6c70e9a21921b81504b8b8ecd.jpeg)
![images/e86b83433b2c60db3107d68bc20fbfd2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e86b83433b2c60db3107d68bc20fbfd2.jpeg)
![images/06bbe2fcd4b84c999ddf300d188e7493.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06bbe2fcd4b84c999ddf300d188e7493.jpeg)
![images/998e9cfa810e6f731140f6e2106b084f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/998e9cfa810e6f731140f6e2106b084f.jpeg)

**"Ulf Stahmer"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2018 16:39*

Nice work. Considered this in the past as an idea for art prints. I like the results. Any hint on the specifics of the linoleum? Curious what sort to purchase if I ever get around to doing some art prints...


---
**Ulf Stahmer** *March 08, 2018 17:35*

**+Yuusuf Sallahuddin** I had read somewhere, perhaps even on this forum, that natural linoleum was safe to cut. I purchased the linoleum at the local arts supply store. I believe that it's made in Germany. I have seen it in two colours: brown and grey. Both have the burlap backing.



I used 40% power and a speed of 600 mm/min in Laserweb. As mentioned, I purposely used an unfocused beam for a wider cut. Just cutting the outline results in a faster cut than engraving.



The biggest challenge I had was with the alignment of overlapping colours. Make sure that you datum the blocks the same. I print the key block onto a sheet of plastic (like an old overhead film), switch out the blocks and then print the from the plastic onto the second block. Hope this makes sense.



I have an old Vandercook press [https://letterpresscommons.com/press/vandercook-sp-15/](https://letterpresscommons.com/press/vandercook-sp-15/). Using simple techniques like the one I describe simplifies colour registration.


---
**Fook INGSOC** *March 09, 2018 03:30*

Damn Cool!...Way to GO!!!


---
**Ned Hill** *March 09, 2018 18:53*

Very nice work!




---
**Ulf Stahmer** *March 09, 2018 19:52*

**+Ned Hill** Thanks, Ned! Especially from you! Your work is always stunning!


---
**James Lilly** *March 11, 2018 22:54*

Extremely cool.  I've always wondered about doing so lino or wood block prints.  I cut and printed a number of laser generated silkscreen prints.




---
**Ulf Stahmer** *March 12, 2018 01:35*

**+James Lilly** Thanks, James. Making blocks to print was one of my justifications for getting the laser cutter in the first place. Once it came in however, I discovered so many other uses that I almost forgot about block making. I've only made blocks twice now. My other block (see my post on December 4th, 2016) was raster-engraved in MDF. It took a long time to cut (as rasters do). I think I like the lino method better.



Off topic, but have you ever tried making silkscreens using photo-emulsion? In my past, I would make screens by photocopying an image onto an overhead film, placing it onto a screen prepared with photo-emulsion and exposing it using sunlight. Worked like a charm! Haven't made a screen in 20 years now. I've been meaning to teach this technique to my youngest son sometime soon.


---
**James Lilly** *March 12, 2018 01:53*

I still do a little photo silkscreening.  I did a lot of it in the past. They now make laser (and inkjet) films that work a little better than the copy film. 



I used to teach high school graphics and had a nice laser in the lab. We cut a lot of vector stencils for short t-shirt runs.


---
**Joe Gerten** *March 20, 2018 03:19*

Killer press! I've yet to mess with any kind of screen printing- but it's on my list. Thanks for the linoleum tip- I tried doing some raster engraving on plywood for stamps, but the results were less than stunning.  


---
**Ulf Stahmer** *March 20, 2018 20:33*

**+Joe Gerten** Thanks Joe. To make a wood block using raster engraving you might consider "screening" your image first. Printing technology has come a long way in the past few decades, so screening is hardly done anymore to the same degree it once was. 30 years ago, photos could't be photocopied with any success without being screened first. It's the process of simulating grey-scale by using dots that vary in size. With relief printing you want the top (printing surface) of your plate to be all at the same level. A normal raster engrave can actually shape your surface into 3D. This is not good to print. You can use Gimp to screen your image and then raster engrave the screened (halftoned) image, leaving the raised "dots" to print.  See: [http://www.screenprinting-aspa.com/the-artists-corner/how-to-print-halftones-on-the-cheap](http://www.screenprinting-aspa.com/the-artists-corner/how-to-print-halftones-on-the-cheap)

Should you decide to try this, don't forget that your plate should be a "negative" i.e. if you want to print the portrait of the girl in the linked example, you should be raster engraving the "white" away to leave the "black" image for printing. Hope this makes some sense.


---
**Joe Gerten** *March 22, 2018 22:05*

**+Ulf Stahmer**  Good info! I've been using inkscape exclusively- I'll take a poke at GIMP.  


---
**Ulf Stahmer** *April 09, 2018 14:49*

**+Mason G** Hi Mason, I'm sure that it can be done that way, but I think that photo emulsion would be much easier.



I used to cut screens by hand, but photo emulsion is SO MUCH easier and faster. Now we can just photocopy and image onto an overhead and use that as a positive. If you want to print a photo, make sure to halftone your image first (see my explanation above). I have printed many photo images on t-shirts using this technique.



Here's the stuff I've used (see below), but there are lower cost versions out there. Apply the emulsion to both sides of the screen. Place your positive image on the screen and weigh it down with a thick sheet of glass (else you may loose some detail). For best results, the emulsion of the positive should contact the emulsion side of the screen. If this is not done, the thickness of your overhead will affect the quality of your screen. Amazing, but true!



Expose your screen (this can be done using natural sunlight - approx. 45 sec. to 2+ minutes depending on where you live and time of year).



Wash out your screen on the exposed side only. You will get undesired washout if you wash both sides of the screen. After washout of unexposed emulsion is complete, expose the opposite side to set the emulsion on that side.



 Once the sensitizer has been added to the emulsion, unused emulsion can be stored in the fridge for a while.

I tend to mix small batches (maybe 1/4 of the kit) at a time to make it last longer. 



I haven't done this in 20 years. All this talk about it makes me want to make some screens again and start printing!



[amazon.com - Amazon.com: Speedball Art Products 4559 Diazo Photo Emulsion Kit](https://www.amazon.com/Speedball-Products-4559-Diazo-Emulsion/dp/B0007ZHGWI/ref=sr_1_1?ie=UTF8&qid=1523284683&sr=8-1&keywords=photo+emulsion+and+sensitizer) 


---
**Fook INGSOC** *April 11, 2018 01:48*

**+Ulf Stahmer** reminds me of my pinhole coffee can camera!!! I used photographic paper for the negative inside the curved can. I exposed for a extended length of time--longer than photographic film of course! Then I developed it in the darkroom. Once developed and dried, it became the negative which I placed on top of another piece of photographic paper, face down, emulsion to emulsion. Expose to enlarger light shining through the upper paper to the lower paper. Developed the lower paper which turned into the positive. Worked better than I expected!!!


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/dcpKPPQu6GB) &mdash; content and formatting may not be reliable*
