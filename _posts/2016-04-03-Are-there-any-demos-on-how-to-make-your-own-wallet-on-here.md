---
layout: post
title: "Are there any demos on how to make your own wallet on here"
date: April 03, 2016 07:51
category: "Discussion"
author: "Tony Schelts"
---
Are there any demos on how to make your own wallet on here. 





**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 08:41*

Probably not, but I can share a cut file for a wallet I designed if you would like. It could probably do with some minor tweaks though (e.g. rounded corners in the top or a double layer of leather for the exterior to give it extra stiffness or even lining the notes slot with something like suede). I'll find the file & link it for you.


---
**Tony Schelts** *April 03, 2016 17:38*

Thanks not sure when i will do it but having the files would be good


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 19:39*

**+Tony Schelts** Do you have a preference which wallet style you would prefer? I tagged you in a post on my profile to show you the results of both (with coin pocket & without). I will upload the CDR files later once I adjust them for you.


---
**Tony Schelts** *April 03, 2016 22:56*

Thank you Yuusuf


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/V2PdExp69jp) &mdash; content and formatting may not be reliable*
