---
layout: post
title: "Just got my laser but it won't initialize with corel laser or laser cut"
date: July 26, 2015 06:46
category: "Hardware and Laser settings"
author: "Adam C"
---
Just got my laser but it won't initialize with corel laser or laser cut. I have checked the board model and the serial number and the usb key is plugged into the computer. When I turn the laser on it travels to the home position in the y direction but not the x direction. Has anyone had these problems?﻿  Do I need to install anything other than corel, corel laser and laser cut? 





**"Adam C"**

---
---
**Stuart Middleton** *July 26, 2015 10:47*

The board should zero in both directions when you switch it on. Check your connections to your motors and make sure the X axis is free to move.


---
**Adam C** *July 26, 2015 17:15*

Thanks Stuart.  The connection of the ribbon type cable was not inserted fully for the x-axis motor.  This corrected the initial zero problem.  I was able to fix the connection to the computer as well.  I'm set to finally start cutting!


---
*Imported from [Google+](https://plus.google.com/102798323960432061359/posts/EDD93zKjjTm) &mdash; content and formatting may not be reliable*
