---
layout: post
title: "While I was fixing my interlock problem [ ] I realized that the middleman board was better located in the Smoothie cabinet rather on the interlock breakout"
date: December 12, 2016 14:27
category: "Modification"
author: "Don Kleinschnitz Jr."
---
While I was fixing my interlock problem [[https://plus.google.com/113684285877323403487/posts/TVvphjoPTSz](https://plus.google.com/113684285877323403487/posts/TVvphjoPTSz)] I realized that the middleman board was better located in the Smoothie cabinet rather on the interlock breakout. It cleaned up the wiring a lot and made the Middleman more accessible for troubleshooting.

The Middleman is screwed to the left wall of the cabinet.





![images/d8c0fb9af9990cd87a2ca41f69ce39da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8c0fb9af9990cd87a2ca41f69ce39da.jpeg)
![images/89d9300f968b70d8a61ba888f9eeba7b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89d9300f968b70d8a61ba888f9eeba7b.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Madyn3D CNC, LLC** *December 12, 2016 22:01*

Love those momentary arcade-style switches.. my acrylic control panel has been on the to-do list for a while now, even bought some solid black acrylic just for that purpose. 



You've got a lot going on in there, I like it. In fact, I just made some room in mine. Plan on mounting the air pump right inside along with power distribution block to centralize air/water/lights on it's own outlet, then run all switches and controls to the new control board.



But right now I'm going to go stalk your post history and write-up's, I'm curious to see everything you've got going on in that machine.  

![images/e4cc3cb49de29d90dc007ede3bf9579c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e4cc3cb49de29d90dc007ede3bf9579c.jpeg)


---
**Kelly S** *December 12, 2016 22:05*

I need to make a box like this for when I extend the bed over in the electronics side.  :D Looks good, and like them buttons!


---
**Don Kleinschnitz Jr.** *December 12, 2016 22:12*

**+Kelly S** I wanted to make a reusable cabinet design for smoothie CNC control that was not just K40 as I plan to get a CNC mill this summer and just duplicate it :).


---
**Kelly S** *December 12, 2016 22:43*

Very cool, I like the idea as next year sometime I plan to just  build my own large laser cutter something like this would be handy.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/FHUKxmjhY7m) &mdash; content and formatting may not be reliable*
