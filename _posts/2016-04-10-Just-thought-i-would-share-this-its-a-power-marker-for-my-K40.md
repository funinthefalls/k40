---
layout: post
title: "Just thought i would share this, it's a power marker for my K40"
date: April 10, 2016 05:36
category: "Hardware and Laser settings"
author: "ben crawford"
---
Just thought i would share this, it's a power marker for my K40. It did not come with any kind of markers on the control panel. I am not sure if the potentiometer positions are standard between units but this scale works for me. 

![images/84c3b16c418f81c37a011c1a5eeac492.png](https://gitlab.com/funinthefalls/k40/raw/master/images/84c3b16c418f81c37a011c1a5eeac492.png)



**"ben crawford"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 09:14*

I actually designed something similar when I first got my machine for the same reason, no markings.



However, from what I learned from other members when I posted it here, the values will actually change over time (as I had mA displayed) due to the tube weakening. Yours seems to be an improvement on that by showing a %. Unfortunately, what today is 50% may be 10mA but in 6 months time it may be 8mA. Another bonus from your gauge is that used in combination with the ammeter you will be able to determine power drop of your laser tube over time (which may assist with indicating when it is time to order a new tube).


---
**Jim Hatch** *April 10, 2016 13:33*

**+Yuusuf Sallahuddin**​ Although the power declines over time, with some sort of marker you can always reposition it to keep reading the proper % for the tube's condition. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 14:17*

**+Jim Hatch** Yeah, the % would remain in the same position at all times. You could reposition it so that the 50% (for e.g.) still points to whatever 10mA is. It's definitely better than no indicator. I've just drawn lines on my machine with pencil & erase them when I feel the need for adjustments. This would be a much more effective solution.


---
**HalfNormal** *April 10, 2016 15:24*

Just rotate the scale as needed over time. So when 50% is no longer 10 ma you rotate the scale to match 10 ma again.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 15:34*

**+HalfNormal** It would be good to add an extra component as a non-rotating original reference point. That way you can see change over time & know in advance that "it's definitely time to order a new tube".


---
**Glyn Jones** *June 29, 2016 08:31*

going to see if it will cut on magnetic photo paper! here is hopeing


---
**ben crawford** *June 29, 2016 17:16*

let me know how it works for you.


---
**Glyn Jones** *June 29, 2016 20:39*

will do thanks for the share


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/BUxxZwt1Jgw) &mdash; content and formatting may not be reliable*
