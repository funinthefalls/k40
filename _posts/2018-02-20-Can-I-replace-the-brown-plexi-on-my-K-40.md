---
layout: post
title: "Can I replace the brown plexi on my K 40?"
date: February 20, 2018 05:03
category: "Discussion"
author: "bbowley2"
---
Can I replace the brown plexi on my K 40?  Full spectrum and Epilog both use CO2 lasers and they have clear covers.

Thanks





**"bbowley2"**

---
---
**Phillip Conroy** *February 20, 2018 06:06*

Yes, just keep checking no holes are forming in clear pannel


---
**bbowley2** *February 20, 2018 14:37*

Thanks




---
**Ned Hill** *February 20, 2018 14:39*

The amber plexi just provides some tinting to cut down on the glare from the beam burn.  But I don't see any real reason you couldn't replace it with clear if you really want to.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/NTEEHmAfSa4) &mdash; content and formatting may not be reliable*
