---
layout: post
title: "Question.. I could use some help please"
date: September 17, 2016 00:37
category: "Discussion"
author: "rick jarvis"
---
Question.. I could use some help please. I want to order a k40 is there any difference in the analog and the digital besides the led's? Also how do I know which control board is in it. Are the ones that come with Corel/laserdraw a little better? Thanks for you help





**"rick jarvis"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 01:22*

From my understanding, the analogue will give you more accurate/timely response on the values it provides. To find out which control board you would have to ask the seller I guess. Corel/LaserDrw ones tend to run on the m2nano board. I have a feeling (although not from experience) that the software workflow for that is better than the Moshi boards/Moshidraw software.


---
**rick jarvis** *September 17, 2016 01:26*

Thank you


---
**Paul de Groot** *September 17, 2016 04:12*

I do have a m2nano board but the experience is not great. At the end everyone swaps them out for an open source controller that gives you all the freedom to use whatever softeare you need.


---
**Ian C** *September 17, 2016 08:59*

I have the LED digit power version with M2 laserdraw board and I think it's great for what it is. Yes you can do more with a controller upgrade, raster engraving for example. However it's only a small cutter the K40, so you have to weigh up if the money you're going to spend on it is worthwhile or not. In my case I'm using it as a lead in to a bigger X700 Clone or cnc router, so for me it's pointless upgrading as the cut area isn't big enough for me to warrant it


---
**rick jarvis** *September 17, 2016 17:50*

Appreciate the help guys I did just order the analog version with the m2nano.

I'm prepared already to upgrade the controller board. Any suggestions to a good one?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 23:32*

**+rick jarvis** That really depends what you are wanting to do with the laser. If you're just wanting to cut, then the stock controller is great for that. If you're wanting to engrave photos, then look into LaserWeb/CNC Web community ([https://plus.google.com/communities/115879488566665599508](https://plus.google.com/communities/115879488566665599508)) & have a read through there. People are using a variety of controllers, but mostly Smoothieboard is recommended (not the MKS/makerbase version though).


---
**rick jarvis** *September 18, 2016 05:10*

Wanting to do photos, I'll check that link out. Thank you!


---
*Imported from [Google+](https://plus.google.com/105230811165966980201/posts/gHvGQ2tAbyP) &mdash; content and formatting may not be reliable*
