---
layout: post
title: "LAYZOR update: Acrylic!"
date: April 18, 2018 21:32
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
LAYZOR update: Acrylic!



[http://www.manmademayhem.com/?p=3295](http://www.manmademayhem.com/?p=3295)





**"Frederik Deryckere"**

---
---
**James Rivera** *April 19, 2018 04:18*

Hmmm...a couple of questions popped into my mind when I saw this:

1) does it already block CO2 wavelength light, and if not, do you plan to add some kind of CO2 laser light filter?

2) What will happen to it if things get a little...flamey?


---
**Jorge Robles** *April 19, 2018 07:08*

What a beauty!


---
**Frederik Deryckere** *April 19, 2018 11:51*

**+James Rivera** hey thanks for your input. As far as I'm aware, any acrylic will block the 10.6µm wavelength just fine. As long as it's not hit by a focused beam obviously.

Safety hazards are always at the user's discretion in diy builds like this. I will have an emergency stop, a fire extinguisher next to the machine, and air assist is going to be added which should prevent it altogether.




---
**Frederik Deryckere** *April 19, 2018 11:52*

**+Jorge Robles** thanks! I agree :-)


---
**Bill Keeter** *April 19, 2018 16:04*

I did similar with my 60w laser. I used a sheet of tinted Polycarbonate. As I think it takes longer to burn through.. and I thought I read somewhere it's already used for windows on laser cutters.


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/fGmy7AmHXNE) &mdash; content and formatting may not be reliable*
