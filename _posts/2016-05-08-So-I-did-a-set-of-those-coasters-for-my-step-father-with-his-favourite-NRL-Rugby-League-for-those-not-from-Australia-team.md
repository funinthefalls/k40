---
layout: post
title: "So I did a set of those coasters for my step-father with his favourite NRL (Rugby League for those not from Australia) team"
date: May 08, 2016 21:54
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I did a set of those coasters for my step-father with his favourite NRL (Rugby League for those not from Australia) team. The Canterbury-Bankstown Bulldogs ([https://en.wikipedia.org/wiki/Canterbury-Bankstown_Bulldogs](https://en.wikipedia.org/wiki/Canterbury-Bankstown_Bulldogs)).



Came up pretty good on 3 of the 6. There are 3 layers in these. 1 @ 4mA (the lighter shaded stuff around nose/chest & the shield border), 1 @ 8mA (the V shape in the background) & 1 @ 10mA (all the top BULLDOGS & bottom 1935 CB stuff). All 1 pass @ 20mm/s.



Observations:

- The 8mA is barely noticeable as different from the 10mA. Not even worth doing as a separate layer/power.

- I tested 6mA instead of 8mA for a couple of them (not sure which) & again, barely noticeable as different from the 10mA.

- I'm thinking to get different shading levels, I might have to vary the rectangular vector masking width. e.g. for really dark, have 0.5mm width with 0.25mm spacing; for medium, have 0.5mm width with 0.5mm spacing; for light, have 0.5mm width with 0.75mm spacing. I will give that a test later sometime to see how it comes out.



If you look close at the two individual images you can see that the nose region which is light on one is really dark on the other. I imagine that is an issue with focal depth or mirror alignment, as nothing else changed. I noticed that the base of my k40 casing is not flat/level & as such, my mount that I created to raise the piece to the right height was probably not equal distance from the focal lens (although maybe 1mm higher at X=300 position than from X=0 position). It's amazing how such a small focal difference causes some significant difference in the results.



![images/4e0d5e8e1963ae79900421ee69d0b648.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e0d5e8e1963ae79900421ee69d0b648.jpeg)
![images/7a10787b7fd1b74df8921cb4a1a6ff3c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a10787b7fd1b74df8921cb4a1a6ff3c.jpeg)
![images/687cdc640642285696dec55a754ca3f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/687cdc640642285696dec55a754ca3f5.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SFhjQWp8tC1) &mdash; content and formatting may not be reliable*
