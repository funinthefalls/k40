---
layout: post
title: "I have been researching the K40 for a few weeks and I think I am getting closer to buying one"
date: July 11, 2016 08:24
category: "Discussion"
author: "fydaman"
---
I have been researching the K40 for a few weeks and I think I am getting closer to buying one. While doing my research I have noticed different variants of the k40 and really just want the best deal and best value. There is the standard one for about $320 on eBay that claim to be the KD-III which has newer features. I've also seen ones with common upgrades already done for about $500. There has been a few members on here that got some deals dealing with people directly and was wondering if anyone can pass me some contacts or links for the best deal. I don't mind spending a couple extra bucks if it's worth it. I look forward to being apart of this community, I feel I have learned so much in the last few weeks and just want to get started already. 





**"fydaman"**

---
---
**Stephane Buisson** *July 11, 2016 08:41*

All depend really where you are, Ebay with local delivery is for many the best option (no taxes surprise).

Now for the model, the original with the potentiometre is a good one for it's simplicity. (I see analog as a +)

As per upgrade/mod do you have pleasure by doing it yourself? (make the most of it feeling).

Mods are not expensive, just time consuming.


---
**Corey Renner** *July 12, 2016 02:10*

I got the new one with digital controls and a couple of other improvements for $378 delivered a couple of weeks ago (ebay from Oakland).  However, most people quote power in ma rather than %, so I added the analog meter for $5 (ebay).  Best of all worlds.


---
**fydaman** *July 12, 2016 03:01*

I'm in the Bay Area so that would work great. You still have the seller jnfo? Or a link of your K40 so I can look at the specifications? **+Corey Renner**


---
**Corey Renner** *July 12, 2016 03:22*

This is the one I bought:  [http://www.ebay.com/itm/401128762209?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/401128762209?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**fydaman** *July 12, 2016 04:57*

It's no longer available and I can't seem to find that model when I search eBay. Any pointers? **+Corey Renner**


---
**Corey Renner** *July 12, 2016 14:41*

Email the seller, see when they're getting more.


---
*Imported from [Google+](https://plus.google.com/103512799338190792485/posts/XysScbh2PGU) &mdash; content and formatting may not be reliable*
