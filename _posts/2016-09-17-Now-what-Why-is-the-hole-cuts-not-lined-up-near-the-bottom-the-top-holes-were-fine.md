---
layout: post
title: "Now what. Why is the hole cuts not lined up near the bottom, the top holes were fine"
date: September 17, 2016 03:53
category: "Original software and hardware issues"
author: "David Spencer"
---
Now what. Why is the hole cuts not lined up near the bottom, the top holes were fine.





**"David Spencer"**

---
---
**David Spencer** *September 17, 2016 03:54*

![images/73312a8bfdffbf4e031ce478fc7a3940.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73312a8bfdffbf4e031ce478fc7a3940.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 04:33*

Is it possible the piece moved during cutting? Can't really say what caused it without seeing the original files. But even then, I imagine that's probably not the issue.


---
**David Spencer** *September 17, 2016 05:01*

nope didn't move, I noticed it on another file also. it seems to be something while engraving. I made some business  cards that the engraving was shifted also.


---
**David Spencer** *September 17, 2016 05:33*

how do I attach a file 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 06:07*

**+David Spencer** Best bet would be to place the file in your Google Drive, then put the link for the file since you can't directly attach files.



However, another thought is are you using a reference point for each of your layers? E.g. a 1 pixel dot in the top-left corner of each layer; or a borderless unfilled rectangle around all objects?


---
**Ariel Yahni (UniKpty)** *September 17, 2016 11:45*

That doesn't look moved to me. Look more like a displacement. Like elongated


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 13:29*

**+Ariel Yahni** If that's the case, could it be an issue with board model & device ID? 



**+David Spencer** Have you set your board model & device ID in CorelLaser or LaserDraw (whichever you are using)?


---
**David Spencer** *September 17, 2016 15:08*

yep set the board and device ID. here is the file  [drive.google.com - chess.cdr - Google Drive](https://drive.google.com/file/d/0B0ysFUNi4tAhckVwVGNwYjVrbFU/view?usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 23:28*

**+David Spencer** Judging by that, there is nothing wrong with the file. I will give it a test when I'm down the garage in a few hours. Only other thing I can think off the top of my head is to check these settings: [https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)


---
**David Spencer** *September 17, 2016 23:47*

ok I changed those settings, trying it again


---
**David Spencer** *September 18, 2016 00:51*

**+Yuusuf Sallahuddin** Holy crap that fixed it. Thank you very much, it has been driving me nuts for 3 days.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 18, 2016 01:21*

**+David Spencer** Sweet. Glad that helped :)


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/FV6NxriZ4MT) &mdash; content and formatting may not be reliable*
