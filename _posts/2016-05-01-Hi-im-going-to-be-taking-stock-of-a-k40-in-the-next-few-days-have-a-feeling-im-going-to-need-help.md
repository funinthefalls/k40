---
layout: post
title: "Hi im going to be taking stock of a k40 in the next few days, have a feeling im going to need help"
date: May 01, 2016 18:17
category: "Hardware and Laser settings"
author: "Ian Ferguson"
---
Hi im going to be taking stock of a k40 in the next few days, have a feeling im going to need help.





**"Ian Ferguson"**

---
---
**Jim Hatch** *May 01, 2016 18:55*

Where are you located?


---
**Ian Ferguson** *May 01, 2016 19:39*

Northern Ireland Jim


---
**Ian Ferguson** *May 01, 2016 19:41*

have my concerns about the whole earthing security. Will I need an earth spike?


---
**Ian Ferguson** *May 01, 2016 19:43*

saw a nasty video of machine arcing, looked very scary


---
**Jim Hatch** *May 01, 2016 19:45*

depends. If the K40 came with the grounding plug you may be okay. But check to see if the wire inside the machine is connected. If it is, take it off and make sure the post it's attached to is free of paint - it may look okay but the paint will keep it from making the ground connection. If it's painted, you can scrape it off, reattach the ground wire and should be good to go using the house main's grounding circuit. 



I did find a transient leak in mine and ended up attaching a ground wire to the grounding lug on the machine and a 10' copper rod in the earth.


---
**Ian Ferguson** *May 01, 2016 19:53*

**+Jim Hatch**

 Thanks Jim, thats good to know. Do i need to silicone the ends of the tube?


---
**Jim Hatch** *May 02, 2016 00:18*

I don't think so. My transient electric leak was likely some slight bleed over in the PSU between the low and high voltage circuits.


---
*Imported from [Google+](https://plus.google.com/100713211036434462043/posts/h41wztEVzij) &mdash; content and formatting may not be reliable*
