---
layout: post
title: "Can someone explain what these various cuts are and can they be done with a stock K40?"
date: April 19, 2016 20:34
category: "Discussion"
author: "Tony Sobczak"
---
Can someone explain what these various cuts are and can they be done with a stock K40?



Green - Pocket Cut 1.1mm

Pink   - Kerf Pocket Cut 3mm

Red is external cut - That I get.

Blue is Internal cut



Google can up with answers to everything but what I wanted.

 



![images/79a9c75e36281bbe10bb6c886ce0c903.png](https://gitlab.com/funinthefalls/k40/raw/master/images/79a9c75e36281bbe10bb6c886ce0c903.png)
![images/74fa3f461d7179f2a11fc34ce9003863.png](https://gitlab.com/funinthefalls/k40/raw/master/images/74fa3f461d7179f2a11fc34ce9003863.png)

**"Tony Sobczak"**

---
---
**Anthony Bolgar** *April 19, 2016 21:49*

The pink or kerf cut is a series of shallowly engraved lines that will allow you to bend the piece. Blue or internal cut I am not positive, but I think it is exactly the same as an external cut, but is always on the inside of the object, Ie, after running the blue internal cut there should be a disk cut out that will fit inside the blue line, so after doing the external and internal cuts, the object is actually just the items between the red and blu\e lines, the rest would be scrap. But don't take what I have said as gospel, I am just stating what I THINK they might mean, I am sure there are more experienced users around that can explain it all for you.


---
**Stephane Buisson** *April 20, 2016 06:43*

Antony could be spot on, not sure about the color scheme, but to each color a function (cut or engrave), more color to prioritize the jobs, as some bits could move or fall off.


---
**Tony Sobczak** *April 20, 2016 15:02*

So I'm guessing that this cant be done on a stock K40


---
**Stephane Buisson** *April 20, 2016 16:37*

**+Tony Sobczak** depend on your software, what board/soft do you have ?


---
**Tony Sobczak** *April 20, 2016 18:49*

Right now stock nano board. I've a ramps sitting here.﻿  Right now coreldraw.﻿  Trying to decide if I want to try it with ramps or wait for a smoothie. I know its better but a ways down the road.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/Ld7SKjgBcb8) &mdash; content and formatting may not be reliable*
