---
layout: post
title: "This is my PSU what is the best way to convert it to smoothie to connect the pwm to the psu?"
date: July 19, 2016 01:50
category: "Smoothieboard Modification"
author: "Alex Krause"
---
This is my PSU what is the best way to convert it to smoothie to connect the pwm to the psu? I need fairly decent control since my main use of the k40 is photo and image engraves

![images/432a1c1338c4706db8b61ae523d15c57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/432a1c1338c4706db8b61ae523d15c57.jpeg)



**"Alex Krause"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 01:52*

The pin that the red wire of the green-red-blue cable. That's what needs to go to pwm pin on smoothie. 


---
**Alex Krause** *July 19, 2016 01:53*

**+Ray Kholodovsky**​ what wire goes to ground on the mosfet?


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 02:14*

You should only need to hook up the pin I specified to the OUTPUT of your MOSFET. 

The MOSFET should share ground with the laser psu ground. Which, if you are powering the smoothie from the laser psu, should be fairly easy. 


---
**Alex Krause** *July 19, 2016 02:28*

**+Ray Kholodovsky**​ from looking at the smoothie diagram am I safe to assume that connecting to pin 2.4-2.5 would be the same as connecting to the mosfet output? 


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 02:30*

I forgot this wasn't my board, where I built level shifting in to the MOSFET circuit. 



Ok, you have a level shifter? Hook 2.4 up to it. 


---
**Alex Krause** *July 19, 2016 02:32*

And it's output to the center tap of the pot circut?


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 02:40*

To clarify that bit: remove the hot glue holding the connector in place, carefully remove the plug, and connect the output to the center pin of the connector on the psu.


---
**Alex Krause** *July 19, 2016 02:42*

**+Ray Kholodovsky**​ if I wanted to keep the Pot can I just remove the +5 from it for on the fly adjustments


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 02:50*

If you wanted to do that you could put a single pole dual throw switch (like a toggle or slide) to go between pwm control and potentiometer control.  But no, the signal (voltage) is relative to both the +5v and to gnd.


---
**Alex Krause** *July 19, 2016 02:52*

Do I need another output from smoothie to connect to the psu to initiate the fire of the laser


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 02:55*

Yes. But this one is easy. you just need to tap into the L(O) pin: this is the right most line of the 4 blue wire bundle on the right. Just hook it up directly to the other mosfet on the smoothieboard.



UPDATE Rightmost not left do not plug 24v into your mosfet output.


---
**Alex Krause** *July 19, 2016 02:57*

Is the screen printing on the laser psu backwards from what I read in my picture the left most wire is 24v


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 02:59*

Good catch.  I totally misread the picture. rightmost connector.


---
**Alex Krause** *July 19, 2016 03:17*

**+Ray Kholodovsky**​ with this setup is there any special modification needed for the laser module of smoothie ware? To initiate both the laser and pwm from different pins?


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 03:21*

There is the stock laser module with pin 2.4. And then you will want to do a switch.laserfire module for M3 gcode to turn the L MOSFET on and M5 to turn it off. 

**+Ariel Yahni** can assist with the exact text of the module, or Google. 


---
**Ariel Yahni (UniKpty)** *July 19, 2016 03:25*

So wich setup are you going for. The 1 or 2 pin? 


---
**Ariel Yahni (UniKpty)** *July 19, 2016 04:32*

**+Peter van der Walt** is there any possibility of damage if **+Alex Krause** connect the L to the negative screw on pin 2.4 on the smoothieboard he has?


---
**Darren Steele** *July 19, 2016 06:21*

So pleased this discussion is taking place.  I have exactly the same PSU as **+Alex Krause** and am at exactly the same stage [ wondering how to fire the laser ], yesterday I just grounded the L and nearly soiled myself when I switched the K40 on and the laser started firing.  Electronics not being my strong point!  I'm still a little confused about whether a 2 pin or 1 pin approach is best but ideally I want to control the laser power via s/w rather than using the potentiometer.


---
**Darren Steele** *July 19, 2016 06:23*

You could also read this thread [https://plus.google.com/u/0/+DarrenSteele/posts/39cTXCfxnSw](https://plus.google.com/u/0/+DarrenSteele/posts/39cTXCfxnSw) and see if that gives you any additional clues


---
**Ariel Yahni (UniKpty)** *July 19, 2016 11:51*

**+Darren Steele** What exactly did you conect?


---
**Darren Steele** *July 19, 2016 12:32*

So I've currently got 2.4 connected up to PWM but still nothing was working because L wasn't connected to anything.  As soon as I grounded L [ by quite literally connecting to GND ] then the laser was off and running.  So now I just need to work out how to selectively connect GND to L


---
**Ariel Yahni (UniKpty)** *July 19, 2016 12:35*

Im not sure what do you mean by 2.4 connected to pwm. Anyway PIN 2,4 is pwm and in your board should have a + and - connector. The - is gnd. Connect L there


---
**Darren Steele** *July 19, 2016 13:48*

Oh, cool, that simple huh?  I'll give it a go on Friday when I'm back home.  Thx


---
**Ray Kholodovsky (Cohesion3D)** *July 19, 2016 14:02*

Yep, that's how to "connect L to MOSFET output". 


---
**Jon Bruno** *July 19, 2016 14:27*

Ariel,, just adding to the question pool here..

So is what you're saying is that the IN pin of the LPSU should be connected to the + lug of the 2,4 ( FAN ) connector and the L wire should be tied to the 2,4 - terminal? if this is the case is the - terminal pulled high when the laser isn't firing?  Wouldn't the LPSU then technically always be in a firing state?﻿


---
**Ariel Yahni (UniKpty)** *July 19, 2016 14:33*

**+Jon Bruno**​​​ no. Just take the L from the PSU right most in the picture above and connect it to the PWM (2.4pin) on the - terminal.  Only 1 cable is used﻿.  Please note that this is how I run it on my azsmz smoothie compatible board and been using it like this for almost 2 month daily. 


---
**Darren Steele** *July 19, 2016 16:07*

So with this method the power of the laser is controlled with the potentiometer?


---
**Ariel Yahni (UniKpty)** *July 19, 2016 16:14*

**+Darren Steele**​ max power will be controlled by the Pot. So if you dial it to 10 mA and the fire the laser at 50% you will get roughly 5mA


---
**Dennis Fuente** *July 19, 2016 16:45*

Hi Guys i am currently working on a smoothie install and i have the same PSU as in the picture above what i fiund is the right side connector with the L is the firing wire I have a DAC on the PWM line that stands for digital to analog convertor i found i can control the power to the laser with a digital signal voltage and keep the laser off when not needed i am currently sorting out an on off control via a pin from the smoothie to a SSR (solid state relay) the first one i tried was wrong as it went from DC to AC I now have on order a DC-DC relay already tested the system works just like it did with the nano board in control safely


---
**Ariel Yahni (UniKpty)** *July 19, 2016 17:11*

O **+Dennis Fuente**​ why do you think you need to do all that? Also explain as this could make others in the thread confused 


---
**Jon Bruno** *July 19, 2016 17:30*

There we go.

That's why I brought it up.

I just wanted to see if there was a good way to clarify both approaches and what the differences are in a layman's sort of way. Not everyone is intimate with the way things work. Some just jump all in with the recommendations of the more savvy but then end up in over their heads. (not me...) just saying...lol


---
**Ariel Yahni (UniKpty)** *July 19, 2016 17:33*

**+Jon Bruno**​ I agree and it's imperative that  this topic gets clarified 


---
**Jon Bruno** *July 19, 2016 17:41*

Well, I was able to deduce that the pin at the defined 2.4 (assigned to fan duty) is most likely an N channel MOSFET and the + side of the connector it shares is probably tied to a rail.

My issues may reside in the nomenclature, 2.4 is a single pin But the image I have defines the entire connector as such. So is the - side on the MOSFET or the + side? (N vs P channel) But that's for another thread...﻿but this definitely needs to be dumbed down for the people following the pioneers.

﻿


---
**Dennis Fuente** *July 19, 2016 19:27*

Hi Guys sorry not trying to confuse anyone and i cannot speak to all the different types of PSU's in these machines i can only speak to the PSU in my machine and it is the same as the one in the picture and to my finding's using an O scope. 

 I have been describing my installation but no one has inquired as to my findings i just read this post this morning


---
**Ariel Yahni (UniKpty)** *July 19, 2016 19:49*

**+Dennis Fuente**​ please post a new thread on this community regarding your findings. I will appreciate if you could use plain English without acronyms. 


---
**Dennis Fuente** *July 19, 2016 20:03*

Sorry forgive me i think in the world i am in and not the fact the everyone dose not understands what i am wrinting my appolgies to all.

 I have tested my finiding's and be leave them to be correct in firing the laser safely and correctly as soon as i install the last componet and the installation is finished i will due my best to post what i have done on the forum hope to have this done by next week or sooner


---
**Alex Krause** *July 20, 2016 01:48*

Very interested in a write up on your setup and your cutting/engraving results **+Dennis Fuente**​ like **+Ariel Yahni**​ said a post of its own would be awesome 


---
**Dennis Fuente** *July 21, 2016 16:25*

So last night the DC-DC relay came i and i installed it worked like a charm i don't have everything locked in place it's all kinda hanging so i would like to help others if i can let me know what and how pass along the info i have gained


---
**Ariel Yahni (UniKpty)** *July 21, 2016 16:38*

**+Dennis Fuente**​ new thread with pictures of that you did and a diagram of. The connection. Nothing need story be Profesional grade


---
**Dennis Fuente** *July 22, 2016 21:47*

So guys i have had a setback for a reason so far undiscovered the smoothie board won't boot up only one led lights up and i can't even discover it through USB, so now double checking all of my circutry the board was working perfectly i did smoe test cut's no problem then shut the system down for the night and now it won't boot


---
**Ariel Yahni (UniKpty)** *July 22, 2016 22:31*

**+Dennis Fuente**​ make sure the SD is fully inserted and making good contact


---
**Dennis Fuente** *July 23, 2016 16:54*

Hi Ariel Thanks for the response i did check the SD card and even installed a new one also did a fresh firmware install still no luck board won't boot i think something went down on the board it sucks because the board was not cheap i only get the first LED to light up and that's as far as it go's disconnected everything took the boards out of the machine and just ran the usb to it still teh same i looked to the smoothie trouble tree found no help with anything on there so i will keep looking it could be the clock but i think the board is toast


---
**Dennis Fuente** *July 24, 2016 16:14*

Well the board is gone nothing has worked to get it to boot i have to get another board not going with a $150.00 smoothie again


---
**Ariel Yahni (UniKpty)** *July 24, 2016 17:36*

**+Dennis Fuente**​ did you ask the manufacturer for help? 


---
**Dennis Fuente** *July 24, 2016 18:49*

Well I thought  about  that but coming from an  electronic background I know  what is going be said and asked then around  we go 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/SYwgvoWB5C3) &mdash; content and formatting may not be reliable*
