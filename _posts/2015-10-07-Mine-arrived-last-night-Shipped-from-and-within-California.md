---
layout: post
title: "Mine arrived last night. Shipped from and within California"
date: October 07, 2015 19:47
category: "Discussion"
author: "David Cook"
---
Mine arrived last night. Shipped from and within California. Wiring was way better than I expected (I've seen terrible pics of wiring in the past with these ) test fired into a piece of blue masking tape no problem. 



Looks like the real cutting area is more like 12" x 10".  A lot better than the stated 200mm x 300mm.



Came with the Moshidraw.     

So can anyone here tell me what the deficiency is with the stock electronics?    I mean what features will I gain if I upgrade to a smoothie setup vs what it is able to do out of the box?



Thank you for your advice



David



![images/3a876664947c9297fe950a9629061641.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a876664947c9297fe950a9629061641.jpeg)
![images/6c47fa8d881be2b73a1d4d72fa1091b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c47fa8d881be2b73a1d4d72fa1091b1.jpeg)
![images/9b5138c3d375d93709665fd483d5b3b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b5138c3d375d93709665fd483d5b3b2.jpeg)
![images/35e43121c136b7c3b1e09cbbc5859cc4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/35e43121c136b7c3b1e09cbbc5859cc4.jpeg)
![images/f4bceec95d3e72c62a62f7cc095efd76.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4bceec95d3e72c62a62f7cc095efd76.jpeg)
![images/3196f37caa825e01b7d39901c474fd90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3196f37caa825e01b7d39901c474fd90.jpeg)
![images/73d72826ff58e087aa8bc69263fa1125.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73d72826ff58e087aa8bc69263fa1125.jpeg)

**"David Cook"**

---
---
**Ashley M. Kirchner [Norym]** *October 07, 2015 20:40*

You won't get a full 12x10 cutting area. The exhaust intake is in the way, and at least on mine, which came with a different, but equally cheesy controller, it parks the head about 25mm away from the left rail. I haven't been able to find where to reset that, quite annoying.



I do plan on making modifications to mine and once I replace the controller for a DSP controller, I hope to gain all of the available bed area.


---
**David Cook** *October 07, 2015 21:05*

**+Ashley M. Kirchner** ah yes I planned on removing that exhause piece, I also have the stuff to setup air assist. The firmware park setting is something I did not realize. I'll also have to check into that. 


---
**Anton Fosselius** *October 08, 2015 05:33*

Ah, same "Fu Yu" text as on mine. Always a nice welcome when you open the package ^_^ mine is still complete stock, works but it's really not worth the time to work against moshidraw. If you value your time change controller board as soon as possible. I am too lazy, so my life is a hell,  that's the main reason I have not used mine for 6 months. ﻿


---
**Stephane Buisson** *October 08, 2015 16:22*

hehe welcome to the club ;-))


---
**Gregory J Smith** *October 09, 2015 01:15*

There's lots of good discussions about upgrades to the controller.  From my perspective I'm happy with the existing controller once I played around and understood how the whole Coreldraw addin worked and what settings did what.  I haven't got a good enough reason to change.  I can engrave and cut using svg, dxf, bitmap and other formats as I need using the Corel draw import functions. The dongle is a pain, but I used an old laptop as a dedicated laser cutter PC, so the dongle stays in there.


---
**Ashley M. Kirchner [Norym]** *October 09, 2015 03:13*

You are correct **+Gregory J Smith**, for what you pay and get, it does as promised, cut and engrave the basics: acrylic, wood, plastic, paper, even your finger should you choose to. However it's the limitations that I'm bumping against.



For one, I don't own any Corel software, I'm not about to go buy it, nor do I fancy using the pirated copy that's provided with the machine. I DO however own the full suite of Autodesk CAD software as well as the full suite of Adobe products. These are what we use at my office to drive our large CNC machine. For example, from Illustrator, we simply submit one single, native illustrator file to the spooler. That file has lines of different colors, telling the CNC what it's cutting, what it's engraving, and what it's creasing (for folding.) The color also determined what tool it uses, whether it's the high speed rotary cutter, the oscillating knife, or the creasing roller. The driver identifies and handles it all. The same goes for a native DXF AutoCAD file.



Now I fully realize that this is all in the driver for the CNC and not so much the controller, however the controller still needs to know how to handle the commands and what to do. But this is also why I said, in the end, I may just decide to roll my own driver/software.



Come back to the laser cutter: there is no reason the same thing can't be applied here. No reason you can't 'print' a gradient and have the driver instruct the controller how to manage the laser's output settings for that. No reason I can't send a single job out of Illustrator that tells it what to engrave and what to cut, as one single job, as opposed to what I currently have to do which is to engrave first as one job, and then cut as a second job. That to me is just dumb.



The other reason for me to move to a DSP is the possibility of adding additional hardware later using the same controller, for example a z-controlled bed, or a rotary tool. I know the stock controller that came with my system does not have that ability at all, no additional pins, headers, or anything. So yes, an upgrade is in order.


---
**David Cook** *October 09, 2015 03:36*

Thank you all for the comments. That paints a clearer picture for me now.  I fully believe in a single job with attributes that control the laser appropriately. That's how it should be anyway. This weekend I'll have time to hook up the water and exhaust and try to connect to it.


---
**Stephane Buisson** *October 09, 2015 08:30*

**+Ashley M. Kirchner** You should create a new post with that text, before it will buried into the blog (just copy/paste).

User will be more in the understanding of the software->hardware chain with a global vision.



it's also my motivation to move to :

my softwares -> Smoothie/Visicut.



When a user is confortable/confident, it's a time saver move and a lot less material waste.


---
**David Cook** *October 12, 2015 12:42*

**+Allready Gone** my laser is not working properly so today I stayed home from work so I can replace the controller with an Azteeg X5 32-bit board. 



I actually convinced myself that 40watts is not enough to cut carbon laminate. But I am interested to know how that comes out. Please let us know.  I did here that the resin used can put out harmful fumes when heated so be careful.



Using it as a printing bed is interesting. I wonder if a matte surface is better than a gloss surface. 


---
**David Cook** *October 12, 2015 15:29*

I have a piece of 0.5mm carbon plate I can try later. Hmm 


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/7932eAWSBMe) &mdash; content and formatting may not be reliable*
