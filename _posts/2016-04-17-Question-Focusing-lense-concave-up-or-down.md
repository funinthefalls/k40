---
layout: post
title: "Question... Focusing lense, concave up, or down?"
date: April 17, 2016 03:52
category: "Discussion"
author: "Brien Watson"
---
Question...  Focusing lense, concave up, or down?





**"Brien Watson"**

---
---
**Gee Willikers** *April 17, 2016 03:54*

Bend/arc up.

[http://www.ophiropt.com/user_files/co2/Spherical-aberration.gif](http://www.ophiropt.com/user_files/co2/Spherical-aberration.gif)


---
**Brien Watson** *April 17, 2016 03:59*

Thanks. I think I put it in backwards then..  Damn it!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 04:06*

There is a side that reflects too, that one should be facing up. My lens is flat on one side & concave on the other. I have the concave facing down for best results. Here's a link to a post I did on this.

[https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SKB2gqE8WmE](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/SKB2gqE8WmE)


---
**Mishko Mishko** *April 17, 2016 09:31*

My machine came with concave up, convex down. And it did cut, but I flipped it over anyway...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 17, 2016 09:33*

**+Mishko Mishko** Mine too. It would cut, but not thicker leathers (3mm) without multiple passes (10+) & high power. Once flipped to concave down, less power & only 1-2 passes were required.


---
**Brien Watson** *April 17, 2016 13:03*

Thanks everyone!  Always a pleasure.  😃


---
**Alex Hodge** *April 17, 2016 17:23*

Convex up.


---
*Imported from [Google+](https://plus.google.com/111917591936954651643/posts/BGesgMvhxBi) &mdash; content and formatting may not be reliable*
