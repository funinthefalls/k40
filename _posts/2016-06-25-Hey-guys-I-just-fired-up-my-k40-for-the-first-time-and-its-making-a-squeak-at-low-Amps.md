---
layout: post
title: "Hey guys, I just fired up my k40 for the first time and its making a squeak at low Amps"
date: June 25, 2016 04:26
category: "Original software and hardware issues"
author: "Nick Lee"
---
Hey guys,

I just fired up my k40 for the first time and its making a squeak at low Amps.  but it will often carry into the higher amp range.  Here is a link to a video of it.

[https://drive.google.com/file/d/0BxAWfVv0_HQKd3VWcS1EemdLYjA/view?usp=sharing](https://drive.google.com/file/d/0BxAWfVv0_HQKd3VWcS1EemdLYjA/view?usp=sharing)



Has anyone seen anything like this?





**"Nick Lee"**

---
---
**Scott Marshall** *June 25, 2016 06:11*

I couldn't open the video,  but what you describe is typical of switching power supplies like the K40 uses, especially at low power. At low power the switching frequency gets low enough the human ear can hear it. If the coils in the supply aren't potted well, they can make quite a racket. It shouldn't be a problem other than annoying the user. There are fixes such as a little thin epoxy on the primary inductor, but generally, it's not worth fooling with.



 I've run into a few 'noisy' supplies that seemed fine on the bench, but squealed terrible when installed. It seems that occasionally, the magnetic field produced but he primary inductor (coil) is strong enough to vibrate the steel enclosure , turning it into a "speaker". (PSU cases are usually aluminum, probably for that reason)



If your sound is more of a repetitive ''snapping', accompanied by an ozone (metallic) smell, you may have arcing going on. If that's the case, it will do harm in short order and you should locate the source (either the power supply or the laser tube connections usually) and clean/insulate/separate as required to stop it.



Hope it's just a noisy power supply, they're pretty common,

Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 25, 2016 06:52*

**+Scott Marshall** I just looked/listened to the video & it's a high pitched squealing noise at 10mA+. Lower than that it sounded like when my laser zaps plywood (semi-normal sound), but it was an odd sound at the higher amperage.



**+Nick Lee** I'd be careful around the back-end of the laser near the tube while doing test fires like such in case it is arcing. You don't want to get hit with the arc as it's very high voltage & according to a video someone else posted recently it can cause your heart to stop. I placed a webcam in the back right corner of the tube area (facing the direction of fire) to monitor for arcs in my system/avoid getting zapped myself.


---
**3D Laser** *June 25, 2016 11:31*

I sounds like yours is tube is arching.  Take a look at the metal color at the end of the tube where the laser comes out see if the plasma is arching to it.  If it is you have a voltage leak in your water causing it to have a charge.  Change your water out and make sure nothing is exposed on your water pump and it should fix the issue.  I had the same thing happened change the water and it helps a lot 


---
**HalfNormal** *June 25, 2016 15:28*

Mine does the same thing. It does not affect the quality of the cut or engrave. I have done engraves that take hours and it has been just fine.  I have purchased a spare power supply just in case the current one fails.


---
**Nick Lee** *June 25, 2016 20:15*

Thanks for mentioning the power supply.  I tested it some more this morning and the sound is more definetly from the PSU.  Ill let you know what i find and try not to kill myself ;)




---
**Nick Lee** *June 25, 2016 20:37*

Update.  

Took the cover off the PSU.  It is definitely coming from the primary inductor coil.  The fame is not vibrating as was mentioned earlier.  Its something inside the coil.  But at this point Im way out of my area of expertese and i feel like the risk of death is rising above single digits playing with that coil. haha so im just gonna live with it.



TLDR

Good news: No arcing, no death

Bad news: crappy PSU, no solutions for sound.


---
**3D Laser** *June 25, 2016 20:55*

**+Nick Lee** cantact your seller my power supply failed in a month and they sent me a new one for free 


---
**Scott Marshall** *June 26, 2016 00:20*

**+Nick Lee** Nick, that's good news. If you want a easy fix, get a tube of 5 minute epoxy (it's usually thin and penetrates well, but starts to set up before it can drain out to the coil)



This works best if you can get the 'donut' horizontal, but you can get away with doing it in with the power supply still hooked to the  laser tube. (those are the real pain to hook/unhook as they're twisted in place and insulated with RTV).



Remove  the supply mounting screws (4), disconnect the line in and signal cables from the front of the supply, and that will get you some slack t move the supply up and tilt it as required. Duct tape it in place with the donut horizontal.



There is usually some white silicone from the factory on/in the inductor as an attempt to do what you are now going to do correctly.



Remove what you can of the silicone, being careful not to put too much force on it or scratch the wire enamel. You can usually just pull the worst of it away with your fingers, it's a filled silicone  with colloidal silica which makes it break away fairly easily. You'll probably find it comes off very easily, and didn't get into the space between the wire and the ferrite core, which is why it's vibrating. You don't have to get it all out, or even close, just make sure there's a path for the epoxy to get in between the wire and core.

It doesn't have to be perfect.

Build a dam around it wit masking tape or modeling clay to contain the epoxy, again beauty doesn't count, you just want the to stay on the coil and not run off before it sets.



Mix up enough epoxy to cover the inductor (or more - better to have too much then not enough, there won't be time for a 2nd batch) in a small paper cup, and as soon as it starts to get thin from the chemical reaction, pour it right down the center of the inductor. Make sure you get the gaps on the outside of the donut as well, thet's where your masking tape job comes into play.



You have to move right along, because once it starts to thicken, it's too late, as it won't flow into the small gaps you are trying to get to. The noise is caused by wires that are ALMOST touching the core or each other, when they vibrate they "bang" against the nearby object, producing the noise. If you can get epoxy in the gas, it can't "bang" anymore.



You don't want to cast the inductor into a "slug" of epoxy, only to get a heavy coat on it that has penetrated the gaps. (Many high quality inductors are dipped in epoxy before installation)



Once the epoxy is hard enough to pull the tape off, go ahead and re-assemble the works, by the time you get it ready to test, the epoxy will be set up plenty well enough.



I see you're concerned about the high voltage, and justifiably so. When powered on, it is undoubtedly a lethal voltage and should be treated with extreme respect. You are wise not to get into it if you don't have the expertise.



But..

With the power off, there's no risk of shock. after 5 minutes, any residual charge is gone and it's totally safe. This is the sort of job anyone with a little  mechanical experience can do in less than an hour with virtually no chance of screwing it up.



Enjoy you new quiet laser. Have fun!



Scott


---
**Akos Balog** *June 29, 2016 14:02*

Nick, could you solve it? Mine does exactly the same... And I also discovered, that the plasma hits only 1 part of the metal ring at the end of the tube, and won't go in the centre of it.



I'll also try to change the water, as Corey Budwine wrote


---
**3D Laser** *June 29, 2016 14:04*

**+Akos Balog** try changing your water when I changed mine it helped a ton I also had to change my pump as it was leaking voltage into the water giving it a charge if you can get distilled or deionized water all the better


---
**Akos Balog** *June 29, 2016 14:08*

**+Corey Budwine** thanks, I'll try it, and report back. I'll also try to test my pump, and maybe try another one...


---
**Akos Balog** *June 30, 2016 18:35*

**+Corey Budwine** I have changed the water, now the noise is mostly gone. Thanks ;)


---
*Imported from [Google+](https://plus.google.com/107557500099114558538/posts/iEujpybTAov) &mdash; content and formatting may not be reliable*
