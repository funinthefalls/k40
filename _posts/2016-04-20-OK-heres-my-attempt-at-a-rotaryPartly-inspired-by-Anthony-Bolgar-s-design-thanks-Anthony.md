---
layout: post
title: "OK, here's my attempt at a rotary.Partly inspired by Anthony Bolgar 's design, thanks Anthony"
date: April 20, 2016 09:13
category: "Hardware and Laser settings"
author: "Mishko Mishko"
---
OK, here's my attempt at a rotary.Partly inspired by **+Anthony Bolgar** 's design, thanks Anthony. There's certainly much room for improvement, but I decided to build something quickly and see what happens. I used a small pancake stepper I had lying around and did a reduction with the pullies I had from some other project. reduction is 2:1 although I calculated I need around 1.83:1, I guess I'll just have to squeeze or stretch the image a bit to get the proper ratio. 

I definitely miss some bearings, but didn't have any of that size at hand, so will have to see if it works as is.

I've attached the motor to the board and it turns without problems, so I'll just have to trick the optocoupler into thinking it's home and do some testing.

Two magnets at the bottom for easy positioning...

I wanted to make a slot for the other axis, but then opted for a row of holes, seemed easier to keep the axis in place...



![images/9aaf9129165e1f9dfdcbf6a0fbe81f9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9aaf9129165e1f9dfdcbf6a0fbe81f9e.jpeg)
![images/53adc1cc46976f05e5015b523c5f8d12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/53adc1cc46976f05e5015b523c5f8d12.jpeg)
![images/fa8300745944bb9c4740e619d2c0b85f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa8300745944bb9c4740e619d2c0b85f.jpeg)
![images/66187ffd0e84a424820a0294230c5bd0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/66187ffd0e84a424820a0294230c5bd0.jpeg)

**"Mishko Mishko"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 02:04*

Looks well done. Hope it works out for you. I like the idea of a slot for the axis not attached to the motor.


---
**Mishko Mishko** *April 21, 2016 05:10*

I haven't had much time to test it yesterday, have to figure out how to trick that home position optocoupler. If I put the gantry in the central position, even with the optocoupler taped, the motor just keeps turning. I'll try to push it all the way back today before switching on, maybe I am missing something, but I diddn't have much time yesterday to play with it, had to install and put my friends 90W laser in function, and thankfully that worked well;)

The slots may sound better than teh row of holes, but you'll absolutely need to have some sort of housed bearings which can slide left and right. Doable, but much more components. As it is now, I just have to loosen the end nuts and I can zig-zag it from one hole to another.

The motor is quite small and is overheating a bit, but I don't think it will be a problem. If it starts to boil, I'll creplace it with a bigger one NEMA 17 rather than fumbling with the board, serching for the step/dir signals, adding EasyDriver etc... As somebody here said, non scientific research;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 21, 2016 05:50*

**+Mishko Mishko** I'm not so certain how the optocoupler things work, however there are a few others here that managed to get it working for home builds of rotary attachments, so maybe they have some pointers for how to trick it.


---
**Mishko Mishko** *April 21, 2016 11:48*

Will check the homing sequence more in detail and report what happened. I've also bought some bearings, so if this works, mabe I'll "upgrade" it later, but I think this should work OK for testing purposes as it is... We'll see...


---
**Chris Parks** *April 23, 2016 15:53*

i wonder how one of those hotdog roller systems would work


---
**Anthony Bolgar** *April 26, 2016 23:49*

Nice job!


---
**Carl Fisher** *May 11, 2016 02:39*

how are you controlling the rotary? Swap out Y axis connections?




---
**Mishko Mishko** *May 11, 2016 07:45*

Yes, I just connected this motor instead of Y


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/G6dQjK1xgJn) &mdash; content and formatting may not be reliable*
