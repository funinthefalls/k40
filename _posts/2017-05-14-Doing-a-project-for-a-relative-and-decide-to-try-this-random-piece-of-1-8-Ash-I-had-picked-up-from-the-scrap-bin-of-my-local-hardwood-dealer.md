---
layout: post
title: "Doing a project for a relative and decide to try this random piece of 1/8\" Ash I had picked up from the scrap bin of my local hardwood dealer"
date: May 14, 2017 01:43
category: "Discussion"
author: "Ned Hill"
---
Doing a project for a relative and decide to try this random piece of 1/8" Ash I had picked up from the scrap bin of my local hardwood dealer.  Now I can see why nobody uses ash for laser engraving :P  The darker summer wood grain lasers dark while the lighter spring wood grain basically stays the same.  Going with my first choice of Cherry, but thought I would share in case anyone was wondering about using Ash. :)

![images/214f04476e9e54ed1ed3ff29f46a422e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/214f04476e9e54ed1ed3ff29f46a422e.png)



**"Ned Hill"**

---
---
**Ashley M. Kirchner [Norym]** *May 14, 2017 01:59*

Yup, tried it last year on something and gave up instantly. Now Cherry ... how well does that cut and engrave? I've been meaning to try cutting some 1/8" but it can be an expensive test. One 12x24" sheet 1/8" thick sheet costs a little over $18 ... 


---
**Ashley M. Kirchner [Norym]** *May 14, 2017 02:00*

Oh, regular K40 tube ...


---
**Ned Hill** *May 14, 2017 02:09*

Oh, Cherry works great.  

![images/fc75cd5bffcdbd0590879427a7db897d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/fc75cd5bffcdbd0590879427a7db897d.png)


---
**Ashley M. Kirchner [Norym]** *May 14, 2017 02:11*

Easy single pass cutting?


---
**Ned Hill** *May 14, 2017 02:15*

Yes, I cut this 1/8" piece at 9mm/s with 41% (~11ma) power.  Probably could have gone a little faster or lower power.


---
**Ashley M. Kirchner [Norym]** *May 14, 2017 02:33*

Cool. Gives me reason to go ahead and buy a sheet to play with.


---
**Mark Brown** *May 14, 2017 22:01*

I have some ash scraps in my shop I was going to experiment with. I sort of figured it would do this.



Looks like your cuts are straight, I almost thought it might make a wavy cut from the density difference, but I guess the laser is too focused for that.


---
**Ned Hill** *May 15, 2017 02:04*

Yeah, the ash actually cut just as well as the cherry.  Ash would probably be good for making things if all you were doing was cutting.




---
**Kyle Kerr** *May 19, 2017 01:43*

**+Mark Brown** since you are going to try Ash, I'm curious if you could try using a picture of the wood as a mask to even out the engraving.

My thinking is take a picture, use Gimp to threshold the image and use that to set up maybe a dual pass engraving to make a more even colored engraving.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 01:52*

The amount of time spent trying to get that lined up properly is likely not worth it. But I'd say it's not impossible to do.


---
**Kyle Kerr** *May 19, 2017 01:58*

**+Ashley M. Kirchner** if you had a small bit of extra space on both sides of the material, you should be able to engrave a single line between two dark patches and adjust as needed?


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 02:01*

If you have the material handy, try it, and see how well it works. As I said, it's possible, just takes fidgeting.


---
**Kyle Kerr** *May 19, 2017 02:02*

I wish I had a laser. :(


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 02:50*

Isn't it time to stop wishing and jump in with both feet?


---
**Kyle Kerr** *May 19, 2017 02:51*

Next time I have spare cash.


---
**Ashley M. Kirchner [Norym]** *May 19, 2017 03:06*

What is this "spare" cash you speak of?? And where can I find it?


---
**Kyle Kerr** *May 19, 2017 03:08*

It is difficult to explain. I don't have any currently to have a reference. :)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Jvjnb6oDW2L) &mdash; content and formatting may not be reliable*
