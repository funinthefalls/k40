---
layout: post
title: "Got my laser setup today and did the first engrave"
date: July 31, 2016 23:04
category: "Discussion"
author: "Robert Selvey"
---
Got my laser setup today and did the first engrave. Is there anyway to speed it up in corel draw I set it to 500mm and it's still slow do I have something setup wrong ?





**"Robert Selvey"**

---
---
**Jim Hatch** *August 01, 2016 00:12*

Could be your machine/device ID in the settings are not correct. Check to make sure they match your Nano board.


---
**Anthony Bolgar** *August 01, 2016 05:29*

I think Jim 's suggestion is the solution. I had the same issue and that was the fix.


---
**Phillip Conroy** *August 01, 2016 05:49*

Just think of how thick the laser beam is at the correct focal point is 0.02 mm that is a lot of strokes to fill 1mm wide ,that is why engraving takes forever


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/g5iNtQadgbC) &mdash; content and formatting may not be reliable*
