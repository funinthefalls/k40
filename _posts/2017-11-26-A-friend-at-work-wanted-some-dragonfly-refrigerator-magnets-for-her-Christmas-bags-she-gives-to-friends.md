---
layout: post
title: "A friend at work wanted some dragonfly refrigerator magnets for her Christmas bags she gives to friends"
date: November 26, 2017 23:32
category: "Object produced with laser"
author: "HalfNormal"
---
A friend at work wanted some dragonfly refrigerator magnets for her Christmas bags she gives to friends. She did not want anything fancy so they are plain simple!  About 3 x 2.5 inches each on birch ply sealed with a clear coat. Here are the results. 



![images/8fb081cdea1b36ca498fa62064cebcd8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fb081cdea1b36ca498fa62064cebcd8.jpeg)



**"HalfNormal"**

---
---
**Ned Hill** *November 27, 2017 04:57*

Nice


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/3VWymkbqp1N) &mdash; content and formatting may not be reliable*
