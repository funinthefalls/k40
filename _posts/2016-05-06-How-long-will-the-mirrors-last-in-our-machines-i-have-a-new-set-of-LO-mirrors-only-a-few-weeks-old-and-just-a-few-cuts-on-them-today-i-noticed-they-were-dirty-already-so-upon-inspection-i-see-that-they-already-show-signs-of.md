---
layout: post
title: "How long will the mirrors last in our machines i have a new set of LO mirrors only a few weeks old and just a few cuts on them today i noticed they were dirty already so upon inspection i see that they already show signs of"
date: May 06, 2016 19:31
category: "Discussion"
author: "Dennis Fuente"
---
How long will the mirrors last in our machines i have a new set of LO mirrors only a few weeks old and just a few cuts on them today i noticed they were dirty already so upon inspection i see that they already show signs of pitting is this normal so soon thanks





**"Dennis Fuente"**

---
---
**Jim Hatch** *May 06, 2016 20:13*

What are you cutting? I've had mine for most of a year and clean them maybe once a month. No pitting that I can see. I did replace the last one in the laser head when I put on the LO air assist head but haven't had any issues with the others.



How much cutting/engraving have you done? How is your exhaust? Between the air assist and opening up the exhaust (getting rid of that small manifold that just takes up room inside) should keep all the smoke away from the mirrors.



If you've cut or engraved PVC or other chlorinated plastics that will eat your optics (chlorine gas).


---
**Dennis Fuente** *May 06, 2016 23:00*

Hi Jim

i am only cutting balsa wood and plywood all hobby grade stuff and i can't cut through the ply which is 3 MM cut's through the top layer and that's it even with multiple pass just burns the wood i have upgraded the lens and mirrors all to  LO parts i am cutting at 4-5 MA  as i have read that higher power will cut the tube life if i really crank up the power to full it will just about cut the ply all the way through the machine has air assist and stock exhaust i have cut it back though


---
**Jim Hatch** *May 06, 2016 23:21*

4-5ma isn't going to cut wood. 12-15 will. I think the rule of thumb is to keep it under 20ma - like 15-18 is still okay & not harm the tube's life. Check your lens. Convex (bump) should be up.


---
**Jim Hatch** *May 06, 2016 23:22*

4-5ma isn't going to cut wood. 12-15 will. I think the rule of thumb is to keep it under 20ma - like 15-18 is still okay & not harm the tube's life. Check your lens. Convex (bump) should be up.


---
**Dennis Fuente** *May 07, 2016 16:47*

The lens is a LO and the bump is up


---
**Jim Hatch** *May 07, 2016 16:57*

how is your alignment (mirrors) - single spot on all of them at all corners of the X/Y axis travel? Are you measuring the focal length right? LO sells two different focal length lenses for our machines. Also the focal length is measured from the bottom of the mirror not the bottom of the lens holder in case that's an issue.


---
**Dennis Fuente** *May 07, 2016 17:43*

my machine has always had a split beam i have relaced all the mirrors with LO also the head and focal lens the LO lens has the same focal length as the orignal what do you mean the focal length is measured from the mirror not the lens HUH


---
**Jim Hatch** *May 07, 2016 18:23*

Sorry, not mirror - measure from the bottom of the lens, not the bottom of the lens holder. If you've got air assist, the bottom of the lens holder (tip of the cone) is 10 or 11mm lower than the bottom of the lens. If it's the standard lens holder the bottom is a mm or two from the bottom of the lens. That makes a difference in the size of the beam and it's cutting power.


---
**Dennis Fuente** *May 07, 2016 19:49*

So again checked the mirror alignment got it best i can with the split beam then i got to thinking ( i hate it when i do this) i looked at the new LO head to see where the beam was coming out of the last mirro and hitting the lens well what i found is it's way off it hits to the inside toward the tube so it's the mirror setting on the housing at an angle machined badly and it's new trying to figure out how to shim it so it hits center of the lens man i am getting to hate my machine


---
**Jim Hatch** *May 07, 2016 22:46*

I had a similar problem when I put my LO air assist head on. The beam hit inside the cone somewhere. With the cone off it hit the material an inch or so behind the head.



So I started from scratch aligining the mirrors. They were all centered but low. So I shimmed them. Needed a couple of washers under the fixed mirror. Then the LO head was too high. I replaced the 3  standoffs holding the mounting plate to the X carriage with 25mm long ones (stock were 30mm high) & 1 washer under the plate screws. That put the plate just above the rail and the beam dead center. 



That fixed my issue with the beam canting off to the back. My spots are dead center at all the mirrors at all 4 corners of travel. It hits the material directly under the nose cone. 


---
**Dennis Fuente** *May 07, 2016 23:08*

i did a mirror alignment and and the shimmed the head mirror to hit dead center  i judt did smore test and got the machine to cut my 3MM ply just had to crack the power up to by the meter 18 MA if the tube goes it goes tired of playing with it


---
**Jim Hatch** *May 08, 2016 00:25*

I usually run mine at 18ma. Don't think that'll hurt your tube. I believe it overdrives at 25ma so you have breathing room.


---
**Dennis Fuente** *May 08, 2016 01:04*

thanks that's good to know i am just going to run it and cut what i need to cut it's why i bought it if the tube gose i guess i will have to buy a replacement for it


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/JB5DMA2b6Kh) &mdash; content and formatting may not be reliable*
