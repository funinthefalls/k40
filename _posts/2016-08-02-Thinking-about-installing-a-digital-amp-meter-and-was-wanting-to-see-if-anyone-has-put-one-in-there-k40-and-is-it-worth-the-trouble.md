---
layout: post
title: "Thinking about installing a digital amp meter and was wanting to see if anyone has put one in there k40 and is it worth the trouble"
date: August 02, 2016 09:09
category: "Hardware and Laser settings"
author: "Michael Knox"
---
Thinking about installing a digital amp meter and was wanting to see if anyone has put one in there k40 and is it worth the trouble





**"Michael Knox"**

---
---
**Don Kleinschnitz Jr.** *August 02, 2016 12:35*

I have, and it is quite valuable to use as a relative indication of power that is repeatable. I wired it across the "Current regulation pot". 

[https://goo.gl/photos/UHfLBjvQDtUsNpMG8](https://goo.gl/photos/UHfLBjvQDtUsNpMG8)



I used this meter: [https://www.amazon.com/gp/product/B00C58QIOM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00C58QIOM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)




---
**HalfNormal** *August 02, 2016 12:36*

**+Michael Knox** A digital amp meter will not respond as quickly as an analog meter will. for what we do, it is more bling than functionality. If you are still using the stock boards and pot, then a digital volt meter is more practical and can consistanly help make finding your sweet spots easier. 


---
**Don Kleinschnitz Jr.** *August 02, 2016 12:41*

**+Don Kleinschnitz** whoops just saw that you posted "amp meter", my hack is to measure laser power setting by measuring the control voltage. I thought about an amp meter but did not implement since I figured it would not add much over the analog meter.


---
**Don Kleinschnitz Jr.** *August 02, 2016 12:50*

**+HalfNormal** That is exactly why I did not do it :).


---
**Michael Knox** *August 02, 2016 20:46*

Thanks for the input


---
*Imported from [Google+](https://plus.google.com/110881466282790245771/posts/GNEesVEwKjd) &mdash; content and formatting may not be reliable*
