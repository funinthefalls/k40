---
layout: post
title: "Thanks again to Scorch Works for his ideas on a work-piece table"
date: April 19, 2017 21:37
category: "Modification"
author: "Ulf Stahmer"
---
Thanks again to **+Scorch Works** for his ideas on a work-piece table. My take on the design uses four pieces of 1/2" aluminum angle, four 2" Chicago screws and four springs (all from Home Depot) to make the spring-loaded brackets. These are then mounted on two MDF side rails.



Two pieces of aluminum angle are cut to just fit in between the laser cutter y-rails (14.5" in my case). The other two are cut about 1-1/2" shorter and nested inside the longer ones. Holes are drilled in each end for the Chicago screws.



The first aluminum rail is attached to the MDF side rails and serves as the fixed datum. The second aluminum rail is notched to sit on the MDF side rails. This allows it to be positioned for differing size work pieces. It's really simple, easy to make with only a hack saw and a drill, inexpensive (materials cost about $20) and looks and works really slick!



The top of the MDF rails are set at the laser focal point and can be shimmed to adjust for various thicknesses of material.



I think it's harder to explain than it actually was to make! Check out the photos. A picture is worth a thousand words!



![images/e292500a81ae8246c72798afd167ba41.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e292500a81ae8246c72798afd167ba41.jpeg)
![images/cb80ffcb8d140b646ecee685063e71b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb80ffcb8d140b646ecee685063e71b7.jpeg)
![images/263c1a3608173681f3ae0c433f8598a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/263c1a3608173681f3ae0c433f8598a9.jpeg)

**"Ulf Stahmer"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2017 00:03*

That's great. Only concern I'd have is long term with the MDF side rails that they may warp. Keep us updated in future if anything like that happens.


---
**Ulf Stahmer** *April 20, 2017 01:06*

**+Yuusuf Sallahuddin** I will. Ultimately, motorized adjustable rails would be fantastic. But the rails are easy to replace if required.


---
**Don Kleinschnitz Jr.** *April 20, 2017 03:18*

Do you remember what and where in HD you got the springs and Chicago screws :).  


---
**Ulf Stahmer** *April 20, 2017 11:57*

**+Don Kleinschnitz** They were in the hardware section. I'm up in the Great White North, so our HD stores have a slightly different stock than those down south. I can get a bag of 4 screws for about $4 Canadian. The compression springs (about $2 each) were also in the hardware section. They had several diameters, lengths and stiffnesses. I just chose one not too soft or hard and of a diameter that sat on the screw head.



If you can't find them readily in your local stores, let me know and I'll get some for you. 


---
**Don Kleinschnitz Jr.** *April 20, 2017 12:40*

**+Ulf Stahmer** FYI: I calculated the spring rate at roughly 1lb/inch, so I found this one online. I need to do some empirical testing to know if that is sized correct.



I think I am going to go with my latest design that uses machined adjustment screws vs Chicago screws. 



Yours is much simpler than mine though!



[mcmaster.com - McMaster-Carr](https://www.mcmaster.com/#9657k384/=17a1mj2)






---
**Ulf Stahmer** *April 20, 2017 14:44*

**+Don Kleinschnitz** 1 lb/in sounds about right. Looks like you've got your implementation well in hand. My formative work experience was in mechanical design. So I should hope that I can still come up with simple cost effective designs! :)


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/jXtjBxmDQVf) &mdash; content and formatting may not be reliable*
