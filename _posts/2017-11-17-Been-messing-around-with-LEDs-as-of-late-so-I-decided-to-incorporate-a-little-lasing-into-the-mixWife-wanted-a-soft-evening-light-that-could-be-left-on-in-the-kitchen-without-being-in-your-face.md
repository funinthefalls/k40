---
layout: post
title: "Been messing around with LEDs as of late, so I decided to incorporate a little lasing into the mix...Wife wanted a soft evening light that could be left on in the kitchen without being in your face"
date: November 17, 2017 00:47
category: "Object produced with laser"
author: "Mike Meyer"
---
Been messing around with LEDs as of late, so I decided to incorporate a little lasing into the mix...Wife wanted a soft evening light that could be left on in the kitchen without being in your face. I decided to etch a piece of plexi and backlight it. I've always like the tree that stands on the savannah at the beginning of the show "Nature" so grabbed an image off the internet and manipulated it in photoshop. Anyhow, here's the result...btw, the tree's called an umbrella thorn.



![images/f627c64e8cda4f92b2cf598bc551ea9d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f627c64e8cda4f92b2cf598bc551ea9d.jpeg)
![images/87e6faf8371575d48c5b170ee2d41f84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87e6faf8371575d48c5b170ee2d41f84.jpeg)

**"Mike Meyer"**

---
---
**Don Kleinschnitz Jr.** *November 17, 2017 02:22*

Sweet!


---
**Ned Hill** *November 17, 2017 02:26*

Beautiful! 


---
**Fook INGSOC** *November 17, 2017 03:26*

Super Cool!!!...I wonder if you could do multiple layers of plexiglass and then have a different color LED on each layer?!...a sandwiched color effect!


---
**Mike Meyer** *November 17, 2017 04:03*

Thanks for nice comments gents...Regarding multiple layers of plexi, you can buy RGB leds that are programmable that would eliminate a lot of headaches that would be involved in positioning different layers of materials. I did it the old fashioned way with individual LEDs and resistors, but there are LED light strips available that are extremely versatile (and relatively cheap).


---
**Rich Sobocinski** *November 17, 2017 13:29*

Nice. Can you show placement of the LED backlighting? Thanks


---
**Mike Meyer** *November 17, 2017 13:37*

Yeah, sure. If you look at the base of the tree, you will see a slot which the plexi slides into; that slot contains 30 LEDs along the length that shine upward. Looking at the base, you can the individual LEDs radiating upward, edge lighting the tree.


---
**Fook INGSOC** *November 18, 2017 02:05*

**+Mike Meyer** I believe you call it "in plane" LED lighting!!!


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/6ziDT29wS7S) &mdash; content and formatting may not be reliable*
