---
layout: post
title: "Curious... with all the talk of what we will do after G+ leaves, I am wondering why I haven't seen anyone bring up the concept of just using a Google Group"
date: January 14, 2019 04:35
category: "Discussion"
author: "'Akai' Coit"
---
Curious... with all the talk of what we will do after G+ leaves, I am wondering why I haven't seen anyone bring up the concept of just using a Google Group. What downsides are there to migrating over to there? This is sincere curiousity, especially since we're all already here... so we all have google accounts. <b>*shrug*</b>



Just thought I'd ask...





**"'Akai' Coit"**

---
---
**James Rivera** *January 14, 2019 18:10*

That's a good question. I hadn't thought of that. I just checked [groups.google.com - Google Groups](http://groups.google.com) and "Create Group" is an option.  Unless this is already being sunsetted, maybe this is a good idea. That being said, I've already created accounts on MeWe and Fosstodon. I'm not really using them much yet, though.


---
**Jamie Richards** *January 19, 2019 23:38*

[https://groups.google.com/forum/#!forum/k40lasers](https://groups.google.com/forum/#!forum/k40lasers)


---
**'Akai' Coit** *January 20, 2019 07:47*

**+Jamie Richards** Requested permission to join your group. :)


---
**Jamie Richards** *February 05, 2019 18:03*

Not much interest in the groups, everyone is moving to Facebook or Discourse.  Heck, some are moving from Facebook to Discourse.  Like this one for the Cohesion3D board.  [forum.cohesion3d.com - Cohesion3D Community - Cohesion3D Support Community](https://forum.cohesion3d.com/)


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/3RvRYjpKich) &mdash; content and formatting may not be reliable*
