---
layout: post
title: "OK so i decided to try laser web since i recently switched to smoothie based platform"
date: July 04, 2016 20:08
category: "Smoothieboard Modification"
author: "Derek Schuetz"
---
OK so i decided to try laser web since i recently switched to smoothie based platform. and i am at a total loss on how to get this thing to work for k40



1) prints images mirrored since K40 homes awkwardly and there no option to change this in laserweb



2) Gcode uses S0-255 in command but smoothie needs S.01-S1 to change power. so i dont know if im missing something there



am i missing something or is this all as intended





**"Derek Schuetz"**

---
---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:15*

**+Derek Schuetz**​ go to settings under gcode and put the on off commands plus the range there


---
**Derek Schuetz** *July 04, 2016 20:17*

Can you show me an example it says to type 0-255 


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:28*

Sure must be 0-1 for. Smoothie


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:29*

**+Derek Schuetz**​ this are my settings [http://imgur.com/loGr79k](http://imgur.com/loGr79k)


---
**Derek Schuetz** *July 04, 2016 20:34*

Oh wow literally just a 1


---
**Derek Schuetz** *July 04, 2016 20:34*

What about it mirroring my images


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:35*

For the invertion you need to change the config file. Y axis must home to limit max so it Park on the top left. The best is to send you the config file for your reference but I don't have it at this moment. 


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:44*

In the meantime you need you Y endstops as Max and home to Max Y


---
**Derek Schuetz** *July 04, 2016 20:52*

ok i just ddi that so i have it all configured as such but now its just slaming into the side because it things positive is negative direction


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:54*

Don't move the X. You just need to flip the Y. Either physically or with a  !  Next to the dir pin


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:54*

Can you send me your file. 


---
**Ariel Yahni (UniKpty)** *July 04, 2016 20:55*

ayahni@gmail.com


---
**Derek Schuetz** *July 04, 2016 20:59*

just sent


---
**Ariel Yahni (UniKpty)** *July 04, 2016 21:03*

What board do you have? 


---
**Derek Schuetz** *July 04, 2016 21:04*

AZTEEG X5 MINI V2


---
**Derek Schuetz** *July 04, 2016 21:05*

motion and laser firing is all good its jut the gcode that laserweb spits out


---
**Ariel Yahni (UniKpty)** *July 04, 2016 21:10*

You beta endstop seems fine.  What's seems strange is all the invertion you have in the Dir and enable pin for the steppers


---
**Ariel Yahni (UniKpty)** *July 04, 2016 21:11*

Laserweb just send the code,  so pressing up should move the gantry away from you


---
**Derek Schuetz** *July 04, 2016 21:17*

you have to invert the stepper direction if you change it to home to max otherwise it just goes the wrong way.


---
**Ariel Yahni (UniKpty)** *July 04, 2016 21:20*

Sure that's the same as rotation the connector. What I don't know is why you  Have the alpha and beta en_pin inverted


---
**Derek Schuetz** *July 04, 2016 21:39*

These just how the config for the x5 works if I non invert steppers don't move. So as it stand now the machine homes in the upper left, when I do a +x move it goes right when I do a -y move it goes down. So the issue is that laserweb spits out a guide whose first move is go some y positive direction which sends it straight in the wall. A ghetto fix would be to just have laser web start where ever the head is like inkscape has so I can position manually and target with laser poijter but I can't seem to find that option in laserweb 


---
**Ariel Yahni (UniKpty)** *July 04, 2016 21:54*

**+Derek Schuetz**​ I've being there and it's not LW. Your 0,0 should be left bottom but you need to home Y max. That way it resembles the coordinate in the screen but moves the gantry away from the bed so you could put and take out the material.  So when you run a job is should start from the left bottom


---
**Derek Schuetz** *July 04, 2016 21:56*

That's just it the k40 is not configured that way so It seems like my only option list to install new endstops


---
**Ariel Yahni (UniKpty)** *July 04, 2016 22:02*

No need to. I dint not install anything else from the stock endstops and have it setup like that


---
**Ariel Yahni (UniKpty)** *July 04, 2016 22:03*

When I get home I'll send you my config


---
**Derek Schuetz** *July 04, 2016 22:25*

Ok sounds good thanks for the help


---
**Jon Bruno** *July 05, 2016 02:43*

Ariel, I have a smoothie on order. Would you share your config with me as well?  Itll save me having to ask you again in 7-10 days.😅


---
**Ariel Yahni (UniKpty)** *July 05, 2016 02:50*

Sure, email


---
**Jon Bruno** *July 05, 2016 03:03*

Thanks big guy!   Sigmazgfx Gmail.😎


---
**Derek Schuetz** *July 05, 2016 04:51*

**+Ariel Yahni** well I removed 2 hashtags and it's working only thing I'm noticing now is the images print with some rather apparent lines do you know what the cause of this would be


---
**Ariel Yahni (UniKpty)** *July 05, 2016 11:17*

**+Derek Schuetz**​ pic? 


---
**Derek Schuetz** *July 06, 2016 01:30*

**+Ariel Yahni** [https://i.imgur.com/EWXGUct.jpg](https://i.imgur.com/EWXGUct.jpg)


---
**Ariel Yahni (UniKpty)** *July 06, 2016 01:36*

**+Derek Schuetz**​ thanks insane. Did you change you steps per mm? Mine are 157.5 I think you had something different. This is important as for each mm you are moving more steps


---
**Derek Schuetz** *July 06, 2016 01:38*

What's your microstepping set at?


---
**Ariel Yahni (UniKpty)** *July 06, 2016 01:38*

So to verify move 100mm with jog and measure the distance traveled


---
**Derek Schuetz** *July 06, 2016 01:39*

I'm doing 1/32 so that's why my value is different m assuming your doing 1/16 I can try doing double yours. Also what is your beam diameter


---
**Ariel Yahni (UniKpty)** *July 06, 2016 01:51*

Ok so you gut that, then 0.1 in beam diameter


---
**Derek Schuetz** *July 06, 2016 02:10*

Ok sink adjusted steps and it didn't do much since it was only off from 5 so I played with beam diameter and lower it down to in stages to finally .005 and it looks decent I can probably go to .001 and it'll be perfect 


---
**Derek Schuetz** *July 06, 2016 02:13*

Now I just need to figure out how to get my SD card reader on my reprap discount display to work and I'll be in good buisness


---
**Ariel Yahni (UniKpty)** *July 06, 2016 02:17*

Can't help you there I don't use the sd


---
**Derek Schuetz** *July 06, 2016 02:18*

Dam well thank you for all the help I'm now all smoothie set


---
**Tony Sobczak** *July 06, 2016 21:23*

Follow


---
**Alex Hodge** *August 07, 2016 18:52*

Okay so I'm in the same boat here as you were **+Derek Schuetz** The K40 0,0 is top left. It makes no sense to treat the top left Y endstop as a MAX endstop and end up not having a MIN Y endstop...Just to make Laserweb happy. So, when I home in Laserweb it takes me to the top left properly, but it thinks it's in the bottom right. Then it engraves a mirror image. And to add insult to injury it slams against the y max (actually min) after the job is complete, ignoring that it's exceeded my bed size and ignoring any endstop. ugh. So, what exactly did you do to fix it?


---
**Derek Schuetz** *August 07, 2016 18:55*

What's your email I'll send my config. And yes it's frustrating because it could easily be fixed if they would just add an option in laser web to change your (0,0) position but Peter says I'm an idiot for wanting this because it's not a standard starting position and all machine home at the bottom left.But hey his software is amazing so you gotta do what you gotta do


---
**Alex Hodge** *August 07, 2016 20:13*

Watch what you say about Peter. Oh it was Peter calling you an idiot, not the other way around so it's all good. :P

Anyway, I just said screw it and installed another endstop at the front of the machine, called it y min and the original y max, flipped my stepper wiring so it would travel in the opposite direction and I'm straightened away. My K40 now homes in the bottom left, 0,0.


---
**Ariel Yahni (UniKpty)** *August 07, 2016 20:45*

**+Alex Hodge**​ even if it makes sense to home at bottom left ( it does)  the gantry will be in the way to load material. 


---
**Alex Hodge** *August 07, 2016 21:15*

**+Ariel Yahni** That's not really an issue because i load material with the power OFF on the laser. I can move the gantry out of the way since there is no power to the steppers.

I just either run a g28 to home before the job starts or position the head at what i want to be 0 and set 0.


---
**Ariel Yahni (UniKpty)** *August 07, 2016 21:18*

So you never load while the machine is on? Where did you home while on the nano? 


---
**Alex Hodge** *August 07, 2016 21:44*

I never used the K40 with the nano. First thing I did was remove it. Just today I installed another endstop so I could home to the bottom left. Prior to that I homed to the top left. I switched because Laserweb3 has no means to flip this orientation. Apparently this is because Peter is very particular about following the standard layout found in Cartesian printers. Or at least that is what the documentation says.


---
**Ariel Yahni (UniKpty)** *August 07, 2016 21:51*

**+Alex Hodge**​ the nano home to top left as default, and nothing makes sense specially if you come from 3DP. I home to my top left but my origin is bottom left. Only using one endstop 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/gttCAcG8rNG) &mdash; content and formatting may not be reliable*
