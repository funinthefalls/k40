---
layout: post
title: "Does anyone know how output formats in laserdraw via Corel work?"
date: July 14, 2015 02:18
category: "Software"
author: "Troy Baverstock"
---
Does anyone know how output formats in laserdraw via Corel work? When I use PLT the scale is over by 1-2mm, but when I use WMF the scale under by about .5mm but much more acceptable. 



Also in Corel unless I have a solid shape, WMF will cut twice even when set to once, PLT cuts once. Alternatively if it's a solid shape, both cut once.



Which format should I be using? The default is PLT, but the scale is inaccurate. I found a video of someone having the same problem and he also shows engraving and cutting in the one run which I didn't realise was possible with this machine/software.




{% include youtubePlayer.html id="MR7967VHKnI" %}
[http://youtu.be/MR7967VHKnI](http://youtu.be/MR7967VHKnI)







**"Troy Baverstock"**

---
---
**tony alexander rico cera** *July 14, 2015 02:22*

Revisa el tamaño del area de trabajo.


---
**David Richards (djrm)** *July 14, 2015 04:58*

Interning video. I wondered what some of those settings were for.


---
*Imported from [Google+](https://plus.google.com/+TroyBaverstock/posts/jEnwfMmovUu) &mdash; content and formatting may not be reliable*
