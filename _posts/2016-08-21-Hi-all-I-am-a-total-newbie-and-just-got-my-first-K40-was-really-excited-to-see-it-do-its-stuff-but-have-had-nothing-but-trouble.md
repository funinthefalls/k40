---
layout: post
title: "Hi all! I am a total newbie and just got my first K40, was really excited to see it do its stuff but have had nothing but trouble :("
date: August 21, 2016 16:24
category: "Discussion"
author: "David Broome-Smith"
---
Hi all! I am a total newbie and just got my first K40, was really excited to see it do its stuff but have had nothing but trouble :(. To summarize (A LOT) the machine doesn't seem to recognise the bed size or homing position. Regardless of the page size in LaserDrw or orientation to top left etc it keeps taking the head to bottom right and trying to engrave on the metal casing and thus making an awful juddery  mirror loosening vibration noise. Anyone experienced this before?





**"David Broome-Smith"**

---
---
**Ariel Yahni (UniKpty)** *August 21, 2016 16:37*

Lots of issues come from poor connections. Regardless if this is the reason for your issue I'll recommend you check each and every connection  ( be carefully, disconnect from power). 


---
**Phillip Conroy** *August 21, 2016 17:29*

if cutting i use 10mm/sec max,engraving 400mm/sec,anymore cutting machine will shake itself out of alignment


---
**David Broome-Smith** *August 21, 2016 18:40*

Thanks guys, will do. Probably wise to  check connections especially since there's a rattling/clicking sound coming from the controlpanel/mainboard area :/


---
**Jim Hatch** *August 21, 2016 20:57*

Are you really sure you set the machine & device id to match your board?



Also check to make sure the cables are in correctly so there isn't one upside down & sending the head to the wrong corner. They should go in only one way, but maybe something shortcut by the factory on yours.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 21, 2016 23:41*

I have found that max speed I can use whilst cutting is 50mm/s without the juddering & shaking out of alignment. However, this varies depending on the complexity of the shape I am cutting. More complex shapes will need slower speeds due to the constant changing on direction.


---
*Imported from [Google+](https://plus.google.com/101369213593415895374/posts/d66uS1BKGnm) &mdash; content and formatting may not be reliable*
