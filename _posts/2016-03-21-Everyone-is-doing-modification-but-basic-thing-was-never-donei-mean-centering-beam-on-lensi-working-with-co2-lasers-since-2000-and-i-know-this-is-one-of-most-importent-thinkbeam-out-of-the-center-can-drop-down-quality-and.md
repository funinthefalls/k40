---
layout: post
title: "Everyone is doing modification ,but basic thing was never done.i mean centering beam on lens.i working with co2 lasers since 2000 and i know this is one of most importent think.beam out of the center can drop down quality and"
date: March 21, 2016 20:56
category: "Discussion"
author: "Damian Trejtowicz"
---
Everyone is doing modification ,but basic thing was never done.i mean centering beam on lens.i working with co2 lasers since 2000 and i know this is one of most importent think.beam out of the center can drop down quality and laser power

I think this should be next step on any mod of k40





**"Damian Trejtowicz"**

---
---
**Jim Hatch** *March 21, 2016 21:27*

True enough. But many of these come through where you just can't get it focused correctly at all corners of the gantry travel. Mine was racked so that I was never going to get it spot on when the head was in the front right of the machine. I had to take the XY rails apart to get it square. Once you're doing that you might as well do the exhaust mod as well as the materials plate so you can have some Z axis control. 



After that adding air assist is key to keep from having to clean the lens everytime you do something smoky - 10 minutes would cut my efficiency down by 25%. If you're adding air assist it makes sense to upgrade the optics at the same time so adding a better lens and mirror (at least for the head) makes sense. 



Swapping the controllers is probably the last on the list of things to modify. That will get you improved open source software but if none of the other things are done you'll still not be happy. If all the others are done, then you can get some pretty decent work done even without new electronics.



Somewhere in there you might want to address safety items like a lid interlock and a flowmeter switch for the water pump but that's dependent much more on your personal work style and concern for safety I think than the others which have clear cut and defined benefits for everyone vs just the folks who might lift the lid and stick their hand in the path of a laser beam. (BTW, that won't kill you - it's an unfocused beam except under the head itself and it'll hurt enough to make you jerk your hand out of the way well before you do major permanent damage :-) )



Just my two cents.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 21, 2016 23:39*

I spent hours & days adjusting my beam alignment to get it as precisely centred as possible. It is very very close to centred for me, in all four corners of the cutting area. It hits pretty much the exact same spot in all four corners.



I will have to agree that it is important, because I did notice a significant improvement on cutting/engraving once I had it precise. Not so much on the power as per se, but more on the lack of power drop as it reaches the further parts of the machine (e.g. front right corner).



Prior to seriously tackling the alignment issue, I was having nearly 50% power drop in the front-right, meaning it would look lighter on engraves in that area & cuts would definitely not cut.


---
**Thor Johnson** *March 21, 2016 23:42*

I'm not in the center, but I used thermal paper (use that so you don't have to burn the paper and smoke your mirrors) to make sure the beam was in the window at all 4 corners.

Then I did some "mods" -- in quotes because they're just addon bits; nothing really complicated requiring disassembly or odd wiring:

I think air assist is a critical first mod (I printed one -- it has a large hole so it won't burn, but that means I run it off shop air), then I made a hanger so I can have a targeting laser ([http://www.thingiverse.com/thing:1002341](http://www.thingiverse.com/thing:1002341)).



The only "weird mods" I've done so far have been to put the laser, pump, fan, etc all on a power strip (so not that weird), and to put the USB key and the laser on a USB Hub (thanks to a user in this group)... In a bit, I'll play with Ramps/Smoothie, but not quite yet.



---

"BTW, that won't kill you - it's an unfocused beam except under the head itself and it'll hurt enough to make you jerk your hand out of the way well before you do major permanent damage"  -- That "Jerking your hand Away" can cause more damage than you think -- my K40 startled me [moved when I thought it was done and I was going to remove the material], and I tipped over the bucked of coolant...


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/hzQ4iRSZ227) &mdash; content and formatting may not be reliable*
