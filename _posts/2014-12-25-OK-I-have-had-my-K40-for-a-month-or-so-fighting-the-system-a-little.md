---
layout: post
title: "OK - I have had my K40 for a month or so - fighting the system a little"
date: December 25, 2014 19:28
category: "Discussion"
author: "Tim Fawcett"
---
OK - I have had my K40 for a month or so - fighting the system a little. I have a problem cutting 5mm acrylic - 15mA and 5mm/s - and it does not fully cut - there is penetration over 60% to 70% of the line, but not enough to break through fully. I have checked alignment - the spot lines up +/-1mm or so over the whole area - three corners align perfectly, the fourth is about 1mm or so off. Mirrors look good but a slight scratch on the laser head mirror - I would be surprised if it made a difference. The lens seems a bit naff - does it need ot be a certain way up?



So what settings are others using?



Tim





**"Tim Fawcett"**

---
---
**Imko Beckhoven van** *December 25, 2014 21:14*

11mA but i run the job twice. Not excact same circomstances but close


---
**Other World Explorers** *December 25, 2014 23:02*

Suggest checking water temp and flow. I like to keep mine @ or below 68F. I use 2 liter bottles with frozen water in them (saves on ice cubes and switch out is splash-less). Also clean the head lenses after each run... soot builds up quickly, make sure you are getting good fume extraction.. smoke can decrease effectiveness as well. I do ramble don't I.. lol 


---
**Tim Fawcett** *December 26, 2014 10:10*

Ok I will look at that. Currently I am using 25l of distilled water in a first-guarded shed but if the laser temp is too high it would explain the efficiency problems. Though the water temp does not seem particularly high (well below 28C) I will look at flow rates - one project is to improve flow and insert a flow sensor along with inflow, outflow and tank temperature sending and a decent interlock system. I am going to set up a build log as I also have a RAMPS 1.4 setup with Marlin on it to try instead of Moshi


---
**Tim Fawcett** *December 26, 2014 10:14*

Ducking auto-correct on the phone - "frost-guarded" shed


---
**Tim Fawcett** *December 26, 2014 19:12*

Sent a couple of hours upgrading the water piping - the flow sensor is now in place - the next stage is the interlocks and temperature sensing so the laser cannot be switched without adequate flow and when the interlocks are closed. I will also be putting in temperature sensing for inflow, outflow and the tank.



The other thing I want is an override for the interlocks so I can do things like alignment with the covers off - but which will autoreset when the covers are closed


---
*Imported from [Google+](https://plus.google.com/113171525751920354895/posts/SoDHpRRH2UE) &mdash; content and formatting may not be reliable*
