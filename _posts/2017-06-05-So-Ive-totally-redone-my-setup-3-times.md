---
layout: post
title: "So I've totally redone my setup 3 times"
date: June 05, 2017 01:16
category: "Hardware and Laser settings"
author: "Chris Hurley"
---
So I've totally redone my setup 3 times. I'm a bit depressed with it. I've even gotten a new lens to to put in the light object head. Granted it's a Chinese one that cost me $20  (good one is on order). The only stuff I cut is 3mm mdf, before   I replaced the head with the light object head on min power (5/6ma) on 25mm sec I could do it in 5 or 6 passes. Now it's more like 15-20. I've tried the old head, tried the new head/old lens, and new head/new lens.  



What power setting / speed do you guys use for 3mm mdf cutting? The reason I'm asking is to get a target to tune the machine.







**"Chris Hurley"**

---
---
**Joe Alexander** *June 05, 2017 01:23*

I havent done mdf yet but i cut 3mm birch ply(similar) at 8mm/s@15ma 1 pass. How are you controlling the signal to the laser psu? do you know the pulse duration? i would think an outrageous number might have this kind of issue but only conjecture.


---
**Joe Alexander** *June 05, 2017 01:35*

if you can run a test to see how many passes @10ma or 15ma. max tube power from manufacture is 23ma usually and I believe they strike around 4ma so maybe the lower voltage is fluctuating in intensity too much?


---
**Gee Willikers** *June 05, 2017 01:46*

Lens installed with curved surface up? Did you realign after the head change? Are you in focus to the focal length of your new lens?


---
**Jim Fong** *June 05, 2017 02:22*


{% include youtubePlayer.html id="sjkEJ3jOB4g" %}
[https://youtu.be/sjkEJ3jOB4g](https://youtu.be/sjkEJ3jOB4g)



When my k40 was new, I did this short video cutting 6mm MDF from Home Depot in two passes.  Stock lens and head.  I haven't cut MDF in months so I don't know if the laser can still do it. It been a very long time since I bothered to clean the lens and adjust them.  I mostly just do engravings on the laser. I use my CNC to cut MDF and ply  instead. 


---
**Chris Hurley** *June 05, 2017 02:57*

It's a stock machine but for the cutter head. How does one know the pulse duration or change that? Curve up, realigned three times. Focus is 50.8 with a 18mm znse. Amount of time on the laser is less then 100 hours and I usually cut at 1/3 power. 


---
**Joe Alexander** *June 05, 2017 03:04*

so that means still stock controller then? if it worked before then it should be fine. do you still get output at the mirrors when holding up a piece of paper? trying to figure out to which point your beam loses power between the various mirrors.


---
**Chris Hurley** *June 05, 2017 03:39*

Each mirror seems to be the same. I cleaned each mirror as well and this seemed to help. Right now I can cut 3mm mdf at 8-9ma at 5mm/s on 5 passes without air asst. Or 8-9ma@25ms on 8 passes. I prefer the faster movement as it help with charring. 


---
**Joe Alexander** *June 05, 2017 04:59*

when you check all four corners of the work area with a piece of tape covering the laser head hole, (entry before the last mirror, not nozzle side) is there only one spot on it or multiple? Is it hitting the middle of the hole or is it off-center? a picture of said tape would help visually confirm your calibration is properly set.(and of reflection point on mirrors while your at it ) I know a lot of this has been suggested but you know how troubleshooting works...IT makes you go through all the steps with them regardless :)


---
**Joe Alexander** *June 05, 2017 05:00*

I would also try flipping the focus lens over, couldn't hurt and will rule that out also.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2017 09:14*

Are you focusing into the centre of the material? or onto the top of the material? That makes a big difference too.


---
**Chris Hurley** *June 05, 2017 13:50*

I'll try all that and post pictures tonight. 


---
**Chris Hurley** *June 06, 2017 02:13*

![images/5a2aecb9def5370e79a756d710c412a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a2aecb9def5370e79a756d710c412a7.jpeg)


---
**Chris Hurley** *June 06, 2017 02:13*

![images/e9a269860b158d2b8f74e39b1826686e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9a269860b158d2b8f74e39b1826686e.jpeg)


---
**Chris Hurley** *June 06, 2017 02:14*

![images/05a6b64e339fcc5ce85b58650bf56426.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05a6b64e339fcc5ce85b58650bf56426.jpeg)


---
**Chris Hurley** *June 06, 2017 02:14*

![images/f07c1ae615ec9319539f386a7f75e480.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f07c1ae615ec9319539f386a7f75e480.jpeg)


---
**Chris Hurley** *June 06, 2017 02:14*

![images/69ca3bd914138c4516a8f89c05357b42.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69ca3bd914138c4516a8f89c05357b42.jpeg)


---
**Chris Hurley** *June 06, 2017 02:14*

![images/c2907b5b95e6cf6fe3f058d79d0a1317.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c2907b5b95e6cf6fe3f058d79d0a1317.jpeg)


---
**Chris Hurley** *June 06, 2017 02:15*

![images/d40ae58d1b44ded3ea855354c0feaf07.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d40ae58d1b44ded3ea855354c0feaf07.jpeg)


---
**Chris Hurley** *June 06, 2017 02:15*

![images/b24d73f4208a5ed71db301fbab304dd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b24d73f4208a5ed71db301fbab304dd7.jpeg)


---
**Chris Hurley** *June 06, 2017 02:15*

![images/47929dc18008857971ed859850abac83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47929dc18008857971ed859850abac83.jpeg)


---
**Chris Hurley** *June 06, 2017 02:15*

![images/93921696a39463ca99c4bc4687a1c76a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93921696a39463ca99c4bc4687a1c76a.jpeg)


---
**Joe Alexander** *June 06, 2017 02:23*

Well I am officially confounded. Seems like your betting beam power all the way to the nozzle, and height of focus is spot on. High Voltage connection looks solid, and I can see the beam energizing. Did you try flipping the focus mirror? maybe yours defies the laws of physics and runs backwards, stranger things have happened. and was that tape on the nozzle tested in all four corners of the gantry? (will show beam intensity is even across bed) Short of that I would guess bad tube/power supply. When mine died it still lit but had no output at all unlike yours.


---
**Chris Hurley** *June 06, 2017 02:37*

Well now my machine is making a lier of me. But I upped by table hight by 3mm and is cutting well. Or at least good enough. 


---
**Don Recardo** *June 06, 2017 11:14*

My laser has digital readout as a percentage 

I cut 3mm MDF at 6mm sec at 18%

Assuming your meter is 23mA max then it equates to about 4.1mA


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/9vrzbnuBbT7) &mdash; content and formatting may not be reliable*
