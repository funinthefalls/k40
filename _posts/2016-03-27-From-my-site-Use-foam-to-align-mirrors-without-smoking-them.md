---
layout: post
title: "From my site: Use foam to align mirrors without smoking them"
date: March 27, 2016 18:23
category: "Hardware and Laser settings"
author: "Thor Johnson"
---
From my site: [http://www.atl-3d.com/use-closed-cell-foam-for-alignment/](http://www.atl-3d.com/use-closed-cell-foam-for-alignment/)



Use foam to align mirrors without smoking them.





**"Thor Johnson"**

---
---
**Gary McKinnon** *March 27, 2016 19:12*

'Thor', very cool name :)


---
**Scott Thorne** *March 28, 2016 00:18*

I made acrylic targets that fit inside my mirror housings...no smoke can get in that way either. 


---
*Imported from [Google+](https://plus.google.com/100850237464188460837/posts/ir68BPfVnkv) &mdash; content and formatting may not be reliable*
