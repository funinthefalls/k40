---
layout: post
title: "the samples pics to be your reference, it was made by 40W mini laser machine FL-k40D"
date: August 18, 2015 07:37
category: "Discussion"
author: "Flash Laser"
---
the samples pics to be your reference, it was made by 40W mini laser machine FL-k40D.

40W laser tube

CorelLaser mainboard could support corelLaser and laserDRW

110V or 220V

hiwin guide rail



![images/d964f51b88bcec36621b05be96b9c074.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d964f51b88bcec36621b05be96b9c074.jpeg)
![images/2da8b86c2a551221a0e6e718c30a309b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2da8b86c2a551221a0e6e718c30a309b.jpeg)
![images/3edc154a6acaa50d5aa2b61e07e5045e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3edc154a6acaa50d5aa2b61e07e5045e.jpeg)
![images/87b842cbf60e31a3882e343ec926caf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87b842cbf60e31a3882e343ec926caf9.jpeg)
![images/4b550b89a158aedad5874c2e5f411491.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b550b89a158aedad5874c2e5f411491.jpeg)
![images/db578bdc890c93139eae12ba01b1db4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db578bdc890c93139eae12ba01b1db4d.jpeg)
![images/42b464ed7f888c7f3a47edf36d0eca59.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42b464ed7f888c7f3a47edf36d0eca59.jpeg)

**"Flash Laser"**

---
---
**Stephane Buisson** *August 18, 2015 08:55*

Well done !

thank you for sharing your designs. 


---
**Flash Laser** *August 18, 2015 09:28*

thanks Stephane!!

we are mini laser engraving machine factory with 10years experience engineer, hope could learn from each other.

coco@hcylaser.com

[www.flashlasercnc.com](http://www.flashlasercnc.com)


---
**I Laser** *August 18, 2015 10:01*

I've always wondered about people using others intellectual property (ie the minion) and selling it. I assume sooner or later someone is going to come knocking. There's heaps of listings around for this type of stuff.



Or am I just way too paranoid?


---
**Flash Laser** *August 19, 2015 00:51*

Hey, thanks for your kindly remind.

what we sale is laser machine, not this cutting wood pics, this pics was cutting by our machine, we don't want to offend any one. anyway, thank you all the same.


---
**Flash Laser** *August 19, 2015 00:51*

**+I Laser** Hey, thanks for your kindly remind.

what we sale is laser machine, not this cutting wood pics, this pics was cutting by our machine, we don't want to offend any one. anyway, thank you all the same.﻿


---
**I Laser** *August 19, 2015 09:44*

All good, I've just seen heaps of stuff for sale that is an obvious breach of copyright and just wondered whether it was just me being paranoid or if people do get sued!



BTW are you linked to Haotian Laser? In private correspondence with that member your email was copied to?


---
*Imported from [Google+](https://plus.google.com/+FlashLaser/posts/Qcv8KhRsMHJ) &mdash; content and formatting may not be reliable*
