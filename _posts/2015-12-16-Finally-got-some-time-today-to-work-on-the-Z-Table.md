---
layout: post
title: "Finally got some time today to work on the Z Table"
date: December 16, 2015 00:45
category: "Modification"
author: "ChiRag Chaudhari"
---
Finally got some time today to work on the Z Table. With limited time and tools I like to cut corners. Looks like 3rd cut (bottom right) was close to perfect. Now have to figure out how I am gonna connect bottom left corner together. BTW other three corners are nice and round, just like planned. 



![images/eb44bc69fce6a873432148786611c370.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb44bc69fce6a873432148786611c370.jpeg)
![images/7940e29b21efb12e2de20d5f3c2127f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7940e29b21efb12e2de20d5f3c2127f6.jpeg)

**"ChiRag Chaudhari"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 09:58*

Looks good. You could maybe use pop-rivet to connect that bottom left corner.


---
**ChiRag Chaudhari** *December 16, 2015 17:03*

**+Yuusuf Sallahuddin** I just googled what pop-rivets are..LOL. Thanks for tip.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/3EkAVz8a4j9) &mdash; content and formatting may not be reliable*
