---
layout: post
title: "Moshi, Marlin, RAMPS.. A dizzying web to noobs!"
date: February 27, 2016 15:11
category: "Software"
author: "Stuart Rubin"
---
Moshi, Marlin, RAMPS.. A dizzying web to noobs!



I just ordered a 50W ,4040 cutter which will come installed with standard Chinese Moshi firmware and PC software. I would like to understand what the various options are for upgrading, compatible PC software, etc., but there seems to be TONS to variations. I'm not shy about the technical stuff; I built a small GRBL-driven laser engraver a while back, but I want to understand what the community as a whole is doing so I can maximize my investment, minimize expenses, and make cool stuff with the least hassle. So, the question: Is there some kind of overview, grid, guide to what hardware/firmware/PC software/pluggins go with what?



I know my machine comes with Moshi, but I know there's also Corel Draw, Laser Draw, Arduino based controllers, RAMPS, various InkScape plug ins, LaserWeb, and probably tons more.



Can someone steer me to some kind of comprehensive overview?



What is the current "best" setup that hobbyists are using?



Thanks for the help!





**"Stuart Rubin"**

---
---
**Stephane Buisson** *February 27, 2016 15:48*

OK good time for an explaination:

A K40 or bigger one, arrived with proprietary hardware and software, Software being the weakness. In open sources softwares you have some very good one, like Visicut or Laserweb, but to run them you need open hardware too.

Now with time passing, we can say arduino/Ramps are a bit old and not as powerfull as ARM based board (like Smoothie). things like Acceleration need more horse power.

the best solution today is smoothieboard or compatible.

that said Visicut or Laserweb work with Marlin board (but not as well as Smoothie).


---
**Stuart Rubin** *February 27, 2016 15:56*

Stephane, thanks for the overview. Very helpful. So, I /think/ we're talking about three separate configurations in order of age/quality- Moshi (as shipped), RAMPs, Smoothie. 

Is there any kind of backward compatibility with the driving PC software, or are the GCodes (or whatever codes) incompatible?

Anyone want to put together a nice grid?


---
**Anton Fosselius** *February 27, 2016 15:59*

**+Stuart Rubin** search ebay for smothieware, you will find some controllers for various prices ;) 


---
**Stephane Buisson** *February 27, 2016 16:00*

Visicut is Mac/windows/linux no need of Gcode, print directly from it, Laserweb is on Chrome (+server)

both free to download, have a look

check my youtube video too


---
**Richard Taylor** *February 27, 2016 18:43*

There is also LaserSaur, but I'm biased!

[http://www2.artaylor.co.uk/store/index.php?route=product/product&product_id=51](http://www2.artaylor.co.uk/store/index.php?route=product/product&product_id=51)


---
**Stuart Rubin** *February 27, 2016 21:05*

Thanks everyone for their input. These additions all help my point that there are many (but not THAT many) options, but it's pretty confusing to know what goes with what.


---
**I Laser** *February 27, 2016 22:03*

**+Richard Taylor**​ wow the cost of that setup is nearly the same as the whole machine!


---
**Richard Taylor** *February 28, 2016 08:34*

Not really. Less than 1/2! More like the cost of a smoothieboard but with plug and play connections for the K40.﻿


---
**I Laser** *February 28, 2016 21:07*

Didn't realise how expensive the smoothie upgrade was! Or how much getting a K40 into the UK was either to be honest.



Just to clarify my comment, I bought my last machine for £225 delivered ($450 AUD), that upgrade set is £180 ($360 AUD).



Think I'll be sticking with the stock controller for a while yet!


---
**Richard Taylor** *February 29, 2016 07:31*

Wow, I need to move country :)


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/aJvjKsfpn9B) &mdash; content and formatting may not be reliable*
