---
layout: post
title: "Please support Open sources hardware and software contributors which is a big contribution to laser business"
date: December 06, 2015 22:10
category: "Discussion"
author: "Stephane Buisson"
---
Please support Open sources hardware and software contributors which is a big contribution to laser business.

the 3 main persons who deserve the most for the large amount of time they put in, would be :

**+Thomas Oster**  developer of  Visicut

[https://github.com/t-oster](https://github.com/t-oster) 

**+Arthur Wolf**   developer of Smoothie firmware

[https://www.paypal.com/us/cgi-bin/webscr?cmd=_flow&SESSION=FWKfcAvPUn_oCj8SHKzXi3Ww0FFfZespqRjVgHu_JNlr0mMZ8QmHr63e0vW&dispatch=5885d80a13c0db1f8e263663d3faee8d64ad11bbf4d2a5a1a0d303a50933f9b2](https://www.paypal.com/us/cgi-bin/webscr?cmd=_flow&SESSION=FWKfcAvPUn_oCj8SHKzXi3Ww0FFfZespqRjVgHu_JNlr0mMZ8QmHr63e0vW&dispatch=5885d80a13c0db1f8e263663d3faee8d64ad11bbf4d2a5a1a0d303a50933f9b2)  

**+Peter van der Walt**  hardware development

[https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=45DXEXK9LJSWU](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=45DXEXK9LJSWU)



**+Arno Yuan-Laser cutting machine**  **+BRM Lasers - Leading Laser Cutting & Engraving Machines Supplier in Europe** **+HaoYue Laser** **+Haotian Laser** **+Senfeng laser cutting &CNC** **+shenhui laser**

![images/bcb635e6c1578763193768fe059961f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bcb635e6c1578763193768fe059961f8.jpeg)



**"Stephane Buisson"**

---
---
**Arthur Wolf** *December 06, 2015 22:11*

**+Stephane Buisson** Yay, thanks very much :)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/dUS6XSN9Z4z) &mdash; content and formatting may not be reliable*
