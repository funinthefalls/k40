---
layout: post
title: "New here, stumbled upon it and thought I'd share one of my happy mods"
date: May 11, 2016 21:45
category: "Modification"
author: "Greg Curtis (pSyONiDe)"
---
New here, stumbled upon it and thought I'd share one of my happy mods.



Had a need (I thought) arise to replace my tube, and the supplier I bought from in Las Vegas had his 50w tubes on sale for the same price as his 40w, so I did what any good old tinkerer would do.



I cut a hole in my cabinet, and bolted that sucker in.



I've since learned that it may have not been my tube, but a wiring issue that I later resolved, so now I keep my 40w for a backup spare, or a later project.



Not a well lit picture, but I have an LCD mounted on my cabinet with a mini PC mounted on the back dedicated for laser operations.



![images/d2f13d40865185c6e6ad3bf5ef250715.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2f13d40865185c6e6ad3bf5ef250715.jpeg)
![images/c713f8ed053c6798dc5391ccc91af908.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c713f8ed053c6798dc5391ccc91af908.jpeg)
![images/36b646d4ab507121faffbe5e3eba5162.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/36b646d4ab507121faffbe5e3eba5162.jpeg)
![images/b7da6683ff3983be25fa91910f4cc926.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7da6683ff3983be25fa91910f4cc926.jpeg)

**"Greg Curtis (pSyONiDe)"**

---
---
**Greg Curtis (pSyONiDe)** *May 11, 2016 21:50*

Used a 4" diameter duct for shielding on the extension, and sheet metal screws to attach it and maintain ground. The end cap pops off in case I need to get to the water tube or electrical fittings easily.


---
**Ariel Yahni (UniKpty)** *May 11, 2016 21:56*

Nice. How much did the tube cost? Who's the supplier? 


---
**Greg Curtis (pSyONiDe)** *May 11, 2016 21:58*

$250 I think, I needed it in a hurry, so couldn't do the normal slow & cheap method. It was crapping out in the midst of a rather large order of custom keychains.



Laser Depot is the company, and it was last August that I did the mod.


---
**Jim Hatch** *May 12, 2016 01:02*

LightObject has an extension box to do the same thing so you can fit a 45W tube (1000mm vs the stock 700mm). It's square in shape and made of the same light gauge steel the K40 cabinet is made of.



I like the ingenuity of the duct but would worry something in my garage would end up bumping it, leaning against it or jumping on it and there'd be magic gas leaking everywhere :-)



Great to see a Macgyver here! Welcome aboard.


---
**Greg Curtis (pSyONiDe)** *May 12, 2016 01:33*

It's surprisingly rigid, and I have planned to add struts if needed, but where it's currently installed there is no chance of bumping it.



I almost got the one from LightObject, but again I needed to be up and running as quickly as possible.


---
**Jim Hatch** *May 12, 2016 01:38*

**+Greg Curtis**​ :-) you should see my garage. Nothing is safe :-D


---
**Randy Randleson** *May 12, 2016 19:15*

im interested in what computer you have in there? Is it a Pi?




---
**Greg Curtis (pSyONiDe)** *May 12, 2016 19:20*

**+Wayne Moore**​​ Zotac ZBOX-ID80-P, bought it from NewEgg a few years back. Upgraded it to 4gb RAM.



The only thing I use it for is the laser machine, I have Google drive synced so I can do my work on my main PC or on the go.﻿



Had to have a Windows capable PC as I haven't yet upgraded the controller, so I'm stuck with LaserDRW.


---
**Eric Parker** *May 14, 2016 00:30*

I'm very ENLIGHTENED by what you've shown me.


---
*Imported from [Google+](https://plus.google.com/101290438034029125250/posts/coQ8zMX84FB) &mdash; content and formatting may not be reliable*
