---
layout: post
title: "Community; It seems that the new K40 digital panel continues to create a lot of confusion and fails often"
date: February 20, 2018 13:04
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Community;



It seems that the new K40 digital panel continues to create a lot of confusion and fails often. 

Many K40 users are replacing them or at least adding analog power meters and controls. 



I am looking for someone to donate a panel to the the K40 lab so that we can:



1. Create wiring diagrams and schematics

2. Create a repair and troubleshooting guide

3. Create a replacement board. I am thinking an open source board with a digital current meter that fits the same spot? Perhaps with other monitoring meters and functions?



What do you think?





**"Don Kleinschnitz Jr."**

---
---
**Joe Alexander** *February 20, 2018 13:50*

for the amount of space the digital board takes one could probably fit the displays for laser power and water temp for a clean looking panel :) throw in a few status leds for color of course heh


---
**Cris Hawkins** *February 21, 2018 03:59*

I have a K40 with a digital panel that I am switching over to an analog meter and voltmeter like yours. I haven't ever fired up the machine as I am also converting it to C3D with external drivers (don't really like the Pololu style drivers).



You are welcome to have my digital board. Just tell me where to send it.


---
**Don Kleinschnitz Jr.** *February 21, 2018 13:27*

**+Cris Hawkins** THANK YOU Chris! I will pm you my address.


---
**Cris Hawkins** *February 21, 2018 14:38*

**+Don Kleinschnitz** Do you want the PC board only, or do you need the membrane panel also?



P.S. I got an email from G+ to "follow" you, but no address. IMO G+ really sucks - I HATE it!


---
**Cris Hawkins** *February 21, 2018 14:44*

**+Don Kleinschnitz** it would be a LOT easier if you go to [hawkjet.com - HawkJET Two Place Personal Jet](http://hawkjet.com) and click on "contact" to send me your address.


---
**Don Kleinschnitz Jr.** *February 21, 2018 15:02*

**+Cris Hawkins** wow both the panel and the board would be awesome. 

G+ is hard to get used to but pretty powerful in the end. 

Once you accept the "follow" you should get my post in your alerts. In any case I sent you an email using your business address.


---
**Abe Fouhy** *February 21, 2018 16:58*

Hey Don you can have mine too. I think I have credit at a pcb making shop too (sunstone circuits).


---
**Abe Fouhy** *February 21, 2018 17:08*

We should make the k40 essential upgrade kit open source.

A bom, schematic and instructions how to take your dangerous bare metal k40 to an interlocked, water cooled, air cooled device with grounds.



1. Safety schematic with pictures, adding grounds, fuses, circuit breaker/power strip, interlocks on power up

2. Monitoring of air, water flow, water temp, exhaust flow (calibrated for auto shutdown if fire happens)

3. Arduino/timer controlled warm-up procedure, checks water flow and temp, air flow, calibrates 02 meter and mad flow sensor in exhaust, interlocks device and turns on laser.


---
**Don Kleinschnitz Jr.** *February 21, 2018 22:05*

**+Abe Fouhy** thanks then I will have a couple of samples. I will pm you.




---
**Abe Fouhy** *February 21, 2018 23:09*

Sounds great bud! It'll be fun to work on a project together.

Hey do you fpga's?


---
**Abe Fouhy** *February 22, 2018 00:17*

We should make code to display all this on the gbrl screen too.


---
**Don Kleinschnitz Jr.** *February 22, 2018 03:56*

**+Abe Fouhy** Na, no fpga's. 


---
**Abe Fouhy** *February 22, 2018 04:15*

Bummer. :)


---
**Don Kleinschnitz Jr.** *February 22, 2018 12:40*

**+Abe Fouhy** why is that? Most cheap and simple to use embedded controllers would do this job easily, no?


---
**Cris Hawkins** *February 23, 2018 02:25*

**+Don Kleinschnitz** the digital panel is on its way. Tracking for USPS is in an email I sent you.... Have fun with it!


---
**Don Kleinschnitz Jr.** *February 23, 2018 04:15*

**+Cris Hawkins** once again thanks. I will start the reverse engineering when it arrives....


---
**Abe Fouhy** *February 23, 2018 06:31*

**+Don Kleinschnitz** I'm using fpgas for a different project.


---
**Cris Hawkins** *February 25, 2018 03:58*

**+Don Kleinschnitz** I'm assuming you got the digital panel today....


---
**Don Kleinschnitz Jr.** *February 25, 2018 04:45*

**+Cris Hawkins** Yup and thanks again now on to some reverse engineering.


---
**Don Kleinschnitz Jr.** *February 25, 2018 15:58*

**+Cris Hawkins** Chris, was the board working when you took it out? In particular did it fire the laser all the time?



Edit: never mind I figured it out .. very strange circuit...


---
**Cris Hawkins** *February 25, 2018 23:40*

**+Don Kleinschnitz** FYI - I knew from the beginning that the stock K40 was user hostile, so I took all the electronics out before I ever plugged it in. I am upgrading almost everything and have not fired the laser at all, nor have I even filled it with water. The board you now have has only been powered on the bench just once, but never attached to the laser.



I have a C3D mini and external stepper drivers. I purchased an analog meter and potentiometer, and will add a voltmeter across the pot like you have. I am building a new exhaust fan from a small a/c motor I have and a HVAC fan from my car (it really blows). For cooling I am using a radiator intended for computer cooling.



The project is about 60% finished. When I finish, I will get to fire up the laser for the first time.;)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Wf8tLp3eScL) &mdash; content and formatting may not be reliable*
