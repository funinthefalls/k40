---
layout: post
title: "So its been over a month since I got my new K40"
date: November 10, 2016 09:44
category: "Smoothieboard Modification"
author: "Andrew Brincat"
---
So its been over a month since I got my new K40. After telling the ebay folks it was a control board issue. they insisted I replace the PSU!



So after not getting anywhere with them i bit the bullet and replaced PSU and guess what? SAME THING!! (laser head does not return to home when powering on and head moves by itself  when firing the test burn)



now they are saying to buy a new control board.



I remember seeing someone post on here about doing  up a dummies guide on installing  Smoothie board. Thinking ill just do this now and have better luck with support from this amazing group than the tight arse ebay people ive been dealing with.



Thanks in advance!﻿





**"Andrew Brincat"**

---
---
**Stephane Buisson** *November 10, 2016 12:08*

Well, you could also see it in a positive way, especially if you kept the 1st power supply ;-))

Going the Smoothie way is easy, work with great open source software like LaserWeb, Visicut, Fusion 360.

more potential than the DSP way, witch is still close source.

Welcome, and don't hesitate to ask.

**+Anthony Bolgar** should also provide me with a latest link to update my old smoothie convertion tuto.

check into community ABOUT for any update.

(**+Anthony Bolgar** if you read me, you should know you can also edit the link onto the Smoothie website [smoothieware.org - SmoothK40 Guide - Smoothie Project](http://smoothieware.org/blue-box-guide))


---
**Bradley Blodgett** *November 12, 2016 17:32*

I'm in the process of doing the same thing. PSU first, laser tube second (Now I have replacements for both I guess) I have my smoothie x4 and my 5v step up. Hopefully will be getting it mostly squared away this weekend. Especially since I have an order that I need my cutter for to finish up. We can take the journey together!


---
**Andrew Brincat** *November 12, 2016 21:38*

Thanks guys I mainly want to use this to laser etch the wooden pens I make, but since  ill be doing a lot more work myself  now i thought I might try adding a rotating axis so i can etch the pen as it turns. Not really  critical but thought I may as well go all out while im at it!! Time to start studying!!


---
**Andrew Brincat** *November 24, 2016 04:13*

**+Bradley Blodgett**​ **+Stephane Buisson**​ any advice on what model board i should look at. 3 - 4 axis max i believe. saw stuff on 4x and 4xc, should i get an LCD? going through robot seed as i live in Australia and would like to get it before xmas hopefully... anything else i should consider? new stepper motors? i have this current nano board so do i need a middle man board? still got so much to learn. acronyms are killing me! haha thanks!!

![images/f0b611718125b1786d554d41adbddb37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f0b611718125b1786d554d41adbddb37.jpeg)


---
**Bradley Blodgett** *November 24, 2016 14:45*

That looks just like my board. I got the 4x board, but I have been struggling with the X-Axis movement. I have an order in for a middleman board to help with the proper pin alignment on the motor and use the optical endstops. I had to order 3 so I can send one out to you once I get them. They are expecting them back in a week or two.


---
**Andrew Brincat** *December 02, 2016 10:41*

Thanks **+Bradley Blodgett**​ that would be awesome!! Im going to order stuff this weekend hopefully!


---
*Imported from [Google+](https://plus.google.com/+AndrewBrincat/posts/4LUkGpJgFtL) &mdash; content and formatting may not be reliable*
