---
layout: post
title: "Today I remove lens for cleaning. Lens size is 12 mm but cover hole is 13.2"
date: August 21, 2017 11:10
category: "Modification"
author: "\u0e2d\u0e21\u0e23\u0e0a\u0e31\u0e22 \u0e18\u0e19\u0e18\u0e31\u0e19\u0e22\u0e1a\u0e39\u0e23\u0e13\u0e4c"
---
Today I remove lens for cleaning. Lens size is 12 mm but cover hole is 13.2. I made spacer ring from ABS for lens centering.



![images/0ac16b8895ed90317043a2f0ca83cbe5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ac16b8895ed90317043a2f0ca83cbe5.jpeg)
![images/b59496cf4a3f6949c898c7cafdc7967f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b59496cf4a3f6949c898c7cafdc7967f.jpeg)
![images/d8c26b9cb72e29612e3d4fec83e140fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d8c26b9cb72e29612e3d4fec83e140fd.jpeg)
![images/cedec1ae85693ee2e917e920821c7c13.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cedec1ae85693ee2e917e920821c7c13.jpeg)

**"\u0e2d\u0e21\u0e23\u0e0a\u0e31\u0e22 \u0e18\u0e19\u0e18\u0e31\u0e19\u0e22\u0e1a\u0e39\u0e23\u0e13\u0e4c"**

---
---
**Steve Clark** *August 21, 2017 16:03*

I've never checked to see how  hot the lens gets during normal long operations, PVC starts coming apart at 140C and melts around 160C. A misaligned beam could create problems too...So if the temp of the lens is below that and no serious alignment problems... you should be ok. 


---
**อมรชัย ธนธันยบูรณ์** *August 21, 2017 17:31*

I am used ABS melt at 200c. But I can change to PEEK later, Thanks for comment.


---
*Imported from [Google+](https://plus.google.com/113246487765078592316/posts/4woLj4z9YUe) &mdash; content and formatting may not be reliable*
