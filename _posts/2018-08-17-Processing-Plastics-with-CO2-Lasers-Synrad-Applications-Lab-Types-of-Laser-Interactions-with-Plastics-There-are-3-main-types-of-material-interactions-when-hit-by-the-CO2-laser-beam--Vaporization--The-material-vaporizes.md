---
layout: post
title: "Processing Plastics with CO2 Lasers Synrad Applications Lab Types of Laser Interactions with Plastics There are 3 main types of material interactions when hit by the CO2 laser beam:  Vaporization  The material vaporizes"
date: August 17, 2018 17:22
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Processing Plastics with CO2 Lasers Synrad Applications Lab



Types of Laser Interactions with Plastics



There are 3 main types of material interactions when hit by the

CO2 laser beam:

• Vaporization

– The material vaporizes into gas residue which is blown out when cut.

This is the cleanest cutting / marking process.

• Melt Sharing

– The material melts into molten droplets which are blown out when cut.

There is typically some melt-back

• Chemical Degradation

– The plastic material chemically degrades typically releasing carbon smoke.

The material chars and has soot residue



[http://microfluidics.cnsi.ucsb.edu/tools/Trotec_laser_cutter/Synrad%20LaserProcessingGuide_Plastics.pdf](http://microfluidics.cnsi.ucsb.edu/tools/Trotec_laser_cutter/Synrad%20LaserProcessingGuide_Plastics.pdf)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/VT5QtzUt9yj) &mdash; content and formatting may not be reliable*
