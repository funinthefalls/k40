---
layout: post
title: "New to the group hello everyone I have a 40 watt in a 50 watt power supply that both failed the 40 watt will only provide 3 milliamp the 50 does nothing is there anyway to diagnose the problem with the power supply other than"
date: January 11, 2018 04:28
category: "Discussion"
author: "jonathon Hendrickson"
---
New to the group hello everyone I have a 40 watt in a 50 watt power supply that both failed the 40 watt will only provide 3 milliamp the 50 does nothing is there anyway to diagnose the problem with the power supply other than throwing a new flyback at it I would like to troubleshoot it before just replacing a part and hope that's the problem





**"jonathon Hendrickson"**

---
---
**Paul de Groot** *January 11, 2018 05:41*

@jonathon Hendrickson can you describe any additional symptoms ? Does the psu ticks or the red led flashes or does it deliver 24 and 5v so i can pinpoint the fault.


---
**jonathon Hendrickson** *January 11, 2018 05:46*

This one doesn't have a red light it just has the green light in the test button when you push it I hear a little bit of a click inside the power supply. The one that produces 3 milliamp everything seems fine other than it will only produce 3 milliamp barely exciting the tube

![images/3efaf54cf04506952edc8c7cc07a5942.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3efaf54cf04506952edc8c7cc07a5942.jpeg)


---
**jonathon Hendrickson** *January 11, 2018 05:48*

I ordered a brand new one from China off Amazon when I got it there was two wires coming out from the flyback I knew that wasn't right one had a plug the other was a bare wire so I tried insulating it not sure why it was there hooking it up and it arked inside the power supply and now it doesn't do anything

![images/7387f966e5d1129e805916ec65631116.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7387f966e5d1129e805916ec65631116.jpeg)


---
**Don Kleinschnitz Jr.** *January 11, 2018 15:53*

I think that second wire (not the HV one) should either be connected on the PCB or it should be connected to ground. Can you point me to the site where you bought it?


---
**jonathon Hendrickson** *January 11, 2018 16:03*

40W 50W CO2 Laser Power Supply for CO2 Laser Engraver Cutter MYJG-50 110V / 220V [amazon.com - Amazon.com: 40W 50W CO2 Laser Power Supply for CO2 Laser Engraver Cutter MYJG-50 110V / 220V: Computers & Accessories](https://www.amazon.com/dp/B07193ZW9H/ref=cm_sw_r_cp_apa_jQ4vAbSA6JSCV)



They had me solder back to the board after they told me to solder and everything fell off and Transit


---
**jonathon Hendrickson** *January 11, 2018 16:03*

![images/e8c48602d6d06c25bcd8c2869961fb3f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8c48602d6d06c25bcd8c2869961fb3f.jpeg)


---
**jonathon Hendrickson** *January 11, 2018 16:04*

But it still won't fire I'm guessing when I fired it up the first time it took something out I had it insulated with rubber but when I push the test button it arced inside


---
**Paul de Groot** *January 11, 2018 20:14*

**+jonathon Hendrickson** are you able to return the item. Seems like it was already defect when they delivered. As don pointed out, the return wire from the tube goes to ground. If it has a short on the pcb then it would explain the tick and 3mA. I speculate that you have a short capacitor in the hv circuit. Or two routes on the pcb are too close too each other and the hv circuit takes that shorter route. This can happen when wires throught the circuit islands on the pcb are sticking out too far near each other. See if you can see it arcing under the pcb and clear the space between the arcing points by cutting those protruding component leads. Make sure you discharge the psu and it capacitors first otherwise you’re for a big shock. Let me know how it goes.


---
**jonathon Hendrickson** *January 11, 2018 20:20*

The one that's only producing 3 milliamps was in a working 40 what system that just all suddenly lost power the one they shipped me is completely dead I had insulated that red wire that should have been connected to the circuit board I don't know if I had it insulated well enough but when I tried firing it it arked inside did not look like it was coming from The Wire and now that one is completely dead so I just tried swapping my flyback from the original two power supplies I have to see if that will make any difference. I would like to try to find a way to troubleshoot these before it gets to the high-voltage Transformer or flyback so that way I could repair these looks like the flyback store only 30 $40 yes and I can probably get one of those locally I'm guessing they're fairly common generic item from a TV repair type place but waiting to get a hold power supply shift is going to take another 10 to 15 days and I really don't have that much time


---
**Don Kleinschnitz Jr.** *January 12, 2018 11:18*

**+jonathon Hendrickson** I doubt you will find the HVT locally it is not the same as TV flyback transformers, sorry.

See my post with links to schematics on another thread. It would be helpful to use one thread so we can keep track of progress in one place.

As I now understand it the grnd on the HVT was hanging loose when you got it and you tried to insulate it. When you powered it up it arch'd [it would if the HVT ground is not connected ] and now it is dead. 



The original problem was the power dropped with the current meter reading only 3ma.

The original problem can be the tube or the LPS!



Do you know that the LPS after you connected the HVT ground is no good? The ma-meter alone will not tell you that for sure.



I would:

1. re-solder all the joints in the unit that you can. Don't solder SMT parts.

2. Plug it in an try it.

3. If it still fails come back here for help doing an arc test.




---
*Imported from [Google+](https://plus.google.com/118336262261768424931/posts/EqSEv41Lzbt) &mdash; content and formatting may not be reliable*
