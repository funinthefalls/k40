---
layout: post
title: "I am going to ask the ultimate n00b question"
date: January 12, 2017 21:15
category: "Modification"
author: "Abe Fouhy"
---
I am going to ask the ultimate n00b question. Why upgrade the laserdrw/coreldraw board to smoothie with laserweb?





**"Abe Fouhy"**

---
---
**Cesar Tolentino** *January 12, 2017 21:30*

It boils down to what the owner or user needs. Others are perfectly fine with the original software. Others does not like the workflow of the OEM software. And the newer boards offer a lot more features. 



In my case, I used mohidraw for 3 years until it died. Now I'm using lasersaur board.


---
**Ariel Yahni (UniKpty)** *January 12, 2017 21:32*

Platform independence, community driven , lots of development onopen source software to easy the use ( LaserWeb ) , grayscale raster among other things 


---
**Abe Fouhy** *January 12, 2017 21:35*

One thing I am having trouble with is that the software doesn't seem to allow me to etch and cut on the same process and that I have to use coreldraw to create a two step process. Is this correct?

When I use the laser at school it does it by color, red for cut and black for engraving, does laserweb do that too? That would be very cool.


---
**Cesar Tolentino** *January 12, 2017 21:43*

**+Abe Fouhy** check this out 

[instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)




---
**Alex Krause** *January 12, 2017 23:05*

To do grayscales like this easily and to have multi operations like cutting and engraving without having to click a ton of buttons... Being able to control power with software instead of locked in to what the pot is set to

![images/a4a9b4a797bd5fe5e46704ae00fcc71f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4a9b4a797bd5fe5e46704ae00fcc71f.jpeg)


---
**Abe Fouhy** *January 12, 2017 23:34*

Oh man that is cool. Awesome! For $150 that looks like my next upgrade.


---
**Ray Kholodovsky (Cohesion3D)** *January 12, 2017 23:55*

Or, just, Cohesion3D Mini. All in one upgrade, pops right in, $99. 

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Abe Fouhy** *January 12, 2017 23:59*

This is really cool, is the importance of PWM control so the laser cools between movements? Or is more about the accuracy of the stepper step?


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 00:00*

None of that. PWM control is so you can actually set the power of the laser, from software, on the fly. That's how Alex gets his awesome engravings, and it's how you can have one job with different cut parameters 


---
**Abe Fouhy** *January 13, 2017 00:04*

Oh cool! Do you work for that cohesion3d? If so do you have instructions to install that board you coukd send me


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 00:07*

Yeah that's my company. Here's the early instructions **+Carl Fisher** wrote: [dropbox.com - C3D Installation.pdf](https://www.dropbox.com/s/emw9n72b2qi97nl/C3D%20Installation.pdf?dl=0)



Keep in mind there are other machine types out there, we support those too, just still working on the docs for it. 


---
**Abe Fouhy** *January 13, 2017 00:12*

Awesome. This looks like it'll work on my kl3020. Same machine really. This works with the laserweb or is there a different software package?


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 00:13*

LaserWeb is what we recommend. 


---
**Abe Fouhy** *January 13, 2017 00:14*

And laserweb is free/opensouce?


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 00:15*

Absolutely. Peter and the team now are doing a wonderful job. It doesn't require any keys to unlock it either :)


---
**Stephane Buisson** *January 13, 2017 09:01*

**+Abe Fouhy** laserweb is great, but more open source software exist. not trying  to challenge once against other, but saying 2 is better than one and keeping the choices open. So you can also try Visicut. (several jobs at once, by colors, etc...)


---
**Stephane Buisson** *January 13, 2017 09:21*

Laserweb/Visicut is independant of Operating system, so it work on Mac, Linux too.


---
**Abe Fouhy** *January 13, 2017 10:13*

What is the lead time from order to delivered for the cohesion3d?


---
**Abe Fouhy** *January 13, 2017 11:09*

Looks like I will be ordering that board after all. I just cooked my OEM after working with the PS.


---
**Robert Selvey** *January 13, 2017 11:26*

Thinking of upgrading to your Cohesion3D Mini, do I have to change anything else out for the stepper motors ?




---
**Kostas Filosofou** *January 13, 2017 11:42*

**+Robert Selvey** no you just replace the old board with the Cohesion3d mini...


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 14:52*

Correct. Everything stays the same you just swap boards. 


---
**Don Kleinschnitz Jr.** *January 13, 2017 15:48*

**+Ray Kholodovsky** note that **+Abe Fouhy** supply is one that I have never seen before and may require some install tweaks. I also think the control panel is different than a standard K40?


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 15:49*

**+Abe Fouhy** pictures of everything please :). 

Machine, power supply, control board, wiring, panel. 


---
**Abe Fouhy** *January 13, 2017 18:03*

Ok will do. I'll document it all


---
**Abe Fouhy** *January 13, 2017 19:25*

Does cohesion3d support power z later?


---
**Ray Kholodovsky (Cohesion3D)** *January 13, 2017 19:56*

Yepp, it has 4 motor drivers so you can run a Z table and a rotary.  If you want to do that now just grab the extra 2 A4988 drivers, or if it's more beefy then you might need the external driver adapters and use a black box external driver.


---
**Abe Fouhy** *January 13, 2017 20:15*

Ray you're the man! Thanks so much


---
**chris82o** *January 18, 2017 23:51*

Can wait to get a cohesion3d mini thats going to be my first upgrade next to air assist 


---
**Ray Kholodovsky (Cohesion3D)** *January 18, 2017 23:52*

Can or can't wait? :)


---
**chris82o** *January 19, 2017 00:12*

Can't lol


---
**Matthew Wilson** *January 30, 2017 05:20*

Is the cohesion 3d mini a software upgrade where you can use an open source program instead of laserdraw that comes with the machine? I am not a tech savvy person, so I need layman terms please.


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 15:53*

In your machine is a green circuit board (also known as card, controller). It forces you to use that crappy coreldraw type software. 

The Cohesion3D Mini is a new circuit board. You take out the old one, and put in the new one. Since this is a physical item, it is a hardware upgrade. 

Now you can do engravings and variable power cuts on your machine using awesome open source software like LaserWeb to control it. 


---
**Matthew Wilson** *February 27, 2017 18:34*

How do I download laserweb3? I would like to play around with the program before the cohesion arrives to get a head start.


---
**Ray Kholodovsky (Cohesion3D)** *February 28, 2017 03:43*

[github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3)



Instructions: [https://github.com/LaserWeb/LaserWeb3/wiki](https://github.com/LaserWeb/LaserWeb3/wiki)


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/j9Umqob83Px) &mdash; content and formatting may not be reliable*
