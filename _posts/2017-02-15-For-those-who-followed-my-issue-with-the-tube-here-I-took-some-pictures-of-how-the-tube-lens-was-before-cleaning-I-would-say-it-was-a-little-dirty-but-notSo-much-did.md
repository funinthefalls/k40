---
layout: post
title: "For those who followed my issue with the tube here I took some pictures of how the tube lens was before cleaning, I would say it was a little dirty but not.So much did"
date: February 15, 2017 01:52
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
For those who followed my issue with the tube here [https://plus.google.com/+ArielYahni/posts/SzYi69n17pd](https://plus.google.com/+ArielYahni/posts/SzYi69n17pd)  I took some pictures of how the tube lens was before cleaning, I would say it was a little dirty but not.So much did. Nevertheless after cleaning no beam coming from it



![images/acce7507b07c4aa64bebdf043027c156.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/acce7507b07c4aa64bebdf043027c156.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2017 03:22*

That's interesting the head on it. Is it removable with those grub screws? 



I think someone else suggested the other mirror (I think on the other end of tube?) may have been burned through. I've never really taken a good look at my tube, so not really sure how they work.


---
**Ariel Yahni (UniKpty)** *February 15, 2017 03:36*

Could be removable, not sure I want to touch it :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2017 04:39*

**+Ariel Yahni** I'd be feeling the same. Almost certain if I removed something like that it would be broken afterwards.


---
**Don Kleinschnitz Jr.** *February 15, 2017 04:50*

Am I right that you said the tube ionizes and draws current?

Has to be something wrong with the internal mirrors, warped or broken.

Any unusual thermal shock before this?


---
**Claudio Prezzi** *February 15, 2017 08:49*

The both mirrors (front and back) of a laser tube was adjusted very accurately during production. Thats what the grub screws are for. There will not be a beam emited or just very low power, if they are not absolutely parallel.



If your tube stopped emiting a beam whithout disajusting the head (by mechanical hit or something), then probably the mirrors are burned inside caused by overheating. In this case no chance to get it back.



But if you hit the head, there is a chance to readjust it to get operation back. But that's probably a job for a pro (with pro equipement).


---
**Bonne Wilce** *February 15, 2017 11:05*

what **+Claudio Prezzi** said, :(

 I have never seen a k40 tube with metal adjustable heads! where did you get it from?

Could you take more photos of the tube?

Do you have screw terminals? 


---
**Ariel Yahni (UniKpty)** *February 15, 2017 11:59*

**+Don Kleinschnitz**  yes it does ionizes and draws current, also i can see the change in intensity. No sudden change in temp, at least not more then the regular. Ambient temp here is about 30, so i us 5gal bucket and add some freeze packs, temp goes gradually down to 22 more or less. I dont start lasing until the temp changes


---
**Ariel Yahni (UniKpty)** *February 15, 2017 11:59*

My tube arrived broken in my K40 so i had to buy a new one, this is where i got it from [https://www.aliexpress.com/snapshot/7692615736.html?orderId=75409484049755&productId=32408231582](https://www.aliexpress.com/snapshot/7692615736.html?orderId=75409484049755&productId=32408231582)


---
**cory brown** *February 16, 2017 08:13*

I thought you got your replacement tube from lightobject? I'm not sure that your tube is a RECI but I thought this is interesting.  

[http://www.recilaser.com/en/newsInfo/ba9e70684e4c4244014e4d3c3ad60b35.htm](http://www.recilaser.com/en/newsInfo/ba9e70684e4c4244014e4d3c3ad60b35.htm)



The link you shared doesn't work for me.


---
**Bonne Wilce** *February 16, 2017 08:35*

Did not work for me too but I was on my phone.

Its an interesting design as normally the cheaper tubes are all glass and the metal ended ones do not start until around 80 to 100w

Although this design is odd because it looks like you still had to silicone it? No screw terminal?

Also the set screws 1 not many to provide real adjustments and 2 don't look like they are locked in place :o 


---
**Ariel Yahni (UniKpty)** *February 16, 2017 12:20*

Here is the updated link [https://www.aliexpress.com/item/40W-CO2-laser-tube-800mm-length-50mm-diameter-wooden-box-carton-foam-package-DHL-freeship-mini/32408231582.html](https://www.aliexpress.com/item/40W-CO2-laser-tube-800mm-length-50mm-diameter-wooden-box-carton-foam-package-DHL-freeship-mini/32408231582.html)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Nkt1mEVT6Xy) &mdash; content and formatting may not be reliable*
