---
layout: post
title: "And it's ready, well almost. Times up for today"
date: December 16, 2015 01:57
category: "Modification"
author: "ChiRag Chaudhari"
---
And it's ready, well almost. Times up for today. Next up, make it stepper driven adjustable Z Bed!  

![images/3ae19a68ee7db8bd6204ddb6c29fab24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ae19a68ee7db8bd6204ddb6c29fab24.jpeg)



**"ChiRag Chaudhari"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 09:59*

Okay, I see you've already soled the corner issue :) Looks great. Can't wait to see the finished product.


---
**ChiRag Chaudhari** *December 16, 2015 17:19*

**+Yuusuf Sallahuddin** Well that is just temporary. Its just a electric tape. I am sure I can do better than that, ha ha. The expanded sheet was ~$8 for 24" x 96" and the Aluminium (yeah i dont write Aluminum) Channel was ~$7. I think am going to try the JB-Weld and see how good it works.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 19:05*

**+Chirag Chaudhari** Haha, good old electrical tape. I thought it was some metal corner cap or something. Those prices are pretty decent for the expanding sheet & the aluminium. I might have to get myself some too. Did you source them locally or online?



Also, just looked up JB-Weld as I've seen a lot of Instructables mention using it also & I didn't know what it is. Supposedly it is "The World's Strongest Bond", so I'd imagine that it'll do a pretty good job.


---
**ChiRag Chaudhari** *December 16, 2015 19:38*

**+Yuusuf Sallahuddin** Forgot to mention, got sheet metal from HomeDepot and Aluminium channel from another small local hardware store. 



I remember using JB-Weld a few years ago, to fix rusted muffler of a old honda. and it worked pretty damn well.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 19:54*

**+Chirag Chaudhari** Unfortunately no HomeDepot & the likes here in Australia. Will have to search around elsewhere, cos that stuff looks like it will make a better table to engrave/cut on (more air holes) than what I am currently using.


---
**ChiRag Chaudhari** *December 16, 2015 20:16*

**+Yuusuf Sallahuddin** More holes, that is exactly the reason I went for it. There was another one too, bit thicker and a lot stronger. Bigger diamonds but the metal part it bit wider too. I think they market it as BBQ Grill replacement, about the same price but two 12" x 24" sheets. 



This one is not so strong, but for 12 x 14 size it does pretty good job. But if I want to put some heavy stuff then I will have to use the other thicker kind that I bought.



I have seen few people using Epilog Mini Helix laser to cut the lather, but they dont use the honeycomb bed. They use another metal sheet on top of it, which is perforated (lot less holes than honeycomb bed). But I think that is to keep the lather in place. See this link here: [https://www.epiloglaser.com/products/legend-laser/legend-vacuum-table.htm](https://www.epiloglaser.com/products/legend-laser/legend-vacuum-table.htm)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 21:24*

**+Chirag Chaudhari** I am currently using a piece of perforated galvanised steel board. It does the job well, and is great for helping hold the leather in place as it is floppy & likes to move with the air assist if I don't use magnets to hold it in place. Seems similar to the epilog one you posted. I think a vacuum setup would be awesome to hold the leather in place too, as it would also remove the smoke from beneath the leather (which currently has to swirl its way to the top extractor fan).


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/Spaz2rnFZFa) &mdash; content and formatting may not be reliable*
