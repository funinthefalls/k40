---
layout: post
title: "Etched a game board, but my eyeballing of the preview square was a little off"
date: June 26, 2016 04:48
category: "Object produced with laser"
author: "Tev Kaber"
---
Etched a game board, but my eyeballing of the preview square was a little off. I need to get some rulers/guides in my laser area or something to be more precise about where I etch.

![images/3f7ee181b0403023cfefe4097c9c5a4c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f7ee181b0403023cfefe4097c9c5a4c.jpeg)



**"Tev Kaber"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 05:10*

You'll get the hang of it eventually. I have one main jig cut out of plywood that pushes up against the back wall & left rail. I've then cut out along the Refer-X = 0 & Refer-Y = 0 so that I've got an L shape. I then use secondary jigs that push up against the 0,0 point (on the main L-shaped jig) to align objects for the cutting/engraving. A simple method for the time being would be to lay masking tape along the entire bed & cut lines every 10mm or so on the X & Y axis. Then you'll effectively have a grid to work off.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/Sc2AbvrdZpK) &mdash; content and formatting may not be reliable*
