---
layout: post
title: "Does any one have a schematic of the stepper motor that comes with the lightobject z table"
date: August 06, 2017 01:57
category: "Modification"
author: "William Kearns"
---
Does any one have a schematic of the stepper motor that comes with the lightobject z table. See the attached picture of the included connection. 

![images/bdb6a01101ee26cc4a2f3d2f96c76d98.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bdb6a01101ee26cc4a2f3d2f96c76d98.png)



**"William Kearns"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2017 02:49*

We have step by step instructions now on the documentation site for how to wire a z bed and or rotary with external drivers and separate psu. 


---
**William Kearns** *August 06, 2017 03:05*

**+Ray Kholodovsky** thanks ray I just downloaded the PDF. I'm also trying to figure out what the pin out is on the (Motor: 42HS03 2A 2 phase 4 wires) what each color is supposed to be?


---
**Ray Kholodovsky (Cohesion3D)** *August 06, 2017 03:06*

Just keep the wires in that order and plug them into the tb6600


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 03:30*

Yeah, that header would already be in the proper pair order. One pair per coil. Doesn't matter which is A and which is B. Just plug it in.


---
**William Kearns** *August 06, 2017 03:43*

awesome thanks guys 


---
**Don Kleinschnitz Jr.** *August 06, 2017 11:59*

If you still need to know pairs,  you can tell simply by progresively shorting two leads together and turn the motor shaft. When you have a pair the motor will be harder to turn. 


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 14:03*

Or use a multimeter and check for continuity. 


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/AvCYhaeZE5s) &mdash; content and formatting may not be reliable*
