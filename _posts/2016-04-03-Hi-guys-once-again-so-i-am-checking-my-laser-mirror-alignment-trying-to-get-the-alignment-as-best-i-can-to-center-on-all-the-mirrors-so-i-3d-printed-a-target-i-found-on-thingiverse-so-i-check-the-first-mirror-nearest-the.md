---
layout: post
title: "Hi guy's once again, so i am checking my laser mirror alignment trying to get the alignment as best i can to center on all the mirrors so i 3d printed a target i found on thingiverse so i check the first mirror nearest the"
date: April 03, 2016 18:58
category: "Discussion"
author: "Dennis Fuente"
---
Hi guy's once again, so i am checking my laser mirror alignment trying to get the alignment as best i can to center on all the mirrors so i 3d printed a target i found on thingiverse so i check the first mirror nearest the tube found it out so i aligned it as close to center as i could get it and now when i check the next mirror in the chain it's way out the beam isn't even hitting the mirror so now i am going crazy trying to figure out what the problem is this nut's.



Dennis  





**"Dennis Fuente"**

---
---
**HP Persson** *April 03, 2016 19:30*

Align the first mirror so you hit the 2nd mirror spot on, if you miss the 2nd mirror you have either a 1st mirror mis-alignment, or the tube itself is not level with the machine internals.




---
**HP Persson** *April 03, 2016 19:35*

Also, check out this PDF. Can´t remember where i found it, but a well written guide for alignment.

(if anyone got the official link, let me know and i´ll edit my link)



[http://wopr.nu/laser/k40-alignment.pdf](http://wopr.nu/laser/k40-alignment.pdf)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 19:36*

Okay, I recall specifically when I first received my machine there was major issues with alignment. It was almost impossible to get the 2nd mirror in the centre (at back & front) or anywhere near centre on the 3rd mirror (at all 4 corners).



It turned out that the first mirror, the mount that it sits on had some issue with not being level with the y-axis rail. So, basically mine was pointing down. So when it hit the 2nd mirror at the back of machine, it could be perfect centre. But when I moved to the front, it would hit nowhere near centre.



So after finding that, I padded the bracket that holds the first mirror underneath with some spacers (just a few random washers of the right thickness) to level it out.



I suggest checking this sort of thing first. Start with Mirror 2 at the back & adjust Mirror 1 until you hit perfectly in the centre of Mirror 2. Then move Mirror 2 to the front & check what is causing the issue. Is it too high/low, is it to the left/right. If nothing you do fixes it, then possibly your Mirror 1 is not level or even the laser tube needs a level adjustment.


---
**Dennis Fuente** *April 03, 2016 19:44*

Ok if i align the first mirror to hit the next mirror on the gantry the beam from the tube is way off center that dose not make sense and now i am checking the machine square and it's way out of wack man this is a mess i

may leave it for now and come back to it if and when i figure it out.



Dennis  


---
**Dennis Fuente** *April 03, 2016 19:48*

Yuusuf 

Ok i am trying to think through all this but i find more and more out of wack where do i start should i square the tube to the machine first and then to the first mirror and then on to the next wow it's bad



Dennis  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 03, 2016 20:30*

**+Dennis Fuente** I am not certain how you go about all of that, as my machine was fortunately quite square. However a couple of others here have had the machine all bent out of shape & had to square it all up first.



I'd suggest take the gantry all out (there is just 4 bolts holding it in) & check it is square, adjust as necessary. Then the laser tube I am not really certain, but would be worth checking it is level to the 1st mirror's centre. I actually removed all my mirrors & just put tape on the wall where the laser would hit (behind mirror 1) to see where it hits. Then I added mirror by mirror & adjusted bit by bit. I will have to do it again in the next few days, as I've got my machine totally disassembled at the moment.



Hopefully one of the others who has experienced the tube being out of level & the gantry being unsquare can give some extra thoughts.



**+HP Persson** Wow, that is an extremely well written guide. They should ship it with the K40s lol.


---
**Dennis Fuente** *April 03, 2016 22:20*

Ok so i took the machine completly apart and found the tube out of alignment then found gantry frame bent oh and what a joke the forward stop was a piece of tubing now i have a 12x12 machine took out the bed found the gantry frame not square with the machine because the bolts that were used to assemble the gantry to the bed were forcing the gantry side way's so i got all this fixed and as square as i can make it with out making a whole new machine maybe that's what i should have started with anyway now back to aligning the mirrors should the mirror next to the tube be centered or don't worry about that just make the beam centered on the next mirror on the gantry centered  the one before the cutting head thanks for the info.



Dennis   


---
**Dennis Fuente** *April 03, 2016 22:48*

Yes I  went  with  laser draw


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 05:50*

**+Dennis Fuente** I made the beam of the laser hit the 1st mirror pretty much spot on in the centre. I found that if it wasn't centre, I was having issues hitting the 2nd mirror in centre (at both front & back). If you can get it to hit the 2nd mirror in centre for both spots, then I wouldn't be too concerned with hitting the centre of the 1st mirror.


---
**Dennis Fuente** *April 04, 2016 16:54*

Yuusuf 

So far if i make the beam hit the first mirror dead center the beam dose not even come close to hitting the next mirror but from every video i have looked at they all have the beam hitting the center on all the mirrors so i don't knwo what gives with mine as yet working on it today thanks for the help.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 17:53*

**+Dennis Fuente** My suggestion is take a piece of paper & place it somewhere close to where the beam exits the hole (from Mirror 1) to see where it is hitting (too high or too low or too far left or too far right). Then you can see which direction you need to adjust it to. Your Mirror 2 on the Y-rail must not be in line with where the beam is going, so maybe when you straightened the gantry it caused it to not line up any more.


---
**Dennis Fuente** *April 04, 2016 19:28*

Yuusuf

I fianly got the mirror to hit on the target i moved the gantry to the middle of the machine and adjusted the mirrors until it hit mid target, next i opened up corel laser to try and cut a part out of 3mm ply but when i opened corel it some how lost the tool bars i finaly got those back and i am able to open a file again anyway i tried cutting the file out of the ply at 4MA and 10 MmPS it won't cut the ply how many passes are nescessary to cut 3MM ply and at what power setting's 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 20:19*

**+Dennis Fuente** I haven't cut ply yet. I have some sitting downstairs that I got to do some tests on, but haven't got around to it yet. However, I have cut 3mm mdf & just earlier I managed to cut it @ 2 passes, 5mA @ 30mm/s. For leather, I've cut it at 7mA @ 7mm/s in 1 pass.



For ply, I'd imagine something around 5-7mA power for a start, then try 10mm/s. Should be able to get through it in 1-2 passes. Others here manage to cut ply with reasonable ease.


---
**Dennis Fuente** *April 04, 2016 21:07*

Yuusuf 

Yes i have read others get through ply in just one pass i can get through the ply in one pass but at a high power setting someone here said that's not a good idea as it's bad for the tube could it be that my mirrors are bad one of the is heavly scracthed , so i guess i will have to try  an experament

the next thing is the table the one that the machine comes with is junk.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 21:27*

**+Dennis Fuente** Yeah I imagine the mirror being scratched might not be very helpful. The higher the power you use on the tube the quicker it will die as far as I understand. Although I wonder if doing 2 passes at 5mA is the same as doing 1 pass at 10mA (in terms of how much quicker the tube dies).

You're right that the table it comes with is junk. I ripped it out pretty much straight away. Until you have something to replace it with, it does the job at least.


---
**Dennis Fuente** *April 04, 2016 21:32*

So i did a search on the forum here for the focal length of the machine and found that it is 50.8 MM so i made a precise block on my CNC mill and then measured my machine bed to lens distance it's no where close to this now i am wondering what gives here so i guess i will have to make blocks and see what happens when it's the right height would be nice to know if these figures are correct

Dennis


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 22:33*

**+Dennis Fuente** 50.8mm from the lens is correct for the stock lens. After I ripped my original cutting bed out & replaced, I was 4mm lower than the original bed. It was causing no end of problems with cutting things. As soon as I raised it (by adding a couple of magnets to the top of the stand-offs) my cutting was pretty decent again.


---
**Dennis Fuente** *April 04, 2016 22:41*

wow this started out to be what i thought a nice tool to have now it's turning into a major project i did the same thing with my 3d printer 

the bed thing on this machine is a big headache i don't want to make it a big deal and spend lot's of money on it just functional would be just fine

Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 03:33*

**+Dennis Fuente** Yeah I thought the same. But I only spent about au$50 to get mine to a functional point. Just purchased a piece of perforated galvanised steel (to use as a replacement cutting bed) & a cheap air pump (to use for air assist). I'm reasonably happy with mine now & it is functional. Could be better, but that's when it costs more money to improve it for what you need.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 07:50*

**+Dennis Fuente** I've just done some tests on 3mm ply. I can cut through it @ 10mA @ 12mm/s speed in 1 pass. Hope that helps with your cutting attempts.


---
**Mishko Mishko** *April 05, 2016 15:36*

I'd never do more passes than absolutely necessary to cut through just to preserve the tube. At an estimated lifetime of 1000-1500 hrs, that amounts to say 10 cents/hour, so no matter if you use it for business or fun, the tube costs probably less than the electricity used. The tube will die eventually no matter if you use it or not, and 1000 hours is plenty. If used for business, the above is a no brainer, and even for a hobby, just use it to its max, just my two cents...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 15:58*

**+Mishko Mishko** Thanks for sharing the estimated lifespan. I had seen something somewhere & misremembered it as having an extra 0 on the end. So 1000 hours, not too bad in my opinion. As you said, at ~10c/hour, that's pretty reasonable & definitely cheaper than the price per kW here in Australia (~28c/kW).


---
**Dennis Fuente** *April 05, 2016 16:37*

Thanks guys i will do some test today and see how it goes



Dennis


---
**Mishko Mishko** *April 05, 2016 18:09*

That's what I've found on most vendor sites, I might be wrong, but that seems like a reasonable lifespan  to me. On the other hand, the tube will deteriorate just sitting on the shelf, so my approach is use it while it lasts, preferably for something useful;)


---
**Dennis Fuente** *April 05, 2016 18:15*

Hi guys so i tested the machine this morning on 3MM ply this is just cheap door skin ply 3 layer nothing special i found the slower i set the machine the more charing around the cut so my best result was @ 10 MM s and a power setting of 20 MA to cut through the ply on one pass what do you think this is with a completly stock machine no air assist just as it came to me thanks for the help.



Dennis 


---
**Dennis Fuente** *April 05, 2016 18:19*

OH i forgot to mention that i took the focal lens out and swaped it around to see if it made a difference i could not see a difference in the cutting.

One other thing that keeps happening is i loose the tool bars in corel any ideas on this.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 06:28*

**+Dennis Fuente** The toolbars always disappear in Corel. You will find an icon down near the clock that can access all the functions for engrave/cut etc.



I was using 12mm/s & power setting of 10mA, but I have air assist (basic air pump like from a fishtank) pointed directly at the cut area, so minimal charring.



Can you get through the ply in 1 pass @ 12mm/s & 10mA at all?


---
**Dennis Fuente** *April 06, 2016 16:29*

no the machine will not cut all the way through the ply unless i double the power to 20 MA and 10 MMs the mirrors are right on the money at the center of gantry travel so i realy don't know what could be causing the issue i have read and looked for an answer to the problem but no luck or change would air assist make a 10 MA change thanks for the info on corel 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 16:42*

**+Dennis Fuente** I'm fairly certain air-assist won't make that dramatic a change on the cutting power. It minimises the scorching of the edges & the dirtying of the lens. It seems odd that there is a 10mA difference between my cutting & yours. Although I just had a thought, you are still using the stock cutting bed right? If so, that could be part of the issue as it is a solid metal area (except the hole/clamp region). I've noticed differences in my ability to cut since I replaced the stock cutting bed with what I am currently using, and I am certain if I was to replace it again with something like the honeycomb, I would see slight improvements again (due to less metal touching the underside of the piece).


---
**Dennis Fuente** *April 06, 2016 19:23*

Oh that's interesting i have my bed removable at the moment but don't have anything to replace it as of yet, so what happens is it the that the beam can't go through because it hit's the metal 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 19:45*

**+Dennis Fuente** The beam can go through, but it does hit the metal & you get reflection to some degree on the bottom of the material you are cutting. Which causes extra discolouration/char from the base of the material. I just notice that on my bed which is say 5mm steel, then 5mm hole, then 5mm steel, etc... where the steel is the material sometimes is still attached partially, in comparison to where there was no steel. Not sure exactly what is going on, but from what I understand, the minimum you can have supporting the material you are cutting the better. One guy (HP Persson) posted some pics of a bed he created using a piece of acrylic with nails poking through it to support the material on the tips of the nails. Another guy (can't remember who) mentioned that he uses magnets & some long screws. He can then move the screws around on the base of the case using the magnet to secure it in place, then position things as he needs. You could give that a test to see if you get any better results.


---
**Dennis Fuente** *April 06, 2016 23:33*

Wow thanks for the idea of magnets i may have something already that would work i will give it a test 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/9DVfFStAdBM) &mdash; content and formatting may not be reliable*
