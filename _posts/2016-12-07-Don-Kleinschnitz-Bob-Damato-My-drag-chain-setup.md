---
layout: post
title: "Don Kleinschnitz Bob Damato My drag chain setup"
date: December 07, 2016 12:56
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
**+Don Kleinschnitz** **+Bob Damato**



My drag chain setup.

Looks like it is actually from the front left screw of carriage plate, not the back centre screw like I originally thought.







![images/7dfaba33d9d5fd1d5c56393da20ff497.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7dfaba33d9d5fd1d5c56393da20ff497.jpeg)
![images/306ebfba53262ca5ac3d37c6cc904c06.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/306ebfba53262ca5ac3d37c6cc904c06.jpeg)
![images/6de65007156bfb10a572840b8b589d52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6de65007156bfb10a572840b8b589d52.jpeg)
![images/ae44f1eafec47c4fe9cacb8fd9b26434.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae44f1eafec47c4fe9cacb8fd9b26434.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Don Kleinschnitz Jr.** *December 07, 2016 13:41*

So you have an X loop on the gantry and just let the Y loop run down the side of the gantry? Is there another drag chain along the right bottom side?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 14:00*

**+Don Kleinschnitz** No other drag chain. Just the one above the X-rail. Not really sure what you mean by X loop & Y loop.


---
**Don Kleinschnitz Jr.** *December 07, 2016 14:13*

**+Yuusuf Sallahuddin** the drag chain only goes left-right (X). What managed the cables up and down (Y)? Do they just lay on the floor?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 07, 2016 15:18*

**+Don Kleinschnitz** The cables on the Y-axis just feed through that hole into the electronics bay. There is enough slack on them to allow it to go from centre of Y (around where the hole is) to top of Y or bottom of Y. I'm assuming by "up and down" you are referring to "front and back" of the machine?


---
**Stephane Buisson** *December 07, 2016 16:34*

Personally I mounted it the other way around, like that never in the ray path :


{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[youtube.com - Smooth K40 conversion 1st steps](https://youtu.be/0r5sdS8zqY0?t=38)




{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[https://youtu.be/0r5sdS8zqY0?t=67](https://youtu.be/0r5sdS8zqY0?t=67)


---
**Don Kleinschnitz Jr.** *December 07, 2016 16:49*

**+Stephane Buisson** thats a rediculously good idea .... duh!


---
**Ashley M. Kirchner [Norym]** *December 07, 2016 21:49*

Mine sits above the rail ... never gets in the beam's path either.

![images/47f9a7e92c9211b22caeff2596e0df12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47f9a7e92c9211b22caeff2596e0df12.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 08, 2016 07:13*

**+Ashley M. Kirchner** That's a good method too. I tried that on mine & for whatever reason it was slapping the roof of the machine when it moved to the far side (right).


---
**Ashley M. Kirchner [Norym]** *December 08, 2016 07:15*

That's because you weren't using one with a tight radius. This is a 7x7mm chain, and I have a good 15-20mm clearance before it hits the cover with the gantry all the way forward where it's closest to the cover.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 08, 2016 07:17*

Mine's a 7mm chain too. Probably something to do with how/where I was trying to mount it on the right of the rail. Maybe a bit high.



edit: actually, you have nice end attachment pieces that hold it properly. I just dodgy'd up a solution with a bit of plywood haha.


---
**Ashley M. Kirchner [Norym]** *December 08, 2016 08:06*

Custom designed and 3D printed plate. :)


---
**Don Kleinschnitz Jr.** *December 08, 2016 14:25*

 **+Ashley M. Kirchner** design is ideal but I am not sure that I can get all my "stuff" through a 7mm chain. I have plans for a auto focus head later on (sure:)!.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 08, 2016 15:53*

**+Don Kleinschnitz** As you notice with mine, I managed to fit 1x 8mm OD airpipe through the chain & had to zip tie the other to the chain since I couldn't fit it in. Best to go with a bigger size if you need more space.


---
**Ashley M. Kirchner [Norym]** *December 08, 2016 19:03*

On my 7x7 chain, I have a 5mm tube through it with plenty of room for wires. Several pairs of wires for that matter. I can run a CAT5 cable through it, alongside the air tube. That gives me 4 pairs of wires. Easy.


---
**Don Kleinschnitz Jr.** *December 10, 2016 14:43*

Update: thanks for all the help from this group **+Bob Damato**. 



I ended up keeping the approach I started with including some modifications. 

.... added a sheet metal plate under the screws on the chains head end attachment. The screws wanted to pull thought the chain.

.... removed some links and now at 24 links not including mounting links, this got the chain out of the beam path.



I ordered a 7mm x 7mm chain to try out a laser cut version of  **+Ashley M. Kirchner**'s  approach.



I tried **+Anthony Bolgar**'s clever approach and had binding problems, perhaps I am using a bigger chain. 



[photos.google.com - New video by Don Kleinschnitz](https://goo.gl/photos/oj9kzdcN69Qv9TTc6)



..................

All in all, so far, I am pleased with its operation and how it cleaned up my cabling to the gantry. I was previously using a shop made "coily cord" but was having problems with it not having repeatable tension.



I also updated the blog post: [http://donsthings.blogspot.com/2016/11/k40-gantry-connection-management.html](http://donsthings.blogspot.com/2016/11/k40-gantry-connection-management.html)



Note: the movement is jerky because I am using the jog function from the panel.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/e6S2ZjN7de7) &mdash; content and formatting may not be reliable*
