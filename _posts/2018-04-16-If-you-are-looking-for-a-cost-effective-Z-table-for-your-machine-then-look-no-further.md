---
layout: post
title: "If you are looking for a cost effective Z-table for your machine, then look no further!"
date: April 16, 2018 09:45
category: "Modification"
author: "Ian Hayden"
---
If you are looking for a cost effective Z-table for your machine, then look no further!  I found on Amazon (US) though no doubt there are other vendors - called a Lab Jack - scissor lift.   Most of them are (only) 100mm x 100m, but managed to find a 200mm square one.   [https://www.amazon.com/gp/product/B073Z7BD5B/ref=od_aui_detailpages00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B073Z7BD5B/ref=od_aui_detailpages00?ie=UTF8&psc=1)



Am going to have to modify it, and my my K40 slightly so that i can adjust the the height outside of the laser area.  possibly just one of those flexible hexagonal screwdriver extenders.    At $30+  it will pay for itself in seconds.    They do have the motorised versions, but at a hike in cost









**"Ian Hayden"**

---
---
**HalfNormal** *April 16, 2018 12:21*

Take a search on the site. A lot of this have used it one time or another. There is a lot of good information if you look on how to modify it for use.


---
**Jim Fong** *April 16, 2018 14:33*

They work but kinda flimsy. I use a round bubble level placed on the material. The top tilts a few degrees so you have to make sure it is level each time you adjust it. 


---
**Anthony Bolgar** *April 16, 2018 20:31*

The other issue is that the screw to adjust the height is in the middle so it goes up and down as well. Makes it hard toi have it go through the front of the case, you need to make an elongated slot. If you can find one that has the screw at the bottom, it stays in the same spot vertically.


---
**Ian Hayden** *April 16, 2018 23:48*

**+HalfNormal** yup - you're right - a lot of info there.  I didnt know they ware called Lab Jacks -  when i looked through this community i was looking Z-table, so missed all these helpful posts.  


---
**Tim Rennels** *April 17, 2018 20:57*

If you have the k40 that's on wheels, there isn't enough room as this thing is 3" collapsed. I bought one.




---
*Imported from [Google+](https://plus.google.com/111645426566096956650/posts/LttXYbyKmXG) &mdash; content and formatting may not be reliable*
