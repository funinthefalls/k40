---
layout: post
title: "It's that time of year :). Made a new Christmas ornament from 1/8\" Alder with a poly finish"
date: November 18, 2017 22:56
category: "Object produced with laser"
author: "Ned Hill"
---
It's that  time of year :).  Made a new Christmas ornament from 1/8" Alder with a poly finish.  The shape is the 47th problem of Euclid, which has Masonic meaning.  



![images/fc8ae61fc94cf60ba31d17245134ad9c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fc8ae61fc94cf60ba31d17245134ad9c.jpeg)
![images/1b1d6d48fd87a716118c798e89c404c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b1d6d48fd87a716118c798e89c404c6.jpeg)
![images/09badcdf3984366ee949f74899c2619e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09badcdf3984366ee949f74899c2619e.jpeg)

**"Ned Hill"**

---
---
**Mike Meyer** *November 18, 2017 23:17*

I'm just guessing here, but the 47th problem of Euclid might mean something like, "outstanding k40 laser project".  Sadly for me, I'm 46 behind. 


---
**HalfNormal** *November 19, 2017 01:00*

If I read the information on the interwebs correctly, it means you are a square!


---
**Ned Hill** *November 19, 2017 03:22*

Square by virtue :)


---
**Travis Sawyer** *April 01, 2018 20:30*

Eureka!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/NVVvvEErW9V) &mdash; content and formatting may not be reliable*
