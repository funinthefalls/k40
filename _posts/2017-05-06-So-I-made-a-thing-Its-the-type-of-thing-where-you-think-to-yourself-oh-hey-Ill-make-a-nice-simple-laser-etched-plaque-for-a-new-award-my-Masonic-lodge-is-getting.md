---
layout: post
title: "So, I made a thing :) Its the type of thing where you think to yourself, oh hey Ill make a nice simple laser etched plaque for a new award my Masonic lodge is getting"
date: May 06, 2017 00:12
category: "Object produced with laser"
author: "Ned Hill"
---
So, I made a thing :)  It’s the type of thing where you think to yourself, “oh hey I’ll make a nice simple laser etched plaque for a new award my Masonic lodge is getting”.  Next thing you know it becomes something that requires you to get a thickness planer to accommodate the evolving artistic vision.  :P  But that’s part of the fun and how you grow as a maker.



Details- Overall it’s 13-3/4”(h) x 10”(w) (34.7 x 25.5 cm). The center section is made from ¼” Alder finished with 2 coats of brush on gloss poly.  The frame is made from pieces of scrap walnut that I re-sawed and planed to the right thickness.  The sides are ¼” thick with laser etched grooves and the corner blocks are 3/8” thick, with Maple wood veneer inlays (first time doing wood inlays), and finished with 2 coats of wipe on poly.  The pieces are all glued to a piece of stained 1/8” birch ply.



The center graphic is a mash up of several antique etchings.  The lion, column and shield part is actually from the printer’s mark for a 16th century German book maker.  It had a different design on the shield that I edited out and then inserted the Square and Compass. 

All in all, I’m very pleased with how it came out.  :)





![images/1407e3515077d311fb8cb30abff3bd14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1407e3515077d311fb8cb30abff3bd14.jpeg)
![images/ad4a6158ac55d07ba019d033aa7254e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad4a6158ac55d07ba019d033aa7254e8.jpeg)

**"Ned Hill"**

---
---
**Jim Fong** *May 06, 2017 00:15*

That's some really nice work. 


---
**Scott Lewis** *May 06, 2017 00:49*

Very nice work


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2017 01:33*

Wow, you're just continually raising the bar Ned. Great results.


---
**Ned Hill** *May 06, 2017 01:36*

Thanks  :)


---
**Alex Krause** *May 06, 2017 01:41*

Use a router table and a fence to groove the frame? Or did you laser it?


---
**Ned Hill** *May 06, 2017 01:48*

I just laser it.  I could have used my router table but I was starting to get a little impatient to get this thing made and lasering was quicker for me. :)  

I'm going to reuse the frame design for other things and I want to look into try doing to joinery for the frame.  Thinking about doing essentially some biscuit joints with a thin slot bit on my router table.  May have to make the sides a bit thicker though.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2017 02:05*

You could maybe do some simple spline joints by cutting a groove in your joints with a circular saw or table saw. Then wedge in a spline. Something like this:



[http://cdn3.craftsy.com/blog/wp-content/uploads/2015/02/spline-examples.jpg](http://cdn3.craftsy.com/blog/wp-content/uploads/2015/02/spline-examples.jpg)


---
**Ned Hill** *May 06, 2017 02:07*

That's good idea, I like that.  Thanks :)


---
**Ned Hill** *May 06, 2017 02:15*

Actually now that I think about it, the joints aren't  going to be at the outside corners so I don't think that would work.  Unless I could cut some slots on the outside edges straight in with my router maybe.  Or better yet I could do an inside corner spline joint with the slot bit.

Also was thinking about possibly doing some butterfly/bowtie key inlays on the backs to join them.


---
**Ned Hill** *May 07, 2017 15:20*

Here's what I'm thinking now.  Use a router slot cutting bit to cut slots through the butt joint ends.  Then laser cut an L-shaped spline (see red lines in pic).  Shrink the frame size slightly then cut rabbits along the inside edge from the back and insert the middle panel.  Once everything is glue in it should be rock solid.  I'll also have to increase the thickness of the frame elements a bit to accommodate all this.

![images/3fe372506f806d7c4dab0fedc3157f3c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3fe372506f806d7c4dab0fedc3157f3c.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2017 15:36*

Looks like a decent idea. What you could also do is have your middle panel (I'm assuming it's 1/8") slot into grooves in all of the framework too.


---
**Nigel Conroy** *May 09, 2017 13:40*

Very nice **+Ned Hill**



I like the idea of using the laser to etch the groves instead of the router. 



You need a Jointer to go with the thickness planer... 



Interested in how the inlays were done.



Did you engrave the inlay design then cut the veneer shape and lay it in?




---
**Ned Hill** *May 09, 2017 14:06*

Thanks **+Nigel Conroy**.  

A jointer would be nice, and I may get one eventually, but in the meantime I  can joint with a planer using shims and hot gluing it level to another piece of wood.  There are also apparently leveling jigs you can make to do this as well.



For the inlays I didn't worry with trying to adjust for the laser kerf.  I just engraved, cut the veneer and glued them in.  If the inlays are a little proud of the surface I recommend using a sharp chisel or block plane to make it flush.  I found on a test piece that sanding them flush can cause some of the darker walnut dust to get embedded into the lighter wood veneer.    Or at least sand very carefully.


---
**Nigel Conroy** *May 09, 2017 16:47*

I picked up a great jointer off craigslist for $50.

Yes there are some jigs to help. 



Thanks I will look into the veneers and inlay as I really like the effect. 


---
**Ned Hill** *May 09, 2017 17:02*

Great idea, I'll put up a standing search on craigslist for one.


---
**Robert Selvey** *May 09, 2017 23:49*

**+Ned Hill** What do you use for a water chiller?




---
**Ned Hill** *May 10, 2017 00:27*

**+Robert Selvey** I use the old 5 gal bucket, water pump and frozen water bottles set up.  I change the water about once a month using distilled water.


---
**Don Kleinschnitz Jr.** *May 10, 2017 11:55*

**+Ned Hill** magnificent!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/VyXVanMLmSH) &mdash; content and formatting may not be reliable*
