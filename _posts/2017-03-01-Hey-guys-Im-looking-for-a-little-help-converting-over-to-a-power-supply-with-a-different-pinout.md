---
layout: post
title: "Hey guys... Im looking for a little help converting over to a power supply with a different pinout"
date: March 01, 2017 23:24
category: "Modification"
author: "Frank Dart"
---
Hey guys... Im looking for a little help converting over to a power supply with a different pinout.  I think I have most of them but really confused with a few and the fact that the one Im putting in has an extra pin doesnt help lol...

The top (blue) is the bad ps coming out and the bottom (red) is the one I need to wire in.

![images/7b694b9ec6fde0dcb53b8c36d9a9cab2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b694b9ec6fde0dcb53b8c36d9a9cab2.jpeg)



**"Frank Dart"**

---
---
**Frank Dart** *March 01, 2017 23:25*

better pic of the ps coming out

![images/7d0513d97e4d2a3a8b506922c1648e0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d0513d97e4d2a3a8b506922c1648e0f.jpeg)


---
**Frank Dart** *March 01, 2017 23:27*

and the one going in

![images/97217ed5a83ce0481e75087749202707.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97217ed5a83ce0481e75087749202707.jpeg)


---
**Madyn3D CNC, LLC** *March 01, 2017 23:32*

what models are those LPSU's?


---
**Madyn3D CNC, LLC** *March 01, 2017 23:36*

![images/20b0ca372143a5b5e371cf41225c4662.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/20b0ca372143a5b5e371cf41225c4662.jpeg)


---
**Madyn3D CNC, LLC** *March 01, 2017 23:37*

![images/f7e833934173ad5a65ec74506c51e043.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f7e833934173ad5a65ec74506c51e043.png)


---
**Frank Dart** *March 02, 2017 00:17*

argh... schematics make me dizzy lol..

No model on the red one other than hy40 on the flyback.

Will have to take the cover off the blue one in a bit to see if anything else to identify on that, but it is from one of the newer style k40s with the digital panel and wheels.

Like I said, Im pretty sure Ive got most matched up correctly its that 7-10 that really has me boggled.


---
**Mark Brown** *March 02, 2017 01:23*

I got the blue one in a non-digital machine. I was poking around in it with a multimeter the other day.  So these are my findings.



1 From Ammeter (ground I suppose, didn't check)

2 Main Ground (chassis)



3-4 Main Input (from power switch)



Panel Controls:

5 Gnd. to Laser Enable Switch

6 Return from Laser Enable Switch (inline with door safety interlock)



7 From Test Fire Button

8 Gnd. to Test Button and Pot



9-10 Pot



To Control Board:

11: 24v (to steppers, I suppose)

12: Gnd.

13: 5v (powers control board)

14: I assume this is how the board enables the laser? It read 4.8v without the laser running.



Does that help you any?  I can actually look at the connections on the pot if you need.




---
**Don Kleinschnitz Jr.** *March 02, 2017 01:27*

**+Frank Dart** Go here:

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Laser%20Power%20Supply)



The pin outs of most supplies are in that post. Let me know if your need more help.



Is your other supply dead? I am collecting and analyzing the cause of dead LPS's for the community. If you want to donate it to the failure testing project I am happy to take it off your hands :).




---
**Paul de Groot** *March 02, 2017 01:29*

Yes pon 14 called L fires the laser when pulled to the ground. 


---
**Don Kleinschnitz Jr.** *March 02, 2017 01:29*

**+Madyn3D CNC, LLC** i noticed your picture. Just for you fun facts. The flyback holds a charge because there is a capacitor potted in that assy along with a voltage multiplier :).


---
**Frank Dart** *March 02, 2017 02:18*

You guys are great!!! Hopefully get it wired up tomorrow night... **+Don Kleinschnitz** its dead and 99.9% sure its the flyback. Once this ps is in and hopefully working, Im going to order the flyback for the dead one as a backup.  If it turns out this one doesnt work, Im just going to order a new ps and hold off for a bit on getting the flyback... money is a bit tight until that tax check comes in.

But if this one doesnt work, I'll gladly send it over to you as well as the blue one if it turns out not to be the flyback.

And I know your gonna warn me anyway, but yes I'm aware of the danger with the flybacks :)




---
**Don Kleinschnitz Jr.** *March 02, 2017 02:23*

**+Frank Dart** :) I was about to type "be careful" ....and you beat me to it. Do you have fly-back sources?


---
**Paul de Groot** *March 02, 2017 02:25*

**+Frank Dart** i get worried when you tell us that you're ordering multipe ps. **+Don Kleinschnitz**​ is investigating the issue and hopefully you don't have to order new ones ever when he gets to the root of this ill designed ps. It just needs som tlc and we should not encourage bad products to prosper but opt for sustainable products that last.😊


---
**Frank Dart** *March 02, 2017 02:56*

**+Don Kleinschnitz** lol... I had already found one for $30 and as luck would have it... its the same exact one you have linked on your site!  

**+Paul de Groot** no worries here... the ps im putting in now is a 2nd hand from one of the fb group I picked up on the cheap that I picked up when I first started my battle with the ebay seller (Amonstar and their aliases should be renamed A Monster)... So knowing this one is used and from an older model, I really dont expect it to last an extended amount of time, but since I already have it why not.  It should at least last until the slow boat delivers the flyback for my original ps and I certainly think its worth the $30 risk to fix it  <b>if</b> I already have a working one. 

If this old one doesnt work I dont want to spend money and wait a month for the fb and have nothing in the meantime, so if thats the case I would rather spend $90 and get a working new one in two days.

TLDR - I am trying hard to balance!


---
**Don Kleinschnitz Jr.** *March 02, 2017 03:23*

**+Frank Dart** I don't know how valid this flyback test is as I'm not exactly sure what's in the poting. I think it's a cap and diode multiplier. Give this a try. 



Get a 12 v supply the beefier the better.

On the HV secondary: 

Position the gnd lead ( this is different by supply type) near (1/8 inch) the HV lead. I taped them to the table.

On the primary, sometimes this is wire sometimes this is bolts, connect one side to the gnd on the 12v supply. 

Intermittently touch the +12v to the other side of the primary. 

The secondary should arc to ground.



This would probably not arc if there is a bad short, but not sure about just a few  windings. 

Try it and let me know. 


---
**Madyn3D CNC, LLC** *March 03, 2017 14:20*

Hi Don, thank you! I learned after the fact that it only holds the charge when in line with the caps,  and with the dangers of removing flybacks from old CCTV's I was under the impression that the coils in flyback can hold charge. That's actually something I learned in this group a little while back. 




---
**Frank Dart** *March 04, 2017 01:04*

**+Don Kleinschnitz** Hoping you can weigh in and help me out the rest of the way here...

The pic was marked out by someone on FB and is how I wired it up, but this is either not right or this PS has issues of its own.  

As soon as it gets power turned on, the test light switch lights up.  Nothing happening on the tube as far as I can see, but for all I know, the tube could have been killed with the old supply.

I went over your site and found the one link with the comparison of the different PS and am even more confused as my 1-4 (red supply)pins are slightly different, and am I wrong in thinking that 5 pin (red supply) should have something connected?

Definitely time for a pizza break... 



![images/37172b1768c39852cf7cf3b96f4dc7a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37172b1768c39852cf7cf3b96f4dc7a6.jpeg)


---
**Don Kleinschnitz Jr.** *March 04, 2017 04:40*

**+Frank Dart** its late I will look at this tomorrow for you ... this is a K40 right?


---
**Frank Dart** *March 04, 2017 04:48*

It is indeed... and thanks again! Im about to crash out myself. I'm thinking the the wire from the 7. on top should really be going to 5. K+ on the bottom? And if so do I need to bridge k- to one of the other grounds? 


---
**Frank Dart** *March 04, 2017 20:50*

Happy me!!! Amazing what one learns once they try reading things with a clear rested head!

Ran back over a bunch of the diagrams, and info from other posts and the wire from 7 was supposed to be to the K+ instead of the K-! 

Swapped it over, powered up and hit the test button on the panel... finally that beautiful glow I havent seen since before christmas!

Again... Thank you **+Don Kleinschnitz** and everyone else that either helps or posts their problems here so that we all get to learn a little something!




---
**Don Kleinschnitz Jr.** *March 04, 2017 21:01*

**+Frank Dart** so funny. I just completed the below drawing. It suggests that the K was wired wrong. 

I also updated my blog to make it more clear.

Curious; Is you "Laser Switch" on 'D" working ok. I had another user could not get that to work.





![images/537723ce8a94e3e1a77cc39282fa2ef7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/537723ce8a94e3e1a77cc39282fa2ef7.jpeg)


---
**Frank Dart** *March 04, 2017 21:02*

For anyone looking for the finally markup:



In the end, this is how mine was wired.

Be very cautious with 1-4, most documentation 

around shows that these match up directly, but 

mine did not, check the labels on the board!

Shown as top pin -> bottom pin

____

1-4

2-1

3-3

4-2

____

5-10

6-11

7-5

8-7

9-8

10-9

_____

11-12

12-13

13-14

14-15

XX-6* nothing connected to bottom pin 6


---
**Frank Dart** *March 04, 2017 22:07*

Sorry **+Don Kleinschnitz** I didnt see you had posted about the same time I had.

I have only done just a quick fire test so far just to see if things worked as I dont really have it wired up properly with real connectors yet or the cooling,etc.

Now that I know the lps and the tube work, Im going to get everything wired properly instead of just twisted on and covered with electrical tape.

So are you asking if the laser will still fire without the loopback on D?

I didnt have any actual safety switches on the original and can see it just a short loopback in my top picture labeled 5 and 6.


---
**Don Kleinschnitz Jr.** *March 04, 2017 22:35*

**+Frank Dart** Yes if you disconnect the D+/_ will it still fire with the Test button?




---
**Don Kleinschnitz Jr.** *March 04, 2017 22:54*

**+Don Kleinschnitz** Ok that is not good. That should be the interlock/safety circuit. That is exactly how the other "D" supply I worked with behaved. 

That means that your "Laser Switch" is not doing anything and you cannot install interlocks, water flow sensor etc. 

Can I ask where you got that supply from and do you have part #, any interface documentation etc?


---
**Frank Dart** *March 05, 2017 00:19*

I havent tried it without the loopback, and I got it 2nd hand from someone in the fb group that was parting out an old k40 the same time mine died,  so no documentation, but I'll certainly take a closer look and see if I can find a part #




---
**Don Kleinschnitz Jr.** *March 05, 2017 00:25*

**+Frank Dart** OMG I read my question to you as an answer  from you.... doing to many things at once :(


---
**Frank Dart** *March 05, 2017 01:13*

Take a break man, or at least cut or engrave something on your own machine and for the love of god turn off the internets so you dont feel compelled to save us!





Ok... if you're still reading this, it means you didnt take my advice.

You my friend, must be able to see the future...  

I did just try it and it fired without the loopback, it also fired using the test button with the power switch off... scary stuff man.



R1407779 is the only thing that resembles a part # on the board.



A blog link that may have something meaningful... or at least is the same supply he used in his build

[opensourcelaser.blogspot.com - Cheap Chinese Laser Engraver/Cutter Upgraded with Smoothieware Running on Azteeg X5 mini board](http://opensourcelaser.blogspot.com/2014/12/cheap-chinese-laser-engravercutter.html)



and another pic of mine... dont mind the shoddy wire job, as I said that was just rigged to see if it worked and will be wired properly.


---
**Frank Dart** *March 05, 2017 01:16*

the pic 

![images/2028dae06841546b7afac8ac03266f0a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2028dae06841546b7afac8ac03266f0a.jpeg)


---
**Don Kleinschnitz Jr.** *March 05, 2017 05:06*

**+Frank Dart** heh, cant see the future, just been there and done that . I noticed on the blog link that you sent me it did not look like that machine had a "Laser Switch".



We must not understand what the "D" really does.



I don't know why the "test" would work with the main power switch off unless the LPS is not connected on the correct side of the power switch? 

1. Where are the 2 AC wires connected to?



We never figured out what was wrong with "D" on the other one but I am willing to keep working with you till we do :).



Do you have a multi-meter and are you comfortable with it?

2.. read  volts on D+ to ground when the LPS is on

3. read volts D- to ground when the LPS is on



We might have to take a close up picture of the back and front of the board so I can see if I can trace the "D" circuit.


---
*Imported from [Google+](https://plus.google.com/116294342889465656724/posts/5nC35u1ZB6E) &mdash; content and formatting may not be reliable*
