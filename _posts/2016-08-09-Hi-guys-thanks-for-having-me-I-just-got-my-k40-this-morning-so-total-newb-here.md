---
layout: post
title: "Hi guys thanks for having me. I just got my k40 this morning so total newb here"
date: August 09, 2016 21:35
category: "Discussion"
author: "Jose Castellon"
---
Hi guys thanks for having me. I just got my k40 this morning so total newb here. I'm trying to figure out the ground lead in the back of the machine mine reads "ground wire joint". Do I need to connect it? And if so how and to what? Please help guys I wanna start using this thing already. 





**"Jose Castellon"**

---
---
**Alex Krause** *August 09, 2016 21:52*

That terminal is for a copper ground rod connection for a dedicated ground


---
**greg greene** *August 09, 2016 21:53*

Absolutely !  BEFORE YOU TURN IT ON  Ground that sucker to an independent ground rod.  As others have pointed out, the designers of this equipment made fundamental errors in their understanding of how electrical equipment needs to be grounded to be safe.  That ground lug is there to remedy that.


---
**Jim Hatch** *August 09, 2016 21:54*

Depends on the internal wiring. Check inside to see if the power supply feed wires from the connection for the mains cord is wired with all three wires (white, black, green or brown). Sometimes that green/brown ground wire is not attached to the grounding lug of the plug or it's cross attached to the common (white) or it's connected to that grounding post you see on the outside.



If it's properly wired on the inside and the cord is a good three wire electronics cord then you don't need to wire anything to the grounding post.



But (you knew there was a but right?) I have mine wired correctly and a separate 10ga ground wire running to a 10' copper rod driven into the earth. There are others who have done the same. There are others who will warn you that you should never do that because your house wiring is already run to an external earth ground and you can create bad things like eddy currents doing this. It's a religious argument. Both sides will argue they're right and both will cite sources backing up their position.



So, it's your option. You only have to do it if you only have two wire cords and the power supply ground is run to the post. You shouldn't need to do it if you have a 3 wire cord and the power supply's wires are correctly attached to the cord receptacle on the machine and your house wiring is properly grounded.


---
**Alex Hodge** *August 09, 2016 21:55*

It's really only necessary if you don't have a grounded power plug. That said, it's not a bad idea to ground the chassis (that ground joint) in case your tube were to arc to the chassis. Personally, I removed that port from the back, ground down the chassis to bare metal in a couple of spots, tied them together with some heavy gauge wire and steel fasteners then connected that to the ground post of the power input. This way my chassis is grounded, and I have two good quality ground points in the chassis that I know I can use easily and reliably.


---
**greg greene** *August 09, 2016 21:57*

There is a youtube video showing why you need to ground this sucker.  Google for it and it may save you from a nasty shock.


---
**greg greene** *August 09, 2016 21:59*

try this [https://groups.google.com/forum/#!topic/opensource-laser/ffET5oi1lQM](https://groups.google.com/forum/#!topic/opensource-laser/ffET5oi1lQM)


---
**Jim Hatch** *August 09, 2016 22:00*

😀 All in the "ground it" camp. There's someone on the group that was very vocal about my being an a**hole for doing it when I posted that I did that 🙂 Waiting for that counterpoint. 



(Except if you're on a boat using it I would not do it and would rely entirely on the boat wiring and shoreline power cord to take care of grounding because in that case I'd be able to watch my sacrificial zincs get eaten from the current Eddie's dumped in the water 😀)


---
**Jose Castellon** *August 09, 2016 22:05*

Thank you guys so much for your responses. 

I have not even tried turning the machine on yet til I resolved this issue. 



I did multiple searches on Google and YouTube and I didn't find anything that talked specifically about this subject. 



My unit did come with a 3 wire connector. I have yet to open the unit up and check to see if it's all wired up. 



I will check that once I get back from buying more supplies for it. 



And when you guys says earth ground what does that mean exactly? 



Like I said I'm a total newb at this and have no knowledge of electrical so please bear with me. 



If anyone can send me a picture of their setup that would be very appreciated. 



Thank you guys so so much for your feedback! 



-Jose


---
**Jim Hatch** *August 09, 2016 22:08*

An earth ground is a long (usually 10 foot) solid copper rod driven into the earth. It's connected to the machine via a heavy gauge wire (usually 10 gauge) to that post on the back of the machine. 



Your house wiring is already connected to one somewhere as well.



Building supply places have the rod, wire and the saddle clamp used to attach the wire to the rod. The rod is somewhat pointy so you can hammer it into the ground behind your house.


---
**Alex Hodge** *August 09, 2016 22:14*

Open it up and follow the wires. that ground joint connects to the ground post on the AC power input plug. Everything in the system connects to that. It's sufficient ground for the system. Also, take apart that ground joint and look at it. It's not even but barely touching the actual metal of the chassis. That little ground post does a poor job of actually grounding the chassis. It would be a fine ground for the electronics though...assuming you drove a ground rod into your backyard or whatever. 



The reality is, you should inspect inspect all the wiring in your K40 before using it. Be sure that all connections are solid. Be sure that all solder joints and/or crimps are good. Upgrade the wiring where appropriate (I believe ground is the most important thing you can upgrade here). But when you do that realize that the ground post on your power outlet hooks up to the best ground available to you...Assuming the wiring in your house is good.



Some people get all up in arms about that ground joint. I personally think it's pretty well worthless and but if you want an auxiliary ground, go ahead. Just maybe upgrade it before you do it. Grind down paint to expose bare metal for your ground joint. Use a stronger, bigger connector for your ground joint. I suggest a fat nut and bolt and an eyelet on the end of a fat copper cable.

Or you know, just ground the chassis to the ground post of the power inlet.


---
**greg greene** *August 09, 2016 22:23*

One final point, The machine uses 24K HV - at sufficient current to cause significant damage to you.  You are only safe it the path to ground in the event of a malfunctioning component is at less resistance through the ground wiring than it is through your body.  You might be surprised at how much resistance there is in 'House wiring ground circuit' These machines do not use highest quality components - be safe - ground the beast.


---
**Jim Hatch** *August 09, 2016 22:47*

**+greg greene**​ +1


---
**Jose Castellon** *August 09, 2016 22:59*

Thank you all so much for your feedback I really do appreciate it 😊. I'm gonna check the wiring to make sure it all looks fine. Considering I do have the 3 wire connector from the machine to the male side of the plug I think I'll just run it without that auxilary ground. I won't be setting it on or around metal. A few other sources have told me it's not necessary with the 3 wire connector. Thank you all once again. 

Hopefully I get to try this bad boy out soon and make some cool stuff with it. 

Cheers guys

-Jose ❤


---
**Jim Hatch** *August 09, 2016 23:12*

🙂 that's an option. I have mine in my garage on a wood bench which sits on a concrete floor. I had it plugged into the house and all worked well.



Then one rainyvday the floor was damp and I was standing there in barefeet and felt a tingle when I leaned over and touched the machine while it was cutting. Didn't feel it if I was wearing shoes or the floor was dry. 



But that pointed to what Greg said about the resistance in my house's ground circuit. I was an easier path. Don't know if it's the machine wiring, ground connection or the power cord. Didn't matter - I did what Alex did. Now both me & the machine's electronics are safe. Could have ignored it cause I should wear shoes but that tiny current leak would still possibly be there. Now it's electrically clean and I feel better 🙂


---
**Jose Castellon** *August 09, 2016 23:24*

😂 oh man I can only imagine what went through your head followed by that jolt. 

I'll be aware of that as well. 

I'm still figuring out where exactly I'm gonna set it up permanently but for not just to learn it I think I'm safe since its indoors and away from any water or metal. 

I'll eventually do some preventive ugrades on it for now I just wanna learn how to use the thing and make some cool stuff. 



If anyone can point me to A thread of things that you all have made I'd love to see it. 



Thank you all ❤

-Jose


---
**greg greene** *August 09, 2016 23:52*

This forum has a repository thread - check it out !

Remember that 3 wire ground has at least two connections that can manifest resistance and isolate itself from true ground

The plug you use is one, the connection in the box is another, the connection at the fuse box is one more, the connection at the fuse box to the round wire is one more and the connection from the ground wire to the ground rod is yet another.  It only takes a little corrosion at one of more connection to make your body a more attractive alternative. :)



Have fun !


---
**Alex Hodge** *August 10, 2016 01:40*

**+greg greene**​ Obviously people should consider the quality of their home/shop wiring. I would probably use an auxiliary ground if I had original wiring from the 50s or something. For sure. I think you're just trying to preach caution and I like that but a "little corrosion" is probably not gonna do it. Unless you tend to lase while naked and standing in a mud pit in your backyard.


---
**greg greene** *August 10, 2016 02:30*

Naked in a mud pit?  Yikes the neighbours would call the men in white coats on that!!!!

Yes, your correct - I am being overly cautious - perhaps too cautious - still it is easy to make sure nothing untoward can happen with the investment in a ground rod and a few feet of wire.  There are lots of examples where this is not practical for sure - so good luck to all.


---
**Alex Hodge** *August 10, 2016 02:51*

**+greg greene** :)


---
*Imported from [Google+](https://plus.google.com/111728896423789579330/posts/VqwyCs74fLy) &mdash; content and formatting may not be reliable*
