---
layout: post
title: "Finally got it aligned ! I was not particularly unhappy with the K40 when it arrived - till I tried to cut wood- my cut times were like 15 ma at 3mm/sec repeated 10 times for 3mm ply"
date: August 09, 2016 21:31
category: "Discussion"
author: "greg greene"
---
Finally got it aligned !



I was not particularly unhappy with the K40 when it arrived - till I tried to cut wood- my cut times were like 15 ma at 3mm/sec repeated 10 times for 3mm ply.  Yikes! not good.  Then another poster listed an alignment procedure from 

timthefloatingwombat@gmail.com  - I found the beam fairly centred on the first mirror but way off going to the second. took me half an hour to sort that.  Being in ham radio I know the dangers of High Voltage so I turned the machine off between adjustments - I recommend you do the same - takes longer but safety first. Once I got the beam centred to the second mirror I got it to adjust to a single point on the head.  I found the trick there was to disregard where on the head aperture the beam hit - just make sure it hits on the same spot in all four corners.  THEN adjust the head to centre the beam into the aperture.  My mistake before was to try to adjust mirror 2 to get the beam centred on the head - this just threw out the beam adjustments to the corners.   Now that I did it the right way I found engraving at 10 ma at 50mm gives me a burn depth of almost 2mm - before that I was just marking the surface of the wood.  WOW what a difference, this was using a LO head, 20mm Si mirror in it and 18mm focus lens.  I can see upgrading the other two mirrors (Mirror 1 - Fixed and Mirror 2 - traveler) to gold ones will  help even more.  So thank you Dr. Fawcett - your instructions turned the machine into a work horse for me.





**"greg greene"**

---
---
**Ariel Yahni (UniKpty)** *August 09, 2016 21:53*

Post the link please for future reference on your great results


---
**greg greene** *August 09, 2016 21:54*

I would if I could remember it !


---
**Ariel Yahni (UniKpty)** *August 09, 2016 21:56*

Lol


---
**greg greene** *August 09, 2016 21:57*

Sadly I suffer from O.L.D.


---
**Ariel Yahni (UniKpty)** *August 09, 2016 22:05*

Here you. Go [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**Eric Flynn** *August 09, 2016 22:45*

After a good alignment , you should be able to cut 3mm ply nicely at 10mm/s @6-7ma.


---
**Vince Lee** *August 13, 2016 16:48*

Congrats.  For anyone else with alignment issues I recommend doing a similar procedure when aligning the beam on mirror 2.  Adjust the angle of mirror 1 only to get the beam hitting the same spot on mirror 2.  Don't worry about whether it is hitting the center yet.  Once the position is consistent just move mirror 2 in its mount to wherever the beam is.  Using this procedure it is dead easy to align all the mirrors.


---
**greg greene** *August 13, 2016 19:30*

yup it cuts 3mm birch at 6ma and 8 mm so fine the pieces just fall right out.  Another mistake I made was not having the step set to 1 pixel - so it kins left uncut gaps at first.  I guess it's a sad day if you don't learn something !!!!


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/c7z2nd6khEF) &mdash; content and formatting may not be reliable*
