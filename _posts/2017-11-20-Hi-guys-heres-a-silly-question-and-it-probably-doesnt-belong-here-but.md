---
layout: post
title: "Hi guys, here's a silly question and it probably doesn't belong here but...."
date: November 20, 2017 23:41
category: "Modification"
author: "Chuck Comito"
---
Hi guys, here's a silly question and it probably doesn't belong here but.... when counting the teeth on a gt2 pully, am I counting the teeth (protrusions) or the indents where the protrusions on the belt fit into? I'm trying to rule out some errors I'm getting with rectangles not closing during cut jobs. Could be loose belts, could be stepper motors or the drivers I'm using. Thanks. 





**"Chuck Comito"**

---
---
**Don Kleinschnitz Jr.** *November 21, 2017 00:21*

[pfeiferindustries.com - Timing Belt Pulley Diameter Charts &#x7c; Pfeifer Industries](http://www.pfeiferindustries.com/timing-belt-pulley-pitch-diameter-outside-diameter-charts-i-12-l-en.html)


---
**Steven Whitecoff** *November 21, 2017 16:17*

Teeth are counted not the notches


---
**Brent Crosby** *November 22, 2017 16:29*

If there are N teeth, then there are N notches. 1:1 correspondence there! Use a Sharpie to mark your starting tooth, and maybe every 5 or 10 teeth to help keep track of your count.



You don't really need to know the diameter (does not hurt, of course). Just keep track of the teeth going into and out of pulleys. As an example, if you have a 20 tooth pulley driving a 100 tooth pulley, the ratio will be 5 turns on the 20 tooth to 1 turn on the 100 tooth. 


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/B4cufA3UBf7) &mdash; content and formatting may not be reliable*
