---
layout: post
title: "Is there a secret to cutting extruded acrylic?"
date: March 10, 2016 00:41
category: "Discussion"
author: "3D Laser"
---
Is there a secret to cutting extruded acrylic?  I have no problem with cast acrylic it cuts right through but when I cut extruded it smells aweful and the cut seems more melted that cut.  The green is extruded and the orange is cast both where cut at the same speed and power  I don't know if you can tell in the photo but the edges on the orange are smooth while the edges on the green look like melted plastic 

![images/7a29ef859c2bd621045a70d64eaaffc8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a29ef859c2bd621045a70d64eaaffc8.jpeg)



**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 01:45*

I haven't worked with acrylic, but from a bit of a google around it seems that extruded acrylic will generally have a burr on one side of the part (when cut). From another article, it says that high-quality extruded acrylic (such as Perspex) will give a smooth clean laser-cut.



I did also notice that the extrusion direction can make a difference when trying to bend the extruded acrylic, so it's possible that the direction of the extrusion may also make a difference on the cuts. Do you notice whether the horizontal & vertical cuts are similar in result on your green piece? Or is one direction "cleaner" than the other?



[http://www.accessplastics.com/cast-vsextruded-acrylic/](http://www.accessplastics.com/cast-vsextruded-acrylic/)

[http://www.pmma.dk/Acryl_stobt_kontra_ekstruderet.aspx?Lang=en-GB](http://www.pmma.dk/Acryl_stobt_kontra_ekstruderet.aspx?Lang=en-GB)

[http://www.cutlasercut.com/resources/tips-and-advice/everything-you-need-to-know-about-acrylic](http://www.cutlasercut.com/resources/tips-and-advice/everything-you-need-to-know-about-acrylic)


---
**Alex Krause** *March 10, 2016 04:58*

Cast and extruded are actually two different materials fabricated with different chemical makeups and processes I work with extruded plastic every day at work on CNC mills and lathes. Extruding gives the plastic a "grain" similar to what you would expect with wood where the material varies in density and the lay of the polymer bonds that make the sheet up. cast employs a heterogeneous mixture that has more uniform properties 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/9Vw2AxCHTxV) &mdash; content and formatting may not be reliable*
