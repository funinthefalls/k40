---
layout: post
title: "Some progress on my machine. I have vector cut a star shape from 2mm acrylic"
date: March 02, 2018 22:55
category: "Discussion"
author: "Mark McCarty"
---
Some progress on my machine.  I have vector cut a star shape from 2mm acrylic.  It cuts out the shape fine, but seems to slightly engrave the image offset to the top?  I tried to take a picture, hope it shows what I am trying to say?

Any thoughts on what is going on?  

Thanks

Mark

![images/1457f6b28867ae03cfe20a3a15eb3a52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1457f6b28867ae03cfe20a3a15eb3a52.jpeg)



**"Mark McCarty"**

---
---
**Adrian Godwin** *March 02, 2018 23:26*

Do you have an air assist nozzle ? This is typical of the effect of the beam reflecting off the inside of the nozzle due to being incorrectly centred.



If you don't have a nozzle (just an open mirror with a lensholder) then perhaps you're working on the edge of one of the other mirrors. Check the beam is carefully centred all the way through.






---
**Mark McCarty** *March 03, 2018 02:05*

**+Adrian Godwin**

Hi Adrian

I do have "air assist" but it is just a fan blowing down, so from reading your post, I'm guessing that's not it?

It may be the mirrors, I will recheck them.

Thanks for the ideas!

Mark


---
**Mark McCarty** *March 04, 2018 21:04*

**+Adrian Godwin** Thanks again, I adjusted the mirrors, all looks good!

Mark


---
**Jean-Phi Clerc** *March 06, 2018 14:08*

**+Mark McCarty** hi , please give me power used for cutting, i would l'île to cut 2-3 mm top on my k40


---
**Mark McCarty** *March 06, 2018 23:38*

**+Jean-Phi Clerc**

I cut the 2mm acrylic at 12mm/s, with the digital readout at 45.


---
*Imported from [Google+](https://plus.google.com/107039262251147908797/posts/VxgjAKbnAxF) &mdash; content and formatting may not be reliable*
