---
layout: post
title: "Black granite and silver alcohol ink"
date: September 23, 2018 21:12
category: "Object produced with laser"
author: "HalfNormal"
---
Black granite and silver alcohol ink.



![images/122e252b7d5a9d2eb5e897044ca1dd31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/122e252b7d5a9d2eb5e897044ca1dd31.jpeg)



**"HalfNormal"**

---
---
**Mike Meyer** *September 24, 2018 02:16*

How did you prep the surface? Can you give me a quick brief on how you use alcohol ink? BTW, that looks very cool...


---
**HalfNormal** *September 24, 2018 02:21*

No prep needed. Sloppy paint alcohol ink on engraved area and wipe excess away with alcohol on a rag.


---
**Don Kleinschnitz Jr.** *September 24, 2018 12:56*

#K40GraniteEtching


---
**Timothy Rothman** *October 04, 2018 05:15*

Awesome


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/DnKFSpr6H5V) &mdash; content and formatting may not be reliable*
