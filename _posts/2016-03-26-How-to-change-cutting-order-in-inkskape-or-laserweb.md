---
layout: post
title: "How to change cutting order in inkskape or laserweb?"
date: March 26, 2016 14:33
category: "Software"
author: "Damian Trejtowicz"
---
How to change cutting order in inkskape or laserweb?

I want cut some holes before outside contour ,bo dont know how, now i need edit my gcode in wordpad





**"Damian Trejtowicz"**

---
---
**Anthony Bolgar** *March 26, 2016 15:16*

If you are exporting the Gcode from Inkscape, the cutting order is based on the order in which the objects are drawn. So draw the holes before the outside contour.


---
**Damian Trejtowicz** *March 26, 2016 15:22*

Ok,thanks,what about imported dxf files?


---
**Anthony Bolgar** *March 26, 2016 15:34*

Not sure about the dxf files.


---
**Sebastian Szafran** *March 27, 2016 06:55*

I use Notepad++ with line numbering for my g-code editing. Useful specially if g-code does not contain numbered lines. 


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/Q4u2qdyX3xS) &mdash; content and formatting may not be reliable*
