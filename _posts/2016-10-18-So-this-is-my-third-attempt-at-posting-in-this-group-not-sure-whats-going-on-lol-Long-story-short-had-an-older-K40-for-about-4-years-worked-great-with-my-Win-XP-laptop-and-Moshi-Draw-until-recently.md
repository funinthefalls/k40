---
layout: post
title: "So this is my third attempt at posting in this group, not sure whats going on lol Long story short, had an older K40 for about 4 years, worked great with my Win XP laptop and Moshi Draw, until recently...."
date: October 18, 2016 21:58
category: "Discussion"
author: "Andrew Brincat"
---
So this is my third attempt at posting in this group, not sure whats going on lol



Long story short, had an older K40 for about 4 years, worked great with my Win XP laptop and Moshi Draw, until recently....



So thought I would get a new K40 to replace the old one (multiple power/earth leaks I spent an entire weekend working on it and didnt fix!)



I have installed the provided software multiple times now, CorelDraw 12 and Corel Laser, nothing I do seems to get the laptop to detect the laser. Keeping getting attached balloon pop up. 



I have USB dongle attached, updated settings to show M2 which is the model on the mainboard, done quite a bit of online searching and tried the eBay seller for support (not much in english lol)



Anyone got any ideas? Short of Doing some Smoothie conversion or something like that (i'm a bit techy but would need a bit of guidance with some things) so if anyone has any ideas on getting software to work that would be great, or if its best to just fork out more money and do some upgrades, any links would be greatly appreciated!!



Thanks in advance

![images/65c1fe7e9903eb99aa8bae0d75c02189.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65c1fe7e9903eb99aa8bae0d75c02189.jpeg)



**"Andrew Brincat"**

---
---
**Alex Krause** *October 18, 2016 22:01*

Have you input the serial number that's on the m2nano board?


---
**Andrew Brincat** *October 18, 2016 22:03*

I have not. Where does one add this?


---
**Alex Krause** *October 18, 2016 22:05*

![images/8b21218f66dd5fceae2bd3db17480ff9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8b21218f66dd5fceae2bd3db17480ff9.jpeg)


---
**Andrew Brincat** *October 18, 2016 22:19*

Ok cool, will check that out. assuming the serial is the  sticker on the  right?

![missing image](https://lh3.googleusercontent.com/-alFc8-BU5VbmwxyNhKVWK0swybnjrfSmN2SlEvOhQmqgrjgGH_Hl4JI1Mq1Z0RHMZKk8qkcKQ)


---
**Alex Krause** *October 18, 2016 22:20*

Yep


---
**Andrew Brincat** *October 18, 2016 22:23*

Sweet going to do a fresh install of just CorelDraw and CorelLaser hoping it works now! Thanks **+Alex Krause**​


---
**Andrew Brincat** *October 18, 2016 22:40*

So fresh install and added serial and still same balloon. Should I install this other software?? Sorry for all the questions just totally lost with this. Thanks

![images/d95926e149c5c2d25cd62ad6285a7cfe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d95926e149c5c2d25cd62ad6285a7cfe.jpeg)


---
**Jim Hatch** *October 18, 2016 22:59*

Does LaserDrw work? I always start with the basic install. It eliminates any Corel or plugin issues. First get LaserDRW installed & working and then move to Corel.


---
**Andrew Brincat** *October 18, 2016 23:08*

I was installing LaserDRW but that was before i put the serial in. Was just following the video provided with the laser. will try just DRW. Thanks **+Jim Hatch**​


---
**Jim Hatch** *October 18, 2016 23:15*

You'll need to do the same machine & device info in LaserDrw (machine settings) that you put in Corel. The dialog box will look exactly the same but it stores the info somewhere else. The page size can be modified (without the factory bed the size is something like 220x320) but it'll work fine with the default settings until you do bigger stuff.


---
**Andrew Brincat** *October 18, 2016 23:27*

So with just LaserDRW installed, I dont get a balloon pop up advising its not connected. but nothing happens when i either drag the image and move it (laser head does not  move) or if I hit starting it loads for a few secs, closes window then nothing...



only thing I noticed was down the bottom of the window next to a printer icon it had "none"....

![images/cc5e227b74361a788e73741e53cbb9b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc5e227b74361a788e73741e53cbb9b0.jpeg)


---
**Jim Hatch** *October 19, 2016 00:01*

So you set the machine & board info, exited LaserDRW and restarted it? When it fired up should have flashed a screen with "Winseal" and Chinese characters and a changing message that checks the machine and ends by saying something to the effect that the USB key was found and authorized.



Then LaserDRW workspace shows up. Next I'd draw a box and next to it I'd type a line of text. Then do an engrave. You have to hit the "starting" button on the right side of the dialog box to make it go.




---
**Andrew Brincat** *October 19, 2016 02:11*

Yeah thats whats happened. double checked the msg on winsealxp splash and it said authorized. 



When using LaserDRW it does not throw an error. but trying to send a job through and nothing. tried moving the image on the preview and still no head movement.



attached a couple  of pics. Got another email from supplier with old Moshi draw instructions lol

![images/8cf86adf7a45d0d31cedd5494fea6b81.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8cf86adf7a45d0d31cedd5494fea6b81.jpeg)


---
**Andrew Brincat** *October 19, 2016 02:17*

try a screenshot lol

![images/94f86d387542ffd67e2ae299695867bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94f86d387542ffd67e2ae299695867bf.jpeg)


---
**Alex Krause** *October 19, 2016 02:27*

Does the machine attempt to home itself when you turn it on?


---
**Andrew Brincat** *October 19, 2016 02:43*

nope


---
**Andrew Brincat** *October 19, 2016 02:45*

Fans start, test fire works (though i just noticed  if i hold it down the laser seems to wonder a bit, as in head moves on its own)... pic of wiring if that helps... will add a video  if i can



![images/5ac14e77bae05d10eb6d3012b63acd11.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ac14e77bae05d10eb6d3012b63acd11.jpeg)


---
**Andrew Brincat** *October 19, 2016 02:47*

best i can do is show the burn on some paper...

![images/be1309c4d15faa9b037abcb66942502a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be1309c4d15faa9b037abcb66942502a.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 19, 2016 05:23*

There is a driver file somewhere on the CD... a few others have recently had this issue of it not detecting the laser also... I have a feeling that fixes it


---
**Andrew Brincat** *October 19, 2016 08:17*

Any idea where it needs to go? Didnt think that would help with the laser head not going to home when powering on and test fire moving head when held down... saw a driver folder but install failed  to run


---
**HalfNormal** *October 19, 2016 12:51*

**+Andrew Brincat** IF the head does not home when you turn the unit on and IF the head is moving when you fire the laser, I would bet money on a bad controller board. The fact that your computer does not recognize the laser is another clue that the controller board is bad. All the information you have posted leads back to the controller board.


---
**Andrew Brincat** *October 20, 2016 00:41*

Thanks **+HalfNormal**​ i will let the supplier know. also sent them this link...
{% include youtubePlayer.html id="6sIWi0mTF6U" %}
[https://youtu.be/6sIWi0mTF6U](https://youtu.be/6sIWi0mTF6U)



thanks for all  the help everyone!!



really appreciate it!


---
*Imported from [Google+](https://plus.google.com/+AndrewBrincat/posts/GEZxAdMpB5m) &mdash; content and formatting may not be reliable*
