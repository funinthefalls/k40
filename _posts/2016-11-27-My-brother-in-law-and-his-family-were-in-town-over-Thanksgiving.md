---
layout: post
title: "My brother-in-law and his family were in town over Thanksgiving"
date: November 27, 2016 03:40
category: "Object produced with laser"
author: "Ned Hill"
---
My brother-in-law and his family were in town over Thanksgiving.  He's a big Superman fanboy so I made him this desk/shelf ornament out of a piece of 3.5x3.5x1" red oak.  Reverse engraved both sides and etched his name in Kryptonian on the edges (50%, ~14mA, at 400mm/s).  Works out that 3.5" (89mm) is right at the focus point.  So I just cut a frame out of 6mm ply to hold the block and glue on some strong magnets to hold the frame steady on the floor of the laser compartment when I etched the edges.  Lol he was so blown away :)



![images/564f0e45b39f7a5609465aa08d4833d4.png](https://gitlab.com/funinthefalls/k40/raw/master/images/564f0e45b39f7a5609465aa08d4833d4.png)
![images/f0fda9d560701a205f06edbc4a0219b7.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f0fda9d560701a205f06edbc4a0219b7.png)
![images/d1aebf84ad71b436b52854d1d853649b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d1aebf84ad71b436b52854d1d853649b.png)
![images/e33cd2226c10e5c9283fdc38802f699a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e33cd2226c10e5c9283fdc38802f699a.jpeg)

**"Ned Hill"**

---
---
**David Cook** *November 28, 2016 16:16*

That came out great !  nice work




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/5YYr2nDjMLE) &mdash; content and formatting may not be reliable*
