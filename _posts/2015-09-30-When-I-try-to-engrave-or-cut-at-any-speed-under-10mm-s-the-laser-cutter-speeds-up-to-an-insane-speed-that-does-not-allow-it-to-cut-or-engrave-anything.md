---
layout: post
title: "When I try to engrave or cut at any speed under 10mm/s the laser cutter speeds up to an insane speed that does not allow it to cut or engrave anything"
date: September 30, 2015 20:43
category: "Software"
author: "Anthony Bolgar"
---
When I try to engrave or cut at any speed under 10mm/s the laser cutter speeds up to an insane speed that does not allow it to cut or engrave anything. Any ideas as to what is causing this?﻿





**"Anthony Bolgar"**

---
---
**Lee Maddison** *October 01, 2015 07:15*

What software are you using ? Im a complete newby and have had my laser a week and have been fighting with all issues so far. But think i'm nearly there. I had a configuration error in settings. Ie wrong laser was set up in there and mine worked one minute and then went mad with other settings. Worth checking that first.


---
**Joey Fitzpatrick** *October 02, 2015 02:51*

You probably have the wrong mainboard number set in corelaser settings.  Check the number on your board and make sure you select the right one in the settings menu


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/1dikMcxsTgz) &mdash; content and formatting may not be reliable*
