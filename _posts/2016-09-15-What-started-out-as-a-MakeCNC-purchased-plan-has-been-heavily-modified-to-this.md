---
layout: post
title: "What started out as a MakeCNC purchased plan, has been heavily modified to this"
date: September 15, 2016 05:37
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
What started out as a MakeCNC purchased plan, has been heavily modified to this. Some of the features are:

- Larger bucket

- Bucket can lower and raise

- Bucket apron opens and closes (the door on the front)

- Moving pistons that extends/contracts as the bucket lowers/raises

- Main hinge pistons that extends/contracts as the vehicle pivots

- Spinning wheels

- Hidden wheel axles behind wheel hubs

- I forgot what else ...



Other notes:

- redesigned the bucket once, rebuilt it twice.

- redesigned the cab once, tore it up after discovering a 2mm discrepancy

- redesigned the axles

- redesigned the main connecting bar on the bucket



There are still some changes and fixes to be implemented. Some things are easy to draw and see in 3D space, but harder to do in real. It's a never ending ... thing.







**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Marshall** *September 15, 2016 07:15*

Wow. That came out great!



I like how you used the charred edges as the only shading. It looks very realistic. Great detail, I especially like the steering cylinders. You really made each part 'work'.

I would have 'given in' to temptation and started shaping parts, painting,  etc but your vision comes through and really feels right.. 



Nice job on the photos too. Good subject appropriate backdrop.

Very nice winter open pit feel. I can hear the turbo spinning up...



A new fellow came on the other day asking what he could make for kids, and I thought of you. (Knew you were doing a different thing, but thought the concept could be applied to kids toys. 



 I wish I had my Laser when the kids were young, I have 2 boys (both in college) and they were both truck/model builders. We used to get the simple laser cut kits and modify them with rolling wheels etc.



My older son is a farm machinery lover and I have a lot of nice die cast models. If I get time I should see if they can be turned onto patterns. There's a nice one with both a full set of disks and it's a grain trailer - it's a good size, about 18" long with the implement attached. Maybe this winter I'll work up some raw patterns and post them up.



 I'd like to see what you would do with it.(if you're looking for new material)



Scott






---
**greg greene** *September 15, 2016 13:27*

My Grandson would love it !


---
**Ashley M. Kirchner [Norym]** *September 15, 2016 17:05*

Thanks **+Scott Marshall**! it's been a labor of love for the most part. The pieces I thought would be the most challenging were made and tested early on (the cylinder & pistons, the hidden axles). Then after "assembling" the whole piece in 3D, I didn't like the squished nature of the bucket, so I redesigned that from scratch, which then posed its own challenges.



I still have a small issue with the large hinge bar on the bucket (it tips too far forward, causing the cab to droop down), so that is being looked at and possibly redesigned internally.



It's a bit longer than I had originally planned, but all in all, I'm happy with the result.


---
**Ashley M. Kirchner [Norym]** *September 15, 2016 17:40*

Also **+Scott Marshall**, I'd love to take a look at the die cast models you have. Do you have any pictures (or plan on taking some?) I do have different ideas for vehicles in my head, and that's more so I can get a better idea of what kind of joints work best for a particular application. Whether something spins, rotates, or slides, how to hide others, etc., etc. 


---
**Adam J** *September 15, 2016 21:46*

Excellent job!


---
**Scott Marshall** *September 15, 2016 23:02*

**+Ashley M. Kirchner** Next time I'm doing pictures I will take some and post them.

Been putting off the photo session too long anyway.


---
**greg greene** *September 15, 2016 23:08*

Did you make the plans yourself - can we purchase a copy?


---
**Tony Schelts** *September 16, 2016 10:05*

I would be interested in that answer also thanks




---
**Ashley M. Kirchner [Norym]** *September 16, 2016 13:57*

Read folks, not just look at the pictures. Read. The very first line gives you the answer. 


---
**greg greene** *September 16, 2016 15:59*

Well, a partial answer anyway, I did see it was a purchased plan, but it has been heavily modified so I was wonder if after the dust - er smoke clears, if the up to date plan would be available.


---
**Ashley M. Kirchner [Norym]** *September 16, 2016 16:43*

Hah, smoke ... I see what you did there. :)



My plan isn't to sell the cutting files for a few reasons, primarily because this is such a custom job that I did both for me, but also for the little kid who I'm gifting this to. Also because there are so many intricate pieces and creating an assembly instruction manual is just not something I want to do. It's a pain to put together as it is. However, I am considering possibly selling completed (already built) vehicles ... but that's still far on the horizon and I also realize that takes a lot of the building fun out of it for some of you. Honestly, the closest you're going to get right now is that MakeCNC version, it's pretty simple compared to this one.


---
**greg greene** *September 16, 2016 16:45*

Thanks!  I'll start there then, you have done a truly great job with it and the kid getting it is going to be very pleased I'm sure - thanks for taking the time to show it to us.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/LjhZqG56sRz) &mdash; content and formatting may not be reliable*
