---
layout: post
title: "Focus the laser beam, what are you using?"
date: August 26, 2016 12:39
category: "Hardware and Laser settings"
author: "Ariel Yahni (UniKpty)"
---
Focus the laser beam, what are you using? Share your experience :)





**"Ariel Yahni (UniKpty)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 12:57*

I tend to place magnets underneath my cutting bed (on top of the original rods that supported the original bed). Then I can adjust in increments of 2mm at a time. Either that or I stack blocks of wood under the cutting bed to get the right height.


---
**Bob Damato** *August 26, 2016 16:11*

I just stack stuff and measure unfortunately. However Im kind of lusting after this. Anyone else use this? 

[http://www.lightobject.com/Power-Z-table-bed-kit-for-K40-small-laser-machine-P722.aspx](http://www.lightobject.com/Power-Z-table-bed-kit-for-K40-small-laser-machine-P722.aspx)


---
**Ariel Yahni (UniKpty)** *August 26, 2016 16:32*

**+Derek Schuetz**​ seems to have one


---
**Derek Schuetz** *August 26, 2016 16:49*

**+Bob Damato** I have the motorized bed I'd suggest against it since it's just to small for the machine I had to make a make shift spacer to make it right and there to.much room for movement 


---
**Phillip Conroy** *August 26, 2016 18:17*

Pin bed and removable tin bed-12 10mm magnets with 65mm long wood screws that i move around ti hold work peace up-i remove tray once a week to clean as cutting only 3mm mdf leaves sticky resadue on tray and screws


---
**Robi Akerley-McKee** *August 26, 2016 19:07*

Looks like I can build one how I want using the 3040 to cut bearing pockets, holes, and lap joints.  I have a lathe for turning the all thread to fit bearings and pulleys.  Looking at at the light object table, I'm thinking making it wider and shorter to go under the gantry side and the Y stepper, make the table just a perimeter piece with legs on the inside to come inside the gantry and go up to the honeycomb table.  I won't get the height adjustment they do but I should get a better cutting bed.  Or I cut the bottom out of the machine and raise the laser up to get more height.


---
**Frank Dart** *August 27, 2016 00:02*

I wrapped a zip tie around the laser head and then cut a second one the length of my focus point. I slide the 2nd one through the small gap then stack whatever is laying around until my piece touches the zip tie.


---
**Don Kleinschnitz Jr.** *August 27, 2016 20:43*

I have the LO table, decided I couldn't build it cheaper. Going to cut out the machine bottom later. 


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/8udmUDJFWv7) &mdash; content and formatting may not be reliable*
