---
layout: post
title: "Output file types with Corel X8 and Corel Laser - Several months ago I moved from using Corel 12 to using Corel X8 with still using the CorelLaser plugin to interface with the laser"
date: December 04, 2016 00:39
category: "Software"
author: "Ned Hill"
---
Output file types with Corel X8 and Corel Laser -  Several months ago I moved from using Corel 12 to using Corel X8 with still using the CorelLaser plugin to interface with the laser.  (Will be moving to Laserweb as soon as I get my Cohesion3d :)  Anyway, I noticed that when trying to cut open curves the g-code generated always closed the curves.  It wasn't until I changed the output cut file from Normal Windows Meta to Enhanced Windows Meta that the open curves would stay open.  Also I found that when etching an image that was composed of curves, like a postscript file, it would treat the whole image as black regardless of the white color of some of the other curve elements.  I was able to get around this by changing the engrave output file type to Jpeg which then didn't engrave the white elements.  Just a couple of things I've come across and thought I would share.





**"Ned Hill"**

---


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/F7QhfE7ffXL) &mdash; content and formatting may not be reliable*
