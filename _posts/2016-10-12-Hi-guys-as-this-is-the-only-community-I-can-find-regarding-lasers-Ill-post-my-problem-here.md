---
layout: post
title: "Hi guys as this is the only community I can find regarding lasers I'll post my problem here"
date: October 12, 2016 13:33
category: "Discussion"
author: "dave wilkes"
---
Hi guys as this is the only community I can find regarding lasers I'll post my problem here.



I have a Chinese large 50w laser the problem is there  is no lighr when I press the pulse button however I've taken the side off the machine and there is a test button on the power supply when I press that it produces a light any ideas what it could be I know the pulse button is working as when I press it a green light lights up on the power supply ..





**"dave wilkes"**

---
---
**dave wilkes** *October 12, 2016 13:42*

Ah when I press the test on the power supply both lights light and one is wp but when I press pulse the wp light does not 


---
**dave wilkes** *October 12, 2016 13:50*

It does not have a chiller port it has three tubes water in /out and air . Would changing the water make any difference at all also could you explain the flow switch as that might be my only option 


---
**dave wilkes** *October 12, 2016 14:18*

I've just had a look I've unclipped the wiring off the flow switch and put it back on and now it's working maybe it was coincidence because this usually happens but after a while it will work again .



Thanks for all your help if it goes again I'll be sure to check the flow first .


---
**dave wilkes** *October 12, 2016 17:55*

It's packed up yet again I've took out the flow sensor cleaned it as there was some kind of jelly substance in there and that's coming from the pump I believe is there a way to join the wires together to bypass it for now 




---
**dave wilkes** *October 12, 2016 19:25*

I'm getting continuity on the flow sensor wires when it's off the machine but when it's back on there is none weird 


---
**dave wilkes** *October 12, 2016 20:48*

Does anyone know the correct wire colours for the psu on my 50w



I have 

TH

TL

WP

G

IN

5V



Thanks


---
**dave wilkes** *October 12, 2016 22:22*

Here's a pic

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100840041252238699532/albums/6340707224200544449/6340707227222990594)


---
**dave wilkes** *October 12, 2016 22:23*

[https://plus.google.com/photos/...](https://profiles.google.com/photos/100840041252238699532/albums/6340707522405874785/6340707520872508002)


---
**dave wilkes** *October 12, 2016 22:26*

Two purple are coming from the water sensor the white one looks like it's going to the flourecent light the other purple I'm not sure along with the brown I've took these wires out and may of got them mixed up not sure laser is still not firing I've been into vendor settings and enabled water protection and now when I press pulse it says water error press escape. .


---
*Imported from [Google+](https://plus.google.com/100840041252238699532/posts/4RcAma6JP74) &mdash; content and formatting may not be reliable*
