---
layout: post
title: "LaserGRBL I found this software but have not installed or tried it as of now"
date: January 07, 2017 20:48
category: "Software"
author: "HalfNormal"
---
LaserGRBL



I found this software but have not installed or tried it as of now.



[https://github.com/arkypita/LaserGRBL](https://github.com/arkypita/LaserGRBL)



LaserGRBL



Laser optimized GUI for GRBL



LaserGRBL is a Windows GUI for GRBL. Unlike other GUI LaserGRBL it is specifically developed for use with laser cutter and engraver. LaserGRBL is compatible with Grbl v0.9 and Grbl v1.1



Download



All downloads available at [https://github.com/arkypita/LaserGRBL/releases](https://github.com/arkypita/LaserGRBL/releases)



Existing Features



GCode file loading with engraving/cutting job preview (with alpha blending for grayscale engraving)

Image import (jpg, bmp...) and line by line GCode generation (horizontal, vertical, and diagonal).

Grbl Configuration Import/Export

Configuration, Alarm and Error codes decoding for Grbl v1.1 (with description tooltip)

Homing button, Feed Hold button, Resume button and Grbl Reset button

Job time preview and realtime projection

Jogging (for any Grbl version)

Feed overrides (for Grbl > v1.1) with easy-to-use interface







**"HalfNormal"**

---
---
**greg greene** *January 07, 2017 21:05*

links give 404


---
**HalfNormal** *January 07, 2017 21:22*

**+greg greene** Fixed! I am not sure why it did not work but repasting the links worked.


---
**Paul de Groot** *January 09, 2017 05:41*

Cool will try that out. Currently I'm using cheton cnc ( at [github.com](http://github.com) )


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/KkZuiDCet14) &mdash; content and formatting may not be reliable*
