---
layout: post
title: "hi all i have got a problem with my laser, it keeps tripping my mains when i switch it on, its only a month old and to be fair i havent even used it as yet"
date: January 27, 2016 20:03
category: "Discussion"
author: "Dave Patmore"
---
hi all

i have got a problem with my laser, it keeps tripping my mains when i switch it on, its only a month old and to be fair i havent even used it as yet.   the funny thing is it does not do it all the time, i have checked all the wiring to see if there is any bare wires but it looks all good

any ideas

dave





**"Dave Patmore"**

---
---
**Sebastian Szafran** *January 27, 2016 23:35*

Might be a differential fuse in conjunction with a poorly grounded machine, which is sometimes a case with Chinese K40. I would suggest: check, check and re-check wiring and correct if necessary. 


---
**Jim Hatch** *January 28, 2016 00:21*

Check the ground lug - I was getting leakage through the grounding lug. The wiring was "right" but I think there was/is an issue with either the high voltage circuit to the laser or the main power supply. I didn't mess around looking. Try running a ground shunt between the lug and the main power supply ground. If that doesn't work, then try a separate ground to the actual earth.


---
**Joseph Midjette Sr** *January 28, 2016 00:36*

Try another outlet in another room. There could be to many devices on the same circuit. Do NOT operate the printer on a extension cord.  If it still does it make sure the voltage switch on the power supply, (if it has a voltage switch), is on the correct setting for your usage. It could very well be the power supply also. Hope this helps...


---
**Dave Patmore** *January 28, 2016 21:26*

hi thanks for your replies.

the fuse is the correct rating and its the trip that goes first,it does it on my shed socket and the house i have a 10mm earth from my main earth block going to my shed and its al so has a ground steak.

when it does trip and it does not do it all the time but when it does, it is as soon as i

 turn it on

could it be the power supply or the laser tube



dave






---
**Joseph Midjette Sr** *January 29, 2016 11:30*

Before you go throwing parts at it take it in the house and plug it in and turn it on. That should tell you if it's something internal. If it still trips the breaker then disconnect the laser driver board and try it again. Step by step eliminate possibilities and find the problem.


---
**Joseph Midjette Sr** *January 29, 2016 12:09*

Also, I believe when you were referred to connecting an earth ground to the printer it is meant as Put a Ground Rod in the earth, outside of the shed, make sure it is at least 3 feet in the ground (should be 6 to 8),  connect the ground wire from said ground rod directly to your printer on the ground lug at the rear of the printer. 


---
**Joseph Midjette Sr** *January 30, 2016 13:19*

Did you get it figured out? What was causing the breaker trip problem?


---
**Pete OConnell** *January 30, 2016 20:42*

Make sure your shed electric feed is not coming from a light ring, it might not be a bad earth but an overloaded circuit


---
**Dave Patmore** *February 08, 2016 17:40*

HI

ok this is where i am at, 

been on to the supplier and they dont seem to be bothered

the laser will trip my mcb but not all the time, i have not used it yet with my pc as i am still waiting for the correct software and dongle

if i turn the laser on it will trip may be 1 in 3 but if i remove my ground from the back it will not trip not that i would use it like that but there must be something leaking to earth

any ideas i favour the power supply but just in case some has had this fault before

thanks dave


---
**Jim Hatch** *February 08, 2016 18:36*

Ah! Do you have a 3 prong power cord for yours (US) or a 2 prong? If it's 3 prong into the machine and you're plugging it into a properly wired 3 prong grounded outlet you should be good without the extra ground from the lug to earth. That could be strong up spurious ground currents (like what sometimes happens to boats plugged into shore power).


---
**Dave Patmore** *February 08, 2016 19:16*

hi i have a 3 pin plug UK spec kettle lead ,when i bought the unit they made it clear that it had to be externally ground to reduce the risk of shock

thee unit came with a damage main board but they supplied a new one for me to replace




---
**Jim Hatch** *February 08, 2016 19:26*

Try running a wire from the ground lug on the machine to the ground lug on the plug (you can get to the connector inside the box or just juryrig a wire to the ground lug on the power cord. Then try it. Since you reliably fail 1 out of 3 times, it should be easy enough to test. If that fixes it, then permanently run the wire between the grounding lug and the main ground pin. It sounds like you're getting a ground loop effect that's triggering the breaker.


---
**Dave Patmore** *February 08, 2016 22:11*

ok my shed has a separate 40amp supply that goes to my fuse board in the shed along with a 10mm earth, this runs back to the main board 

i can run my mig welder and compressor with no problems

i just dont understend how and why it trips 

but when it does do it it is instant as soon as i flick the switch

dave








---
**Joseph Midjette Sr** *February 08, 2016 23:22*

Check the voltage wire going to the tube, make sure there is not a cut in it or laying against the metal anywhere. Disconnect all components from the power supply then turn it on and see what happens....


---
*Imported from [Google+](https://plus.google.com/106320656164461253905/posts/cFAKtJQu6Ac) &mdash; content and formatting may not be reliable*
