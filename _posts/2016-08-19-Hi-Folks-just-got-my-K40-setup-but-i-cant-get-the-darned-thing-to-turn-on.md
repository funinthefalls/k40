---
layout: post
title: "Hi Folks, just got my K40 setup, but i can't get the darned thing to turn on"
date: August 19, 2016 02:19
category: "Discussion"
author: "Chris Sader"
---
Hi Folks, just got my K40 setup, but i can't get the darned thing to turn on. Mine didn't come with a power cable, so I stole one off of an LCD monitor.



This is the model I bought, with the "upgraded" panel: [http://www.ebay.com/itm/172283110501?euid=c6c6e1799584470a88293b75833ae3cb&bu=43142881398&cp=1&sojTags=bu=bu](http://www.ebay.com/itm/172283110501?euid=c6c6e1799584470a88293b75833ae3cb&bu=43142881398&cp=1&sojTags=bu=bu)



When I turn on the switch next to the e-stop and switch on the red power switch, nothing happens. When I try turning on the Lighting Switch (green), nothing happens. Confirmed power going to the outlet, as Im running the water pump off of the same one.



Any ideas? Photos of panel and wiring attached, everything looks and feels solid and correctly wired as far as I can tell, but I could be wrong.



![images/70a6a25c3f8f37a388bb1899b3285b47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/70a6a25c3f8f37a388bb1899b3285b47.jpeg)
![images/d96aab0fedda0056360283f7ac7bb9ef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d96aab0fedda0056360283f7ac7bb9ef.jpeg)
![images/0e5c0a8f4ba88e551c1d869aca1b683b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e5c0a8f4ba88e551c1d869aca1b683b.jpeg)
![images/8e6f80a61ba4e31850558964b2ca7a97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e6f80a61ba4e31850558964b2ca7a97.jpeg)

**"Chris Sader"**

---
---
**Corey Renner** *August 19, 2016 02:30*

Twist the red E-stop button so that it pops up.


---
**Gee Willikers** *August 19, 2016 02:30*

Check the fuse, located within the power jack.


---
**Chris Sader** *August 19, 2016 02:34*

Gah! :facepalm:


---
**Chris Sader** *August 19, 2016 02:35*

That moment when you feel a little embarrassed and slightly stupid. Thanks **+Corey Renner**​


---
**Corey Renner** *August 19, 2016 02:37*

Nothing to feel silly about.  Those E-stop switches are a bit odd, especially if you haven't used one before.  cheers, c


---
**Alex Krause** *August 19, 2016 04:40*

To be honest this estop isn't like most the standard estops... most have the twist rotation indicated by arrows 


---
**J.R. Sharp** *September 08, 2016 13:45*

Is this actually better than the stock one?




---
**Chris Sader** *September 08, 2016 13:49*

Define "better". I liked that I didn't have to run the e-stop myself. The digital power meter is fine, doesn't really improve anything except i'm not guessing if i've turned a knob exactly 50%. Lighting is nice, glad I didn't have to run that myself too. Wiring inside was super cleanly done and didn't have any loose wiring issues like some have had with the stock ones.



Are these things I could have easily done myself, probably for a little less? Yes, but I wanted to get to the major upgrades (Smoothie, pin bed, etc.) faster, so it was worth it to me.


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/HdL1MSkEBMF) &mdash; content and formatting may not be reliable*
