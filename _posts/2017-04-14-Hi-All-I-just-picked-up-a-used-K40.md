---
layout: post
title: "Hi All, I just picked up a used K40"
date: April 14, 2017 00:57
category: "Software"
author: "StewNet StewNet (stewnet)"
---
Hi All,



I just picked up a used K40.  But I don't have the software is there somewhere I can download it from.  Also, do all the K40's require the USB thumb drive for them to work?



Thanks for any help!





**"StewNet StewNet (stewnet)"**

---
---
**Ariel Yahni (UniKpty)** *April 14, 2017 01:33*

**+StewNet StewNet**​ if it has the stock board then you NEED the USB key for the software to work


---
**StewNet StewNet (stewnet)** *April 14, 2017 01:36*

If I need to purchase another controller what would your recommend?


---
**Ariel Yahni (UniKpty)** *April 14, 2017 01:42*

The easiest all-around solution at the moment is from **+Ray Kholodovsky**​. Many happy customers 


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 01:44*

Thanks Ariel. 



Here's a link: 



[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**StewNet StewNet (stewnet)** *April 14, 2017 02:43*

Thank you Ray


---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 02:44*

Any questions just let me know **+StewNet StewNet**!


---
**Jean-Phi Clerc** *April 19, 2017 08:14*

Have bought a K40 , very interested by your upgrade kit ! Thanks for information


---
*Imported from [Google+](https://plus.google.com/104640767361287982937/posts/hbpERmMkEHy) &mdash; content and formatting may not be reliable*
