---
layout: post
title: "Issues with the new tube :| So below is a pic of the tube I've received from eBay seller reci-laser (not sure if they're affiliated with reci)"
date: May 17, 2016 06:42
category: "Hardware and Laser settings"
author: "I Laser"
---
Issues with the new tube :|



So below is a pic of the tube I've received from eBay seller reci-laser (not sure if they're affiliated with reci).



Here is the advert: [http://bit.ly/200i1LO](http://bit.ly/200i1LO)



As you can see the designs are completely different, in fact this tube is different than all the tubes I've previously seen... Now as per my other post I have a strange issue with very fine bubbles. The problem with this tube is there's a ridge where it tapers, this captures the fine bubbles, they accumulate and make big bubbles.



Now to be honest, I usually wouldn't care what the tube design was (unless this was a proven inferior design) but this design is exacerbating my fine bubble issue. It looks inferior to what's advertised...



As what I've received differs considerably from what's advertised I'm think of contacting seller to complain, thoughts please. :)



Am I being unreasonable?

![images/7593efe8bd7d58b9920f25a9d2f0d272.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7593efe8bd7d58b9920f25a9d2f0d272.jpeg)



**"I Laser"**

---
---
**Phillip Conroy** *May 17, 2016 06:55*

Where do you have the water tank?


---
**I Laser** *May 17, 2016 07:02*

It is below, but I have two machines running from the same tank.



The other machine doesn't have any issues with fine bubbles and neither did this machine until I swapped the tubes out.



I used a very small drop of dishwashing liquid on the glass nipples to help get the water tubing on, so not sure whether that's the issue.



Pump has been running for well over 24 hours now and the bubbles are still there. :(


---
**Phillip Conroy** *May 17, 2016 07:12*

Just cut mdf for 20 hours with stock exhaust fan and will not see any bubbles or anything else in tube


---
**Phillip Conroy** *May 17, 2016 08:10*

i have mine tank on a extension out back at laser cutter hight,try lifting end with bubbles 6 inches and chock witb a couple of books ,run pump few hours- water drains back through water pump when off alowing air into tube that is why my tak is at tube hight


---
**I Laser** *May 17, 2016 08:24*

Thanks Phillip, but I chocked the tube on an angle last night when I noticed it wasn't clearing. It's been like that overnight and all day.



The bubbles are so fine that I'd say they wouldn't be an issue if the tube was like my others ie it didn't taper in, which has caused a ridge that traps them. My next test is to swap the pumps over just incase that's somehow causing them.



Have people seen this tube design before? Also am I being unreasonable thinking I should have got what was pictured in the advertisement (ie metal cap, non tapered)?﻿


---
**Phillip Conroy** *May 17, 2016 08:35*

Another thing you could try apart from bigger pump is swap hoses on the pump you have-reverse the flow


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 17, 2016 09:01*

Right, I think I misinterpreted earlier. I was thinking the bubbles were forming in the inlet/outlet tubing. Not the laser tube. Makes sense now. To be honest, I think that the seller advertising with pictures of metal cap means they should send a metal cap one. Not something else. Or they should at least have something mentioned in the advert (e.g. your product may vary from pictures shown).



I'd definitely try getting your reservoir tank level or higher than the tube to see if that removes or minimises the issue. I run mine with tank up on a shelf above my head height & the laser at about hip height.


---
**Stephane Buisson** *May 17, 2016 09:03*

(with water tank higher than the tube) do you get rid of some bubbles ? does new bubble appear ? does the bubbles could be the result of a chemical reaction? (different of air intake in the circuit).


---
**I Laser** *May 17, 2016 09:25*

Thanks guys, I'll try raising the tank, though it's 20kg so will need to figure out how to do that.



I've contacted seller to ask why I've been sent something different than pictured. I've swapped over the pumps to no avail. Might be a chemical reaction, the washing up liquid and antifreeze? Also tried reversing flow, they now just collect at the other end...



The design is similar to design B in this reference pic: [http://bit.ly/1qq8nWB](http://bit.ly/1qq8nWB)

The glass tapers back in well before the tubes ends. Where it tapers is a ridge and the fine bubbles trap there. As said before if the tube was the same design as my previous ones I'd most likely have never noticed it!



I can't see bubbles prior to after the coiled glass, though I think it's more likely the flow of the water slows enough by then for them to gather at the top.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 17, 2016 11:22*

It's a matter of getting them out, then keeping the positive pressure on the water (i.e. having the outlet pipe submerged). Where the fine bubbles are collecting, is there anywhere nearby that where the bubbles could exit? You might have to tilt the machine slightly to allow the bubbles to float to top & escape out of an outlet pipe. Then once that is done, if your outlet is submerged no bubbles should reintroduce. Unless it is a possible chemical reaction, as suggested by Stephane.



Alternatively just to test, you could lower the laser cutter below the level of the water tank, if that makes it easier than lifting the water tank to somewhere above the laser. If it then works, you can then determine a solution for putting the water tank higher. Fortunately I have a shelf bolted onto the wall in the garage at about eye height (not quite head height as I keep smacking my head on it lol). It seems to support the 25-30 odd Litres that I have in my tank (+ the other random stuff on that shelf).


---
**I Laser** *May 17, 2016 13:37*

Ha lucky you, no shelf mounted and only shelving I have in there is cheap and wouldn't hold the weight.



Nice idea lowering instead of raising. I still don't get how it will make a difference to be honest. I have two machines both using the same tank, only the one has bubbles. :( I'm willing to give anything a go so will try it. I'm just hoping it's some strange reaction and will settle!


---
**I Laser** *May 17, 2016 23:30*

As a follow up, when questioned about the different design, seller just stated it was best quality in China. I responded that their eBay ID and advertisement leads people to believe they are selling reci style tubes.



Why do people have to be so devious...



Anyway, machine has been below the tank for about 3 hours now and the bubbles are still there. :|


---
**I Laser** *May 18, 2016 01:09*

Feel like I'm smacking my head against a wall. The seller wants to argue the point, I've explained numerous times that the picture differs significantly from the product they are selling, yet seller still wants to argue.



At this stage I would warn everyone against purchasing from reci-laser on eBay (link in OP) as their after sales support is abysmal.



Could only imagine the saga if you actually needed the tube replaced...


---
**Phillip Conroy** *May 18, 2016 07:59*

How about taking tube  out of machine and standing up whill running pump-air pocked end up


---
**Alex Hodge** *May 18, 2016 16:28*

If there's one trick to water cooling its to always have your reservoir at the highest point in the system. Your fine bubbles will go away. Put the laser on an incline and run with the reservoir above the highest part of the tube. Run like that until all your bubbles have moved to the reservoir. Then you can set the laser back horizontal again and run like normal. But your reservoir needs to always stay at the highest point of the loop.


---
**Alex Hodge** *May 18, 2016 16:34*

An alternative to keeping the reservoir at the highest point is having a bleed/fill line. Basically a T is installed in the line before the laser. The branch of the T facing up. As bubbles attempt to pass, they instead go up the T. Works best with larger diameter tubing in my experience, but should work here too. If you can't have your reservoir above your laser for some reason.


---
**Stephane Buisson** *May 18, 2016 16:47*

**+Alex Hodge** like it, but you need a airtight closed reservoir. (or you can't open the line)


---
**Jon Bruno** *May 18, 2016 16:55*

thats funny, its not a RECI laser tube...


---
**Alex Hodge** *May 18, 2016 19:23*

**+Stephane Buisson** Eh, as long as the T is above the reservoir, you're fine. It's like a little reservoir.


---
**I Laser** *May 19, 2016 00:32*

Like the t idea, it's was on all yesterday sitting below the tank on the floor. Bubbles were still present last night.



Exasperated I decided to replace the inlet tubing, bubbles still present. :\



Placed the machine back on its stand and went to bed, this morning the bubbles seem to be clearing a little (hope it's not my imagination, this has been 3 days of screwing around!).



It's definitely not a RECI tube, the seller has nothing to do with RECI other than listing a couple of RECI products.



He's chosen to ignore the fact that his eBay ID misrepresents and is plain devious. He also thinks there's no issue with showing pictures of a product and sending something completely different. Both of which are against eBay T&C's.



The pictured tube is a metal capped coletech tube. From a little research I believe coletech made a 40w metal capped tube a few years back, not so sure they still do now though.



Anyway as per previous post, given my experience with the seller I will not be purchasing from them again or would I recommend them to anyone.



Also as stated multiple times, the design of this tube is poor. There are internal ridges on both ends of the tube, these ridges capture everything... So extremely minute bubbles that would normally exit the tube, gather at the ridge forming larger bubbles.



If/when the bubbles are gone I'll be testing power. I'm dreading any issues as getting them rectified by seller will no doubt prove impossible.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 01:08*

**+I Laser** Good to hear that the bubbles may be clearing up. Hopefully you can get it totally clear very soon & give it power tests. I'd also be dreading any issues as this seller seems quite horrid to deal with.


---
**I Laser** *May 19, 2016 04:32*

Yeah, he's being far from helpful that's for sure. He can't grasp there's an issue with his product pics or the fact his ebay ID is misleading. Apparently if he lists a couple of reci tubes within the 800+ items he's listed that makes it okay. <b>facepalm</b>



Anyway, bubbles seemed to clear so I did a couple of test fires, was quite happy with the results (around 25 watts @ 10mA) but then I checked the tube again and the bubbles are back. I'm so over it at this stage.



Looks like next step is a T, anyone know if it's possible for the tube to be defective and it's introducing gas ever so slightly into the water?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 19, 2016 05:34*

**+I Laser** That sounds plausible, since it is CO2, maybe it is introducing gas into the water. I'd imagine no way to actually test that though.


---
**I Laser** *May 19, 2016 11:02*

So I've now replaced the tube that connects the front piece to the main tube. I'd noticed between my fiddling air was getting trapped there. Still no good.



I've reversed flow direction again, bubbles are still there but look to be clearing again. Any issue with running the water from the business end of the tube to the back? I haven't fired it again so who knows.... Still thinking the tube could be faulty.


---
**Alex Hodge** *May 19, 2016 13:49*

Seems most likely that you're pulling air from somewhere besides the tube itself. You're using the stock pump? You dont have one of those venturi pumps that mix air in for the fishes? :)

No leaks? Is your pump running submerged or inline?


---
**I Laser** *May 19, 2016 23:00*

I've got two pumps, both stock (crappy) submerged types. Haven't found any leaks, checked over the tubing. The only thing I can think to do now is replace all tubing with new tube (I used some tubing I had, which should have been fine but I'm grasping at straws here). Also might try a closed T in the loop.



I just can't work out it out, no bubbles in the second machine, both use the same tank. I've swapped over the pumps, replaced tubing (as per note above), placed machine lower than resevoir, switched flow direction, waited days for the bubbles to clear, etc.



I have an S bend in the return pipe so when pump is turned off no air is reintroduced to the loop. Bubbles seemingly cleared yesterday, ran a few test fires to measure tube output and the bubbles were back... :\



Seller is now MIA to my queries.



edit: Seller has responded, they've added pictures of all glass 40w tube to the listing, only problem is this is still a different design to what I've received. In fact had I received the design they've now posted I would most likely be lasing and not ripping my hair out 4+ days later!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 01:57*

**+I Laser** I replaced my tubing with some stuff from Bunnings. I don't imagine the tubing would be the issue, unless there is a slight air leak somewhere. Maybe check your silicone connections on the tubing. Might be a minute leak in amongst your silicone? Or maybe you don't have silicone on the tubing... I put heaps on the connection area.


---
**I Laser** *May 20, 2016 02:37*

I haven't got silicone on either machines, but am willing to try anything... I assume you're referring to the glass nipples for the water inlet/outlet tubing?



Seems a trip to bunnings is in order, I don't think it's the tubing either but just don't know what else to do.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 03:43*

**+I Laser** I chopped the originally tubing about 2 inches back from the glass nipples, because I was uncertain how to get it off & thought I would probably break the glass trying. Then I got the tube from Bunnings & siliconed it onto the 2 inch pieces that were attached to the glass nipples. If you can get off the nipples, then I'd suggest straight replace, but maybe put a bead of silicone around the join to ensure no air can get in there from bad seal.


---
**Alex Hodge** *May 20, 2016 05:12*

I can't see tubing being the problem here. You're pulling air in somewhere. I suggest you get a proper water pump, run it inline rather than submerged. Keep your intake hose away from you exhaust hose in the reservoir so the bubbles don't get sucked right back in.﻿ Make sure both are completely submerged in coolant at all times. Oh and try straight distilled water for a bit.


---
**I Laser** *May 21, 2016 00:25*

It wasn't the tubing, new tubing with silicone on the glass nipples. Still got extremely fine air bubbles, seriously you'd never know they were there if there wasn't for the stupid internal ridge capturing them... Time to introduce a T just before the tube. :\



Alex, what pump would you suggest. I have a swiftech PC cooling pump but under the impression it's too powerful.



I've looked extremely hard for very fine bubbles in the other machines tube and can't see any. I just keep coming back to this particular tube being the issue....



I think the seller has discovered this thread as the picture in the ebay advert is finally correct. Not that it helps me, no offer of assistance or remedy, just blanket 'best quality in china' statements.


---
**Alex Hodge** *May 21, 2016 01:41*

The swiftech would be great. Give it a shot.


---
**I Laser** *May 22, 2016 10:04*

T-junction installed: <b>check</b>

Swiftech pump installed: <b>check</b>

Bubbles: <b>check</b>



Seriously I'm nearing the point of smashing this tube. How is it possible the tube in the machine next to this one draws from the same tank, tested with the same pump, without a t, without an inline pump has no bubbles yet this POS does?



It <b>has</b> to be the tube....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 22, 2016 10:44*

**+I Laser** That sounds quite frustrating. Is the are the bubbles are accumulating visible in the photo on this post? I couldn't work out where the bubbles were.


---
**I Laser** *May 22, 2016 21:30*

It is, I'm quite annoyed at wasting nearly a whole week trying to rectify this.



Yes you can see the bubble in the pic, you need to zoom in. Here's a copy with them circled, green circle is the stream of extremely fine bubbles, red circle shows the internal ridge where they're captured and you can see larger bubbles formed there.



[http://s33.postimg.org/mswfcqzbj/tube1.jpg](http://s33.postimg.org/mswfcqzbj/tube1.jpg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 00:50*

**+I Laser** Thanks for that circled pic. I was wondering is there anything we can do to prevent them accumulating in that spot (e.g. put some silicone in that area inside the tube) but judging from the pics that would be nigh impossible).



Another thought I had is that maybe this style of tube is made to operate vertically? Wouldn't really help much in this case without a serious modification of the K40 case, but I am thinking that if it was standing upright it may not allow the bubbles to accumulate in that position.


---
**I Laser** *May 26, 2016 22:20*

Yup, I'll put the machine on it's side and just strap everything to the bed lol.



So I'm not saying its fixed, but I had noticed when only one pump was on there were less bubbles. I wondered if my flow switch arrangement might have been aerating the water. There were no bubbles on the surface of the tank, but there were a small number in the surface of the bottles the float switches are mounted to.



I routed the water tubing straight back into the tank, I also bought 16 litres of distilled water and replaced the tanks water. Tested last night and it seems to have fixed things, though I've been here before so fingers crossed...



I'm now thinking the last time I test fired, I'd switched the other pump on inadvertently (it's on the same power board as my extraction fan). This caused the bubbles, not the firing of the laser.



Oh the sellers final input was that the output nipples should be a the highest point. I'd tried that, even though every machine I've seen/had has the wires to the tube at the highest point and the nipples on the side closest to the back of the machine. :\



Anyway if this works I'll need to replace my flow switches, all because the overly sensitive tube design. :|


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 22:35*

**+I Laser** Hope it goes well for you. If I recall correct from my machine, the nipples face the back as you said, although I'd have to check to be certain.


---
**I Laser** *May 27, 2016 08:07*

Thanks, well and truly over it. About nine days in all... On the bright side I'm more educated for the next tube change over I suppose...



Yeah the nipples faced the rear of the machine on both of the machines I bought. Most pics I've seen on here has the nipples facing the rear too. So not sure about the sellers depth of knowledge.



Also the way he practically panned off any suggestion of an issue with the tube didn't sit well. He's a drop shipper so would have no idea the actual state of the tubes that are sent.



The original listing shows the metal capped tube and also lists the brand as RECI. When questioned he stated RECI don't make 40w tubes. Anyway I don't get the impression he's at all willing to contemplate returns/refunds, if my latest fix amounts to nothing I'll be doing a Paypal reversal and will await an address to send the tube back to. Here's hoping that's not required...


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/ChVeqEWcSN2) &mdash; content and formatting may not be reliable*
