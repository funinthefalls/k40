---
layout: post
title: "Software Question. I'm trying to do a single pass cut, but it keeps doing 2 passes"
date: June 22, 2016 21:47
category: "Software"
author: "Ned Hill"
---
Software Question.  I'm trying to do a single pass cut, but it keeps doing 2 passes.  The repeat is set to 1 so I'm not sure what's going on.  Thoughts?  Thanks.

Edit: Using CorelLaser12





**"Ned Hill"**

---
---
**Tony Sobczak** *June 22, 2016 22:10*

Set the line width to .001. Any of the standard choices will result in cutting both sides of the line﻿


---
**Jim Hatch** *June 22, 2016 22:10*

Check your line thickness. It needs to be .001mm otherwise if it's thicker it will make multiple passes.


---
**Ned Hill** *June 22, 2016 22:16*

That fixed it.  Thanks so much :)


---
**Ned Hill** *June 22, 2016 22:21*

Do you leave the same line thickness for engraving?


---
**Jim Hatch** *June 22, 2016 22:28*

Engraving is done by filling in the space between lines - you need to close the objects in your drawing and the space inside the lines is engraved (or outside if you choose raised vs sunken).


---
**Jim Hatch** *June 22, 2016 22:30*

A thick line is sometimes interpreted by the software as a filled box so gets engraved. Not sure what the threshold is vs having it just make multiple raster passes as the outside of an engraved object. 


---
**Ben Walker** *June 23, 2016 16:59*

**+Jim Hatch** I have noticed that as well.  If there is any color fill at all (even white) it tries to engrave.  The fill must be absolutely transparent to prevent this.  This was a huge struggle for me when I first got the machine.  Especially when I was layering shapes.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/QTwuRug3Etg) &mdash; content and formatting may not be reliable*
