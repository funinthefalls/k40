---
layout: post
title: "My K40 just arrived in the mail today (3 days early!) however the X axis belt was broken"
date: April 28, 2017 02:30
category: "Original software and hardware issues"
author: "Darryl Kegg"
---
My K40 just arrived in the mail today (3 days early!) however the X axis belt was broken.



I assume that belt should be a closed loop?   Can anyone give me the length, width and tooth pitch for that belt, and where I can buy a replacement?    A diagram of the routing would be helpful too.



Thanks in advance!





**"Darryl Kegg"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 28, 2017 02:48*

I've heard rumor of GT3 (but I would think GT2 but I don't know), so will wait for others to chime in. 

My advice is to contact seller for replacement. 


---
**Joe Alexander** *April 28, 2017 03:16*

its GT2 from what I see on my machine, and might not be closed loop. I'd recommend removing the gantry(as you will have to anyways to replace the belt) and seeing how it attaches to the laser head carriage under/in the gantry rail.


---
**Jim Fong** *April 28, 2017 03:20*

[lightobject.com](http://lightobject.com) - X axis belt for K40



They sell a replacement.  I bought a flow sensor from them before.  If it is gt2, you can pick that up on eBay for cheap.  I have a bunch I bought for corexy printer.  


---
**Kostas Filosofou** *April 28, 2017 03:37*

The belt is MXL 0.080"/2.032mm pitch 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2017 04:00*

The belt on mine was not a closed loop, however it is screwed into the head carriage to "join" it. I believe GT2 is correct for the belt also.


---
**Alex Krause** *April 28, 2017 05:12*

Buy the minimum length from Light objects... It will be enough to to make 2 belts if you have a problem


{% include youtubePlayer.html id="_NRnRwHtjrc" %}
[https://youtu.be/_NRnRwHtjrc](https://youtu.be/_NRnRwHtjrc)


---
**Steve Clark** *April 28, 2017 16:15*

When I received my K40 after going through and tightening things, cleaning then adjusting the mirrors it worked fine. I guess I was lucky. It can be hard enough to figure out how to use the laser without it having a problem before you even start! 


---
*Imported from [Google+](https://plus.google.com/104543549241847780442/posts/XiDjdwrGqfF) &mdash; content and formatting may not be reliable*
