---
layout: post
title: "I'm selling my (new) K40 it's a 50W lasercutter, bed size 600x400 mm, motorized z axis, with tools (air compresor, water pump, honeycomb plate)"
date: March 22, 2016 17:25
category: "Discussion"
author: "Lo\u00efc Verseau"
---
I'm selling my (new) K40 it's a 50W lasercutter, bed size 600x400 mm, motorized z axis,  with tools (air compresor, water pump, honeycomb plate).  Works great for a 50W. I sell it because I need a 100W instead. 

![images/afddee83e71831c8ca07e925d6c612b9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afddee83e71831c8ca07e925d6c612b9.jpeg)



**"Lo\u00efc Verseau"**

---
---
**Loïc Verseau** *March 23, 2016 06:48*

I'm near from paris


---
**Phillip Conroy** *March 31, 2016 22:39*

check replace mirrors and focal lens(the down ward facing lens on movable laser head)can you post photos of work,


---
**Loïc Verseau** *March 31, 2016 22:45*

Mirrors and lens are very clean. But I'll post pictures tomorrow 


---
*Imported from [Google+](https://plus.google.com/102787032729416934676/posts/RFRaAW3LHg6) &mdash; content and formatting may not be reliable*
