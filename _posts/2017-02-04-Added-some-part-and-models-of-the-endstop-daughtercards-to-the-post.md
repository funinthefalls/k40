---
layout: post
title: "Added some part and models of the endstop daughtercards to the post:"
date: February 04, 2017 23:29
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Added some part #s and models of the endstop daughtercards to the post:



[http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)





**"Don Kleinschnitz Jr."**

---
---
**Alex Krause** *February 05, 2017 05:10*

I hav e a hard time with the idea that optical endstops in a dust/soot prone environment is a good idea at all... When I removed my X carriage there was such a resinous buildup that I see this a failure point and much prefer the mechanical roller lever switch 


---
**Andy Shilling** *February 05, 2017 09:16*

I have just fitted the Arduino endstops and used the original daughter card thanks to your work **+Don Kleinschnitz**.

simply de-soldered the optical sensor and the 6 pin FFC for the Y Axis and run a new cable for the X.  I Agree with Alex about how sooty these can get and unfortunately the X Axis optic isn't in the most easiest of places to clean.


---
**Don Kleinschnitz Jr.** *February 05, 2017 14:19*

**+Andy Shilling** I do not understand what you did. 

You interfaced a Y switch to the current large daughter-card where the Y sensor was ?



By the 6 pin did you mean to say the X axis?



You installed an X switch and ran a cable to where the 6 pin connector was on the large daughtercard? 



Where are the switches mounted?

What about the x stepper cable.


---
**Andy Shilling** *February 05, 2017 14:32*

**+Don Kleinschnitz** as you can see using your diagram I worked out I could completely remove the optical endstop for X axis, remove the 6 pin FFC connection and solder the arduino endstop direct to the daughter board using the pins as illustrated. This allowed me to use what I had rather than having to wait for a middleman board.

![images/b0d474e56c18c124371eca7cd366b426.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b0d474e56c18c124371eca7cd366b426.png)


---
**Don Kleinschnitz Jr.** *February 05, 2017 15:02*

**+Andy Shilling** ah so you replaced the X opitcal sensor with a switch. I am surprised it fit. 

Can you point me to the arduino end-stop you refer to?

So the x optical end-stop is still there?



I am still confused how this elliminates a middleman?  You still have the ribbon cable going to the controller right?


---
**Andy Shilling** *February 05, 2017 15:19*

Sorry calling them arduino is probably wrong. I also didn't mean eliminate the middleman only enable me not to have to stop working because I didn't have a middleman board. 

The Large FFC from me C3D mini goes to the daughter board then I have wired the new endstop as I explained above. 

Here is the link to the endstops.



[amazon.co.uk - Signswise 3 Pack Mechanical Endstop for 3D Printer Makerbot Prusa Mendel RepRap CNC Arduino Mega 2560 1280: : Baby](https://www.amazon.co.uk/Signswise-Mechanical-Endstop-Printer-Makerbot/dp/B015H14MIW/ref=sr_1_1_a_it?ie=UTF8&qid=1486307653&sr=8-1&keywords=arduino+endstop)


---
**Don Kleinschnitz Jr.** *February 05, 2017 18:32*

**+Andy Shilling** ok thanks. Got it now, a bit slow, its Sunday. I like that switch assembly.


---
**Andy Shilling** *February 05, 2017 18:38*

**+Don Kleinschnitz**​ yes really nice to work with and with the 3 holes in them means you have a bit of choice when it comes to mount them.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Qk8pe4r7hd1) &mdash; content and formatting may not be reliable*
