---
layout: post
title: "Finished the cable chain and getting the new light objects head and lens in"
date: November 19, 2016 07:00
category: "Modification"
author: "Kelly S"
---
Finished the cable chain and getting the new light objects head and lens in.  :)  Took a while to find a soft enough hose to run through the chain, ended up cutting up one of my air brush hoses that have a fabric shell, super soft tubing and pretty tough as well.



![images/4c27fd641faebaee3ea13d928f697dc8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c27fd641faebaee3ea13d928f697dc8.jpeg)
![images/950e018869f4d6c93a4dc11fb535ae4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/950e018869f4d6c93a4dc11fb535ae4d.jpeg)

**"Kelly S"**

---
---
**Alex Krause** *November 19, 2016 07:10*

Where are you routing the air out of the machine?


---
**Daniel Wood** *November 19, 2016 07:43*

That looks excellent. Really nice job!!


---
**Kelly S** *November 19, 2016 13:31*

**+Alex Krause** through the hole in the back it came with.  :)  And thank you **+Daniel Wood**, it definitely took some fanaggaling haha.


---
**Kelly S** *November 19, 2016 16:22*

Here is a link to the thingieverse file I used by the way [http://www.thingiverse.com/thing:1275013](http://www.thingiverse.com/thing:1275013)



[thingiverse.com - Cable Chain and Mounts for K40 Laser by DAcreates](http://www.thingiverse.com/thing:1275013)


---
**Ashley M. Kirchner [Norym]** *November 20, 2016 04:04*

Nice work. Reminds me to go take pictures of my setup.


---
**Kelly S** *November 20, 2016 04:09*

**+Ashley M. Kirchner** Curious to see.  :)




---
**Kelly S** *November 20, 2016 19:11*

Vent cut and back in, and finding focal point of lens.

![images/c39719dd6ca793df33fab04b56afbcba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c39719dd6ca793df33fab04b56afbcba.jpeg)


---
**Ashley M. Kirchner [Norym]** *November 20, 2016 21:00*

Mine is over the top, and there's a second one on the side that goes to the back of the enclosure.

![images/1ed0f59c11673f059e74b27f2b1f061c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ed0f59c11673f059e74b27f2b1f061c.jpeg)


---
**Kelly S** *November 20, 2016 23:31*

**+Ashley M. Kirchner** that is neat, I have a hinge on the right side that hangs all over, I like how it looks over top though, very cool.


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/et1HZ9t2kVr) &mdash; content and formatting may not be reliable*
