---
layout: post
title: "Well I have been using this k40 for a couple years on and off without any issues"
date: January 16, 2019 19:11
category: "Discussion"
author: "Jeff Price"
---
Well I have been using this k40 for a couple years on and off without any issues. I have also upgraded the control system as the original one died. I was having a power issue and decided to clean the lens and accidentally put in backwards. Then my last mirror broke in half. I believe because the lens was backwards and I was etching stainless steel with the spray stuff. Anyway, I purchased 3 new mirrors and have broke 2,  one before I realized the lens was backwards and one after. I was not on stainless when the last mirror broke. Right down the middle on all of them. Anything I am missing? I have not changed anything else. 





**"Jeff Price"**

---
---
**Ned Hill** *January 16, 2019 21:56*

Having the lens in backwards should not affect the mirrors.  It would just lead to a less focused spot on the work surface.  



Mirrors can break for a couple of reasons: mechanical or thermal stress or a combination of these inputs.  Mechanical stress could be imparted by having the mirror holders applying too much / uneven pressure.  Thermally the mirrors can heat up if they are absorbing laser energy.  For quality mirrors they should not be absorbing any of the beam energy.  However dirt on the mirrors can cause energy absorbance and can then lead to cracking from the spot heating of the mirror.  



What kind of mirrors are you using (i.e. Gold plated silicon, Moly)?  Laser mirrors are also first surface mirrors so be sure to have the proper side facing the laser beam.


---
*Imported from [Google+](https://plus.google.com/112939900204378074264/posts/GuGrtkAnqwh) &mdash; content and formatting may not be reliable*
