---
layout: post
title: "Okay, I have a few questions.. I am attaching a Temp sensor to the tube, and was wondering what the normal temp range of the tube is?"
date: March 20, 2015 04:31
category: "Hardware and Laser settings"
author: "Sean Cherven"
---
Okay, I have a few questions..

I am attaching a Temp sensor to the tube, and was wondering what the normal temp range of the tube is? And at what temp should the tube be shut down at?





**"Sean Cherven"**

---
---
**Dan Shepherd** *March 20, 2015 14:32*

 The optimum water temperature for a C02 laser is 22 ~ 25 degrees C. Be careful of thermal shock when adding water for coolant. Water temperature over 10 degrees different from ambient temperature can cause thermal shock that can break the laser tube. Add the water slowly while the pump is running and the laser is turned off. High water temperature will shorten the life of the laser tube and water temperature too cold will lower the output wattage. Use a thermometer and keep it in the 22-25 degrees C range.   There is a lot of documents about installing, using and upgrading the k40 in the Laser Engraving and Cutting facebook users group, files section  [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
**Sean Cherven** *March 20, 2015 14:44*

I was asking about the actual tube temperature, not the water temperature. I am adding a temp sensor to both the water and the actual tube. 



I need to know the temperature of the actual tube (I'm going to tape a thermister to the outside of the tube).



But thanks for the water temp info, that did help me out also.


---
**Tim Fawcett** *March 20, 2015 20:34*

Hi Sean



as I understand it - the critical thing is the water temperature - it should reflect the temperature of the gas in the tube. If you go much above 28C then the lasing efficiency goes down. IIUC the next stage depends on what glue has been used to fasten the mirrors on. The tube gets too hot and they fall off.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/Yg2AuGNU3Th) &mdash; content and formatting may not be reliable*
