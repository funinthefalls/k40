---
layout: post
title: "heres an idea maybe its been thought of"
date: July 04, 2017 06:16
category: "Modification"
author: "William Kearns"
---
heres an idea maybe its been thought of. If i cut the bottom out of the K40 and attach a cutting bed the same size as the opening between the x and y rails and have it positioned under the machine so it can accommodate deeper objects?

![images/2be2965d2323523664ac66f9919b809d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2be2965d2323523664ac66f9919b809d.jpeg)



**"William Kearns"**

---
---
**Jim Hatch** *July 04, 2017 12:59*

That's doable and as you pictures, something like a lab jack will be useful to give you an adjustable bed. However when removing the floor you'll want to reinforce the remaining structure so it does not flex. The rails get their positional rigidity from the case and removing a significant portion of the base will affect that.



You will also need to make sure you operate it only when everyone in the room is wearing proper laser safety glasses as now you'll have a Class 4 laser.



You could look at the way FSL did something similar to what you're suggesting with their expansion bottom on their Muse laser.



Keep us posted as to how it goes.


---
**Jim Fong** *July 04, 2017 13:14*

I have one and find it to be very useful. I laser lots of odd sized wood pieces so having quick adjustable height is nice.  The lab jack does wobble a bit so just have to make sure larger pieces are parallel. I made a simple height gage that is marked 84mm from bottom of the laser.  I use that to set focus (50mm from lens)

I was thinking about getting a larger one.....



[https://plus.google.com/photos/.](https://plus.google.com/photos/.)..


---
**Jim Hatch** *July 04, 2017 13:21*

**+Jim Fong**​ Did you cut the bottom out of your K40 or just put the jack inside the existing case? I have that jack and agree that it can wobble. It also takes about 2" of depth away. The OP question about removing the bottom (assume that's not a mistake that means the bed inside) is one I've thought of but not done. The Muse has a factory solution that does just that so we know it's doable.


---
**Jim Fong** *July 04, 2017 13:29*

**+Jim Hatch** i misread the op comment. I haven't cut the bottom yet.  I really need to do that for the rotary axis. 


---
**Anthony Bolgar** *July 04, 2017 14:55*

I cut the bottom out on one of my K40's, and then use steel studs frome Home depot to frame in the botttom, that way it is still enclosed properly, and I gained 3.5" in height


---
**Nathan Thomas** *July 04, 2017 16:29*

**+Anthony Bolgar** do you have pics of it anywhere? I'd like to see what it looks like, love the idea


---
**Anthony Bolgar** *July 04, 2017 16:39*

I will take some pics today, will post them later today.




---
**William Kearns** *July 04, 2017 17:17*

Old work bench will mod with casters and some of those adjustable screw feet to level in place. ![images/07bd05edea63e9d5344525a39293e1cc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07bd05edea63e9d5344525a39293e1cc.jpeg)


---
**E Caswell** *July 04, 2017 18:19*

I have one of the 150x150 lab jacks, taken the bottom section off and just have the screw jack and the top section.  

I test run a nema 17 connected to the screw thread with a coupler and it powered the jack easily and was fairly smooth.

 That's my next mod once I get the AAxis finished. :-)


---
**William Kearns** *July 04, 2017 19:19*

Exhaust duct shortened de-burred edges won't cut you!![images/55a45fb63e26711e5123a81c54493863.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55a45fb63e26711e5123a81c54493863.jpeg)


---
**E Caswell** *July 04, 2017 19:58*

**+William Kearns** I removed mine altogether and just cut a 110mm Dia hole around the original oblong slot to suit the stub for the extractor in the back. (I had to blank off in some of the oblong tho)

Fitted a bigger extractor and does the job really well now. much happier with extraction.


---
**Madyn3D CNC, LLC** *July 08, 2017 21:21*

I used a scissor jack before I upgraded to a Z axis, I 3D printed mine, worked great! 

![images/92f936391f12afbf5be27832f3128fa8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92f936391f12afbf5be27832f3128fa8.jpeg)


---
**Christopher Elmhorst** *July 18, 2017 15:31*

**+William Kearns** I did the same thing, and then painted it flat black, can barely even see it! [https://plus.google.com/photos/.](https://plus.google.com/photos/.)..


---
**William Kearns** *July 18, 2017 15:44*

Cutting bottom out for z table (lightobject)going to double its travel distance ![images/6508a34e0756e54d5106fe973d04d6e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6508a34e0756e54d5106fe973d04d6e4.jpeg)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/2YDgrPvYNmn) &mdash; content and formatting may not be reliable*
