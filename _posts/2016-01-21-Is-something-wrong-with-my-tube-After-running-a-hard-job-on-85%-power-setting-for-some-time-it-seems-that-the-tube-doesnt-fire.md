---
layout: post
title: "Is something wrong with my tube? After running a hard job on 85% power setting for some time, it seems that the tube doesn't fire"
date: January 21, 2016 18:34
category: "Hardware and Laser settings"
author: "Sunny Koh"
---
Is something wrong with my tube? After running a hard job on 85% power setting for some time, it seems that the tube doesn't fire. This is the sign of a failed tube?

![images/3a739a57d648963e304251b35a87da4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a739a57d648963e304251b35a87da4b.jpeg)



**"Sunny Koh"**

---
---
**Scott Marshall** *January 22, 2016 04:33*

Could be your power supply as well. Need info. 



Did it shut down or finish the job? Water temp? Any meter indication? Smell/feel hot?  Does the motion control power up? Give us some background info please.



Hoping it's minor...



Scott


---
**Sunny Koh** *January 22, 2016 12:20*

It is not minor, after 1/2 a day when my other company staff was QQ the technical support in China, they say that the tube is damaged due to water circulation issue. Which is not not likely as I have the pump running and the bucket is a 15 Gal in size full. The last job was a cut at 1mm per second at 85% power which is normal. It was to cut a 10mm thick piece of Acrylic into Pen Holders.



For replacement, they want me to send back the Tube to their factory. Considering that I got them to post the machine to Shenzhen when I collect it, (The Machine costs 2400 RMB)  it is not so good as the logistic chain to get a new tube in.



So I just ordered a new tube (720 RMB) to be sent to my associates in Shenzhen and trying to figure a way to get a cheap flight during peak travel season (Next 2 weeks) to get the tube back into Singapore from China. (Chinese/Lunar New Year is on the 8th of Feb, the whole of China stops work)



Hopefully when I am there I can purchase parts for the exhaust system as I was running the machine open with a fan blowing out the window. Temp outside is about 30 degrees Celsius (86 Fahrenheit) Thinking if I need to actually get a water chiller instead of using the bucket.



Be in the US in 3 weeks time, hopefully can get some parts.



Thinking that this machine needs a Ampere Meter and other items. 


---
**Scott Marshall** *January 22, 2016 14:55*

Sorry to hear that. 

As you mentioned, It does need safety interlocks, a decent fan etc as you  mentioned. It sounds like you have the additional obstacle of difficult parts procurement/shipping. Maybe you can work at some of the peripheral upgrades while trying to arrange transport for your new tube. Sounds like you're getting decent pricing on the tube, 720RMB is about $110 USD, (I couldn't get one that cheap)  but if the tube should be under warranty , it's no deal.



I'm surprised how tough it is for you to get parts sent into Singapore. Is it a customs issue? I can get parts from all over the world here in the US Via DHL etc. for pretty reasonable rates. I just ordered a belt drive (around 4kg) for my milling machine out of Turkey, and it's coming via "National Mail" for $30 us in 5 days. (so they claim anyway - I just jinxed that)



I can give you sources for parts in the US if you are planning to hand carry them back to Singapore. It depends on just where in the US you'll be visiting what's easily available over the counter. In the Industrial Cities you can buy almost anything, outside of them, I often find it cheaper, faster to order from Asia. Funny thing is most of them COME from your neck of the world in the 1st place. 



I've been lucky and have collected a couple magnehelics for water and airflow monitoring, as well as low priced interlock switches, thermometers, etc. - mostly used from the surplus/decomissioned industrial market. I got a 800cfm  blower and 8"duct (which are way overkill for just the laser, a 4" ought to do it fine) from California where they are sold for greenhouse use. 



My laser cutter was severely damaged when it arrived, and they gave me $200usd to repair it. The damage was quite extensive and I just got done making a new head and re-assembling. (Mill gears let go while cutting the new carriage head) Laser fires, steppers run. Real test today sometime.



Hey, it's a hobby, just keep having fun..... 

The trip can be as rewarding as the destination.



Scott﻿


---
**Sunny Koh** *January 22, 2016 16:08*

**+Scott Marshall** Thanks. The issue with Singapore is that we are big enough that we can buy certain items but small enough that we don't have certain easy access to things. The plane ticket when it is on discount to Hong Kong is well, around USD$200.00 which makes it a good shopping trip for parts. It was cheaper for me to fly into Kong Kong, cross the border into China, collect the machine and fly back then it is to ship it out of China direct which was exactly what I did.



I think the easiest example is the price of a Dremel, a basic one in the US is around USD$30, here I am looking at SGD$80 (USD$56) And the headaches of 110 vs 240. We don't have big hardware stores like Home Depot or Lowes. So sourcing for things become a hunt in finding who sells the things and at what price. Somethings are cheaper and easy to find, but the prices on certain items such as certain shields well, the price increase is so shocking. (I am working on a project that requires a BT LE Audio board, on locally SGD$50. one from China or Amazon US, USD$10)



The other real issue is cost and time for shipping. USPS costs are really very expensive to ship anything out of the US. And anything from China takes 2 to 3 weeks.



I will be LA in Manhattan Beach for the LA International Pen Show (That is why we wat to get this machine to engrave pens for people as a cheap value add). And I know it is walking distance to the smallest Fry's in the US. I was trying to laser cut some pen displays when this happens.



Should be in the bay area in March so that would be another chance to get things.



I think the whole episode taught me that the mind the exhaust, as although the room can be air con, with a joke of a stock fan to extract the fumes and no where to go, and temperatures at 90s, even with 15 gallons of water, the pump and water temperature should not be taken as a thing for granted. 



Which brings down to the question, what is the water temperature and flow rate people have especially during those long laser sessions? Seriously considering a CW-3000 instead of the 15 Gallon bucket.


---
**Scott Marshall** *January 22, 2016 17:21*

Sunny,

Thanks for the explanation. Sounds like you get to travel a fair bit. I was forced to close my Industrial controls business a few years back because of health issues, and I really miss the work, and the travel. It's why I have a house and shop that look like a a cross between a junkyard and NASA.



On the chiller requirement, just above ambient is all you need to keep the tube safe. Some claim you can squeeze out a little more power by chilling it down to 10c or so, but if you need that few percent, you've got too small of a machine to start. Another issue besides the cost of running an actual chiller is condensation on the tube causing arcing and corona discharge issues. You know better than I just how much the heat in your tank rises. My effeciency  figures show no more than 200watts going into the water (.015a x 15kv = 225w) I've got mine set up with a 5 gallon bucket, a $15usd transmission cooler as a heat exchanger mounted to the front of my laser cabinet, so that the make-up air goes thru the heat exchanger on the way in. (the bonus there is it blocks laser light from getting out, and I'm doing double duty with my exhaust fan).

I consider that overkill, but I'm just getting to where I'll be pushing it for longer periods. I doubt the heat exchanger is even needed. I put a mesh coffee filter on the return line and let the water cascade into the tank from there, thus giving an audible confirmation it's really flowing. I have a cheap Acrylic paddle flowmeter on the way. 



It's my personal opinion a chiller is extreme overkill on this machine. Those Cw-3000 look like pretty crappy machines for the money. I'd build one using a "dorm fridge" 1st.



Try a transmission cooler or a heater core in the loop, and add some airflow if you really build heat. One other "cheapie" solution I've thought about is simply running a loop through my shop fridge and valving it in circuit if the coolant temp goes up. You could valve it with a couple of $10 3-way solenoid  valves and use a low buck temp controller , or just use a manual valve. I guess some guys use "blue Ice" packs, or just throw in Ice when it warms up. Whatever works.



These are the kind of solutions that won't win engineering awards, but they get the job done. If I didn't use used, re-purposed, and surplus equipment, I couldn't afford most of my 'toys' (Laser included).



I've done this sort of stuff for 40 years, and have learned that just because IBM does it, doesn't mean it's "normal" for the rest of the world with a budget.  Of course, they'd spend 50 grand to engrave a few pens in a heartbeat....  (I know, I used to sell them $5000 hot plates for wafer curing)



If I were you, I'd check the power supply out and make sure the tube is bad before replacing it. If I had to change mine, I'd look at the better ones,(RECI) -  they cost more, but have a MUCH longer expected life time 10k hours for most of them, as opposed tot he generic all glass ones which state 2-3k hours expected life, with sudden death seeming to be a frequent occurance based on reports here.



As always, sorry to run-on. Hope some of it helped.






---
**I Laser** *January 22, 2016 21:38*

Not sure if it's true or not but I've read multiple times not to let the water go above 25c. If you're in 30c ambient temps, it's likely your water would be routinely sitting above that even when not working the machine ( I know mine does ;) ).



Apparently the CW-3000 is a glorified radiator and fan setup, so completely useless. Like others here I put frozen bottles of water into my tank to keep the temps down.



Also sounds like you've got a digital machine, so 85% is an unknown. Apparently you shouldn't be putting more than 18mA through either, so you might want to invest in a meter too.



Hopefully the above helps and it's not an expensive time consuming fix to get things going again!


---
**Sunny Koh** *January 23, 2016 02:20*

**+Scott Marshall** Thanks, actually what you said is most helpful, seems that I will be double checking the water temperature when I get into the office. Right now thinking if going the 15 Gal bucket may not be a good idea as it is too big to use ice packs or the blue ice.



Actually just emailed Beijing RECI and asked if they have a tube that size. Their smallest tube seems to be a 90W Tube which is 1200mm long and 80mm diameter. I know that the tubes our machines use is up to 1000mm long for the 50w but the diameter is 50mm. If they do, might spring for it with their power supply if it works. Based upon my research, I would need something of the 25 watt for what I need to do normally but big is definitely "better". A quick check on taobao shows that the tube is 2080 RMB but they have a S1 75W tube for 1480 RMB. But all are 80mm size.


---
**Sunny Koh** *January 23, 2016 02:34*

**+I Laser** Thanks, so the ampere-meter is a must and so is a temperature probe for the water. I understand that the ampere-meter goes inbetween the return from the laser tube into the power supply but other than that do know have much information. Should I get a Digital version or analog? Reading on light objects site has something about isolating the power supply to it if you are running it on the same source.


---
**I Laser** *January 23, 2016 03:41*

I previously posted about the amp meter as it's something I'm not knowledgeable about, Scott provided some excellent information. But yes it goes inline on the negative side. As for analog or digital, I went analog only because I've already got an analog machine and figured that works well enough so bought the same model meter. If I could link to the post I would, but not sure how to, so you'll have to do a search for it ;)



And yes definitely wise to keep an eye on the water temps. As Scott pointed out, the laser runs better at lower temps, as the temps increase the power degrades, someone else will have to explain the physics. ;)



For what it's worth, I've got a 15 litre tank, I have multiple 3 litre frozen bottles of water which bring the temps down below 15c for hours whilst working, even on a hot day.



Just re-read the thread and noticed you were cutting 10mm acrylic. To be honest I'd think cutting 10mm acrylic in one pass is well beyond the machines capabilities!


---
**Sunny Koh** *January 23, 2016 04:34*

Checked the water temperature in the 15 gal bucket and found it to be 32.1 Degrees Celsius even without load from the last while the pump is operating. 


---
**Scott Marshall** *January 23, 2016 07:00*

I'd have to assume that's ambient? You are indeed cooling the pump as well, but I'd expect that heat loss to the environment would easily take care of that, and with that much water, it's going to take quite a long time to reach equilibrium. That's the advantage of the large volume tank, it will slow down changes, but ultimately, you're going to have to actively remove the heat. For someone cutting out a few parts an hour, then a long wait, that alone may do it. 



I'm going to make some assumptions here, correct em if I'm wrong. Your ambient temp is 31c (90f) and you run for an hour or more each run, with a short pen re-load, then another long run. It sounds like your duty cycle is quite high for your engraving. You have a lot of small heat inputs building up, and a low differential to your environment.



A few ideas to help (I hope). 1st of all, shut down your pump when not running the laser or cooling down the system. 2nd  thought – use an efficient thermal conductor for your tank walls. I have a old deep fryer tank made from stainless steel. Plastic is an insulator. Get the tank up off the floor on small legs or blocks to allow airflow under and all around the tank (don't put it in a closet or such). Cascade the return water down the tank side or a cookie sheet to take maximum advantage of the water movement (this will also create evaporative cooling, something you may or may not want to do).

Air condition the room. Getting the ambient temp and humidity down is better for this kind of equipment. I've installed air conditioners in larger control panels  - it's that important in some climates (Zimmer in Puerto Rico was a bear).



Other, more pro-active solutions are:



>>using city water over a heat exchanger to cool your process coolant This is only an option if city water is cheap, readily available and it's politically correct in your locale to use it and drain it. If your water source is underground piping, tap closest to the building inlet to get the coolest water. Insulate tubing.



>> If you are a junk collector, wall mounted water fountains, Dorm Fridges, Even full size freezers/fridges can be pressed into service for use as they are, or you can use the compressor and heat exchangers to build your own chiller. Stay away from peltier effect modules, they are inefficient and power hogs.



>>Look to aquarium hobbyists selling out their gear, small chillers are common in the Aquarium world.



>>Computer water cooler systems can be had cheaply (especially used) and although small, can be ganged, and you only need to move 200-250w, well in the range of serious gaming computer systems.



I think the small heat exchanger combined with room AC would be a practical, efficient solution. If you're in a small room, or can close it off, small Air Conditioners aren't too expensive, and provide other benefits like human comfort and reduced humidity. Just make sure to run a makeup air duct for your Laser exhaust or your exhaust will pump your nice cool, dry air right outside. 



I just had a thought, I have a couple of floor air conditioners that use flexible ducts and a window insert to circulate outdoor air through them. You could kill 2 birds with 1 stone if you used such a unit, and drew your outdoor air from the laser exhaust, connecting the air inlet to the back of the laser, make up air into the laser cabinet to the other flexduct and so on. That's kinda out there, but would work. You would get an exhaust system, laser cooler and room AC all in one. The only downside is you would be running smoke thru your evaporator.I'd buils a plywood box and put a furnace filter (fiberglass) inline. It ought to work well if your smoke is “clean and light”. It's the kind of plan that some people wouldn't go for, but will work. It will require thinking it through.



Just a thought. I may look into that myself.



Ran long again. Like I said, I miss my work.

Glean what you can... 



Scott




---
**beny fits** *January 23, 2016 07:07*

i had the same trouble i put in a new lazer tube and it still did not work i have orded a new power sapply as i have been told that the transformer in the power unit can fail and the transformer can be replaced but it was just as ezy for me to get a full new unit as its hard to get anythnig good in australia lol


---
**Sunny Koh** *January 23, 2016 08:01*

**+Scott Marshall** Thanks. I was honestly surprised when I used the meat thermometer on it. I did not expect it to be higher than the room temperature by a few degrees. My duty cycle is actually not so bad but I was trying out the machine to get parts ready for an up coming event. (Expect 5 minutes of use and sometimes heavy use when getting big orders)



Right now looking into active cooling solutions. The tap water here is also hot. My business partner is suggesting a Aquarium cooler, about 1/10 HP. Costing about SGD$300 new which is at least better than the 2500 RMB I am looking at for a CW-5000.  



The room is aircon but the air-conditioning was off as I don't have an effective exhaust system in place yet. So the whole point of the matter was, hot room, big bucket, long cutting cycle and that ended up with a overheated tube.



First order of business is to fix the exhaust because acrylic pens (PMMA) is what I would be engraving half the time. The other is coated metal pens. Hopefully a 100 to 200 CFM solution will be enough. Though I am budgeting for a 240 CFM system right now. (Any idea guys as it seems some websites suggest a 375 to 400 CFM for US based lasers)


---
**I Laser** *January 23, 2016 09:24*

Ha love the double up there **+Scott Marshall**, aircon and exhaust at the same time.



I;ll repeat what I said earlier **+Sunny Koh**, trying to cut a 10mm piece of acrylic on these machines is asking way too much. You need to throttle down the power and go over it a number of times. 85% power at 1mm will kill your machine for certain, especially given your tropical climate!


---
**Scott Marshall** *January 23, 2016 15:39*

Seems like a bunch of us think a lot alike...



The Aquarium chiller may be a little small. See if they have specs on it's capacity and do the math before buying.



Blower specs are VERY vague. Most don't spec pressure differential, temp, of if and what kind of ductwork is affixed. A good 4" centrifigual inline would move about 200cfm, ought to do it. Avoid expanding duct, and put a fiberglass furnace filter or aluminum grease trap filter (like you find in a range hood) on the cabinet outlet (unscrew the foolish extension 1st, and pitch it) The filter will keep embers, or light parts from ending up in your duct, or worse - your blower wheel.



I'd Keep a good fire extinguisher handy, and don't walk away... 



Cut a small rectangle and put a similar filter (or transmission cooler) on the front just below the window, this will supply your makeup air, providing FLOW - which you can't get without letting air IN (a serious design flaw of the CCLC). If you need to preserve your AC, make a shallow airbox (can be as simple as cardboard and duct tape) over the inlet filter and run a duct out to  a window or such. Plastic 4" dryer duct may do the trick here.



Oh, tape up or otherwise fix all the holes in the bottom/electronics enclosure etc. They'll just ruin you nice smooth flow you've just created across your rack of pens....



If you do these things, you'll find you don't need a huge fan to draw off the smoke.



I'll post pictures of my setup soon. Just got it running and have been off track lately. It's still pretty a work in progress. As is my milling machine now. One thing leads to another, as you are aware...



Scott


---
**Sunny Koh** *January 23, 2016 17:09*

**+I Laser** Well, I was actually had it one multiple passes as I know that the most the machine can do is 5mm. The problem started in the middle of the first pass I guess. 



The aquarium chiller costs SGD$350 for a 1/10 HP version which should solve the problem, but I think plus the pumps, a CW-5000 from China looks pretty reasonable given I should be replace the pumps as well.



Does anyone know the temperature difference between the water input to the tube and the output? Trying to size different solutions right now.


---
**Sunny Koh** *January 23, 2016 17:26*

**+Scott Marshall** Don't quite understand, are you trying to say I should work on a system to draw air from outside and seal the system so that the air-con doesn't leak?



Second part is something I am trying to understand is I shouldn't change the size of the ducting? I am currently considering between a 4" or a 6" duct fan. The 6" Model is this one [http://forsythia.en.ec21.com/Duct_Inline_Fan_Hf_150p--8130240_8168721.html](http://forsythia.en.ec21.com/Duct_Inline_Fan_Hf_150p--8130240_8168721.html)



While the 4" Model is either [http://forsythia.en.ec21.com/Duct_Fan--8130240_8130241.html](http://forsythia.en.ec21.com/Duct_Fan--8130240_8130241.html) or [http://www.amazon.com/Rule-240-Marine-Blower-4-Inch/dp/B000O8D0IC/ref=sr_1_1?ie=UTF8&qid=1453569900&sr=8-1&keywords=4%22+dc+240+fan](http://www.amazon.com/Rule-240-Marine-Blower-4-Inch/dp/B000O8D0IC/ref=sr_1_1?ie=UTF8&qid=1453569900&sr=8-1&keywords=4%22+dc+240+fan)


---
**I Laser** *January 24, 2016 01:54*

Ah sorry, the way it read (85% @ 1mm) sounded like you were trying to get through it in one pass.


---
**Scott Marshall** *January 27, 2016 07:57*

If you run the machine bone stock, the air is drawn out through the small rectangular duct. this creates negative pressure in the cutting chamber, which draws air in through gaps, leaks, and holes all over the cabinet. This causes random unpredictable air currents at the workpiece, and further reduces the effectiveness of the stock fan. 



What I'm saying it if you control the air inlet so you have the incoming air 'washing' the workpiece, even the stock fan works pretty good. 



What I did is remove the factory duct assembly (it's just attached with 4 machine screws  around the outlet flange), That simple mod greatly increases the airflow POTENTIAL. To harness it, you must control the inlet air. A good place and easy way to start is to just crack the door open by 10 or 15mm and see how your smoke is carried away. 



If this works well for your pens, you can cut a air inlet in the front wall of the cabinet. (the sheet metal cuts well with a Dremel or die grinder cutoff wheel). Seal the other air leaks to improve the flow if needed.



If you find the smoke is depositing on the pens near the rear, you may want to build cardboard air dams to direct the flow, or turn the pens parallel to the airflow.

The arrangement works just lke an airwash on a woodstove glass door (if you're familiar with them)



To sum it up, the biggest problem with the stock fan is  the poorly (or lack of) designed airflow path. The stock fan is actually not too bad for fairly 'clean' (less smokey)work if you let it breathe by removing the slotted duct and provide an air inlet. 



To finish up the exhaust system, you can add flow controls (slotted doors like a barbecue grill or woodstove inlet) cover any inlets with filters that will block light if that's a concern, and make flow ''directors" from any handy material (cardboard to sheet metal).



I've discovered on sheet stock, smoke tends to deposit on the surface toward the rear, so I just lay a sheet of cardstock on it.



The last "touch" I'm considering (may do it tonight) is to add a heat exchanger to the airflow, it's free cooling, and covers the inlet hole. Here's the one I bought     [http://www.ebay.com/itm/NEW-Auto-Transmission-Trans-Oil-Cooler-Car-like-Hayden-403-/172071896277?vxp=mtr](http://www.ebay.com/itm/NEW-Auto-Transmission-Trans-Oil-Cooler-Car-like-Hayden-403-/172071896277?vxp=mtr)



It's about an 30mm too tall for the front surface, but seems a good fit for the left side. If I move the inlet to the left side, the airwash will be left to right, which may suit your pen etching well.

Think of the inlet air as 'blowing' across the workpiece.



On the duct size, 4" is plenty unless you are running a very long distance, in which case you may have to go up on the size to overcome the restriction. Under 5 meters 4" should work fine unless you have a real weak fan.



The fan you linked wouldn't be my 1st pick. It would work OK though.

 Given the possibility (even though it's remote) of fire or hot embers, I would stick with a metal unit. That fan is appears to be a "fan in a tube" arrangement rather than a 'squirrel cage' blower wheel type. The fan in duct type aren't tolerant of high pressure differentials, and are generally lower performance units. That fan is also a 2 speed, which isn't all bad, but they generally can't be used with infinite speed controls,  which are a better choice for the laser cutter. (although it DOES say it's speed controllable.) 

The only 'killer"  issue with it is the Polypro body. It's very flammable.



Shoot for a 175 CFM or better metal fan rated for use with a speed control.

 It will cost a bit more, but you'll be better off all around.

A little bigger or smaller won't be a deal breaker. Like I said, The stock fan CAN be made to work just fine. It's just not ideal.



Here's the one I bought. 

[http://www.ebay.com/itm/160875914720?_trksid=p2057872.m2749.l2649&var=460157320530&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/160875914720?_trksid=p2057872.m2749.l2649&var=460157320530&ssPageName=STRK%3AMEBIDX%3AIT)

They are not the cheapest, but are built well, all metal, hammertone powdercoated. well balanced, and should contain a fire if that ever happens.





I bought  an 8" because I'm using it for other machinery. the 4" model is more than adequate for the K40. As I suggested before, put the fan at the far end of the duct and run a speed control.



With a speed control and dampers, you should be able to adjust the system to meet any needs, even a much larger laser if you upgrade later.



All this is just my personal opinion, but I do have some experience in this sort of thing. There's a lot of ways to set up these little cutters, and I'd bet there's no 2 the same out there, but they  work fine. Use what you can get, and what feels right to you. Just be safe.



As usual, It's long, but I hope it's useful.



Scott



﻿


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/cB3EraQC9gs) &mdash; content and formatting may not be reliable*
