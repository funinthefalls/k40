---
layout: post
title: "I made my first edge lit sign today!"
date: December 23, 2016 01:52
category: "Object produced with laser"
author: "Bob Damato"
---
I made my first edge lit sign today!  Sort of :)   I restored a '55 Thunderbird with my dad a couple years ago, so I decided to make the Tbird logo. It came out pretty decent...   



I need to build a box for the bottom now.  

Heres why I say 'almost'.... I did the engraving with the Laser, but had to cut it out by hand.  No matter what I did, I couldnt get cut lines where they belong. Heres  what I did, maybe someone can tell me what I did wrong.



In Corellaser, I imported the Tbird  bmp.  On a new layer, i used the line tool and made straight lines where I wanted cuts.  When it came  time to cut after the engrave, it was all over the place.  Luckily i had so little faith in my abilities that I had the laser switch off.  



Is there a better  way? What did I do wrong?  I posted the CDR here: [http://maxboostracing.com/tbird.cdr](http://maxboostracing.com/tbird.cdr)



Open to any criticisms or comments.

Thank you!



bob



![images/954783374af072fafa9fa2aa2cc04b5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/954783374af072fafa9fa2aa2cc04b5b.jpeg)



**"Bob Damato"**

---
---
**Jonas Rullo** *December 23, 2016 02:47*

Cool! I wish I had a larger bed so I could make really big edge lit stuff. I've been making larger pieces using CNC and get similar results with the right acrylic. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 23, 2016 04:31*

That looks great. In regards to what you did wrong for cutting, I'll take a look at the CDR file & see if there is anything I can see glaringly obvious that could cause the cut issues & let you know if I find anything.



edit: okay, it is giving me an error on trying to retrieve the file... says "404 - File or directory not found".



So, maybe some suggestions to look at are regarding your CorelDraw/CorelLaser settings. Can you post a screenshot of your current settings? Also, were you the one that was recently having issues with cutting & WMF output etc? I have a feeling that was you :D


---
**Bob Damato** *December 23, 2016 12:58*

**+Yuusuf Sallahuddin** Yes, that was me! And I took what I learned from that and it didnt work here. (sigh). I will post what I have and try posting the file again...




---
**Bob Damato** *December 23, 2016 13:54*

Im not sure why the  link doesnt work, my apologies. It looks right on  the server.  :( 

This is how I have it set up...  



This is that object manager  [maxboostracing.com - maxboostracing.com/Laser/tbirdlayers.jpg](http://maxboostracing.com/Laser/tbirdlayers.jpg)



This is the drawing:  [http://maxboostracing.com/Laser/tbirdall.jpg](http://maxboostracing.com/Laser/tbirdall.jpg)



This is the  cut lines layer: [http://maxboostracing.com/Laser/tbirdcutlines.jpg](http://maxboostracing.com/Laser/tbirdcutlines.jpg)








---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 23, 2016 17:34*

**+Bob Damato** I took a look at the 3 files you shared again & I have a feeling the issue is still the same thing with cutting, where your cut lines are not joined to each other.



I see in the first one (object manager screenshot) that there is a group of 60 objects that make up your cut lines. You want to try get it as 1 closed shape.



Are you using CorelDraw to do the design or other software? Might be worthwhile using something else to design your cut lines (e.g. Inkscape).


---
**Bob Damato** *December 24, 2016 00:12*

I am using corelDraw to make the cut lines.  I would have thought even if they werent connected, it would have cut approximately where I put them!  Im good in photoshop, but thats about it. Corel draw, in my opinion, kind of sucks. I guess I just need to practice more. Im heading to the bookstore tomorrow, it might be time for a coreldraw book. :o/




---
**Ned Hill** *December 24, 2016 02:14*

Hi **+Bob Damato** :)  If you are still having problems cutting open curves try using EMF as  your output file.  Alternately, for an outline this simple, use the polyline tool to trace the cut path and you will get a continuous line.  You can make adjustments to that cut path using the shape tool to add/move line nodes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2016 04:08*

**+Bob Damato** Personally, I dislike Corel Draw also. I would be much more inclined to use some other software (Inkscape or Illustrator) to draw my cut lines & then import into Corel Draw as SVG format.



It technically should cut approximately where you put them, but you would need to make sure you have a reference point for the top-left corner (I used a border-less fill-less box around the lot). After that, the only thing that I can think that would be causing it to be cutting in an odd position is if your Board model & Board ID are set incorrectly. The machine will work sometimes but do odd stuff other times.


---
**Bob Damato** *December 24, 2016 17:27*

Thank you Yuusuf and Ned.  Im getting MUCH closer. I used your recommendation of polyline for my cut line. now it is one object and actually cuts as a cut and not all over the place. Problem is, the cut isnt over the engrave like it should be.   This is my object box now.  [maxboostracing.com - maxboostracing.com/Laser/tb1.jpg](http://maxboostracing.com/Laser/tb1.jpg)



This is my polyline over the engrave: [http://maxboostracing.com/Laser/tb2.jpg](http://maxboostracing.com/Laser/tb2.jpg)



This is the dialog box. I tried top left, center, etc.

[http://maxboostracing.com/Laser/tb3.jpg](http://maxboostracing.com/Laser/tb3.jpg)



[http://maxboostracing.com/Laser/tb4.jpg](http://maxboostracing.com/Laser/tb4.jpg)



And this is what it comes out like (I practiced on card stock)

[http://maxboostracing.com/Laser/tb5.jpg](http://maxboostracing.com/Laser/tb5.jpg)



I tried the box around the whole mess too. not sure what the issue is now.




---
**Ned Hill** *December 24, 2016 17:36*

**+Bob Damato** Try doing the engrave with WMF output instead of BMP


---
**Bob Damato** *December 24, 2016 18:28*

**+Ned Hill** no luck Ned :(




---
**Ned Hill** *December 24, 2016 18:39*

If you want to try and post the cdr file again I'll try take a look at it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2016 18:42*

**+Bob Damato** Well, it's progress so far. At least in regards to the cut line.



Now, when engraving are you selecting the engrave object + the corner reference point (CRP) & then for cutting selecting cut object + the CRP? (I'm assuming the layer called "Border" is your CRP).



It seems like your position is off for the cut by being too high in comparison to the engrave. Not 100% sure why that is happening at the moment.



Can you verify these settings for your machine & share screenshots again if possible:



Corel Laser Device Initialise settings: [https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)



In particular the Mainboard in the dropdown at the top & the Device ID at the bottom. If either of these doesn't match the details on your mainboard in the electronics panel, modify them to match.


---
**Bob Damato** *December 24, 2016 21:54*

**+Yuusuf Sallahuddin** and **+Ned Hill**  That was it! Now Im able to actually cut where I want to!  The issue was two-fold, for some  reason when I hit cut, it had a different Y offset than when I engraved. but it was only 3 mm, and I was easily off by 20mm. Secondly, my device ID was off. Im 100% certain Ive fixed this before, I dont know why it reverted back. Very strange. But thank you guys, I appreciate your help and patience with me!




---
**Ned Hill** *December 24, 2016 22:43*

You are very welcome **+Bob Damato**, we've all been there.  If you ever get the chance please pay it forward to other people who may need help. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 25, 2016 13:57*

**+Bob Damato** Glad that your issue seems sorted out now :) The software is odd, so I guess if things start being funny again check that Device ID again (might change back for whatever odd reason).


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/9TRC343NNtT) &mdash; content and formatting may not be reliable*
