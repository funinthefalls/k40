---
layout: post
title: "Just wanted to share some images from my first few days with the cutter."
date: November 08, 2015 21:58
category: "Object produced with laser"
author: "Nathaniel Swartz"
---
Just wanted to share some images from my first few days with the cutter.﻿



![images/1da7b4f5c60bb64e3dd510d2cff6bc04.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1da7b4f5c60bb64e3dd510d2cff6bc04.jpeg)
![images/a09b4b80828cf66d71fe14845954cc7f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a09b4b80828cf66d71fe14845954cc7f.jpeg)
![images/623aac19939dcaeb53c4bac5fbd8541f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/623aac19939dcaeb53c4bac5fbd8541f.jpeg)
![images/67f070425001ba526ee55ba231ce87c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/67f070425001ba526ee55ba231ce87c5.jpeg)
![images/c4ec84cb9bf34c0a9c90478be1adfef6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4ec84cb9bf34c0a9c90478be1adfef6.jpeg)

**"Nathaniel Swartz"**

---
---
**little savage** *November 09, 2015 19:33*

your a natural!  Im still faffing with mine!


---
**Nathaniel Swartz** *November 10, 2015 00:28*

Thanks!  I think I've just been remarkably lucky not to hit any major issues yet.


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/KLmqX23JB9N) &mdash; content and formatting may not be reliable*
