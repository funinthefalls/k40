---
layout: post
title: "DIY Copper Mirror Performance. I ordered some 10ga 20mm copper discs and made mirrors from 3 of cleanest"
date: January 11, 2017 16:01
category: "Modification"
author: "Jeff Johnson"
---
DIY Copper Mirror Performance.



I ordered some 10ga 20mm copper discs and made mirrors from 3 of cleanest. I honed them by hand first on 320 grit sandpaper, then 2000 and then using jewelers rouge and they came out pretty darned nice. 



However, after testing with these mirrors, I found the performance to be so close to the original mirrors that I can't tell a difference.  Has anyone else tried Copper mirrors and if so, what results did you see.

![images/3339143c6f28cc51d91d9fe7dfe2e5b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3339143c6f28cc51d91d9fe7dfe2e5b1.jpeg)



**"Jeff Johnson"**

---
---
**Jim Fong** *January 11, 2017 16:47*

I've read that making mirrors out of old disk drive platters also works.  The original mirrors on my k40 are not very good so I may have to try this.   


---
**Coherent** *January 11, 2017 17:59*

**+Jim Fong** I've also read some stuff about the mirrors from old disk drive that said they were better than the stock mirrors in the smaller import machines. The hard drive discs/platters are made from polished aluminum. I have a  few I pulled and a cnc mill so cutting them would be a pretty simple process and only take a couple minutes to program and cut. Since the price is "free" it would be worth the time if you have the means to cut them easily. If I ever get around to cutting some and testing them, I'll pass on the results here.


---
**Jon Cypher** *January 11, 2017 18:02*

Youtube:  RDWorks Learning Labs.  This guy has put out a lot of videos for his CO2 laser - definitely should check them out.  

Copper mirrors: 
{% include youtubePlayer.html id="ELeqP3tHlp0" %}
[youtube.com - RDWorks Learning Lab 46 Copper Mirrors+HQ Lens=20% More Power](https://www.youtube.com/watch?v=ELeqP3tHlp0)

He goes into testing efficiency of the mirrors.  Overall issue was that they tend to wear, but it is just a matter of resurfacing them.


---
**Jeff Johnson** *January 11, 2017 19:05*

**+Jon Cypher** This is where I got the idea for the copper mirrors. It took less than 5 minutes to prepare each mirror so it was well worth the time to try. My mirrors turned out much nicer than his appeared to be but my only method of testing was to see if there was any change in the cutting speed and power for known materials. Maybe if I had a fancy power meter I would see a difference.


---
**Maker Cut** *January 11, 2017 21:34*

**+Jeff Johnson** truly you need the power meter to see the difference. Russ still sells the "dohickey" for the tests. I bought one and it works well.


---
**Jamie Richards** *April 03, 2018 21:55*

I'm using hard drive platters.  Performs at least as well as stock, maybe even a percent or so better and these things will last forever!  I messed up quite a few before getting it right because just the slightest warpage  throws things off.  I've been wanting to try the copper method, too.  Where did you find the copper?


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/YXVeirCz7W4) &mdash; content and formatting may not be reliable*
