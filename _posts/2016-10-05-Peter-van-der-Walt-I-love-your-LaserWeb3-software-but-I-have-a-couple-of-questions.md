---
layout: post
title: "+Peter van der Walt I love your LaserWeb3 software but I have a couple of questions..."
date: October 05, 2016 22:38
category: "Software"
author: "Timo Birnschein"
---
+Peter van der Walt I love your LaserWeb3 software but I have a couple of questions... I recently converted my K40 laser cutter to GRBL (latest master 0.9j) and I'm trying to get everything to work properly. 



I CROSS POSTED THIS INTO THE LASERWEB GROUP AS WELL.



My biggest issue at the moment is the uneven speed at which GRBL executes small circles (huuuge amount of commands) because it seems LaserWeb3 does not support arcs and circles in this format:

G2 X -36.061 Y -19.061 I  -1.061 J  -1.061



Since one of those commands is replaced with an incredible amount of small vector movements my laser slows down significantly without reducing laser power (since it still receives the same feedrate command).



Are you planning on implementing arcs and circles? Or is there an option that I couldn't find to activate them?



My second biggest issue is the work with DXF files. Whenever I export a DXF using solidworks (with the polyline option activated and merging set to 0.01) and import it into LaserWeb3 and check the gcode view as well as the movements that my laser makes, I realize that none of the paths, arcs and circles are connected to each other. Movements are erratic and an outline shape seems cut up into little chunks including the problem mentioned above that curves are not represented as arcs but as little tiny vectors. So tiny in fact that I'd question if this is necessary.



Are you planning on implementing a path optimization (traveling salesman) and arc estimation using a biarc Fitting algorithm?





**"Timo Birnschein"**

---


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/fHr5YdzwRoW) &mdash; content and formatting may not be reliable*
