---
layout: post
title: "Hi, that's my first post here. I have a problem"
date: March 12, 2016 11:58
category: "Hardware and Laser settings"
author: "Giacomo Deriu"
---
Hi, that's my first post here. I have a problem.

I bought a k40 just 3 month ago, I've always used to cut very thin materials like paper or thin mdf sheets so I usually set the power as 2-3mA. Yesterday I started notice that the power seems to change during the job in a range from 1mA to 5 mA. That's not good since happens that the beam is not powerfull enough to cut or it get too strong burning the edges of the material.

When I bought the machine I knew it lack precision and I didn't expect to buy a cutting edge tool , but thats seems to be something really wrong that prejudice the alse the basic use of the tool.

I wonder if anyone already had this problem and possibly how could be possible to solve it.

Thanks





**"Giacomo Deriu"**

---
---
**Scott Marshall** *March 12, 2016 18:25*

Welcome.



Sounds like your Current control potentiometer is "dirty".



There's a carbon strip inside which tends to get "dirty" with time, especially if not used much, causing what's known as a "noisy" control (because when it happens in audio equipment it manifests as a crackling noise as the control is moved). This can be often fixed by simply working the control back and forth a few dozen times. A shot of WD-40 or CRC contact cleaner in the crack around the terminals often helps as well (it "rehydrates" the carbon film) 



The factory control is pretty cheap and prone to this issue. Try the cleaning 1st, but you may have to replace it sooner or later.



 Replacement with a better quality one is cheap and fairly easy, but does require soldering. You can get a 10-turn Pot and vernier drive unit for <$10, and it will allow much better control of the adjustmeent, especially down at low engraving levels.



I forget the value used, but will look it up and get back to you. You will need what's called a Linear taper Pot.



Hope it's an easy fix,

Scott



PS it's a 5K Ohm Potentiometer.



Here's a link to a suitable 10-turn pot with Vernier Drive. it's a wirewound pot and they're nearly immune to becoming "noisy" as the Carbon, Carbon film and Ceramic types are:

[http://www.ebay.com/itm/1x-3590S-2-104L-5K-Ohm-Rotary-Wirewound-Precision-Potentiometer-Pot-10Turn-XT-/111725547354?hash=item1a035c8f5a:g:5cEAAOSwv0tVTdgX](http://www.ebay.com/itm/1x-3590S-2-104L-5K-Ohm-Rotary-Wirewound-Precision-Potentiometer-Pot-10Turn-XT-/111725547354?hash=item1a035c8f5a:g:5cEAAOSwv0tVTdgX)


---
**Giacomo Deriu** *March 12, 2016 21:13*

wow, thanks for your reply mr. Marshall.

No problem for soldering and I agree that the best is to fix it once for all. I try to clean it first, maybe it helps figure out whether that's the problem or not.

Thanks again for your help!


---
**Scott Marshall** *March 12, 2016 22:04*

Have a go at cleaning it, if that works, you'll be back in business until the new one comes.



Hope that is all that's wrong. Mine was a little "twitchy" set low when it was new, and I've heard others mention that. It seemed to settle down with use. If there's a problem beyond the control, it's probably the nature of the power supply, and you may have to speed up the head to allow turning up the current a couple ma to get it into the 'stable zone'.



Good luck,

 Scott


---
*Imported from [Google+](https://plus.google.com/112554405852389236666/posts/MzQcsJjmTaU) &mdash; content and formatting may not be reliable*
