---
layout: post
title: "I think I broke my PSU. Instead of investing in replacing it, I'm thinking of upgrading the whole power supply/driver board chain"
date: April 23, 2018 14:18
category: "Discussion"
author: "Frederik Deryckere"
---
I think I broke my PSU. Instead of investing in replacing it, I'm thinking of upgrading the whole power supply/driver board chain. Is there a repository somewhere with popular options and their respective pro's/cons. 





**"Frederik Deryckere"**

---
---
**Joe Alexander** *April 23, 2018 15:31*

check out [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com) it has a ton of useful info


---
**Don Kleinschnitz Jr.** *April 24, 2018 12:17*

What do you mean by the "power supply/driver board chain"?


---
**Frederik Deryckere** *April 25, 2018 15:31*

since a replacement k40 psu costs eighty bucks already, i'd better just upgrade the whole electronics part. I hear good things about cohesion3D, but it's hard to get in EU. Then there's Gerbil which is kind of an unknown to me. Question, which driver board/psu combo is known to work well and is as plug and play as possible?




---
**Don Kleinschnitz Jr.** *April 26, 2018 10:37*

The Laser Power Supply (LPS) and the stock K40 control board have only three connections in common. All other connections to the LPS are needed for operation but are unrelated to the controller.



The connections from LPS to controller are:

1. pwm control

2. 24vdc

3. 5vdc



Replacement of the LPS does not require the replacement of the controller. The chief reason that folks replace the controller is to get to better software control like:

1. Laserweb

2. Lightburn



Then again K40 whisperer can be used with the stock board



.....

The cohesion 3D board is designed to directly replace the stock board.

**+Ray Kholodovsky** can provide more info.






---
**Frederik Deryckere** *April 26, 2018 12:41*

**+Don Kleinschnitz** I did some testing. I wasn't sure if I damaged the nano board as well. (I accidentally switched the two 4-pin phoenix connectors around and powered on, which tripped my house's main fuse - ouch) Turns out the 24V still works, the 5V doesn't. So I connected another 5V adapter to those pins and sure enough the machine homed as it is supposed to. So I guess the nanoboard is still working and I only damaged the PSU. So at this time I'll only replace the PSU, but since those chinese ones are said to be pretty low quality and unsafe, is there an drop-in replacement of better quality? I'd like to keep the testfire buttons and ampere meter + pot as in the stock machine


---
**Don Kleinschnitz Jr.** *April 28, 2018 09:51*

As far as I know there are better quality but much more expensive laser supplies but they are not drop in.

If only the 5V is bad you may be able to repair it. Let me know if you want to try.

If you do the conversion to a C3D you will not need the 5v. In fact the recommendation is to get a separate 24vdc supply anyway.

One approach is to keep the LPS and get a 24VDC supply and a C3D board. You will have a far better machine.




---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/JpavfQexntf) &mdash; content and formatting may not be reliable*
