---
layout: post
title: "LaserWeb3 Help! I am seeking settings that have been tested by others as correct and functional"
date: November 20, 2016 23:35
category: "Hardware and Laser settings"
author: "Matthew Prater"
---
LaserWeb3 Help!

I am seeking settings that have been tested by others as correct and functional.  



Today-  I Installed Scott's ACR/Smoothie/Supplemental power kit, rechecked my connections, and fired it up. I accessed Laserweb3 and inserted some settings that I found online. G28 Start, M1 start laser, M5 stop laser, M5&G28 endcode. Laser diameter- 0.1, and finally bed area of 200x300.  Machine powered up fine, laser homes to top left (just as stock m2Nano did).



The machine was ending out by attempting to go further up when I ran a test print without laser on. 



First, I would appreciate if your would share your Laserweb3 settings with me, and how functional they are. 



Second, I would like your opinion s to whether this could be a software setting issue, or a incorrectly wired stepper motor. 



Thank you for your help!!!



 





**"Matthew Prater"**

---
---
**Ariel Yahni (UniKpty)** *November 21, 2016 00:52*

**+Matthew Prater**​​ laser On is G1 laser Off is G0. ﻿


---
**Matthew Prater** *November 21, 2016 01:39*

**+Ariel Yahni**  thanks for responding, however I'm not sure what you are saying. Could your rephrase so that I can better understand?


---
**Matthew Prater** *November 21, 2016 01:41*

I'm beginning to think that my stepper motor is wired backwards. The home on LaserWeb3 is 0,0, however my machine is homing at 0,200 (or the upper left hand corner).


---
**Ariel Yahni (UniKpty)** *November 21, 2016 01:49*

**+Matthew Prater**​ I fixed the typo above. Laserweb should home to max so upper left. Still 0,0 will be lower left


---
**Mike Grady** *November 21, 2016 01:57*

Bed area should be 300x200 'x' axis is first then 'y' and in settings/tool  place uploaded image in quadrant top left this is normal


---
**Matthew Prater** *November 21, 2016 05:21*

Thx Peter 


---
**Matthew Prater** *November 21, 2016 05:48*

Awesome! Any word on when LaserWeb4 will be ready to use? 


---
*Imported from [Google+](https://plus.google.com/113895077196814551282/posts/JVyUc2rfVp9) &mdash; content and formatting may not be reliable*
