---
layout: post
title: "I looked back though the old posts, and didn't see any suggestions on where to obtain a K40"
date: June 29, 2015 21:14
category: "Discussion"
author: "Kirk Yarina"
---
I looked back though the old posts, and didn't see any suggestions on where to obtain a K40.  Are all the different sellers shipping the same thing, or are some better than others, other than the control panel?   What about the quality of the packaging and shipping?



There's at least two shipping control panels, one with an analog meter that's priced as low as $375 on eBay, and another with a digital display that's a hundred bucks more.  Any real advantage to the digital version, including differences in the machine's mechanical innards?  It sounds likely that the analog meter controls are, well, less than optimal.  Is the (claimed to be) "improved" model any better and would it potentially hold off a control upgrade for a while (I understand both need the dongled low budget PC software)?  Or is it better to stick with the original, expect to replace it fairly soon, and save the extra money?



Thanks!



Kirk





**"Kirk Yarina"**

---
---
**charlie wallace** *June 29, 2015 21:39*

they're about the same, i have the digital panel one, if you were replacing it with a separate controller it wouldn't matter. little easier to set the levels on the digitla, they both use the same driver software,, what you really want is the later versions of the mosihiboards but i'm not aware of anyone on ebay listing that difference, again if changing eventually doesn't matter.


---
**Stephane Buisson** *June 30, 2015 08:57*

Hi Kirk, and welcome here.



What you are buying is a XY axis with laser and  power supply in box. consider the Moshi electronic board as buggy (barely usable at first) and in the community we are looking for the best way to update it. (still to find a solution from board to software, powerful enough at reasonable budget). the control panel would be part of the upgrade, and honestly go for the cheapest as it make no difference.



Ebay prices vary a lot, depending on location, demand and supply, 375 usd (delivred?) is not bad.


---
**Black Spline** *July 01, 2015 05:49*

I'm in the market for a K40 and have noticed the 'newer' version with the digital display.  They have an added limit switch and air assist. They come with LaserDraw software and the Corel plugin. Not sure what controller they use. Here is an example:



[www.ebay.co.uk/itm/331563059870?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.co.uk/itm/331563059870?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Stephane Buisson** *July 01, 2015 10:03*

+Black Spline your ebay link,This new K40 version is a step in the right direction, air assist and laser pointer show they are listening/reading the community.

I know as well they are fedup with the moshi software. but still using the hardware which is compatible with Laserdraw (Corel pulgin). To my view the difference in price is not worth this factory update as anyway  this machine will need an board update later.

Personally I still think the best software option is using Visicut and waiting to see which other board

 **+Thomas Oster** will choose to develop a driver for. (Laos being too expensive)


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/bLFWRLRmght) &mdash; content and formatting may not be reliable*
