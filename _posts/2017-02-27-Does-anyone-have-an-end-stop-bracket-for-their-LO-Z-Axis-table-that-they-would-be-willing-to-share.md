---
layout: post
title: "Does anyone have an end stop bracket for their LO Z-Axis table that they would be willing to share?"
date: February 27, 2017 19:16
category: "Modification"
author: "Tony Sobczak"
---
Does anyone have an end stop bracket for their LO Z-Axis table that they would be willing to share?



TIA





**"Tony Sobczak"**

---
---
**Don Kleinschnitz Jr.** *February 27, 2017 19:57*

I used this bracket and cut it from acrylic and modified it: 

[lightobject.info - K40 mini Z table limit switch installation and mounting - Lightobject.com Support Forum](http://www.lightobject.info/viewtopic.php?f=16&t=2112)



In my blog @ 

[http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)


---
**Tony Sobczak** *February 28, 2017 19:34*

Thank You.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/1AqzGQjhfno) &mdash; content and formatting may not be reliable*
