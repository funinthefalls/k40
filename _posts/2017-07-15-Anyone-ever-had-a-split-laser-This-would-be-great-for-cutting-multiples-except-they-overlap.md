---
layout: post
title: "Anyone ever had a split laser? This would be great for cutting multiples, except they overlap"
date: July 15, 2017 21:25
category: "Discussion"
author: "Josh Speer"
---
Anyone ever had a split laser? This would be great for cutting multiples, except they overlap. I just cleaned the mirrors and lens. 

![images/ce5085953b82ccf5e12f4626aad98269.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ce5085953b82ccf5e12f4626aad98269.jpeg)



**"Josh Speer"**

---
---
**Don Kleinschnitz Jr.** *July 15, 2017 21:42*

Very artistic...


---
**HalfNormal** *July 15, 2017 22:18*

Make sure the head is square with the second mirror.


---
**Phillip Conroy** *July 15, 2017 23:02*

Do you have air assist,beam hitting side of air nozzle 


---
**Ned Hill** *July 16, 2017 03:09*

Yep, alignment issue.




---
**Josh Speer** *July 16, 2017 03:56*

Yup you guys were right on, had to square up the head. Thanks!


---
*Imported from [Google+](https://plus.google.com/115420425425105333016/posts/cxGb2JuegeM) &mdash; content and formatting may not be reliable*
