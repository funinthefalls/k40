---
layout: post
title: "How do you run a test because I tried but the GFCI outlet shut off how much power and what do I push first"
date: April 28, 2018 23:31
category: "Discussion"
author: "Dale Capen"
---
How do you run a test because I tried but the GFCI outlet shut off how much power and what do I push first 

![images/ddef576de6306a284be8ff817d743e4d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ddef576de6306a284be8ff817d743e4d.jpeg)



**"Dale Capen"**

---
---
**Ned Hill** *April 30, 2018 02:46*

First make sure the emergency "STOP" button is pulled out (may have to turn it first to unlock it).  Then toggle the "Power switch" which powers the board and then turn on "Switch machine" to energize the system.  Put a piece of paper under the head, set the power to ~15% and press the "Laser test switch" button briefly to see that the laser fires (burn paper).  You will want make sure the mirrors and lens are clean and do a full mirror alignment. (make sure the lens is oriented with the curved(bump) side up.)  A couple of different alignment guides can be found by searching the hashtag #  K40Alignment in this group.


---
**Kiah Connor** *May 02, 2018 03:38*

I have the same one! Rotate your stop button to the right, and it'll pop out and everything will magically start working (as mentioned by Ned above)


---
**James Rivera** *May 03, 2018 16:45*

I have the exact same panel, too. My laser doesn't really fire until I hit 20% though. From reading posts here, it seems the digital readout is not a reliable indicator of power and many people replaced it with a potentiometer and analog amp meter which provides much better control and indication of power. 


---
*Imported from [Google+](https://plus.google.com/110408102287186991812/posts/KYgMMm44N4z) &mdash; content and formatting may not be reliable*
