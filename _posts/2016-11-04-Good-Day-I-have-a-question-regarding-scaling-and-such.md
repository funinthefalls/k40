---
layout: post
title: "Good Day, I have a question regarding scaling and such"
date: November 04, 2016 18:55
category: "Software"
author: "Kris Sturgess"
---
Good Day,



I have a question regarding scaling and such. I'm currently using CorelDraw 12/Corel Laser.



I want to cut the attached project on my K40. Obviously table dimensions are 300mmx200mm.



In Corel I have a template for my bed setup and I usually import the.dxf file.



What is the best way to scale it to my bed size and keep accuracy with all (3) .dxf files?



Do I just diagonally stretch until the border is even with my template? or is there a better way to do it?



Sorry for the lame question, just trying to figure all this out.



Cheers!



Kris







**"Kris Sturgess"**

---
---
**Ariel Yahni (UniKpty)** *November 04, 2016 19:10*

It will  it be that's easy, specially the middle sections where the material thickness  come to play. Example if the material is 3mm and you scale what material would you use? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 04, 2016 19:41*

There is a section I have on the CorelDraw toolbar that allows you to manually type in the dimensions. One button next to it is a lock (to keep the X/Y scale in proportion). So you could select all the elements & type in 300 (or 200) for the longest side of the DXF & it should scale to fit your template.



That's a nice little tool organiser by the way.


---
**Jim Hatch** *November 04, 2016 21:42*

Scaling is tricky for finger joints. When you scale it you're going to change the depth of the notch. That depth has to be sized to match the thickness of the material you're going to use. You're going to need to adjust those (& the corresponding finger) to match the material after you've done the resize of all of the elements.


---
**Kris Sturgess** *November 05, 2016 00:32*

I'll take a looksie again when I get home. I'll scale then measure the fingers. Should give me a ballpark % to scale to.


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/88s7voijndh) &mdash; content and formatting may not be reliable*
