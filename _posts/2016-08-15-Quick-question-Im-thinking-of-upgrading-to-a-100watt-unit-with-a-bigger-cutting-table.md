---
layout: post
title: "Quick question. Im thinking of upgrading to a 100watt unit with a bigger cutting table"
date: August 15, 2016 13:31
category: "Original software and hardware issues"
author: "Bob Damato"
---
Quick question. Im thinking of upgrading to a 100watt unit with a bigger cutting table.  The problem is, the smallest dimension is 46 inches and wont fit through my door so it will need to stay out in the garage instead of going into my hobby room.  



I live in CT and it can get mighty cold here (have seen 10 below 0 pretty much every winter).  Other than making sure I have antifreeze in the tube and water supply, are there any other issues Ill have with it? If I decide to go out in the middle of February and do some cutting will it be OK?  



bob





**"Bob Damato"**

---
---
**HalfNormal** *August 15, 2016 14:05*

LightObject has a kit you can build. You might take a look at it and customize it for your location. [http://www.lightobject.com/Aluminum-Frame-Kit-C71.aspx](http://www.lightobject.com/Aluminum-Frame-Kit-C71.aspx)


---
**Jim Hatch** *August 15, 2016 14:28*

I'm in CT as well and run the pump whenever it's below freezing (& I use Dow Frostguard).﻿



BTW, I'm looking at either a 100 or 130W with a 4' X 3' bed too. Trying to decide between buying g one off eBay and fixing it with controller and PSU upgrades or go with a local build like the LO one.


---
**Bob Damato** *August 15, 2016 15:14*

**+Jim Hatch** Ive looked at the LO ones too but they seem a lot more expensive than what I can get off ebay. Although ebay can be a bit of a gamble.  Where abouts in CT are you? Im in Cheshire. Maybe we should pow wow about upgrades!


---
**Jim Hatch** *August 15, 2016 15:28*

**+Bob Damato**​ I was actually looking at making a LO + Lasersaurus hybrid along with a local guy in town to weld up a frame. But an eBay big unit would have all the parts & pieces and I'd just budget to swap the tube out after I burn through the stock one (figuring a 130W eBay one really comes to me with a 100 or 120W actual tube) but upgrade to controller out of the box and switch it to Laserweb and Smoothie.



I'm in north central CT north and west of Hartford just under the notch that MA takes out of the state.


---
**Bob Damato** *August 15, 2016 16:45*

Oh sure, I know where you are. Not too far.   When I started looking at a LO laser I knew Id be in it for 2x the $ and 10000x the time.  the ebay ones seem to have pretty much all the features I really need at this point, but Id also do the smoothie upgrade at some point.


---
**Jim Hatch** *August 15, 2016 16:57*

The 100W large bed (3'x4') eBay ones are in the neighborhood of $3500. It's getting it in the basement that I'm hesitant about 🙂 One I build would be in door sized components.


---
**Bob Damato** *August 15, 2016 17:35*

Yes, that is a very very good point.  Thats a big issue for me and Id much rather have it in the house. Maybe I do need to explore that option more seriously


---
**Jim Hatch** *August 15, 2016 17:42*

**+Bob Damato**​ If we can split the housing into two parts - one for the tube, electronics and gantries another for the Z axis bed and lower housing then it could come through a standard door. If I could see one in the wild I'd be able to see if I could disassemble it or if there's something a SawsAll could help with making it two parts.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/KzvExn8A33w) &mdash; content and formatting may not be reliable*
