---
layout: post
title: "Posted this in the wrong place ....."
date: February 27, 2018 02:01
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Posted this in the wrong place ..... 



[https://plus.google.com/+DonKleinschnitz/posts/WpHy2M8a2Xq](https://plus.google.com/+DonKleinschnitz/posts/WpHy2M8a2Xq)





**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *February 27, 2018 14:37*

An added MINUS. This panel draws about .2 amps from the stock K40 supply. That's about 20% of the safe 5V capacity and some new machines have LPS with the fan missing. I have to believe that the stock LPS is running on its limit with these vintage machines.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/LiQJuHhaKdT) &mdash; content and formatting may not be reliable*
