---
layout: post
title: "Has anyone seen a tutorial for adding a analog meter onto a digital k40?"
date: December 30, 2015 06:23
category: "Modification"
author: "I Laser"
---
Has anyone seen a tutorial for adding a analog meter onto a digital k40? I've got an analog meter on it's way but not sure how to hook it up :|





**"I Laser"**

---
---
**Brooke Hedrick** *December 30, 2015 06:31*

When I went from analog to digital, I just put the digital inline the same way it was for analog.  Digital required 2 extra pins for 5v and gnd.



Analog should be a simpler connection - assuming it requires no additional power.  I believe my analog gauge just had 2 pins.



If you don't get a reading when you try it, you could have the pins/contacts/terminal connected backwards.  Just switch them around.


---
**I Laser** *December 30, 2015 07:23*

Yes it's only a two pin meter, so no need for extra power. Just not sure which wire it should be going on, my old analog cutter has a single line from the PSU going up to the meter then the other side is connected to the tube.



The digital cutter is a bit smaller, owing to a more cramped electrical compartment, so at this stage I'm not sure what I'm looking at and with all the talk of high voltage and death I'm a bit wary! Thinking I'm going to need better access to determine what's going on.



edit: So after a closer look, there's a green wire which hooks onto the tube closest to mirror 1, which is the same wire the analog machine has the meter inline to. I'm assuming I'll just snip this wire and put the meter inline there and should be good to go, unless someone tells me otherwise! :) 


---
**Brooke Hedrick** *December 30, 2015 13:48*

Can you send a photo of the digital meter from front panel and inside the electronics compartment so we can see what it looks like and the connections?


---
**Scott Marshall** *December 30, 2015 16:06*

What you describe is a Milliamp meter, it measures current flow and is placed INLINE with the power to the lasertube. Negative connects to the power supply and positive comes from the negative connection on the lasertube. It's wired this way to keep the meter potential (voltage) as close to ground  as possible for safety and to prevent arcing at the meter.



 Be careful if you disconnect the laser side as it is at the full high voltage supply potential.



If the meter you have is a voltmeter, it's not what you need, you need a milliammeter, (for example 0-25ma) make sure you get the proper range based on the maximum current your laser is rated for. 

A milliammeter is actually a volt meter with a shunt resistor across the movement (sometimes internal, or often mounted right to the terminals), and a proper scale in amps or milliamps. It works based on Ohms law which says that the current (in amps) is equal to the voltage ACROSS the resistor divided by the resistance (In Ohms).



Example if the resistor is 1000ohms, and has 15volts across it 15/1000=.015amps or 15 milliamps 



(yes, if your voltmeter is the right range, you can add a resistor and use it, but you are fooling with very high voltage, so buy one unless you are sure of your soldering and resistor sizing etc.)



Many times manufacturers connect a voltmeter to the power supply control lines (where the controller connects). If the power supply control  is analog, a voltmeter will give you an idea of the power being called for (0-100%) but you'll need to make a 'lookup chart". 

If the power supply control is digital, it WILL deflect, but it will only give a very rough (if any) idea of the power being called for, as the control is a duty cycle on-off type of control (PWM - Pulse Width Modulaton)



(I'm an engineer and a controls guy, this is my field)



Hope I helped more than confused.



Scott




---
**I Laser** *January 02, 2016 01:01*

**+Brooke Hedrick**: the compartment is pretty cluttered, I just followed the line coming from the far side of the tube back in to it. That's how the current one is hooked up, so assumed I should be able to do the same thing with the new one.



**+Scott Marshall**: Wow thanks for the very informative post! A few questions:



1. I bought the exact same mA meter that's in my other analog k40, so I assume it's correct (it's 0-30mA).



2. Not sure whether the power supply control is digital or not. Any easy way to tell?



3. Your comments about the accuracy of a meter, not being an electronics guy, am I wasting my time trying to put a meter on it? I just like to keep my current machine below 10mA, the new machine has no meter, just percentage of power which is kind of useless!



4. Also when you say about being careful snipping the line to place the meter inline, I assume if the machine is off it's safe. ie there's no capacitor waiting to fry me? lol


---
**Scott Marshall** *January 03, 2016 03:43*

1 - if you got the replacement meter for that unit, just check to make sure there's not a remote shunt resistor in the circuit on the 1st machine. Probably not, most of those small meters have it right inside. The 10amp and up ones use a remote shunt.



It should wire into the ground side of the tube fine. (+ goes to the tube negative, and - goes to power supply negative  if you get it backwards, it will just deflect downscale. No harm. just safe the supply, and reverse the leads.

Just make sure you don't cut into the high side of the tube.



2. yes, the power supply will usually have the control input marked as to it's input type. Some use a 4 to 20 ma current loop to call for output power - there's also a 0-5v version, which is really just the same as the 4-20 ma without a load resistor. These are the analog type. 



There's a variety of digital communication standards for industrial controls. I2C, Rs232 and  Ftdi are common ones. I2C is a digital variation of the  current loop, and RS232 is a 0 or 5v binary variation of USB (Universal Serial Buss) that we are all familiar with. The only real differences in the FTDI, RS232, and USB are syntax. Some have start and stop bits, run at  different baud rates etc. 



Many power supplys with remote inputs like those in lasers have BOTH analog and Digital inputs. Most often there are 2 input connectors, and all you need to do is read the markings - the one hooked up is how it's being controlled. I just got my 40W CCL running, and I'll look at mine if it helps and walk you through the hookup. I THINK these use the analog inputs, with the power selected by the "power pot" on the front panel setting the 0-5v or 4-20ma level, and then the controller board simply calls for on and off by cutting the (probably =5v)power to the pot. This pulsing on and off rapidly  gives a flickering, unstable meter reading that's actually worse with a digital meter. To get an accurate reading, you need to aim the laser at a safe target, press the test button and note the power level during steady state operation..



Somebody here that's had more time with the CCL  (CheapChineseLaser) could probably tell you for sure on the control type.

 I need a few days with mine, I just test fired it a couple hours ago. It came quite damaged and I'm still negotiating a settlement so haven't had a chance to get familiar with it yet..



3 - most of the analog cheapie meters are within 10 or 15% at best, but that's no reason NOT to use one for a reference if you have one. A better quality digital one can be had for 20 bucks and is worth the investment in my opinion. I agree that limiting power is an excellent way to try to squeeze out more useful life out of these cheap tubes. The harder you run them, the more likely they are to develop leaks, have diminished gas and mirror quality which will lead to failure. The cheap meter will do it, just be aware that 10ma on it may NOT really be 10ma. Check it's calibration with a digital multi-meter if you are concerned. It can be done safely with a battery, resistor and put the meters in series. I wouldn't recommend putting your portable meter in the laser circuit unless you are very experienced with HV and know safe practices.



4 - That's one of the "safe practice" techniques I speak of. 

To "safe" a HV circuit, 1st power down, and disconnect the mains. Ensure it cannot be plugged in or powered up accidentally - a tie wrap thru the line plug works well.(USA style plugs only) A wrap or 2 of tape on European style plugs does the job too.  It's easy to plug in the wrong cord while working. 



2nd -  ALWAYS use a jumper wire, connecting it 1st to ground or power supply negative , then to the HV lead, allowing it to arc out if there is indeed any stored energy. Leave it connected for a minute or 2. Some people leave it connected whlie working, it's an extra measure of safety. Just remember to remove it before power up.

 

Even if the power supply doesn't contain an output capacitor, at 15kv and up, flashtubes, CRTs, even parallel wires can store enough energy to be unpleasant or dangerous. When in doubt, ask someone who does it for a living.



Hope my long winded explanations help, if you need more help, or want a step by step on the meter install, just ask, I'll help if I can.



Scott










---
**I Laser** *January 05, 2016 22:04*

Thanks again Scott for your detailed explanation. I'm not home right now, but from memory I'm pretty sure the resistor is in the meter. I'll have a closer look when I get back and update. Really appreciate your knowledge, it's an asset to those of us that aren't electronically inclined! :)


---
**Scott Marshall** *January 05, 2016 22:28*

Glad to help. I was forced to retire due to health issues, and miss the work a lot. 



 I'll be getting even pretty soon.

 Once mine is fixed, (shipping damage)I'm sure I'll need help working through the unique controller it comes with...


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/hZjWZfruwWU) &mdash; content and formatting may not be reliable*
