---
layout: post
title: "Little box I did yesterday testing out my mini fridge water chiller"
date: September 07, 2016 01:41
category: "Object produced with laser"
author: "Robert Selvey"
---
Little box I did yesterday testing out my mini fridge water chiller.

![images/f38a6b07e78245c4f52d92f39c573696.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f38a6b07e78245c4f52d92f39c573696.jpeg)



**"Robert Selvey"**

---
---
**greg greene** *September 07, 2016 01:54*

what wood?


---
**Ariel Yahni (UniKpty)** *September 07, 2016 01:55*

How many passes? 


---
**Robert Selvey** *September 07, 2016 04:16*

.25 inch poplar two passes at 5ma at 10mm


---
**Tony Schelts** *September 07, 2016 08:09*

What Machine you using 40w


---
**Robert Selvey** *September 07, 2016 11:20*

Yes the k40


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/JwBzNHKHUv9) &mdash; content and formatting may not be reliable*
