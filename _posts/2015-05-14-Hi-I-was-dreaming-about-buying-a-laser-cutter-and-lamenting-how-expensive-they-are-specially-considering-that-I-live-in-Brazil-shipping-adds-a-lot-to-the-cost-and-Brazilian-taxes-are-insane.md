---
layout: post
title: "Hi! I was dreaming about buying a laser cutter and lamenting how expensive they are specially considering that I live in Brazil, shipping adds a lot to the cost, and Brazilian taxes are insane!"
date: May 14, 2015 11:01
category: "Discussion"
author: "Carlos Ribeiro"
---
Hi! I was dreaming about buying a laser cutter and lamenting how expensive they are specially considering that I live in Brazil, shipping adds a lot to the cost, and Brazilian taxes are insane!



Then I found out yesterday about low cost 40W lasers. Seems like a well hidden secret! 



I'm thinking about buying one for myself, and I'd like to know if anyone recommends some specific supplier on eBay or Amazon. I found it on Amazon for US$ 500, and over eBay for a lower price (US$ 389). I'm a longtime Amazon customer though and I fear that eBay may have a harder time with Brazilian customs than Amazon.



There's one big issue that's the software. Some units come with different software. I would prefer my unit to work with Corel Draw, and for what I've seen on videos this is possible. Which is the best unit for this?



I also noticed that there are small variations in the control panel. Some have digital controls, while other units have analog controls. Is there a big difference between the two?





**"Carlos Ribeiro"**

---
---
**Stuart Middleton** *May 14, 2015 15:29*

This took me a long time to decide. Make sure they specify CorelDraw as the other software is horrible. The control panel laser power isn't important. I have digital but it does the same job. If you can get air-assist (I don't have this) as this makes the most difference and is one thing most people without want to add.

And be prepared for a damaged machine. A high percentage are damaged in some way. Mine was dented, the fan broke and the laser tube was cracked. They were all fixed, but it was annoying.



Good luck!


---
**Eric Parker** *May 20, 2015 17:22*

Regarding Digital vs knob; I finally decided on going with the knob.  After a bit of research, I figured out that an analog knob actually varies the laser power, whereas a digital PWM control, while appearing to do the same, actually PWMs the laser output at full power.  I'm replacing the stock knob with a multi-turn pot for finer control.


---
*Imported from [Google+](https://plus.google.com/117222998186488390566/posts/V1E2fGmYXbo) &mdash; content and formatting may not be reliable*
