---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) So, today I am working on a case for my new phone (Microsoft Lumia 950)"
date: March 25, 2016 04:27
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



So, today I am working on a case for my new phone (Microsoft Lumia 950). Going to make it a leather case that flips open like a book.



So, I've started by getting the dimensions (online, from Microsoft store website), then I've drawn up the image for cutouts to make sure I get everything correctly cut in the right places. I found a photo (on forbes website) of the Lumia 950, showing the front & back. I used that to assist with drawing up the image to make sure I have the cutouts in the correct place.



After drawing it all out, I used the K40 Laser Cutter to cut out the front & back pieces onto 3mm thick balsa wood (after testing on some thick paper stock first). 2 passes @ ~5mA power @ 20mm/s. I then took these pieces to compare to the phone to check if the cutouts are correctly positioned.



After that, I've engraved the front & back lightly onto 3mm MDF pieces, cut out to the overall shape. I've then cut them out, plus a 3rd overall shape. As the phone is 8.2mm thick, I need 3x 3mm MDF to simulate the phone shape/size. I have then glued all 3 pieces together (which are in my vise at the moment drying). This will be used as a "plug" for wet-forming my leather around in order to make the phone case.



![images/3bac8fdae0ca4259b6c49a43c18aa010.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bac8fdae0ca4259b6c49a43c18aa010.jpeg)
![images/a80cfc37f123dc97a9b555e318b0766d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a80cfc37f123dc97a9b555e318b0766d.jpeg)
![images/06ef797972522571ccfa844fcac00655.png](https://gitlab.com/funinthefalls/k40/raw/master/images/06ef797972522571ccfa844fcac00655.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Dennis Fuente** *March 25, 2016 16:30*

lot of work but nice job


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 25, 2016 16:34*

**+Dennis Fuente** Still lots more to go tomorrow, once the pieces for my "plug" (I don't know what else to call it, just in boat-building it is called a plug) are glued together. Then onto making a clamping frame & then wet-forming the leather around it. Fun :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 26, 2016 04:53*

So, for anyone interested in the progress of this piece, I will not post any more here in the K40 community, unless there are Laser Cutting/Engraving elements. The rest you can see publicly on my profile.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/c9sFZpRgLr7) &mdash; content and formatting may not be reliable*
