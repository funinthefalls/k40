---
layout: post
title: "This only works if you remove the POT from the front panel.."
date: July 26, 2016 17:52
category: "Smoothieboard Modification"
author: "David Cook"
---
[https://drive.google.com/open?id=0ByNecjb0RD-TOF9jYkNJbW5Xc3c](https://drive.google.com/open?id=0ByNecjb0RD-TOF9jYkNJbW5Xc3c)

This only works if you remove the POT from the front panel.. and is considered a single wire control. The TH4- connection is just holding the laser fire pin the GND. The PSU activates the laser and controls the intensity by PWM. I'm not sure how good that is for safety but the operation is fine. 



Disclaimer:

This is what I have done to my machine,  I have no idea if it will work on your machine and advise to use this information at your own risk :-)﻿





**"David Cook"**

---
---
**Alex Hodge** *August 03, 2016 14:20*

Weird. With my LPS, if I connect TH4 to that pin (What I've been calling L), my laser continuously fires. I flashed the latest non-edge firmware and updated my config to reflect the new pins but still no luck.


---
**Alex Hodge** *August 03, 2016 14:39*

So David, is your power supply different (pin out) than the one in the "SmoothK40" guide?

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)

Also, how did you come to the decision to use those particular pins (1.23 vs 2.5 for instance)?


---
**David Cook** *August 03, 2016 16:42*

**+Alex Hodge** I believe the pinout is almost the same.  It looks like the 5V and ground pins may be opposite.  



for your second question,  that's tough.  After utilizing some fire water and herbs and a couple nights of deep forum searching I believe I found a post that hinted at such a re-mapping of the pins. 








---
**Alex Hodge** *August 03, 2016 17:03*

**+David Cook** LOL. I hear ya. I think there needs to be an effort put forth by all of us to take pictures of our various power supplies and the pin outs we've discovered for each. Imagine how easy this conversion would be if we had proper documentation!


---
**Jon Bruno** *August 05, 2016 14:31*

Alex, I'm currently doing research on this very issue.. the reason your laser continuously fire is that TH4- is tied to ground.



1.23 for PWM power control is ok.. the problem is in the enable pin selected.



I'll bet a dollar that you're testing with the POT still in place and feeding the input pin on the LPSU.



I'm checking into this as I type this..and I'm trying to see if it's as simple as switching to the + pin but since 0.26 is an ADC I don't know if it can be used as a proper switch.

We just might need to go back to 2.5 as proven in previous builds.





hang in there... this should be resolved shortly.


---
**Alex Hodge** *August 05, 2016 15:06*

**+Jon Bruno** Actually, I just ditched TH4. Firing with - on P2.5 connected to L (rightmost pin on LPS). That's the pin the stock board used for firing control. Not sure why we've all been trying to use one of the pins from the test fire button all this time instead of L. It works great. 1.23 works perfect for PWM though. OH, also you owe me a dollar :) ditched the pot when I got my smoothie installed.


---
**Jon Bruno** *August 05, 2016 15:20*

Did you remove D14 or are you working with it installed?


---
**David Cook** *August 05, 2016 15:21*

It appears that in my setup I am really only using the PWM pin to fire the laser AND control it's intensity.  I have no idea how I even ended up plugging into TH4  :-/    my TH4- connection is simply enabling the laser to be able to be turned on by the PWM pin.


---
**Alex Hodge** *August 05, 2016 15:21*

This is how I wired up. My config is basically the same as David's with the difference being I set switch.laserfire.output_pin to 2.5!^ instead of .26!^ because I'm using 2.5 -.

[https://drive.google.com/open?id=0Bx_Wrl9uJSWLelF5UTM0YVB3OUE](https://drive.google.com/open?id=0Bx_Wrl9uJSWLelF5UTM0YVB3OUE)


---
**Alex Hodge** *August 05, 2016 16:08*

**+David Cook** LOL


---
**Alex Hodge** *August 05, 2016 16:15*

**+Jon Bruno** I left it installed...I'm not an electrical engineer but seeing as how I'm on the -, I would think it should be okay. Maybe not? I suppose I should break out my multimeter...


---
**Jon Bruno** *August 05, 2016 16:37*

**+Alex Hodge** The LED could be an issue...


---
**Jon Bruno** *August 06, 2016 00:38*

**+Alex Hodge**​​ Take out the !  it should read 2.5^

if you leave the ! the pin will be active on startup and M3/M5  is inverted.

No good.


---
**Jon Bruno** *August 06, 2016 01:02*

Alex, the pin does pull up to 24v when idle..pull D14 or build the zener diode clipper that brian phillips put together


---
**David Cook** *August 06, 2016 01:14*

**+Jon Bruno** smoothie is not using M3/M5 commands.  It uses G1 to lase while moving  and S for power.  


---
**Jon Bruno** *August 06, 2016 01:39*

M3 M5 controls the enable pin as per the config but Im not a g-code guru so I'm not sure.. I do know however that it was able to properly control the enable pin with 2.5^ and manually sending M3/M5 resulted in proper operation as well. but more importantly, the pin is in the expected state when idle.


---
**Jon Bruno** *August 06, 2016 01:47*

**+David Cook**  this is what mine says...

switch.laserfire.enable true #

switch.laserfire.output_pin 2.5^ # 

switch.laserfire.output_type digital #

switch.laserfire.input_on_command M3 # fire laser

switch.laserfire.input_off_command M5 # laser off


---
**David Cook** *August 06, 2016 01:55*

I suppose.. but my gcodes from laser web do not have m3 or m5 in them. So I'm not sure how that actually works. I think m3)m5 is more for grbl or Marlin. Not sure. I just know I don't use them


---
**Jon Bruno** *August 06, 2016 02:01*

I dont know man.. I just hack away and when something works I share.. 

I wouldn't have gotten this far without your help.


---
**David Cook** *August 06, 2016 02:04*

**+Jon Bruno** oh I'm the same way. I trial and error this thing into what it is today.   I did populate the optional areas in LaserWeb with the M3/M5 commands in the beginning. But then I learned how G1 works and came to the conclusion I did not need M3/M5.  Those commands would not have worked with my current wiring anyway now that I know my fire pin is perpetually grounded lol.  


---
**Jon Bruno** *August 06, 2016 02:05*

LOL!! true


---
**Damian Trejtowicz** *August 30, 2016 05:23*

How do You guys set up power levels?i mean i done all like wiring,except level shifter and i can only set up max 60% of power because this is allready 20mA,

When i use laserweb i can only use power from 10 to 60%


---
**David Cook** *August 30, 2016 15:51*

**+Damian Trejtowicz** I set the max limit in the config file.  mine is set to .8     and in Laserweb  it is 1    so that the gcode generator will assign S levels from .1 up to 1   with 1 being 100%  ( of the 80% max limit I set)


---
**David Cook** *August 30, 2016 15:54*

laser_module_maximum_power                   .8             # this is the maximum duty cycle that will be applied to the laser


---
**Damian Trejtowicz** *August 30, 2016 16:22*

In my case when i setup 0.8 amps meter show 30mA on 100%


---
**David Cook** *August 30, 2016 16:27*

It's probably because your laser power supply is not current limited properly for your tube.  I think it's a way they cheat to make it seemore powerful but at the expense of shorter life. You'll want to set you max duty cycle I'm the config file to limit max laser current to do.ething that you are comfortable with 


---
**Jon Bruno** *August 30, 2016 18:08*

**+Damian Trejtowicz** You need to use the level shifter to properly control the power output.

The two wire setup requires it. The pin 1.23 is a PWM output that needs to be raised from 3.3 (MCU pin voltage) to 5v logic voltage. if you don't use a level shifter to switch from 3.3 to 5v logic unexpected results can occur.


---
**Jon Bruno** *August 30, 2016 18:15*

Hey David.. when you gonna fix that wiring diagram? Laser enable should be on 2.5- not TH4- lol


---
**David Cook** *August 30, 2016 18:21*

**+Jon Bruno** well that is actually how my working system is currently wired up at the moment :-)~


---
**Jon Bruno** *August 30, 2016 18:35*

Yeah, true..... but it's not right..

You could just change it to "Tie to any available ground." 

lol


---
**Damian Trejtowicz** *August 30, 2016 19:27*

For me level shifter is not a problem because if i not get 5v i should not get 30mA on level meter


---
**Jon Bruno** *August 30, 2016 19:34*

There is a difference between analog voltage and a TTL signal.


---
**Damian Trejtowicz** *August 30, 2016 19:43*

I understand this but previous power supply when level shifter was removed it was like i wrote,i was only able to get 50% of power

But i will try with level shifter

As the source 3.3v what should i use(previously i was using power pin from endstops,),as the 5v reference i was using 5v from 24v line in psu


---
**Damian Trejtowicz** *August 30, 2016 19:44*

I understand this but previous power supply when level shifter was removed it was like i wrote,i was only able to get 50% of power

But i will try with level shifter

As the source 3.3v what should i use(previously i was using power pin from endstops,),as the 5v reference i was using 5v from 24v line in psu


---
**Jon Bruno** *August 30, 2016 19:45*

Are you using the MKS SBase 1.3?


---
**Jon Bruno** *August 30, 2016 19:54*

I do not know what end stops you are using but I would assume you have a standard k40 which uses 5v endstops. (if MKS SBase 1.3) switch the endstop voltage jumper to 5v and use the spare V and G pins in the unpopulated endstop connectors.the 3.3v tap can be taken from the external stepper driver pins.. I use E1's 1st pin..  check with a meter first should read ~3.3v (P4.29?)


---
**Damian Trejtowicz** *August 31, 2016 04:35*

I use mks sbase 1.3 and i switched jumper to 3.3v. I get 5v for endstops from tft plug


---
**Jon Bruno** *August 31, 2016 11:30*

Ok. If all of your voltages are correct you should be good to go.


---
**Damian Trejtowicz** *September 02, 2016 07:00*

Update.

All done like should be and looks like my PSU is for bigger laser tube because on 100% my ampmeter is out of [scale.in](http://scale.in) my case i get 20mA when i setup .6 as max.,or my level shifter is broken


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/YhfFyNfKRMt) &mdash; content and formatting may not be reliable*
