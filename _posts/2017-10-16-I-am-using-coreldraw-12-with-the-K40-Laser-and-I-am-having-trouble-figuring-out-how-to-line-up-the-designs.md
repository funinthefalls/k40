---
layout: post
title: "I am using coreldraw 12 with the K40 Laser and I am having trouble figuring out how to line up the designs"
date: October 16, 2017 21:06
category: "Software"
author: "Doug Western"
---
I am using coreldraw 12 with the K40 Laser and I am having trouble figuring out how to line up the designs.  I have attached the picture, but I am toying around with an ornament and I put the design on and then draw the outline circle to cut in corel draw.  I then send the design to the laser to engrave, and then send the circle to the laser to cut.  I guess when I send it, it lines each part op to the top left instead of keeping it in the position it was in the design.  Can some one give me some help on how to do this, I am obviously overlooking something.  You can see the results

![images/2c832d9f386685a2978ac6a80dcdfe08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2c832d9f386685a2978ac6a80dcdfe08.jpeg)



**"Doug Western"**

---
---
**Sebastian C** *October 16, 2017 21:34*

the problem I encountered as well: Corel will position everything on the top left corner (more precise the corner you specified in the LaserDraw pugin).

Your engraving is smaller than your circle/cut and so both will be send to the corner, ruining your workpiece.

Normally I just engrave the cut as well or place a tiny square or dot  (some 0,01mm)  on the top left of the project which will be cut and engraved and everything is then relative to this 'zero point'. 

This workaround sucks but you will get used to it until you upgrade to a more sophisticated software with another board. (looking at you cohesion and lightburn :)



If I remember correct the K40 Whisperer software will do as well, but  personally did not like the slow processing on my old laptop and got used to the corel process. 








---
**Sebastian C** *October 16, 2017 21:40*

![images/161abd7787cdc1d62cf76050e4be15f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/161abd7787cdc1d62cf76050e4be15f2.jpeg)


---
**HalfNormal** *October 17, 2017 00:49*

You can always reference your drawing in LaserDRW by center instead of top left corner. Everything should line up fine.


---
**Ned Hill** *October 17, 2017 01:25*

Just helped someone with this today [plus.google.com - Probably an easy fix. Using Corel draw. Making nametags out of acrylic. En...](https://plus.google.com/u/0/105465271425419072597/posts/LZJWAbTeqnL)




---
**Phillip Conroy** *October 17, 2017 07:00*

Put a small mark line top left in both for a start spot for laser


---
**Sebastian C** *October 17, 2017 09:28*

**+HalfNormal** but if you center align, you will have the same issue, only this time moved to the center. This works for centered designs though.

The only way I figured out is harcoding a common origin marker on both engraving an cutting design/layer. A box will do the trick as well, but will extend the engraving time (depending on the dseign) 


---
**HalfNormal** *October 17, 2017 12:42*

You also have to remember to put the pixel/box in the design and make it white so it will not engrave/cut. Most engrave/cut designs are centered. Yes not all but most.


---
**Doug Western** *October 17, 2017 12:45*

Thanks guys, I have had since that I designed in a program and imported in and just made two images the same size and did it that way.  I was hoping for an easier way I guess.  😁


---
**Gary Balu** *October 22, 2017 16:10*

The simple solution is to center everything at the same point. Find the center of your circle. Place the center of your design at the same point. Cut or engrave off that center point. 




---
*Imported from [Google+](https://plus.google.com/102298503204290496961/posts/YYXs3JEHdSk) &mdash; content and formatting may not be reliable*
