---
layout: post
title: "So glad I switched to a smoothieboard"
date: August 30, 2016 03:33
category: "Smoothieboard Modification"
author: "Alex Krause"
---
So glad I switched to a smoothieboard. "I'm giving it all she's got Captain"

![images/51ad91b4ca67042fa70ce3d24090d007.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51ad91b4ca67042fa70ce3d24090d007.jpeg)



**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 30, 2016 04:00*

Very nice results on that.


---
**Alex Krause** *August 30, 2016 04:07*

**+Yuusuf Sallahuddin**​ thanx brother can't wait to see more of your image engraves


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 30, 2016 07:06*

**+Alex Krause** I've got big plans for one on leather for a bag in the near future. Will be coming up in a week or two after I get more leather.


---
**Marvin Pagaran** *August 30, 2016 11:41*

How did you prep the pic?


---
**Alex Krause** *August 31, 2016 04:04*

**+Marvin Pagaran**​ this is a grayscale raster done on a smoothieboard upgrade I pre process the image I'm Gimp photoshop but this can be done in a variety of image editing software with advanced processing such as contrast/brightness,curve adjust, and desaturate. There was no dithering of the image


---
**Jez M-L** *September 07, 2016 13:42*

Alex,



What settings did you use in laserweb for the power, white and dark speeds please?  I want to get a rough ballpark on what I should be using


---
**Skull Duggary John E Clark** *September 17, 2016 17:45*

I am also.  Could I ask a question?  Do you use the level shifter for PWM?  If so, can you take a pic of how you have it wired for me to reference?




---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/fB3erjq8iBK) &mdash; content and formatting may not be reliable*
