---
layout: post
title: "Lots Of Cool Stuff to Make! Just look at this bowl!"
date: December 04, 2016 04:17
category: "Repository and designs"
author: "HalfNormal"
---
Lots Of Cool Stuff to Make!



Just look at this bowl!

[https://www.zenziwerken.de/Uebersicht](https://www.zenziwerken.de/Uebersicht)

![images/610aceb17cd2338761375c646a634b46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/610aceb17cd2338761375c646a634b46.jpeg)



**"HalfNormal"**

---
---
**Kelly S** *December 04, 2016 06:18*

There is some really neat material on here, thanks for the share. 


---
**E D** *December 04, 2016 08:26*

Coool.

Did you all the staff on website using K40?


---
**Stephane Buisson** *December 04, 2016 08:34*

post moved to Repository & designs section


---
**HalfNormal** *December 05, 2016 01:52*

**+Enrico Deppi** most of us own a K40 or two. Others here have moved on to bigger and better units but still hang out. We welcome everyone to come and enjoy the site.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/QZQDbNe84w1) &mdash; content and formatting may not be reliable*
