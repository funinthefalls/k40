---
layout: post
title: "Originally shared by Ashley M. Kirchner [Norym] After my last experiment with colors, and vowing not to do that again for a while (because of the labor involved), I had to try some new release liner tape that we have laying"
date: March 28, 2016 02:37
category: "Discussion"
author: "Jose A. S"
---
<b>Originally shared by Ashley M. Kirchner [Norym]</b>



After my last experiment with colors, and vowing not to do that again for a while (because of the labor involved), I had to try some new release liner tape that we have laying around at work (graphics shop), so naturally ... more experiments with engraving and painting. Though in this case I also threw in some playing with speed and power. There is white paint there, however because it gets burned, you can't really tell. I considered re-painting it after the slits were cut but decided I like the rustic look so I left it as is and just peeled the tape of. Since I do two at a time, I am going to re-paint the white on the second one and compare them.



Edited to add: Edited to add: The tape I'm using is made by RTape ([http://www.rtape.com/product-lines/conform-series-with-rla](http://www.rtape.com/product-lines/conform-series-with-rla)) and it's specifically the 4050RLA and 4075RLA ones. It can also conform to curved surfaces (hence the name 'Conform'). Easy to use, easy to lift, leaves no residue.



![images/a4eee620ed037c762ef23218319bfb08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4eee620ed037c762ef23218319bfb08.jpeg)
![images/1fbbe2a1017c6715273e41e80d1abe77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fbbe2a1017c6715273e41e80d1abe77.jpeg)

**"Jose A. S"**

---


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/djxLMie5PW3) &mdash; content and formatting may not be reliable*
