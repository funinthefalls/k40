---
layout: post
title: "It happened my k40 died after a year and 3 months"
date: October 30, 2018 10:10
category: "Original software and hardware issues"
author: "Printin Addiction"
---
It happened my k40 died after a year and 3 months. I am assuming it is the power supply. It worked fine last weekend, but this past Saturday the power supply was squealing at 42% power, usually I only heard that noise at below 22%.  Plus,  it wasn’t engraving at 42%, (usually I use 28-36%) so I started increasing power until it started engraving, which was at 68% (highest I ever had it). I was able to do a few small pieces at that power, then while watching it engrave I saw the dot almost disappear to barely noticable. At that time increasing the power had no effect. Is it possible the tube went, or is it the power supply. 





**"Printin Addiction"**

---
---
**Don Kleinschnitz Jr.** *October 30, 2018 15:05*

Most likely the supply's HVT. What kind of coolant?




---
**Printin Addiction** *October 30, 2018 21:59*

**+Don Kleinschnitz Jr.** just distilled water in a 5 gallon bucket.


---
**Printin Addiction** *November 01, 2018 10:11*

**+Don Kleinschnitz Jr.** Will I be able to tell if it is the HVT that went bad by examining it?


---
**Don Kleinschnitz Jr.** *November 01, 2018 12:14*

**+Printin Addiction** 

<b>Disclaimer: Warning, laser High Voltage supplies have extremely high voltages and can hold a lethal charge even after the power is removed. Do not attempt repair of these supplies unless you have certified training and experience in High Voltage electronic equipment and testing and repair procedures. </b>

<b>Any attempt to test or repair a HV supply is at your risk.</b>



You usually cannot tell because the internal diodes go bad. You can try taking the cover off the LPS and running the machine in the dark to see if you can see corona or arcs around the HVT. 



You can also tape a grounded wire on the tube with a bared end within a <1/2" of the anode. Then run the laser and see if there are violent arcs when you pulse test. Pulse briefly do not hold the test for any length of time.



You can replace the HVT but lately I have seen pretty cheap supplies @ about $40 so it may not be worth it.




---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/6nPhXC8wbKU) &mdash; content and formatting may not be reliable*
