---
layout: post
title: "Hey all just got a used k40"
date: December 06, 2016 14:19
category: "Modification"
author: "Chris Menchions"
---
Hey all just got a used k40. I am looking to change it over to mks gen board. Are there any wiring how to's around?



The motors, belts, pulleys, and endstop are done. Just need to figure out wiring the psu and controlling the laser via pwm.



Any help is appreciate





**"Chris Menchions"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 14:25*

You already have the MKS board or you are looking to get one? 


---
**Chris Menchions** *December 06, 2016 14:28*

**+Ray Kholodovsky** I already have 3 MKS gen boards laying around. My sbase is in use and 1 Arduino/ramps. I'm completely new to lasers and looking for wiring help


---
**Don Kleinschnitz Jr.** *December 06, 2016 14:58*

This conversion is not exactly the same as you describe but the technical details may help you with your conversion.

[donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



The following it an example of a LPS PWM interface.

[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**Chris Menchions** *December 06, 2016 14:58*

Awesome thanks Don...I'll have a look shortly, how u don't mind a few questions maybe!


---
**Don Kleinschnitz Jr.** *December 06, 2016 15:49*

**+Chris Menchion** certainly willing to pay forward all the help I got from this forum. Let the questions flow.


---
**Chris Menchions** *December 06, 2016 17:46*

Lol and it starts...is there a parts list anywhere?



What is and do I absolutely need opto-coupler?



Thanks Don


---
**Chris Menchions** *December 06, 2016 17:49*

Btw are u in Canada? Newfie here!


---
**Don Kleinschnitz Jr.** *December 06, 2016 19:01*

**+Chris Menchion** what parts you need depends on what you plan to do. Many of the parts are included in the schematics and the associated sections of the link I sent you.



What opto-coupler are you referring to?


---
**Chris Menchions** *December 06, 2016 19:10*

**+Don Kleinschnitz** well I was just reading the pwm section. Bare with my I'm wiring and on mobile so trying to read when I can.






---
**Don Kleinschnitz Jr.** *December 06, 2016 19:21*

If your referring to this section 



[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)



... you only have to find an open drain to connect to the laser power supply "L" pin.



The optocouplers I think you are referring to are inside the supply?



I can tell you exactly how if I had a schematic of your controller board and a picture of your LPS connectors.


---
**Chris Menchions** *December 06, 2016 19:21*

I'll look more tonight. Thanks for all the time and energy you put in to your blog


---
**Chris Menchions** *December 06, 2016 19:26*

Sadly I haven't sized up the psu. I was concentrating on getting the gantry all refurbished. Now it's time to get the wiring down. Add started I have my motors, belts pulleys and endstops completed.



Thanks a million. I know time is precious I run the Folgertech 3d printers group on Facebook with over 2600 members and co run the BLTouch Facebook group.

So thanks for all the hard work!


---
**Don Kleinschnitz Jr.** *December 06, 2016 19:28*

**+Chris Menchion** if this is the board you will probably have a challenge with schematics ...



[http://reprap.org/wiki/MKS_GEN](http://reprap.org/wiki/MKS_GEN)


---
**Chris Menchions** *December 06, 2016 19:31*

Yes that it it's. Mks gen v1.4. It's 8 bit atmega. Basically all in 1 ramps/Arduino stack. Exactly same pinouts but able to take 24v.


---
**Chris Menchions** *December 06, 2016 19:34*

I'm actually thinking about the smini but that is an after Xmas thing now. Make due with what I have.


---
**Don Kleinschnitz Jr.** *December 06, 2016 19:41*

take a look Cohesion3D

[https://plus.google.com/communities/116261877707124667493](https://plus.google.com/communities/116261877707124667493)

 with **+Ray Kholodovsky** piece of cake conversion.


---
**Chris Menchions** *December 06, 2016 19:42*

Awesome. I'll take a look tonight!


---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 20:05*

Yes, you'd really want a 32 bit board for laser pwm and motion control.  Smoothie currently implements this best and we have a Cohesion3D Mini for K40 bundle that drops right in. 

Cohesion3d.com [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://Cohesion3d.com)


---
**Chris Menchions** *December 06, 2016 20:08*

Then I guess I'll use my sbase. Sadly my print quality well suffer lol


---
**Chris Menchions** *December 07, 2016 19:58*

Just ordered a mks s mini. That should do the trick right?


---
**Chris Menchions** *December 07, 2016 19:59*

Just ordered a mks s mini. That should do the trick right?


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 20:09*

If you look thru some of these g+ groups including the Smoothieware one you will find loads of people with issues with their MKS smoothies. I advise you cancel that order and purchase a known working smoothie. Doesn't have to be from me (although you're welcome to check my reputation around here), but don't expect any support from the community regarding MKS products. 


---
**Chris Menchions** *December 07, 2016 20:16*

Yeah mks can be very finicky. Took me a few weeks to get it stable on one of my printers. The biggest thing it's they don't like electrical noise.



I got the mini for 40cad shipped. I can't justify taking my sbase off my 3d printer and go back to Arduino.



Not can I afford 100 or so on a board being close to Xmas and supporting my wife and 3 kids. 



I'll try with mks if it gives me to much trouble I'll sell it and buy better in the new year.



Thanks I appreciate the input


---
**Chris Menchions** *December 07, 2016 20:18*

Btw it was my deep freezer that was causing interference lol


---
**Chris Menchions** *December 07, 2016 20:18*

Btw it was my deep freezer that was causing interference lol


---
**Chris Menchions** *December 13, 2016 13:53*

![images/8530ca5734fb298de8d2f1791a7218fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8530ca5734fb298de8d2f1791a7218fd.jpeg)


---
**Chris Menchions** *December 13, 2016 13:53*

![images/fcd91822d9b9a4b565c4d52b4b5fb53a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fcd91822d9b9a4b565c4d52b4b5fb53a.jpeg)


---
**Chris Menchions** *December 13, 2016 13:54*

Any info on this type of supply? I tried your site Don but didn't match up.


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 13:59*

Looks like mine with the JST connectors - I recognize those. Not sure where the green pluggable terminal (2nd pic) is in relation to the first pic. 


---
**Chris Menchions** *December 13, 2016 14:00*

in on the opposite side of the psu same side as the "clear" 4 pin


---
**Chris Menchions** *December 13, 2016 14:01*

What i mean is on the back but same side of the board right/left


---
**Chris Menchions** *December 13, 2016 14:03*

I'm redoing the wiring and the psu wasn't plugged in so kinda lost here. What would k+/- and d+/- mean


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:14*

**+Chris Menchion** I'm guessing using the same naming. 

On my supply K-, K+ is the "Laser Test"

that would logically leave the D-, D+ to be the "Laser Switch" function (interlocks and panel switch).


---
**Chris Menchions** *December 13, 2016 15:15*

What is the 3 pin for? gnd,in,5v


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 15:21*

Potentiometer. IN is the wiper. That's where our pwm signal feeds into. 


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:23*

**+Ray Kholodovsky** I would not use IN I would use "L" :).


---
**Chris Menchions** *December 13, 2016 15:23*

Ok L is on the bigger 4 pin


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:24*

Put pot (5K) across 5v and gnd with the center wiper on IN.


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:24*

On the connector with the 24VDC.


---
**Chris Menchions** *December 13, 2016 15:25*

Well I will be using PWM on Smoothie is the plan. 




---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2016 15:26*

**+Don Kleinschnitz** I know you prefer the L approach. I like replacing the pot. It keeps settings absolute and works for my customers. 


---
**Chris Menchions** *December 13, 2016 15:28*

I need to go get some connectors etc. 30cm snow today but that won't stop me lol


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:48*

**+Ray Kholodovsky** the problem is not that it will not work and not that I prefer it. I will just say it, with respectful disaproval: "ITS A HACK".



Its that the use of IN as an input from smoothie digital PWM creates unpredictable power control levels. (That per-job tweaking will correct).

I have proven this tecnically every way I know how to. No-one has found anything wrong with using "L" even though it is simpler, more predictable and uses less parts? Go figure!


---
**Chris Menchions** *December 13, 2016 16:22*

No offense guys I don't want to start anything here. You both provided great info, Which one is "better" is like 3d printing and calibrating the extruder by steps or by multiplier. I do not want to use a pot I'd like to have it all software controlled via the pwn 1.23 pin




---
**Don Kleinschnitz Jr.** *December 13, 2016 16:40*

**+Chris Menchion** :) you aren't starting anything new. **+Ray Kholodovsky**​, others and I have been bantering this for some time. Nothing personal intended. 

BTW you do need the pot to adjust max power as the laser wears and you need to keep power under 18ma. You don't want to have to keep changing config file %.

The L controls the power from smoothie and pot controls intensity. 


---
**Chris Menchions** *December 13, 2016 16:41*

Ok I thought so glad thats clearer.  going to size this up now




---
*Imported from [Google+](https://plus.google.com/104576389148296391165/posts/Mfuz711rjpV) &mdash; content and formatting may not be reliable*
