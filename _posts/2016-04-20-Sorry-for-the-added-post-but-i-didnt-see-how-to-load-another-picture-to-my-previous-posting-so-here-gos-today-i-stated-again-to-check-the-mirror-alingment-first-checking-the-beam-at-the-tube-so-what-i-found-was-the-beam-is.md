---
layout: post
title: "Sorry for the added post but i didn't see how to load another picture to my previous posting so here go's today i stated again to check the mirror alingment first checking the beam at the tube so what i found was the beam is"
date: April 20, 2016 20:46
category: "Discussion"
author: "Dennis Fuente"
---
Sorry for the added post but i didn't see how to load another picture to my previous posting so here go's today i stated again to check the mirror alingment first checking the beam at the tube so what i found was the beam is coming out of the tube split dose this mean the tube is bad i did a test seeing how much power it took to cut through 3 MM pop ply i had it all the way up to 15 Ma and it would not cut through 

![images/1f0e783a1fe466bcd8032ff10c750137.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f0e783a1fe466bcd8032ff10c750137.jpeg)



**"Dennis Fuente"**

---
---
**Joe Keneally** *April 20, 2016 21:13*

Have you checked to be sure the exit port for the beam from the tube is clean and defect free?


---
**Dennis Fuente** *April 20, 2016 21:37*

I have had nothing but problems with this machine from day one i took a real good look a the exit end of the tube and i think i maybe see a small crack in it not sure don't see any water leak outside or inside the tube, nothing in the end of the tube like debree or anything so not having any experince with these machines i don't know what i am looking at or what is normal i will try and take a picture of the tube and post it 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2016 22:27*

There was a recent post where I think Scott Thorne had his beam coming out doughnut shaped right at the tube, when firing at low power. Does the beam still come out with this "split" when higher powers are used (e.g. 10mA)?


---
**Dennis Fuente** *April 20, 2016 22:29*

do you think my tube is the problem all along i posted a picture of the tube end nearest teh first mirror 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/53ywwcpCAMu) &mdash; content and formatting may not be reliable*
