---
layout: post
title: "I see designer jeans in the future!"
date: May 30, 2016 12:48
category: "Object produced with laser"
author: "Scott Thorne"
---
I see designer jeans in the future!

![images/2dbacce7fa8b11305b4cf905337afeae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2dbacce7fa8b11305b4cf905337afeae.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 30, 2016 13:07*

Looks good Scott. Just got to be careful to not hit it with too much laser. Gentle like this makes a nice effect.


---
**Scott Thorne** *May 30, 2016 13:43*

**+Yuusuf Sallahuddin**...that was 10% power and 300mm/s speed....turned out great.


---
**Ariel Yahni (UniKpty)** *May 30, 2016 13:53*

Have you washed them? 


---
**Scott Thorne** *May 30, 2016 15:11*

**+Ariel Yahni**...just in the sink...not in the washer...but it's still sturdy right there where the raster is. 


---
**Ariel Yahni (UniKpty)** *May 30, 2016 15:17*

I did mine with several different setting and that better one faded once out of the washer


---
**Alex Krause** *May 30, 2016 18:23*

Throw on some fabric paint and let it dry before you laser.... I had to do something similar on some black leather I engraved to make it "Pop"﻿


---
**Jeremie Francois** *May 30, 2016 19:33*

I hope you removed your leg first ;)


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/GnFHhTn3f75) &mdash; content and formatting may not be reliable*
