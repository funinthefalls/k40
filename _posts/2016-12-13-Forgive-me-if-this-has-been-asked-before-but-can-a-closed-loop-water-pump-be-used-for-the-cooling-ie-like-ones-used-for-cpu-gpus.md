---
layout: post
title: "Forgive me if this has been asked before, but can a closed loop water pump be used for the cooling (i.e., like ones used for cpu/gpu's)?"
date: December 13, 2016 13:42
category: "Discussion"
author: "James G."
---
Forgive me if this has been asked before, but can a closed loop water pump be used for the cooling (i.e., like ones used for cpu/gpu's)?





**"James G."**

---
---
**Anthony Bolgar** *December 13, 2016 13:56*

I am not sure, but from my experience with my watercooled spindle on a cnc, it barely handles that. I really do not see being able to use just a cpu cooler by itself, maybe with a small reservoir it might work. If you do try it, make sure you have a temp guage and shut it down if the temp gets too high. Don't want to fry the tube.


---
**James G.** *December 13, 2016 14:00*

Yeah, I was just thinking of using the pump, large reservoir and radiator


---
**Stephane Buisson** *December 13, 2016 14:02*

CO2 tube work best close to 15°C water, some use frozen water bottle in the bucket to reduce temp. (depending on seasons and where you are on the globe)


---
**greg greene** *December 13, 2016 14:16*

I use an Aquarium heater - cooler - digital controls - cooler feeds tube first, then water exits into a  4 litre container which then feeds the pump, the pump feeds the cooler - works very well in my basement location.  These coolers are fairly cheap and made for 24/7 operation


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:06*

**+greg greene** what cooler are you using?




---
**greg greene** *December 13, 2016 15:12*

this one



[http://www.ebay.com/itm/Aquarium-Thermostat-Chiller-Heater-Adjustable-Fish-Tank-Salt-Fresh-Water-/282033379947?hash=item41aa7fda6b:g:FDEAAOSwa~BYQkCJ](http://www.ebay.com/itm/Aquarium-Thermostat-Chiller-Heater-Adjustable-Fish-Tank-Salt-Fresh-Water-/282033379947?hash=item41aa7fda6b:g:FDEAAOSwa~BYQkCJ)


---
**Don Kleinschnitz Jr.** *December 13, 2016 15:22*

**+greg greene** interesting there was a discussion on these some time ago and users reported that they did not cool enough. I think these are peltier coolers/heaters?


---
**greg greene** *December 13, 2016 15:53*

Yes, mine works well and there are more on their website that are perhaps more suited - but it works well enough for now.  I track the digital thermometer as I use the cutter and it seems to keep up fairly well.  One thing I do do, is to start it up a half hour or so before I start cutting so that the reservoir is well chilled, That helps a lot.


---
**Don Kleinschnitz Jr.** *December 13, 2016 16:41*

**+greg greene** I may try this summer. 


---
**HP Persson** *December 13, 2016 17:41*

Most of the PC watercooling pumps are not submersible, you need to have the pump below and outside the tank, then it´s ok to use PC watercooling pumps. You still need the volume of coolant though, like 2-5gallons atleast.



Plenty more flow in them than the china fish tank pump.

I built my complete cooling loop from PC cooling parts. tubing, radiators, fans, fittings and temp controllers with a 240mm radiator outside my window with four 120mm fans, works really good in the Swedish climate :)


---
**James G.** *December 13, 2016 17:53*

So it's not a closed loop system then?  I didn't plan on submerging it.   I was thinking either externally mounted 240 or 480mm radiator and a couple of in-line reservoirs , which I would think could dump the heated water/coolent pretty efficiently. What are you keeping the 2-5 gallons of coolent in? Have a picture of the setup by chance?  Thanks


---
**Jim Bilodeau** *December 16, 2016 01:33*

I have a closed loop water cooling setup with a 120mm radiator and it works well enough for short runs. I would recommend going larger though - 240mm at least. I'll probably switch mine out with 240mm if I start running the laser longer than 1-hour straight. (see mid-way through page for my build for reference [http://jimmybdesign.com/k40-laser-cutter/](http://jimmybdesign.com/k40-laser-cutter/) )


---
*Imported from [Google+](https://plus.google.com/114312362882399477330/posts/Rr6c9dQfFU6) &mdash; content and formatting may not be reliable*
