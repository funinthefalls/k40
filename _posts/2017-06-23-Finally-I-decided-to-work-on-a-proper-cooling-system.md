---
layout: post
title: "Finally I decided to work on a proper cooling system"
date: June 23, 2017 23:44
category: "Modification"
author: "Bruno Ferrarese"
---
Finally I decided to work on a proper cooling system. 



Brought home a mini refrigerator from Lowes for $69. 



Not sure if I'm disassembling and dropping the cooling element/temp sensor inside the water bucket or if punching a couple of holes and putting a water reservoir inside the chamber. Probably going with second option. 



![images/1ab0480bed006daedc325e54363daaa3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ab0480bed006daedc325e54363daaa3.jpeg)
![images/e7d26f16ad3329e27a95b31736ad0362.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e7d26f16ad3329e27a95b31736ad0362.jpeg)
![images/52a44c2c3775c9f9e7ab0a450de6927c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52a44c2c3775c9f9e7ab0a450de6927c.jpeg)
![images/383e4ec6be333307f6b28ca9aa90985e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/383e4ec6be333307f6b28ca9aa90985e.jpeg)
![images/0aa1bdb6d9f293f725585593fe460eeb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0aa1bdb6d9f293f725585593fe460eeb.jpeg)

**"Bruno Ferrarese"**

---
---
**Don Kleinschnitz Jr.** *June 24, 2017 00:10*

#K40lasercooling

I was thinking about this approach with a copper coil inside the frig?


---
**Joe Alexander** *June 24, 2017 00:21*

**+Don Kleinschnitz** that was my idea also but I concluded I would still have to have the copper submerged within the fridge to increase the temperature transfer. toying with peltiers atm instead :) and yes I am diligently stalking that post you tagged earlier, hes got the right idea.


---
**Don Kleinschnitz Jr.** *June 24, 2017 00:25*

**+Joe Alexander** I like the peltier idea but it seems a lot less efficient and a lot more $ than a small frig. with a coil or reservoir. 


---
**Joe Alexander** *June 24, 2017 02:30*

yea i can see that. as i already had the ssr and a potential controller all it cost me so far was the peltiers(8x 12706 for 15$ on wish). Hoping the operating cost in Kw/h is not too high in comparison to the mini fridge. could be a better cooling device though, potentially able ot handle heavy usage and the fluctuations thereof.


---
**Josh Rhodes** *June 24, 2017 04:45*

These fridges are frequently cheap/free, especially when the semester ends at colleges... 


---
**Bruno Ferrarese** *June 25, 2017 17:27*

WIP

![images/222aae8dcaefa8f27eb3637a47d340ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/222aae8dcaefa8f27eb3637a47d340ff.jpeg)


---
**Abe Fouhy** *June 26, 2017 16:52*

I think adding a reservoir in the fridge with a cool in the reservoir should work well


---
**HP Persson** *June 26, 2017 23:38*

Any plan to swap out the pipes and heat exchanger?

Could go pretty small with that setup, i used a similar sized compressor to cool a PC years ago :)


---
**Bruno Ferrarese** *June 27, 2017 03:38*

Building a mini chamber (1/4 gallon only)

![images/602ab2e7d455454728141b11d47863dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/602ab2e7d455454728141b11d47863dc.jpeg)


---
**Bruno Ferrarese** *June 27, 2017 03:39*

Actually less than a quarter (first time using foam - that thing grow as hell)

![images/a0e315d3f2ca1d9bc3613f20c8c02548.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e315d3f2ca1d9bc3613f20c8c02548.jpeg)


---
**Bruno Ferrarese** *June 27, 2017 03:42*

Still working on the condenser coils. Lid in the works. 

![images/bf43882f0bf2e9853e959e6d613618b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf43882f0bf2e9853e959e6d613618b1.jpeg)


---
**Josh Rhodes** *June 27, 2017 04:24*

I'm confused.. are those the hot coils...


---
**Bruno Ferrarese** *June 27, 2017 14:00*

**+Josh Rhodes** Yes


---
*Imported from [Google+](https://plus.google.com/111443543748488809183/posts/YDKMrFQTm1p) &mdash; content and formatting may not be reliable*
