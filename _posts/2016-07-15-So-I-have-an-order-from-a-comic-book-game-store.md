---
layout: post
title: "So I have an order from a comic book/game store..."
date: July 15, 2016 16:15
category: "Materials and settings"
author: "Alex Krause"
---
So I have an order from a comic book/game store... I keep running into the cast acrylic wanting to Bow upwards at the ends of the finish part ... is there a way to keep from having to much distortion of the parts

![images/e6cc7904704590485684a1d19a37d14b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e6cc7904704590485684a1d19a37d14b.jpeg)



**"Alex Krause"**

---
---
**HP Persson** *July 15, 2016 16:29*

When i do alot of small stuff i have one layer with the cut, and leaving out some small tabs so it doesnt bend or twist.

Then i run the next layer, cutting theese tabs away.



You can also try with more air from fans, keeping the parts cool prevents alot of warping. Noticed this on thinner acrylic.

I got two 80mm fans in front of the laser and a 50mm at the laser head, helps alot (with smoke too).


---
**3D Laser** *July 15, 2016 16:47*

Alex do you have the measurements of the x wing templates I want to make some and a tray but my designing skills are very low right now 


---
**Ariel Yahni (UniKpty)** *July 15, 2016 17:11*

Could you run lower power multiple pass


---
**Alex Krause** *July 15, 2016 17:14*

**+Corey Budwine**​ I have a design made for CnC Router that I'm converting over to have nicer Numbering and arrows. I am working on getting the laser Kerf adjusted in the design to keep these within .200 -.400 mm of the ones that come with the starter packs 


---
**Alex Krause** *July 15, 2016 18:05*

**+Ariel Yahni**​ I will give that a shot


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/HXuXkigYPVy) &mdash; content and formatting may not be reliable*
