---
layout: post
title: "K40 at work, let's start the fun !"
date: January 06, 2016 16:33
category: "Object produced with laser"
author: "Stephane Buisson"
---
K40 at work, let's start the fun !



I am designing a kind of "bread board" frame to create automatas.

things start to come togethers, you start to understand why I posted the involute gears, 507  movements, ... links.



![images/a564fff7ea0eb4586a97c95d87f2b495.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a564fff7ea0eb4586a97c95d87f2b495.jpeg)
![images/3ae41e22ef17ede45b3396e2a5e0a014.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ae41e22ef17ede45b3396e2a5e0a014.jpeg)
![images/aa1af135e7024207d99db35d02e5d3e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa1af135e7024207d99db35d02e5d3e4.jpeg)
![images/fe4ca544f8fafaa70452ae0c78a97d0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe4ca544f8fafaa70452ae0c78a97d0f.jpeg)

**"Stephane Buisson"**

---
---
**Ashley M. Kirchner [Norym]** *January 06, 2016 16:37*

What material is that?


---
**Stephane Buisson** *January 06, 2016 16:41*

**+Ashley M. Kirchner** fruit boxes from the market ;-)), sort of 3 mm plywood. (look at my very early posts)


---
**Ashley M. Kirchner [Norym]** *January 06, 2016 16:59*

Oh nice, recycled material! Eco friendly automata.


---
**David Cook** *January 10, 2016 06:32*

That's cool


---
**Gary McKinnon** *January 13, 2016 12:03*

Wow! Amazing! I've been into robots/AI/Automatons for a while but not had the chance to build much. By the way i just saw your hangout request, sorry but i don't check that Google mail i just have it so i can use Google stuff but don't use it for email.


---
**Stephane Buisson** *January 13, 2016 13:11*

**+Gary McKinnon** not a big deal, it was to say, i was happy with this community only, and wasn't looking for more work for myself with other website/forum.


---
**Gary McKinnon** *January 13, 2016 13:20*

Ah yes, okay thanks.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/JDHyrWf4x6X) &mdash; content and formatting may not be reliable*
