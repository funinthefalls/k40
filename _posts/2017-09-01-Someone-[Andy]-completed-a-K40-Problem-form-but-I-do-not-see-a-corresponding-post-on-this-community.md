---
layout: post
title: "Someone [Andy] completed a K40 Problem form but I do not see a corresponding post on this community"
date: September 01, 2017 12:02
category: "Original software and hardware issues"
author: "Don Kleinschnitz Jr."
---
Someone [Andy] completed a K40 Problem form but I do not see a corresponding post on this community.



[https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit#response=ACYDBNhJhuVO1KNiZSICeX9IP13FiefCPjcII6V6WTTiHV7gqSPmBpb1tu65uQ](https://docs.google.com/forms/d/1MobwxZ8COA6bW_0utAhYLYkoiEJZQERNs5wLi_fXgsY/edit#response=ACYDBNhJhuVO1KNiZSICeX9IP13FiefCPjcII6V6WTTiHV7gqSPmBpb1tu65uQ)



The problem is stated in the form as: 

<b>I replaced a bad stepper motor with the only one I could find, and now if I cut a 25mm circle, it will cut a 25mm X 50mm oval. All of my "X" movement is doubled. Is there something i can change to fix this? Or do I need a new motor?</b>



Andy:

Can you post your problem in this community so we can help you out.



1. Why do you think your motor was bad?

2. What motor was in your machine. Post picture of label.

3. What motor did you put in your machine. Post picture of label.

4. Did you replace the pulley or use the old one?



Community:

...Has anyone replaced a stock stepper?

...If so with what?

...Any help on Andy's problem?

 









 





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/bq1JS8c3SgP) &mdash; content and formatting may not be reliable*
