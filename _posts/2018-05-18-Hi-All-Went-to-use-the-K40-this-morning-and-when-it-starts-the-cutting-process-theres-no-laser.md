---
layout: post
title: "Hi All Went to use the K40 this morning and when it starts the cutting process, there's no laser"
date: May 18, 2018 08:36
category: "Discussion"
author: "Frank Farrant"
---
Hi All



Went to use the K40 this morning and when it starts the cutting process, there's no laser.   Re-started K40 and PC - still nothing.  Was working fine last night.



If I press the Test button on the Laser - nothing, but if I press the Test button on the Power Supply it fires fine.  



Really appreciate any urgent help to fix. 

Thanks

Frank





**"Frank Farrant"**

---
---
**Joe Alexander** *May 18, 2018 11:57*

did you verify that the interlock loop is connected? this is the wires going to the laser enable toggle button. You can always just jumper it to ground to test(WP to G).


---
**Frank Farrant** *May 18, 2018 13:21*

**+Joe Alexander** 

Hi Joe, thanks for the reply. 

Here's a couple of pictures. 

I can call u if your in the UK if that's ok ?


---
**Frank Farrant** *May 18, 2018 13:26*





![images/e1bfc28b537752e459c0617dcde5344e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1bfc28b537752e459c0617dcde5344e.jpeg)


---
**Joe Alexander** *May 18, 2018 14:15*

im in the US, sorry. but I believe the one you want to jumper is P and G(the black and red wires that look damaged in the photo above. Those should go to the front panel toggle button for laser on/off. essentially the laser PSU will not fire unless P is connected to ground AND L is toggled by your controller( or k- to k+ from pressing the test fire button) Make sense?


---
**Frank Farrant** *May 18, 2018 14:48*

**+Joe Alexander** Hi. I had a problem a few months ago and needed a new power supply. China tech guy asked me to connect the red and black using a small piece of wire to test the old supply. Hence they look as they do. 

Are you saying I need to do that now for it to work ?

Just wondering what might have failed elsewhere for me to have to do it ?  Was working ok up to yesterday. 

Happy to do it if you can just confirm. 

Thanks 


---
**Joe Alexander** *May 18, 2018 15:40*

no you shouldn't have to jumper those 2 wires to make it work, but it would eliminate them and the button from the loop to ensure that the problem doesn't exist within that area. other than that I would try changing the water(using distilled water only) as it can become more conductive over time. if those don't work then I would guess its an issue within the laser PSU itself.


---
**Frank Farrant** *May 18, 2018 16:28*

**+Joe Alexander** Hi and thanks

I've joined them and it works fine. 

Is it ok for me to use for now ?

What should I test next ?




---
**Joe Alexander** *May 18, 2018 18:32*

if its working as it should with the wires jumpered then I would replace those damaged wires and re-check.


---
**Frank Farrant** *May 18, 2018 20:42*

**+Joe Alexander** I guess you mean replace them and don't jump them ? And see if it works ?


---
**Joe Alexander** *May 18, 2018 20:53*

yep




---
**Don Kleinschnitz Jr.** *May 20, 2018 11:13*

**+Frank Farrant** Does your machine have a water flow sensor?

Trace the black and red wire to see if it goes to a flow sensor or the panel. The flow sensor should be in series with this circuit.

Some newer machines come with a flow sensor that are notorious for "opening".


---
**Frank Farrant** *May 20, 2018 22:58*

**+Don Kleinschnitz** Hi Don

I had a message from China tech department saying I need a new water protector - they sent me the picture attached. 

I have s new one coming over but do you know if I can buy anything local in the UK ?

Thanks

![images/2da99bb9a2bc9160a92c2c347276a84f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2da99bb9a2bc9160a92c2c347276a84f.png)


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/bUX7XQMMb4F) &mdash; content and formatting may not be reliable*
