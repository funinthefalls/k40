---
layout: post
title: "So I know this has been discussed previously but wondering if anyone has thoughts or ideas on this"
date: June 06, 2017 15:43
category: "Discussion"
author: "Nigel Conroy"
---
So I know this has been discussed previously but wondering if anyone has thoughts or ideas on this.



When I go to use Plywood I find that the material is usually not flat and obviously that will cause problems with the focal point.



I purchased some approx 8mm plywood yesterday and found that when I cut the sheet to sections 20"x20" they had a warp in them. 



The solution to store them flat with a weight on top is a great one and I'll look to get some material stored like this for future use but that doesn't solve the situation when you need it straight away.



I have used spring clamps to hold down the pieces to the frame and the table but I have to sacrifice cutting area for that.



I recall seeing somewhere that someone hand some "hooks" the used to go over the material and either through a honeycomb table or a blade bed to clamp it in place. 

Does anyone else do this or have seen this? or any other wonderful suggestions?





**"Nigel Conroy"**

---
---
**Andy Shilling** *June 06, 2017 16:08*

I've used the wife's steam iron on thinner ply to soften the fibres and that helps but I'm not sure on anything thicker than 3mm


---
**Stephane Buisson** *June 06, 2017 16:21*

I recall magnets (but wasn't for thick wood).

but yes combination of wet+ weight when you have time is perfect. prepare your stock in advance.


---
**Joe Alexander** *June 06, 2017 16:23*

maybe cut some aluminum U rails to "clip" onto the edges to help keep it straight? like this:

[mcmaster.com - McMaster-Carr](https://www.mcmaster.com/#9001k12/=17yawr5)



cheap enough for a length to cut into a top and bottom guide. 


---
**Nigel Conroy** *June 06, 2017 21:59*

I was thinking something like this




{% include youtubePlayer.html id="0OB9Phc7B4U" %}
[https://youtu.be/0OB9Phc7B4U](https://youtu.be/0OB9Phc7B4U)



Or similar idea


---
**Nigel Conroy** *June 06, 2017 21:59*

I was thinking something like this




{% include youtubePlayer.html id="0OB9Phc7B4U" %}
[https://youtu.be/0OB9Phc7B4U](https://youtu.be/0OB9Phc7B4U)



Or similar idea


---
**Nigel Conroy** *June 06, 2017 21:59*

I was thinking something like this




{% include youtubePlayer.html id="0OB9Phc7B4U" %}
[https://youtu.be/0OB9Phc7B4U](https://youtu.be/0OB9Phc7B4U)



Or similar idea


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 07, 2017 13:29*

Interesting idea. I usually flip the ply (3mm, never worked with thicker yet) so that the bow is facing down then magnet my corners/edges down (galvanised steel cutting table & neodymium magnets). Then the centre doesn't poke up like in that video **+Nigel Conroy**. You could use the same method he does with the little clips on the edges for thicker wood (since magnets may not work so well on your thickness or you may not have magnetic cutting table), just face the bow down if possible.


---
**Nigel Conroy** *June 07, 2017 18:09*

Blades aren't magnetic but honeycomb is. Thicker wood will need the clips I think. 

Good point about the thinner wood


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/fCCuwoFZHi1) &mdash; content and formatting may not be reliable*
