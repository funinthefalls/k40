---
layout: post
title: "HalfNormal I could not comment on your post for some reason"
date: February 07, 2016 05:15
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
**+HalfNormal** I could not comment on your post for some reason. The danger of replacing with a 2K pot instead of the existing 1k pot is that you would be only able to drive the laser at 1/2 power in mA (Basic Ohms law)





**"Anthony Bolgar"**

---
---
**HalfNormal** *February 07, 2016 06:31*

Sorry. Comments were accidentally turned off. Not sure if it is actually Ohm's law at play. Have not been able to find accurate description of the control circuit of the power supply.


---
**Anthony Bolgar** *February 07, 2016 07:09*

Here is the schematic for the power control:



[http://www.buildlog.net/cnc_laser/images/manual_control.png](http://www.buildlog.net/cnc_laser/images/manual_control.png)



It shows the power adjustment pot as 5k, not 1k


---
**Anthony Bolgar** *February 07, 2016 08:04*

Wow, rookie mistake, but what can I expect for being up since Thursday at 7:30am (About 68 hours awake now with about 3 10 minute cap naps. Sorry for the mis-information.


---
**Sean Cherven** *February 07, 2016 12:06*

The only thought I'd watch out for is the wattage of the pot. 

I don't know how many watts is required, however.


---
**Sean Cherven** *February 07, 2016 12:08*

Okay, good to know!


---
**Scott Thorne** *February 07, 2016 13:01*

You guys are damn smart I feel stupid reading some of the electrical things....Lol...that's why I didn't bother trying to upgrade the k40....I bought it for 366...sold it for 500...bought the 50 watt gtsun engraver with dsp and a 20x12 work space...I don't think I could have completed the upgrade.


---
**Scott Thorne** *February 07, 2016 13:05*

**+Peter van der Walt**​...so right you are...I live in the U.S and I had to register my phantom 3 with the faa....talk about stupid...we have no rights here anymore...I do enjoy reading your post and admire the work you do Peter....I have two blue laser diodes in host already with drivers for sale...one is a 6.8 watt and the other is a 2.2..

Both work wonderful.


---
**Scott Thorne** *February 07, 2016 13:11*

**+Peter van der Walt**​...always...I'm a maintenance supervisor for a packaging firm....I'm all about saftey!


---
**Sean Cherven** *February 07, 2016 13:12*

I live in the U.S. also, and I too had to register my Blade 350 QX3 with the FAA. Atleast they offered the registration for free for the first few months.



**+Peter van der Walt** is the best! I can't wait until his drop-in replacement controller board is completed. I wanna get my hands on it and get started with the upgrade! I'm still running stock lol.


---
**Scott Thorne** *February 07, 2016 13:16*

**+Sean Cherven**​.....I have to be honest man....I loved the way my k40 performed stock....I just wanted more cutting and engraving room and the motorized up down table...I got the 50 watt machine for 1800.00 on Amazon and its amazing man...cuts through 1/2 acrylic at 60 % power at a speed of 6 mm/s....where are you in the U.S?


---
**Sean Cherven** *February 07, 2016 13:22*

I live in South Carolina, and I don't really have many complaints on the stock K40 either, other than trying to use existing designs, such as DXF files is nearly impossible.



I am fully capable of upgrading the K40 to any controller board, I just don't feel like cutting and splicing the wires inside the unit. That's why I want a direct drop-in replacement before I do my upgrade.



**+Peter van der Walt** Do you have a link to a specific place or post that you keep updated about the status of the controller board that your working on?


---
**Scott Thorne** *February 07, 2016 13:24*

I can't say I blame you one bit....**+Sean Cherven**...good luck with the upgrades!


---
**Scott Thorne** *February 07, 2016 13:25*

I'm in Atlanta...cold today....Lol


---
**Scott Thorne** *February 07, 2016 13:26*

**+Sean Cherven**...do you use inkscape ?


---
**Sean Cherven** *February 07, 2016 13:38*

**+Scott Thorne** I have not taken the time to learn inkscape yet. Been so busy with everything else going on. And I'm in Iva, SC.  Atlanta, GA is only a couple hours away.


---
**HalfNormal** *February 07, 2016 17:02*

Thanks to everyone who helped to clarify how the pot operated with the powersupply. A big shout out to all the southern living laser users! I lived in Atlanta GA for 18 years. Loved the south. Now back to AZ living.


---
**Scott Thorne** *February 07, 2016 18:18*

Lol...I go to az all the time for the company I work for....I was in Chandler a few months ago....normally I'm in Phoenix.


---
**HalfNormal** *February 07, 2016 19:16*

Living in the great white north of AZ, Flagstaff. A bit cooler in the winter and a lot cooler in the summer!


---
**Scott Thorne** *February 07, 2016 20:07*

I heard that....I wanna go to Sedona and Flagstaff next time I'm there.


---
**Scott Thorne** *February 07, 2016 20:09*

**+Sean Cherven**...you should use inkscape....it makes importing dfx files a breeze...it's free and easy to use...easy as hell to design boxes and such with it.


---
**Sean Cherven** *February 08, 2016 18:13*

I'll have to give it a try someday, but right now I'm too busy with everything going on. It's a busy busy life, ya know? lol


---
**Scott Thorne** *February 08, 2016 22:49*

**+Sean Cherven**...yup I know all too well brother....you need anything let me know Sean.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/PG7D4YkZrDx) &mdash; content and formatting may not be reliable*
