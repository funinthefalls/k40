---
layout: post
title: "So a quick query on how to fire my laser using Mach 3 I have already built a cnc machine and use mach 3 to run it and the spindle, i have added my laser tube to my gantry along with the original K40 laser Power Supply and"
date: January 16, 2018 06:19
category: "Modification"
author: "Mark Charters"
---
So a quick query on how to fire my laser using Mach 3



I have already built a cnc machine and use mach 3 to run it and the spindle, i have added my laser tube to my gantry  along with the original K40 laser Power Supply and have all the wiring etc setup and running. Laser can test fire using manual test button so that side of it works.



Was originally going to use mini cohesion 3d board but that would complicate things a bit to much having to rewire motors etc so just using my current board.



Now trying to work out how to get mach 3 to send the fire command to the laser using my current breakout board. the board is powered via usb and thinking of using the relay to send the fire command. So thinking 5v power from the power supply into Relay B then connect the laser fire line to Relay A.



The trick then is making Mach 3 close the relay for cutting operations any thoughts pin diagrams are in the pictures. thanks in advance for the help.





![images/9ec6decd1fd7b03e3632d796d7c791fe.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9ec6decd1fd7b03e3632d796d7c791fe.png)



**"Mark Charters"**

---
---
**Paul Mott** *January 16, 2018 07:22*

Mach3 uses a unique laser switching command set (M11/M10) which only takes effect at the instant of an axis movement thereby eliminating unwanted burn spots at the start and end of engraved lines etc. These commands are explained in more detail here; [http://hobbycncart.com/publ/cikkek/mach3_temaju_cikkek/switching_a_laser_under_mach_control/8-1-0-29](http://hobbycncart.com/publ/cikkek/mach3_temaju_cikkek/switching_a_laser_under_mach_control/8-1-0-29)






---
**Gunnar Stefansson** *January 16, 2018 08:40*

Cool Mark... I use mach3 for my laser and I use the Z-axis direction pin to trigger the laser... so I basically only do 0.1 depthof cut so every time I want to cut the Axis goes into -0.1 changing the direction pin to CW (On) and when done goes up to 0.1 changing the direction pin to CCW (Off) which works great with no real noticable delay... 


---
**Mark Charters** *January 17, 2018 02:06*

**+Gunnar Stefansson** 



Hi Gunnar, I looked at that idea but as already have a Z axis that is operational sorts want to stay away from this option. ![images/df858280365692c77624b90aa93287db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df858280365692c77624b90aa93287db.jpeg)


---
**Mark Charters** *January 17, 2018 02:19*

**+Paul Mott** 



Hey Paul thanks for that I didn’t know I about the M11/M10 looks pretty awesome   So in Mach 3 would I configure this in ports and pins page and spindle tab. I see here it’s only talks about M3, M4, M7 & M8. 



And how would the Gcode look would the M10/M11 code just be at the start and then it would cut all the time or does it need to be at the start and finish of each bit just like the spindle cut info is?![images/fab599ccd06c1dd163f7504f685875e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fab599ccd06c1dd163f7504f685875e4.jpeg)


---
**Mark Charters** *January 17, 2018 03:07*

**+Paul Mott** 



Works a treat also found this link in theory ran a couple of lines and the spindle shows as going and stopping (which would be the laser firing?) 



[warp9td.com - FAQ M-Code](https://warp9td.com/index.php/faq/faq-m-code)



Only thing struggling with now is actually getting the laser to fire using the fire in line to the I have tested at the relay spot and and no Voltage change? Any thoughts on configuring Mach 3?


---
**Paul Mott** *January 17, 2018 07:38*

**+Mark Charters** 

Mounting that laser tube vertically spells disaster. During use a certain amount of debris is created (mainly from the metal electrodes). This debris then falls onto the output coupler, burns in, causes overheating and damage. It will work but the tube will have a very short lifetime indeed.



There is a sample Gcode file (called; Trispokedovetails(laser).tap) in the DemoGcode.zip folder here;

 [http://www.machsupport.com/forum/index.php/topic,18345.msg126486.html#msg126486](http://www.machsupport.com/forum/index.php/topic,18345.msg126486.html#msg126486)



Basically M11Px is used to turn the laser ON for feed-rate moves and M10Px is used to turn the laser OFF for rapid moves (where x is the Output# which is configured within Mach3 to be used as your laser trigger input). These M11/M10 commands are automatically added to the Gcode by a modified post-processor for the particular CAD/CAM software that you are using.



If you have plenty of reading time then Bro. Tweakie’s thread on the Artsoft forum may be of interest.

 

[machsupport.com - The Laser Project.](http://www.machsupport.com/forum/index.php/topic,12444.0.html)




---
**Mark Charters** *January 17, 2018 08:05*

Awesome thanks for the help Paul much appreciated 😋👍 will have a good read of tweaking thread looks like a lot of good info 


---
**Gunnar Stefansson** *January 17, 2018 09:57*

**+Mark Charters** Yeah, ok! :D if you have a z-axis, then scratch that :D But thanks for asking as **+Paul Mott** really has some good answers, I'll look into it myself! 



Oh and **+Paul Mott** thanks for mentioning the vertical tube issue! as that didn't cross my mind at all :)


---
**Vince Lee** *January 31, 2018 23:31*

I believe the fire input to the laser power supply is typically an optolator that is active low so you don't necessarily need a relay (but you can use an SSR just in case since it can react fast and takes almost no current).  I bet the output to  from the controller board is open when off and  tied to ground when on.  If so, all you need is a pull up resistor to get it to work.




---
*Imported from [Google+](https://plus.google.com/105802162101662753798/posts/ZW6KNqyqLrP) &mdash; content and formatting may not be reliable*
