---
layout: post
title: "Hey all great community here been some great stuff to read through at lot of it has saved me from many headaches"
date: October 22, 2016 22:52
category: "Original software and hardware issues"
author: "C\u00fario Design"
---
Hey all



great community here been some great stuff to read through at lot of it has saved me from many headaches. 



just wondered if anyone had had the issue or had any ideas whats going on, seems to be in the middle of my cutting area a bit of a low quality/ dead area, the top 1/3 and the bottom 1/3 are consistent just the middle 1/3 seems to produce weaker engravings and less reliable cuts. have done the 1 million sticky notes aligning and cleaned all the mirrors made sure the lens is correctly orientated, 





**"C\u00fario Design"**

---
---
**greg greene** *October 22, 2016 23:04*

Check the rails - perhaps one is a little bent and moves the head  so it tilts and the beam is hitting the inside of the head.


---
**Cúrio Design** *October 22, 2016 23:06*

there is nothing worse than spending a day trouble shooting to have someone give you an answer in less than 10 mins haha. cheers greg 




---
**Scott Marshall** *October 24, 2016 00:42*

If your material or bed is sagging in the center it can throw off the focus. I had mine actually being sucked in by the exhaust once (I have a very large exhaust system I use for other things as well and was experimenting to see what different flows did for smoke deposition) Those kind of problems can drive you crazy.



The K40 cabinet isn't usually square, straight or even, and the bottom can 'oilcan' despite the stiffener strips. What you set it on matters.



The easiest way to check the focus distance is to make a gauge block  then check the head to work distance at intervals across the work surface. If anything is throwing it off, that will reveal it.



Scott


---
*Imported from [Google+](https://plus.google.com/103772136542346189065/posts/9KAo4jHEJTr) &mdash; content and formatting may not be reliable*
