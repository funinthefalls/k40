---
layout: post
title: "Hey all, Just like to pick peoples brains"
date: December 05, 2015 22:46
category: "Discussion"
author: "I Laser"
---
Hey all,



Just like to pick peoples brains. I've noticed a shift in the zero point on my k40. I assume this has been caused by the belt skipping. A couple of times the machine has 'spazzed' out when turned on.



Now I'd like to reset the zero position, but in corel laser you can't set the initial start point to a negative number which is what I now require. So is the only way to remedy this to physically skip the belt?



Cheers





**"I Laser"**

---
---
**Scott Thorne** *December 06, 2015 04:45*

Your zero should come from the small limit switches that home the carriage I would think.


---
**I Laser** *December 06, 2015 10:15*

To be honest I don't think it has limit switches and that's the reason it has on occasion banged against the end when turned on.



Corel moves it further in again when it initialises, I would like to change this home position if possible but just can't find useful settings.


---
**Scott Thorne** *December 06, 2015 12:54*

It has two small limit switches they are located under the carriage at the upper left, they tell the it the home position...check them and see it they might be obstructed.


---
**I Laser** *December 06, 2015 21:58*

Ah thanks, I was looking for standard limit switches and I just found what seems to be an optical switch on the y, couldn't see the x but will have a better look later.



The machine homes okay, although on occasion when first powered on it tries to push the head beyond the upper limits and requires powering down. I'm thinking the shift in home position is more likely due to me bolting the gantry further down (had it out to cut the exhaust shroud).



So with that in mind it seems there's no way to set the home position as corellaser won't let me set it to a negative number. :(


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/RFU6ZiTpRLw) &mdash; content and formatting may not be reliable*
