---
layout: post
title: "I need the following: Optics mirrors and their mounts, a PSU and 40 watt tube and tube mounts"
date: January 18, 2017 11:10
category: "Discussion"
author: "laurence champagne"
---
I need the following: Optics mirrors and their mounts, a PSU and 40 watt tube and tube mounts. 



I was wondering if there was anywhere I could get these things cheaper than just buying a whole unit on ebay. The prices are back down to around $350 now.



If you add in the resale value of the M2 nano, dongle,  and rails I think it might be cheapest to just purchase the whole unit right??



I already have a k40 that I bought for around $320, and am more than happy with my purchase and have gotten way more than $320 use out of the thing. So their "quality" issues aren't a concern.





**"laurence champagne"**

---


---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/Ckr9R2K1gL9) &mdash; content and formatting may not be reliable*
