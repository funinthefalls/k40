---
layout: post
title: "I've done several tests. I've done different dpi and settings but none of them seem to work"
date: February 17, 2016 16:47
category: "Discussion"
author: "Patryk Hebel"
---
I've done several tests. I've done different dpi and settings but none of them seem to work. I still get blanks in top and middle of the engraved photo. Any suggestions?





![images/6031de9c8e99cc6a12aaf96a8800cf92.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6031de9c8e99cc6a12aaf96a8800cf92.jpeg)



**"Patryk Hebel"**

---
---
**Stephane Buisson** *February 17, 2016 17:07*

Just suppositions based on laser technology:

- problem with PSU (inconsistant mA)

(how much did you use? near limit ?)

- problem with laser gas (too hot, faulty tube)

Maybe a clue in repeatability 3X under Dog's nose.


---
**Patryk Hebel** *February 17, 2016 17:14*

**+Stephane Buisson** well it was at 10 mA. 


---
**Patryk Hebel** *February 17, 2016 17:28*

**+Stephane Buisson** Well machine doesn't even cut now. It took me 2 rounds to cut 3mm mdf. Now it doesn't even cut at 4 rounds.


---
**Stephane Buisson** *February 17, 2016 17:55*

if you dismiss temp issue, I would think of PSU first for inconsistant current (hight voltage -> mA) maybe faulty capacitor (trying to find you an idea matching with repeatability). just my 2cts.


---
**Scott Marshall** *February 17, 2016 19:07*

To check if the laser is putting out, put a piece of tape or paper between the front of the laser tube the 1st mirror and hit the test button. If it burns a hole, it's probably optics, if not, it's the tube ,PS etc like Stephanie suggested.



If you're losing power along with the erattic output, 1st thing to do is check optics. 



Unless the alignment is dead on perfect (which it almost never is), the laser sweeps across the mirrors/lens as the head moves. Dirt or a burned spot on a mirror/lens can cause a weak zone as the beam enters it. The dirt also progressively weakens the cutting power.



 The laser output mirror (recessed but the high voltage seems to attract smoke and dust), 2 mirrors exposed  - also need an occasional cleaning, and the head contains last mirror and lens. These get the worst of the dirt because they're right at the business end. You can clean these 2 elements in place, but it's tricky. You may want to unscrew the top and bottom halves of the head, this allow you to get the parts in your hands so you can clean and inspect well. If you do this, mark the top had where it meets the plate, and the plate with a scratch so you can put them back in the same orientation.



According to mirror/lens manufacturers (and it's how I do it) clean pure (no color or fragrence) solvent like MEK or Acetone on a cotton q-tip - wipe until clean, then wipe dry with a 2nd clean q-tip. If you run into baked/burned on dirt, don't be afraid to scrub with the cotton swab. Brief soaking can help, <5 minutes.

If you have a burned lens or mirror and no spares on hand, you can usually rotate the element to get the bad spot out of the active zone. Trial and error.



I use Brake Cleaner - DO NOT use Carb Cleaner, it contains oil. Wal-mart sells a {"Super Tech" non-chlorinated Brake Parts Cleaner} that is cheap, and works great.



While cleaning - INSPECT. If there are any burned spots, the element will need replacement (this happens if it gets dirty and you keep using it - the dirt burns, damaging the optic)



Once all optics are confirmed clean (careful not to move them) Re-assemble the head onto the plate, and align the marks made earlier.



Fire it up and power test.



All else being equal, your power should have returned.



If not, you have alignment or laser/power supply issues.



One last thing, I notice on mine, there's a range where the laser fires erratically, it works fine above and below (mine is around 3ma) - If that's the problem, it should go away or at least change if you go up or down on your setting.



Hope some of this helps



Scott



PS - This is a quick, not 100% reliable, but not bad test for dirty optics.



Run the cutter at full power for a minute or 2, 

then cut power, make safe the laser and quickly feel each mirror stand and the head, the dirty one will be hot. 


---
**Patryk Hebel** *February 18, 2016 00:38*

**+Scott Marshall** Thanks a lot. Will try that :)


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/fK2W3pJW3RQ) &mdash; content and formatting may not be reliable*
