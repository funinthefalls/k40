---
layout: post
title: "So, I have been away from laser work for some time now"
date: September 16, 2017 01:58
category: "Software"
author: "timb12957"
---
So, I have been away from laser work for some time now. Just getting back into the swing.  I have moved my software (Coreldraw12 and CorelLaser) from my Windows 7 laptop to my new Windows 10 desktop. I have encountered the following issue. Previously when I would load an object in CorelLaser and either cut or engrave it, after the laser finished cutting, the computer returned to CorelLaser and I could cut or engrave another of the loaded object. Now however, after the laser finishes, my computer goes to Coreldraw and the tool bar for cutting/engraving etc is not present. The only way to cut another of the same object is to shut down the software, and reload again. Anyone have a solution for this issue?





**"timb12957"**

---
---
**Anthony Bolgar** *September 16, 2017 02:07*

Check the system tray, a lot of times it is just minimized out of view.


---
**timb12957** *September 16, 2017 02:15*

Thanks for the suggestion, but nope not there. The Coreldraw screen still shows, but the tool bar for laser functions is closed.


---
**Printin Addiction** *September 16, 2017 02:58*

There is a great new tool by scorchworks called K40 Whisperer. You can raster engrave, vector engrave and vector cut all from one file.


---
**Phillip Conroy** *September 16, 2017 04:13*

With my k50 and coreldraw x7 I assigned a keyboard key to start the plugin to show cutting window,maybe you can do same


---
**Tadas Aš** *September 16, 2017 06:00*

**+Phillip Conroy** how do you do it? Sounds usefull


---
**timb12957** *September 16, 2017 13:35*

phillip, wouldn't that just be a quick way to restart each time? I would still have to reload my design then size and position it to cut a second piece.


---
**HalfNormal** *September 16, 2017 15:41*

A quick search for "tool bar" brings up all these answers.

[K40 LASER CUTTING ENGRAVING MACHINE](https://plus.google.com/communities/118113483589382049502/s/tool%20bar)


---
**Phillip Conroy** *September 16, 2017 18:30*

Open macro manager ,then find the laser plugin ,right click on it and assign key to stsrt


---
**timb12957** *September 16, 2017 20:12*

HalfNormal, thanks. I did find the icon and able to use it to cut multiple designs. But still do not understand why the tool bar started minimizing.


---
**HalfNormal** *September 16, 2017 20:17*

Just a quirk of the software. Something we have to put up with for using it.


---
**timb12957** *September 18, 2017 00:53*

On a plus side , wish all solutions were this simple! lol


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/U9wSs5FZhqB) &mdash; content and formatting may not be reliable*
