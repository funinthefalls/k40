---
layout: post
title: "All the pieces for my project are now laser cut out"
date: October 02, 2016 15:09
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
All the pieces for my project are now laser cut out. Thanks #LaserWeb3 :)



On a side note, I would liked to have made the whole thing bigger, but 2 limitations restricted that: K40 cutting area is only 330 x 230mm  (or 13 x 9 inch) currently & the Kangaroo hide was not big enough to do bigger. I could've used 2 hides, but it's already au$110 worth of leather in this piece, using about 90% of the hide (and leftover sections are not usable for anything larger than a keytag).



Cut Settings: 6mA power (on pot), 20mm/s speed, 100% power (in LW3).

Material: 1-1.5mm Natural Vegetable Tanned Kangaroo Hide/Leather



**+Ariel Yahni** Another note for cutting leather, watch for it curling due to the heat beneath it or something. I had a piece (in this pile) curl up significantly & the cut ended up about 2mm out of place.



Weight objects down, magnet them down, double-sided tape them down, vacuum bed them down, anything to hold them down :D Make sure to weight them inside the actual cut objects. For this piece that failed for me, I had the magnet outside the cut area & thus the piece that was cutting lifted. You may be able to see it in the photo, it's one of the ends of one of the strap pieces (at front of table on right, right corner).



I have video of the curling, but have to go through them & upload to youtube. I'll chuck them up later when I edit them & can be bothered.







![images/abe891b6df4e634957acec9722220c84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/abe891b6df4e634957acec9722220c84.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 15:12*

Oh, the top right corner piece, I decided to "test" cutting with the paper template over the top. Thought it might minimise soot stains on the top of the piece. Stupid idea haha. Turns out all it did was trap smoke under it & maximise soot stains haha.


---
**Alex Krause** *October 02, 2016 15:18*

Messenger bag?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 15:19*

**+Alex Krause** Not quite, but it is a bag. Have to wait & see for now. It's for a competition.


---
**Ariel Yahni (UniKpty)** *October 02, 2016 15:27*

This is becoming a tutorial.!!! When you dye does the burn mark go away? Or you need to do something before? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 16:10*

**+Ariel Yahni** It actually will be a tutorial, as part of the competition. So you can get full details from my write-up for that in a week from now. But, regarding the soot stain, it doesn't always get covered by the dye (depending on the colour/darkness of the dye). If I was dyeing black, it would be all good & I wouldn't bother cleaning it up, but since I am planning to use mostly a light Tan colour & some other light colours, it will definitely be visible unless I clean it up a bit. Not 100% sure what I am going to do to remove that yet, but will test on small scraps tomorrow & see what I can do to remove it easily without damaging the leather. Also, majority of those marks will be covered by rivets/buttons, so only some of the marks will be visible if they don't clean up well.


---
**Ariel Yahni (UniKpty)** *October 02, 2016 16:13*

I can't wait a week :). Thanks


---
**Alex Krause** *October 02, 2016 16:13*

**+Yuusuf Sallahuddin**​ give denatured alcohol a shot...in some parts of the world it's called methylated spirits... it works well with wood... everclear is another option I find they work better than isopropyl alcohol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 02, 2016 17:35*

**+Alex Krause** I've previously used Methylated Spirits before for cleaning up soot on leather, so I'll give it a shot on these pieces. One thing I've noted with it in the past is that it dries the leather out a lot, so conditioning is definitely in demand after it. Thanks for that :)


---
**Bob Damato** *October 05, 2016 09:11*

I use this stuff and its amazing. I dont even bother taping the leather to cover the cut marks, and it doesnt matter how bad the smoke marks get, this takes it off with ease.



[http://www.laserbits.com/leather-products/leather-finish/lus-013-leather-finish-4oz.html](http://www.laserbits.com/leather-products/leather-finish/lus-013-leather-finish-4oz.html)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 05, 2016 15:46*

**+Bob Damato** Thanks for sharing that Bob. I will have to look into that in future. I've found that **+Alex Krause**'s suggestion of denatured alcohol (methylated spirits) seems to do an average job removing the burn/soot marks. Does a much better job if I use it straight after cutting/engraving rather than leaving it overnight before I cleaned it up.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/YbjuSpyyziR) &mdash; content and formatting may not be reliable*
