---
layout: post
title: "I just got my K40 last week"
date: September 17, 2016 02:52
category: "Hardware and Laser settings"
author: "Steve Prior"
---
I just got my K40 last week.  I just tried engraving and cutting 3/32" Lexan tonight and where I was cutting a simple circle the plastic turned black and re-melted.  Can anyone suggest some initial settings to try for engraving and cutting 3/32" Lexan?





**"Steve Prior"**

---
---
**Ariel Yahni (UniKpty)** *September 17, 2016 03:27*

**+Steve Prior**​​ Lexan / polycarbonate is a no go on a laser cutter. ﻿At least that's my experience. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 03:36*

According to this article ([atxhackerspace.org - Laser Cutter Materials - ATXHackerspace](http://atxhackerspace.org/wiki/Laser_Cutter_Materials)) cutting Lexan produces lethal chlorine gas. Also corrodes the hell out of the machine (meaning your machine will end up damaged). Possibly best to avoid it.


---
**ThantiK** *September 17, 2016 03:45*

Noooooooooo lexan = bad!


---
**Scott Marshall** *September 17, 2016 05:32*

You want Acrylic. They are easily confused. Polycarbonate is gummy and soft if you cut the edge with a pocketknife,



Acrylic is brittle and hard. That's what works on a laser cutter, cuts like butter. You still want good ventilation, but it's much less nasty than Polycarbonate.



The down side to acrylic is the brittleness. Drill it with a very dull drill, and be careful breaking through, Or you can finish the hole running backwards. (you can make a nice acrylic drill bit by drilling concrete for a few seconds with an old, cheap bit.)



If you remember that hard and brittle are the way to tell, you can a find acrylic in the pile of scraps at a glass cutting center in the home center. (sometimes you can buy the whole can of drops for 10 bucks) (material hint, hope you don't raid my home center.....)


---
**Jim Hatch** *September 17, 2016 12:18*

**+Scott Marshall**​ I drill acrylic with a laser 😁



Tap Plastics is pretty common across the US and you can find acrylic there. You can also get it in various sizes and colors from Amazon. I use cast acrylic for my projects as it's more uniform and consistent than extruded acrylic but it's probably over engineering on my part - it's not like extruded acrylic is hugely inconsistent 🙂



BTW, it's almost never the size advertised if you're looking for 1/4, 1/8", etc. It'll be something smaller. That's a good thing to know if you are making things where you use a thickness setting for sizing like fingers on box joints.



The other thing is that if you're making boxes and things you might want to use "dog bones" - small circular holes - on cut corners. They relieve cracking stress.




---
*Imported from [Google+](https://plus.google.com/+StevePrior/posts/Nv22ajRmbDj) &mdash; content and formatting may not be reliable*
