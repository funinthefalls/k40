---
layout: post
title: "Anyone in here who changed the standard cooling system for a closed loop watercooling system using radiators or peltiers ?"
date: August 07, 2016 22:05
category: "Modification"
author: "Bart Libert"
---
Anyone in here who changed the standard cooling system for a closed loop watercooling system using radiators or peltiers ? If so, how good does it work for you and what do you exactly use ? I believe we should keep the water of the laser tube below 26°c (about 78°F) ?

![images/1c5ada4a577fe000fc2626ffec3b1ca7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c5ada4a577fe000fc2626ffec3b1ca7.jpeg)



**"Bart Libert"**

---
---
**Alex Hodge** *August 07, 2016 22:50*

Yep. Single 120mm radiator. Works great. Though, double would be fine too. Get a pump that can handle keep the flow rate up through a radiator.


---
**I Laser** *August 07, 2016 23:46*

If it's passive you're going to want to live in a area with a cool year round climate!



I briefly looked into peltier cooling last year, it became apparent that the inefficiencies were too large to make it worthwhile, so like others I've ended putting frozen bottles of water into my tank.


---
**Alex Hodge** *August 08, 2016 03:54*

I just wired in an AC 120mm fan to use with my radiator. Works a treat. I wouldn't run passive without a realllllly big radiator.


---
**Brook Drumm** *August 08, 2016 05:47*

You guys are nuts. And. I. Love. It.

-Brook 


---
**Bart Libert** *August 08, 2016 07:03*

So 1 120mm radiator and a good powerfull fan is enough to actively keep the temp ok? My home temp here never goes above 21°c so that is 5 degrees below that 26°c for the laser. I was thinking about 2 120mm radiators. So if 1 will do, 2 will be ok with the fans on a little lower speed. I have TEC peltiers around tol but these are indeed very inefficient


---
**Gunnar Stefansson** *August 08, 2016 10:44*

I've tried with 3 peltier modules with some waterblocks, but they couldn't keep up with the laser on full power more than 10min. Very inefficient, today I've got 2x 240rads like the one in your picture with these awesome fans! best fans I've ever had, great power, around 250cfm at 5500rpm and a super silent and powerful water pump from Delphi. Here are the Links: [http://www.ebay.de/itm/Delphi-Water-cooling-pump-ddc-1vc-52412270-dc12v-10w-E82642-IP32-VER-5-4-/371670311769?hash=item568946e359:g:UBkAAOSwEK9UBaoQ](http://www.ebay.de/itm/Delphi-Water-cooling-pump-ddc-1vc-52412270-dc12v-10w-E82642-IP32-VER-5-4-/371670311769?hash=item568946e359:g:UBkAAOSwEK9UBaoQ) and [http://www.aliexpress.com/item/Free-Shipping-Delta-3-9A-violent-fan-12CM-the-best-TFC1212DE-oversized-air-volume-four-wire/787435543.html?spm=2114.13010608.0.80.KuDLRW](http://www.aliexpress.com/item/Free-Shipping-Delta-3-9A-violent-fan-12CM-the-best-TFC1212DE-oversized-air-volume-four-wire/787435543.html?spm=2114.13010608.0.80.KuDLRW)






---
**Bart Libert** *August 08, 2016 11:43*

Gunnar, so if I look at these fans, then I immediately see something strange. 12V upto 4 AMPS. Wow that is huge power. This means 1 such a fan draws +- 50Watt power. A "regular" fan uses less than 10 watt. A regular fan is also only capable of around 50cfm . So you mean that you need 2 x 240 rads = 4 times a 120x120 rad + 4 times such a fan to cool your 40watt laser? It sure sounds very powerfull, but then again Alex Hodge above speaks about single 120mm radiator (no mention about fan power) and you directly go to 4 radiator power an 4 ultra fans. Can you tell me what they exactly do to your water temp ? I suppose in your setup you are capable of maintaining room temp in your water at all time, even after longer time of 100% laser usage ? After al your cooling is a real beast ;) . I am in the process of designing my own 40w laser cutter so I certainly have space for extra radiators if needed ;) . Would just be nice to know exactly how good these setups serve you. And 1 more (IMPORTANT) question. These fans have 4 terminals. How to you actually drive them ? 1 connector is ground, 1 is +12V, but what about the others (which are most probably for the pwm speedcontrol) ?




---
**Gunnar Stefansson** *August 08, 2016 14:21*

**+Bart Libert** Yes with 4 of those fans I'm more than capable of maintaining room temp :) I have a Water temp probe and I just turn up the fan speed according to water temperature. They are quite loud but this was my third attemp at water cooling my 50w tube, so I was tired of trying so I just whent with Definetly enough :D I have an arduino in my system where I use a PWM output to set the speed of all 4 fans. I believe the 4th wire is for rpm/min but I don't need it. Typically my fans run at around 20-40% power but it is warmer at this time in Denmark, during winter they run pretty quiet. 

I am thinking on returning with some form of peltier cooling with some more efficient peltiers but after the water is already at room temp from the cooling right now, just as an experiment ofc. :)


---
**Bart Libert** *August 08, 2016 14:28*

OK, so can you please tell me how exactly you connect them to your arduino ? I know aout PWM, so you do connect red to PSU 12V, black to ground os PSU and also GND on arduino and then.....  I see a blue and yellow wire too. Which one do you use and do you just connect this one to a pwm enabled arduino pin ? Any idea at what frequency you run this pwm pin ? Very interested.




---
**Alex Hodge** *August 08, 2016 14:45*

I have a 1 gallon reservoir, a 396GPH pump and 110CFM fan. My radiator is an old Danger Den I believe. It's been in a box in my closet a while...Honestly though, when it comes to water cooling, better to overspec than underspec. Also keep in mind that different radiators will have different number of rows, thicknesses, flow restriction, surface area, etc. even when they are both technically "120mm". Bigger is better. Fans for radiator use should have more fins. Generally that means higher static pressure. You want high static pressure and high CFM. Generally noise isn't a huge issue but I've always found Delta "screamers" to be a bit shrill. Though they are always great performance fans and last forever. YMMV.


---
**Alex Hodge** *August 08, 2016 14:50*

**+Bart Libert** **+Gunnar Stefansson** If you want to use DC fans like Gunnar, a fan controller is really cheap and much easier to setup than using an arduino just for fan control. Something simple like this:

[https://smile.amazon.com/Zalman-Fan-Speed-Controller-FANMATE-2/dp/B000292DO0/](https://smile.amazon.com/Zalman-Fan-Speed-Controller-FANMATE-2/dp/B000292DO0/)


---
**Bart Libert** *August 08, 2016 14:55*

The sound is not the biggest issue. I currently operate a 80w laser with an industrial chiller, but for the 40W I would like to go a little more compact ;) . Also I would prefer to use an arduino since my whole machine will be powered by 1 raspberry pi and 2 or 3 arduino's all coöperating ;)




---
**Alex Hodge** *August 08, 2016 15:01*

**+Bart Libert** Sounds fun. I'm looking forward to some pictures when you're done!


---
**Gunnar Stefansson** *August 08, 2016 21:06*

**+Bart Libert** I use an Arduino Mega 2560 R3 and by default the PWM outputs are at 490Hz, I just use one PWM output to control the speed of all 4 Fans and that works great. **+Alex Hodge** yes thats a very simple controller indeed, I have a 5" touchscreen attached that display's room temp, water temp with graph etc with some basic controls aswell and I automatically speed up the Fans depending on a water temperature probe, or else I would have done something like a normal pc fan controller :)  **+Bart Libert** if you're already have 2 or 3 arduino's in your system you could do that aswell :) I'm looking forward to seeing what you come up with in your build, especially your design as I've also built and designed my own ;)


---
**Bart Libert** *August 10, 2016 18:09*

**+Gunnar Stefansson** Can you tell me if you used the aliexpress version of the fan or a REAL Delta fan ? At the prices they show on aliexpress, these fans are clones of the original make. The "real" ones cost $30 and up.


---
**Gunnar Stefansson** *August 10, 2016 18:26*

**+Bart Libert** I bought them from Aliexpress, I bought 5 for $8.93 a peace, And all of the fans work and are of high quality, they have alot of weight to them, so can't say they feel cheap. It took along time to arrive, like 4~6weeks can't remember exactly... They have put up the prices since I can see, but normal Price of $30 !!! I didn't know that, then I'd also be asking the same question you're asking right now :) I bought them September last year! I'm at the Workshop tomorrow I could make a small video of the Fans, I have a spare one so you can see allround how it looks and sounds, if that would help ;)


---
**Bart Libert** *August 10, 2016 19:21*

That would be very nice. They will almost certainly be clones then, but a good clone doesn't need to be bad of course. I allready discovered that they need (or better they prefer) a around 25kHz pwm signal . All very possible with the simplest arduino.




---
**Gunnar Stefansson** *August 10, 2016 20:04*

Ok! 25kHz, then there's quite a way to go, from what I'm using... :) yeah I think you are right with the good clone part... Didn't know about that, I just knew from the moment I got them and tested them, that I was in love... :D I'll upload a video friday ;)


---
**ViciousViper79** *September 06, 2016 21:51*

Had some spare radiators lying around that I won't use for my PCs anymore. Stuffed them together and it works passive and pretty well. I am thinking about putting an additional PSU in place to run some vents for better cooling. So far the watter stays cool after a couple of cutting sessions.


---
**Bart Libert** *September 06, 2016 21:57*

How many and what size radiators did you hookup and on what wattage of laser ?




---
**ViciousViper79** *September 06, 2016 22:40*

I have 1x240, 1x80 radiators attached. Also some pad to mount a peltier element and cool it with Corsair H80 but that is not yet operational and maybe never will due to the fact that cooling with radiators and convection seems to be enough. I got about 6L of water in a plastic tank with the pump so plenty of additional heat capacity. The laser I have is std. K40 with 40W. So I guess it is less than that. I want to mount 2x120 Vents and 1x80 Vent for the radiators to get some more performance if needed later.



Here is a picture of the setup:

[https://www.facebook.com/viciousviper79/photos/a.764785466886693.1073741828.756018644430042/1254885724543329/?type=3&theater](https://www.facebook.com/viciousviper79/photos/a.764785466886693.1073741828.756018644430042/1254885724543329/?type=3&theater)


---
*Imported from [Google+](https://plus.google.com/104850277500909359562/posts/KECa8ciCvDz) &mdash; content and formatting may not be reliable*
