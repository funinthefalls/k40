---
layout: post
title: "I was wondering about this in the GRBL 1.1f congig file"
date: February 02, 2019 18:13
category: "Modification"
author: "Custom Creations"
---
I was wondering about this in the GRBL 1.1f congig file. Since the K40 fires on low and off on high, is this what to use instead of having a logic switching circuit? I am using the Protoneer CNC Arduino shield.



// Inverts the spindle enable pin from low-disabled/high-enabled to low-enabled/high-disabled. Useful

// for some pre-built electronic boards.

// NOTE: If VARIABLE_SPINDLE is enabled(default), this option has no effect as the PWM output and

// spindle enable are combined to one pin. If you need both this option and spindle speed PWM,

// uncomment the config option USE_SPINDLE_DIR_AS_ENABLE_PIN below.

// #define INVERT_SPINDLE_ENABLE_PIN // Default disabled. Uncomment to enable.



// By default on a 328p(Uno), Grbl combines the variable spindle PWM and the enable into one pin to help

// preserve I/O pins. For certain setups, these may need to be separate pins. This configure option uses

// the spindle direction pin(D13) as a separate spindle enable pin along with spindle speed PWM on pin D11.

// NOTE: This configure option only works with VARIABLE_SPINDLE enabled and a 328p processor (Uno).

// NOTE: Without a direction pin, M4 will not have a pin output to indicate a difference with M3. 

// NOTE: BEWARE! The Arduino bootloader toggles the D13 pin when it powers up. If you flash Grbl with

// a programmer (you can use a spare Arduino as "Arduino as ISP". Search the web on how to wire this.),

// this D13 LED toggling should go away. We haven't tested this though. Please report how it goes!

// #define USE_SPINDLE_DIR_AS_ENABLE_PIN // Default disabled. Uncomment to enable.







**"Custom Creations"**

---


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/dUQewzeU1JM) &mdash; content and formatting may not be reliable*
