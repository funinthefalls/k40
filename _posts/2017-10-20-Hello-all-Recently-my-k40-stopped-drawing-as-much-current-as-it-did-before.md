---
layout: post
title: "Hello all, Recently my k40 stopped drawing as much current as it did before"
date: October 20, 2017 23:40
category: "Original software and hardware issues"
author: "Alex Raguini"
---
Hello all,



Recently my k40 stopped drawing as much current as it did before.   I have the digital percentage control board and I added a current meter.  At 85% it used to draw 20ma.  Then, my machine dropped to 10ma max. I thought it was the power supply so I replaced it.  With the new power supply installed, max current draw is 5ma.  What's going on?



I bridged the 5v to the laser control in and it draws 5ma max.  I thought it was the power connection to the tube so I removed the wire and cleaned it.  I soldered it to the tube to ensure a good connection and re-insulated the connection. It still only draws 5ma.  I tried a dvm and got the same reading. 



Did I get a bad pws?  Is my tube going bad?  The tube is less than. 30 days old. Or, is there something I'm missing?  Suggestons?





**"Alex Raguini"**

---
---
**Don Kleinschnitz Jr.** *October 21, 2017 00:00*

What kind of coolant are you using? 

What is temp of coolant?


---
**Alex Raguini** *October 21, 2017 00:07*

The temp is 22c. I'm using distilled water with a little chlorox to keep it clear. 


---
**Arjen Jonkhart** *October 21, 2017 01:44*

Possibly the same problem RDWorks ran into? Poor quality laser. Here are some youtube vids of him testing it.


{% include youtubePlayer.html id="JfRpa9Y5Woc" %}
[https://www.youtube.com/watch?v=JfRpa9Y5Woc](https://www.youtube.com/watch?v=JfRpa9Y5Woc)


{% include youtubePlayer.html id="DJBRZ2J0Q9A" %}
[https://www.youtube.com/watch?v=DJBRZ2J0Q9A](https://www.youtube.com/watch?v=DJBRZ2J0Q9A)




---
**Alex Raguini** *October 21, 2017 01:51*

I’m going to go back to the old power supply and see if it goes back to 10ma. I’m also going to change my water. 



If that doesn’t work, what’s a good 4ow laser tube?


---
**Alex Raguini** *October 21, 2017 02:20*

Thanks. I’ll check out the videos. I’ve seen some of his already. 


---
**Arjen Jonkhart** *October 21, 2017 03:47*

Why not upgrade to the 50W while you are at it? ;)


---
**Art Fenerty (ArtF)** *November 05, 2017 12:16*

Sounds like the gas in the tube is depleted. Not shooting for long periods, or running at max for too long without a rest can do that. Replacing tube is usually the fix. The input is PWM for power, so bridging it to 5 volts makes it shoot max..in our case 10ma. Your new power supply may have slightly lower voltage out, or not matched to your tube which can affect end current. 


---
**John Warren** *November 25, 2017 03:51*

I am running into a similar issue... Did you ever pin down the exact cause?


---
**Alex Raguini** *November 25, 2017 16:06*

yes, my ammeter went bad and was giving me false information.  I also suspect the digital control board that controls the 5v pwm signal wasn't reliable either.  I've replaced the ammeter with a combo Volt Meter/Milliampmeter and the digital control with a simple high resolution pot.  I now have much greater control of my laser power for cutting and engraving.




---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/7ACRmsyAWLn) &mdash; content and formatting may not be reliable*
