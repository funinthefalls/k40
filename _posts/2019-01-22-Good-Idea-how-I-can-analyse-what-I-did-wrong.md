---
layout: post
title: "Good Idea how I can analyse what I did wrong?"
date: January 22, 2019 21:13
category: "Discussion"
author: "BEN 3D"
---
Good Idea how I can analyse what I did wrong? This is the backside of the cut. Wood 4mm. 15mA 40k laser.

Top left is not cut, button right is cut well. 

Do I need to move up the left side of the wood? Or less down? In other words, is the Laser Last mirror to far away or the opposit ?

![images/e9848af365315bda4a2dc2b074bf80d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9848af365315bda4a2dc2b074bf80d2.jpeg)



**"BEN 3D"**

---
---
**'Akai' Coit** *January 23, 2019 02:23*

Mirrors could be out of alignment, wood could be warped and needs to be held down in certain spots. I'd start with ensuring the mirrors are aligned first and go from there.


---
**James Rivera** *January 23, 2019 06:50*

^ what he said. Or maybe your bed is not flat w/r/t the laser gantry?


---
**Stephane Buisson** *January 23, 2019 09:03*

Check if the laser path output is vertical (in the head) and doesn't bounce aside. (look like some offset lines on your pieces)


---
**HalfNormal** *January 23, 2019 12:53*

The culprit could be the plywood itself. When you see areas that look like they have been skipped and other that look normal, it could be the glue or other issues with the wood itself.


---
**Sebastian Szafran** *January 23, 2019 18:37*

I would bet for bed levelling, this was my issue when I faced same cutting results.


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/1kKwwgfU2az) &mdash; content and formatting may not be reliable*
