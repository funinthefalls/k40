---
layout: post
title: "I have tried to do the mirror alignment multiple times and I always run into the problem that my bottom right and button left never align"
date: September 27, 2017 17:03
category: "Hardware and Laser settings"
author: "Frank Jones"
---
I have tried to do the mirror alignment multiple times and I always run into the problem that my bottom right and button left never align. I can get one or the other to center and then I get to the end and try to make the necessary adjustments and then it starts all over again. Anyone with any ideas frustrated.....thank in advance.



















[https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)





**"Frank Jones"**

---
---
**Jim Hatch** *September 27, 2017 18:25*

Odds are that the frame the gantries ride on are not level and square. I had to remove them from my machine and tweak them outside the box to get them square before I was successful myself. My front right corner was off of square by about 1/4". You can check it with a square before you pull it out. That then leaves the gantry not parallel to the front rail (likely also the laser tube as well). 




---
**Frank Jones** *September 27, 2017 20:29*

I will try pulling out that whole section out next. I know this might sound stupid, but how did you check it and more importantly how did you tweak it to make it square? Maybe it will be easier to see after I remove it. 


---
**Jim Hatch** *September 27, 2017 20:54*

I used a small builder's square (an aluminum triangle). Place it in the corners and see if the two sides of the square contact the rails. It's pretty quick to see when they're not. Anything with known good 90 degree corners would suffice to check.



There are a couple of screws that allow you to adjust the right side rail but they're only accessible when the rails are out of the machine. Ditto if I recall on the horizontal ones. Once all 4 sides are squared up the gantry is easy to check to see that it's parallel to both the front & back rails. You can adjust the screws holding it on the rails to pull one end in or out as needed.


---
**Ned Hill** *September 28, 2017 01:39*

Here's another alignment guide.  [plus.google.com - - Laser Alignment Guide - With all the laser alignment questions we get, here...](https://plus.google.com/u/0/108257646900674223133/posts/LGfT6SS3Tcc)


---
**HP Persson** *September 28, 2017 12:56*

I wrote the guide you linked to, check out the new green text in the article change just a day or two ago :)


---
**Mark Kirkwold** *September 30, 2017 03:19*

Keep in mind that the center of the mirror is not necessarily the right place to be. The mirrors can only change the direction of the beam, and that change must be to make the beam path parallel to the axis of movement. What needs to happen is that the beam hits the same part of the target mirror  no matter where the target mirror is in its travel. 


---
**Rob Watson** *October 08, 2017 21:03*

I can confirm that following HP Perssons instructions works a treat




---
**Frank Jones** *October 08, 2017 21:28*

Thanks all I finally got it. What a pain. Has anyone here bought an 80 or 100 watt Chinese laser cutter? I have not looked into if they have the same problems or not. Thanks to everyone that had some input.


---
*Imported from [Google+](https://plus.google.com/112243468105675917143/posts/XWYqMnri4hw) &mdash; content and formatting may not be reliable*
