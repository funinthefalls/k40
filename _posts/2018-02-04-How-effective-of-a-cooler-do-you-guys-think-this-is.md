---
layout: post
title: "How effective of a cooler do you guys think this is?"
date: February 04, 2018 13:45
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
How effective of a cooler do you guys think this is?



[https://www.banggood.com/DIY-XD-2333-12V-15_4V-30A-360W-Three-Chips-Semiconductor-Refrigerator-Water-Cooling-Equipment-Kit-p-1086641.html?rmmds=search&cur_warehouse=CN](https://www.banggood.com/DIY-XD-2333-12V-15_4V-30A-360W-Three-Chips-Semiconductor-Refrigerator-Water-Cooling-Equipment-Kit-p-1086641.html?rmmds=search&cur_warehouse=CN)





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *February 04, 2018 15:09*

**+Steve Clark** I think you build one right? How does this compare to what you have?



Whoa 30 amps!


---
**Anthony Bolgar** *February 04, 2018 15:13*

360W unit , I think that is what the CW-5000 's run at about.


---
**Anthony Bolgar** *February 04, 2018 15:14*

**+Don Kleinschnitz** Did you get your Nextion board?




---
**Don Kleinschnitz Jr.** *February 04, 2018 17:56*

Yes but I got the wrong one (no gpio)..:(.


---
**Steve Clark** *February 04, 2018 19:32*

Sorry Don I forgot to link you. Long answer...I don’t like it. The problem I see is getting rid of the heat with this one. There is not enough surface area to carry it off, nor a method (fans and surface area). 



The peltiers are fine it’s just that the supporting housing assembly is a bit of a joke. In order for the peltiers to cool down to their rated ability you need the appropriate heat dissipation also... otherwise they saturate quickly and that limits their cooling ability.



 I looked up the chip number and I also do not see where they are getting the 360 watt claim.



[http://www.thermonamic.com/tec1-12710-english.pdf](http://www.thermonamic.com/tec1-12710-english.pdf)



This one below, IMO, is a link to a better unit. Note that, the claimed wattage on for both of these is really incorrect and need to be looked at in perspective. The unit you’re looking at, as I said, has a 360 watt claim. Reading the tech sheet, at the max voltage the seller claimed… you will only get 100 watts cooling per chip under perfect conditions. I’ll repeat that …“perfect conditions”.

 

Not to go into too much detail but the actual cooling ability will be about 60 to 70% of the input wattage. The other problem is 16 or 17 volt transformers of that amperage are pricey. 12 volt 30 amp DC transformers can be had for 20 bucks new. That’s why I ended up with one for each of my peltier banks.



On your example and, using the tech sheet the end result cooling I saw was 65 watts per peltier or only  195 cooling watts … IF THE GENERATED HEAT IS REMOVED. You would have to buy the finned extrusion, conductive grease, fans and screws to put it all together.



The one I’ve linked to below has all the components to function correctly. However, understand it’s claimed rated cooling wattage is a 170 watts (which I don't believe I think it's 126 watts functional) MIGHT be enough if it’s not a hot day, but two would work for all your needs year round. (I decided to run two similar  peltier banks that are suppose to give me about 126 watts functional cooling each or a total of 252 watts cooling with both banks.)



I also am not trying to keep 19 liters of water cool. The only reason to have that much water is as a heat sink to dump the warmed water in.

 

With a peltier system there is no need for all the water. The only reason you use the water is it is the cheapest and most efficient way to get the heat out of the laser tube and to the peltiers surfaces. The peltiers pull the heat and throw it out on their radiating (hot) side which has the finned surface area attached and fans to clear the heat.



I’m only using about 2 ½ liters in a closed and insulated system with a small mixing tank. That’s all.

 

Mine works great summer and winter time and keeps the water within 1 or 2 degrees at all times, no matter how long I’m running the laser. It appears that 250 “functional” watts is a good number for the way I’ve set mine up. (read about my build for details).



[ebay.com - Details about 180W Semiconductor Refrigeration Radiator Thermoelectric Peltier Water Cooler](https://www.ebay.com/itm/180W-Semiconductor-Refrigeration-Radiator-Thermoelectric-Peltier-Water-Cooler-/263166880642?hash=item3d45f7eb82) 






---
**Anthony Bolgar** *February 05, 2018 05:12*

Thanks for the info **+Steve Clark**. I have a CW-3000 that I was going to use it in conjunction with. I will check out the link you provided.


---
**Claudio Prezzi** *February 05, 2018 08:51*

**+Anthony Bolgar** The CW-5000 has spcified 800W of cooling power (compressor cooling).



The CW-3000 just uses passive cooling with a radiator and a fan and does not specify the cooling power.


---
**Don Kleinschnitz Jr.** *February 05, 2018 12:35*

**+Steve Clark** I think this is your last post on the cooler: [https://plus.google.com/u/0/102499375157076031052/posts/A2m6Csg59b8](https://plus.google.com/u/0/102499375157076031052/posts/A2m6Csg59b8)



Did you release a schematic and parts list?



I figured that the bottom line consideration on a peltier subsystem cooling performance is what temp below ambient can you achieve while cooling some amount of water, in some amount of time, at some amount of lasing activity, right?

I have moved my K40 to the garage and it can hit >90F there in the summer. I did not expect that with your setup I could get to an acceptable temp level (77F).



Is that conclusion valid from your experience?






---
**HP Persson** *February 05, 2018 22:16*

I made a similar one, 4x80w peltiers, dual loops, one for the laser, one for the hot side (on a radiator), gained more this way than air cooling the hot side.



Did not test it very much before i found more stuff to upgrade so my lasers was unusable :P

But the first tests were good.



Thread here -> [plus.google.com - Everyone doing peltiers, so here´s my next version. Had a 4x40w peltier earli...](https://plus.google.com/u/0/112997217141880328522/posts/S8hdAGRKnmw)




---
**Steve Clark** *February 06, 2018 01:58*

**+Don Kleinschnitz**  I have one it's a little rough I'll post it later I have used it now up in to the high 90's without problems for even long runs.


---
**Steve Clark** *February 06, 2018 23:57*

**+Don Kleinschnitz** Don, schematic and parts list posted on a new post. Please let me know if I posted any dead shorts! I didn't see any.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/bR7AhTk94X9) &mdash; content and formatting may not be reliable*
