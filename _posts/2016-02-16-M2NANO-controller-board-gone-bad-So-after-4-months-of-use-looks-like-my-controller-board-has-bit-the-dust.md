---
layout: post
title: "M2NANO controller board gone bad. So after 4 months of use, looks like my controller board has bit the dust"
date: February 16, 2016 01:24
category: "Discussion"
author: "HalfNormal"
---
M2NANO controller board gone bad.



So after 4 months of use, looks like my controller board has bit the dust. It will start to fire the laser then nothing. If I press the test button, the laser fires and will vary from 0% to 100% laser power with the power pot as long as the test button is pressed. This leads me to believe that the Power Supply and laser tube is just fine and it is not receiving the correct signal from the controller board. I emailed the the seller who claims to answer emails within 8 hours. They are in Hong Kong and it is the start of their day so I hope to receive a reply by morning. 

Good news is that I ordered all the parts to convert it to the Ramps option when I ordered the laser so maybe it is time to start the process.

Will update when if/when I hear a reply.





**"HalfNormal"**

---
---
**Sean Cherven** *February 16, 2016 01:50*

Did you check the wiring from the far right connections on the power supply, that goes to the controller board?



In this picture, the wire I'm referring to is the one marked "L" from the power supply. It goes to the M2 controller board. Check the wiring to make sure it's good.



Picture:

[http://4.bp.blogspot.com/-84-8KpcR0vY/U-UAoggAaII/AAAAAAAAEp4/aYj0-6NZ8HY/s1600/CO2LaserRampsHookup.jpg](http://4.bp.blogspot.com/-84-8KpcR0vY/U-UAoggAaII/AAAAAAAAEp4/aYj0-6NZ8HY/s1600/CO2LaserRampsHookup.jpg)


---
**HalfNormal** *February 16, 2016 01:54*

Did a quick connect and disconnect on the cables but did not check continuity


---
**Sean Cherven** *February 16, 2016 02:07*

Interesting... Does it do anything strange or funny? or does it just not work at all?


---
**HalfNormal** *February 16, 2016 02:38*

Does everything but fire the laser at the pot setting. If the power setting is high enough, it will produce a beam.


---
**Sean Cherven** *February 16, 2016 02:47*

Well that don't make any sense at all... Do you have a logic tester? If so, you should check to see if the controller is producing a "active low" signal.



Or you could try wiring the test fire wire in replace of the L wire. I do believe they both do the same thing.


---
**HalfNormal** *February 16, 2016 03:05*

I do have test equipment. Will have to do my homework and see how the system actually operates.


---
**Anthony Bolgar** *February 16, 2016 04:11*

If you need a new M2Nano board, I have one that only has about 3 hours time on it I would be willing to sell.


---
**HalfNormal** *February 16, 2016 12:38*

Anthony, email me what you are asking. Can PayPal  you.


---
**Anthony Bolgar** *February 16, 2016 13:20*

How the heck do I email in google+? Drop me a line at anthonybolgar@live.com


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/KADXMfKEXc4) &mdash; content and formatting may not be reliable*
