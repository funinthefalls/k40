---
layout: post
title: "First draft ofcthe power supply. Have not annotated the parts yet"
date: October 06, 2016 06:35
category: "Hardware and Laser settings"
author: "Paul de Groot"
---
First draft ofcthe power supply. Have not annotated the parts yet. Hopefully get that done this weekend. The snubber diodes were added by me since a transistor shortened and the rectifier diode in the 24volt supply. As you see grounds between 220/110 and low power gnd are separated but can fail if a capacitor fails. Hope this helps everyone. 

![images/cbbc541aa239bf129633b01bce29f70f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/cbbc541aa239bf129633b01bce29f70f.png)



**"Paul de Groot"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 12:35*

**+Scott Marshall** This may be of interest to you.


---
**Don Kleinschnitz Jr.** *October 06, 2016 13:27*

Awesome, this is just what we needed, thanks for doing this. What tool did you create this in and can we get the source as this picture is a bit hard to read? 


---
**Don Kleinschnitz Jr.** *October 06, 2016 14:16*

Also on the next version if you can clarify where the control signals enter the supply that would help me verify the external control schema. Its also difficult for me to find the AC input. Hard to read when I blow it up...

Again thanks for doing this, very helpful.


---
**Paul de Groot** *October 06, 2016 20:11*

**+Don Kleinschnitz** i used eaglecad which is free. And of course i will publish the schematic in its full form. I knew some people were after this so i publish this first one asap.


---
**Paul de Groot** *October 06, 2016 20:14*

**+Don Kleinschnitz** yes a bit of a narrative is required to explain the functions. The reversed logic of firing the laser with 0 can be easily fixed as well. Keep you all posted.


---
**Don Kleinschnitz Jr.** *October 06, 2016 20:49*

Awesome looking forward to the eagle file.


---
**David Richards (djrm)** *October 06, 2016 22:42*

Nice work, big thank you.


---
**Paul de Groot** *October 06, 2016 22:52*

**+david richards** you're welcome. Still haven't found my hardware issue yet.


---
**Don Kleinschnitz Jr.** *October 07, 2016 12:32*

**+Paul de Groot** what hardware issue are you fighting. You mean why this happened or another problem?


---
**David Richards (djrm)** *October 07, 2016 16:49*

**+Paul de Groot** Here is a description I have just found about how the TL494 chip works, it may help you. [http://www.savel.org/2005/08/23/tl494-magic-chip](http://www.savel.org/2005/08/23/tl494-magic-chip)/ kind regards, David.

[savel.org - TL494 – magic chip &#x7c; Savel brain dump in English!](http://www.savel.org/2005/08/23/tl494-magic-chip/)


---
**Paul de Groot** *October 08, 2016 06:37*

**+Don Kleinschnitz** my problem is that the rectifier diode shortens after I start moving my steppers. I used replacements (FR307) which should be working but after a few steps they blow. Ordered the originals HER308G as well from RS online which arrived yesterday. Replaced it and so far so good. This one seems to hold.


---
**Paul de Groot** *October 08, 2016 06:40*

**+david richards** yes saw that. One of the issues I had is that the high voltage seems to kick in when I hook up my laptop via the usb port. The OP amp input 15 and 16 are floating so I have grounded them and that seems to have solved that problem. Slowly rectifying the design flaws...


---
**Don Kleinschnitz Jr.** *October 08, 2016 11:59*

**+Paul de Groot** Still seems possible you have a grounding problem and these are symptoms not the cause. Hope you current changes fix it for you.

Which diodes are you referring to? I still have difficulty viewing the schematic as it is blurry when zoomed.

The floating op amp inputs isn't good but shouldn't really cause excessive current draw in the steppers? But maybe these are two problems.

 Is this a standard K40 setup or do you have a conversion in place. 


---
**Paul de Groot** *October 08, 2016 22:44*

**+Don Kleinschnitz** it's the rectifying diode in the 24volt supply line. I have a nanom2 and a grbl cncshield with a arduino. Also have made my own to get playing with it. I think that indeed there were some ground issues. Haven't assembled it all together yet since i still have annotate the schema.


---
**Paul de Groot** *October 08, 2016 22:49*

Still a work in progress to replace my cnc shield so i can just plug in all my connectors. Just waiting for the headers to arrive 

![images/b32c5697d6bfc1e046bbccd66b664cea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b32c5697d6bfc1e046bbccd66b664cea.jpeg)


---
**David Richards (djrm)** *October 09, 2016 07:36*

**+Paul de Groot** Perhaps the problem is with the stepper driver and not the power supply itself. If one of the driver output transistors had hailed it could draw a high current when it tries to turn on its complementary driver. Substitute the driver and use a power supply with ammeter. hth David.




---
**Paul de Groot** *October 09, 2016 08:05*

**+david richards** thanks i already have replaced the stepsticks as well. So far it seems to be alright. Will test a bit more. 


---
**Don Kleinschnitz Jr.** *October 10, 2016 09:45*

**+Paul de Groot** I stumbled across this spec that suggests these supplies only put out 24@1A and 5V @ 1 A. Not enough to run much of a stepper.

[http://www.ebay.com/itm/40W-Power-Supply-Mini-CO2-Laser-Rubber-Stamp-Engraver-Cutter-Engraving-110-220V/311665474162?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D38530%26meid%3D3bc7b9f262f246fca7512e47acedd983%26pid%3D100005%26rk%3D2%26rkt%3D6%26sd%3D172276804836](http://www.ebay.com/itm/40W-Power-Supply-Mini-CO2-Laser-Rubber-Stamp-Engraver-Cutter-Engraving-110-220V/311665474162?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D38530%26meid%3D3bc7b9f262f246fca7512e47acedd983%26pid%3D100005%26rk%3D2%26rkt%3D6%26sd%3D172276804836)



[ebay.com - Details about  40W Power Supply Mini CO2 Laser Rubber Stamp Engraver Cutter Engraving 110/220V](http://www.ebay.com/itm/40W-Power-Supply-Mini-CO2-Laser-Rubber-Stamp-Engraver-Cutter-Engraving-110-220V/311665474162?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D38530%26meid%3D3bc7b9f262f246fca7512e47acedd983%26pid%3D100005%26rk%3D2%26rkt%3D6%26sd%3D172276804836)


---
**Paul de Groot** *October 10, 2016 20:20*

Great find **+Don Kleinschnitz** and the price is much lower than usual (160 Dollars). 


---
**beny fits** *October 12, 2016 20:09*

Is the the one you were telling me about on instrucabuls


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/Wpmov9JDdJJ) &mdash; content and formatting may not be reliable*
