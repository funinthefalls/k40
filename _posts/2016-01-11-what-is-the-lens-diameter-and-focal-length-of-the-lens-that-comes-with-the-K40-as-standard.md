---
layout: post
title: "what is the lens diameter and focal length of the lens that comes with the K40 as standard?"
date: January 11, 2016 17:37
category: "Hardware and Laser settings"
author: "Pete OConnell"
---
what is the lens diameter and focal length of the lens that comes with the K40 as standard? I'm in need of a new lens but don't want to be modifying the rest of the machine just yet.





**"Pete OConnell"**

---
---
**Anthony Bolgar** *January 11, 2016 17:56*

12mm diameter lens with a 50.8mm(2") focal length


---
**ChiRag Chaudhari** *January 11, 2016 17:56*

Dia = 12mm and Focal Length = 2"


---
**Pete OConnell** *January 12, 2016 00:02*

cheers **+Anthony Bolgar** and **+Chirag Chaudhari** 


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/GTp7iB37BFr) &mdash; content and formatting may not be reliable*
