---
layout: post
title: "Another laser tube replacement question... I see some ppl solder the connection"
date: June 05, 2017 21:03
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Another laser tube replacement question...



I see some ppl solder the connection. I don't have a solder and I see it would void the warranty if used. 



Has anyone tried another connection method with success?







**"Nathan Thomas"**

---
---
**Don Kleinschnitz Jr.** *June 06, 2017 13:55*

See if this post helps. 

There are many approaches. I think that the safest is just wrapping the HV lead around the post and then potting the silicon sleeve back on.

I have personally not tried these as my tube has not been replaced. HV connections don't have to be soldered, just mechanically stable and then insulated properly against arcs. 



[donsthings.blogspot.com - K40 Laser Tube Specifications, Maintenance, Failure & Replacement](http://donsthings.blogspot.com/2017/05/k40-laser-tube-specifications.html)


---
**Jeff Johnson** *June 07, 2017 19:48*

I use tiny wire splicing crimp connections. I crimp the wire to one end and squeeze the other end in the crimp tool until it slides snugly onto the post on the laser. DO THIS WITH THE CRIMP CONNECTION OFF OF THE LASER!!!!!! Squeeze a tiny bit, test, then repeat until it's snug. The ones I used look like the red one, second from the right. [elecdirect.com](http://www.elecdirect.com/media/catalog/category/Butt-Splices-Connectors.jpg)




---
**Don Kleinschnitz Jr.** *June 08, 2017 12:59*

**+Jeff Johnson** interesting approach, I assume that after this you put the silicon sleeve back on and fill with silicone?

I would also make sure that if the crimp sticks up much that the HV cable is strain relief-ed to prevent any torsional force. These connection posts are fragile as they go into the glass.




---
**Jeff Johnson** *June 08, 2017 13:39*

I didn't silicone it but the wire is secured to the tube and protected by the box surrounding it. When I got a new tube I went with a much more powerful 1 meter tube and I'm using a spark plug wire for the high voltage.


---
**Nathan Thomas** *June 08, 2017 13:42*

What is the purpose of the silicone and is it something I can get at a hardware store...?(home depot etc)

Talking about the silicone tube and liquid/gel.


---
**Jeff Johnson** *June 08, 2017 14:02*

I think the silicone just secures the wire and helps prevent corrosion.


---
**Don Kleinschnitz Jr.** *June 08, 2017 20:53*

**+Jeff Johnson**, **+Nathan Thomas** I believe the silicon is to stabilize the connection and keep dust etc away from the connection thereby preventing an arc. Most any material can become charger and collect around a connection and eventually create a HV track to ground.



The silicon to use is in the post I linked above:

"Alternate silicon: Permatex Blue RTV Gasket Maker. Available in auto and big box hardware stores.

703 adhesive silicone rubber insulation/potting/electronic waterproof sealant (the stuff that comes with the machine)"



.... I think I even found some in Wallmart.




---
**Jeff Johnson** *June 09, 2017 15:00*

**+Don Kleinschnitz** Thanks, I'll get some and properly seal my connection.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/H2PveR7WMkK) &mdash; content and formatting may not be reliable*
