---
layout: post
title: "River Rock engraving 300mm/S 4ma"
date: May 17, 2016 20:38
category: "Object produced with laser"
author: "Alex Krause"
---
River Rock engraving 300mm/S 4ma

![images/b8b1a4c44851c99f142ac35e71753924.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8b1a4c44851c99f142ac35e71753924.jpeg)



**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *May 17, 2016 21:21*

That's super cool!!!! 


---
**Alex Krause** *May 18, 2016 05:09*

Thanks **+Peter van der Walt**​ :) when China gives you lemons ;) make lemonade :P


---
**Alex Krause** *May 18, 2016 05:31*

**+Peter van der Walt**​ cardboard is your best friend with the stock software I cut out a duplicate of my blank and use it for alignment by using the border selection then click the preview button and it traces out where my image will be placed. It makes alignment alot easier 


---
**Stephane Buisson** *May 18, 2016 06:28*

**+Peter van der Walt** except repeatability, I can't remember why, but it was not always the case, but at that time, I lost so much time running after the parameters, so glad it's in the past.


---
**Alex Krause** *May 19, 2016 05:38*

I would really love to see what kind of results I could get with a unit from **+FabCreator**​ on various projects. I think I have more fun testing what materials and surface finishes I can work with more than anything


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/FyAhMGhZvb3) &mdash; content and formatting may not be reliable*
