---
layout: post
title: "Hello everyone, I and my wife ar very much news to the K40 and laser cutting back in general"
date: December 03, 2016 15:40
category: "Discussion"
author: "Tom Lauten"
---
Hello everyone,



I and my wife ar very much news to the K40 and laser cutting back in general.



We unwrapped our machine yesterday and have been attempting to get SOME results since. We have Corel Draw 12 and CorelLaser loaded and operating.



We have some b&w art prepped on Photoshop. We have made various attempts at cutting through some card. Most have worked well enough once we got the speed up, but we have a few truly puzzling issues.



1) I'm trying to cut A4 sheets. I have the standard new file size set to 310x210 mm in CorelLaser and the same dimensions for the cutting area in the in the cutter preferences. I drew a dot in each corner of the artwork, put a sheet of paper in the cutter trying best to line it up in a "centre" position. When I set it going it did indeed cut the dots but it seems to have done it at half scale! The rulers on the Corel document show it as still being 310x210. When we placed  a different image on the same size blank Corel image the cutter overshot the one edge of the card because the card wasn't placed correctly in the cutter but it seemed to be the size we had expected...all but A4! Aaarrrrggghhh! Any ideas how I can get the blank art file and cutting bed to correspond to each other and stay in scale to each other?



2) I tried using the CorelLaser "cutting" option as opposed to setting the engraving option to cutting but can NOT get it to work. No preview image shows up in the cutting control window as it does when I select the engraving function. Does anyone else have this problem. Does the cutting function give any different results than cutting in the engraving function?



I have soooooo many more basic questions but you folks sure could help cure a number of headaches with these 2 issues...sure hope you could shed some light.



Cheers, Tom Lauten







**"Tom Lauten"**

---
---
**Jim Hatch** *December 03, 2016 16:27*

Make sure you set the machine info (both model & machine id) in both LaserDRW and Corel. Cutting with lasers is only done for lines and the line thickness shoukd be .001mm so it doesn't try to engrave. The K40 cannot both engrave rasters & cut vectors in the same job - they have to be done separately.



When you turn the machine on the head shoukd move to the upper left corner. That's the home position and shoukd match the upper left of your material. You can move the drawing from there using the offset parameters in the cutting/engraving dialog box but first get it all working from the home position before trying to do more.


---
**Ned Hill** *December 03, 2016 17:37*

Tom, with the corel plugin it will place the image at the origin regardless of where it is on the page unless you give it an origin reference.  Typically people put a small dot, or in my case I prefer a 1mm x 1mm square, with no fill or outline, at the top left origin point of the page, not the image.  This will then be the origin reference for your image and you select both the origin reference and your image to cut or engrave.   You can create an origin jig by placing a piece of cardboard or plywood that securely fits in the top left corner of the laser bed and then draw 2 cutlines extending to the right and down from the origin.  do these cuts and you will have will be able to place your pieces exactly at the origin each time. 

The engraving option is done by rastering the image one scan line at a time from top to bottom where as the cutting option is  vector based where is follows a line directly.  This is mostly done for cutting but can be used to do vector engraving if your design is line based.  Be sure to set cut lines to 0.001mm in width or it will cut both side of the line.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 03, 2016 18:07*

I'd say your issue with scale is as Jim mentions, board ID & model related. Set those & you should be good to go on that one.



In regards to the cutting not appearing as a preview, I'm thinking that could be related to the output that is sent to the plugin.



See these settings: 

[drive.google.com - Check CorelDraw Settings.png - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)


---
**Nimba Creations** *December 03, 2016 18:09*

Hi folks...ok...whooooosh! Right over my head with an awful lot of that. Problem is many of the terms are still "Greek" to me. Like trying to teach Russian to a Frenchman using Mandarin!



Perhaps I should start with images...



My latest test sent the laser skittering across the card at 8 million mm/s in a scratching mess. I think I have messed all my settings up...sigh...can't even figure out how to start again and get even a poor result!



![images/1ebaac6171ebbe2d5a5a59713ffb5129.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ebaac6171ebbe2d5a5a59713ffb5129.jpeg)


---
**Nimba Creations** *December 03, 2016 18:11*

![images/7c356e8032312c2cf73883f7895bdc6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7c356e8032312c2cf73883f7895bdc6b.jpeg)


---
**Nimba Creations** *December 03, 2016 18:14*

Everything was going ok then the lightening bolts went all lumpy (the left hand side of the bolt is supposed to be symmetrical...the lower line of it is all wiggly) and the scale started gong off.

![images/87b4ec45f2677d8245555caa4a97f1a1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87b4ec45f2677d8245555caa4a97f1a1.jpeg)


---
**Nimba Creations** *December 03, 2016 18:15*

This was the last, INSANE run...you can see the laser just went mental and at a hell of a rate of knots! Slightly alarming.

![images/952063b136b247a28d5235b7f2f3f699.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/952063b136b247a28d5235b7f2f3f699.jpeg)


---
**Nimba Creations** *December 03, 2016 18:18*

Here are some shots of the settings I currently have. I think I have conflicts and a real mess but I am lost as to where I should be at or even how to get back to anything sensible. I'm sure there are other set up prompts I have missed out or forgotten about

![images/84efe2691066a932f3fc33fbc7e8c13b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/84efe2691066a932f3fc33fbc7e8c13b.jpeg)


---
**Nimba Creations** *December 03, 2016 18:19*

![images/928ff1f704dd6a9415d0780c61a6dd0d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/928ff1f704dd6a9415d0780c61a6dd0d.jpeg)


---
**Nimba Creations** *December 03, 2016 18:20*

![images/a905e9a2d50114337860e1dacebd32fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a905e9a2d50114337860e1dacebd32fb.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 03, 2016 18:21*

See my previous comment **+Nimba Creations**. Set your cutting data to WMF. Looks like the only setting that needs changing.


---
**Nimba Creations** *December 03, 2016 18:24*

My blank cutting control screen.... :(



Worth mentioning that the artwork was an imported b&w jpeg created in Photoshop

![images/9ec287c514d0b0665be235031ecc4ec9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9ec287c514d0b0665be235031ecc4ec9.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 03, 2016 18:25*

**+Nimba Creations** Sorry, also set your Board ID to D4574FE572F00B92 (from your photo of your board). It is currently set to something else & that tends to cause some strange happenings when sending data to the laser.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 03, 2016 18:27*

**+Nimba Creations** Are you selecting the objects you want to cut? Because in your settings for it you have it set to "Only Selected", whereas for engrave you have it set to "All Pages". You need to select what you want to cut in that case (or change that setting to "All Pages" also).


---
**Nimba Creations** *December 03, 2016 18:33*

Sorry folks, I seem to have switched from my personal to business account...oh man, me and software today.



 I am going to have dinner and sit back down and look at everything again. I just have to find where to set cutter ID stuff and matching software stuff and try to work out what half of the terms you kind and helpful people are using actually mean in "newbie idiot language"!


---
**Ned Hill** *December 03, 2016 18:56*

**+Nimba Creations**​ you need to change the Device ID circled in the attached pic to D4574FE572F00B92. 

![images/39b07db18bf08efdcf31ad8fd7c441f8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/39b07db18bf08efdcf31ad8fd7c441f8.png)


---
**Nimba Creations** *December 04, 2016 15:29*

Hi everyone.



Well, I'm finally back on track thanks to you. The insane skittering cutter was owing to the speed setting set waaaaaaaay to high as that was the only speed it would work atbwhen the board reference was wrong. Now it falls more inline with the kind of speeds I have read about...sort of 8-15 mm/s.



Can NOT get the whole placement/reference/bed size/cutting area thing worked out. It certainly isn't WYSIWYG!!!



Tried putting in a reference point, an unfilled, un outlined square but it disappears when I save the file...sometimes... and I just don't know what page/layer to put it on nor how to get the software to recognize it and bias the cutting to it as a reference point so that I can align the artwork on screen with the material to be cut when the machine starts doing its thing. VERY unfamiliar with Corel...I'm a Photoshop guy.



I'm pretty much only cutting A4 sheets but there seems to be little correspondence to that size in terms of the cutting area and placement within it.



Can't I just set an A4 work/cutting area in the machine and lock that down? My dream is to be able to lay an A4 sheet in to a registered area in the machine and KNOW that this area accurately corresponds to the artwork in CorelLaser. Then be able to Scale and place the artwork on screen and KNOW it will correspondingly cut it into the material as per the artwork on screen. 



Ideally, someday, I would like to cut CAD parts so I could slot together small mechanisms using the materials thickness and slots cut into the material and KNOW it will all piece together but unless I can place things accurately and strictly maintain dimensions I'm in for serious headaches.


---
**Ned Hill** *December 04, 2016 16:16*

**+Nimba Creations** The first thing I do when creating a new page is zoom into the top left corner. Then in the top left corner of the edge rulers there is a little symbol you can drag to the corner of the page and that sets the 0,0 origin at that corner of the page.  Then I create a 1x1mm box and set the line widths to None.  Place the box at the top left corner of the page and this is now your origin reference.  You will have noticed that if you just select your image and then go into engrave, or cut, the preview puts the image into the top left corner regardless of where it is on the page.  This is a quirk of the software.  To get around this you have to select both the origin reference you created and your image. Then the image position will be as you have it on the page. 

To see where your origin is in the laser bed, put of piece of paper in the bed with the laser head in the home position and do a low power test fire.  This is where the machines 0,0 origin point is located.  To consistently place your material in the laser it's advisable to create an origin template out of a piece of 1/4" ply, or other material.  The ply needs to be big enough to fit into the top left corner of the laser bed and extend at least part way through the origin position.  Then in corel create 2 lines with widths of 0.001mm.  One line goes from the origin to the right, parallel to the top edge of the page.  The other goes from the origin down the edge of the page.  Extend the lines so they go beyond the wood.  Then cut the lines and you now have an origin template to easily place your material each and every time.  I've attached a pic of my laser bed for reference.  The piece of wood in the top left is my origin template or jig.  Hope this helps.

![images/c541ca29de6097cdb7e75ee65f9d2cc2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c541ca29de6097cdb7e75ee65f9d2cc2.jpeg)


---
**Nimba Creations** *December 04, 2016 17:02*

Ok, I think I kind sorta follow.



My cutter seems to kinda go to a vertical centering point as default and the left and right seems random, that's  the problem, there seems to be no consistency. I do usually have the origin reference set to upper left in the engraving/cutting options.  So are you saying that if I put this invisible square in the upper left the visual page in Corel this layout is how the laser should cut the artwork in relation to its assigned cutting area?



Is there a place one should set the bed size for the cutter? If so where is that place? What size should it be for the K40? How does one tell the laser that the active area for any given file is A4 with its origin point as a datum? My left hand edge seems to keep shifting...I think.



Do your controls for the laser (the engraving/cutting/stop button etc) disappear when you start cutting NEVER to show up again? My controls (just a small strip of icons) appear in an upper bar of the Corel screen but go away as soon as the cutter starts. If I need to stop the cutter I have to manually power it off. Is this normal? To get the controls back I have to shut down CorelLazer and reopen it again...every time.



How does one "select" both the artwork and the invisible square so that the software and cutter pay attention to their relationship to each other and cut accordingly? What layer do you create the square on? Could this be saved as a default or does it need to be done for every new file. When you save the file does it save the invisible reference point as well?


---
**Ned Hill** *December 04, 2016 17:46*

Ok going to try to answer each one at a time.  You need to make a few setting changes if you haven't already.  See pics.  

![images/b52e74e02d3ac7e5bc52428f438b54e9.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b52e74e02d3ac7e5bc52428f438b54e9.png)


---
**Ned Hill** *December 04, 2016 17:47*

Here's your page size settings (these are max)

![images/906294a32a422860a2a5904b0609d667.png](https://gitlab.com/funinthefalls/k40/raw/master/images/906294a32a422860a2a5904b0609d667.png)


---
**Ned Hill** *December 04, 2016 17:48*

The toolbar disappears to your windows toolbar.  See pink icon in pic. Right click on it.

![images/0a439d74b2c78258036b0b0140814cd1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0a439d74b2c78258036b0b0140814cd1.png)


---
**Ned Hill** *December 04, 2016 18:00*

Yes  the software puts the little square in the top left corner and keeps everything else spaced in relation to that square.  Each element you want to engrave, or cut, you have to individually select from the view manager on the right hand toolbar (you may have to turn it on from the view drop down menu).  You will also need to select the square.  Some people put cuts in one layer and engraves in another to keep them sorted, but not a necessity.  You can work across layers so you don't have to have a square in each layer, but sometimes you do if you want to turn a layer off to make selecting the different elements easier.  Something you will have to play with.


---
**Ned Hill** *December 04, 2016 18:02*

Personally, I have created a blank page template so I don't have to do all these things each time I create a new project.


---
**Nimba Creations** *December 04, 2016 18:11*

Ok, I shall have a go.



Btw, does this look right. I set the page size to A4. The "origin-X" & "origin-Y" setting... the 10mm values, are they like margins? Should/could I set those to "0"?

![images/c47ac62721eaaa541381b2e3efd19aab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c47ac62721eaaa541381b2e3efd19aab.jpeg)


---
**Ned Hill** *December 04, 2016 18:12*

Another thing to consider.  When you first turn on your machine is it should home to the top left  as the origin.  If you release the rail while the machine is on and manually move the head it will keep use that new position as the origin unless you click on reset.


---
**Ned Hill** *December 04, 2016 18:15*

Yes Set the origin values to 0


---
**Nimba Creations** *December 04, 2016 18:17*

Great stuff...thanks...fingers crossed!


---
**Nimba Creations** *December 04, 2016 19:42*

OMG!!! So almost there.



1 step forward, 3 Back.



Tried to get everything lined up, almost worked. 



Now the top of the vertical axis is BEHIND the exhaust vent!!!!!



I tried to cut an A4 alignment guide and the head found its start point then tried to cut L&R straight across the vent about 10mm in!!!!!



I've tried resetting the head by hitting the reset button and by turning on the cutter both with and without the computer hooked up but it just keeps going back to that point. I am mystified!



I'm also now getting an electrical "tingle" from the cutters cabinet!


---
**Ned Hill** *December 04, 2016 20:00*

Ok, my bad I forgot that you still had the stock vent in.  So set the y-axis origin value to 10mm.  That's why most people end up removing or cutting back that vent to give more room.  Not sure about the electrical tingle.  Are you using a grounded outlet?


---
**Nimba Creations** *December 04, 2016 20:17*

Ahhh, the margin setting I changed before...ok, makes sense.



It isn't a grounded lead I don't think. There is a grounding lug on the back of the chassis...I guess I'll hook that up to a radiator or something. I guess I better check all the wiring again to make sure everything is staying where it's supposed to. It isn't high voltage but it also ain't right.



I think I got the reference square thingie to work. Managed to scale an image and rightly place it where I wanted on the card stock...all barring material alignment issue. Thanks for all this help. Feeling pretty good for just two days into it all.




---
**Nimba Creations** *December 05, 2016 19:54*

Hello all,



I thought I was pretty much there...now, as I am trying cut cut guide lines for the edge of the working area to make a positioning jig, the laser cuts both on the way out from the starting point AND on the way back. The 2 lines I drew (1 on the X and.1 on the Y axis) are hairline. This is getting so frustrating.



Any thoughts?


---
**Ned Hill** *December 05, 2016 20:08*

Line width needs to be set to 0.01mm or less for cuts or it will cut both sides of the line.


---
**Nimba Creations** *December 05, 2016 20:13*

Ah...I see. So do I need to redraw or can I reset the line width? 



Where does one set the line width and can it be saved as a permanent minimum or does one have to specify it each time?


---
**Ned Hill** *December 05, 2016 20:19*

Just reset the width.  Select the lines, right click and select object properties.  Or you may be able to do it from the top toolbar depending on how you are set up.  You will need to do it each time.


---
**Nimba Creations** *December 05, 2016 20:36*

Thanks so much Ned, you're a hero!


---
*Imported from [Google+](https://plus.google.com/+TomLauten/posts/DsXWmQkEGwq) &mdash; content and formatting may not be reliable*
