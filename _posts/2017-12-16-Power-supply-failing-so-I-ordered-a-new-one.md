---
layout: post
title: "Power supply failing so I ordered a new one"
date: December 16, 2017 13:08
category: "Original software and hardware issues"
author: "Kevin Lease"
---
Power supply failing so I ordered a new one.  I saw a web page authored by HP Persson in which he advocated for preemptively upgrading the bridge rectifier to one rated for more amps before installation.  Have others done this and thoughts about it?





**"Kevin Lease"**

---
---
**Don Kleinschnitz Jr.** *December 16, 2017 13:59*

Information on my blog about LPS repair and parts. 



Did your supply blow its fuse?

That is usually an indicator that the bridge is bad. If so it can be repaired by replacing the bridge.



If the supply is "failing", suggests its still working but not dead, then I doubt you have a bridge problem.



Upgrading the bridge may prevent a failure but if the bridge is not already failed I would leave it alone and upgrade it when/if it fails.



The actual part in the supply varies by the vintage, some have marginal parts some are ok.



[donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/7v4kLdXg33B) &mdash; content and formatting may not be reliable*
