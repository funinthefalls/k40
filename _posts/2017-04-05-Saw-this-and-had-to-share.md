---
layout: post
title: "Saw this and had to share!"
date: April 05, 2017 23:39
category: "Smoothieboard Modification"
author: "HalfNormal"
---
Saw this and had to share!

[http://www.thingiverse.com/thing:2228964](http://www.thingiverse.com/thing:2228964)





**"HalfNormal"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 02:20*

Most certainly. I came across this on the fb group a few days ago. It's friggin amazing. I'll make one! 


---
**Ray Kholodovsky (Cohesion3D)** *April 06, 2017 02:22*

Many props to Rich Lee over at the Facebook K40 group for this design!![images/0d0d620dbe5511e8e1ccd312311ad8aa.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0d0d620dbe5511e8e1ccd312311ad8aa.png)


---
**Josh Rhodes** *April 06, 2017 04:17*

Looks like an og gameboy. 



It's a good look.


---
**Coherent** *April 09, 2017 12:34*

Very nice. Thanks for sharing the link and great job by the original thingiverse poster.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/ch8aNNyziyo) &mdash; content and formatting may not be reliable*
