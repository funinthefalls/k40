---
layout: post
title: "I just got my K40 (from ebay) and am in the process of converting the controller to a Smoothieboard (clone)"
date: February 07, 2016 07:59
category: "Hardware and Laser settings"
author: "Abe Tusk"
---
I just got my K40 (from ebay) and am in the process of converting the controller to a Smoothieboard  (clone).  I had been testing the PWM on the Smoothieclone with success and was in the process of further testing when I think I heard a 'pop' and then the power supply wasn't supplying power at all anymore.



The fan next to the laser is still running and the fuse located in the three prong wall mains connector looks to be good.  The red power button on the case still lights up when the power is switched on and I've checked that there is 120V AC being supplied to the power supply but the power supply itself looks to be dead.  No power is being supplied to the 4 prong output (24V, Gnd, 5V, Lo, the 4 prong VH connector on the far right in the picture), the fan on the power supply is not moving and pressing the 'test' button does nothing (e.g. the 'power' light does not come on).



I opened the power supply case up to have a look inside.  I see what looks to be a fuse soldered directly on board (there is a silkscreen piece of text next to it that says 'FU2').  I checked the continuity across it and there was none.  None of the capacitors looks to be damaged: I don't see them 'bowed' and no magic smoke came out that I could see.



Does anyone have any suggestions on what to do?  Would it be possible to replace the fuse on board?  If so, what fuse should I replace it with?  What would happen if I just bypass the fuse entirely and make a connection with a soldered piece of wire across?



I'm happy to provide more pictures or describe anything else I've done.  Any help would be appreciated.

![images/34b4db75a8e046df50b3981131da4c0b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34b4db75a8e046df50b3981131da4c0b.jpeg)



**"Abe Tusk"**

---
---
**Phillip Conroy** *February 07, 2016 08:36*

Most fuses have value stamped on one end,failing that solder in a fuse holder that has wires attached,then start at 1 amp and if it blows try 2 amp if it blows 3 amps etc


---
**David Richards (djrm)** *February 07, 2016 10:01*

When a fuse blows leaving the glass blackened inside it usually indicates a short circuit has caused it to blow, I would not replace it until I had found the cause. Likely culprits are the diode bridge rectifying the mains, and/or the main switching devices of power supply - on or near the heat sinks. I have looked for the schematics for these power supplies without success so it is difficult to be more precise. What were you doing when it failed? David.


---
**Abe Tusk** *February 07, 2016 16:21*

I noticed that one of the mains wires into the PS was loose when opening it up, so maybe what happened is there was a loose wire that caused it to connect intermittently, surging the power and causing the fuse to blow?



On visual inspection, what I assume to be the diode bridge rectifier look fine along with the components around it (no blackened/burnt components, nothing looks out of the ordinary).



At any rate, I'll take your suggestions and not bypass the fuse.  I don't see any label on the fuse but I'll try Phillip Conroy's suggestion and progressively increase fuse values.



If I did need to replace the whole power supply, has anyone had to do this?  I notice the wire going into the laser tube is thick and not really detachable.  Would it be alright to cut this and attach it to a new power supply?  Do I have to worry about soldering vs. mechanical joints and insulating it properly (I assume this is a high voltage line to drive the laser)?


---
*Imported from [Google+](https://plus.google.com/102172526207413999595/posts/4iq5nkkcq99) &mdash; content and formatting may not be reliable*
