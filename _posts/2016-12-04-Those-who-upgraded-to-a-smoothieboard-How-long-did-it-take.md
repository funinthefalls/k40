---
layout: post
title: "Those who upgraded to a smoothieboard. How long did it take?"
date: December 04, 2016 21:38
category: "Discussion"
author: "Ric Miller"
---
Those who upgraded to a smoothieboard. How long did it take? I have a board and a middle board but haven't done it because I have been using my laser so much. 





**"Ric Miller"**

---
---
**Ariel Yahni (UniKpty)** *December 04, 2016 22:32*

I suggest you make sure you are very clear what goes where and that all connections can be reverted. Also if you are going to use LaserWeb make sure it's running and know how to use it before doing the swap. 


---
**Jonathan Davis (Leo Lion)** *December 04, 2016 23:32*

Better yet take pictures of the original look and as you remove each cable.


---
**greg greene** *December 05, 2016 00:09*

Take the plunge - It's a little confusing at first - and sometimes it takes  a little longer if your old like me, but the journey is worth it and you will learn a great deal. 


---
**Don Kleinschnitz Jr.** *December 05, 2016 14:48*

This may help you avoid some research. [donsthings.blogspot.com - The K40-S "A Total Conversion"](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)




---
**greg greene** *December 05, 2016 17:00*

I used Ray's Cohesion 3D board - had some hiccups but it's working fine


---
**Ariel Yahni (UniKpty)** *December 05, 2016 17:24*

**+greg greene**​ what solved your issue? 


---
**greg greene** *December 05, 2016 18:06*

The setting to fire the laser had a Capital M followed by a three

When GCode generated, it changed it to a small case m followed by a three - no clue why, but I just send an M3 at the start before selecting run GCode and it appears to be working ok - the laser now fires.  I believe though there is a problem in LaserWeb with clearing the cache - or que as it appears to me that it picks up commands from previous operations and that causes the Homing issue.  There may be a command I don't know about that forces a cache clear - but I haven't found it yet.


---
**Ariel Yahni (UniKpty)** *December 05, 2016 18:20*

**+Peter van der Walt**​ any idea here ^^^


---
**greg greene** *December 05, 2016 18:32*

nope let me email you my screen shots - I can't get them to load into a reply


---
**greg greene** *December 05, 2016 18:35*

apparently  I'm not on the everyone list - all I get is a copy of the screen shot - and no postbutton


---
**greg greene** *December 05, 2016 18:36*

love it peter I'll post that in my shop !!!


---
**greg greene** *December 05, 2016 18:37*

I've thought of that - but I don't think it's the cause


---
**greg greene** *December 05, 2016 18:37*

or both


---
**greg greene** *December 05, 2016 18:40*

email sent Don, Thanks for looking at it


---
**Kelly S** *December 05, 2016 22:06*

I used the c3d board and it went very fast.  Machine side was fast, software I got stuck a few times, but with the wonderful help of +Peter van der  was all squared away and I am happily cutting.


---
*Imported from [Google+](https://plus.google.com/102361688882180483990/posts/TJv1VZWXHFN) &mdash; content and formatting may not be reliable*
