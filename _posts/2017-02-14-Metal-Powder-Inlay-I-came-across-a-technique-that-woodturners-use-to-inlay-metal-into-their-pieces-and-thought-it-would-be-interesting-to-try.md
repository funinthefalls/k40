---
layout: post
title: "Metal Powder Inlay - I came across a technique that woodturners use to inlay metal into their pieces and thought it would be interesting to try"
date: February 14, 2017 20:07
category: "Object produced with laser"
author: "Ned Hill"
---
Metal Powder Inlay -

I came across a technique that woodturners use to inlay metal into their pieces and thought it would be interesting to try.  Basically you fill an engraved area with powdered metal, flood the powder with very thin CA glue (super glue), let it harden and then sand it back.  Did this piece in pine with aluminium powder.  For only my second attempt (first was a big failure ;) it turned out pretty well.  Some small voids where the metal powder didn't settle into.  Also with the finer grit sandpaper it was creating dark metal dust that settled into the wood grain and wouldn't come out.  Most of that is hidden by the stain though.  Going to do some more test pieces and then I have some Brazilian walnut I want to try this on.  You can also get other powdered metal like brass, copper, or  bronze.  After I get it dialed in I'll do a complete write up.



![images/a40df41f8743e40e0eb57a920a7e110b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a40df41f8743e40e0eb57a920a7e110b.png)
![images/cd3c0f5ff29e793985b6f4bc375cd862.png](https://gitlab.com/funinthefalls/k40/raw/master/images/cd3c0f5ff29e793985b6f4bc375cd862.png)

**"Ned Hill"**

---
---
**Ariel Yahni (UniKpty)** *February 14, 2017 20:13*

I very much like that process


---
**Andy Shilling** *February 14, 2017 20:22*

Looks great, may I suggest trying with clear epoxy resin. You could mix the dust in to the resin before adding the activator, that way it might be easier to work out how much of each you need per mm3 to get results you require. 



Also maybe not lol I might be over thinking it.


---
**Ned Hill** *February 14, 2017 20:55*

**+Andy Shilling** using a metal powder/epoxy mixture is another way to do this, but you don't get as high of a metal density.  Just depends on the look you are going for I think.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 14, 2017 23:15*

That's really cool. Although, I assume your title should be "Powder" not "Power"?



I wonder if the void areas are a result of air bubbles in the glue or if tapping the metal dust down first (to have it fill its own voids) would have assisted?


---
**Ned Hill** *February 14, 2017 23:26*

Lol yes thanks **+Yuusuf Sallahuddin**, fixed that.  The void areas are not bubbles, just areas of clear glue.  The nice thing about the really thin glue is that it fills from the bottom up so eliminates the air bubbles as far as I can tell.  Had lots of voids and and a few bubbles on my first attempt.  It got a lot better on the second try when, after I flooded the powder with glue, I tapped the sides of the piece with a small wrench.  The powder settled noticeably because the liquid glue was allowing the metal pieces to move more readily.  Probably need to do a better job of tapping.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 02:24*

**+Ned Hill** I just had a crazy thought... What's conductivity like with this method? Would be interesting to be able to use this method to make circuit pathways.


---
**Ned Hill** *February 16, 2017 02:38*

**+Yuusuf Sallahuddin** that would actually be a cool idea, but I just check with my multimeter and I don't get any conductivity.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 02:53*

**+Ned Hill** Ah, I thought the glue may cause issues. That's unfortunate. I'm wondering if it would be possible to melt the metal powder & pour it into the engrave area. Although I have a feeling that the temperature required would be too high for the wood to handle without scorching.



Or, is there a bonding agent that provides electrical conductivity? No idea. But, ideas have to start somewhere :D


---
**Ned Hill** *February 16, 2017 03:11*

**+Yuusuf Sallahuddin** It would be possible to melt solder into an engraved area.  I did this superman logo using plumbing solder.  This was just melted straight off the spool and dripped in, but would probably be better to melt a portion in a crucible and pour.  Just burned the wood a bit and that came off when it was sanded down. 

 

I also had the thought of seeing if we could find something to make the glue conductive.  Will have to do a bit of research on that front.

![images/3a3da6a49faa4e9160c6d442cc7697f8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3a3da6a49faa4e9160c6d442cc7697f8.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 03:14*

**+Ned Hill** Ah yeah, I remember seeing that one. Solder is a nice option. I was considering (if I had a CNC already) that solid metal inlays would do the trick or even some ceramic tiles routed with the pattern (as you could pour metal into them without too much concern of heat).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 03:22*

**+Ned Hill** I found this: [https://www.jaycar.com.au/wire-glue-9ml/p/NM2831](https://www.jaycar.com.au/wire-glue-9ml/p/NM2831)



MSDS here: [https://www.jaycar.com.au/medias/sys_master/images/8928226246686/NM2831-msdsMain.pdf](https://www.jaycar.com.au/medias/sys_master/images/8928226246686/NM2831-msdsMain.pdf)



According to the MSDS it is an electrically conductive carbon adhesive.



[https://en.wikipedia.org/wiki/Electrically_conductive_adhesive](https://en.wikipedia.org/wiki/Electrically_conductive_adhesive)



According to that wiki article, the adhesive requires 80% conductive particles in order to work. So maybe in your case with the aluminium powder, your ratio of glue to powder threw off the possible conductivity.



Might be worth a try of mixing the powder into the glue before filling the engraved area?


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/5sygAU4XvMY) &mdash; content and formatting may not be reliable*
