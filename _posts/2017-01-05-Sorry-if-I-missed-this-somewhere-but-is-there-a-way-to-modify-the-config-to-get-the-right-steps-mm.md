---
layout: post
title: "Sorry if I missed this somewhere, but is there a way to modify the config to get the right steps/mm?"
date: January 05, 2017 21:38
category: "Smoothieboard Modification"
author: "Chris Sader"
---
Sorry if I missed this somewhere, but is there a way to modify the config to get the right steps/mm? I'm getting roughly 1/2 the expected distance in the X and Y axes.





**"Chris Sader"**

---
---
**Chris Sader** *January 05, 2017 21:47*

I'm assuming the Alpha and Beta steps/mm in the config file are the ones I need. I doubled them and it seems close, but now it's moving about 1mm too far. Is this where I should be tweaking this?



**+Don Kleinschnitz** I saw that yours was 2x what it should have been at 315.15...mine's now at 320 and seems almost right (minus that 1mm noted above)...does that sound right?


---
**Don Kleinschnitz Jr.** *January 05, 2017 22:03*

**+Chris Sader** I had to change mine to 157.575 ??? Weird! Something else must be different. We should compare config files.


---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:08*

157.75 for me


---
**Chris Sader** *January 05, 2017 22:11*

**+Don Kleinschnitz** **+Ariel Yahni** just got mine fine tuned to 100mm move via AZSMZ LCD = 100mm movement in real world and mine are now at 316.


---
**Chris Sader** *January 05, 2017 22:14*

Well crap, through Laswerweb, 100mm = 35mm


---
**Chris Sader** *January 05, 2017 22:16*

which would imply I need to be somewhere around 903 in config??




---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:17*

**+Chris Sader** what is your current setting?


---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:18*

it will also depend on the type of microsteping you are using and type of driver, so whatever get you moving the correct distance is ok based on the above


---
**Chris Sader** *January 05, 2017 22:25*

**+Ariel Yahni** **+Don Kleinschnitz** ignore my comment about 903 above...I'm an idiot and forgot to put the microsd back in after editing the config. 316 is getting me right at 100mm now.



Ariel, my pots are set at .41


---
**Ariel Yahni (UniKpty)** *January 05, 2017 22:27*

**+Chris Sader**  cool, the other i was reffering to the microsteping itself no the current. Anyway does not matter as you have it running at the correct distance


---
**Don Kleinschnitz Jr.** *January 05, 2017 23:21*

**+Chris Sader** dont feel bad last night I flailed after changing it only to find that I wrote the old file back to the SD card and tested it again .....


---
*Imported from [Google+](https://plus.google.com/107747838010116449754/posts/K4awhFEx2KL) &mdash; content and formatting may not be reliable*
