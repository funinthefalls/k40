---
layout: post
title: "Starting to figure out K40 Whisper"
date: July 17, 2017 03:31
category: "Object produced with laser"
author: "Kenneth White"
---
Starting to figure out K40 Whisper.



![images/49249cdf1bb5f72219a5d3010d181298.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/49249cdf1bb5f72219a5d3010d181298.jpeg)
![images/f763723f353db8453ae07d9e01bdbb66.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f763723f353db8453ae07d9e01bdbb66.jpeg)

**"Kenneth White"**

---
---
**Scorch Works** *July 17, 2017 03:56*

In the skull pic it looks like you had the halftone turned on because I can see the square around the image. If you turn off the halftone for images that are black and white (no grey) you should not see that. (Or make sure your background is pure white)


---
**Christopher Elmhorst** *July 17, 2017 04:56*

there's an option for half tone??




---
**Scorch Works** *July 17, 2017 05:11*

Yes, under 'Settings'-'Raster Settings' I don't have any good pictures to show yet.  I want to add settings to get the "dynamic range" right.  Here is a test I did with it. (the engraved image is about 1.75 inches tall)

![images/bfc27e8efde35d2843392d0c33232b06.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfc27e8efde35d2843392d0c33232b06.jpeg)


---
**Kenneth White** *July 17, 2017 05:18*

**+Scorch Works** Thank you! i didnt even realize hahaha,  Also the Raster settings  do work pretty well, The issues in my engraving were more due to the Photo in use, and learning curve to having a brand new laser cutter something ive never really used before.


---
**Anthony Bolgar** *July 17, 2017 12:03*

Thanks for figuring out the Nano protocols, I think you just made a lot of people very happy :)


---
*Imported from [Google+](https://plus.google.com/100692370397257622708/posts/2sbWjKomF2y) &mdash; content and formatting may not be reliable*
