---
layout: post
title: "Hey guys can anyone help im engraving an image but it engraves inverted i inverted it in coral but the machine goea haywire"
date: August 31, 2016 22:11
category: "Software"
author: "Aaron Priestley"
---
Hey guys can anyone help im engraving an image but it engraves inverted i inverted it in coral but the machine goea haywire





**"Aaron Priestley"**

---
---
**Alex Krause** *September 01, 2016 00:58*

Can you post a picture of the image?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 01, 2016 04:43*

There are two options in the CorelLaser engraving panel for Sunken (or not). One will cause it to engrave inverted. Check what that tickbox is set to (up the top centre if I recall correct).


---
**Aaron Priestley** *September 01, 2016 09:08*

Thank you i will give that a try


---
*Imported from [Google+](https://plus.google.com/105047873705840641419/posts/SRFRsALeHmd) &mdash; content and formatting may not be reliable*
