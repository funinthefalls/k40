---
layout: post
title: "Tie Fighter earrings! These turned out better than I expected"
date: November 03, 2016 03:43
category: "Object produced with laser"
author: "Jeff Johnson"
---
Tie Fighter earrings! These turned out better than I expected.  I used plexiglass from Home Depot that measures about 2.1mm thick. They're 21mm wide x 18.5mm high x 15mm deep.

![images/fb9ac4ffadcb4afbc596d479de7ae1fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb9ac4ffadcb4afbc596d479de7ae1fd.jpeg)



**"Jeff Johnson"**

---
---
**Anthony Bolgar** *November 03, 2016 03:53*

They look good. Nice job!


---
**Darren Steele** *November 03, 2016 09:14*

Very very cool :)


---
**Randy Randleson** *November 03, 2016 12:09*

awesome!




---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/Doo1DDRH8ms) &mdash; content and formatting may not be reliable*
