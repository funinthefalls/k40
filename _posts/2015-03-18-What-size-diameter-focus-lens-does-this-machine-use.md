---
layout: post
title: "What size diameter focus lens does this machine use?"
date: March 18, 2015 01:46
category: "Hardware and Laser settings"
author: "Sean Cherven"
---
What size diameter focus lens does this machine use?

Also, what type of focus lens do you recommend for cutting on this machine?





**"Sean Cherven"**

---
---
**Jim Coogan** *March 18, 2015 02:42*

The K40 comes with a 12mm diameter lens that has a 50.8mm focal length.  With that lens you can cut, engrave, and mark objects.  There are three lens that Light Objects carries for this unit.  They are the 38.1mm ZnSe lens which is optimal for engraving, especially on acrylic because of the low heat from the beam.  The next is the 50.8 mm ZnSe lens which is a compromise between the 38.1mm which is for engraving and the 63.5mm ZnSe lens which can cut up to 6mm of acrylic and 20mm of wood.  The 50.8mm works well for engraving and cutting.  But the other two work exceptionally well in their respective areas.  So the recommended one for cutting is 63.5mm.  Your current lens can do it too but not as well.  And you can switch between the three as long as you remember and have a little jig that  has marks for 38.1mm, 50.8mm, and 63.5mm.  That way you know how close you need to have your work so that the focal point is right where you want it.  And remember, sometimes its just as good to be further away and at times, a little closer. 


---
**Sean Cherven** *March 18, 2015 02:47*

Just the info I needed! Thanks!


---
**Sean Cherven** *March 18, 2015 02:49*

Okay, I looked on light objects, and they don't carry any 12mm lens.

Are you sure 12mm is the correct size?


---
**Jim Coogan** *March 18, 2015 03:23*

12mm is what the machines come with.  If you have the Light Objects air assist unit then it takes an 18mm lens. 


---
**Sean Cherven** *March 18, 2015 03:27*

Okay, gotcha! I'm planning on upgrading to a air assist sometime soon anyways, so i'll just order a lens along with it.


---
**Sean Cherven** *March 18, 2015 03:30*

One more question: The Focal Length... is that the distance the object should be away from the lens?



For example, with a 50.8mm Lens, should the object to be cut be 50.8mm away from the lens? Or am I totally wrong?


---
**Jim Coogan** *March 18, 2015 05:08*

Yes, that is how many of us look at it.   You can set up a piece of plywood so that the middle is that distance from the lens but keep one end up higher and one lower.  Then cut a line that is 2 inches to the left and 2 to the right of where 50.8 would be.  Don't move the wood and look for when the line becomes crisp.  On both side it will be a little less crisp.  The point at which is is crisp is what you should use on your projects as the focal point.  It might be at 50.8 and it might be a little different.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/AUHHwfKrUpK) &mdash; content and formatting may not be reliable*
