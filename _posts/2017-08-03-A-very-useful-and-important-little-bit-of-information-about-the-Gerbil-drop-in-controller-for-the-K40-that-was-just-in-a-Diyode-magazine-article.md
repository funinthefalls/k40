---
layout: post
title: "A very useful and important little bit of information about the Gerbil drop in controller for the K40 that was just in a Diyode magazine article"
date: August 03, 2017 19:34
category: "Discussion"
author: "Anthony Bolgar"
---
A very useful and important little bit of information about the Gerbil drop in controller for the K40 that was just in a Diyode magazine article.



Quoted from the article:



"I made sure that the PWM has a base PWM train signal, so as to keep the gas excited and energised at all times during the engraving. This prevents the latency in igniting the laser operation, and extends the lifespan of the tubes (not being switched fully on and off); all big laser manufacturers use this this energising trick. "



A nice bonus on top of the improved engraving features and the easy installation. Always great to extend the tube life, especially for guys like me that run at high mA (I consider the tube to be an expendable resource that I calculate the replacement of into my costs)





**"Anthony Bolgar"**

---


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/FCsEhLVFUZS) &mdash; content and formatting may not be reliable*
