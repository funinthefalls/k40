---
layout: post
title: "Hey everybody. Anyone can tell me where to get some 12x8 or 12x12 good quality wood suitable for Engraving in the US, preferably on the east coast?"
date: February 19, 2016 04:15
category: "Material suppliers"
author: "Ariel Yahni (UniKpty)"
---
Hey everybody.  Anyone can tell me where to get some  12x8 or 12x12 good quality wood suitable for Engraving in the US, preferably on the east coast? I've run out of testing material for testing. Have used cardboard, plywood, balsa, mdf,  jeans,  cookies. 





**"Ariel Yahni (UniKpty)"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2016 04:20*

Ah, so are jeans officially on the 'done' list?


---
**3D Laser** *February 19, 2016 04:26*

I use eBay i found a good seller for birch ply and acrylic I get a box of 40 12x12 birch for 50 dollars and 20 sheets of 12 X 12 acrylic (all different colors for about 90 


---
**Ariel Yahni (UniKpty)** *February 19, 2016 04:30*

**+Ray Kholodovsky**​ I would say so. You had something in mind for jeans? Maybe I'll save a spot for a chillipeppr logo


---
**Ray Kholodovsky (Cohesion3D)** *February 19, 2016 04:47*

**+Ariel Yahni** No I was just surprised since Peter only released the video in the last day and that was a quick turn around time on your part.  Unless of course you had done this before?  

CP would be nice.

This is the logo I'm interested in: 

[http://imgur.com/dIsKOJT](http://imgur.com/dIsKOJT)

I did it on my CNC earlier this week:

[https://goo.gl/photos/tSPjhJ6TcC6WmUUGA](https://goo.gl/photos/tSPjhJ6TcC6WmUUGA)
(It's for Cohesion3D which is the name of the 3D printers I build)

I can send you the dxf if you'd like. 


---
**Jim Hatch** *February 19, 2016 05:05*

I use Amazon, eBay and locally I've got a Woodcraft store (Google them, you may find one near you).


---
**Anderson Ta** *February 19, 2016 05:24*

You can get birch ply at your local Michael's...that way you can check it for flatness too. Depending where you are Woodcraft has brick and mortar locations too.


---
**Stephane Buisson** *February 19, 2016 10:48*

and also (often 3mm plywood in EU) :

[https://plus.google.com/117750252531506832328/posts/57ozsyxMQK9](https://plus.google.com/117750252531506832328/posts/57ozsyxMQK9) 


---
**Ariel Yahni (UniKpty)** *February 19, 2016 11:45*

**+Ray Kholodovsky**​ that jeans has a good couple of logos.  C3D looks very nice, are you selling those Allready?  


---
**Ariel Yahni (UniKpty)** *February 19, 2016 11:47*

I need to be able to purchase the ply online since I live outside the US. Even though my country has large amount of trees, all wood is imported but mostly mdf,  bad quality ply and chipboard


---
**Ben Walker** *February 19, 2016 12:23*

Woodpeckers on Amazon is on east coast (jersey) and ships fast.  Best price I found on Baltic birch.   Mine came out to about $1.06 per 12x12 1/8 board.  Not a single knot so far.


---
**Scott Thorne** *February 19, 2016 14:07*

I use Amazon...fast shipping...get my wood and acrylic from there.


---
**Ariel Yahni (UniKpty)** *February 19, 2016 14:12*

**+Scott Thorne**​ any seller in particular? 


---
**Scott Thorne** *February 19, 2016 14:49*

**+Ariel Yahni**...kencraft co.....that's where I get my wood from....with the acrylic...I just find the cheapest and quickest on Amazon.


---
**Jim Hatch** *February 19, 2016 23:47*

**+Scott Thorne**​ Ditto. Amazon shipping is superfast to get this stuff. 



Quick reminder to make sure the acrylic is "cast acrylic" as it's laser safe while other common formulations of "acrylic" can be lexan or plexi and toxic to you & the machine. Big box stores often don't know the difference.


---
**Scott Thorne** *February 20, 2016 00:04*

You're right...I forgot to mention that.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/UeCTbve5Tti) &mdash; content and formatting may not be reliable*
