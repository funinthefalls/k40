---
layout: post
title: "So we just got a new machine, our other one caught fire =(.."
date: May 02, 2015 21:30
category: "External links&#x3a; Blog, forum, etc"
author: "Dingo D"
---
So we just got a new machine, our other one caught fire =(.. it came with the mirrors sooo mis-aligned that it burnt a hole in my shirt when I tested it! now for the past 3 days I have been trying to align the mirrors and I just cant get it to cut! I swear it hits in the middle. I run a test, and after 4 to 5 goes, it isnt even cutting through!!  Are there any easy solutions to lining the mirrors? Also..I read about focusing the fixed mirror!? Any help is much appreciated..thank you!!





**"Dingo D"**

---
---
**Dan Shepherd** *May 03, 2015 02:02*

Sorry to hear that your having so much trouble getting the mirrors aligned, I had the same problem.  They have detailed instructions and video's in the files section of the facebook users group Laser Engraving and Cutting

[https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/) 


---
**Tim Fawcett** *May 03, 2015 20:44*

Here are some instructions I did for aligning the beam:

[http://www.floatingwombat.me.uk/wordpress/?attachment_id=30](http://www.floatingwombat.me.uk/wordpress/?attachment_id=30)


---
**Dingo D** *May 04, 2015 18:56*

Thank you very much!! Found what the real problem was...Our water wasnt cooling enough! Felt the water and it was warm! Got some cooling devices to help keep the laser cool. SOO many factors to this thing! =) Now that we keep the laser nice and cool, it seems to be cutting well again.


---
*Imported from [Google+](https://plus.google.com/+DingoD/posts/KXAUBQ8VqJz) &mdash; content and formatting may not be reliable*
