---
layout: post
title: "Anyone \"With Knowledge of This\" please just answer a simple question"
date: February 27, 2016 04:00
category: "Hardware and Laser settings"
author: "Joseph Midjette Sr"
---
Anyone "With Knowledge of This" please just answer a simple question.



I have attached pictures of my board; it has a port which I am assuming it is for an addition of another stepper? Not really sure but it makes me think I could use it for a rotatory?



Anyhow, that's not what I want to use it for, I want to pull the 5vdc from it and use it for my 5vdc crosshair indicator laser. I want to run a plug from it to a switch so I can pinpoint where my start is before I burn, cut, etc.



Can anyone elaborate on this port, (in the close-up pic with the port on the side), and also the other picture with the whole board showing a port on the bottom, what is the port on the bottom? I can take the whole board off but hope someone just has experience with this... Can I utilize this port for maybe some led lighting?



My main thing is, I know this is my controller board; will it hurt to use this for what I am intending? I mean, the ports are there, why not utilize them???



Also, I am going to send another question about the crappy analog "Current Indicator", so heads up....







![images/3d17f60dc68cb4f0ba144f771f381611.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d17f60dc68cb4f0ba144f771f381611.jpeg)
![images/a0e81665ab9bbe19df36e53ce1bf518c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e81665ab9bbe19df36e53ce1bf518c.jpeg)

**"Joseph Midjette Sr"**

---
---
**Tony Sobczak** *February 27, 2016 06:10*

Good question. I'd like to know too. 


---
**David Richards (djrm)** *February 27, 2016 07:24*

Greetings Joseph, Tony, I am using the 5v output from that connector to power my two laser crossed line markers. The power is fed through a toggle switch on the front panel, they don't draw much current.



I have assumed the two unused sockets are duplicates of the signals used in the ribbon cable connector. The connector next to the ribbon for stepper motor and the one you are referring to having the x and y end stop inputs. I doubt you would be able to use these for anything else.



Hth David.﻿


---
**Stephane Buisson** *February 27, 2016 07:37*

**+Joseph Midjette Sr** your chance to do anything with that proprietary board are low, or you will need heavy reverse enginering, it was easy we should have found a way already,**+david richards** is right.


---
**Jim Hatch** *February 27, 2016 14:31*

I use the 5V for my inbox LED lighting strips. (Yeah I know it would have been easier to replace the stock amber acrylic window on the lid but the LEDs light up the interior very nicely :-)


---
**Trực Chính** *February 27, 2016 21:27*

5v line has filter FS1 so you are safety to use that line.


---
**The Technology Channel** *February 28, 2016 14:11*

Its a Fuse not a filter.


---
*Imported from [Google+](https://plus.google.com/104424492303580972567/posts/i5sueP4gp3e) &mdash; content and formatting may not be reliable*
