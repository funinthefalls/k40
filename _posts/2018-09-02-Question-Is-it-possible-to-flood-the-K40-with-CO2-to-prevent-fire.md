---
layout: post
title: "Question: Is it possible to flood the K40 with CO2 to prevent fire?"
date: September 02, 2018 20:28
category: "Discussion"
author: "Rene Munsch"
---
Question: Is it possible to flood the K40 with CO2 to prevent fire? Or will the gas also prevent the Laser to cut? ;-)





**"Rene Munsch"**

---
---
**HalfNormal** *September 02, 2018 21:01*

Two problems, first you need airflow to remove the smoke and debris. Second, it would take a lot of inert gas to fill the cutting area which would be pulled out through the ventilation to remove the smoke and debris. A fire extinguisher and paying attention is the best prevention.


---
*Imported from [Google+](https://plus.google.com/111325559173207527657/posts/Qd5AHrWwqAs) &mdash; content and formatting may not be reliable*
