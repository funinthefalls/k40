---
layout: post
title: "Hi guys ! How often do you have to check the alignment ?"
date: May 14, 2018 12:33
category: "Hardware and Laser settings"
author: "Manon Joliton"
---
Hi guys !

How often do you have to check the alignment ?

I checked it a month ago and it's already bad aligned ... is it normal ? Do you have any advice to keep the alignment right ?

Thank you :3





**"Manon Joliton"**

---
---
**Ned Hill** *May 14, 2018 15:42*

I probably check about every month or so and occasionally see some drift.  The best thing to do to keep the alignment from drifting too much is to make sure sure all the mirror mounts and lens holder are tightened down good.


---
**Manon Joliton** *May 14, 2018 15:59*

**+Ned Hill** ok well I guess I'll check this monthly then, what other thing do you use to do commonly to make sure the machine will keep working ?


---
**Ned Hill** *May 14, 2018 17:06*

It all comes down to how often you use the laser.  The only thing I really try and do monthly is to change out the cooling water (use distilled water only).  You will also need to clean the lens and mirrors periodically.  If you have an air assist you can probably get away with cleaning the lens every other week, but everyday you use it if you don't have air assist.  Mirrors I typically check and clean monthly, more often with heavy use.  


---
**Manon Joliton** *May 14, 2018 18:01*

**+Ned Hill** ho i didn't know that it needed so much care !

As I use my laser almost everyday i should probably take care of it every time then :3

Thank you :D


---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/Syu9raaqQPM) &mdash; content and formatting may not be reliable*
