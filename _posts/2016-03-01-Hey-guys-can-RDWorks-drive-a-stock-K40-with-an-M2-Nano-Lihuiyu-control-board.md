---
layout: post
title: "Hey guys, can RDWorks drive a stock K40 with an M2 Nano Lihuiyu control board?"
date: March 01, 2016 18:03
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Hey guys, can RDWorks drive a stock K40 with an M2 Nano  Lihuiyu control board? And if not, what would I need to do (other than ripping all the electronics out and putting in a Ramps, AWC, or DSP, or .... ?)





**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Thorne** *March 02, 2016 10:35*

Rd works only works with ruida controllers....won't work with the k40.


---
**Ashley M. Kirchner [Norym]** *March 02, 2016 17:09*

RDWorks work with <b>anything</b> else at all? Is it proprietary to those controllers?


---
**Scott Thorne** *March 02, 2016 17:41*

Yes it is. ...ruida controllers are the only ones that I know of. ....I have one in my 50 watt....rdworks laserworks is my software.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/2pMdweZPHMv) &mdash; content and formatting may not be reliable*
