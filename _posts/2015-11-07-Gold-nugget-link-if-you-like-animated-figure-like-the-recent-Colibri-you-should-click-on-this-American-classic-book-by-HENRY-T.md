---
layout: post
title: "Gold nugget link if you like animated figure like the recent Colibri, you should click on this American classic book by HENRY T"
date: November 07, 2015 15:21
category: "Repository and designs"
author: "Stephane Buisson"
---
Gold nugget link



if you like animated figure like the recent Colibri, you should click on this American classic book by HENRY T. BROWN

[http://507movements.com/about.html](http://507movements.com/about.html)



Imagine what you can do with Lasercut/3DPrint/CNC 

We are living in a fantastic digital time for makers ;-))

![images/e82146582f4af3bd07b346d9abd24f1f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e82146582f4af3bd07b346d9abd24f1f.jpeg)



**"Stephane Buisson"**

---
---
**Ashley M. Kirchner [Norym]** *November 08, 2015 04:48*

I also like this guy's channel and use it a lot for inspiration. You can also download PDFs containing the majority of what he creates.


---
**Stephane Buisson** *November 08, 2015 09:35*

sorry **+Ashley M. Kirchner**, do I miss a link or the guy's name ?



edit: found it

[https://www.youtube.com/channel/UCPd0_nhe5UkxPRHofK6eUug](https://www.youtube.com/channel/UCPd0_nhe5UkxPRHofK6eUug)


---
**Stephane Buisson** *November 08, 2015 10:54*

and that one 
{% include youtubePlayer.html id="playlist" %}
[https://www.youtube.com/playlist?list=PL2DBFF0A09C41DEEA](https://www.youtube.com/playlist?list=PL2DBFF0A09C41DEEA)


---
**Ashley M. Kirchner [Norym]** *November 08, 2015 13:37*

Sorry, I was exhausted when I got home last night. The link I meant to post was this one: [https://www.youtube.com/channel/UCli_RJkGWfZvw4IlDLHNCQg](https://www.youtube.com/channel/UCli_RJkGWfZvw4IlDLHNCQg)


---
**Stephane Buisson** *November 08, 2015 13:53*

**+Ashley M. Kirchner** nice indeed, thank you


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/6YwXVVyzLRz) &mdash; content and formatting may not be reliable*
