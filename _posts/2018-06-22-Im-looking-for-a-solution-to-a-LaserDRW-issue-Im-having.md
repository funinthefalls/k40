---
layout: post
title: "I'm looking for a solution to a LaserDRW issue I'm having"
date: June 22, 2018 17:36
category: "Software"
author: "James Lilly"
---
I'm looking for a solution to a LaserDRW issue I'm having.  When I vector cut, my cutter traces/cuts a shape clockwise and when it finishes, it repeats with a second cut counter-clockwise. That wouldn't be that big of deal, but it appears that the second cut is slightly outside the first, like one pixel, just enough to be annoying and make the cut far less cleaner than it could be.   Ideas?





**"James Lilly"**

---
---
**HalfNormal** *June 23, 2018 19:51*

What software are you doing your graphics in? This problem is usually due to double lines being present.


---
**James Lilly** *June 23, 2018 22:50*

Adobe Illustrator.  


---
**Justin Mitchell** *June 25, 2018 14:48*

I you are using laserdrw then do not feed it lines to cut, only solid shapes, otherwise you are likely to have it trace the outline of the lines, which is what you described here.  It is extremely fussy about what would count as a hairline so it is safer just to feed it solids, with holes/shapes punched out where needed.  it will then trace the outline of that shape only once. Like this:

![images/447884e58e19e1737c4933af755ee181.png](https://gitlab.com/funinthefalls/k40/raw/master/images/447884e58e19e1737c4933af755ee181.png)


---
**James Lilly** *June 26, 2018 02:32*

Ah...make sense.  I'll give it a try


---
**James Lilly** *June 28, 2018 06:12*

Thanks Justin! Worked pretty well for LaserDRW.  I thought I'd post a  few screen shots of what I did. Created first on Illustrator, next placed into Inkscape.On the left is the left is the cutting file on the right is the engraving. 

![images/74d110219609709d6ce9a41e728ed6db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74d110219609709d6ce9a41e728ed6db.jpeg)


---
**James Lilly** *June 28, 2018 06:14*

What is did next is opened up one of the two Inkscape files in LaserDRW and copy the graphic.  Next I opened the other file in laserDRW and simply pasted it in.

![images/677b2a1d70ed09c7d4df6ae683c019cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/677b2a1d70ed09c7d4df6ae683c019cd.jpeg)


---
**James Lilly** *June 28, 2018 06:18*

The files were registered in Illustrator and Inkscape as well, but I find that they are a bit off, thus I pasted "layers" into Laserdrw.  You can see that I've aligned them manually. Next, I cut the outline, or black shapes. To engrave, simply delete the cutting graphic and the engraving graphic is still registered.

![images/cac88973406da1e708603a5f4a56065b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cac88973406da1e708603a5f4a56065b.jpeg)


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/JJGTSJVFCou) &mdash; content and formatting may not be reliable*
