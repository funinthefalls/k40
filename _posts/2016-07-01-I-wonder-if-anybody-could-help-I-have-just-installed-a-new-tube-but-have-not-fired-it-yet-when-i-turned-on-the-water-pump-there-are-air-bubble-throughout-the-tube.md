---
layout: post
title: "I wonder if anybody could help? I have just installed a new tube but have not fired it yet, when i turned on the water pump there are air bubble throughout the tube"
date: July 01, 2016 12:05
category: "Discussion"
author: "george ellison"
---
I wonder if anybody could help? I have just installed a new tube but have not fired it yet, when i turned on the water pump there are air bubble throughout the tube. Some at the right hand side and a big one on the left end, how to I get rid of these or does it not matter, thanks





**"george ellison"**

---
---
**Julia Longtin** *July 01, 2016 12:44*

it DEFINATELY matters.


---
**Jim Hatch** *July 01, 2016 12:55*

Easiest way is to get your water reservoir above the tube and let gravity do its thing. Let the pump run for a day and the bubble should be gone. If a bubble gets trapped in some corner of the tube you may need to tilt/tip it so you can get it to move to the top and out the tubing into the reservoir. Not hard, just a bit tedious.


---
**Don Kleinschnitz Jr.** *July 01, 2016 13:13*

When I get this I have to tilt my machine at a 45 and rotate it. i.e dont be afraid to lift the machine ...


---
**george ellison** *July 02, 2016 08:35*

I will try lifting the pump above the tube for a while, then when the bubbles go should it be placed underneath/below the tube?


---
**Jim Hatch** *July 02, 2016 14:44*

Once the bubbles are gone you can put the bucket & pump most anywhere as long as you keep the outlet tube under water. Otherwise air can get back in. 


---
**Ben Marshall** *July 05, 2016 02:06*

I stand mine up (water out at the top) and the bubbles are gone in seconds


---
**george ellison** *July 05, 2016 08:47*

So with the tube at the top and the opening to the door at the bottom? Will try this, just need to get some new de-ionised water first. Thanks


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/bFXm3mdh2MH) &mdash; content and formatting may not be reliable*
