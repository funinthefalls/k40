---
layout: post
title: "Anyone know how i can get a copy of corel laser draw?"
date: January 03, 2017 01:11
category: "Software"
author: "John Austin"
---
Anyone know how i can get a copy of corel laser draw? i reformatted my computer and try to reinstall it and can not get it to work.



Is there any other software that will work with a stock k40?





**"John Austin"**

---
---
**Alex Krause** *January 03, 2017 02:44*

You will need to install Corel first then install the Corel laser plugin I think **+Yuusuf Sallahuddin**​ has a Dropbox link for it somewhere


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 03, 2017 08:29*

I've got a link for CorelLaser 2013.02 plugin ([http://www.lcshenhuilaser.com/download.html](http://www.lcshenhuilaser.com/download.html)) & Corel X7 on OneDrive ([https://1drv.ms/f/s!AqT6a-6Vl15Nh7lqpg7o6Ap5MGg1aQ](https://1drv.ms/f/s!AqT6a-6Vl15Nh7lqpg7o6Ap5MGg1aQ)).


---
**Matthew Wilson** *January 15, 2017 19:44*

When I downloaded the program, it was for a 30 day trial. I have not connected the laser as I am just learning to make designs on it first, but it is a ton better than laserdraw. Which coreldraw suite do I need to purchase so I can use it with the k40 past the 30 day window? Thanks in advance!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 15, 2017 21:38*

**+Matthew Wilson** Any version of CorelDraw that is not Home & Student up to x8 seems to work from all reports here. For whatever reason, home & student versions don't work. Have a read back through past posts for further verification. However, for probably cheaper than the price of the CorelDraw suite, you can purchase a replacement controller board (Arduino?, Smoothie, Cohesion3D, etc) & utilise the open-source LaserWeb software (which is fantastic).


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/Edy6DPmZ6Ft) &mdash; content and formatting may not be reliable*
