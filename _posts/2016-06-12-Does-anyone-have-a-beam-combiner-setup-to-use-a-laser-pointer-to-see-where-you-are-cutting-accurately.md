---
layout: post
title: "Does anyone have a beam combiner setup to use a laser pointer to see where you are cutting accurately"
date: June 12, 2016 06:44
category: "Modification"
author: "Eric Rihm"
---
Does anyone have a beam combiner setup to use a laser pointer to see where you are cutting accurately. Would make focusing mirrors and finding focal easy too.





**"Eric Rihm"**

---
---
**Anthony Bolgar** *June 12, 2016 06:53*

I think someone hadf tried that last year but found there was not enough room for the combiner. An alternative for focusing is to make a laser dot holder that slips over the cutting head and sends the beam backwards through the system.


---
**Don Kleinschnitz Jr.** *June 12, 2016 12:20*

I have seen it in other forums but the problem is that it adds more complexity and robs power from already marginally powered systems.

Not the least of which they are expensive compared to the cost of the unit ...



[http://www.ebay.com/itm/CO2-laser-machine-beam-combiner-set-includes-lens-and-red-dot-/262464412482?hash=item3d1c191b42:g:QTAAAOSwmtJXT1Wd](http://www.ebay.com/itm/CO2-laser-machine-beam-combiner-set-includes-lens-and-red-dot-/262464412482?hash=item3d1c191b42:g:QTAAAOSwmtJXT1Wd)



Here is a mirror that theoretically could be used as a combiner with DIY mounts. [https://www.amazon.com/1064nm-Combiner-Marking-Cutting-Engraving/dp/B00VY11GQQ/ref=sr_1_1?ie=UTF8&qid=1465733687&sr=8-1&keywords=beam+combiner](https://www.amazon.com/1064nm-Combiner-Marking-Cutting-Engraving/dp/B00VY11GQQ/ref=sr_1_1?ie=UTF8&qid=1465733687&sr=8-1&keywords=beam+combiner)



I have questioned the need for this sophistication in my system vs the added cost and optical complexity. I set my angular dot at the right focal point and that seems to work for me.


---
**Vince Lee** *June 12, 2016 17:56*

The most elegant solution I have seen is to mount two laser line indicators at right angles on the head so that their intersection is on target no matter the depth.  Before I heard of this setup I made my own indicator using a laser diode mounted on an angular actuator that swings it down into the beam path when enabled.  It was hard to get aligned at first but now it aides in mirror alignment too.  It is on a 2 way keyswitch that enables either it or the cutting laser.


---
**Eric Rihm** *June 12, 2016 18:48*

I just saw that as well and thought the line lasers were a really good idea. I was considering buying the beam combiner because I'm planning on getting a red china 60w really 50w machine to upgrade in a few months.


---
**Anthony Bolgar** *June 12, 2016 19:39*

I have used a 3D printed air assist nozzle that had dual mounts built in for 12mm line lasers. It worked great for determining the laser beam location, and of course depth does not matter when using dual lasers. I have been modifying the stl file to work with a LightObjects air assist head just keeping the laser mount portion of the stl and getting rid of the air assist portion.


---
**Don Kleinschnitz Jr.** *June 12, 2016 20:37*

[http://www.thingiverse.com/thing:1063067](http://www.thingiverse.com/thing:1063067)


---
**Anthony Bolgar** *June 12, 2016 22:32*

That is the one I am attempting to modify.


---
**Don Kleinschnitz Jr.** *June 13, 2016 01:12*

Here is one laser cut. If this link works



[https://scontent-lax3-1.xx.fbcdn.net/v/t1.0-9/13450171_1080999525292275_1324006172932164553_n.jpg?oh=d22ffe74ea436deef67b610933a8d40d&oe=580CE59A](https://scontent-lax3-1.xx.fbcdn.net/v/t1.0-9/13450171_1080999525292275_1324006172932164553_n.jpg?oh=d22ffe74ea436deef67b610933a8d40d&oe=580CE59A)


---
**Pippins McGee** *June 13, 2016 11:44*

**+Don Kleinschnitz** do you have vector file for that?


---
**Don Kleinschnitz Jr.** *June 13, 2016 12:53*

Sorry I don't, it was posted on the FB "Laser Cutting and Engraving" forum so the author +Rick Gonzalaz but I do not see any files yet.


---
**Anthony Bolgar** *June 13, 2016 13:23*

I have the files for one that is almost identical, if not the same one. I will post them to google drive today and post the download link.


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/BjXbrsKQxkW) &mdash; content and formatting may not be reliable*
