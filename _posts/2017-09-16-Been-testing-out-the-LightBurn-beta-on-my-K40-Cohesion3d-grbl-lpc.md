---
layout: post
title: "Been testing out the LightBurn beta on my K40 Cohesion3d/grbl-lpc"
date: September 16, 2017 21:45
category: "Object produced with laser"
author: "Jim Fong"
---
Been testing out the LightBurn beta on my K40 Cohesion3d/grbl-lpc.  I'm more than happy with these initial laser raster scans.   New features and bug fixes are happening really quick.  



![images/a31cdff6e2e4fc83065fb31efd1ae35b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a31cdff6e2e4fc83065fb31efd1ae35b.jpeg)
![images/b8756d1e44671e2cbcaab820307f73dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8756d1e44671e2cbcaab820307f73dc.jpeg)
![images/3dfdc565293dc84ef38b4e6d54f6baf0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3dfdc565293dc84ef38b4e6d54f6baf0.jpeg)
![images/a231f490f0e9cd01c743d5f7bb691e74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a231f490f0e9cd01c743d5f7bb691e74.jpeg)
![images/fcf8e0c342413f37b1c6b8f32b69815e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fcf8e0c342413f37b1c6b8f32b69815e.jpeg)
![images/c8bef7ac48d53bfc3bbce5a91f0291d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c8bef7ac48d53bfc3bbce5a91f0291d4.jpeg)

**"Jim Fong"**

---
---
**Anthony Bolgar** *September 16, 2017 23:55*

I will be starting the documentation and tutorials next week. Just need to finish learning all the features myself.


---
**Jason Dorie** *September 17, 2017 00:25*

Remember if you have any questions about how something works you can ask me.  I might even know the answer.  ;)


---
**Anthony Bolgar** *September 17, 2017 00:27*

Of course you would know the answer, you are Canadian, remember we actually teach our kids something in school ;)




---
**Tony Sobczak** *September 17, 2017 19:23*

Following


---
**Jason Dorie** *September 17, 2017 19:25*

For anyone interested, you can follow the LightBurnLaser page on Facebook, or follow me on YouTube - I post regular updates on both.


---
**Kelly Burns** *September 19, 2017 11:34*

Looks great.  


---
**Kelly Burns** *September 19, 2017 11:36*

Nice work **+Jason Dorie**    Anxious for the opportunity to try it out.   


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/h7586j8EVGj) &mdash; content and formatting may not be reliable*
