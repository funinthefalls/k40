---
layout: post
title: "1 old pc power supply + two 120mm server fans from a computer junk shop = $10 and a heckuva upgrade from the stock exhaust system"
date: July 31, 2018 03:41
category: "Modification"
author: "S Th"
---
1 old pc power supply + two 120mm server fans from a computer junk shop = $10 and a heckuva upgrade from the stock exhaust system. 



 It’s not as quiet as the bilge fans that a lot of people use, and it’s kinda funky looking because of my space restrictions, but it moves a lot of air and it was really cheap!

![images/129cb16be2df382c910249449efc397d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/129cb16be2df382c910249449efc397d.jpeg)



**"S Th"**

---
---
**Don Kleinschnitz Jr.** *July 31, 2018 11:26*

😀


---
**Jean-Phi Clerc** *August 07, 2018 15:22*

perhaps a little guide for construct it ? thanks !


---
**S Th** *August 07, 2018 16:54*

**+Jean-Phi Clerc** unfortunately, I don't have any other pictures of the build. It is pretty specific to my personal setup, so I'm not sure how well it would apply to other users. 



It was based on this design ([https://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/264830-new-diy-exhaust-k40-co2-laser-cutter.html](https://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/264830-new-diy-exhaust-k40-co2-laser-cutter.html)) and I modified it pretty heavily using OnShape. I use OnShape because I am a student and can get free access to that software, but I doubt there are many others here who use it. It does have a really good tool to add the finger joints and it's completely cloud based which is pretty convenient for me.



But if you have any specific questions I'd be happy to help.


---
**Jean-Phi Clerc** *August 08, 2018 04:14*

**+S Th** thanks you for explanations,and

 the link source,  yep good tool onshape, your work give me some new ideas :-)


---
*Imported from [Google+](https://plus.google.com/116589695629402182526/posts/TpA9SHNJnvN) &mdash; content and formatting may not be reliable*
