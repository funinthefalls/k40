---
layout: post
title: "Acrylic \"nixie tubes\"! A great article on one design and it has the files so you can make them yourself"
date: January 15, 2017 19:18
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Acrylic "nixie tubes"!



A great article on one design and it has the files so you can make them yourself.

[http://hackaday.com/2017/01/14/smoothly-modernized-nixie-display/#comments](http://hackaday.com/2017/01/14/smoothly-modernized-nixie-display/#comments)

Files found here; [https://github.com/connornishijima/Lixie-hardware](https://github.com/connornishijima/Lixie-hardware)





**"HalfNormal"**

---
---
**Bill Keeter** *January 16, 2017 02:05*

Love this. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/ceRfSm77TSN) &mdash; content and formatting may not be reliable*
