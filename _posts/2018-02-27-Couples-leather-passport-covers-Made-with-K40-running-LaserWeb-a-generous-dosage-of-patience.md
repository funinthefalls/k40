---
layout: post
title: "Couples leather passport covers. Made with K40 running LaserWeb, & a generous dosage of patience"
date: February 27, 2018 03:49
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Couples leather passport covers. Made with K40 running LaserWeb, & a generous dosage of patience.



Thanks to **+Ariel Yahni** for sharing his template for size reference (also borrowed the extra pocket idea). Very happy with the end result.



Cowhide leather, 2 coats of red dye, 1 coat of Seal & Shine, 2 coats of Edgecoat. Tested colour fastness (by wetting with leather conditioner). Nothing rubs off onto cloth, so seems all is good.



![images/e0c99dab2fead98bb4774115f287ff08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0c99dab2fead98bb4774115f287ff08.jpeg)
![images/f8f11a5adaa807b2a495dbe16fac83d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8f11a5adaa807b2a495dbe16fac83d6.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ned Hill** *February 27, 2018 13:47*

Beautiful work!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 27, 2018 14:40*

**+Ned Hill** Thanks Ned :)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Xa3urrYhLfu) &mdash; content and formatting may not be reliable*
