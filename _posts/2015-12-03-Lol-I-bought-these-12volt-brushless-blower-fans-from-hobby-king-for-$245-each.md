---
layout: post
title: "Lol I bought these 12volt brushless blower fans from hobby king for $2.45 each"
date: December 03, 2015 20:40
category: "Modification"
author: "David Cook"
---
Lol I bought these 12volt brushless blower fans from hobby king for $2.45 each.  Figured I would try an experiment to see if it could work for the air assist. One fan had a ton of airflow so two should be better :-) lol.  



![images/13063d14b387425b5c811e81935d7647.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/13063d14b387425b5c811e81935d7647.jpeg)
![images/42b5b4cc4ab7de1d10de206e4484127f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42b5b4cc4ab7de1d10de206e4484127f.jpeg)
![images/aa3a8ba50cf186218797d590e197847a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa3a8ba50cf186218797d590e197847a.jpeg)
![images/d00fc958238270276affd1d69eb168ca.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d00fc958238270276affd1d69eb168ca.jpeg)
![images/a2308720b7e207d9fa8aee8501eac894.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2308720b7e207d9fa8aee8501eac894.jpeg)
![images/4794ef7bd6e40d09a784345e821e8b6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4794ef7bd6e40d09a784345e821e8b6c.jpeg)

**"David Cook"**

---
---
**ChiRag Chaudhari** *December 03, 2015 21:37*

Looking pretty awesome thats for sure. I am also planning not to use Air Compressor, Instead hook up my shop vac. It has pretty good airflow. Or even Leaf blower may work . Just a bit loud!


---
**David Cook** *December 03, 2015 21:38*

**+Chirag Chaudhari** the flexible stands came with the blower fans to!  Was a great deal they had.  I hope it has enough pressure to make it thru the air line. 


---
**ChiRag Chaudhari** *December 03, 2015 21:40*

Oh wow really. Sounds like pretty good deal. How about a link !


---
**David Cook** *December 03, 2015 22:01*

**+Chirag Chaudhari**    well I bought them when they were on sale after thanksgiving holiday,  they are $12 now   [https://www.hobbyking.com/hobbyking/store/uh_viewItem.asp?idProduct=54252](https://www.hobbyking.com/hobbyking/store/uh_viewItem.asp?idProduct=54252)


---
**ChiRag Chaudhari** *December 03, 2015 22:05*

Ah OK. Thanks.


---
**David Cook** *December 07, 2015 16:48*

As expected. Not enough air pressure to make it thru the airline.  Its enough flow to keep smoke and soot off of the lens but that's it. Time for a compressor. 


---
**Scott Marshall** *December 18, 2015 18:19*

A small, (maybe 50mm)  ducted fan from HK, with a ESC (speed control) varied using a cheap servo tester makes a powerful air blower, and generates quite a lot of air pressure for a fan. It might do the job. I've used them for a variety of industrial make-do solutions. A cheap fan, esc and servo tester can be had for around 20 bucks if you shop ebay carefully.

On the loud side though, but most air compressors are.

Love my shop air, silent and unlimited (well almost)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/RMwWDw6Lu4Y) &mdash; content and formatting may not be reliable*
