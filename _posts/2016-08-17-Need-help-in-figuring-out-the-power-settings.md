---
layout: post
title: "Need help in figuring out the power settings"
date: August 17, 2016 21:32
category: "Hardware and Laser settings"
author: "VetGifts By G"
---
Need help in figuring out the power settings. So far I haven't went beyond 5 on the power knob(mA gauge haven't moved above 2. All I have done was engrave wood and ceramic tile so far, and I am almost happy with the results. I could go darker by maybe going up on the dial, but is that going to affect the depth of the engraving? I keep seeing people recommend 35% or 60% power for something. How do I translate that onto the K40 dial and mA gauge?





**"VetGifts By G"**

---
---
**Jim Hatch** *August 17, 2016 21:47*

The K40 should be kept to 18ma or lower since it's really about a 32W tube. Any higher and you're shortening the life of the tube. 



If you treat 20ma as = 100% then 50% = 10ma, etc. You'll not likely to get it to lase below 4ma (or 25%). 



More sophisticated laser control systems allow you to specify the power you want for a cut or engrave (usually by line color in your design file) but the software we have doesn't so you have to use to dial. Keep a log of what power settings work for different jobs on your laser and you'll be able to get pretty close on new designs the first time. You can make a reference template of power & speed but because you can only specify them by the job it's incredibly tedious to do.


---
**Alex Krause** *August 17, 2016 21:58*

Just because the mA guage only says you are driving at 2 you should really use the test fire button and let the guage level out what you are actually driving the tube at... ma guage will almost always read lower while engraving because it's turning off and on faster than the guage can read


---
**Jim Hatch** *August 17, 2016 22:01*

And to really complicate things - the effective power of most lasers isn't linear so 100% is not going to cut or engrave twice as deep or fast as 50% but it's a good enough measure for our purposes. 🙂


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 04:54*

Depending what you want to engrave/cut the power levels will vary. I highly suggest just testing a bunch of 10mm squares on a scrap of your material you intend to work with & using different variations of power/speeds to see what results you get. Then keep that as a reference chart for future use of the same material. Also, you will find that some materials when hit with higher powers will engrave deeper, although some the difference will be barely noticeable (e.g. merbau hardwood @ 10mA @ 350mm/s engrave looks only 0.5mm deeper than the same @ 4mA @ 350mm/s).


---
**Robi Akerley-McKee** *August 18, 2016 05:50*

I fried a K40 tube running 15mA for 30 minutes straight.  Plated the inside of the tube a nice pink, and then wouldn't lase.  I haven't replaced it yet, since I have a second machine.  1st machine I used the Moshidraw board for 3 weeks, until a grbl board I ordered came in.  Tried to use that for 2 weeks, until i gave up on it and used a Arduino and RAMPS 1.4 I got for a Reprap.  That worked a treat.  I've used the machine for about 6 months when I fried the tube right after I got computer control of the air assist and the water pump and exhaust fan.  I had a MKS Gen v1.4 (arduino 2560/RAMPS 1.4 all in one board) which I then put into the 2nd machine.  I just ordered a MKS Sbase board to try smoothieware.  I've had PWM power control of the laser power since the GRBL board.  It's really nice to be able to vector cut, vector engrave and raster engrave.


---
**VetGifts By G** *August 18, 2016 13:21*

I like the idea to make a scrap board with samples and label the settings. I have been marking the dial with a pencil and writing the settings and outcome, but it's getting too much. I also didn't realize testing the laser is to figure out the mA. Thanks for the answers, it starts to make sense now.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 18, 2016 20:59*

**+Miro G** Testi Laser button will also be useful for determining where it is going to fire or to help with alignment of the mirrors.


---
**Scott Pollmann** *August 20, 2016 00:02*

**+Alex Krause**  out of curiosity.  I've got a New laser. I can turn my power pot to full and still only hit about 10mA.  This seems low to me.  To the point that I am thinking my pot isn't letting enough current through?


---
**Jim Hatch** *August 20, 2016 00:05*

**+Scott Pollmann**​ have you checked to see if the knob is tight? It may be slipping on the pot's post. Should be a small setscrew on the side to tighten it.


---
**Alex Krause** *August 20, 2016 00:07*

Run a volt meter from the center tap of the pot to the left terminal on it check for dc voltage... full power to the pot should read 0v but that's really hard on on the tube 10ma should reads around 2 volts... quick question are you using the test fire button to read your mA power or is that what it reads while engraving


---
**Scott Pollmann** *August 20, 2016 00:09*

**+Alex Krause** will check the voltage.  That reading is either with test fire or while cutting. 


---
**Scott Pollmann** *August 21, 2016 20:12*

**+Alex Krause** at 2V I get about 7mA and at 4.86V (my max) I get about 9mA. 


---
**Scott Pollmann** *August 21, 2016 20:13*

**+Alex Krause** full power is reading the 4.86V


---
**Alex Krause** *August 21, 2016 20:14*

Sounds like the potentiometer inside the psu has been dialed back


---
**Alex Krause** *August 21, 2016 20:15*

Or the mA guage is defective


---
**Scott Pollmann** *August 21, 2016 20:18*

Replacing the gauge would be easy, no idea how to change the POT. Going to add some pics of my alignment in a bit.  I've now upgraded to a LO lens and I don't feel my mirrors are to out of alignment.  I'm surprised I can't cut through laminate flooring though. 


---
*Imported from [Google+](https://plus.google.com/113361677380464824789/posts/SMZHhmXJKfS) &mdash; content and formatting may not be reliable*
