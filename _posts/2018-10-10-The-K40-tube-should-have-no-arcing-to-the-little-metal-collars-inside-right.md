---
layout: post
title: "The K40 tube should have no arcing to the little metal collars inside, right?"
date: October 10, 2018 18:24
category: "Original software and hardware issues"
author: "ThantiK"
---
The K40 tube should have <i>no</i> arcing to the little metal collars inside, right?



I'm getting a tiny bit of arcing on mine; I'm guessing the solution is to replace the coolant with distilled water?





**"ThantiK"**

---
---
**James Rivera** *October 10, 2018 19:13*

Distilled water is going to be better than most (if not all) cooling fluids. What coolant are you using?


---
**ThantiK** *October 10, 2018 19:28*

Distilled water, but the pump that came with the K40 ran dry for a couple weeks when the laser wasn't in use.  I'm concerned that maybe it's damaged and leeching metal into the water - I'm gonna be replacing the pump and flushing the water.



I'm also using a biocide agent in the water ([http://www.sidewindercomputers.com/peptpcobi1.html](http://www.sidewindercomputers.com/peptpcobi1.html)) and I'm thinking about trying it without the biocide.

[sidewindercomputers.com - Petra'sTech PT Nuke PHN Concentrated Biocide (10mL) - PC Coolant & Dyes](http://www.sidewindercomputers.com/peptpcobi1.html)


---
**James Rivera** *October 10, 2018 22:52*

**+ThantiK** I cannot say whether that is a good choice or not. IIRC this (what I use) is what **+Don Kleinschnitz Jr.** recommended.

[amazon.com - Robot Check](https://smile.amazon.com/gp/product/B001D4TNH8/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Don Kleinschnitz Jr.** *October 10, 2018 22:53*

I would not use anything other that what our tests have revealed are safe, distilled water and algeacide. Most agents are too conductive as they do not worry about current flow in the water due to high voltages. You could check it with a conductivity meter.


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/1WQWaPLBtJZ) &mdash; content and formatting may not be reliable*
