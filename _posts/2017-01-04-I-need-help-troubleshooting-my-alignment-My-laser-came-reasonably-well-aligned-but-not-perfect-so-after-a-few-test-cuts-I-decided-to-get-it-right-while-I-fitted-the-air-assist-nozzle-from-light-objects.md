---
layout: post
title: "I need help troubleshooting my alignment. My laser came reasonably well aligned but not perfect so after a few test cuts I decided to get it right while I fitted the air assist nozzle from light objects"
date: January 04, 2017 05:31
category: "Hardware and Laser settings"
author: "Simon Cook"
---
I need help troubleshooting my alignment. My laser came reasonably well aligned but not perfect so after a few test cuts I decided to get it right while I fitted the air assist nozzle from light objects. After adjusting my fixed mirror to be as close to dead centre as I can manage I now get two spots from a single fire on the moving mirror. I thought I might be just punching through the tape and back out but using thicker card shows this not to be the case. Anyone have and idea why I'm getting two beams on one fire?

![images/de017fb6badce90a04a67359d6cf5b77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de017fb6badce90a04a67359d6cf5b77.jpeg)



**"Simon Cook"**

---
---
**Alex Krause** *January 04, 2017 05:59*

Move one mirror... And you have to move them all


---
**Alex Krause** *January 04, 2017 06:00*

Also is this burn from the exit end of the LO head or on one of the mirrors 


---
**Simon Cook** *January 04, 2017 06:24*

I was expecting to move all mirrors. I just didn't get past adjusting the first mirror. Somehow I seem to now be producing a dual beam ?! This photo is the beam arriving at the first flying mirror. 


---
**Ned Hill** *January 04, 2017 13:35*

Check and make sure the first mirror isn't crack or has some other flaw.  Do you get a nice symmetrical single spot on the first mirror?  You can always switch the first mirror with one of the other mirrors if you want to test to see if it is something to do with that first mirror.  Also check the exit mirror on the laser tube itself. 






---
**greg greene** *January 04, 2017 14:36*

Your beam has to strike the CENTRE of the fixed mirror when it exits the tube - else you will never get a good alignment. If it is - then check that the track the travelling mirror rides on is not twisted or bent.


---
**Ned Hill** *January 04, 2017 22:49*

Did you check the things I mentioned?


---
**Simon Cook** *January 04, 2017 22:50*

**+Ned Hill** of course, swapping mirrors is a great idea. I'll give that a try. 


---
**Simon Cook** *January 04, 2017 22:50*

**+greg greene** As I said it's striking the centre of the fixed mirror and the result is apparently a dual beam at the next mirror and I'm completely flummoxed as to how that's happening


---
**greg greene** *January 04, 2017 23:09*

That is a mystery all right - how did switching the mirrors go?


---
**Simon Cook** *January 05, 2017 07:59*

Thanks guys. Was a blemish on the fixed mirror I couldn't see. By perfecting the alignment I landed smack-bang in the middle of it. Switched mirrors around so I can dodge the blemish and it all lined up and is cutting great. Where's the best source for new mirrors? Lightobjects? Is it worth getting a new lens from them too even if it's the budget "IMPROVED 18MM ZNSE FOCUS LENS (F50.8MM)"? I already have the air assist so it would save me shimming it. 


---
**Ned Hill** *January 05, 2017 12:54*

**+Simon Cook**  Glad to hear you figured it out. I purchased new Moly mirrors and the lens you referenced from Light Objects from the start so I can only attest to their quality which I found to be very good.  Moly is probably over kill at the k40 power level but they are more durable.  I would recommend getting the improved lens. You can find mirrors and lens cheaper elsewhere but it's a crap shoot as to the quality you would get.  I decided to just pay the extra to buy them from LO because they had a known quality. You can search through the posts here to see if there have been other recommendations and/or just start a new post and ask for recommendations.


---
**Robert Selvey** *January 07, 2017 16:28*

You could also make your own mirrors out of Hard Drive Platters.


---
**Simon Cook** *January 07, 2017 21:16*

**+Robert Selvey** I looked into that but the few examples I found of those who have done it and tested transmission seemed to show poor results. Maybe handy for an emergency replacement but seems like mirrors designed for CO2 are a bit better. 


---
*Imported from [Google+](https://plus.google.com/101890222799169927378/posts/1YKVUEVVLx9) &mdash; content and formatting may not be reliable*
