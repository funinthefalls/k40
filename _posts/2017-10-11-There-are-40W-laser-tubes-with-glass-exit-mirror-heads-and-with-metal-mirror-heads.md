---
layout: post
title: "There are 40W laser tubes with glass exit mirror heads and with metal mirror heads"
date: October 11, 2017 20:56
category: "Original software and hardware issues"
author: "Lars Andersson"
---
There are 40W laser tubes with glass exit mirror heads and with metal mirror heads.



Does the head material matter?





**"Lars Andersson"**

---
---
**Anthony Bolgar** *October 11, 2017 22:38*

A metal head is supposed to be better quality. I have never seen any data on it however.


---
**Phillip Conroy** *October 12, 2017 07:12*

Most metal head tubes use screw connection and not the crap pin


---
**Phillip Conroy** *October 12, 2017 07:16*

[http://www.ebay.com.au/itm/40W-700mm-Engraver-Laser-Tube-Holder-Support-For-CO2-Cutting-Engraving-Machine-/201989686682?hash=item2f0786059a:g:dnMAAOSw-wFZbGGv](http://www.ebay.com.au/itm/40W-700mm-Engraver-Laser-Tube-Holder-Support-For-CO2-Cutting-Engraving-Machine-/201989686682?hash=item2f0786059a:g:dnMAAOSw-wFZbGGv)


---
**Phillip Conroy** *October 12, 2017 07:16*

[ebay.com.au - Details about 40W 700mm Engraver Laser Tube + Holder Support For CO2 Cutting Engraving Machine](http://www.ebay.com.au/itm/40W-700mm-Engraver-Laser-Tube-Holder-Support-For-CO2-Cutting-Engraving-Machine-/201989686682?hash=item2f0786059a:g:dnMAAOSw-wFZbGGv)


---
**Lars Andersson** *October 12, 2017 08:58*

Is the metal housing of the totally reflecting mirror connected to the power supply red wire so the whole part is at high voltage?



[i.ebayimg.com](https://i.ebayimg.com/images/g/k20AAOSw2kZZbGGu/s-l300.jpg)


---
**Phillip Conroy** *October 12, 2017 09:42*

That's why the reci and spt tubes have silicon caps for hi voltage end


---
*Imported from [Google+](https://plus.google.com/117177573126096232373/posts/3xrTCdJSbTU) &mdash; content and formatting may not be reliable*
