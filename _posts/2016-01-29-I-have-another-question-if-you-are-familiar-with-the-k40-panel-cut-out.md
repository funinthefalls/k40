---
layout: post
title: "I have another question, if you are familiar with the k40 panel cut out?"
date: January 29, 2016 06:15
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
I have another question, if you are familiar with the k40 panel cut out? I downloaded the dxf file imported to inkscape, now what do I do? It has 42 objects when I select all. What's next?? Between the last example help and this one I think I will really have an awesome understanding of what the heck I'm doing lol. Here's the link [https://drive.google.com/file/d/0B8S25d-5M3YyYTB6WTh1TVBNcmM/edit?usp=sharing](https://drive.google.com/file/d/0B8S25d-5M3YyYTB6WTh1TVBNcmM/edit?usp=sharing)﻿





**"Andrew ONeal (Andy-drew)"**

---
---
**Justin Mitchell** *January 29, 2016 12:09*

The plugin seems happy enough with it just as a set of lines to cut.  



If you really want to make a single solid object out of it then you have a bit of work as its only lines and circles.  to make one of the box shapes, drag the select tool to select all 4 lines. then with the 'Edit Paths by Nodes' tool drag select each corner seperately, hitting the 'join selected nodes' button.  once you do that to all 4 corners you should have a rectangle object.  you can then as before 'difference' the smaller rectangles from the big one to make cutouts.   



If anyone knows a faster way to do it please say, i did try 'combine' but it doesn't always work and you have to use join nodes to fix it up.


---
**Andrew ONeal (Andy-drew)** *January 29, 2016 19:12*

Hey thanks again man, this stuff is really frustrating especially when using temperamental plugins. I ended up getting code as is and then generated code with red lines, haven't put it on laser just yet to test.


---
**Andrew ONeal (Andy-drew)** *January 29, 2016 19:17*

I thought that objects like this would have to be solid color except areas being cut? Like that frame you helped with?


---
**Justin Mitchell** *January 29, 2016 22:10*

I always use solid objects, but as i understand it doesnt <i>have</i> to be.  there are often gotchas if you only use lines, if its too thin it might not be cut at all, if its too wide it will get cut twice (as if it was a thin rectangle)  but if you give it a solid object then it knows whats an outline


---
**Andrew ONeal (Andy-drew)** *January 30, 2016 01:53*

Is there a way to fill color on this example via inkskape?


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/5jBAVHPhweT) &mdash; content and formatting may not be reliable*
