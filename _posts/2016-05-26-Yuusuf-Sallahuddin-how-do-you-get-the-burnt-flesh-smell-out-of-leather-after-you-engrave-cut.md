---
layout: post
title: "Yuusuf Sallahuddin how do you get the burnt flesh smell out of leather after you engrave/cut?"
date: May 26, 2016 05:30
category: "Materials and settings"
author: "Alex Krause"
---
**+Yuusuf Sallahuddin**​ how do you get the burnt flesh smell out of leather after you engrave/cut?





**"Alex Krause"**

---
---
**Anthony Bolgar** *May 26, 2016 05:41*

You can put it in a container of baking soda for a few days. Febreeze is also helpful. I am sure **+Yuusuf Sallahuddin** probably knows better or faster ways.


---
**Alex Krause** *May 26, 2016 06:05*

Cool! I have an order for 25 leather bracelets to engrave by the 28th for a local bands Merch booth they only take a few minutes each but they smell like burn arm hair while you are grilling 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 06:18*

**+Alex Krause** I tend to not really notice it myself, but it could depend on the leather you are using. Is the leather you are using already dyed? If not, is it natural vegetable tanned leather?



Some tanning processes (although I don't know everything about them) contain crazy chemicals.



But, further to your question, usually when I make something with leather on the laser, I am going to be dyeing the finished product. Then I also add a leather conditioner to  it. Sometimes also a lacquer or sealer.



You will rarely smell any burnt smell after all that process.



A good leather conditioner can be used straight after engraving/cutting. You need to put on a decent amount (say 1.5" circle blob per sq ft.) & spread it evenly over the piece & allow it to soak into the leather. You will then smell the leather/conditioner instead.



A good homemade conditioner can be made with beeswax & coconut oil. Although I'm not 100% sure of the ratio, but it would be somewhere around 1 part beeswax to 3-5 parts coconut oil.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 06:18*

**+Alex Krause** note: if you are intending on doing more engraving/cutting of the piece, I would not put the conditioner until after all has been done.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/UQuyFjZavGG) &mdash; content and formatting may not be reliable*
