---
layout: post
title: "What are your \"best settings\" for engraving rubber stamps with your K40?"
date: June 16, 2018 14:18
category: "Materials and settings"
author: "Zsolt Iv\u00e1n Varga"
---
What are your "best settings" for engraving rubber stamps with your K40?





**"Zsolt Iv\u00e1n Varga"**

---
---
**James L Phillips** *June 17, 2018 20:03*

I have a pdf from Facebook with all kinds of settings and material but I don't know how to post it.


---
**Kiah Connor** *June 18, 2018 05:32*

Upload it to Google Drive and post the link perhaps?




---
**James L Phillips** *June 18, 2018 14:56*

I found it with google. [engraversnetwork.com - www.engraversnetwork.com/files/MVX_Laser_Settings-Guide.pdf](http://www.engraversnetwork.com/files/MVX_Laser_Settings-Guide.pdf)


---
**Kiah Connor** *June 19, 2018 01:43*

I actually remember seeing that during my initial research into which laser cutter to get! 



Nice. I'm doing templates of each of my materials anyway, might stick these to the back of each too :D


---
*Imported from [Google+](https://plus.google.com/110077238752547397888/posts/SeezUeaspDL) &mdash; content and formatting may not be reliable*
