---
layout: post
title: "A buddy of mine whose wife is a quadriplegic ask me to come up with an ornament that incorporated the Superman/Christopher Reeve connection"
date: December 11, 2016 22:24
category: "Object produced with laser"
author: "Mike Meyer"
---
A buddy of mine whose wife is a quadriplegic ask me to come up with an ornament that incorporated the Superman/Christopher Reeve connection. Here's my modest offering... photos are side 1 and the flip side.



![images/23065164b01fc3edf5a10d252796f939.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/23065164b01fc3edf5a10d252796f939.jpeg)
![images/9e77823c744ade0fe67694d97daaadc2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e77823c744ade0fe67694d97daaadc2.jpeg)

**"Mike Meyer"**

---
---
**Cesar Tolentino** *December 11, 2016 22:34*

I really like it.  Very nice


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/7q4s5ED1Zrx) &mdash; content and formatting may not be reliable*
