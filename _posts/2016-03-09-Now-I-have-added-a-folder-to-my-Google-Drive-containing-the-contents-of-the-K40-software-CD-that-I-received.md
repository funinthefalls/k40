---
layout: post
title: "Now, I have added a folder to my Google Drive, containing the contents of the K40 software CD that I received"
date: March 09, 2016 08:03
category: "Software"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Now, I have added a folder to my Google Drive, containing the contents of the K40 software CD that I received. I have shared the folder for anyone with link can view (& subsequently is supposed to be able to download it), however one user tried to download the contents & came across some error stating that only "it can only be handled by the owner".



I've changed the permissions to be "Public on the web - Anyone on the Internet can find and view", which means you should be able to download it also.

I'm not really sure what I have done wrong originally, however the other option (that I'm fairly certain works) is that anyone who would like access to the folder to download it can provide me an email address & I can add it to the invite list for permissions.



If you need to be added to the list because it doesn't allow you to download, let me know here please.



Also note: please be aware that something in this software package flags as a virus. This is the original cd that I (& I assume many others) were sent for the K40. If you have concerns about virus, I recommend you don't download it (or if you do, use on a machine that you don't connect to the internet with).



[https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)

(folder).

![images/8557fa2eba3506cf4a9ee85ad6b814a5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8557fa2eba3506cf4a9ee85ad6b814a5.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 08:06*

**+HP Persson** give it a test now.

**+Martin Larsen**


---
**Martin Larsen** *March 09, 2016 09:03*

Hi Yuusuf.

When i open your google drive link, some of the files are not showing.

I cannot see the EXE files :(

Perhaps google drive doesnt allow this?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 09:28*

**+Martin Larsen** Oh, well maybe it's better if I just put it all in a .rar file



I'll give that a go. Will have to wait for it all to compress & then reupload. I'll drop a message here once that is completed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 10:38*

**+Martin Larsen** Should be good to go now. Just 1 file now as .rar format. Hope it works for you. By the way, did you get one of the USB hardware-key sticks? It should look like the gold one here [http://appliedabsurdity.org/wp/wp-content/uploads/2015/01/Laser-035.jpg](http://appliedabsurdity.org/wp/wp-content/uploads/2015/01/Laser-035.jpg) that says Lihuiyu Studio Labs on it.


---
**HP Persson** *March 09, 2016 15:20*

**+Yuusuf Sallahuddin**  Let me know if you need any space to share the file, i can spare some on one of my servers ;) If google drive doesnt work.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 16:11*

**+HP Persson** Ah, it seems to work on Google Drive as 1 .rar file. Martin mentioned that he got it and it is working now. If you are still interested in grabbing it to see if there is a difference with your files. You can always chuck it on one of your servers as a backup copy in case others need it in future. I will leave it in my Google Drive for this purpose too.


---
**Andrew ONeal (Andy-drew)** *March 09, 2016 21:33*

Corellaser can be used from this file?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 22:49*

**+Andrew ONeal** It can, but be aware some users say that something in amongst these files flags as a virus. I use the CorelDraw12 & CorelLaser plugin on my system.


---
**Andrew ONeal (Andy-drew)** *March 10, 2016 02:34*

I paid monthly membership fees to Corel but never saw Corel laser, is this new?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 02:43*

**+Andrew ONeal** CorelLaser isn't an actual Corel software. It is a plugin, created by someone else (not sure who), that allows CorelDraw to "talk" to the laser cutter.


---
**Canapés Económicos** *March 22, 2016 15:12*

Hi Yuusuf:

I have broken my CD... Could u send to me the software?

canapecalidad@gmail.com

Thanks


---
**Tony Sobczak** *March 22, 2016 15:58*

**+Canapés Económicos**​ Yuusuf  placed in on a drive so you can download it. Much faster for you.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 22, 2016 20:23*

**+Canapés Económicos** Hi Canapes. As Tony mentions, it is contained on my Google Drive as a .rar link. If you check the full comment on this post you will see a link at the bottom to the folder on my Google Drive where you can download the .rar file containing the entire CD contents.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/PPD3GDs6a5o) &mdash; content and formatting may not be reliable*
