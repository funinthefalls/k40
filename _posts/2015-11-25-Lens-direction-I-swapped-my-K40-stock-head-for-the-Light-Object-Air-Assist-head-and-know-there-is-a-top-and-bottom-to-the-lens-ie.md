---
layout: post
title: "Lens direction? I swapped my K40 stock head for the Light Object Air Assist head and know there is a top and bottom to the lens (i.e"
date: November 25, 2015 01:07
category: "Hardware and Laser settings"
author: "DIY3DTECH.com"
---
Lens direction?  I swapped my K40 stock head for the Light Object Air Assist head and know there is a top and bottom to the lens (i.e. reflective & concave side) before removing.  When taking the lens out the "reflective" side was pointed down and the concave side up and made notes.  However read on this form (or at least the way I took it) that this should be reversed. So which is right as I do not trust the assembly of the machine as being accurate in the first place.  I have to also say I have tried it both ways and don't see a difference which bring me to the genesis of the question.  After changing the air assist is great however the laser no where seems the same power wise as before.   I have cut the same material as (as the same sheet) where one pass at 68% cut the piece out and now two passes at 90% won't do it.  Also yes did measure down of the lens 5 cm an ensure the material was at the same focal point.  Thing that make you go hmmm...





**"DIY3DTECH.com"**

---
---
**Gary McKinnon** *November 25, 2015 01:18*

The focal point should be half-way through the material, i read. Are you sure something else isn't affecting power output, as nothing that you've mentioned seems responsible, if you've tried the lens both ways now ?




---
**Todd Miller** *November 25, 2015 01:57*

How do you determain the focal point on an unknown head/lens arraignment ? 


---
**DIY3DTECH.com** *November 25, 2015 02:58*

**+Gary McKinnon** Yes tried it both way and don't see a difference.  Also it seems to be working are crank up the %, the burn reflections brightens,  But have not changed anything else yet... 


---
**DIY3DTECH.com** *November 25, 2015 02:59*

**+Todd Miller** Simply measured down from the focal point of the lens is 5 cm...


---
**Todd Miller** *November 25, 2015 03:40*

But,



How does one know what I have have for a focal point ?



Perhaps a standard for these type of machines or would it based on the diameter of the lens ?

 


---
**Joey Fitzpatrick** *November 25, 2015 06:01*

To find your focal distance, angle a piece of wood across you bed.  Draw a single line horizontally.  Cut the line. Measure from the thinnest point on the cut line to the focal lens and this will be your actual focal length 


---
**Coherent** *November 25, 2015 12:29*

Convex  (domed) side up. All you need is a piece of white paper, an overhead light and a ruler to find your focal point. Here's the link to a video that shows how.


{% include youtubePlayer.html id="v0znprj3xsw" %}
[https://www.youtube.com/watch?v=v0znprj3xsw](https://www.youtube.com/watch?v=v0znprj3xsw)


---
**DIY3DTECH.com** *November 25, 2015 16:05*

Interesting and thanks for comments and suggestions , also for clarity the seller listed in the specifications that the focal length is 5cm.  However will try to the paper test as that looks interesting and easy to do. 


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/8PEHvPcnajM) &mdash; content and formatting may not be reliable*
