---
layout: post
title: "So I'm battleling with my water temp"
date: October 05, 2016 19:30
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
So I'm battleling with my water temp.  Currently without cooling I am at 31c. After throwing some of those plastic containers for cooling into my 5gal bucket and Turing on the AC I can go down to 23C. I'm still not happy and my cuts are still far away from effective. So I'm on a quest to find a solution and though about water chiller bellow ( not my image).  Anyone has some input on how would this work? 

![images/271920ec0aec46fca61536209b93c988.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/271920ec0aec46fca61536209b93c988.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**greg greene** *October 05, 2016 19:41*

I got a cheap - 89 buck aquarium chiller on ebay - works good, pump from tank flows through the chiller then the tube - then back to tank - keeps temp at about 15 degrees C. 


---
**Ashley M. Kirchner [Norym]** *October 05, 2016 20:01*

You live in a rather hot environment there **+Ariel Yahni**. I have a 2 gallon tank, and two 20-oz bottles frozen solid, will bring my temp down into the teens easy. And now that it's getting cooler for us, last night's first run had water flowing at 9C (but then it stabilized around 13C once I started cutting.)



I would try a smaller bucket (or just less water) and see if you can cool that down. A larger tank means you might be able to keep the water temp more stable, however cooling that much water will take quite a bit. I keep several of those bottles frozen and just swap them out every so often.


---
**Maxime Favre** *October 05, 2016 20:03*

Yup adding a tank (more water) in line helps. Depending what you're planning, the CW5000/5200's might be a no brain option.

But yeah that's cool to convert a water fountain ! Maybe you can add a beer cooling option ;)


---
**Don Kleinschnitz Jr.** *October 05, 2016 23:16*

I have been wondering about this unit: 

[https://www.amazon.com/IceProbe-Thermoelectric-Aquarium-Chiller/dp/B001JSVLBO/ref=sr_1_2?ie=UTF8&qid=1475708981&sr=8-2&keywords=aquarium+cooler](https://www.amazon.com/IceProbe-Thermoelectric-Aquarium-Chiller/dp/B001JSVLBO/ref=sr_1_2?ie=UTF8&qid=1475708981&sr=8-2&keywords=aquarium+cooler)

[amazon.com - Amazon.com : IceProbe Thermoelectric Aquarium Chiller - 4 in. x 4 3/4 in. x 7 1/2 in. : Aquarium Supplies : Pet Supplies](https://www.amazon.com/IceProbe-Thermoelectric-Aquarium-Chiller/dp/B001JSVLBO/ref=sr_1_2?ie=UTF8&qid=1475708981&sr=8-2&keywords=aquarium+cooler)


---
**Ariel Yahni (UniKpty)** *October 05, 2016 23:24*

**+Don Kleinschnitz** from the reviews it seems that it does not lower from ambient that much. Then again the above solution im not sure either


---
**greg greene** *October 05, 2016 23:48*

this one works for me

[http://www.ebay.com/itm/282033379947?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/282033379947?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Don Kleinschnitz Jr.** *October 06, 2016 00:24*

**+Ariel Yahni** This is the review made me wonder about our application.

"It says 10 gal can get 6-8 degree cooling. I am using it in a 5 gal that is only about 3/4 full and non insulated with a high intensity light. The normal temp of the tank is a blazing 90F. With the cooler it gets to 77F and at night with the light off it gets to 72F."


---
**Ariel Yahni (UniKpty)** *October 06, 2016 00:32*

**+Don Kleinschnitz** that a good review and in par with my temps but im looking to go bellow 68 and see if my cut get better


---
**Don Kleinschnitz Jr.** *October 06, 2016 00:33*

**+greg greene** I like that one much better + I have been thinking of moving my machine to the shop (from den) and I need to heat it during winter. This unit does both. Also includes the temp controller ....


---
**Ariel Yahni (UniKpty)** *October 06, 2016 00:34*

I cant seem to find the shipping weight on that one. My overall cost could end up into the 200


---
**Don Kleinschnitz Jr.** *October 06, 2016 01:48*

On amazon it is more expensive but free shipping for $158.00 at least here, don't know where you are:



[https://www.amazon.com/Aquarium-Thermostat-Chiller-Heater-Adjustable/dp/B01JUZR18E/ref=sr_1_1?ie=UTF8&qid=1475713530&sr=8-1&keywords=Aquarium+Thermostat+Chiller+Heater+Adjustable+Fish+Tank+Salt%2FFresh+Water](https://www.amazon.com/Aquarium-Thermostat-Chiller-Heater-Adjustable/dp/B01JUZR18E/ref=sr_1_1?ie=UTF8&qid=1475713530&sr=8-1&keywords=Aquarium+Thermostat+Chiller+Heater+Adjustable+Fish+Tank+Salt%2FFresh+Water)



[amazon.com - Amazon.com : Aquarium Thermostat Chiller Heater Adjustable Fish Tank +160GPH Water Pump : Pet Supplies](https://www.amazon.com/Aquarium-Thermostat-Chiller-Heater-Adjustable/dp/B01JUZR18E/ref=sr_1_1?ie=UTF8&qid=1475713530&sr=8-1&keywords=Aquarium+Thermostat+Chiller+Heater+Adjustable+Fish+Tank+Salt%2FFresh+Water)


---
**Ariel Yahni (UniKpty)** *October 06, 2016 02:05*

Thats almost 60 more? Im in Panama central america, it costs me 5.00 a pound to bring it here


---
**Phillip Conroy** *October 06, 2016 05:43*

Be carefull buying this type of cooler ,going by the wattage it uses 72 watts is do not sound like it is refridigerated <s>sounds more like cheap 12  volt car fridige</s> only works by plizer cooling so will only drop temp by 10-15 deg .i use a second hand  under sink refridigerated cooler that uses 400 watts.and will drop my 11 liter water temp even after 3hour of cutting down to 10 deg.must get around to hooking up my temp controler to it ,i have a lasef beam power meter and have tested the power of the beam at diffrent temps and can tell you going from 40 watts at 15 degress to 20deg the laser beam power drops 8 watts


---
**Phillip Conroy** *October 06, 2016 05:46*

Forgot to add also beware of a laser cooler called cw3000 i have seen  photos of the inside and it is not a good cooler to buy as is only a water radator and a fan


---
**Robert Selvey** *October 06, 2016 11:33*

I had my water running through a mini fridge and water would get down to 12c condensation was a nightmare.


---
**Scott Marshall** *October 06, 2016 17:01*

Those type of coolers use a peltier effect chip and are very low btu. Not even close to keeping up with a laser over a long run. They work fine in the water dispenser because they have forever to bring down a small tank of water to drinking temp, but the small chilled volume of water won't help your cause much.



You'll increase the water mass a little, and draw off a little heat, slowing the rate at which the temp climbs, but climb it will. 



I ran calculations on the BTU required at one point, but don't recall exactly what is needed. I DID determine even the smallest window AC unit (5k btu) will do it with ease, and they're about $100.



My plan (If I ever feel well again and catch up) is to take a super cheap window AC (NO digital controller, dial type) and add a water tank to the evaporator and run a second loop through that, controlling circulating pump and AC with a simple temperature controller. Total cost is about $200us, $100 for the AC, and the balance for a small pump (or re-use an old stock K40 pump) and electronics.

You can just put it in a window, or even run tubing to your basement or garage if you want it silent.



I'm hoping to offer a controller/plumbing kit (just add your own Garage Sale or Ebay AC)



Peltier Chips just can't do the job for the size of a K40. And they're so incredibly ineffecient that if you racked up enough to do the job, they would require several hundred amps of 12v power and need their own cooling device (they convert all that power to heat and add it to the heat transferred)



Another idea I've had is just bury a few hundred feet of 6-8mm PE tubing in the backyard, no heat pump required in my lattitude.



It still  looks like the PE tubing loop in the freezer is the most practical.



Still looking for the easy solution on this one, as are you. 



Scott


---
**Jeff Johnson** *October 06, 2016 17:23*

Living on the Gulf Coast of Florida, I keep my freezer stocked with several frozen gallon jugs during hurricane season. Since getting the K40 I've been putting them to use. I have a 5 gallon bucket with about 3.5 gallons of water in it. Each evening before I start using the laser, I put one of the frozen jugs in the bucket. This keeps the water chilled for hours. When I'm done, I put the jug back in the freezer.


---
**Ariel Yahni (UniKpty)** *October 06, 2016 17:29*

**+Jeff Johnson**​ to what temp can you get there with the water jugs? 


---
**Ariel Yahni (UniKpty)** *October 06, 2016 17:31*

**+Scott Marshall**​ the one I have uses gas. I had it unplugged so today I will see how low temp the water gets


---
**Jeff Johnson** *October 06, 2016 18:00*

**+Ariel Yahni** I'm not sure but the water in the return tube is still cold after several hours. I've been wanting to get a thermometer and flow meter though.


---
**Scott Marshall** *October 07, 2016 02:16*

**+Ariel Yahni** You have a shot there, I think you need about 1200btuh if my memory serves, and that's about 1/5th the size of the smallest room AC, to give you a rough idea what size it might be. I'd think if they're investing in going with a freon type loop, they would be making it recover quickly enough for high volume use (like an office building with 10+ users)






---
**Ariel Yahni (UniKpty)** *October 07, 2016 02:25*

I just tested the water temp coming out of this and was almost 61F


---
**Scott Marshall** *October 07, 2016 02:28*

This might help you work out the numbers,



A BTU is the amount of cooling/heating required to lower or raise 1 pound of water, 1°F at one atmosphere.



Assuming that you are near 1 atmosphere (you aren't on top of a mountain or something), 400 gallons of water is about 3,335 pounds so to cool this from 70°F to 45°F (25°F change) requires 3,335 pounds x 25°F change = 83,375 BTU. To change 400 gallons of water just 1°F requires 3,335 BTU.



You might need or want to apply a time factor to your analysis. Perhaps you want to cool 400 gallons of water from 70°F to 45°F in 2 hours. The cooling system would then need to remove only half of the 83,375 BTU's each hour so your cooling system could produce one half of the cooling it would need to cool the water in 1 hour.



Here is a handy formula for calculating the cooling requirement of water:



BTU = Gallons x Temperature Change (°F) x 8.33



Thanks to:[http://www.advantageengineering.com/index.php](http://www.advantageengineering.com/index.php)

These guys make Industrial Chillers and know the business well.


---
**Don Kleinschnitz Jr.** *October 07, 2016 13:12*

Interesting, should we be thinking about using an external pump vs a submersible one. My stock K40 water pump is 30W, isn't that 30W that I have to cool in addition to the wattage of the laser tube?


---
**Don Kleinschnitz Jr.** *October 07, 2016 13:41*

An additional site if you are thinking about peltier technology. [https://tetech.com/peltier-thermoelectric-cooler-module-calculator/?mode=1&dtmax=70&heatLoad=70&hotSideTemp=26&coldSideTemp=20&potted=0&emailsent=0](https://tetech.com/peltier-thermoelectric-cooler-module-calculator/?mode=1&dtmax=70&heatLoad=70&hotSideTemp=26&coldSideTemp=20&potted=0&emailsent=0)



By my simple calcs, a peltier would need a capacity of about 139 watts for a heat load of 70 watts creating about 10 degrees change. The peltier coolers above from Amazon are 60-70 watts .... marginal. It looks like a home brew dual peltier would work. 

 Something like:

A heat exchanger (CPU heat sinks) & mounting in bucket $40

A 24V 12 amp supply $22

2x modules @ $44 

[tetech.com - Peltier - Cooler Module Calculator - TE Technology](https://tetech.com/peltier-thermoelectric-cooler-module-calculator/?mode=1&dtmax=70&heatLoad=70&hotSideTemp=26&coldSideTemp=20&potted=0&emailsent=0)


---
**Nick Williams** *October 10, 2016 05:31*

A simple solution is to pickup a good size dorm refrigerator drill holes in the door for you coolant tubes and place a five gallon bucket inside. note, you may be tempted to drill into the side of the refrigerator but there is a chance you may hit tubing or electronics the door is a safe bet. This is a very cost effective way to get a reliable cooling system. Because the water is already cold before you start you job the refrigerator only works a little to dissipate the heat generated during the job. If you keep your eyes open you can pick these up for $20:-)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/gmcdpTCxQrc) &mdash; content and formatting may not be reliable*
