---
layout: post
title: "UPDATE: PWM is working!! but cuts have been effected"
date: May 09, 2017 22:37
category: "Smoothieboard Modification"
author: "Ben Myers"
---
UPDATE: PWM is working!! but cuts have been effected.



Hi, I have a K40 laser and im doing the smoothieboard upgrade using MKS sbase 1.3 board with the MKS tft. I have all mechanical parts working includind endstops, home is working correctly.



I am having trouble with the pwming on the laser. I am trying to follow some of the guides here but not sure which wire to connect to 2.4 on my board. do I use the IN or the L in Dons guide he suggest using L but when I do that everytime I move the laser fires. some other guides suggest using IN..



Any help would be appreciated.



Thanks



Ben

![images/8d9e2af33d3f66881d3b1cbcc707f180.png](https://gitlab.com/funinthefalls/k40/raw/master/images/8d9e2af33d3f66881d3b1cbcc707f180.png)



**"Ben Myers"**

---
---
**Richard Vowles** *May 09, 2017 23:25*

Yeah, using an MKS base is not going to win you any friends sorry. They doa closed source smoothie and offer no sorr support not give back to the community.


---
**Ben Myers** *May 09, 2017 23:29*

**+Richard Vowles**  I know I am trying to purchase a real smoothie but cant get one here. I am going to donate to Don and Wolfman for all the great work they do.






---
**Kelly Burns** *May 09, 2017 23:46*

Your objection and stance might be appropriate if he posted in the Smoothieware support, but I don't see any issue with him asking a hookup/wiring question here.  


---
**Ben Myers** *May 09, 2017 23:51*



Hi **+Kelly Burns**  just looked at some of your projects and they are amazing! I also seen you have made your machine full width, I will be doing that upgrade once I have all the new electronics working correctly. Did you use makerslide?


---
**Kelly Burns** *May 10, 2017 00:13*

Yeah, I did both the physical and electronics upgrade at the same time and under a deadline for customer projects.  It was painful.  In my rush, I actually fried a Genuine Smoothie board with a stupidity.  



I will be publishing a complete write-up for both electronics and physical upgrades.  All my stuff is leveraged off the great work of +DonKleinschnitz, **+Ariel Yahni** and others.  


---
**Kelly Burns** *May 10, 2017 00:15*

Look at the **+Ray Kholodovsky** 's Cohesion board.  I believe it's a Smoothie community approved setup and the Mini is a drop-in replacement for the K40 controllers.  Its also priced just a little bit more than the hated MKS sBase.   Ray also provides great support in the community. 




---
**Kelly Burns** *May 10, 2017 00:26*

Here's the hookup I had when I had the MKS sBase in mine.  (-) pin of 2.4 (FAN) going to L on the PS.   PWM Frequency should be  200-400 (not 20 as documented in a few places)  I can't remember where I left off with that, but I know 200 gave me a decent start I PWM laser power control. 

  

![images/7b45cffae6f7c246b6c44b107dfc710d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b45cffae6f7c246b6c44b107dfc710d.jpeg)


---
**Ben Myers** *May 10, 2017 03:20*

Hi Kelly thank you for comments..



**+Don Kleinschnitz**, **+Ariel Yahni** are brilliant and without the information that they have published I would not be any where near where im at now.



I will look into the cohesion board an might use the MKS on my 3D printer.



I have my L going to that pin also but when I move any axis the laser turns on?? 


---
**Ray Kholodovsky (Cohesion3D)** *May 10, 2017 04:44*

Here's that link: [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Ben Myers** *May 10, 2017 05:09*

thanks **+Ray Kholodovsky** looks awesome.. have to save my pennies now!!




---
**Paul de Groot** *May 10, 2017 06:26*

The L input is switching the laser psu on/off. The in is the pwm input of the laser. Don had an experiment where he used the L as pwm and the in fixed to 5v. Both methods work. Cheers 


---
**Don Kleinschnitz Jr.** *May 10, 2017 11:47*

**+Ben Myers** it looks like you have read the blog posts on this subject so I will not link it here.



The L pin connection schema works fine if all of configuration and such is set up right including the polarity of the signal.



Please post: 

...Picture of your power supply (the input pin side)

...Picture of your connections between the MKS and the LPS

... Picture of your control panel

...Your configuration file



If I recall right **+Jorge Robles** was using a  MKS you might try an connect with him. I generally do not post info on MKS because the do not cooperate with the smoothie community. However I do help everyone .......

....

If you can you point me to a schematic of the board you are using I will look at it for you.

....

Have you verified that your LPS is working correctly.



1. Push the test switch on down on the supply does it fire?

2. Disconnect anything that is connected to L of the LPS.

3. With interlocks enabled and the "Laser Switch" ON, ground the L pin on the LPS, does it fire?



Generally from the symptoms it sounds like something is grounding the L pin. Which suggests something in the config file. 




---
**Ben Myers** *May 10, 2017 11:48*

**+Paul de Groot**  thanks, im unsure what you mean. I have it hooked up the L but it seems to be firing with no pwm? just full power up to the max set on the pot.




---
**Ben Myers** *May 10, 2017 13:28*

**+Don Kleinschnitz**  Hi Don thanks for your reply and thank you for all your work on the K40.. I have attached the pictures and config files as requested. 



Connecting are as follows as per above first posted picture. (no change is original wiring)



High Power

(as per above, no changes)



Logic

G -> laser on switch (no change)

P -> laser on switch (no change)

L -> Fire Switch (no change)

G -> no change

IN -> 2.6 negative terminal (blue wire)

5v -> no change



Low Power

24v -> 12-24 input positive terminal

GND -> 12-24 input negative terminal

5v -> not connected

L -> 2.4 negative terminal (brown wire)



with it setup like this the laser turns on and off as expected but the power level when set as low as s0.3 in the gcode runs at 20ma  and wont even cut through 3mm mdf when with the M2 nano it would cut the mdf at about 11ma. (test gcode below)



G21 ; All units in mm

G28 ; home all



G00 X2.762953 Y240.677863 F6000

M3 S0.3

G01 X132.762873 Y240.677863 F400

M5



Using the M2 nano board everything worked correctly.



I really appreciate your assistance and when I can afford to I will be purchasing a real smoothieboard and donating you some money.  



I think I'm rambling a bit here. let me know if I need to clarify anything. Thanks again



Ben

![images/49c0b85224066305142a53bcc05794b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/49c0b85224066305142a53bcc05794b1.jpeg)


---
**Ben Myers** *May 10, 2017 13:28*

![images/deb727acf3fa1f8756cf764392bffb95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/deb727acf3fa1f8756cf764392bffb95.jpeg)


---
**Ben Myers** *May 10, 2017 13:29*

![images/07109ca1f766e11f31ea5863b29a4ea7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07109ca1f766e11f31ea5863b29a4ea7.jpeg)


---
**Ben Myers** *May 10, 2017 13:29*

Couldnt find an actual schematic. hope this will suffice.

![images/7f20ff437ee983ba7db4e7146caded24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f20ff437ee983ba7db4e7146caded24.jpeg)


---
**Don Kleinschnitz Jr.** *May 10, 2017 16:39*

**+Ben Myers** 

are  you saying that you have a configuration that works but the power is to low as set from Gcode?



"with it setup like this the laser turns on and off as expected but the power level when set as low as s0.3 in the gcode runs at 20ma  and wont even cut through 3mm mdf when with the M2 nano it would cut the mdf at about 11ma. (test gcode below)"


---
**Paul de Groot** *May 10, 2017 21:22*

**+Ben Myers** it seems you used the extruder E2 pin from the board to drive the pwm input IN. Maybe you need to configure smoothie to use E2 as laser pwm port?


---
**Ben Myers** *May 10, 2017 22:28*

**+Don Kleinschnitz** not sure if the pwm is working correctly. In the test gcode i posted above I set the power level to s 0.2 and the laser runs about 12ma if I set it to s 0.3 it runs at 24ma. when I have the M2 nano board it would cut the 3mm mdf at 12 -13 ma now with the MKS at 24ma it almost makes it way through. **+Paul de Groot** the E2 pin is the laser fire pin the Fan pin is the pwm.


---
**Ben Myers** *May 10, 2017 22:29*

My config didnt upload Ill try again.






---
**Ben Myers** *May 10, 2017 22:41*

# NOTE Lines must not exceed 132 characters

# Robot module configurations : general handling of movement G-codes and slicing into moves

default_feed_rate                            8000             # Default rate ( mm/minute ) for G1/G2/G3 moves

default_seek_rate                            8000             # Default rate ( mm/minute ) for G0 moves

mm_per_arc_segment                           0.5              # Arcs are cut into segments ( lines ), this is the length for

                                                              # these segments.  Smaller values mean more resolution,

                                                              # higher values mean faster computation

mm_per_line_segment                          5                # Lines can be cut into segments ( not usefull with cartesian

                                                              # coordinates robots ).



# Arm solution configuration : Cartesian robot. Translates mm positions into stepper positions

alpha_steps_per_mm                           158.2               # Steps per mm for alpha stepper

beta_steps_per_mm                            158.2               # Steps per mm for beta stepper

#gamma_steps_per_mm                           400             # Steps per mm for gamma stepper



# Planner module configuration : Look-ahead and acceleration configuration

planner_queue_size                           32               # DO NOT CHANGE THIS UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING

acceleration                                 300             # Acceleration in mm/second/second.

#z_acceleration                              60              # Acceleration for Z only moves in mm/s^2, 0 uses acceleration which is the default. DO NOT SET ON A DELTA

acceleration_ticks_per_second                1000             # Number of times per second the speed is updated

junction_deviation                           0.05             # Similar to the old "max_jerk", in millimeters,

                                                              # see [https://github.com/grbl/grbl/blob/master/planner.c](https://github.com/grbl/grbl/blob/master/planner.c)

                                                              # and [github.com - grbl](https://github.com/grbl/grbl/wiki/Configuring-Grbl-v0.8)

                                                              # Lower values mean being more careful, higher values means being

                                                              # faster and have more jerk

#z_junction_deviation                        0.0              # for Z only moves, -1 uses junction_deviation, zero disables junction_deviation on z moves DO NOT SET ON A DELTA

#minimum_planner_speed                       0.0              # sets the minimum planner speed in mm/sec











# Stepper module configuration

microseconds_per_step_pulse                  1                # Duration of step pulses to stepper drivers, in microseconds

base_stepping_frequency                      100000           # Base frequency for stepping, higher gives smoother movement



# Cartesian axis speed limits

x_axis_max_speed                             30000            # mm/min

y_axis_max_speed                             30000            # mm/min

#z_axis_max_speed                             200              # mm/min



# Stepper module pins ( ports, and pin numbers, appending "!" to the number will invert a pin )

alpha_step_pin                               2.0              # Pin for alpha stepper step signal

alpha_dir_pin                                0.5              # Pin for alpha stepper direction

alpha_en_pin                                 0.4              # Pin for alpha enable pin

alpha_current                                0.8             # X stepper motor current

alpha_max_rate                               10000.0          # mm/min



beta_step_pin                                2.1              # Pin for beta stepper step signal

beta_dir_pin                                 0.11             # Pin for beta stepper direction

beta_en_pin                                  0.10             # Pin for beta enable

beta_current                                 1.2             # Y stepper motor current

beta_max_rate                                10000.0          # mm/min



gamma_step_pin                               2.2              # Pin for gamma stepper step signal

gamma_dir_pin                                0.20!             # Pin for gamma stepper direction

gamma_en_pin                                 0.19             # Pin for gamma enable

gamma_current                                1.5              # Z stepper motor current

gamma_max_rate                               100.0            # mm/min



# Serial communications configuration ( baud rate default to 9600 if undefined )

uart0.baud_rate                              115200           # Baud rate for the default hardware serial port

second_usb_serial_enable                     true            # This enables a second usb serial port (to have both pronterface

                                                              # and a terminal connected)

#leds_disable                                true             # disable using leds after config loaded

#play_led_disable                            true             # disable the play led

pause_button_enable                          true             # Pause button enable

#pause_button_pin                            2.12             # pause button pin. default is P2.12

#kill_button_enable                           false            # set to true to enable a kill button

#kill_button_pin                              2.12             # kill button pin. default is same as pause button 2.12 (2.11 is another good choice)

#msd_disable                                 false            # disable the MSD (USB SDCARD) when set to true (needs special binary)

#dfu_enable                                  false            # for linux developers, set to true to enable DFU



# Extruder module configuration

extruder.hotend.enable                          false        # Whether to activate the extruder module at all. All configuration is ignored if false





# Second extruder module configuration

#extruder.hotend2.enable                          false      # Whether to activate the extruder module at all. All configuration is ignored if false







# Laser module configuration

laser_module_enable                           true      # Whether to activate the laser module at all. All configuration is

                                                              # ignored if false.

laser_module_pin                            2.4!             # this pin will be PWMed to control the laser. Only P2.0 - P2.5, P1.18, P1.20, P1.21, P1.23, P1.24, P1.26, P3.25, P3.26

                                                              # can be used since laser requires hardware PWM

laser_module_max_power                       0.8            # this is the maximum duty cycle that will be applied to the laser

laser_module_minimum_power                   0.1

laser_module_tickle_power                    0.1             # this duty cycle will be used for travel moves to keep the laser

laser_module_default_power                   0.1                                                              # active without actually burning

laser_module_pwm_period                      200              # this sets the pwm frequency as the period in microseconds



switch.laserfire.enable                      true             #

switch.laserfire.output_pin                  2.6v            # connect to laser PSU fire (^ = active HIGH, v active LOW)

switch.laserfire.output_type	             digital	      #

switch.laserfire.input_on_command	     M3               # fire laser

switch.laserfire.input_off_command           M5   	      # laser off





# Hotend temperature control configuration

temperature_control.hotend.enable            false       # Whether to activate this ( "hotend" ) module at all.

                                                              # All configuration is ignored if false.



# Hotend2 temperature control configuration

temperature_control.hotend2.enable            false      # Whether to activate this ( "hotend" ) module at all.

temperature_control.bed.enable               false        #All configuration is ignored if false.





# Switch module for fan control

switch.fan.enable                            true             #

switch.fan.input_on_command                  M106             #

switch.fan.input_off_command                 M107             #

#switch.fan.output_pin                        2.4              #

switch.fan.output_type                       pwm              # pwm output settable with S parameter in the input_on_comand

#switch.fan.max_pwm                           255              # set max pwm for the pin default is 255



#switch.misc.enable                           true             #

#switch.misc.input_on_command                 M42              #

#switch.misc.input_off_command                M43              #

#switch.misc.output_pin                       2.4              #

#switch.misc.output_type                      digital          # just an on or off pin







# Switch module for spindle control

#switch.spindle.enable                        false            #



# Endstops

endstops_enable                              true             # the endstop module is enabled by default and can be disabled here

#corexy_homing                               false            # set to true if homing on a hbit or corexy

alpha_min_endstop                            1.24^            # add a ! to invert if endstop is NO connected to ground

alpha_max_endstop                            nc            # NOTE set to nc if this is not installed

alpha_homing_direction                       home_to_min      # or set to home_to_max and set alpha_max

alpha_min                                    0                # this gets loaded after homing when home_to_min is set

alpha_max                                    250              # this gets loaded after homing when home_to_max is set

beta_min_endstop                             1.26^            #

beta_max_endstop                             nc            #

beta_homing_direction                        home_to_min      #

beta_min                                     0                #

beta_max                                     250              #





# optional order in which axis will home, default is they all home at the same time,

# if this is set it will force each axis to home one at a time in the specified order

homing_order                                 XYZ              # x axis followed by y then z last



# optional enable limit switches, actions will stop if any enabled limit switch is triggered

alpha_limit_enable                          true             # set to true to enable X min and max limit switches

beta_limit_enable                           true            # set to true to enable Y min and max limit switches

#gamma_limit_enable                          false            # set to true to enable Z min and max limit switches



alpha_fast_homing_rate_mm_s                  50               # feedrates in mm/second

beta_fast_homing_rate_mm_s                   50               # "

gamma_fast_homing_rate_mm_s                  4                # "

alpha_slow_homing_rate_mm_s                  25               # "

beta_slow_homing_rate_mm_s                   25               # "

gamma_slow_homing_rate_mm_s                  2                # "



alpha_homing_retract_mm                      5                # distance in mm

beta_homing_retract_mm                       5                # "

gamma_homing_retract_mm                      1                # "



#endstop_debounce_count                       100              # uncomment if you get noise on your endstops, default is 100



# optional Z probe

zprobe.enable                                false           # set to true to enable a zprobe





# Panel

panel.enable                                 true             # set to true to enable the panel code

#panel.lcd                                    smoothiepanel     # set type of panel

#panel.encoder_a_pin                          3.25!^            # encoder pin

#panel.encoder_b_pin                          3.26!^            # encoder pin



# Example for reprap discount GLCD

# on glcd EXP1 is to left and EXP2 is to right, pin 1 is bottom left, pin 2 is top left etc.

# +5v is EXP1 pin 10, Gnd is EXP1 pin 9

panel.lcd                                   reprap_discount_glcd     #

panel.spi_channel                           0                 # spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)

panel.spi_cs_pin                            0.16              # spi chip select     ; GLCD EXP1 Pin 4

panel.encoder_a_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                         3.26!^            # encoder pin         ; GLCD EXP2 Pin 5

panel.click_button_pin                      1.30!^            # click button        ; GLCD EXP1 Pin 2

panel.buzz_pin                              1.31              # pin for buzzer      ; GLCD EXP1 Pin 1

panel.back_button_pin                       2.11!^            # back button         ; GLCD EXP2 Pin 8

panel.external_sd                     true              # set to true if there is an extrernal sdcard on the panel

panel.external_sd.spi_channel         1                 # set spi channel the sdcard is on

panel.external_sd.spi_cs_pin          0.28              # set spi chip select for the sdcard (or any spare pin)

panel.external_sd.sdcd_pin            0.27!^            # sd detect signal (set to nc if no sdcard detect) (or any spare pin)







# pins used with other panels

#panel.up_button_pin                         0.1!              # up button if used

#panel.down_button_pin                       0.0!              # down button if used

#panel.click_button_pin                      0.18!             # click button if used



panel.menu_offset                            0                 # some panels will need 1 here



panel.alpha_jog_feedrate                     6000              # x jogging feedrate in mm/min

panel.beta_jog_feedrate                      6000              # y jogging feedrate in mm/min

panel.gamma_jog_feedrate                     200               # z jogging feedrate in mm/min



panel.hotend_temperature                     185               # temp to set hotend when preheat is selected

panel.bed_temperature                        60                # temp to set bed when preheat is selected



# Example of a custom menu entry, which will show up in the Custom entry.

# NOTE _ gets converted to space in the menu and commands, | is used to separate multiple commands

custom_menu.power_on.enable                true              #

[custom_menu.power_on.name](http://custom_menu.power_on.name)                  Power_on          #

custom_menu.power_on.command               M80               #



custom_menu.power_off.enable               true              #

[custom_menu.power_off.name](http://custom_menu.power_off.name)                 Power_off         #

custom_menu.power_off.command              M81               #



# Only needed on a smoothieboard

currentcontrol_module_enable                  true             #





return_error_on_unhandled_gcode              false            #



# network settings

network.enable                               true            # enable the ethernet network services

network.webserver.enable                     true             # enable the webserver

network.telnet.enable                        true             # enable the telnet server

#network.ip_address                           auto             # use dhcp to get ip address

# uncomment the 3 below to manually setup ip address

#network.ip_address                           192.168.3.221    # the IP address

#network.ip_mask                              255.255.255.0    # the ip mask

#network.ip_gateway                           192.168.3.1      # the gateway address

#network.mac_override                         xx.xx.xx.xx.xx.xx  # override the mac address, only do this if you have a conflict






---
**Ben Myers** *May 10, 2017 22:52*

**+Don Kleinschnitz** From you website I believe that I should be able to use the L wire on the Low Power connector connected to 2.4 (fan neg terminal) and pwm that for laser power ?




---
**Ben Myers** *May 11, 2017 11:51*

I think im getting somewhere with 2.4 connected to L. Laser turns on and off as expected but the power level is always the same no mater what is specified in the gcode around 8ma.




---
**Don Kleinschnitz Jr.** *May 11, 2017 19:04*

**+Ben Myers** do you have a pot on your lps? Try adjusting it. 

Btw the actual power is the pots pwm DF * the S pwm DF. 


---
**Ben Myers** *May 11, 2017 23:04*

so the actual amps shown are not correct on the analog meter?


---
**Don Kleinschnitz Jr.** *May 12, 2017 13:00*

**+Ben Myers** the ma on the analog meter are correct. The controllers control of  current is what I am referring to. The pot can override the PWM control of from the controller. 

Turn the pot to its lowest setting and increase it while printing and see if you can go from light to dark, lower current to higher current.

This might be better done by marking a solid fill or grey shade pattern.

Once we get your power under program control we can find out why cutting is not as expected.

All indications are that your PWM is working just not showing it in the print.


---
**Ben Myers** *May 13, 2017 04:22*

I do have a oscilloscope if it helps. So i am going to engrave a solid black rectangle and slowly turn up the pot.


---
**Ben Myers** *May 13, 2017 10:52*

Ok I have pwm working (somewhat) I have changed laser_module_pwm_period from 20 to 200. when it is set to 20 the power option doesn't really make a difference when it is set to 200 it definitely makes a difference. Using the same speed and ma as when I had the M2 nano board it doesnt cut all the way through 3mm mdf as it did with the old board. Any ideas?




---
**Ben Myers** *May 13, 2017 11:09*

looks promising.. Original File

![images/38ade77ca4716debe54232ed24f2b1b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/38ade77ca4716debe54232ed24f2b1b2.jpeg)


---
**Ben Myers** *May 13, 2017 11:17*

Engraved file

![images/baf4196012acd73bde003f5d2c84082d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/baf4196012acd73bde003f5d2c84082d.jpeg)


---
**Don Kleinschnitz Jr.** *May 13, 2017 11:33*

**+Ben Myers** it appears your PWM control is working. Try this pattern at different pot settings.


---
**Ben Myers** *May 13, 2017 14:14*

this one (different to first) was done at laser_module_pwm_period - 400 (last one at 100) and 10ma

![images/d58142af0b74343b7eb9673988dc6336.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d58142af0b74343b7eb9673988dc6336.jpeg)


---
**Ben Myers** *May 13, 2017 14:15*

will keep trying this pattern at different settings




---
**Ben Myers** *May 13, 2017 14:16*

btw I donated to your paypal without you and several other guys, no one would be able to do this.. Many thanks.


---
**Don Kleinschnitz Jr.** *May 13, 2017 19:01*

**+Ben Myers** many thanks?


---
**Ben Myers** *May 13, 2017 23:00*

**+Don Kleinschnitz** Just thanking you for all your hard work you do for this community and helping me with an unsupported board. I really appreciate it.


---
**Ben Myers** *May 14, 2017 10:32*

5ma to 7ma 200mm/s 0.1 laser diameter

![images/cba00f1629a47df288237e89517fce05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cba00f1629a47df288237e89517fce05.jpeg)


---
**Ben Myers** *May 14, 2017 11:37*

9ma 200mm/s 0.2 laser diameter

![images/1a1c066e4b0f1ade9c014da8699a9736.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1a1c066e4b0f1ade9c014da8699a9736.jpeg)


---
**Ben Myers** *May 14, 2017 11:41*

so I am pretty happy with the pwming but my cuts are not as powerful as before. Any ideas **+Don Kleinschnitz**


---
**Ben Myers** *May 15, 2017 00:54*

if I change the laser_module_pwm_period from 400 to 20 the machine cuts perfect again.. but pwm is no good




---
**Don Kleinschnitz Jr.** *May 15, 2017 12:22*

**+Ben Myers** I have never compared cutting on this config vs the Nano setup. 

I am guessing but never proven empirically that the higher frequency PWM is providing a PPI kind of effect when cutting. This is a beneficial artifact of running the PWM period to fast for the lasers response time.

 

However lower period settings are two fast for the information that we are feeding the laser, at these speeds, during engraving. The data in vector cutting is typically slower.



So, lower PWM setting artificially enable cutting with lower power but that same setting is to fast for engraving.



This may be one of the trade-offs with this setup. I suggest cutting down the speed and doing both engraving and cutting comparisons. 



Most users are finding an optimum place to run both.

......

What speed are you running in this comparison



Actually you are the first that has mentioned this, at least that I recall.

What is the difference in current vs cut vs thickness vs speed of the same material?



The variables that I am assuming you have not changed between the before and after cutting comparison are:



...Laser power including any alignment issues

...Surface speed (feed rates)

...Material type and thickness



... Theory trying to be practice ......

Now that I am inside the LPS I recently measured its period to be closer to the the 400 us range which matches my theoretical calculations for the threshold for acceptable engraving.



I have for some time thought that the the single control schema and speeds that we are using marginally meet our expectations for marking quality and speeds. In fact that is what caused me to start the laser control and LPS research that seems to have become an obsession.



The theory and some of the empirical tests that I have done when mixed with some research information suggests that we are expecting the laser to switch to fast.



The stock K40 does not engrave rather it dithers so it does not have the  same problem we are dealing with. Its internal LPS PWM is probably matched to the laser response and it is a constant setting from the pot.



In our design we are sending a PWM signal that controls both power levels and marking data in a single system. That signal is then again modulated by the LPS constant internal PWM. My models show that it is not possible to optimize the laser control with this single control. 



Concepts that can be combined to optimize with a larger PWM period setting:

1. Slow down the feed rates

2. Dither instead of engrave

3. Make PWM a variable control

4. Design a dual power control schema

5. Add PPI control



I have not had time to test the above concepts but I have started work on a design for #4.


---
**Ben Myers** *May 16, 2017 04:58*

**+Don Kleinschnitz** thank you for your detailed reply. I will try to answer all your questions



"What is the difference in current vs cut vs thickness vs speed of the same material?" - I will run these test and post back



<s>------------------------------------------------------------------------------------------------</s>



"The variables that I am assuming you have not changed between the before and after cutting comparison are:"



...Laser power including any alignment issues - Can swap back to M2 Nano board and it cuts fine

...Surface speed (feed rates) - Same as Nano

...Material type and thickness - Same as Nano



<s>------------------------------------------------------------------------------------------------</s>



Concepts that can be combined to optimize with a larger PWM period setting:



3. Make PWM a variable control - Could we have 2 values in the config e.g.

1. laser_module_pwm_cut_period - 20us

2. laser_module_pwm_engrave_period - 400us



We would need to have Laserweb read these settings and apply them to the g-code generator or maybe as you stated just be able to type in each layer of laserweb the pwm period.




---
**Ben Myers** *May 18, 2017 09:11*

**+Don Kleinschnitz** I think I may have figured out why I am not getting decent cuts.. (well you did) I cam across your post about cooling water



 [plus.google.com - COOLING WATER "IT MATTERS" After flailing for a few days with what I thought...](https://plus.google.com/+DonKleinschnitz/posts/jDRGVhd6zqy)



and after changing to demineralised water (couldnt find Distilled)

it is cutting much better. I'm only a few cut in but thr results look much better. You are a genius.


---
**Don Kleinschnitz Jr.** *May 18, 2017 09:56*

**+Ben Myers** I am glad the research has helped you and thanks for following up so that we can see cause and effect.

I think the genius is **+HP Persson** as his tenacity in getting awareness of this problem  drove us to investigate. Also **+Ned Hill**'s chemistry expertise was invaluable in wading through the mire of solutions.



You now join a growing group of believers that "Cooling Water Matters".....


---
**Ben Myers** *May 18, 2017 10:06*

**+Don Kleinschnitz** as an example with the Nano board I cut at 8mm/s @ 16ma. with the new water I now can cut at 8mm/s @ 7ma. Definitely better. 



Still need a better solution to allow for different pwm setting for cut and engrave in laserweb/smoothieboard. 


---
*Imported from [Google+](https://plus.google.com/116976341841265694435/posts/VqyhKTxVaNy) &mdash; content and formatting may not be reliable*
