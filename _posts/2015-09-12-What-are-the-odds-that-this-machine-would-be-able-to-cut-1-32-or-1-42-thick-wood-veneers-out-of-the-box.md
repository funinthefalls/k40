---
layout: post
title: "What are the odds that this machine would be able to cut 1/32 or 1/42 thick wood veneers out of the box?"
date: September 12, 2015 16:46
category: "Discussion"
author: "David Vanderbush"
---
What are the odds that this machine would be able to cut 1/32 or 1/42 thick wood veneers out of the box?  The one upgrade I'd look at immediately would be the air assist.



Thanks,



Dave



[http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/201264099304?hash=item2edc466fe8](http://www.ebay.com/itm/High-Precise-40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-/201264099304?hash=item2edc466fe8)





**"David Vanderbush"**

---
---
**David Vanderbush** *September 12, 2015 19:04*

Sweet, I just pulled the trigger on one, and am now headed to Harborfreight.com for a compressor.


---
**David Vanderbush** *September 13, 2015 00:40*

How about mother of pearl?


---
**Gregory J Smith** *September 13, 2015 11:37*

I can do 7mm plywood with a couple of slow passes. Using home grown air assist.


---
**Layne Brown** *September 26, 2015 21:39*

I got mine a few weeks ago and will be using it for inlay work also.  Keep in touch...we can compare notes.


---
**David Vanderbush** *September 26, 2015 21:58*

**+Layne Brown** will do,  I've got some mixed media things I'd like get going but it may be a while. 


---
**Layne Brown** *September 30, 2015 06:51*

I went ahead and ordered a dsp upgrade for mine so I can get away from Moshi.  So far I've only been able to set it up and tinker a bit.  Between it and my 3d printer, my head is spinning and my spare time depleted working on upgrades and mods between the two. Hopefully I'll have some things sorted out before Halloween.  I'm dying to cut some pearl inlay for a Vintage Takamine guitar I've been refurbishing.  Maybe some marquetry work on it as well.  


---
*Imported from [Google+](https://plus.google.com/111162928678168201894/posts/bnTsPTRjr8V) &mdash; content and formatting may not be reliable*
