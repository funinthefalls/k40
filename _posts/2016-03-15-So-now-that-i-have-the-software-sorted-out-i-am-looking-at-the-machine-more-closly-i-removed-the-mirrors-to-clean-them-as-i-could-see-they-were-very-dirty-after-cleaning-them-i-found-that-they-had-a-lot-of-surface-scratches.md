---
layout: post
title: "So now that i have the software sorted out i am looking at the machine more closly i removed the mirrors to clean them as i could see they were very dirty after cleaning them i found that they had a lot of surface scratches"
date: March 15, 2016 22:37
category: "Discussion"
author: "Dennis Fuente"
---
So now that i have the software sorted out i am looking at the machine more closly i removed the mirrors to clean them as i could see they were very dirty after cleaning them i found that they had a lot of surface scratches will this make the beam disrupted 

Thanks





**"Dennis Fuente"**

---
---
**Scott Marshall** *March 15, 2016 23:01*

It's not ideal, but minor marks are usually not serious. The laser only hits about a 1/8" circle on the mirror, if you do a 'Tape test" as if you're aligning, rotate the mirrors so the good part is where it hits.

A set of mirrors is only about $25 or so.﻿



One test to tell if they are doing harm is to run the laser hard, shut it down quick and feel the mounts. If they're hot, the mirror is absorbing energy.



[http://www.ebay.com/itm/3-Dia-20-25mm-Si-Mo-Reflection-Reflective-Mirror-CO2-Laser-Engraver-Cutter-/252295525503](http://www.ebay.com/itm/3-Dia-20-25mm-Si-Mo-Reflection-Reflective-Mirror-CO2-Laser-Engraver-Cutter-/252295525503)


---
**Dennis Fuente** *March 15, 2016 23:03*

Scott Thanks for the reply where is the best palce to buy a set of mirrors ebay or ?



Thanks

Dennis 


---
**Scott Marshall** *March 15, 2016 23:10*

I linked you Saite Cutter on Ebay, I've dealt with them quite a few times, always happy. 7-10 days to Northeast US.



If you're ambitious, there's guys making them from old Harddrive platters with good success. 


---
**Dennis Fuente** *March 15, 2016 23:15*

Thanks  again I will  try your  test and  see what  happens  first  if the  mirrors  are  bad I will  look into  replacement 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/1yPhRovDkNw) &mdash; content and formatting may not be reliable*
