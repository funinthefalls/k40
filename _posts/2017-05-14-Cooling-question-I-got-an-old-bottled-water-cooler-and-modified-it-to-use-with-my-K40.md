---
layout: post
title: "Cooling question. I got an old bottled water cooler and modified it to use with my K40"
date: May 14, 2017 21:07
category: "Modification"
author: "Martin Dillon"
---
Cooling question.  I got an old bottled water cooler and modified it to use with my K40.  I have 2 questions.  

One, can I oriented my evaporator coils horizontally like shown in the picture or do they need to be vertical?  

Second, is it going to hurt anything to have the condensing coils in the water or do I need to set up some type of exchange system where I circulate my water through a bucket of cold water? I am worried about changing the conductivity of the water by having copper coils in the cooling water.

![images/bb0866afdb9c0e3d66e69444b7876c77.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb0866afdb9c0e3d66e69444b7876c77.jpeg)



**"Martin Dillon"**

---
---
**Cesar Tolentino** *May 14, 2017 22:17*

One. I believe since it's a closed loop system, horizontal or vertical orientation does not matter.



Second. I do not know the answer.


---
**Anthony Bolgar** *May 14, 2017 23:09*

**+Don Kleinschnitz** may know the answer to the conductivity question.


---
**Steve Clark** *May 15, 2017 02:18*

I think Cesar is right that it most likely makes no difference on the refrig. coils.



My opinion about the copper coils is that they will most likely leach out into the coolant water and would cause conductivity issues. It would be interesting to know if Don has any studies with copper immersion over time.



I'm struggling with that issue also and how I'm going to  use my Peltier system as the cooling blocks are aluminum which I believe can cause the same problem.



I was considering stainless coils ...pricey... but if they are passivated correctly then that could prevent that problem. However (Sorry I have to go here) assembly operations that could scratch the surface can cause problems so I’m not sure how I would handle this.



 I mostly settled on just running the coolant through  the Peltier system and I’m looking at epoxy coatings (Maybe 1838 made from 3M) by diluting it in acetone and flushing through the aluminum cooling blocks then force air drying .



 That may be an answer for your copper coils as well… coating them with an epoxy.



  Unfortunately, 1838 epoxy is pricey 50 plus bucks for a 2 oz kit plus shipping so I'm looking for an alternative. 




---
**Ned Hill** *May 15, 2017 02:21*

Chlorine or low pH will accelerate copper oxidation/dissolution.  Just pure water will also cause copper to leach due to dissolved oxygen or absorption of CO2 which can lower the pH.  Not sure of the rate off hand, but it could cause significant conductivity increases overtime.  I'll try and dig into it a little more later.


---
**Martin Dillon** *May 15, 2017 04:24*

Right now I have straight distilled water. I hadn't thought about epoxy coating the coils.  right now they are connected to the stainless cylinder that held the cold water.  I think I will try the epoxy and see how it goes.  

What are the signs of conductive water?  Squealing power supply?


---
**Cesar Tolentino** *May 15, 2017 04:31*

From my experience with Reef Tanks and making it cooler during summer months is I deal it with volume.



I rather have 15 gallons of water vs 5 gallons + evaporator coils. A drop of few frozen bottled water too helps delay the water temp rise. If you can put a 30 gallon brute can at the back instead of 5 gallon bucket, that will make a huge difference. No additional hardware required, simple and nothing to maintain, of course except for the pump.



Also add those cheap digital water temp monitor found on pet stores. At least you have something to monitor with.


---
**Cesar Tolentino** *May 15, 2017 04:35*

Oh, if you worry about the space underneath that cart, you will be surprised how small a foot print of 10 gallon fish tank have or a 15 gallon hight, or a 20 gallon long. Just 2 cents.




---
**Steve Clark** *May 15, 2017 05:00*

One thing to be careful about with a refrigerated system is having your coolant colder than the dew point of the air. According to some Tube manufacturing sources I've read. If the water is too cold then it is claimed that it can cause dew to form on the internal mirrors in you laser tube and this can lead to failure of the tube at the worst. 



You might what to consider putting a thermostat in your system.

 

A larger tank will help cooling as long as the ambient or starting temperature is low. Where you have a problem is on hot nights or higher temps than you want in the tank. 



Ice packs work but they are without temp. control.






---
**Phillip Conroy** *May 15, 2017 09:22*

I had dew form on my laser system the other night ,winter in Australia, at night I suddenly lost all cutting power in the beam.l was in a unheated room and everything fine the ext day


---
**Phillip Conroy** *May 15, 2017 09:32*

Now got one of these

[ebay.com.au - Details about  Thermo-Hygrome<wbr/>ter Temperature Dew Point Relative Humidity Audible Visible Alarm](http://www.ebay.com.au/itm/181781860689?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT)


---
**Martin Dillon** *May 15, 2017 12:59*

I know it has been mentioned before but what is the ideal temp?


---
**Don Kleinschnitz Jr.** *May 15, 2017 13:24*

**+Steve Clark** 

I have not done any tests on the effect of copper on water conductivity. We would need to calculate the area exposed to the water considering the flow rates etc. Could be complicated to get right. 

It might be easier to just do it and measure the conductivity regularly, which I reccomend anyway.



**+Ned Hill** I don't know what the rate of oxidization would be especially considering water flow. I would imagine that the time it takes for the water to become conductive from copper oxidation would be longer than the time between maintenance exchanges anyway.....  but that is a WAG.


---
**Steve Clark** *May 15, 2017 17:19*

**+Martin Dillon** I’ve spent some time reading up on and talking to manufactures recently and there seems to be some minor conflict between them as to the idea temperatures of operation.



 For the most part they all agree it’s generally  acceptable for the tubes to be kept within 20℃ - 31℃.



 However, your relative humidity  controls what the ideal temperature is and those zones could be even  lower if one was able to control the moisture content  by keeping it’s dew point below your operating temperature.

 

The main concern is avoidance of internal mirror condensation and this write up below has a helpful chart (Table 1).



[http://www.synrad.com/Manuals/Technical%20Bulletins/TB12_Prevent%20Condensation.pdf](http://www.synrad.com/Manuals/Technical%20Bulletins/TB12_Prevent%20Condensation.pdf)



 






---
**Martin Dillon** *May 15, 2017 18:26*



**+Steve Clark** thanks for the pdf.  Lots of good info.  From all the feedback, I think I am going to ditch the cooling and go to a bigger reservoir.  I definitely need to invest in a good thermometer.  


---
**Steve Clark** *May 15, 2017 19:35*

I personally would keep your cooling system but you will need to add a thermostat, probe, and relay to turn it on and off as it’s needed to keep you water cool. 



Or just add a temperature probe/readout and a switch to turn it on and off by hand as needed. Got to be careful this way though…a switch with a built in timer might be safer. 



[http://www.ebay.com/itm/Tork-Spring-Wound-Auto-Off-Wall-Box-Mounting-Time-Switch-30-Min-Interval-Timer-/331948926394?hash=item4d49b2a5ba:g:n7EAAOSwV0RXvOH5](http://www.ebay.com/itm/Tork-Spring-Wound-Auto-Off-Wall-Box-Mounting-Time-Switch-30-Min-Interval-Timer-/331948926394?hash=item4d49b2a5ba:g:n7EAAOSwV0RXvOH5)



I’m using these below as I don’t want to spend the time building a controller.



[http://www.lightobject.com/JLD612-DC-12V-Dual-Display-Fahrenheit-Celsius-PID-Temperature-Controller-P443.aspx](http://www.lightobject.com/JLD612-DC-12V-Dual-Display-Fahrenheit-Celsius-PID-Temperature-Controller-P443.aspx)



[http://www.lightobject.com/40A-Solid-State-Relay-SSR-DC-In-DC-Out-P315.aspx](http://www.lightobject.com/40A-Solid-State-Relay-SSR-DC-In-DC-Out-P315.aspx)



[http://www.lightobject.com/10ft-3M-K-Type-Thermocouple-P114.aspx](http://www.lightobject.com/10ft-3M-K-Type-Thermocouple-P114.aspx) 



You will need to buy or make a heatsink for the relay if you go this route.




---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/i8ApRPo5u5m) &mdash; content and formatting may not be reliable*
