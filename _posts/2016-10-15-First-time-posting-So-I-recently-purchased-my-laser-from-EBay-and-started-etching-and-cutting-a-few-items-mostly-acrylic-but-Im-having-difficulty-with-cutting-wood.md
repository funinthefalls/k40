---
layout: post
title: "First time posting So, I recently purchased my laser from EBay and started etching and cutting a few items (mostly acrylic) but I'm having difficulty with cutting wood"
date: October 15, 2016 17:00
category: "Materials and settings"
author: "Chris Kehn"
---
First time posting😀



So, I recently purchased my laser from EBay and started etching and cutting a few items (mostly acrylic) but I'm having difficulty with cutting wood. Specifically plywood. The mirrors are clean and aligned almost perfectly. 



I guess my question is...at what speed and power setting are you guys having the most success? I'm trying to cut 1/8 inch plywood. Also, does anyone have experience in engraving different types of wood? Like, speed/power based on species?



Thanks!



Oh, one thing to keep in mind, I haven't modified anything yet. I'm working with the basic setup.😉









**"Chris Kehn"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2016 17:40*

I would consider checking a) focal distance (needs to be 50.8mm from base of lens to centre of the material for best cut results). Then b) check the lens is oriented correctly (convex side facing up, mine came pre-installed upside-down).



Speeds I use currently for cutting plywood are 6mA power, 10mm/s, 2 passes. You could probably do like 12mA power, 10mm/s, in one pass.


---
**greg greene** *October 15, 2016 20:00*

8 and 8 goes right thru


---
**Chris Kehn** *October 15, 2016 20:14*

Great, thanks for the help guys.



I'll check the focal length and lens orientation.😃




---
**1981therealfury** *October 27, 2016 13:15*

How did it go Chris?  I have a similar issue, its currently taking me 4 passes at 10mm/s with 100% power to cut through 3mm (1/8inch) plywood?  I already checked lens setup, mirror alignment and ramp test and have set the bed so its correct for the narrowest part of the beam but still having issues.


---
**greg greene** *October 27, 2016 13:46*

check also to make sure the beam is hitting in the center of the hole in the head.  If it isn't, then it is likely hitting the side of the head on the inside, and therefore not hitting the centre of the focal lens. Put a piece of paper over the focal lens and see if the beam is coming out of the lens in the center


---
**1981therealfury** *October 27, 2016 13:57*

**+greg greene**

 Thanks Greg,  I checked all the mirrors and the entry to the laser head with masking tape but never though to check the actual lens and exit hole alignment.  Will give that a check over once i get home.


---
**greg greene** *October 27, 2016 14:18*

I had the same problem as you did, and was surprised to find that my mirror at the top of the head reflected the beam so it hit the side of the head going to the lens.  Re orienting the mirror and the arm that holds in place fixed that for me.  I can't stress enough that having the beam strike the centre of all the mirrors is really crucial to optimum operation.


---
**Chris Kehn** *October 27, 2016 16:39*

I think I got everything lined up (took 3 hours) but it seems like the beam is much wider when the software is set to cut rather than engrave. As an example, after some engraving last night, I wanted to then cut out the piece. Once I initiated the cut routine, the beam seemed fairly wide. I'm thinking I still need to tweak the power level setting and speed of the cut.


---
**greg greene** *October 27, 2016 19:28*

It will seem that way because the higher cutting speed will cause the adjoining material to scorch.  The beam width at focus should be close to .150 mm (Thats point 150)  I found this out by cutting finger joint boxes and experimenting with setting the kerf width - I found the best fit at .075 - which is kerf/2. Your results may differ depending on the cut and quality of you focusing lens - mine was from Light Objects.  So for a 'finer' cut use lower power and more passes and see how tht works on the material your using.


---
**Chris Kehn** *October 30, 2016 21:22*

Just an update.



I've been able to cut 1/8 inch hard maple set at 10Ma @ 9mm/s. Pretty good considering my mirrors are not perfectly aligned. I discovered this while cutting out some Christmas ornaments. It seems the left side of my cutting surface is a bit out of optimum focal length. But, I can't complain. 290.00 and a bit of manual tweaking isn't too bad for a laser engraver/cutter in my book!😀


---
**Ariel Yahni (UniKpty)** *October 30, 2016 21:33*

**+Chris Kehn** If you can please share what you have done or whats would you say to others in the same situation


---
**Chris Kehn** *November 02, 2016 16:26*

**+Ariel Yahni** sure thing!



I went back and checked the alignment of the mirrors and made some small adjustments (something like 1/4 turn on one of the mirrors screws). Then, I did a couple of experimental cuts with some 1/8 maple strips, tweaking the cutting speed and power level until I came up with the speed and power settings listed above. Next, I need to do the same thing with plywood and acrylic.



I can't stress enough how important it is to clean and align the mirrors. There are many reasons why these devices are inexpensive and I believe this is one. Also, take your time and don't stress working out the initial bugs. Breathe...alot!


---
**Ariel Yahni (UniKpty)** *November 02, 2016 16:43*

**+Chris Kehn**​ thanks for the input on the above. Could you add with what you cleaned them? 


---
**Chris Kehn** *November 02, 2016 16:45*

Sure. I used isopropyl alcohol. The same stuff you can purchase over the counter at most pharmacies or grocery stores. 


---
*Imported from [Google+](https://plus.google.com/108027001410327769629/posts/YLKvLuYuNag) &mdash; content and formatting may not be reliable*
