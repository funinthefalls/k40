---
layout: post
title: "Here is a shadow box. The background is a laser engraved map on leather with engraved and cut 4mm poplar"
date: June 08, 2016 10:34
category: "Object produced with laser"
author: "Ben Walker"
---
Here is a shadow box.   The background is a laser engraved map on leather with engraved and cut 4mm poplar.  I have biscuits (also produced from the scraps) to make the state float over the map.  

![images/2c6b4e380ebc4752d7fe99380c702e64.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2c6b4e380ebc4752d7fe99380c702e64.jpeg)



**"Ben Walker"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 08, 2016 10:49*

That's cool. It would be interesting to see with some lights shining inwards, casting shadow of the state onto the background.


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/YJrVesBF2Dk) &mdash; content and formatting may not be reliable*
