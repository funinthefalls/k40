---
layout: post
title: "i seem to have lost the stock zero point when i turn I the machine it travels where ever it wants"
date: September 16, 2016 00:16
category: "Original software and hardware issues"
author: "David Spencer"
---
i seem to have lost the stock zero point when i turn I the machine it travels where ever it wants. Before I can cut I have to Manually move it back to the top left corner. any ideas

 





**"David Spencer"**

---
---
**Ariel Yahni (UniKpty)** *September 16, 2016 00:23*

Recheck all your endstops connections, something loose Wil make the stock board go nuts


---
**David Spencer** *September 16, 2016 00:54*


{% include youtubePlayer.html id="zXeYrCl83eI" %}
[youtube.com - September 15, 2016](https://youtu.be/zXeYrCl83eI)


---
**David Spencer** *September 16, 2016 00:55*

I cant find any loose connections




---
**Ariel Yahni (UniKpty)** *September 16, 2016 00:58*

That's shows just a slow move. Maybe you need to check in setting that the correct board type it's selected.


---
**David Spencer** *September 16, 2016 01:08*

I checked that also. It happens when i turn on the machine, then when I try to cut it thinks that is the top left


---
**Ariel Yahni (UniKpty)** *September 16, 2016 01:16*

The I'll say it's one one of the endstop cables. I had the same issue while turning on the head just went elsewhere. 


---
**Ashley M. Kirchner [Norym]** *September 16, 2016 01:26*

If you have an optical end-stop, clean it. My upper left end-stop gets dirty from all the wood cutting and when it can't read anymore, the machine does exactly that what you are experiencing. I used compressed air to blow the dust off.


---
**David Spencer** *September 16, 2016 14:09*

Does the K40 come with optical end-stops?


---
**Ariel Yahni (UniKpty)** *September 16, 2016 14:13*

**+David Spencer**​ some version do


---
**David Spencer** *September 16, 2016 14:16*

well I will check when I get home


---
**Ashley M. Kirchner [Norym]** *September 16, 2016 15:48*

Both mine are optical. I'm planning to replace them with mechanical ones when I upgrade the controller. 


---
**David Spencer** *September 16, 2016 23:07*

**+Ashley M. Kirchner** **+Ariel Yahni** thank you both for the direction to look in solving my problem. It turned out the optical was dirty, I had a piece of paper catch fire just before it started happening, the soot from that must have been the culprit. Thanks again for the help.


---
**Ashley M. Kirchner [Norym]** *September 16, 2016 23:07*

Glad you are back in business.


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/UR3WJoLHM5C) &mdash; content and formatting may not be reliable*
