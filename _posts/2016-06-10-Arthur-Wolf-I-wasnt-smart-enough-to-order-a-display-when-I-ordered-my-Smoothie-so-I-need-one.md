---
layout: post
title: "Arthur Wolf I wasn't smart enough to order a display when I ordered my Smoothie so I need one"
date: June 10, 2016 12:29
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+Arthur Wolf** I wasn't smart enough to order a display when I ordered my Smoothie so I need one.

The GLDC display and shield are out of stock as are the regulators.

Do you have an ETA on new stock?





**"Don Kleinschnitz Jr."**

---
---
**Arthur Wolf** *June 10, 2016 12:38*

**+Don Kleinschnitz** Few weeks I think.


---
**Jim Hatch** *June 10, 2016 12:40*

What kind of display do you need? Can't you use the Smoothie with the standard power meter or were you also upgrading to a digital display at the same time?


---
**Don Kleinschnitz Jr.** *June 10, 2016 13:40*

The GLDC provides a local control panel that interfaces to the Smoothie. Power meter (current meter) etc that are in the K40 are untouched.


---
**Jim Hatch** *June 10, 2016 13:56*

Good. I'm going LaserWeb 2 myself. 😃


---
**Don Kleinschnitz Jr.** *June 10, 2016 16:30*

Laserweb use instead of a control panel is one reason why I decided to go Smoothie+Laserweb :). in fact it is what jerked me away from the LO DSP.

However I also may want to use the machine standalone and may actually move it to my shop which is in the basement which has no PC, Ethernet etc. 

Lastly, it exists therefore I should have it :)



The lack of a GLDC will not slow me down due to laserweb availability.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/U7B2YT2apcf) &mdash; content and formatting may not be reliable*
