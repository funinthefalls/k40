---
layout: post
title: "Engraving stainless steel"
date: December 19, 2015 17:58
category: "Object produced with laser"
author: "Linardas Verkulevi\u010dius"
---
Engraving stainless steel



![images/7863d5e4aa93a28f8dbf0803b686711f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7863d5e4aa93a28f8dbf0803b686711f.jpeg)
![images/19d85f62d054ccfe51ab40bf4054acc8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19d85f62d054ccfe51ab40bf4054acc8.jpeg)

**"Linardas Verkulevi\u010dius"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2015 18:08*

That's interesting. I wasn't sure these machines were capable of engraving stainless steel. Thanks for sharing.


---
**ChiRag Chaudhari** *December 19, 2015 18:16*

Nice what is that white coat ?


---
**Linardas Verkulevičius** *December 19, 2015 18:18*

With this can :) 


{% include youtubePlayer.html id="dT5TH85XpKQ" %}
[https://www.youtube.com/watch?v=dT5TH85XpKQ](https://www.youtube.com/watch?v=dT5TH85XpKQ)


---
**ChiRag Chaudhari** *December 19, 2015 18:20*

Alright, thanks for the info man


---
**Kirk Yarina** *December 19, 2015 19:37*

How does it differ from using spray Moly lube as a marking agent?


---
**Linardas Verkulevičius** *December 19, 2015 19:56*

Sorry I can't compare Moly lube because i haven't used it. But laser scriptor looks pretty good on stainless steel.


---
**Raja Rajan** *December 22, 2015 04:06*

Very interesting and i am new to this laser you mean even cheap chinese laser machine will work with additive spray or we need to have metal laser engraving machine?  


---
**Fab7Studios** *December 22, 2015 06:09*

Anyone have a source for this spray in the USA?


---
**Linardas Verkulevičius** *December 22, 2015 06:16*

Yes, cheap chinese laser (40w CO2) works well with additive spray. I'm using Brasil brand  additive spray: 



"Laser scriptor spray metal/black 300ml"


---
**Linardas Verkulevičius** *December 22, 2015 06:19*

*Brazil ;)


---
**Luis Quintero** *February 14, 2017 06:25*

What rotary can be used for tumblrs with this engraver?


---
*Imported from [Google+](https://plus.google.com/113167710139335239517/posts/epE4U97X9Y6) &mdash; content and formatting may not be reliable*
