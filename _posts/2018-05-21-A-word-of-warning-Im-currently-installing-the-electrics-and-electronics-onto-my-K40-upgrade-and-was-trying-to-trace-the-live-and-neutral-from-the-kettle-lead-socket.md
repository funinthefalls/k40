---
layout: post
title: "A word of warning. I'm currently installing the electrics and electronics onto my K40 upgrade and was trying to trace the live and neutral from the kettle lead socket"
date: May 21, 2018 08:48
category: "Discussion"
author: "Duncan Caine"
---
A word of warning.



I'm currently installing the electrics and electronics onto my K40 upgrade and was trying to trace the live and neutral from the kettle lead socket.  Both L&N cables from the socket to the power switch are blue and both cables from the power switch into the cable harness are red.  I know that the live on the kettle socket is on the left (looking into the socket) this is also where the socket fuse is. Anyway, to cut a long story short, I traced L&N through the cable harness and found that the integral power sockets fitted into the casing are cross wired ie Live into Neutral and vice versa.  Perhaps you electricians can comment on this?





**"Duncan Caine"**

---
---
**Don Kleinschnitz Jr.** *May 21, 2018 12:48*

Not good! 

What voltage is the case sitting at relative to the safety ground?


---
**Duncan Caine** *May 21, 2018 14:11*

**+Don Kleinschnitz** 240v mains supply to the kettle socket.


---
**Don Kleinschnitz Jr.** *May 21, 2018 14:25*

**+Duncan Caine** 

??? Measured from safety ground to the Lasers case is 240V!!! You sure?


---
**Duncan Caine** *May 22, 2018 09:41*

**+Don Kleinschnitz** As you will guess, electrics is not my subject.  What I gave you before was the mains voltage to the laser, obviously not what you are looking for.  So this is now what I did. Connected mains power to the kettle socket, set the multimeter to measure AC voltage, then put one probe on the 'cross wired socket' earth and the other on the casing and the result was 0.  Is this what you wanted me to do?


---
**Don Kleinschnitz Jr.** *May 22, 2018 12:39*

**+Duncan Caine** phew yes that is correct and the case is not floating :)



Some suggestions:



<b>Be careful measuring 240V.</b>



You should swap the neutral and hot in the socket to insure that nothing is left sitting hot. This should not change the operation of anything just makes it correct. Check the web for the correct neutral placement for your type plug.



To insure nothing is floating.I would then test all the surfaces for voltage from their surface to safety ground (earth). Should be 0.



Inspect and test that the connection LPS-FG is connected to FG Earth. With power off is should read 0 ohms.



Trace the earth connection and insure it is solidly screwed to the case somewhere.  


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/6Q2G8Lht3Xi) &mdash; content and formatting may not be reliable*
