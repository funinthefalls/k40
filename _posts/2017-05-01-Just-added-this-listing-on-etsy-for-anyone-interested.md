---
layout: post
title: "Just added this listing on etsy for anyone interested"
date: May 01, 2017 00:39
category: "Materials and settings"
author: "Brian Crowe"
---
Just added this listing on etsy for anyone interested. I have three sizes of light bases for 1/4" acrylic. Includes lighted base in your choice of color and power cord. [https://www.etsy.com/listing/528079931/lighted-base-for-14-acrylic-signs-for-6](https://www.etsy.com/listing/528079931/lighted-base-for-14-acrylic-signs-for-6)

![images/44b21f44a6353e7fbd7544f0263bdf24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44b21f44a6353e7fbd7544f0263bdf24.jpeg)



**"Brian Crowe"**

---


---
*Imported from [Google+](https://plus.google.com/115665123936931789543/posts/VvnCYvHudXe) &mdash; content and formatting may not be reliable*
