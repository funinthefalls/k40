---
layout: post
title: "Has anyone added an adjustable head to their K40?"
date: October 25, 2016 17:58
category: "Modification"
author: "K"
---
Has anyone added an adjustable head to their K40? I want some sort of adjustable Z, and while I like the table idea, an adjustable head seems so much easier. The photo is of the head on Full Spectrum's hobby laser. It seems super simple and smart. Are there any issues with a head like this that I'm just not thinking about? My thought was to take the existing LO air assist head and separate the mirror mount from the lens mount in a similar configuration.



![images/425e35b6f4a40d5f474f05e217a29292.png](https://gitlab.com/funinthefalls/k40/raw/master/images/425e35b6f4a40d5f474f05e217a29292.png)
![images/066595caf2b0c4e28023119cc6ddb839.png](https://gitlab.com/funinthefalls/k40/raw/master/images/066595caf2b0c4e28023119cc6ddb839.png)

**"K"**

---
---
**Jim Hatch** *October 25, 2016 18:07*

You can get an adjustable air assist head on eBay. Believe Saite is the name of it.


---
**Ariel Yahni (UniKpty)** *October 25, 2016 18:08*

I'm in on the idea. Other manufactures also use the same principle


---
**K** *October 25, 2016 18:45*

**+Jim Hatch** I did see one for the K40. I like the challenge of making one myself. That's not to say that I won't end up spending $35 to buy one from eBay in the end though, LOL.


---
**greg greene** *October 25, 2016 21:19*

interesting 


---
**Bob Damato** *October 26, 2016 12:16*

This seems like a good idea. Do you have a link to one online?


---
**Ariel Yahni (UniKpty)** *October 26, 2016 12:26*

[https://goo.gl/MP7VwA](https://goo.gl/MP7VwA)


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/XGzidZ942bb) &mdash; content and formatting may not be reliable*
