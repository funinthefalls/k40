---
layout: post
title: "I'm Just curious but where can you buy a K40 laser unit?"
date: August 17, 2016 15:37
category: "Discussion"
author: "Jonathan Davis (Leo Lion)"
---
I'm Just curious but where can you buy a K40 laser unit? I bought this crazy machine and of course Gearbest sends me a defective Controller board. currently they are refusing to send me the crazy part that i need.





**"Jonathan Davis (Leo Lion)"**

---
---
**Eric Flynn** *August 17, 2016 16:11*

Ebay.   There are many many that ship from the states , for around $330 shipped.  Just search for 40W laser


---
**Jonathan Davis (Leo Lion)** *August 17, 2016 16:36*

**+Eric Flynn** ok? As I only paid about $190 for the thing shipped! Shipping being $30


---
**Alex Krause** *August 17, 2016 18:57*

If it doesn't work it's a 190$ paper weight with a 2.5w laser on it


---
**Jonathan Davis (Leo Lion)** *August 17, 2016 19:00*

**+Alex Krause** yep


---
**HalfNormal** *August 17, 2016 21:09*

**+Jonathan Davis** You can use any CNC controller on it. Some are as cheap as $25 including arduino and driver board.


---
**Alex Krause** *August 17, 2016 21:18*

Yah I have seen the grbl boards for next to nothing 


---
**Jonathan Davis (Leo Lion)** *August 17, 2016 23:07*

**+HalfNormal** that actually are not defective 


---
**Eric Flynn** *August 17, 2016 23:10*

**+Jonathan Davis**   What made you come to the conclusion your device is defective?  I know it had an issue , but are you sure there is actually something defective about it, or just not set up properly?


---
**Jonathan Davis (Leo Lion)** *August 17, 2016 23:12*

**+Eric Flynn** the x-axis port on the controller board doesn't work even by switching the cables 


---
**HalfNormal** *August 18, 2016 00:52*

**+Jonathan Davis** if an axis is not working, it is most likely the driver board. I run an x-carve and have swapped out the K40 guts with an arduino and driver board comparable to the  $25 setups available. It could be the arduino but I doubt it.


---
**Jonathan Davis (Leo Lion)** *August 18, 2016 00:58*

**+HalfNormal** well it is a clone version 


---
**Eric Flynn** *August 18, 2016 01:12*

The driver board should be removable. You could try swapping them for one another to confirm if its the driver or not.


---
**Jonathan Davis (Leo Lion)** *August 18, 2016 01:13*

**+Eric Flynn** I have done that yes and it's the main board itself. **+Ariel Yahni** helped me figure that out. 


---
**Eric Flynn** *August 18, 2016 01:14*

OK, that would indeed confirm that.


---
**Jonathan Davis (Leo Lion)** *August 18, 2016 01:15*

**+Eric Flynn** yep,[https://goo.gl/photos/D3uTBhL31EPLqLsz6](https://goo.gl/photos/D3uTBhL31EPLqLsz6)

I do have a video as well 


---
**HalfNormal** *August 18, 2016 02:32*

That is an arduino nano with pololu style drivers. All easily replaced. That said, might want to replace with genuine parts.


---
**Jonathan Davis (Leo Lion)** *August 18, 2016 02:35*

**+HalfNormal** okay but I'd not think that that would actually fix it 


---
**HalfNormal** *August 18, 2016 12:43*

**+Jonathan Davis** You could also replace it with a Ramps 1.4 board and the laser software that can be found on this board for the same price. Lots of luck to you.


---
**Jonathan Davis (Leo Lion)** *August 18, 2016 16:20*

**+HalfNormal** hmm


---
**Eric Flynn** *August 18, 2016 17:15*

Yea, that would be the best option short of buying a K40.


---
**Jonathan Davis (Leo Lion)** *August 19, 2016 01:35*

**+Eric Flynn** understandable, as I'm fighting Gearbest to get the part I need replaced at their expense via warranty. Cause so far I'm getting the impression that they are unwilling to admit they made a mistake and are now just trying to weasel out of making good on the purchase!


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/Sx4cmzCnhQi) &mdash; content and formatting may not be reliable*
