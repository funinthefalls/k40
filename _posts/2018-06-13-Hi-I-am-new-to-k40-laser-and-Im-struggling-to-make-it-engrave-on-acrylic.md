---
layout: post
title: "Hi. I am new to k40 laser and I'm struggling to make it engrave on acrylic"
date: June 13, 2018 12:10
category: "Software"
author: "Silviu-Alexandru Danila"
---
Hi.

I am new to k40 laser and I'm struggling to make it engrave on acrylic. When I engrave on glass the image it looks nice and white but not the same on clear acrylic.  I have tried to use the plugin for CorelDraw but is not working so I am using the laserDRW software. 

Please can someone help me with some advice what can I do?







**"Silviu-Alexandru Danila"**

---
---
**HalfNormal** *June 13, 2018 12:43*

There are a lot of different types of acrylic. Cast acrylic will engrave nicer than extruded acrylic. It will take a lot of experimenting to find the sweet spot.


---
**Silviu-Alexandru Danila** *June 13, 2018 20:28*

**+HalfNormal** 

Thank you for your reply. 

So the secret is the focal distance?




---
**Joe Alexander** *June 13, 2018 21:52*

focal distance along with speed and power level most likely. But as **+HalfNormal** said the type of acrylic is important. one engraves much better than the other.


---
*Imported from [Google+](https://plus.google.com/111553264877097197981/posts/BBS4cWCtctt) &mdash; content and formatting may not be reliable*
