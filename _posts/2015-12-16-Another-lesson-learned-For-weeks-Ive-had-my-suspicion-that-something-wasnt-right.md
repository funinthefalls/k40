---
layout: post
title: "Another lesson learned: For weeks I've had my suspicion that something wasn't right"
date: December 16, 2015 06:41
category: "Hardware and Laser settings"
author: "Ashley M. Kirchner [Norym]"
---
<b>Another lesson learned:</b>



For weeks I've had my suspicion that something wasn't right. Despite everything being aligned, albeit a tad high by the time it reaches the head mirror, something was still nagging at me. Part of that nagging was that I 3D printed an air nozzle and I was always putting it on at a slight angle otherwise the laser beam would hit the edge of the hole ... but how to figure that out.



So today I 3D printed a 50mm long tubular extension that went on the downward portion of the head. Stuck a piece of tape at the very bottom and fired the laser. Sure enough, nowhere near center. The beam was coming out at an angle out of the head. With the stock build, there's not a whole lot you can do. The mirror at the top doesn't have any kind of adjustments but one <i>can</i> loosen and rotate the head piece holding the mirror. So, a bit of fiddling with that and I managed to get the beam as close to center as I could. It's still off by a few mm, but certainly better than it was and looking at the test holes, they're a lot straighter through the acrylic than before.



But now this brings me to another realization and another point of improvement: that head mirror. It would be nice if that too can be adjusted properly. So I'm looking around for mounts and I may as well replace all of them at the same time. The ones the machine comes with are so flimsy, the side one is constantly vibrating out of alignment. Rather annoying.





**"Ashley M. Kirchner [Norym]"**

---
---
**ChiRag Chaudhari** *December 16, 2015 07:42*

Very interesting and informative.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 10:01*

Yeah it does seem that the stock chassis & mounts do cause a few problems with alignment of the beam. It would be nice to rebuild the entire chassis using a carpenter's square haha.


---
**Gary McKinnon** *December 16, 2015 11:38*

Good info, thanks.


---
**Ashley M. Kirchner [Norym]** *December 16, 2015 15:35*

**+Yuusuf Sallahuddin**​, that's one of the first things I did when I got the machine. Before I even started using it, I took out the rails and straightened them. It was badly out of shape. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 15:43*

**+Ashley M. Kirchner** Smart move. I didn't & spent days instead trying to align my mirrors haha.


---
**Ashley M. Kirchner [Norym]** *December 16, 2015 18:19*

Yeah, the rails were pretty bad. THE rear one on mine is still out of whack (it's actually bent), but the side ones are parallel and level, so the gantry moves nice and smooth without twisting. Even the gantry itself was twisted on one side. But then, what would you expect for something that cheap ... :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 16, 2015 19:08*

**+Ashley M. Kirchner** Perfectly true of cheap products. Generally results in less attention to detail. I'm curious, when you pulled all the rails out, do you have to remove all the electrical/mechanical components at the same time? I have avoided removing my rails as I dread breaking something in the electrical/mechanical section (or having leftover screws when I reassemble, as I usually do).


---
**Ashley M. Kirchner [Norym]** *December 16, 2015 19:15*

Nope, not all of it. The Y-axis stepper is in the front right, so I unly unplugged that one from the control board - it's the plug one, not the ribbon cable. Under the front and back rails are two bolts (four total). Once you undo those, the whole thing lifts up. The X-axis stepper has that long ribbon cable, so as long as you're careful you can lift the whole thing out of the chassis and lay it down on the table.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/BQQXur1aEfy) &mdash; content and formatting may not be reliable*
