---
layout: post
title: "I ve found a guy that is selling an old chinese laser that looks like the k40 but has a bigger cutting area 50x40cm or 50x30cm (he uses both specs in his ebay ad)"
date: March 25, 2017 21:26
category: "Discussion"
author: "Lucian Depold"
---
I ve found a guy that is selling an old chinese laser that looks like the k40 but has a bigger cutting area 50x40cm or 50x30cm (he uses both specs in his ebay ad). The machine is about 6 years old and he paid about 1400$... He wants 280$ for it. Does someone know more about this machines with a bigger area ? I suspect that the machine could be from a time where the quality was better, because they did not save money with a smaller cutting area... 



Oh and the laser works and he says that it has only 30w so maybe i should have called it K30...







Lucian





**"Lucian Depold"**

---
---
**Lucian Depold** *March 25, 2017 23:15*

![images/cdd8f280aad6d6753e5bbd68abe3c7d0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdd8f280aad6d6753e5bbd68abe3c7d0.jpeg)


---
**Ned Hill** *March 25, 2017 23:30*

K40 is a bit of a misnomer as the tubes are really more like 30-35W new.  The only thing I would say to watch out for is the laser tube given the stated age of the machine.  Tubes are consumables and will degrade with use and age.  If it is the original tube the output maybe considerable less than a new one and you may end up needing to replace it depending on what you are going to be doing with the machine.


---
**Lucian Depold** *March 25, 2017 23:36*

For my purpose even 10W would be ok. I am happy that i found a machine with such a big cutting area at that price. I am planning to upgrade it with LaOS or Coherent. I can not deside yet.


---
**Claudio Prezzi** *March 26, 2017 11:06*

I would go for it. The tube can be replaced if needed. The bigger work area is realy nice.


---
*Imported from [Google+](https://plus.google.com/105895976959355796398/posts/HCvxGB4igRv) &mdash; content and formatting may not be reliable*
