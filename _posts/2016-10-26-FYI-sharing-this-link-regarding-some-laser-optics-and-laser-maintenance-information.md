---
layout: post
title: "FYI sharing this link regarding some laser optics and laser maintenance information"
date: October 26, 2016 14:08
category: "External links&#x3a; Blog, forum, etc"
author: "Don Kleinschnitz Jr."
---
FYI sharing this link regarding some laser optics and laser maintenance information.



[http://www.parallax-tech.com/faq.htm](http://www.parallax-tech.com/faq.htm)



There is always discussion on these forums about cooling water.

From this source:

If you are using city water, filter it.

If you are using a closed circuit flow system, check the water and change it every six weeks or so, or drop a few drops of Algaecide in the recirculator. Better yet, use diluted Dow Frost.









**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/D9BVCQDEtUK) &mdash; content and formatting may not be reliable*
