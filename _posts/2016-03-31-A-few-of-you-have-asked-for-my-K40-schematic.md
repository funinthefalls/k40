---
layout: post
title: "A few of you have asked for my K40 schematic"
date: March 31, 2016 12:34
category: "Modification"
author: "Don Kleinschnitz Jr."
---
A few of you have asked for my K40 schematic.

I shared it at this location:

[http://www.digikey.com/schemeit/#2ek9](http://www.digikey.com/schemeit/#2ek9)



Let me know if you cannot get to it. You can access it here directly and that way you get changes or you can export and save to a .pdf.



Note: the hacks I have made to my unit are included such as the wiring for the temp display/alarm and flow switch.





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/NVv8mpzHdxW) &mdash; content and formatting may not be reliable*
