---
layout: post
title: "I was having an old CPU cooler laying around and i want to make a water cooling for the laser what's the optimal temperature for the water ?"
date: January 09, 2017 09:42
category: "Modification"
author: "Kostas Filosofou"
---
I was having an old CPU cooler laying around and i want to make a water cooling for the laser what's the optimal temperature for the water ?  



![images/8fc46cc5a9575f2c2a71f7bc589f0020.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fc46cc5a9575f2c2a71f7bc589f0020.jpeg)
![images/3efde12c08b3f116b83bface9d603fa2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3efde12c08b3f116b83bface9d603fa2.jpeg)

**"Kostas Filosofou"**

---
---
**HP Persson** *January 09, 2017 10:32*

You cannot cool the water to perfect temps with a cooler like that.

If you are lucky, you get ambient temp +5c or similar.

But it´s a cool project, you learn alot with tinkering with them :)



I keep my water temp to 18c, i would say 15-20c is ok temps.


---
**Kostas Filosofou** *January 09, 2017 10:36*

**+HP Persson**​​ I use also temperature controller like this one .. my cellar is pretty cool already.. I thing I can achieve this temperatures..





![images/d9fd7066f07a4dd2ddb01c2f3bbc8946.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d9fd7066f07a4dd2ddb01c2f3bbc8946.jpeg)


---
**HP Persson** *January 09, 2017 10:42*

As you are cooling the fins with ambient temp, the temp inside cannot get lower than that.

And thoose coolers are pretty small fin area, removing 40w constant heat is not close what they can do, maybe 5-10 watts.



Add a second fan on the other side, so you get one fan pushing, one fan pulling, you get some better results. Or put it outside the window ;)



To get more heat away from the water you need a radiator, the bigger the better. Or a real cooler, like peltier or refrigirator compressor.

I use a 240mm rad, but have it outside my window to cool it as ambient isnt enough in my room.

![images/ad8d00e8abf0f8658abece8db797690b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad8d00e8abf0f8658abece8db797690b.jpeg)


---
**Kostas Filosofou** *January 09, 2017 10:54*

Ah ok .. I got it .. I'll give it a try tho now my room temperature is about 16°c and the summer is about 20-25°c 


---
**Stephane Buisson** *January 09, 2017 12:19*

from tube specs optimum value is 15°C


---
**HalfNormal** *January 10, 2017 00:25*

Some people have reported condensation on the tube when the water is cold and there is high humidity.


---
**Don Kleinschnitz Jr.** *January 10, 2017 16:36*

**+Niels Jensen** some operating specs on similar tube as a reference: 

 

[http://www.recilaser.com/en/productInfo/fc9181e840aa427d0140aa55363800f1.htm](http://www.recilaser.com/en/productInfo/fc9181e840aa427d0140aa55363800f1.htm)



"The operating environment: temperature 2-40℃; humidity 10-60%."


---
**Abe Fouhy** *February 19, 2017 16:00*

**+HP Persson** Do you have specs on the peltier coolers to use?


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/K9dnSMDtEFr) &mdash; content and formatting may not be reliable*
