---
layout: post
title: "Are there clear instructions for removing the frame and squaring"
date: February 16, 2016 09:01
category: "Discussion"
author: "Tony Schelts"
---
Are there clear instructions for removing the frame and squaring.  preferably with pictures. Thanks 





**"Tony Schelts"**

---
---
**3D Laser** *February 18, 2016 03:53*

If you find one please let me know I am looking for one as well my laser is in a few pieces now as I took it apart to cut down the exhaust.  Now I got to get it squared and aligned should be painstakingly fun 


---
**Tony Schelts** *February 18, 2016 09:22*

can you send instructions for removing the frame please


---
**3D Laser** *February 18, 2016 12:03*

It's easy 

1.  Use an Allen wrench to remove the five screws from the bed silver bed so that it can be removed.  

2.  Use an Allen wrench to remove the black plate at the bottom this is covering access to the two booths that are holding the frame down

3.  Use and Allen and lift up the machine to unscrew the five post that where holding up the z bed.  Access to these are on the bottom of the machine 



(I replaced my bed with a grate I cut down from Home Depot.  And used these post and some washers to support it.)



4.  Use a pair of pliers to remove the four bolts holding the X and y axis down



5.  Disconnect from the board (the ribbon cable just pulls out)



6.  Gently lift it out and be sure not to knock the mirrors 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/JAN3QnB8w8J) &mdash; content and formatting may not be reliable*
