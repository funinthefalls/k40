---
layout: post
title: "Anybody use anything like this for keeping the tube cool"
date: August 08, 2016 17:24
category: "Modification"
author: "David Spencer"
---
Anybody use anything like this for keeping the tube cool

[http://www.viaaquaoceanpure.com/heatcool/ViaAqua_chillers_models.html](http://www.viaaquaoceanpure.com/heatcool/ViaAqua_chillers_models.html)





**"David Spencer"**

---
---
**Eric Flynn** *August 08, 2016 18:47*

Ive used them before for other stuff.  It will work just fine.  However, it is a bit overkill on a 40W tube unless you intend to run it at 100% duty cycle for extended periods.


---
**Phillip Conroy** *August 08, 2016 19:22*

I run a undersink water chiller and it cools very well,i have to turn the chiller off after about an hour as it cools below 15 deg,i am cuttong for 3 hours at a time so needmore cooling than most people ,the chiller cost me $20 second hand Anc a new cost of over $1200 of hand i think it is 500 watts


---
**David Spencer** *August 08, 2016 19:47*

I can pick it up used for 40 dollars


---
**Phillip Conroy** *August 09, 2016 06:51*

only partial flow will go through chiller so you will need an extra pump


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/3eSwc7xeykL) &mdash; content and formatting may not be reliable*
