---
layout: post
title: "Hey, I think I solved my laser tube bubble problem"
date: February 01, 2018 02:49
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Hey, I think I solved my laser tube bubble problem. 

Simpler than you think!



I also completed a K40 cooling circuit upgrade as part of the project to move and make my K40 mobile in my shop ..



Details here: 

[http://donsthings.blogspot.com/2018/01/improved-k40-cooling-circuit.html](http://donsthings.blogspot.com/2018/01/improved-k40-cooling-circuit.html)








**Video content missing for image https://lh3.googleusercontent.com/-BFjKwFSo3m8/WnKATbuelkI/AAAAAAAAy2I/9CQRa0isbSEmYgrWGzAi_ZTgLHn_PQ8SgCJoC/s0/20180131_105752.mp4**
![images/913e2c373fdbeaeeac43862d57844dc5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/913e2c373fdbeaeeac43862d57844dc5.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Paul de Groot** *February 01, 2018 05:51*

Maybe i missing something very obvious but what is the solution. Did you make that impeller youself? Impressive, if i would build it, it would leak for sure  😂


---
**Don Kleinschnitz Jr.** *February 01, 2018 12:47*

**+Paul de Groot** check out the link to the blog in the post above. The simple version is I put a drain in the bucket :)!

Nah the impeller is purchased .....


---
**Ned Hill** *February 01, 2018 20:53*

Nice post and thanks again for sharing your work.  I see you got the air flow meter as well :)


---
**Don Kleinschnitz Jr.** *February 01, 2018 21:06*

**+Ned Hill** yes I really like that meter, thanks for pointing me to it! BTW I tried to use it as a water flow indicator but it is to restrictive .....


---
**Printin Addiction** *February 03, 2018 01:53*

**+Don Kleinschnitz** What kind of plastic fittings are on the water indicator, mine came with huge metal ones, that were too big for the tubing.


---
**Don Kleinschnitz Jr.** *February 03, 2018 02:56*

**+Printin Addiction** mine came with none. These are 1/4 NTP nylon I think, from home depot.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/iymcpArZy1F) &mdash; content and formatting may not be reliable*
