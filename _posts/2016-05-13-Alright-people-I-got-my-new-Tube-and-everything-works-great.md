---
layout: post
title: "Alright people, I got my new Tube and everything works great"
date: May 13, 2016 09:35
category: "Hardware and Laser settings"
author: "Gunnar Stefansson"
---
Alright people, I got my new Tube and everything works great.



I found out why my previous tube stopped working. (Look at images and find 1 error) :)



Now my qustion to you guys is can I somehow change that mirror or will the GAS escape as soon as I pull that mirror off? 



Any suggestions? :) 



![images/24e695b11f7d245a404931d038708210.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24e695b11f7d245a404931d038708210.jpeg)
![images/f1a4ccbb0020ad2314654c5b810b7357.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1a4ccbb0020ad2314654c5b810b7357.jpeg)
![images/1650cc836c8206d2ba07809aa1bde5d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1650cc836c8206d2ba07809aa1bde5d9.jpeg)

**"Gunnar Stefansson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 10:08*

Is that a hole in the mirror? Not certain about changing the mirror. Hopefully someone knows as it could come in handy in future.


---
**Phillip Conroy** *May 13, 2016 10:16*

I have a tube that has cracked glass,end mirror should be ok- where are u


---
**Gunnar Stefansson** *May 13, 2016 10:57*

**+Yuusuf Sallahuddin** Yes it's a hole in the mirror! so that's why I didn't get the effect that I should have... there's must have been impurities on the mirror from the start...



**+Phillip Conroy** I'm located in denmark, and will definetly be happy if I could get that unused mirror of yours, but I want to see if it is possible to replace it in the first place...



Personally I don't believe it is possible, as the photons need to hit the mirror... or else there is a glass sheet in between the mirror and to the inner gasses. but that wouldn't be effective! 



If anyone knows, please do let me know ;)


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/dj2229cwUmz) &mdash; content and formatting may not be reliable*
