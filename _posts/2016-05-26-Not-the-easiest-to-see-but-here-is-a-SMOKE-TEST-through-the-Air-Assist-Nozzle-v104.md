---
layout: post
title: "Not the easiest to see, but here is a SMOKE TEST through the Air-Assist Nozzle v1.04"
date: May 26, 2016 07:33
category: "Air Assist"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Not the easiest to see, but here is a SMOKE TEST through the Air-Assist Nozzle v1.04.



I tried incense through the air-pump & it didn't show any visible smoke. Then I had a brilliant idea to just light a cigarette & blow the smoke through the pipes. It shows vaguely what is going on with the smoke as it exits, but I guess the PSI of my breath through tiny pipes is not correct in comparison to the pumps. The smoke should funnel in further away from the nozzle end-point (at 50.8mm from the lens, which would be about 40.8mm from the base of the nozzle).





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/QHZTHdcDHEV) &mdash; content and formatting may not be reliable*
