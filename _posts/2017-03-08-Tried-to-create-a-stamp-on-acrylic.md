---
layout: post
title: "Tried to create a stamp on acrylic"
date: March 08, 2017 06:00
category: "Discussion"
author: "Marvin Pagaran"
---
Tried to create a stamp on acrylic. I notice the engraved part is not smooth. Is this normal? I ran it at 50% 200mm/s



![images/933accb8b958127e91d7f0c18acb2ae2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/933accb8b958127e91d7f0c18acb2ae2.jpeg)
![images/8250b0110c105ee871f1e7d86927ffb3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8250b0110c105ee871f1e7d86927ffb3.jpeg)

**"Marvin Pagaran"**

---
---
**Alex Krause** *March 08, 2017 06:17*

Need an picture that is in focus to see what you are referring to...


---
**Andy Shilling** *March 08, 2017 22:26*

The laser basically evaporates the acrylic so you won't get a smooth finish on that, if you can get some natural linoleum flooring this can make fantastic stamps but you must make sure it's the original linoleum and not just floor vinyl as this gives off a poisonous gas when cooked with a laser.


---
**1981therealfury** *March 10, 2017 13:18*

I have done quite a lot of engraving with Cast Acrylic and can confirm that the engraved area is never flat, it will be full of tiny ridges that follow the horizontal plane of the engrave I'm afraid.  You can either reduce them or increase them by (presuming its fully stock) changing the pixel width of each pass over the material, in laserdraw its above the setting for repeating the job X times.   setting of 1 gives the smoothest finish but takes a lot longer and is still not fully flat.


---
*Imported from [Google+](https://plus.google.com/107482131084712765082/posts/37CaphWkvCD) &mdash; content and formatting may not be reliable*
