---
layout: post
title: "Has anyone used SVGNest - ? Seems it is useful to maximise the use of material when cutting"
date: January 22, 2016 11:51
category: "Software"
author: "Pete OConnell"
---
Has anyone used SVGNest - [https://github.com/Jack000/SVGnest](https://github.com/Jack000/SVGnest) ? Seems it is useful to maximise the use of material when cutting. Just want to see if anyone has put it to good use?





**"Pete OConnell"**

---
---
**Stephane Buisson** *January 22, 2016 12:27*

Good find , thank's **+Pete OConnell** to reveal that to the community.



SVG is one way to go (like visicut), pdf, hpgl, ... to unleash the design from the CAD software.

NESTING is a nice addition, indeed. (LaserWeb **+Peter van der Walt** )


---
**Chris Brick** *January 22, 2016 16:15*

I was about to post this. You beat me to it. 


---
**Stephane Buisson** *January 22, 2016 16:17*

**+Jack Qiao** is now in contact with **+Peter van der Walt** 


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/YxH4vwEokCw) &mdash; content and formatting may not be reliable*
