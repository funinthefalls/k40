---
layout: post
title: "Ive just broken a mirror! Are there any people that stock 20mm mirrors in the uk that I should call upon or is it just pot luck on ebay?"
date: November 27, 2016 20:36
category: "Original software and hardware issues"
author: "Andy Shilling"
---
Ive just broken a mirror!  Are there any people that stock 20mm mirrors in the uk that I should call upon or is it just pot luck on ebay?





**"Andy Shilling"**

---
---
**Ulf Stahmer** *November 28, 2016 17:56*

You could always make your own! [imajeenyus.com - Hard disk drive mirrors for CO2 laser cutter](http://imajeenyus.com/optical/20140813_hdd_mirrors/index.shtml)


---
**Andy Shilling** *November 28, 2016 19:06*

lol already have, I've just finished it. photos coming up in a bit once I've checked it out.


---
**Andy Shilling** *November 28, 2016 22:14*

Ok So I went down the route of making a new mirror. Below is the link to my gdrive to show the photos if anybody else wants to have a go.



Any questions  just Ask but I think the photos show the process quite well.



[drive.google.com - Laser - Google Drive](https://drive.google.com/drive/folders/0B4PVxTUHzQtfUGNGUHZZanliNVU?usp=sharing)


---
**Ulf Stahmer** *November 29, 2016 02:51*

Great job! How well do they work?


---
**Andy Shilling** *November 29, 2016 06:16*

Yes much better than I could have hoped for. I probably won't bother getting a new one now.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/PVQta3akDaB) &mdash; content and formatting may not be reliable*
