---
layout: post
title: "Could do with some CorelLASER Help! When I use CorelLASER I make one layer for the Cut and another layer for the Engrave, but CorelLASER always takes the top left most pixel or vector as its starting point, so the cuts and"
date: May 06, 2016 00:20
category: "Software"
author: "Pigeon FX"
---
Could do with some CorelLASER Help! 



When I use CorelLASER I make one layer for the Cut and another layer for the Engrave, but CorelLASER always takes the top left most pixel or vector as its starting point, so the cuts and engrave layer don't line up, is there any way around this other then putting a registration mark on the top left of both layers?





**"Pigeon FX"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 01:34*

Pretty much no way around it (that I'm aware of) other than the registration mark as you mentioned. I always put a 1mm x 1mm square registration on all my layers. Others have said you can use 1 pixel, but I can't see 1 pixel unless zoomed in a lot, so I use the 1mm2.


---
**Scott Marshall** *May 06, 2016 01:36*

I'm pretty sure that's a bug (like they did't even address it) in the firmware.


---
**Jim Bennell (PizzaDeluxe)** *May 06, 2016 06:22*

I've been meaning to make a tutorial for my work flow, there is a work around. Here's an explanation of what I do. 



I'll start with making layers in Inkscape - engrave - inner cuts - outer final cuts. Ill make more layers if I need objects cut out inside of another object  in a specific order. You can separate the objects in Corel as well if you don't do it ahead of time I just find working in layers from the start is nice. 



Ok so now the part about having things line up correctly in Corel when you go to do your engrave/cut processes. You usually want to start with your engrave process if you have one(if not start with your first cut layer). Now you want to go to all the other layers (cut layers) and select all their objects and turn fill and line weight off. Now they will be invisible and Corel will disregard them while engraving. But they will still be there on the page you just cant see them and Corel will not lase them. Everything will line up correctly now.



After the first job process I'll turn off all my engrave layer fill and line weights the same way and go to the next layer usually Cut1 and ill change its fill to black but ill leave the line weight off. It seems to help the double cut issues. And ill run the rest of my cut jobs the same way. 



Doing the cuts this way with black fill and no lines seems to help the cut process and visually see what each job will be in case I accidentally left one object on the wrong layer. 



Also if your object isn't closed you will get those double cuts I believe and the object probably wont fill black. You can close/weld verts in Corel if you use the node editing tool , I just select all the nodes right click on one of them and click join . You can join nodes in Inkscape or what ever program you use as well. 



Also There are the CorelLaser settings where you can set the engraving and cutting area and pixel size. I always run my engrave at 2 and the cut at 1 pixel. and area set to current page.



I tried to just turn off different layers using the eye, or setting them to not print, and also trying to use the option of "only selected" instead of current page in the laser data settings. Nothing worked except for the above method of turning off fill and lines for layers you don't want processed at that time. 



Hope that helps, I been meaning to make a Youtube tutorial for my workflow, ill try to get on it soon.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 08:24*

**+Jim Bennell** Thanks for sharing that. It is a good idea to leave all the layers in place & just change their fill to none & line width to none, then it should register the top left most corner pixel for any layer (provided they are all selected or you use current page as you said). I will give it a go next time I do a engrave/cut combo.


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/eSgbu92757J) &mdash; content and formatting may not be reliable*
