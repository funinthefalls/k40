---
layout: post
title: "Not sure if any of you have come across a you tuber by the name of \"Sarbar Multimedia\", but he has done some in-depth analysis of laser engraving setup"
date: June 10, 2016 17:07
category: "Discussion"
author: "Ben Marshall"
---
Not sure if any of you have come across a you tuber by the name of "Sarbar Multimedia", but he has done some in-depth analysis of laser engraving setup. Although he is using a "50w" (upgraded to 60+w eventual), He hypothesizes and then tests his steps. Very good source on fundamental ways to improve most Chinese lasers.





**"Ben Marshall"**

---
---
**Anthony Bolgar** *June 10, 2016 17:22*

Empirical evidence is the only way to make power and speed decisions for your laser. Every ones will be slightly different, so make the time to do some experiments with different materials to come up with a list of settings that work well with your machine.


---
**Jim Hatch** *June 10, 2016 20:56*

second these! He's got a lot of good ones up there. Some of the software stuff doesn't specifically apply to our use of LaserDrw or CorelLaser (he had RDWorks) but they're still good tutorials and intros. He's got something like 70 videos out there.


---
**Ben Marshall** *June 10, 2016 21:34*

I really like how he goes above and beyond to prove his theories about how his engraver functions


---
**Don Kleinschnitz Jr.** *June 10, 2016 22:06*

I have watched them all and also connected with him on a few ideas. I like that he analyzes stuff as an engineer using tests and data.




---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/2kRoHTKfYAV) &mdash; content and formatting may not be reliable*
