---
layout: post
title: "I hope is not completly dead,tomorrow i need to replace fuzz in my psu and see whats happen"
date: May 16, 2016 12:27
category: "Discussion"
author: "Damian Trejtowicz"
---
I hope is not completly dead,tomorrow i need to replace fuzz in my psu and see whats happen.

Why its always like that when you get all to work(smoothie,display,full power spectrum)

![images/7fac5d6cb1af5166c2b8f91a0d73071b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7fac5d6cb1af5166c2b8f91a0d73071b.jpeg)



**"Damian Trejtowicz"**

---
---
**Damian Trejtowicz** *May 16, 2016 12:50*

I need right rating of this fuse,so if any have ,can it share please?


---
**Anthony Bolgar** *May 16, 2016 23:50*

What PSU do you have?


---
**Damian Trejtowicz** *May 17, 2016 06:13*

MYJG40W, i think its old type with separate fire and pwm pins


---
**Anthony Bolgar** *May 17, 2016 09:17*

The fuse should be on the Neutral wire of the mains, So if it is 220V, a 3Amp fuse should be about right. Easy way to check is to solder in a fuse holder, put in a 3A fuse, power it up and see if the fuse blows. IF it doesn't then slowly raise the cutting power to maximum, if fuse does not blow, you should be good to go, if it blows try a 4A fuse. I would not raise the fuse amperage to higher than 4A without knowing the actual original rating.


---
**Damian Trejtowicz** *May 17, 2016 09:33*

Im wondering,maybe it was overloaded,now i wil try to use separate psu forsmothy and motors and separate for lasertube.finger cross fuse holder soldered,now time for tests 


---
**Damian Trejtowicz** *May 17, 2016 09:42*

No lucky,4a fuze just blow off,looks like psu is dead. Now jow to disconnect lasertube?just cut  red cable from psu or other way?


---
**Anthony Bolgar** *May 17, 2016 09:47*

PSU may not be dead, there may be a short circuit in your wiring. Doublecheck all connections before doing anything else.


---
**Damian Trejtowicz** *May 17, 2016 09:49*

I checked all,there was no connection to psu except main,so im sure there is no short circut at all.

Laser tube looks fine too


---
**Anthony Bolgar** *May 17, 2016 09:57*

You can always try a 10 Amp fuse, at this point you have nothing to lose,  if the PSU is fried, it will not make things worse, if it works then problem solved.


---
**Damian Trejtowicz** *May 17, 2016 10:06*

Not sure about this,because i point when fuzz in psu gone my mains braker in house gone too.i already wrote to my k40 seller and se said they know about this problem(so looks like im nit first) and they i think will send me another psu.

But im wodering about it can be made by laser tube?i mean laser tube burn psu?


---
**Anthony Bolgar** *May 17, 2016 12:45*

In very rare cases a bad laser tube can damage a PSU. But very unlikely. I would now wait for the seller to send you a new PSU


---
**Alex Hodge** *May 18, 2016 22:21*

**+Anthony Bolgar** fire. he has his house to lose. if you're blowing fuses its never a good idea to just put a bigger fuse in.


---
**Damian Trejtowicz** *May 18, 2016 22:43*

Seller is sending me new psu,he told me they had that problem few times before but i need to wait about month for new part.i think my psu was overheated because it was very hot near ,looks like fan stop working

When i get new psu i will use 2 psu's 

One gor smoothie and drive and other only for laser tube

I just wondering should i connect all minus cables to one point


---
**Anthony Bolgar** *May 18, 2016 22:48*

The PSU is already tripping his mains breaker. He could put a 1000A fuse in and never draw more current than what the mains breaker will allow. Now if the mains was not tripping, I never would have suggested it, although any fuse value that is less than the mains breaker value will not cause a fire in the house wiring , unless there is a defect in the mains wiring. And if that was the case, plugging anything into that outlet such as a cell phone charger, all the way up to a toaster could (and probably already would have) cause an issue like fire. If the PSU functioned with a higher fuse value, without tripping the mains breaker, it would provide some clues as to where to begin the troubleshooting process. This type of testing by the very nature of the process , demands that it be supervised by a human being, not left unattended, so risk of the PSU catching fire and destroying the house is essentially zero, unless a person recklessly leaves it unattended.


---
**Alex Hodge** *May 19, 2016 13:22*

**+Anthony Bolgar** huh for some reason it shows that you posted that suggestion before he mentioned his mains breaker tripping in my Google+. You guys must have posted within seconds of each other or something. Anyhow, no worries. Personally I wouldn't troubleshoot a blowing fuse by trying a bigger one ever. To each his own.


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/ieQDVBmCVZN) &mdash; content and formatting may not be reliable*
