---
layout: post
title: "I ordered my K40 from a US seller on eBay for under $300"
date: September 02, 2016 15:00
category: "Discussion"
author: "Jeff Johnson"
---
I ordered my K40 from a US seller on eBay for under $300. I got it in less than a week and so far I am very pleased with it. I inspected everything closely and there are no missing screws or broken pieces. I have my own Corel Draw X7 and the Corel Laser plugin on the CD works, though the toolbar it ads to the top right often disappears. I can, however, still access the functions from the Windows Task Bar on the bottom right using the Corel Laser icon that appears while it is running.



Another interesting thing I discovered is that the mirrors are perfectly aligned. The seller told me I would have to align them but it appears this was done at the factory. Using the white tape and sending a test pulse on the lowest  power from all 4 corners creates a single, tiny hole.



It's obvious that I need to replace the bed with a steel mesh so that's next on my list. I'll update with my progress and any problems I have. I really appreciate all the knowledge shared here - it's saved me countless hours of frustration. Heck, I was cutting paper the first night and on the second I was engraving.





**"Jeff Johnson"**

---
---
**greg greene** *September 02, 2016 15:39*

Don't assume the mirrors are aligned quite yet !  I got the same results as you - but I was wrong.  Put tape over the mirror closest to the laser tube and make sure the beam is hitting the center of the mirror, if not - realign the tube, then clean that mirror - put tape of the the y axis mirror and test spot it when that mirror is closest to the first mirrorm then check it again at mid travel and furthest away.  If you still get only one spot - your good, if not you need to adjust that first mirror.  



Check out [theflyingwombat.com - The Aussie Flying Wombat](http://theflyingwombat.com) for clearer instructions.


---
**Jeff Johnson** *September 02, 2016 15:46*

Thanks for the advice. I'll do that when I get home this evening.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 02, 2016 16:22*

Another quick check is to also check your lens orientation. You want it convex side facing up.


---
**Vince Lee** *September 02, 2016 18:11*

Regarding the mirror alignment, I seen comments where folks have taken hours to do it.  It should only take a couple of minutes if you do it right.  Just don't try to center the beam and get one spot at the same time.  The purpose of angling any mirror is to align the beam with the head's axis of travel so the spot stays in the same place on the target.  To center the beam on a target, don't move the beam, move the target instead.  Only if a target if beyond its range of motion do you need to go back earlier in the process, either adjusting the tube mounts (for vertical adjustment) or moving a mirror in and out in its mount while keeping its angle unchanged (for horizontal adjustment).


---
**Robi Akerley-McKee** *September 06, 2016 06:27*

Honey comb bed can be aluminum.  It's what I'm running.


---
**J.R. Sharp** *September 08, 2016 13:22*

I too have problems with the toolbar disappearing after the first burn.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 15:37*

**+J.R. Sharp** The toolbar always disappears. You can find access via the icon in the system tray (near the clock in windows). You can right click & access all the necessary functions from there.


---
**greg greene** *September 08, 2016 19:12*

You can then uncheck and check again the tolbar tab and it will reestablish itself


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 20:43*

**+greg greene** Depending on version of windows I think. I could never seem to get it to return in Windows 10 using that method, but if I recall correct when I was running Windows 7 it did work.


---
**greg greene** *September 08, 2016 20:48*

I have had no problems in win 10 with it, and also works ok in Win 7.  Must be the magic of Microsoft at work again.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 20:57*

**+greg greene** Maybe my end then, could be something else in the setup causing it to not return for me (I am running win10 through bootcamp on a macbook).


---
**greg greene** *September 08, 2016 22:21*

Good Luck !


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/7AEzrcX9jJt) &mdash; content and formatting may not be reliable*
