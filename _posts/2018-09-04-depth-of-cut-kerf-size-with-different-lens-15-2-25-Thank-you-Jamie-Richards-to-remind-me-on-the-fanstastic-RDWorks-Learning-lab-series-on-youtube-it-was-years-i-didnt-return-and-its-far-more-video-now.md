---
layout: post
title: "depth of cut, kerf size, with different lens (1.5, 2, 2.5\") Thank you +Jamie Richards to remind me on the fanstastic RDWorks Learning lab series on youtube, it was years i didn't return and it's far more video now"
date: September 04, 2018 14:56
category: "External links&#x3a; Blog, forum, etc"
author: "Stephane Buisson"
---
depth of cut, kerf size, with different lens  (1.5, 2, 2.5")




{% include youtubePlayer.html id="WbBPps42iHc" %}
[https://www.youtube.com/watch?v=WbBPps42iHc&index=58&list=PLeKaKWOIPgi_XP6Nxzeapp-vUMCLQyVMB](https://www.youtube.com/watch?v=WbBPps42iHc&index=58&list=PLeKaKWOIPgi_XP6Nxzeapp-vUMCLQyVMB)



 Thank you +Jamie Richards to remind me on the fanstastic RDWorks Learning lab series on youtube, it was years i didn't return and it's far more video now.





**"Stephane Buisson"**

---
---
**Timothy Rothman** *September 04, 2018 17:53*

Russ is a treasure!  I've seen most of his videos (although at 1.5x speed :) ).


---
**Jamie Richards** *September 08, 2018 19:44*

The 1.5" lens is an interesting thing.  I still can't get over how well it cuts through thicker material than the 2".  People have argued it's not logical.  Logical or not, it's fact and I see no reason to go back to the 2".


---
**Stephane Buisson** *September 09, 2018 06:53*

**+Jamie Richards** the physics show the ray is more concentred. the spot surface area on material is smaller, for the same amount of power, so you increase burning efficiency and get a smaller kerf too. 


---
**Jamie Richards** *September 09, 2018 14:06*

I know, but people are arguing that the vertical focus area isn't far enough and it is meant to cut thinner material.  All I can say to them is try it for yourself and come to your own conclusion. I trust Russ!


---
**Jamie Richards** *September 15, 2018 09:51*

In addition, some don't believe the dot is smaller.  I don't understand how some people have a problem with facts.  They are saying the focal length is shorter, not a smaller beam etc. when there are charts showing it's smaller.  I just don't get it.  Someone a long time ago told me about the bell curve and how you'll have people from one end to the other regardless if it's right or wrong with views on pretty much everything and I find good examples of that all the time.  Like the ones who break in a house, Rob the people and shoot everyone, you'll have someone defending him saying, well, maybe he was trying to feed his family or ones who know him crying he's a good person and could never do it.  With lasers, you can have video proof, but because they can't do it, you can't either.  When someone can do something I can't, I find out why and make it work, I don't sit and say it can't be done.  Sorry, rant over. lol


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/T2sissx88sS) &mdash; content and formatting may not be reliable*
