---
layout: post
title: "One of my first attempts using Moly (3 coats) (Molykote) 15ma @ 20mm/s, turned out great, the white marks are just washing powder from cleaning it"
date: January 17, 2017 00:35
category: "Object produced with laser"
author: "Rodney Huckstadt"
---
One of my first attempts using Moly (3 coats) (Molykote) 15ma @ 20mm/s, turned out great, the white marks are just washing powder from cleaning it.

![images/de1842a9a08a6aa78a3e88c74b1fb20c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de1842a9a08a6aa78a3e88c74b1fb20c.jpeg)



**"Rodney Huckstadt"**

---
---
**Jonas Rullo** *January 17, 2017 01:11*

Molykote the gasket lubricant?


---
**Rodney Huckstadt** *January 17, 2017 01:12*

yep dry lube


---
**Jonas Rullo** *January 17, 2017 01:40*

Hmm, the one I found on Amazon was a silicone grease. Must be something different. Where do you get the Molykote?


---
**Jonas Rullo** *January 17, 2017 01:52*

I found molybdenum disulfide powder. Would you apply it as a water slurry and let dry?


---
**Rodney Huckstadt** *January 17, 2017 02:24*

the powder I have seen used with detergent as the liquid then let dry,  the molykote I picked up from a bearing supply place ([BSC.com.au](http://BSC.com.au) I think for around $40) you can get other stuff, but in Australia

 it is harder to get ya hands on


---
**Coherent** *January 17, 2017 20:21*

Nice job on the flask. Looks great. They sell dry lube spray moly for gun lube and automotive or general use in the US. Check the auto parts stores or any place that sells bearings or contractor supplies like Graingers if you're looking for it locally. Many places stock it. Last time I bought some it was about $10 for a can of the spray.


---
**Rodney Huckstadt** *January 17, 2017 22:59*

that's the US, very different story here down under :) some places it's as rare as rocking horse shit :)


---
**Thor Johnson** *January 18, 2017 07:14*

Which Molykote product did you use?  It looks like there are several to choose from...


---
**Rodney Huckstadt** *January 18, 2017 08:14*

the D-321


---
**Rodney Huckstadt** *January 18, 2017 08:15*

don't get it from blackwoods as they charge twice the price or more


---
**Thor Johnson** *January 18, 2017 12:56*

In the US, cheapest I found is from [skygeek.com - Dow Corning Molykote 557 Silicone Dry Film Lubricant - 11 oz Aerosol Can](http://bit.ly/2jwqDOZ) , but shipping is $15 for aerosols, so get multiple can...


---
**Rodney Huckstadt** *January 18, 2017 21:34*

that one looks like the silicon, don't know if it has any moly in it, try this one, [http://www.skygeek.com/molykote-d321r-321-dry-film-lubricant-spray-13-5-oz-aerosol-can.html](http://www.skygeek.com/molykote-d321r-321-dry-film-lubricant-spray-13-5-oz-aerosol-can.html)


---
**Justin Nguyen** *January 22, 2017 21:57*

I think we can use RZ-50 dry film lubricant as it cheaper. Question on the job: is it done with engraving option? And the file in use was vector?


---
**Rodney Huckstadt** *January 23, 2017 10:41*

all engraved and combo of vector text and jpg


---
**Rodney Huckstadt** *January 23, 2017 10:43*

RZ-50 is PTFE (Teflon) and have never heard of it working


---
*Imported from [Google+](https://plus.google.com/109243783228349869845/posts/jWbpttusUBT) &mdash; content and formatting may not be reliable*
