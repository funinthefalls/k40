---
layout: post
title: "Ok I am getting close. I have the X and Y working correctly and the laser firing"
date: November 14, 2016 02:53
category: "Smoothieboard Modification"
author: "George Fetters"
---
Ok I am getting close.  I have the X and Y working correctly and the laser firing.  I am getting faint lines between the letters when I etch.  Its like the laser is not completely off.  I am using an opticoupler to fire the laser  it is driven from pin 2.5 inverted signal.  The 5v and L are from the powersupply.  I suspect that for some reason I am not getting a clean 5v so the laser is on at a very low level.  Do I need a resistor across the 5v and L to put the signal high? 

![images/507329a98d722dd58b4004adb288ef24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/507329a98d722dd58b4004adb288ef24.jpeg)



**"George Fetters"**

---
---
**Don Kleinschnitz Jr.** *November 14, 2016 03:05*

Can you post a schematic or drawing of how you are connected through the opto-coupler.

You do not need an opto-coupler or any resistors to interface to L. L is the cathode of an optocouplers diode. You simply have to gnd it. 



Are you referring to the faint lines as what is erroneous?




---
**Alex Krause** *November 14, 2016 03:18*

Do you have any "Tickle" power enabled in your config file? Setting your min_laser_power other than 0 will cause this in some instances


---
**Bill Parker** *November 14, 2016 19:42*

Can you not just pull the line down with a pulldown resistor?


---
**Don Kleinschnitz Jr.** *November 14, 2016 20:07*

**+Bill Parker** don't think I understand your question. The "L" pin on the LPS is the cathode of a LED whose anode is tied through a 1K resistor to 5V. All of this is inside of the supply. 

To fire the laser you just need to pull "L" to ground. Nothing else needed. 

Simply connect the drain of a MosFet or the collector of a transistor both of which are driven by PWM. 

That is what the "Simple Smoothie PWM Control" shows:



[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)

[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**George Fetters** *November 14, 2016 21:20*

I am using the Azteeg x5 mini.  It's my understanding the azteeg only has 3 pwm one for the hot end on 2.5 one for the heater on 2.6 and one for a fan.  i am using pin 2.5 I measured the voltage for the hot end pins as 24v so i put an optocoupler in between the azteeg and the power supply.  on the power supply side i connect L to G to fire the laser.  It's not 5v,  I was looking at the smoothie k40 project and saw 5v on a diagram for a levelshifter.  My power supply does not resemble the one in this picture either.  I'll upload pics later


---
**George Fetters** *November 15, 2016 02:34*

![images/92c074b260a34738317961f424906519.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92c074b260a34738317961f424906519.jpeg)


---
**George Fetters** *November 15, 2016 02:38*

So this is probably the best photo that shows the optocoupler

there are 2 wires from the hotend connection on the right a purple and gray wire that go to the optocoupler on the left side of the pic.  There are 2 wires on the other side of optocoupler they are brown and black.  Those 2 are to the 3rd and 4th screw terminals on the middle powersupply connector.


---
**Don Kleinschnitz Jr.** *November 15, 2016 15:19*

**+George Fetters** I am wandering in the dark here as I don't know your exact config. so I am sorry if this is not helpful. Its hard for me to understand your configuration from the picture, a schematic of what you have done would help. 



I also tried to find a schematic for your controller and could not.



It does not look like you are connected to the 'L" pin and the circuits given above only work for that. 



When driving the L pin you do not need a level-shift-er.



To drive the L pin you only need to find an open drain on the controller and connect it as my PWM control shows. 



For ref here is a picture of what an open drain looks like. In my case I used P2.4.



![images/bd3f1abb26758169264605dacc122877.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bd3f1abb26758169264605dacc122877.jpeg)


---
**George Fetters** *November 15, 2016 20:27*

I am using this image as a reference and the 2 pins the level shifter in this diagram goes to which are the L and G on the middle connector.  [http://chibidibidiwah.wdfiles.com/local--files/blue-box-guide/k40SmoothieWiring4.png](http://chibidibidiwah.wdfiles.com/local--files/blue-box-guide/k40SmoothieWiring4.png)

[chibidibidiwah.wdfiles.com - chibidibidiwah.wdfiles.com/local--files/blue-box-guide/k40SmoothieWiring4.png](http://chibidibidiwah.wdfiles.com/local--files/blue-box-guide/k40SmoothieWiring4.png)


---
**George Fetters** *November 15, 2016 20:29*

I am using this wiring diagram of the azteeg x5 mini [files.panucatt.com - files.panucatt.com/datasheets/x5mini_wiring_v1_1.pdf](http://files.panucatt.com/datasheets/x5mini_wiring_v1_1.pdf).  In particular I am using the hot end connection + and - which equate to pin 2.5.


---
**George Fetters** *November 15, 2016 20:30*

There is a G and L on the connector on the right is that the same G and L as the middle connector?  Maybe I should be using that.


---
**George Fetters** *November 15, 2016 20:35*

I am not using a level shifter.  


---
**Alex Krause** *November 15, 2016 20:38*

**+Kim Stroman**​ has a working azteeg setup... but the PSU is different... still might have some insights on how it was connected 


---
**George Fetters** *November 15, 2016 20:43*

I think this might be the **+Don Kleinschnitz** I think this might be what you are asking about.  

![images/789d3908db1236f45304e4ae379f9999.png](https://gitlab.com/funinthefalls/k40/raw/master/images/789d3908db1236f45304e4ae379f9999.png)


---
**K** *November 15, 2016 20:51*

I had previously tried 2.5 like you're using and as you noticed the voltage was 24v, so I went with pin 2.4 on the EXP1 header. My board is version 3.0 though. Not sure if yours is the same. I also thought I read somewhere that the shifter is needed if you use the 5v since the board only sends 3.3v and so it doesn't shut the laser down properly. I'm using a shifter on ground, 3.3v and 2.4 on the X5. 


---
**George Fetters** *November 15, 2016 20:54*

So if I understand this diagram correctly I just need to connect pin #4 on J2(which is the - of the hot end connector) to L.  


---
**Don Kleinschnitz Jr.** *November 15, 2016 21:48*

**+George Fetters** yes pin 4 looks right just connect it to L and make sure configuration is turning that fet on for pwm. Likely that means not inverted.


---
**Don Kleinschnitz Jr.** *November 16, 2016 14:04*

Make sure that you are connected to "L" on the rightmost connector. That is the same connector that has 24VDC, 5VDC and gnd. You should not be connected to the middle connector other than what was there in the stock config.

...................

Looking at the picture at the top of the post we should challenge whether or not we are working on the right problem. 



Although getting the PWM on "L" without a level shift-er etc is a good thing to do, if the PWM was not working I wouldn't expect it to manifest like the picture above. If you are connected to L the only thing that input can do is to turn the laser on or off. It cannot partially turn the laser on. 



Are there any other connections between the controller and the LPS?



Lets reset as to what we think the problem is: 

1. Are we trying to get rid of the thin lines that are inside the characters?

2. Do we think these are caused by the laser being still on while its moving to another position to engrave? (I am assuming these lines are not in the image we are trying to engrave?). 

3. Do we think it is something else?


---
**Don Kleinschnitz Jr.** *November 16, 2016 14:05*

**+George Fetters** is yours working now?

**+Carsten N** your post says you are switching the LED's cathode. What LED are you referring to?


---
**George Fetters** *November 18, 2016 07:18*

Ok I connected L from the right most connector to pin 4 from J2.  This has the same effect as the optocoupler I was using.  The laser fires but does not shut off.  In troubleshooting I further realized the reason I get a fine line vs a deeper cut on the letters was the speed between cuts not the cut itself.  So basically the laser is never turning off or varying intensity.  I did determine that shorting G and L on the right most connector does fire the laser so I am on the right pin.


---
**Alex Krause** *November 18, 2016 07:20*

What is your min_laser_power set to in your config file?


---
**George Fetters** *November 18, 2016 07:43*

I was just reading blog **+Don Kleinschnitz** and I ran across something I may need to do.  In your notes it mentions to remove the pot and connect IN to 5V.  Furthermore I think an additional resistor was added to the pot by a former owner to limit how high the laser can be set in an effort to prolong the life of the laser tube.  I will try removing the pot tomorrow and let you know how that goes.


---
**George Fetters** *November 18, 2016 07:49*

min_laser_power is 0


---
**Don Kleinschnitz Jr.** *November 18, 2016 13:38*

What about the PWM signal being the wrong polarity, it should be a static 5V signal that goes to gnd to fire the laser. 



I would leave the pot and its resistor alone for now since the laser will fire. As long as there is nothing connected to IN from the smoothie we are ok.



What pin 4 and J2 are you referring to?



Ideally I need to know where on the smoothie PWM comes from and what circuit it goes through in Scotts board and then to "L". Until Scott shows up I will keep guessing ....



Scott's installation instructions must reveal something about how "L" is connected and what goes in the config file for PWM.


---
**George Fetters** *November 18, 2016 16:22*

**+Kim Stroman** I am not seeing 2.4 on the EXP1 header. 


---
**George Fetters** *November 18, 2016 16:25*

**+Kim Stroman** I think I found it.  It looks like my silk screen has 3v3 next to it.  Does 2.4 do pwm?  I can put the Logic level converter back in and try driving it with that.


---
**George Fetters** *November 18, 2016 16:31*

**+Kim Stroman** Not going to work for me.  I do not have 2.4 :(


---
**Don Kleinschnitz Jr.** *November 18, 2016 17:01*

You can follow the "Simple Smoothie PWM Control  method. It does not need anything but two wires. In this config it would not connect to Scott board at all. It connects to two smoothie edge connectors not an internal header.



[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html#more](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html#more)

[donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html#more)


---
**Don Kleinschnitz Jr.** *November 18, 2016 17:18*

**+George Fetters** if you have a smoothie you have p2.4 I am using it.


---
**George Fetters** *November 18, 2016 18:31*

I have an azteeg x5 mini


---
**Don Kleinschnitz Jr.** *November 18, 2016 18:34*

**+George Fetters** I must be loosing it I thought you bought the full kit with smoothie.


---
**George Fetters** *November 18, 2016 19:00*

No I have the azteeg x5 mini v1.1 it runs the smoothie firmware but there are differences.


---
**Don Kleinschnitz Jr.** *November 18, 2016 20:21*

What Px.x is U14 on the scheme you posted 3d ago. Connect L to that pin and assign it to pwm with no inversion?


---
**K** *November 18, 2016 21:40*

What about pin 1.3? It almost looks like your EXP 1 is the mirror of what the EXP 1 is on the v3. 


---
**George Fetters** *November 18, 2016 22:51*

Is 1.3 a pwm pin?


---
**Don Kleinschnitz Jr.** *November 18, 2016 23:01*

Can you point me to the schematic for your board.


---
**George Fetters** *November 19, 2016 04:25*

Here is a link to download the schematic. [http://files.panucatt.com/datasheets/x5mini_design_files.zip](http://files.panucatt.com/datasheets/x5mini_design_files.zip)


---
**Don Kleinschnitz Jr.** *November 19, 2016 13:59*

**+George Fetters** WOW! This morning I realized that I have been confusing two post threads with similar problems. This one and [https://plus.google.com/u/0/106012823225343259431/posts/N1gpsp74317](https://plus.google.com/u/0/106012823225343259431/posts/N1gpsp74317)



So if I have sounded a little nuts that's why! I will look at this thread closer and the schematic and get back on here with my brain engaged.



I will look at the schematics and reset my understanding of this problem.

[plus.google.com - I purchased the ACR Full kit from all-tek systems about a month…](https://plus.google.com/u/0/106012823225343259431/posts/N1gpsp74317)


---
**Don Kleinschnitz Jr.** *November 19, 2016 14:23*

**+George Fetters** Here is what I can tell from the schematics. Sorry if some of this is redundant but I got my beams crossed on this and another problem.



Use P2.5 which is called HOTEND_PWM.



Wiring:

On AZEETEG J2-Pin 4 (HOTEND) to LPS-L Pin 4 [rightmost connector on the LPS]

On AZEETEG J2-Pin 2(gnd) to LPS-gnd Pin 2 [rightmost connector on the LPS]



Configuration file changes/check:



.........Make sure the laser module is enabled and all laser modules definitions are "UNCOMMENTED"

# Laser module configuration

laser_module_enable                          true            

laser_module_pin                             2.5

laser_module_maximum_power                   1.0             

laser_module_minimum_power                   0.0             

laser_module_default_power                      0.8            

laser_module_pwm_period                      20              

                                                              

........ Make sure all modules that use 2.5 are disabled or changed to a different port. 

For example the Hotend should be disabled, it also uses 2.5 and will conflict if left on

# Hotend temperature control configuration

temperature_control.hotend.enable            false             




---
**George Fetters** *November 19, 2016 22:39*

This does not work.  I have removed all other references to 2.5 every laser module definition was uncommented and the wiring is as you described.  I am using G1 and G0 to fire and turn off the laser.  If I invert 2.5 the laser is always on so thats not it either.


---
**Don Kleinschnitz Jr.** *November 19, 2016 23:16*

**+George Fetters** the fact that the laser fires all the time when you invert the pin and not at all when its not suggests that we are connected to the right pin on the LPS, the LPS is working . It also suggests that the output FET is working.

I think the setup I gave you is right. It seems that for some reason the AZEETEG is not asserting a PWM signal.



I assume you do not have an oscilloscope?



As a stab in the dark you can move the "L" connection to J2-pin 6 and configure P2.7. I doubt that will change anything though.



I am new to Gode does G1 & G0 fire the laser or just move the gantry?



This is baffling! 



When I review above, this worked (except stray lines) when you employed a opto-coupler connected to 2.5 ... right!


---
**K** *November 20, 2016 03:17*

Are you using a shifter that's grounded on both ends? I seem to remember having the same issue of it being constantly on. 


---
**George Fetters** *November 20, 2016 15:08*

**+Kim Stroman** I am not using a level shifter at the moment.  I am using **+Don Kleinschnitz** method of driving the laser by connecting L from the laser to an open drain on the mosfet connected to the Hot End - (which should be an open drain).  I am beginning to believe **+Don Kleinschnitz** is correct and I am not getting pwm from the azteeg or I am driving the laser with the wrong Gcode command.  Originally it was M3 and M5 but someone mentioned it should be G1 and G0 so I switched to that.  I will try going back to M3 and M5.  I have not used an oscilloscope in years, I will try that too and see if I can see the pwm signal on 2.5.  I'll try 2.7 today too.  **+Kim Stroman** I tried to go back to a level shifter yesterday but it appears may be a fake one or I am not wiring it right.


---
**George Fetters** *November 20, 2016 15:11*

I think i have 2.5 and the right pin because when I invert it, it is on solid.  If I had the wrong pin I would not get anything.


---
**Don Kleinschnitz Jr.** *November 20, 2016 19:41*

**+George Fetters** I agree and think we have the right set up. The Azteeg actually has a level shift-er in front of the MOSFET between the processor and the driver  :).

I would scope the inputs and outputs of that level shifter I think it is U13.


---
**K** *November 20, 2016 20:56*

**+Don Kleinschnitz** This is interesting to hear, that the Azteeg has one built in. 


---
**George Fetters** *November 26, 2016 14:50*

Ok I think I must be missing something from my config file.  I hooked an oscilloscope up to 2.5 and I am not seeing a square wave or any wave for that matter.  I am thinking I not using the correct gcode commands to fire the laser.  Are there any config entries I can add to force the laser to fire and shut off on a specific command?


---
**George Fetters** *November 26, 2016 14:51*

I am using G1 and G0 to fire the laser currently.


---
**George Fetters** *November 29, 2016 16:14*

The laser is working!  Thanks **+Don Kleinschnitz** and **+Kim Stroman** for all your help.  I have an issue with the laser intensity not being controlled still but the always on problem is fixed.  I am using 2.7 and a couple different config entries.



switch.laserfire.enable                      true             #

switch.laserfire.output_pin                  2.7^           # connect to laser PSU fire (!^ if to active low, !v if to active high)

switch.laserfire.output_type                 digital          #

switch.laserfire.input_on_command            M3               # fire laser

switch.laserfire.input_off_command           M5               # laser off

![images/2a68fb4dd93197cfa6e7f9eab841fee5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a68fb4dd93197cfa6e7f9eab841fee5.jpeg)


---
**George Fetters** *November 29, 2016 16:16*

**+Kim Stroman** I think you are using a level shifter to IN to control intensity is that correct?


---
**Don Kleinschnitz Jr.** *November 29, 2016 17:59*

**+George Fetters** the pwm signal controls the lasers intensity??? Use whatever software you are driving the controller with to set the power level.


---
**George Fetters** *November 29, 2016 18:40*

So I believe I found it

switch.laserfire.output_type                 digital          #

this should be set to pwm.  Its been ignoring the pwm values I have been setting in software because the config file says to do it digital as 0 or 1.  :S


---
**Don Kleinschnitz Jr.** *November 30, 2016 14:40*

**+George Fetters** when you get a moment could you summarize your PWM configuration and wireing so that I can capture what we did for others?


---
**George Fetters** *December 01, 2016 20:59*

I will do this this weekend.  I'll send you details and a wiring diagram for it.


---
**Don Kleinschnitz Jr.** *December 02, 2016 02:12*

**+George Fetters** awesome!


---
*Imported from [Google+](https://plus.google.com/114125781899580900020/posts/abPmZDokSjA) &mdash; content and formatting may not be reliable*
