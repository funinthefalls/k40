---
layout: post
title: "Does anyone have the current required for the steppers in the K40?"
date: July 22, 2016 14:11
category: "Smoothieboard Modification"
author: "Jon Bruno"
---
Does anyone have the current required for the steppers in the K40?

I was jogging my smoothie conversion around for a little while and noticed a little singing in the LPSU in some positions..

The config sample I got from Ariel is set to 1.5A.

Needless to say, my LPSU power light is now winking at me and needs to be torn apart so I can put some of the smoke back in.



<i>_EDIT_</i>

I just found the links for the PPS stepper page in the bluebox guide..

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)



([http://www.smoothmotor.com/Standard-Models/Nema-Stepper-Motor.html](http://www.smoothmotor.com/Standard-Models/Nema-Stepper-Motor.html))

hmm.. 0.5A.. **+Ariel Yahni** you may want to adjust your settings a tad..





**"Jon Bruno"**

---
---
**Ariel Yahni (UniKpty)** *July 22, 2016 14:28*

**+Jon Bruno**​ it will depend if you have integrated steppers or not. Mine are not integrated and only smoothieboard has the integrated


---
**Jon Bruno** *July 22, 2016 14:29*

...and MKS Sbase 1.2/1.3


---
**Scott Marshall** *July 22, 2016 14:31*

Standard K40 motors are 1.0 Amp 



Set Alpha current and Beta current to 1.0


---
**Jon Bruno** *July 22, 2016 14:36*

[http://www.smoothmotor.com/Standard-Models/Nema-Stepper-Motor.html](http://www.smoothmotor.com/Standard-Models/Nema-Stepper-Motor.html)

references the manufacturer site where the specs can be looked up, I can't get it to work but Stephane's write up shows them as 0.6 for Alpha and 0.5 for Beta.

That would depend on your particular model motor installed of course.

Fortunately I have one last LPSU spare to try and get it right...


---
**Ariel Yahni (UniKpty)** *July 22, 2016 14:36*

For the rest that could read this post you need to set up the pot on your stepper driver to 1 amp as suggested above


---
**Jon Bruno** *July 22, 2016 14:37*

**+Ariel Yahni** .... If you run external drivers.


---
**Ariel Yahni (UniKpty)** *July 22, 2016 14:39*

**+Jon Bruno**​ drop in also


---
**Jon Bruno** *July 22, 2016 14:42*

Right!


---
**Jon Bruno** *July 22, 2016 23:18*

I just read that typical 24v rail max is 1.0A

0.6 alpha and 0.5 beta sounds more legit for sure.


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/5EaVvBcFuf6) &mdash; content and formatting may not be reliable*
