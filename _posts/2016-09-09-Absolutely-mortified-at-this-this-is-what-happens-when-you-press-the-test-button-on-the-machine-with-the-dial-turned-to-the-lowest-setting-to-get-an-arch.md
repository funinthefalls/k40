---
layout: post
title: "Absolutely mortified at this, this is what happens when you press the test button on the machine with the dial turned to the lowest setting to get an arch"
date: September 09, 2016 18:50
category: "Discussion"
author: "J DS"
---
Absolutely mortified at this, this is what happens when you press the test button on the machine with the dial turned to the lowest setting to get an arch.





**"J DS"**

---
---
**Ariel Yahni (UniKpty)** *September 09, 2016 18:58*

**+J DS** its called Arcing (form an electric arc.). Unplug the machine, disconnect the tube, reconnect the tube, apply silicon ( the one included ), put the hose ( plastic isolator ), fire again


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 20:16*

Wow, that is highly unsafe. Don't stand anywhere near that arcing. As Ariel said, the high-voltage wire mustn't be insulated correctly where it enters the laser tube.


---
**J DS** *September 09, 2016 21:47*

I've covered it in silicone and the rubber sleeve along with insulation tape and it's still arcing




---
**Ariel Yahni (UniKpty)** *September 09, 2016 21:51*

By you or from factory? If you haven't redone it I suggest you do


---
**Scott Marshall** *September 09, 2016 22:28*

Your laser tube is bad.

Electricity takes the path of least resistance, and if a 2.5" jump thru the air is easier than thru your tube, it has no gas in it.



This isn't an uncommon problem with these. Send this video to your seller, and you should be able to work a very high settlement. I got $250 for some broken parts and a bent carriage. If you can do the same, you can install a new tube and actually end up ahead on the money.



Don't do it anymore, besides being somewhat unsafe, you'll destroy the power supply, which seems Ok for now. It's the 2nd most expensive part and you'll want it if you fix the Laser (which I'd suggest doing, as you'll probably not get a replacement out of the seller).



This is a good video to illustrate to people just how dangerous these power supplies are. That arc can easily kill you if it were to jump to you instead of the chassis. It's NOT like static electricity or a sparkplug wire. 






---
**J DS** *September 10, 2016 15:30*

Seller is forwarding me a full new unit


---
**Ariel Yahni (UniKpty)** *September 10, 2016 15:31*

A great win for mankind


---
**J DS** *September 10, 2016 15:54*

Nor sure what to do with this large taser now


---
**Ariel Yahni (UniKpty)** *September 10, 2016 15:55*

You can get a new tube from china for about 200 shipped DHL.



Or you could donate some of the parts for research. We need specially the PSU


---
**Scott Marshall** *September 10, 2016 16:56*

Glad to hear you've got an honest seller who is willing to do the right thing. I'd like to know who you bought it from, as I'm considering a purchase.



I'm looking for a test unit, which doesn't need a functioning tube, I build upgrade kits and the testing of them is monopolizing my personal laser, so much so I'm on the verge of ordering another one just so I can burn out panel labels and such. Which is why I got into this laser thing in the 1st place.



I'd make you an offer on the dead one but the only way shipping on it might be feasible would be if you are located in the Northeastern US. Based on your power cord I'm guessing you may be a few thousand miles east of there. (UK?) 



Hope you have better luck with unit #2!



Scott






---
**Ariel Yahni (UniKpty)** *September 10, 2016 16:58*

**+J DS**​ please donate it to **+Scott Marshall**​ very good information for the community and you future smoothie upgrade ( you will)  if he can do a biopsy of the PSU


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 10, 2016 17:20*

**+J DS** That's great news that your seller is sending a complete replacement.


---
**Scott Marshall** *September 10, 2016 18:17*

**+Ariel Yahni** 

Thanks for thinking of me.



 I've now got new Genuine K40 PSUs in stock and am developing a kit with both voltage (0-20kv) and current (0-20ma) meters and a new front panel insert. Choice of Modern LCD or Very cool looking corner hung analog meters.



 I've got a deal going for a dozen more with a Chinese supplier if they ever learn to ship them so they don't arrive bent -- It astounds me how anyone could ship a PSU in a plastic bag and not expect it to be damaged. They've done it twice now, despite my complaints on the 1st 2. I'm concerned if I buy 12, they'll all show up damaged...



Sorry to go off on the tangent there, the bottom line is I'll be stocking PSUs in the US for a very fair price with 3 day free shipping to the Contentinal US. I'm adding a built-in High voltage quick connect to them that will eliminate any tube scraping, wire re-wrapping and re-insulation. Just cut your old PSU wire, attach the included Highvoltage connector with a set screw, and plug in the power supply. No soldering, RTV or actual wiring required. These are "White Connector" Power supplies, but include a plug in adapter to accept the "Green" connector, so these will replace any K40 PSU.



My PSU kit will make a PSU change a 5 minute affair, and will be priced competively with a slow boat from China one. (I also have a set screw connector for the tube connection (if you're changing a tube or have to remove it) coming very soon - no wire wrapping, just slide on the brass sleeve and tighten a setscrew. Too cheap not to use on a tube replacement.



I've not yet had time to go thru the circuit of the PSUs and trace them out. I plan on posting the generated schematic for all the experimenters once I get it done.  I'll post detailed photos of the PSU internals next time I'm taking pictures - which should be soon, I've got a few more new products I need to get up on my site, and Yuusuf can't do much until I do my job (Hate the paperwork - nothing new there)of taking photos and writing the descriptions.



I have in stock (but not yet on the website) Double shielded transparent (like 'Hospital Grade' so you can see the shielding) with both braid and foil internal shields, and double ferrites (both ends) they're 10 feet long and about as noise resistant as you can make a USB cable. Not a "glamor mod", but they are the best USB cable I could create. $15 plus shipping (freeshipping if ordered with something else)





Been moving a little slow lately, but still going, thanks to all who are patient with my pace. I've got a ton of cool stuff in the works for K40 modders, but it's taking longer than I expected to get them market ready.



The Switchable is on the bench, and the prototypes will be available soon. The 1st batch of ACRs is largely gone, and I need to get the 1st production run of those going pretty soon.



I've got something here for you too, Ariel - I  just need to test it, and it ought to go out on Monday.



Sorry, I got talking here (you may have noticed that happens to me sometimes...) and this turned into an ALL-Tek Update. I need to do that this weekend too.



Look for my update soon, and visit the site (ALL-TEKSYSTEMS.com)

for new products and tech updates to be posted soon.



Scott



PS - Sorry JDS for running on on your thread. Glad you have a new laser coming, I look forward to hearing it works perfectly right out of the box (well, there's got to be a 1st time - right?)


---
**Ariel Yahni (UniKpty)** *September 10, 2016 18:41*

**+Scott Marshall**​ amazing. Next step please carry some tubes. :) 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 10, 2016 19:29*

**+Ariel Yahni** That would be a great idea/addition... Carry everything to fix the laser :)


---
**Scott Marshall** *September 10, 2016 20:11*

Going as fast as I can. This stuff is expensive...



Tubes are tough. If I warranty them the way I need to to abide my personal business ethic (no questions asked replacement of defective stuff and 100% customer support for free), then I have to price them so high as to be able to absorb the cost of the occasional person who runs one dry and submits a claim. That makes my price for your basic tube non-competitive with the big guys (name withheld), who won't replace a tube at all. (I have several people who have had personal experience with this, and the big guys claims a K40 stock pump is inadequate and will cause a tube to fail in a couple weeks. If you scream enough, they give you $100 store credit which means you get another overpriced, questionable tube that's not warrantied unless you also buy a pump from them - It's not how I'll ever do business) 



While I'm not really expecting to get rich doing this, I'd like to at least break even someday, and tubes don't look like a good product.

If I can't offer something better, I don't see a reason to get in the fight with companies that can buy tubes by the crate or truckload.



These all glass tubes have a gas leak thing going that's impossible to screen.

'

The metal in the tube expands at a different rate than glass, and metal alloys which duplicate the glass coefficient of expansion are closely guarded trade secrets. (Have been since the invention of the light bulb)



The rub is even if there's a leak form the factory, it's so slow that the tube doesn't show ill effects for a long time, often a couple of years.



That's where the myth that tubes have a 'shelf life' comes from. People put up a brand new tube, and pull it out 2 years later only to find it's weak or no good. The gas leaks out, one molecule at a time, if the tube is in service or not. If it is in service, the temp cycling can speed up (or cause) the leak, but either way, if it's a bad seal at the start, the tube is doomed. 



The high end tubes use mechanical gaskets and rings to seal the wiring and optics, and these are less likely to leak, tolerate the temp cycling better, and some are even servicable (for a price).



There's very few of these tubes available in K40 sizes, so we are stuck with the all glass crapshoot. 



If I were to buy a tube right now, I'd be taking the same gamble as anyone else would, and if I were to buy 50, I'd STILL be taking the same chance. Seeing as there's no way I know of to verify a tubes condition, I can't offer a customer anything better than your basic ebay Chinese Importer/Exporter can.



My advice on tubes is to buy low, and hope. Maybe you can shift the odds a bit by going with a name brand manufacturer, but how can you even be sure that's who made the tube?



If I could offer good quality, mechanical seal tubes for a tolerable price ($500us range), I may consider it, but right now, I can't even find one to buy for my personal laser.



If you step up to 80-100w class tubes, you at least get into "semi-mechanical" seal tubes (basically a mix of types - these are the ones with a higher "estimated life"), but you're looking at a lot of money, mods to the k40, new PSU and still have the risk of getting a bad one (although you'd like to think it's lower)

You're spending the kind of money that makes a whole new laser more practical - which is why you don't see many 60w+ K40s.



I wish that wasn't the case, maybe someone who knows more about these tubes can come up with a solution, but I havn't seen it as yet.



I did it again and ran on. 



It's a subject I'd like to discuss more, maybe there's an answer to the conundrum, but right now, It looks to me like it's best to buy a moderate to low priced tube and give it it good polish with your best rabbits foot before installing (Using an ALL-TEK PSU and connection kit of course!)



I'm afraid the tube is the ONE part of the K40 I can't see a way to improve upon (within MY capabilities anyhow).



Scott


---
**Ariel Yahni (UniKpty)** *September 10, 2016 20:51*

**+Scott Marshall**​ anyway I can help let me know


---
**Ben Walker** *September 12, 2016 12:35*

I don't trust these sellers that offer a replacement.  Been waiting for a part since May and in July they offered to replace the whole unit.  Now they just ignore my emails altogether.  Don't do anything till you count your chickens.

My 2 pence


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 16:47*

**+Ben Walker**'s advice might be good to follow. Make sure to put a PayPal claim (or whatever service you used to pay for the machine) if you haven't received replacement machine or parts by the time the claim period ends (I think 20 or 30 days?).


---
**Scott Marshall** *September 12, 2016 16:58*

Ben has a point. Make sure they don't try to "run out the clock" on you.

It's a common tactic of unscrupulous sellers to keep you busy sending photos etc, then later simply ignoring you until the ebay buyer protection period expires. After that ebay won't even discuss the subject.



If you have any suspicion that's what's being done, ask for ebays help. They will take over, give the seller a deadline,(usually<10days) which, if not met, results in an immediate decision in your favor. They then credit your account using the sellers money, and that's the last you'll hear of it.  Ebay actually does a good job if you follow their system exactly. I got burned once (fortunately it was only $40 and learned the game as a result). I've since had 2 claims summarily awarded to me because the seller didn't respond. 



Good Luck



Scott


---
**Scott Marshall** *September 12, 2016 17:34*

**+Yuusuf Sallahuddin** We cross posted on that. They great minds think the same.....


---
*Imported from [Google+](https://plus.google.com/102322879087562424255/posts/GJGVBAHLdGd) &mdash; content and formatting may not be reliable*
