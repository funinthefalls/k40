---
layout: post
title: "Well I have just acquired a Universal Laser systems M-300 35 watt RF Laser ( got it for $400) Caveat: RF Laser is from 2001 and it is dead"
date: October 11, 2016 19:53
category: "Modification"
author: "David Cook"
---
Well I have just acquired a Universal Laser systems M-300  35 watt RF Laser

( got it for $400)  Caveat:  RF Laser is from 2001 and it is dead.  Motion control, autofocus , Z-table etc still works great !!



Replacement 35 watt RF Laser module is ~$1,800 USD.



For now,  I am going to remove it's computer & Stepper driver & 48V power supply and the RF laser head.   And transplant the K40 Laser tube, Power supply and Smoothie based control board into it and control it all with Laser Web.



Probably sounds ludicrous, but I don;t want to spend the $1,800 at the moment to fix it.



I will be 3D printing adjustable glass tube mount and mirror mount  to make it work.  

I'll create an adapter board  ( Vector board)  that takes the standard motor and endstop wiring harnesses and adapts them to plug into the MKS SBASE  that I have.



Essentially turning my K40 into a 300mm x 600mm machine with autofocus and Z-height variable.



I did have to model and print a new belt idler pulley to fix the Y-Axis 





[https://goo.gl/photos/fUr4rqkUXvPwSdkFA](https://goo.gl/photos/fUr4rqkUXvPwSdkFA)





**"David Cook"**

---
---
**Ariel Yahni (UniKpty)** *October 11, 2016 20:01*

I'm would do that In a heartbeat 


---
**Scott Marshall** *October 13, 2016 11:34*

You may want to consider fixing it and selling it. You would make enough to scratch  build a real  nice CO2 laser...


---
**Stuart Middleton** *October 20, 2016 07:31*

Why can't I find a bargain like this! :(




---
**David Cook** *October 20, 2016 13:21*

**+Stuart Middleton** that's what I usually say. I was just in the right place at the right time. 


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/1kWGH34Tudo) &mdash; content and formatting may not be reliable*
