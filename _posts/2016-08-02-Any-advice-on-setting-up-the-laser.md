---
layout: post
title: "Any advice on setting up the laser ?"
date: August 02, 2016 06:07
category: "Discussion"
author: "John Austin"
---
Any advice on setting up the laser ?

what to do and what not to do type stuff.

I unboxed my laser last week with the hope of starting to set it up but had other things i had to get done.



Taking Friday off to start trying to get it set up.



I have order the honeycomb bed the air assist head with a 18mm lens. Air pump and a stronger fan for the smoke removal and laser glasses





**"John Austin"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 07:08*

Check that the rails are square.

Check water pump works (no bubbles in tube too).

Check alignment.

Check lens orientation (bump side up, flat down).

Set focal point (50.8mm to centre of material or maybe different if your 18mm lens is different).



Can't think of anything else off the top of my head. I'm sure others will add their 2c too.


---
**Scott Marshall** *August 02, 2016 07:11*

Plug the water pump and the laser into a switched outlet strip.

This will help prevent accidently firing the laser with no water flow.

It will destroy the tube in seconds if run without flow, and once you realize what you just did, most people rush and plug in the pump, so the cold water hits the ripping hot glass with predictable results.



There's a few things you should do right off, before starting using it.

!st get it going to make sure it all works (so you can go back on the seller if needed with a untouched machine. If it works ok, shut it off and start taking it apart.

Remove the silver "worktable" or clamp or whatever they call it. It's junk and actually prevents you from focusing the laser properly.



While you're in there, mark the position of the brown rails on the bottom with a sharpie and carefully remove the entire frame. it's only 2 screrws and a couple of plugs. Then remove the "duct" from the rear panel by removing the 4 screws at the corners. Put the duct with the worktable in the recycle bin. replace the fan mounting strips with the 4 screws you took out. Re-install the motion control frame, getting it on the sharpie marks as closely as possible. This 'duct' performs no useful and restricts the already inadequate fan so that it's unusable. If you remove the duct, the stock fan works OK if you don't restrict the flow with ductwork that is too small or long. Add the factory blue plastic duct to the pile in the recycle, it's too small and a fire hazard. Use only non-flammable duct on the exhaust. 4" expandable aluminum duct is good for short runs, 6" for longer ones. Vent outdoors, laser fumes can be very nasty.



Then (refer to guide linked below) align the mirrors. (they were out of alignment when you started, so it's no loss there. The 1st couple of time you align the machine, it will be confusing and slow. After a couple of times you'll be able to do it in under 10 minutes. Some 1" lo-tack masking tape (the 3m Blue stuff) and aluminum foil are all you need. I put the foil under the tape so the vapors from the tape don't pollute the mirrors.



That will get you on the road. Pretty soon you'll want an air assist head and a better set of mirrors Saite cutter on ebay is a good place to deal with.



To replace the factory workholder, there's a lot of choices. If you want to spend, you can buy a fancy honeycomb plate, but plain expanded metal or Walmart disposable grill toppers work well for a buck and a half. Another choice is a nail board, just a piece of 1/4 plywood with 17ga brads in a 1" grid. Adjust the height on these surfaces with a wooden shim assortment. The focus is critical to the performance of the laser, as the mirror alignment. This is accomplished by raising anfd lowering the workpiece, but the Site cutter air assist head has the advantage of having adjustable focus, so once you get the workpiece height close, you can dial it in from there. Other benefits of the air assist head are that it keeps fires damped out, keeps smoke from depositing on the workpiece, and protects the lens. You will need an air pump or compressor with regulator, but it's worth the trouble.



Read thru the group's history, This is probably the 30th time I've typed this, this time I'm saving it for the next person. Wish we had Stickies here. There's a ton of info here, and the people are super helpful.



Be careful around your new laser, there are no safety interlocks, so there is exposed line voltage, laser light that can burn or blind you, and 15,000 volts all unguarded.



Do check the main ground wire it attaches to the top of the IEC line cord jack with a quick slide terminal, and they frequently fall off in shipping, ungrounding the whole laser. Solder it in place if you can, or just make it as tight as possible.



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



[http://www.ophiropt.com/](http://www.ophiropt.com/)



[http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005)



[http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005](http://www.ebay.com/itm/CO2-40W-Stamp-Engraver-K40-Laser-Head-20mm-Mo-Mirror-Znse-Focus-Lens-18mm-/252320098005)



The new head requires shimming and the hole opened a bit. You can buy a plate with a larger hole, ask here for details on the install when you get there, and it needs larger lens. The best way to do it is buy the bare head (no mirror or lens) and buy the mirror/lens kit listed here.

(covered some stuff you already know about, but can pass it on now)



Hope it helps, be safe and have fun!



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 07:29*

**+Scott Marshall** Stickies are available in G+ communities, just only for posts/topics. Also, has to be "pinned" by a moderator. I had some "pinned" posts on my own page/wall for a while (although I unpinned them now).


---
**John Austin** *August 02, 2016 15:52*

﻿

**+Scott Marshall**  Thanks for typing all that out.

I plan on stating on it this weekend unless life gets in the way again like last weekend.


---
**Jim Hatch** *August 03, 2016 02:57*

I'd move Scott's recommendation on checking the wiring to the top of the list. These sometimes come with wiring issues on the main plug - from bad grounds to crossed uninsulated wires. Make sure it's not going to let all the magic smoke out when your turn it on the first time.


---
*Imported from [Google+](https://plus.google.com/101243867028692732848/posts/1o9o4uqHEPV) &mdash; content and formatting may not be reliable*
