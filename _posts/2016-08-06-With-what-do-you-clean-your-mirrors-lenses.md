---
layout: post
title: "With what do you clean your mirrors / lenses"
date: August 06, 2016 00:12
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
With what do you clean your mirrors / lenses





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *August 06, 2016 00:14*

Poll on FB group [https://m.facebook.com/questions.php?question_id=715192905279062](https://m.facebook.com/questions.php?question_id=715192905279062)


---
**Jon Bruno** *August 06, 2016 00:27*

sandpaper and steel wool


---
**Ariel Yahni (UniKpty)** *August 06, 2016 00:29*

**+Jon Bruno**​ and then a hammer to put it back? 


---
**Jon Bruno** *August 06, 2016 00:39*

I use an old pair of rusty pliers that I set aside just for that purpose.


---
**I Laser** *August 06, 2016 00:46*

So that's what I've been doing wrong!


---
**Greg Curtis (pSyONiDe)** *August 06, 2016 02:37*

I use rubbing alcohol and a microfiber cloth, but when you wash the microfiber cloths, wash and dry them alone to avoid the extra lint, and do not use fabric softener or dryer sheets. They will impart oils and potentially leave a residue on the mirrors/lens.



Often I will just wipe with a dry clean microfiber if it's just a quick dusting.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 04:30*

I use plain water lately. Seems to keep it cleaner than it ever did when I used mineral turpentine or methylated spirits.


---
**Julia Longtin** *August 06, 2016 16:14*

scrubbing bubbles bathroom cleaner


---
**Gunnar Stefansson** *August 08, 2016 10:29*

I use ear cleaning tips dipped in 97% Alcohol and then use a Glasses cleaner cloth to really get all the dust particles away. Works out really great.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/ANw3L5oBNeF) &mdash; content and formatting may not be reliable*
