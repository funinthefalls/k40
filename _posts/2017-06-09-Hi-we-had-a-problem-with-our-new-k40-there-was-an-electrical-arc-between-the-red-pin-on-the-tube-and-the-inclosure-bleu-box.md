---
layout: post
title: "Hi! we had a problem with our new k40 , there was an electrical arc between the red pin on the tube and the inclosure bleu box"
date: June 09, 2017 09:35
category: "Original software and hardware issues"
author: "J. DTRTR"
---
Hi!

we had a problem with our new k40 , there was an electrical arc between the red pin on the tube and the inclosure bleu box. we then make a better isolation between tune and case (also ratoting a bit the tube to create a bigger gap between the two)



that actually fix the arcing issue (better insulation and small rotation of the tube) BUT now nothing happen .. just faint screechy sound from the pwer supply ans no laser output ... the mA guage on the case dont move either. we also discover that trying the laser create a HUGE amount of static energie .. it's a bit scary tbh .. and we have no clue on what to do now.. 



it's really frustrating if you have an idea ... 



fyi (when the laser arrive it barrely work for 2 mins.. and i had a new PSU ordered .. ) 




**Video content missing for image https://lh3.googleusercontent.com/-FWvkQDJ-5dY/WTpr6hAU4OI/AAAAAAAANyA/pw733h783_IIvUvWQHF-csmkvDZmXjKsQCJoC/s0/VID_20170608_154621.mp4**
![images/6b81188478b7c8cfa6e0f9a62ddc1fed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b81188478b7c8cfa6e0f9a62ddc1fed.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-ToUzmuxU0uY/WTpr6hd_U5I/AAAAAAAANyA/gGudWQ7rGMIqYNBoliUy80sTu2qoUrtgQCJoC/s0/VID_20170608_154235.mp4**
![images/49c9c6e20c630fe853f27ea550078572.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/49c9c6e20c630fe853f27ea550078572.jpeg)

**"J. DTRTR"**

---
---
**Chris Hurley** *June 09, 2017 11:51*

When mine made this sound the tube was broken. It was arcing to the case. 


---
**J. DTRTR** *June 09, 2017 11:53*

hum sooo i've a broken tube .. since the beginning .. nice :( 




---
**Chris Hurley** *June 09, 2017 12:13*

Yup , mine broke in shipping to. On the bright side mine let all the water go through the machine so at least that didn't happen to you.


---
**J. DTRTR** *June 09, 2017 12:15*

ouch .. yeah mine works for 2 mins ... so I 'll have to buy a new tube then ... 




---
**Chris Hurley** *June 09, 2017 12:46*

I would contact the seller a lot of times though offered to sing you a new tube. I'm assuming you haven't had it for a long time? 


---
**J. DTRTR** *June 09, 2017 12:47*

the problem is that a do have it since long time (never use it ) but i never really had time before to work on it  ... 




---
**Chris Hurley** *June 09, 2017 12:49*

Well on the plus side replacing the tube is super easy. 


---
**J. DTRTR** *June 09, 2017 12:52*

hey hey yes but it's still half the price of the machine ...  at least I have direction where to look at ... 






---
**Chris Hurley** *June 09, 2017 13:09*

Yeah $200 all in is the norm. 


---
*Imported from [Google+](https://plus.google.com/101405162141132297916/posts/FxkDsXEJWPW) &mdash; content and formatting may not be reliable*
