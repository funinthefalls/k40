---
layout: post
title: "Finally a nice Open-Source laser. Also it runs Smoothieboard"
date: June 02, 2016 08:15
category: "Discussion"
author: "Arthur Wolf"
---
Finally a nice Open-Source laser. Also it runs Smoothieboard. And they support Laserweb's work.

If you need a laser, go get one there. If you don't re-share please !



<b>Originally shared by Bonne Wilce</b>



FabCreator Lasers now live on kickstarter!



[https://www.kickstarter.com/projects/fabcreator/fabcreator-lasers](https://www.kickstarter.com/projects/fabcreator/fabcreator-lasers)





**"Arthur Wolf"**

---
---
**Stephane Buisson** *June 02, 2016 11:51*

Kick starter campain is now live !


---
**Saskia Smits** *June 02, 2016 13:57*

The FabKit is the perfect way to upgrade your bed size. Just transplant your K40 stock tube and its power supply into our assembly and you are on your way! ;)


---
**John-Paul Hopman** *June 02, 2016 14:20*

From one of the videos, it appears that the laser moves with the gantry rather than being stationary in the back of the box as with typical K40 laser cutters. What are the benefits / drawbacks of this system?


---
**Saskia Smits** *June 02, 2016 14:30*

Carrying the laser tube on the gantry has many benefits, first of which is a shortened optical path. Reducing the distance the beam needs to travel increases the effective laser power. It also makes alignment a lot easier as you only have one flying mirror. 

In the FAB40 and FAB100 the optical path is fully enclosed. This ensures no smoke particles land on the mirrors and lens nor get in the way of the travelling laser beam. 

We have left this open in the FABKit for easier user access and as it is a lot easier to build

Carrying the laser tube also makes for a very space efficient build, you get a larger cutting area to machine size




---
**John-Paul Hopman** *June 02, 2016 14:32*

**+Saskia Smits** Thank you.


---
**Stephane Buisson** *July 03, 2016 12:49*

 Sorry to see the campain failed, independantly of your work quality, maybe a price entry point to hight for market. #yearoflaser

but thank you for sharing your view on moving lasertube. maybe an **+Mark Carew** openbuild project


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/1v8XL5tz4BK) &mdash; content and formatting may not be reliable*
