---
layout: post
title: "Does anyone have a source for lamp bases like these?"
date: February 24, 2018 23:38
category: "Material suppliers"
author: "David Davidson"
---
Does anyone have a source for lamp bases like these?



[https://www.lampsillu.shop/collections/trucks-heavy-equipment](https://www.lampsillu.shop/collections/trucks-heavy-equipment)









**"David Davidson"**

---
---
**Jim Hatch** *February 25, 2018 02:05*

eBay (search for "led menu") or Amazon. The ones on eBay are cheaper but take longer. The LED bases on Amazon are about 2X but are quick delivery - they also have a 12-pack.


---
**jindrich stehlik** *February 25, 2018 18:44*

I am just thinking to order from these lads :

[xstron.com - XSTRON &#x7c; 3D Optical Lamp LED Bases Manufacturer Supplier Dropshipper and Wholesaler](http://www.xstron.com)

Also like bases for LED signs 


---
**Ned Hill** *February 26, 2018 00:08*

I tried the "led menu" keyword search and it didn't work for me on either Amazon or Ebay.  What worked for me was using the search "led lamp base acrylic".


---
**David Davidson** *February 26, 2018 00:26*

Going to give these a try.


---
**Stephane Buisson** *February 26, 2018 12:43*

here (without acrylic) :

[ebay.co.uk - Details about 3D 7 Color Changing Touch Switch LED Night Light Lamp Base Dock with USB Cable](http://www.ebay.co.uk/itm/3D-7-Color-Changing-Touch-Switch-LED-Night-Light-Lamp-Base-Dock-with-USB-Cable-/162573272174?_trksid=p2349526.m4383.l4275.c10)


---
**bbowley2** *February 27, 2018 19:30*

I make them custom.  I tried the round bases.  Too flimsy and @ about $8.00 each, too expensive.

![images/413cedd60ea90f5ae7f6e46f723602e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/413cedd60ea90f5ae7f6e46f723602e0.jpeg)


---
**Ned Hill** *February 27, 2018 19:32*

**+bbowley2** Where do you source your led strips from?  Been thinking about doing some edge lit things and making my own bases. 


---
**David Davidson** *February 27, 2018 20:00*

**+bbowley2** I've been thinking about that too. After all, I do have a CNC router. Do you have any details on your base you can share? How do you price your work?


---
**bbowley2** *February 28, 2018 03:58*

When I was building bases I got the LED Strips from amazon.



The bases I use now are custom with multi color lights.


---
**bbowley2** *February 28, 2018 21:14*

**+David Davidson** I have to buy 100 pieces minimum $10.00 ea.   


---
*Imported from [Google+](https://plus.google.com/+DavidDavidson-tristar500/posts/XgPgfHkN5Y2) &mdash; content and formatting may not be reliable*
