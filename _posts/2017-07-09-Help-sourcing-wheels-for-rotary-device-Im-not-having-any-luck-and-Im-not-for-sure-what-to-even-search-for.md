---
layout: post
title: "Help sourcing \"wheels\" for rotary device? I'm not having any luck and I'm not for sure what to even search for"
date: July 09, 2017 19:18
category: "Discussion"
author: "laurence champagne"
---
Help sourcing "wheels" for rotary device? I'm not having any luck and I'm not for sure what to even search for. Every other part is easily and cheaply found except for these darn wheels. Anyone have any suggestions or alternatives?





**"laurence champagne"**

---
---
**E Caswell** *July 09, 2017 19:55*

 **+laurence champagne**   You could use something like these! all you need to do is remove them from the frame and mount them on to a drive.



I turned some Derlin down on a lathe but would use these if I hadn't.





[ebay.co.uk - Details about  4 x UNIVERSAL 32mm Rubber Castor Trolley Wheels Fixed Middle 1'' Small Mini](http://www.ebay.co.uk/itm/4-x-UNIVERSAL-32mm-Rubber-Castor-Trolley-Wheels-Fixed-Middle-1-Small-Mini-/382076988999?epid=917459859&hash=item58f5902e47:g:TrMAAOSwubRXIy~c)


---
**Joe Alexander** *July 09, 2017 22:14*

i lasercut mine from acrylic and combined 4 layers, with the outside layers being slightly larger to create a lip.


---
**Stephane Buisson** *July 10, 2017 05:49*

**+Joe Alexander**  inside the lip you insert a rubber band


---
**Don Kleinschnitz Jr.** *July 10, 2017 13:08*

#K40Rotary


---
**laurence champagne** *July 10, 2017 22:54*

**+Don Kleinschnitz** Off topic, but would you mind sharing a copy of your config file? I just got the cohesion board and am having trouble dialing it in. I just watched that video with the rotary and you have that thing going faster than I thought the k-40 could lol.


---
**Don Kleinschnitz Jr.** *July 11, 2017 00:24*

**+laurence champagne** 

[http://donsthings.blogspot.com/search/label/K40%20Smoothie%20Configuration](http://donsthings.blogspot.com/search/label/K40%20Smoothie%20Configuration)



What video.... I am not running a rotary?


---
**laurence champagne** *July 11, 2017 10:57*

**+Don Kleinschnitz** I'm sorry, I just went back and looked. I though it was you but it was a video of someone else you shared. 




---
**Don Kleinschnitz Jr.** *July 11, 2017 13:14*

**+laurence champagne** no worries I do that all the time. Try searching with #K40rotary...


---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/PeSwK3oMQcr) &mdash; content and formatting may not be reliable*
