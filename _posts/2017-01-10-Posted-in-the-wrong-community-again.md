---
layout: post
title: "Posted in the wrong community .... again!"
date: January 10, 2017 16:35
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Posted in the wrong community .... again!



<b>Originally shared by Don Kleinschnitz Jr.</b>



While trying to complete ramp tests to get my focus dialed in I found that the #2 mirror was unstable.

This was caused by a improperly drilled detent. I re-drilled the hole...



If your alignment keeps changing or you have jitter in the image, especially at the extreme right, this may be the cause!



While I had the assy. out it out I added a locking screw because I am not keen on springs as a means of holding mirrors in place  while moving.



The whole story is here: [http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html)







![images/298d2a5e601728a1777eede71eaa5bbf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/298d2a5e601728a1777eede71eaa5bbf.jpeg)
![images/e9c516d6de82924689b27f2511e43219.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9c516d6de82924689b27f2511e43219.jpeg)
![images/b989ebdb9fd2b92ebeb2af47750ccf82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b989ebdb9fd2b92ebeb2af47750ccf82.jpeg)
![images/ee296b117b296513b576780aa33d956c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee296b117b296513b576780aa33d956c.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Ned Hill** *January 11, 2017 04:54*

Nice observation, would not have even considered that.  Yeah the springs are a failure point.  I had one of the pins holding one of the spring fail on me one time sending the mirror to a crazy angle.




---
**Don Kleinschnitz Jr.** *January 11, 2017 14:56*

**+Ned Hill** be careful the locking screw when tightened to much can pull the frame out of allignment, last nights test. Just need to snug up the screw. I may add another on the other side.




---
**Don Kleinschnitz Jr.** *January 13, 2017 15:41*

Added two improvements:

1: adding a plastic ferrule under the locking screw allowing some compliance and therefore keeps it from cam-ing the frame when it is tightened and messing up the adjustment. I used a plastic spacer normally used to isolate screws when mounting PCB's. Still do not over tighten. 



2. I think I found a simple way to make the mirror mount-bracket position adjustment (underneath) accessible. To properly adjust I found that I had to remove the bracket, make the coarse adjustment and then reinstall multiple times to get the bracket in the center of its adjustment range and then do the fine adjustment from the topside using the brackets mounting screws. 

I added two thumbscrews that are accessible underneath with your fingers allowing you to loosen and adjust without having to use a screwdriver while blindly hunting the screw heads.



More info and parts list in the detailed post @: 



[http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html)



![images/659f0f8935d943e56966c68f48c264a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/659f0f8935d943e56966c68f48c264a0.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/KBTmuHo6pFf) &mdash; content and formatting may not be reliable*
