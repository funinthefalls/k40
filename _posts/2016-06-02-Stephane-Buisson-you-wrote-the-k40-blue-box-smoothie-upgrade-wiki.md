---
layout: post
title: "Stephane Buisson you wrote the k40 blue box smoothie upgrade wiki?"
date: June 02, 2016 05:24
category: "Smoothieboard Modification"
author: "Alex Krause"
---
**+Stephane Buisson**​ you wrote the k40 blue box smoothie upgrade wiki? I had a a quick question regarding the level shifter I see that the ground from the power supply is connect to the shifter on the power supply side but there is no ground connected on the 3.6v side... wiring up the shifter is where im hitting a roadblock I don't want to mess it up





**"Alex Krause"**

---
---
**Stephane Buisson** *June 02, 2016 07:21*

**+Alex Krause**  in this wiki I explain what work for me (with this model of PSU), I replicate the connections used by the moshi to the psu (pwm).

Nano and other PSU combination could be different. (PWM->IN).

when you succeed, please add/edit blu-box guide.



Ground could be an issue (ground loop). see also [https://plus.google.com/114978037514571097180/posts/cFoouZqQAng](https://plus.google.com/114978037514571097180/posts/cFoouZqQAng)


---
**Stephane Buisson** *June 02, 2016 07:25*

in my case, the level shifter have common 0V(G) on 3.3v&5V side. (from memory)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/JVBpCwWsEu3) &mdash; content and formatting may not be reliable*
