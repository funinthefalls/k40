---
layout: post
title: "Bought a k40 chinese laser. It uses corel laser addon to corel draw"
date: December 02, 2015 04:31
category: "Discussion"
author: "Mtnclyde Lindeman"
---
Bought a k40 chinese laser. It uses corel laser addon to corel draw. This works but all the files sent get cut in half heigth and width. This includes text and vector cutting. Any suggestions? I selected the right mother board and there are various file formats to choose from. All have the same result.





**"Mtnclyde Lindeman"**

---
---
**Gary McKinnon** *December 02, 2015 11:01*

So the drawings are scaled by 0.5 or there is literally a line through them ?


---
**Ian Hayden** *December 02, 2015 11:27*

set dpi to 1000


---
**Mtnclyde Lindeman** *December 02, 2015 14:41*

**+Ian Hayden**

I will give this a try to see what happens. Thanks


---
*Imported from [Google+](https://plus.google.com/110233609803664556637/posts/Z1UAmWM5aB1) &mdash; content and formatting may not be reliable*
