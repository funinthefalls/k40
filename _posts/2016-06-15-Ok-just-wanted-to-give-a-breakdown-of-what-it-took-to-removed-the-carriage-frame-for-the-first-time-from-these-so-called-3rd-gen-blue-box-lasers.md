---
layout: post
title: "Ok just wanted to give a breakdown of what it took to removed the carriage frame for the first time from these so called 3rd gen blue box lasers"
date: June 15, 2016 00:34
category: "Original software and hardware issues"
author: "Ned Hill"
---
Ok just wanted to give a breakdown of what it took to removed the carriage frame for the first time from these so called 3rd gen blue box lasers. 

The problem comes in that the inside vent chute is L shaped with a big bottom drop that sits behind the back mounting ledge for the frame.  With the wide bottom drop you can't push the frame back far enough for the front to clear the edge of the case.  The vent is bolted in from the inside so it has to come out through the front.  Unbolted, it is still impossible to remove vent as it can't clear the mounting ledge.  Tilting the front of the vent down gives more room but not enough, still like 3mm short.  Above the vent chute on the inside there are 2 bolts that hold one of the mounts for the laser tube.  These block the vent from being able to till down even more.  So had to unmount the tube and removed the nuts.  Also had to remove the left hand mirror on the frame so the right side would clear the locking arm hinge mount.  After all this was finally able to wiggle the frame and vent out.  Vent is staying out now or course.  Lol, way more disassembly work then I wanted to start out with ;)



![images/d6ea8f92c392e044edf6f4301db94bac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6ea8f92c392e044edf6f4301db94bac.jpeg)
![images/dc03a60881a9c0b6c978e4be09ad7682.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc03a60881a9c0b6c978e4be09ad7682.jpeg)
![images/8103ed29c6492bb39212ec56fe0470eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8103ed29c6492bb39212ec56fe0470eb.jpeg)
![images/7d12ddca136c517a37353072049fea64.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d12ddca136c517a37353072049fea64.jpeg)

**"Ned Hill"**

---
---
**Brandon Smith** *June 15, 2016 00:51*

The vent is worthless anyway. Ventilation is much improved when it is removed :)


---
**Corey Renner** *June 15, 2016 02:39*

I had the same issue, but I wasn't willing to remove my tube.  I cut the vent apart in-place with tin snips and removed it in pieces, took about 15mins.  I designed and 3D printed a new one that mounts from the outside and only comes as far as the cut-out in the rear I-beam.  Works great and can be installed/removed easily from the outside.


---
**Corey Renner** *June 15, 2016 02:41*

BTW, I lucked-out and didn't even need to re-align anything.  Glad I made witness marks on everything before starting.


---
**Jim Hatch** *June 15, 2016 03:36*

Dremel is your friend 😃


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 06:31*

At least now you know how it all works inside :) Makes for much better understanding once you've pulled it apart & put it back together again.


---
**Don Kleinschnitz Jr.** *June 15, 2016 12:29*

Gosh this is an ugly design, needs a redo.


---
**Ned Hill** *June 15, 2016 12:50*

**+Yuusuf Sallahuddin** Very true.  That's the main justification I told myself for going through the hassle. :)


---
**Stephane Buisson** *June 15, 2016 13:23*

did you take the time to identify your stepper, any photo of belt tensioner ?


---
**Ned Hill** *June 16, 2016 00:21*

**+Stephane Buisson**  here are pics of the stepper motor and belt tensioner.  

[https://www.dropbox.com/s/c3f18dm9v9is2t3/2016-06-15%2020.08.00.png?dl=0](https://www.dropbox.com/s/c3f18dm9v9is2t3/2016-06-15%2020.08.00.png?dl=0)

[https://www.dropbox.com/s/5ydakcuro4xwdxi/20160615_133834.jpg?dl=0](https://www.dropbox.com/s/5ydakcuro4xwdxi/20160615_133834.jpg?dl=0)


---
**Tev Kaber** *July 01, 2016 05:30*

Hmm, I tried the tin snips approach, but after an hour of hacking at it, all I've managed to do is convert it into a jagged chunk of metal, still firmly wedged in there.  :( **+Corey Renner** must have better tin snips than I do.  Guess I'll try again tomorrow.


---
**Jim Hatch** *July 01, 2016 12:57*

**+Tev Kaber** If you have a Dremel or other high-speed multi-tool with a cutoff wheel that will make short work of it.


---
**Tev Kaber** *July 01, 2016 14:10*

I should have just done that, cut the vent shorter and left it in place. Instead I tried removing it entirely, I'm not sure I could reach inside with a Dremel to cut the larger part to get it out.  Sigh, now I'm feeling like I should have left well enough alone...


---
**Tev Kaber** *July 01, 2016 19:31*

File under "it seemed like a good idea at the time"... finally got the vent completely removed, now smoke removal is much worse, blows around the interior.  I should have just cut the vent shorter and left it at that.



I think the next thing I'll be designing to cut is a new vent.  :P


---
**Jim Hatch** *July 01, 2016 19:38*

Do you have the rear exhaust sealed? I used a rubber weatherstripping around the stock housing to get mine sealed so it pulled the smoke out. (I also have a bank of inward directing small fans in the front panel pushing the air towards the back but sealing the exhaust should be enough to keep it clear in there.)


---
**Tev Kaber** *July 01, 2016 19:42*

Yeah, no smoke is leaking out of the machine, but rather than getting pulled straight back out of the machine, it swirls around first.  I don't have any inward fans, though.


---
**Ned Hill** *July 01, 2016 19:49*

I take it your's doesn't have ventilation slits in the front cover?


---
**Tev Kaber** *July 01, 2016 19:57*

Yeah, no slits on the front. 


---
**Ned Hill** *July 01, 2016 20:00*

You could try and prop the door up just a little bit to let some air flow across.  I've seen some 3d printed replacements and I believe one where someone cut one out of wood.


---
**Tev Kaber** *July 01, 2016 20:52*

I tried propping it open, didn't seem to make a difference. I think it needs a little directing in the back. My 3D printer is currently out of commission, so I'll probably lasercut something. 


---
**Otto Souta** *March 29, 2017 10:01*

I removed the offending slot, cut 120mm dia hole in the back of the machine, mounted 120mm powerful 12V computer fan on 6mm plywood to correspond with the hole, printed 120mm square to 100mm round hose connector, blocked all holes around the machine and propped the lid 8mm on two plastic feet. This creates nice airflow across the cutting surface.


---
**Abe Fouhy** *October 25, 2017 20:04*

**+Ned Hill** How is the k40 working with the exhaust fully removed? Did you just remove the four bolts in the back and front to remove that?


---
**Ned Hill** *October 25, 2017 20:27*

**+Abe Fouhy** it works just  fine without the internal exhaust vent.  If you read the post above I had to removed the gantry (four bolts) and unmount the tube because of clearance issues with one of the tube mounting bracket bolts.  K40 cases can vary so you might not have to unmount the tube on yours.  If you remove the gantry I recommend using a marker to create registration marks on the gantry supports to make lining it back up easier.  


---
**Abe Fouhy** *October 25, 2017 23:45*

**+Ned Hill** Thanks so much Ned, I did reread that, sorry for the double question. I appreciate the help.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/TWpTcM9gQeY) &mdash; content and formatting may not be reliable*
