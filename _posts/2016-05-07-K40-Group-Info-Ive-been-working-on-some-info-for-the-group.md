---
layout: post
title: "K40 Group Info I've been working on some info for the group"
date: May 07, 2016 23:39
category: "Discussion"
author: "Scott Marshall"
---
K40 Group Info



I've been working on some info for the group. Here's some of it. I have more coming, including drawings and info on common mods.



I will be selling kits in the near future for individual upgrades, like a "Lighting Kit", Interlock Kit" etc. they will be available as 'parts & instructions' or Solder free "plug in" versions for people who don't want to do the wiring themselves. All my designs will be published here, so you can source and build them yourself, or use my drawings as "inspiration" for your own way of doing it. I've been approached several times about the kits, so I'll be starting with a few simple ones, and we'll see how it goes.



I was forcibly retired from my successful Industrial Controls business in 1998 by a serious health problem. ALL-TEK Systems was my company. I had 4 employees and worked for IBM, AT&T, Bristol Meyers/Zimmer, Welch-Allyn (Winner of “Partners in Quality Award” there) and a variety of less well known companies.  We specialized in custom solutions to manufacturing  and assembly line challenges. I really miss the work.



I'm the sort who just can't take sitting around, so this kit building experiment is as much for my sanity as any financial gains. I am still am 'under the weather' a good portion of the time, so have  been reluctant to commit to a business which requires my full time attention. I also refuse to delay someones order just because I took ill after they placed it. In order to prevent that, I'll only put the kits up for sale one at a time as they are finished and ready to ship. I still need to finish my website and put it up, so this may be a few weeks off. If you really want/need something in the meantime, drop me a line and I'll do my best to accommodate you. If you're interested in being a “Beta tester”, drop me a line. Any experience level is fine. I'm not looking to 'go big' on this, only to help out my friends here and in the RC community with their hobby, and maybe defray at least the cost of my materials.



Anyway, here is what I have learned in the past couple days of dissecting the K40's heart (Brain?) the M2 Nano Controller Board, Rev 7.





Here is a link to download the documents (Thanks to Halfnormal for making that happen)

[https://drive.google.com/file/d/0B9qXcw3VRDg0S0xrS1dReXpXWDRRUFlTcXNvNUNvaW8tVDVR/view?usp=sharing](https://drive.google.com/file/d/0B9qXcw3VRDg0S0xrS1dReXpXWDRRUFlTcXNvNUNvaW8tVDVR/view?usp=sharing)﻿



If you have any information to add to my data, find errors, or just have comments, please send it to my email, and I will add it to the our “Reference Section”



I hope this helps out a few of you who are doing controller upgrades etc.



![images/3ca60631238e16477c9252cd4c0b9429.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ca60631238e16477c9252cd4c0b9429.jpeg)
![images/c36ebcaa92d76bf912fed4c02db6d9d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c36ebcaa92d76bf912fed4c02db6d9d2.jpeg)
![images/a591019703109330852668afbe75f2e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a591019703109330852668afbe75f2e9.jpeg)

**"Scott Marshall"**

---
---
**Jim Hatch** *May 08, 2016 00:23*

Nice idea. Thanks for contributing. One note on "Connector #4", the flat FFP one. On my Nano board that's the one used for the Y-axis control. The JST-4 connector you have for your Y-axis is not used on my Nano board.



Not sure why sometimes they use the JST connector and sometimes the FFP one. Maybe a difference in which factory uses which kind of cables.


---
**Ariel Yahni (UniKpty)** *May 08, 2016 00:31*

Your da men **+Scott Marshall**​ good luck


---
**Scott Marshall** *May 08, 2016 01:47*

**+Jim Hatch** 

Double checked, Thought maybe I just mixed them up - It happens after you stare at a board long enough. Mine has the flat cable tied to what is marked the "X" JST, it's the one directly adjacent to the FFP connector.



Maybe they have the board silkscreen wrong? It could be they have the JST Y(marked) connector hooked to the X axis. 



It makes complete sense that they would run the Y thru the FFP cable as it's the moving motor, while the X is fixed.



I need to go look at my chassis to see how it was connected.

The only other thing I can think of is maybe you have a different revision?



Once I figure it out, I'll note it on the table. Rev A! Didn't take long.



Thanks for the heads up, Good catch!



Scott


---
**HalfNormal** *May 08, 2016 02:26*

**+Scott Marshall** email me the docs and I will post them on Google drive for anyone to download. larrygon at gmail. 


---
**Scott Marshall** *May 08, 2016 02:35*

Will do, already sent them to Yuusuf.



I tried to use the link for the EL site, but couldn't do a thing with it. Linked to my dropbox, wasn't sure where to go from there. Don't want to make a mess of it for you guys to fix. Sorry for being so behind on this communication medium.



Thanks!



Scott.


---
**HalfNormal** *May 08, 2016 02:38*

**+Scott Marshall** No worries. We appreciate all you do and contribute! 


---
**Scott Marshall** *May 08, 2016 03:17*

CONNECTOR SOURCES



I will be stocking these soon, (think I have some JST-4s but need to root through my "inventory") If I find some 4 pins, I'll let you know.



I have FFP connectors coming in. I'm doing my own adapter or "middleman" arrangement with screw terminals. They'll be cheap, and available soon.



Another tidbit that just occurred to me, If you need JST connectors, shop for "Balance Connectors" the JST connector is commonly used on Lipo RC batteries for balance charging , it brings out each individual cell for the charger to even out the voltages.



I find EmilyandLilly on the bay to be reliable, cheap and fast for that sort of thing. Hobbyking.com is probably cheapest, but a tad slow sometimes. If you only need a couple, Ebay is best.



So far I've only found the FFP connectors at Digi-Key.  Shipping is steep there.


---
**HalfNormal** *May 08, 2016 05:28*

Here is the link to the files

[https://drive.google.com/file/d/0B9qXcw3VRDg0S0xrS1dReXpXWDRRUFlTcXNvNUNvaW8tVDVR/view?usp=sharing](https://drive.google.com/file/d/0B9qXcw3VRDg0S0xrS1dReXpXWDRRUFlTcXNvNUNvaW8tVDVR/view?usp=sharing)


---
**Scott Marshall** *May 08, 2016 05:58*

**+HalfNormal** Thanks, I'll move it to the top.


---
**Phillip Conroy** *May 08, 2016 07:15*

good work


---
**Tony Sobczak** *May 29, 2016 19:03*

Following


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/3MvkGvZ8in2) &mdash; content and formatting may not be reliable*
