---
layout: post
title: "variable machine zero stop Hi everyone, I'd like to share a simple solution to set a variable zero stop for the laser head"
date: January 11, 2018 18:55
category: "Discussion"
author: "java lang"
---
variable machine zero stop

Hi everyone, I'd like to share a simple solution to set a variable zero stop for the laser head. In my system there is no electrical endstops mounted, I'm always working relative (setting the head to the workpiece upper left corner). But in some cases  manually positioning the head isn't precise enough (although I have laserpointers mounted) and there is a need for mechanical stops. Let's say you have an amount of identically pieces you want to engrave/cut relatively to the pieces origin but you must move the laser head while changing the pieces. The piece itself is in a fixed position on the bed, i.e. by using a jig. 

And here is my solution to accomplish this task:

a) for the Y-transport I'm using a strong magnet. The magnet is attached to one of the Y-rods and the magnet-surface is protected with a tape to avoid scratches on the rod. Now I can move  the magent to define the wanted Y-stop postion.

b) for the X-transport (magnets don't work here) I'm using a sheet from an unused PC-slot and is cut/bend so that it fits on the underside of the X-rail. I drilled a threaded hole for a fixing screw. Now I can move the sheet to the wanted X-Position and fix it with the screw.

Hopefully I made myself clear and you can see the mechanism on the pics,

happy cutting

 



![images/6b4672023da88c66e03f969523d9d314.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6b4672023da88c66e03f969523d9d314.jpeg)
![images/96bbd77ab54363fea4b332bd65f9e292.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96bbd77ab54363fea4b332bd65f9e292.jpeg)
![images/0a37ebacad03ddf1560a1cc6132edfe8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a37ebacad03ddf1560a1cc6132edfe8.jpeg)

**"java lang"**

---
---
**Paul de Groot** *January 11, 2018 20:45*

Is that solution not grinding up your x and y rubber belts or is it very forgiving?


---
**java lang** *January 11, 2018 20:49*

**+Paul de Groot** No, there is no contact with the x/y-belt. 


---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/NsJa9mvRMq6) &mdash; content and formatting may not be reliable*
