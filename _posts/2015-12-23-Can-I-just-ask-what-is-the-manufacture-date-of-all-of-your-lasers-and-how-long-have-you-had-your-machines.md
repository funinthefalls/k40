---
layout: post
title: "Can I just ask what is the manufacture date of all of your lasers and how long have you had your machines?"
date: December 23, 2015 02:56
category: "Discussion"
author: "Anthony Coafield"
---
Can I just ask what is the manufacture date of all of your lasers and how long have you had your machines? Is a 2.5 year old laser in a (supposedly) brand new machine normal? Thanks.





**"Anthony Coafield"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 23, 2015 07:25*

I received my K40 in early October 2015 & I've just checked the laser tube & found that it has absolutely no details at all on it. Just one sticker that has a barcode & says something about "Do not tear up" (can't read it all as it is curving beneath the tube).


---
**Anthony Coafield** *December 23, 2015 22:50*

Thanks Yuusuf. 


---
**I Laser** *December 28, 2015 09:47*

Mines undated too on both machines I've got. Maybe the guys that sold yours got lax on removing the sticker lol!


---
**Anthony Coafield** *December 28, 2015 20:57*

Thanks **+I Laser** They probably couldn't get in to take it off on mine due to the stripped screw!


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/aDmXU7R9vuw) &mdash; content and formatting may not be reliable*
