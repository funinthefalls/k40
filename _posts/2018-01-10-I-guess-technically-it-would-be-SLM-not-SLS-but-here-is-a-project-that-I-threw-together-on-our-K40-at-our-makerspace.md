---
layout: post
title: "I guess technically it would be SLM not SLS, but here is a project that I threw together on our K40 at our makerspace"
date: January 10, 2018 00:25
category: "Discussion"
author: "Ryan Branch"
---
I guess technically it would be SLM not SLS, but here is a project that I threw together on our K40 at our makerspace. The goal for me was to try and make it as minimally invasive as possible. So I limited myself to using only minor hardware changes. No electronics or smoothie configs were changed. 



Is it practical? No.   Is it useful? Not really.     Is it cool? It's definitely amusing.    I am still working on posting everything (layer change script, simplify 3d profile, etc) to Hackaday.io, but I thought I would share this video first since it gives a general overview.





**"Ryan Branch"**

---
---
**Adrian Godwin** *January 10, 2018 00:31*

Would it work with icing sugar ?

And could you add a spar below the carriage to wipe the surface ?




---
**Ryan Branch** *January 10, 2018 00:48*

Not sure about icing sugar. I did add a bar below in the second version which is shown off about half way through the video. (Sorry it's such a long video). 



Not sure if powdered sugar is the same as icing sugar. But I know from Evil Mad Scientist Laboratories sugar slm printer from 10 years ago, that most of powdered sugar is actually starch and not crystalline sugar.



I actually won't be doing any more testing on this project, but I will be posting the details so other people can replicate it and experiment if they want.


---
*Imported from [Google+](https://plus.google.com/101842495658501740623/posts/ZzGMcWE7MnA) &mdash; content and formatting may not be reliable*
