---
layout: post
title: "hell to all I have not been using my laser for a while now but came up with an idea for it some time ago i recall someone on the forum that they had some success with engraving a design on metal has there been anything new on"
date: August 09, 2017 21:13
category: "Discussion"
author: "Dennis Fuente"
---
hell to all I have not been using my laser for a while now but came up with an idea for it some time ago i recall someone on the forum that they had some success with engraving a design on metal has there been anything new on this using the k40 thanks 





**"Dennis Fuente"**

---
---
**Jim Hatch** *August 09, 2017 21:49*

Depends on what you're thinking of for "engraving". I have successfully marked aluminum (iphones, ipads) - it results in a white colored "engrave" on the silver anodization of the metal. It looks like an engrave on wood but there's no real depth so it's marking not technically engraving.



Similarly I've used Cermark/Thermark as well as Dry Moly Lube to create black images on other metal materials. These are more like the way a laser printer fuses toner to a piece of paper. The sprays deposit molybdenum onto the metal which when lasered (treating it like an engraving), fuses the stuff to the metal and the rest can be washed off with alcohol. It looks like the stainless steel water bottles you've no doubt seen that have logos and things "engraved" or printed on them. Again, it doesn't actually cut into the metal but it leaves a scratch resistant (almost scratch proof) black image as if it were engraved.


---
**Dennis Fuente** *August 10, 2017 16:46*

Jim 

Thanks for your reply sorry for the miss spelling of hello anyway what i wish to do is mark metal for hand engraving so i might give this a try with automotive moly spray do you think this would work also in the post here there's what looks like a response in Chinese don't know what that's about thanks again 


---
**Jim Hatch** *August 10, 2017 18:02*

The automotive spray I use is CRC Dry Moly Lube Spray. It's about $11 at the local autoparts store. It's also available online from Amazon I believe.


---
**Dennis Fuente** *August 10, 2017 21:36*

Jim 

Thanks i am going to give it a try.




---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/H6xYEib2q4X) &mdash; content and formatting may not be reliable*
