---
layout: post
title: "Hi Great to find this group. I have a K40d Laser Machine and need a new Power Supply"
date: December 03, 2017 20:58
category: "Discussion"
author: "Frank Farrant"
---
Hi

Great to find this group.  



I have a K40d Laser Machine and need a new Power Supply.   Can anyone help ?



Really appreciate it.   I'm in the UK / Oxford.



Thanks

Frank





**"Frank Farrant"**

---
---
**Andy Shilling** *December 03, 2017 22:07*

Heres the one I use along with the Cohesion 3d mini control board. Takes a few weeks to arrive in the Uk but seem pretty reliable.



[aliexpress.com - New MYJG-40 110/220V 40W Power Supply K40 Co2 Laser Engraver Rubber Stamp Mini Engraving Cutting Machine 3020](https://www.aliexpress.com/item/New-MYJG-40-110-220V-40W-Power-Supply-K40-Co2-Laser-Engraver-Rubber-Stamp-Mini-Engraving/32666595117.html?spm=2114.search0104.3.8.snbXwi&ws_ab_test=searchweb0_0,searchweb201602_3_10152_10065_5000015_10151_10344_10068_10130_10345_10324_10342_10547_10325_10343_10340_10341_10548_5130015_10192_10541_10190_10084_10083_10307_10301_10303_10539_5080015_10312_10059_10313_10314_10184_10534_100031_10604_10603_10103_10605_5060015_10596_10142_10107,searchweb201603_1,ppcSwitch_3&algo_expid=f0aff1dc-d119-40fe-ae60-514b68642ffc-1&algo_pvid=f0aff1dc-d119-40fe-ae60-514b68642ffc&rmStoreLevelAB=5)


---
**Don Kleinschnitz Jr.** *December 03, 2017 22:22*

There are multiple different LPS's. Check your type and insure that you buy a replacement with the right type of connections.



Many available also on amazon and ebay.


---
**Frank Farrant** *December 03, 2017 22:25*

Hi 

This is the same model so I'd hope it would be ok.  

I really need a PS in the next couple of days, so was hoping I could find one in the UK.  Any ideas ?

Thanks




---
**Andy Shilling** *December 03, 2017 23:25*

As Don said try Ebay or Amazon you might be lucky enough to find one that you could get in a couple of days.




---
**Justin Mitchell** *December 06, 2017 11:48*

When i looked afew weeks back there was only one uk seller at crazy prices, but there are a few on ebay that hold stock in germany or italy, the shipping should only be a few days and you will avoid any potential wrangles with customs. Just hit the 'Item Location: European Union' option in ebay search


---
**Frank Farrant** *December 12, 2017 23:39*

Hi

I received the new Power Supply today and connected the cables at the front and the red cable at the back to the tube but still no laser.   Any ideas ?

The machine powers up, the internal light and red dot work but no laser. 

Green light on on the board. 

How can I test for output from the PS ?

Thanks in advance 

Frank 


---
**Don Kleinschnitz Jr.** *December 13, 2017 12:20*

**+Frank Farrant** 



Its unlikely that the new supply is bad.

More likely: Either the supply is not enabled or the tube is bad.



Insure the supply is enabled. 

1.) turn the pot to its max position. This assumes you DO NOT have a digital power control. If your power control is digital return here and let us know.



2.) the pin "IN" should read near 5VDC.



3.) if the pin reads 5VDC pulse the TEST button ON THE SUPPLY.



4.) if the laser does not fire your problem is likely the tube or its connections



5.) if the IN pin does not read 5VDC with the pot turned fully up then return to this forum for more help.



............



...its difficult and dangerous to verify the supply is outputting. 



<b>I DO NOT RECOMMEND THIS PROCEDURE. It should only be employed by a professional that has been trained in High Voltage troubleshooting procedures</b>



 but, 



you can test the LPS for an output by creating an arc from the anode to a grounded wire.



<b>This is dangerous because:</b>



... Voltages as high as +20,000VDC at relatively high current are silently present.

... Arcs are unpredictable

... The HV supply looks and sounds innocuous until it nails you.

... You can damage the LPS with an open arc.



.......................

1.) WITH THE POWER OFF, use the Chicken Stick Procedure in the  post below to discharge the anode of the supply (see section A1: Chicken Stick Procedure.)



2.) Connect a wire to electrical ground, the case, somewhere in or near the laser compartment.



3.) After using the "Chicken Stick Procedure" Expose the anode by pulling the silicon sleeve off the anode pin. 



4.) Tape the other end of the bared & grounded wire to the tube about 1/2 inch from the anode. I use black electrical tape wound around the tube. 



5.) GET FULLY AWAY FROM THE LASER COMPARTMENT



6.) With one hand behind your back touching nothing, use the other hand to PULSE (do not hold) the laser test button to ON from the control panel.





A NASTY arc should occur.......



[donsthings.blogspot.com - Repairing the K40 LPS #1](http://donsthings.blogspot.com/2017/07/repairing-k40-lps-1.html)

a


---
**Frank Farrant** *December 13, 2017 21:38*

Hi Don

I'm probably as technical as a doughnut, but I learn fast.  



I have a K40d with digital readouts on the top.  



I have changed the tube, before the China Tech guy said it was the PS.   



Before I do anything else, can you let me know the best way to re-fit the 2 cables to the tube please, so I can re-test it.



Many thanks for the help so far.








---
**Don Kleinschnitz Jr.** *December 14, 2017 15:39*

Check my blog, there are replacement instructions there. 

I did not know you had a digital panel. I wonder if it is properly controlling the supply.


---
**Don Kleinschnitz Jr.** *December 14, 2017 15:40*

Have you checked the voltage on the power supplyd "IN" pin,?


---
*Imported from [Google+](https://plus.google.com/110419041310362555971/posts/EjJDZT2XNis) &mdash; content and formatting may not be reliable*
