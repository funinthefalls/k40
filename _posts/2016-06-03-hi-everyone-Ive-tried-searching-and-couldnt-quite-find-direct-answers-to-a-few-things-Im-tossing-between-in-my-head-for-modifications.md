---
layout: post
title: "hi everyone, I've tried searching and couldn't quite find direct answers to a few things I'm tossing between in my head for modifications"
date: June 03, 2016 03:11
category: "Modification"
author: "Pippins McGee"
---
hi everyone,

I've tried searching and couldn't quite find direct answers to a few things I'm tossing between in my head for modifications. hoping a few of you may like to share your opinion or knowledge please?



1) For honeycomb table install

Can anyone advise which is better:



Lightobject honeycomb [http://www.lightobject.com/300X200-Honeycomb-Fit-K40-machine-P705.aspx](http://www.lightobject.com/300X200-Honeycomb-Fit-K40-machine-P705.aspx)

or

saite_cutter ebay honeycomb [http://www.ebay.com.au/itm/Honeycomb-Work-Bed-Table-Platform-CO2-40W-3020-Laser-Stamp-Engraver-K40-32x22cm-/252339652332](http://www.ebay.com.au/itm/Honeycomb-Work-Bed-Table-Platform-CO2-40W-3020-Laser-Stamp-Engraver-K40-32x22cm-/252339652332)



Appears lightobject table is BIGGER than the ebay one, how is that possible and still fit in the k40? which one will actually fit properly if they are different sizes....?

Shipping cost from lightobject is a killer...(3x the price of the product just for shipping.. but if its the better solution...)





2) saite_cutter laser head incl. air assist [http://www.ebay.com.au/itm/40W-Tube-Mini-CO2-Laser-Rubber-Stamp-Engraving-Shenhui-SH-K40-3020-Head-Mount-/252289904595](http://www.ebay.com.au/itm/40W-Tube-Mini-CO2-Laser-Rubber-Stamp-Engraving-Shenhui-SH-K40-3020-Head-Mount-/252289904595)



VS



the lightobject laser head incl air assist.[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)



Which is better?

According to my reading, the saite_cutter head is adjustable and so is more flexible for if your bed isn't exactly at the stock 50.8mm height from the laser anymore.

Any other points worth noting?



3) Is this a suitable air pump for air assist?

[http://www.ebay.com.au/itm/100L-MIN-Electromagnetic-Pond-Aquarium-Hydroponics-Air-Pump-12outlets-AU-stock-/111996991723?hash=item1a138a78eb:g:u54AAOSwll1WxDYi](http://www.ebay.com.au/itm/100L-MIN-Electromagnetic-Pond-Aquarium-Hydroponics-Air-Pump-12outlets-AU-stock-/111996991723?hash=item1a138a78eb:g:u54AAOSwll1WxDYi)



or is this ia better choice (with a little 3L tank)

[http://www.ebay.com.au/itm/Voilamart-1-6-hp-Compressor-Air-Brush-Stencils-Hose-Kit-for-Airbrush-Spray-Gun-/122002689631?hash=item1c67ed4e5f:g:smIAAOSwKfVXGASQ](http://www.ebay.com.au/itm/Voilamart-1-6-hp-Compressor-Air-Brush-Stencils-Hose-Kit-for-Airbrush-Spray-Gun-/122002689631?hash=item1c67ed4e5f:g:smIAAOSwKfVXGASQ)



apologies if these have been answered before. I tried searching but couldn't find direct answers to these.



thanks.





**"Pippins McGee"**

---
---
**Derek Schuetz** *June 03, 2016 04:47*

I bought light object head and honey comb. Honestly the air assist head doesn't produce enough pressure even with an airbrush compressor (had a 3d printed air assist at first and it was like a jet). As far as the bed you need something to raise it. Designed and 3D printed some legs that raise it to the correct height to cut 3mm material and if I'm cutting thinner I just put something one top of the 3mm material


---
**Pippins McGee** *June 03, 2016 05:44*

**+Derek Schuetz** hi derek, the air assist I mainly just want for keep smoking off the lens as currently smoke bellows up and engulfs the lens and the laser beam whilst cutting.

I assume even the smallest amount of air volume and pressure should keep smoke blown away as long as its constant air flow.



RE: the honeycomb. you are saying it is not a plug and plug fitment into the k40? one must create custom 'legs' of the correct height in order for the bed to sit at the right focual distance from the laser head?



the modifications really add up on top of the machines cost I'll tell you that. sheesh


---
**Phillip Conroy** *June 03, 2016 06:35*

What are you using your laser cutter for?


---
**Pippins McGee** *June 03, 2016 06:48*

i personally use for 3mm MDF/ply cutting (maybe 6mm if it can) and soon cutting 3mm cast acryllic , my wife use for cutting paper/card (selling wedding invitation packages etc.)

the current bed is a huge pain with that silly clamp thing poking up off the surface - need to change it for something better asap.


---
**Phillip Conroy** *June 03, 2016 07:20*

I cut 3mm mdf and find it is very dirty,with a ghoneycomb bed you will be forever cleaning it..I use magnets with 65mm long screws held on top,like a pin bed .so far i have only needed to make up 10 sets of magnets/screw combos to hold my work peace and they are movable .l have a metal tray that i sit the magnets on and weekly remove and run under hot water while brushing to remove the buildup of the sticky stuff.

Honycomb beds are great for paper and other stuff that needs a lot of support.

You could make a removable honeycomb bed fot use with paper and use magnets /screws for mdf


---
**Derek Schuetz** *June 03, 2016 07:28*

**+Pippins McGee** ya the honey comb bed is just that the k40 has no adjustable bed height so you need to raise the honey comb to the correct height depending on the thickness of the material. For example 3mm you need to raise the honey comb 64mm


---
**Pippins McGee** *June 03, 2016 10:06*

Thanks Phillip.

I see Derek.

Is this the go to solution for a cheap adjustable height honeycomb?



[http://www.ebay.com.au/itm/131778752714](http://www.ebay.com.au/itm/131778752714)


---
**Derek Schuetz** *June 03, 2016 15:19*

Ooh that is really neat I may need to get home of those 


---
**Pippins McGee** *June 03, 2016 16:15*

**+Derek Schuetz** yes I think it is what I will try. with an extended lever that can poke out the front of the machine for adjustment.

however the one linked will be too tall i think,  even in its shortest form (80mm)



hopefully one like this

[http://www.ebay.com.au/itm/4-Stainless-Steel-Lab-Stand-Table-Scissor-Lift-laboratory-Jiffy-Jack-100-100mm-/282017811436?hash=item41a9924bec:g:8T8AAOSw4hdXH1U3](http://www.ebay.com.au/itm/4-Stainless-Steel-Lab-Stand-Table-Scissor-Lift-laboratory-Jiffy-Jack-100-100mm-/282017811436?hash=item41a9924bec:g:8T8AAOSw4hdXH1U3)

is short enough in its contracted form (45mm)


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/Uw6jTjAVJKu) &mdash; content and formatting may not be reliable*
