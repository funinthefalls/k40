---
layout: post
title: "Hello K40 Fans. I am having a minor issue hopefully I can get assistence?"
date: September 16, 2016 08:09
category: "Original software and hardware issues"
author: "Gideon Sauceda"
---
Hello K40 Fans. I am having a minor issue hopefully I can get assistence? I am currently running the corel laser software that is supply with the machine. I need to set the laser to only pass 1 time. Currently the laser passes 2 times. Ive tried multiple ways and have set the repeat to 0 and it sets at 1?



I hope I can pass once only? Thank you!!





**"Gideon Sauceda"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 08:21*

It's an issue with the width of the line. Set your line width for cut objects to 0.01mm. When you set to hairline (or above) it registers it as needing to cut on both sides of the line.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 08:22*

Oh, you want "repeat = 1". Really it should be called "Passes = 1" because it is an option for how many passes rather than how many repeats.


---
**Gideon Sauceda** *September 16, 2016 08:39*

Great! Ill get on that asap. Thank you.


---
**Phillip Conroy** *September 16, 2016 08:55*

You can also flood fill the object if it suits


---
**Gideon Sauceda** *September 16, 2016 21:36*

**+Yuusuf Sallahuddin** Hello yuusuf. I have made them changes within the laser software and that work perfect. Now, when i import a bmp file or jpeg and its a solid line for example a square i now get this double sided laser cut? Ive tried various thickness and formats. Ive now check the mother board and have notice the device id is incorrect. Going to enter the id and hope this helps? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 21:45*

**+Gideon Sauceda** For cutting you don't want to use bitmap or jpeg formats. You want to use vector formats (such as SVG, AI, I think DXF also). Even a 1 pixel line in a bitmap will possibly register as 2 sided cut. Definitely change the Device ID to what is correct, as the laser likes to do weird things when that is set wrong.


---
**Gideon Sauceda** *September 16, 2016 22:33*

**+Yuusuf Sallahuddin** currently i am working on ai. I am exporting the drawing to .bmp .png most.

I am not getting a solid black when i export out. This may have somerhing to do with this trace function. ??? I will try it soon. Thank you


---
**Gideon Sauceda** *September 17, 2016 00:14*

i have made  the changes and enter the proper id to the device. i am so confused? am i missing a software to help me with fine cuts? i see some people working with "Corel 4"  and exporting is this what's missing? I am currently on Adobe Illustrator and I draw paths?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 01:16*

**+Gideon Sauceda** Draw your paths in AI, then export your files from Adobe Illustrator as AI9 format. Then import that into CorelDraw. Then make sure the line width is 0.01mm when you have it imported. Then try cut. It should only cut once.



In higher versions of CorelDraw (x5, x7 I tested) you can import SVG files easily. CorelDraw12 that came with my K40 would always make the SVGs come out strange & AI formats greater than AI9 would be weird.



Also, check these settings:

[https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)


---
**Gideon Sauceda** *September 17, 2016 01:19*

THANK YOU! I guess what's missing is COREL DRAW.. he he he i am working direct on Laser Draw. I am missing "Corel Draw" Guess i never installed it or it did not work? since no manual was provide its been fun finding any info in the dark. I am going to seek a copy now.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 01:25*

Oh, you're using LaserDrw. Well, LaserDrw is a standalone software. With CorelDraw, you will need CorelDraw & the CorelLaser plugin (both were on my original CD they provided). You will need to install CorelDraw 1st, then install CorelLaser second. Then, to run it, you run the CorelLaser file. It will open CorelDraw & the plugin's toolbar.


---
**Gideon Sauceda** *September 17, 2016 01:31*

i moved and lost the cd. i am currently going to download i guess a link you have for the copy of the cd. 

i had sense i was missing something, I believe it did not want to install and since i work in adobe illustrator 

it did not matter to me. man,, you have been much knowledgeable. thanks again!


---
**Gideon Sauceda** *September 17, 2016 04:36*

did a double install. one of my towers is running windows 10 and it was giving me issues. I went back 

to my windows xp and it works right on. Now I got to learn this software. I added your perimeters and got 

some what acquainted. still my night mare persists double stroke, i did load both software's and both see

to be communicating. now, i got to learn how to import my files from ai. thank you. again


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 06:05*

**+Gideon Sauceda** You're welcome. Hope you can work out the double cut & file import issues. Any extra assistance just ask away & someone will help :)


---
**Gideon Sauceda** *September 17, 2016 15:23*

EUREKA! Finally got it. it was a short battle of settings and finally got it... WOW! so excited to have control of this machine for now.. :) Thank you!!! ill post what I make in the next two days.


---
**Gideon Sauceda** *September 17, 2016 22:42*

![images/b6374967dc4f2013a4084fb83847da5e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6374967dc4f2013a4084fb83847da5e.jpeg)


---
**Gideon Sauceda** *September 17, 2016 22:42*

![images/01241915b99fa7f41936c62857974796.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/01241915b99fa7f41936c62857974796.jpeg)


---
**Gideon Sauceda** *September 17, 2016 22:44*

Yuusuf heres whats xome out the k40 with corel draw. Thanks for the help!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 17, 2016 23:34*

**+Gideon Sauceda** Nice work :) Hope to see more of your work in the future.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 19, 2016 21:16*

**+Adam Gibbons** See my reply to your post. You may need to get CorelDraw & CorelLaser to see if that works. Note: not sure what controller your machine runs on, but my stock controller was the m2nano variant.


---
*Imported from [Google+](https://plus.google.com/116888087804682757951/posts/3GonD8jxnFf) &mdash; content and formatting may not be reliable*
