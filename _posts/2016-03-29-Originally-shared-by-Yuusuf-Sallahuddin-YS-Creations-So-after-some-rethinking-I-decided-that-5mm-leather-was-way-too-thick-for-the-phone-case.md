---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) So after some rethinking, I decided that 5mm leather was way too thick for the phone case"
date: March 29, 2016 08:14
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



So after some rethinking, I decided that 5mm leather was way too thick for the phone case. I've changed it up & now using 1-1.5mm thick kangaroo hide.



So I started by dyeing the exterior sides of 2 pieces of leather (300x200mm).



Then the 1st piece was laser cut for the interior section (that holds the phone). Not pictured at the moment because it is being wet formed into shape.



Then the 2nd piece was laser cut for the exterior section. Part of the exterior section folds back to the inside, for the front screen cover. So again, I've wet it to fold it & leaving it to dry (pictures shown).



To be fancy, I added my logo onto the front screen cover flap & added 2 slots internally that will fit either credit cards or business cards.



So much easier to work with the kangaroo hide. 5mA @ 30mm/s cutting, 3 passes. I did the logo section just as 1 pass with same settings and it almost cut through the leather, so 3 passes was not necessary.



![images/a6386bad5c8e2b58a1ba95d25ef486f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a6386bad5c8e2b58a1ba95d25ef486f3.jpeg)
![images/7ecec746a6e6b2fe7aa23ec4a7a92ece.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7ecec746a6e6b2fe7aa23ec4a7a92ece.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/7agzqf8FwZB) &mdash; content and formatting may not be reliable*
