---
layout: post
title: "My nephew's first birthday party is this weekend so I made a cake topper and tiny toppers for cupcakes"
date: June 02, 2017 03:31
category: "Object produced with laser"
author: "Jeff Johnson"
---
My nephew's first birthday party is this weekend so I made a cake topper and tiny toppers for cupcakes. The propellers spin even on the tiny planes which are cut from 1/32 inch plywood. They have a 2 inch wingspan.

![images/b6b6e2a4911e0fe7b3799c0fcfb8e3c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6b6e2a4911e0fe7b3799c0fcfb8e3c6.jpeg)



**"Jeff Johnson"**

---
---
**Ned Hill** *June 02, 2017 03:41*

Wow, that's awesome!  Lol I wish I had gotten my laser before my daughter was no longer a "kid". :P


---
**Anthony Bolgar** *June 02, 2017 03:45*

What a nice job, your nephew will be pretty pleased I am sure. And **+Ned Hill**, your kids will always be kids at heart, my daughter and her husband are into Cosplay at conventions like Comic Con, so I get to make them all sorts of props.


---
**Joe Alexander** *June 02, 2017 04:34*

You would be amazed at how many of my friends kids love it when I give them various 3DBenchy's I print. Maybe its the mocking voice calling them "tugga'!" like that horrible south park episode about Russel Crowe, who really knows? :P Made my godson one "as big as his head" boy was he grinning!


---
**Don Kleinschnitz Jr.** *June 02, 2017 12:31*

**+Joe Alexander** impressive and fun!


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/8zapLeAct3P) &mdash; content and formatting may not be reliable*
