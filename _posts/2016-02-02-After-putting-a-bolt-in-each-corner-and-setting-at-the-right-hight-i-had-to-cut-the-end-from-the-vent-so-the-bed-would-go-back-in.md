---
layout: post
title: "After putting a bolt in each corner and setting at the right hight i had to cut the end from the vent so the bed would go back in"
date: February 02, 2016 08:14
category: "Modification"
author: "beny fits"
---
After putting a bolt in each corner and setting at the right hight i had to cut the end from the vent so the bed would go back in



![images/825ee968f351b1840221083bf166133a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/825ee968f351b1840221083bf166133a.jpeg)
![images/711d3e9adb03573ecbf63e5925033cdc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/711d3e9adb03573ecbf63e5925033cdc.jpeg)
![images/96be713bc8d02874baf4eb61275f6925.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96be713bc8d02874baf4eb61275f6925.jpeg)
![images/661faa46be494b7997c8529822495a12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/661faa46be494b7997c8529822495a12.jpeg)
![images/10a791d5b3dc5ca849235d309a37c6a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/10a791d5b3dc5ca849235d309a37c6a6.jpeg)
![images/c4060ebb50dacb656debd6a8af0ca85e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c4060ebb50dacb656debd6a8af0ca85e.png)
![images/d4eba760638bc4cc45245f66f2075cfa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d4eba760638bc4cc45245f66f2075cfa.jpeg)

**"beny fits"**

---
---
**I Laser** *February 02, 2016 08:52*

Pretty common, I did the same to my first machine. To be honest I wish I'd taken the whole vent out!



My latest machine has four screws holding the vent in. Probably the only thing that is better lol.


---
**beny fits** *February 02, 2016 08:53*

Yeah my vent is welded in or i would have taken it out 


---
**I Laser** *February 02, 2016 09:00*

Yeah my first machine is welded too, can't even easily cut it out from behind... Such a pain in the arse lol!


---
**Jim Hatch** *February 02, 2016 14:43*

Another mod to consider is getting a set of nylon spacers from Home Depot or wherever to allow you a way to quickly adjust the depth of the bed. I have mine set to 1/4" material because that's what I usually use but have sets that I can use to lower it a bit for 3/8" or raise it for 1/8". Or create an adjustable mount for the focusing head so you can raise or lower it easily. I'll do that next now that my Lightobjects head is in and I'm done with modding that. I didn't want to build a custom adjustable mount and then swap out a new head it wouldn't fit with.


---
**Ashley M. Kirchner [Norym]** *February 02, 2016 20:42*

Mine was bolted in ... I say <b>was</b> because I removed it. And I 3D printed a 'corner' that allows me to put material at the same exact position each time. And the laser beam's 0,0 is exactly 1mm into the material. Works flawlessly each and every time.


---
**beny fits** *February 02, 2016 20:50*

Yeah when I finally get a 3d printer I'll have some fun making things might even make a new laser cutter lol


---
**Jim Hatch** *February 02, 2016 21:49*

**+beny fits**​ You can use the laser to cut some plywood or acrylic spacers or corners or alignment blocks. I like acrylic because I can cut thicker acrylic than plywood with my laser (or I use the makerspace laser which is 60W).


---
**Ben Walker** *February 04, 2016 14:08*

Mine is bolted in but when I removed the screws it still would not budge.  It seems to be put in there like a jigsaw puzzle and bends around the tube so I may have to get this clever because I am oh so tired of printing air assist nozzles.  Even my mod of a mod sometimes hits and breaks the collar.  And that is nearly an inch of working space I really could use.




---
**Ashley M. Kirchner [Norym]** *February 04, 2016 17:30*

**+Ben Walker**, I had to remove the entire X-Y rails to get the duct out. It could be that yours is the same, from the inside. There are 4 bolts holding the rails in place, two on the back and 2 in the front. Check for those.


---
**Ben Walker** *February 04, 2016 17:41*

Oh good.  I am going to look at it better.   I'm assuming the rails have to come out.  Thanks for that tip Ashley.  


---
**Ashley M. Kirchner [Norym]** *February 04, 2016 17:53*

Yeah, you have to pull the whole thing out. Which means, when you put it back in, your mirrors will be out of alignment. Just be aware of that.


---
**Jim Hatch** *February 04, 2016 18:14*

**+Ashley M. Kirchner**​ but the upside is you can get the rails square & true (mine weren't quite on when I got mine out of the box). :-)


---
**Ashley M. Kirchner [Norym]** *February 04, 2016 18:34*

Yep, I had to do the same with mine. Even the gantry was messed up.


---
**Jez M-L** *August 23, 2016 16:25*

How/what did realign the rails to? I imagine a big engineers square off of the laser tube? But even then the outer glass of the tube may not be perpendicular with the laser line. Can you share you method please as I would like to get a very thing back in as square as possible. 



Cheers!


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/Bds4Gq1ofMG) &mdash; content and formatting may not be reliable*
