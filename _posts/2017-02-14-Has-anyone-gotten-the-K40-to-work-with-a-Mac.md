---
layout: post
title: "Has anyone gotten the K40 to work with a Mac?"
date: February 14, 2017 04:24
category: "Discussion"
author: "Brian Hughes"
---
Has anyone gotten the K40 to work with a Mac?  I'm debating buying a K40 and needing to buy a windows laptop to make it work.  Thanks!





**"Brian Hughes"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 14, 2017 04:44*

The only option I'm aware of for running on a Mac (with the default software) is to use Bootcamp or a virtual machine running Windows.



Next option is to upgrade the controller board to one that allows you to use LaserWeb (which is usable on Mac also as it just requires Google Chrome to run).



PS: I ran the stock software on my Macbook using Bootcamp/Windows 7 & 10 before I upgraded my controller.


---
**Jorge Robles** *February 14, 2017 07:45*

Try virtualbox or other virtualizer. Anyway you will end replacing the motherboard for a gcode one, and you will the run with your favorite os


---
**Michael Omiccioli** *February 14, 2017 15:56*

I use Vbox all the time on my MAC.  I like it better than Boot Camp because I don't want to reboot.


---
**Phil Willis** *February 14, 2017 16:51*

Not sure the software dongle will work with a virtual machine..


---
**Brian Hughes** *February 14, 2017 16:56*

Thank you all.  Has anyone successfully used Vbox on the Mac running Windows 10 with the software dongle (stock software) with the Corel compatible controller board?  Michael, do you use Vbox to run the K40 specifically on your Mac?  Hoping to validate this will work before buying, then perhaps upgrade the control board later.


---
**Jorge Robles** *February 14, 2017 16:56*

W7 is enough


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 14, 2017 17:58*

**+Brian Hughes** Can't say I used Vbox, but with Bootcamp & Win 10 the stock software ran. Although, eventually the Corel Draw 12 failed (since it was a pirated version that they provided) & wouldn't work. So switched to Corel Draw x5 & then eventually x7. Combined with the CorelLaser 2013.02 plugin they gave to control the laser it worked well.



You could always test an install on Vbox & test Corel Draw x5 or 7 with the plugin (obviously you can't control a laser you don't have, but you can see if the software runs at least). I have hosted copies in either my dropbox/onedrive/google drive if you would like to test.


---
**Brian Hughes** *February 14, 2017 20:20*

Thank you, Yusuuf.  That is very kind of you.  So you mean that I can set up VirtualBox with Windows on my Mac, and then try out running your files that came with the K40 to double check that they run in the VM before purchasing?  That would be really helpful.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 14, 2017 23:07*

**+Brian Hughes** I don't have the CorelDraw 12 that came with the K40 anymore (as it stopped working for me & many others) but I have CorelDraw x7 hosted here: [https://1drv.ms/f/s!AqT6a-6Vl15Nh7lqpg7o6Ap5MGg1aQ](https://1drv.ms/f/s!AqT6a-6Vl15Nh7lqpg7o6Ap5MGg1aQ)



There is a keygen that goes with that & a bit of stuff around to install/activate. I can't remember the exact process. But you can probably just use the trial without activating for testing purposes (as it doesn't really matter if it runs out in 30 days).



You can get CorelLaser 2013.02 from here: [http://www.china-cncrouter.com/download_page2.html](http://www.china-cncrouter.com/download_page2.html)

(Number 6 in the list).



I have a feeling there may have been a USB driver for it too, but that shouldn't be necessary for testing purposes without a machine (& I don't think I have that shared anywhere).


---
**Brian Hughes** *February 15, 2017 04:15*

Wonderful, I'll give this a try, thank you Yuusuf.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2017 04:39*

**+Brian Hughes** You're welcome. Hope it helps you with your decision to buy.


---
**Brian Hughes** *February 15, 2017 14:07*

**+Yuusuf Sallahuddin** I installed Virtual Box and Windows 10, and everything looks to be working fine.  I'm able to extract and install the CorelLaser and CorelDraw packages.  When I go to run CorelLaser, I believe the first time I saw a message saying "The USB key is not plugged in", which makes sense, since I don't have one yet.  But when the application continues, it gives me a dialog box error of "The UI language registration list is invalid." And then takes me to a web page in all Chinese.  Can you tell me what happens when you run the program?  Have you seen the error above?  Thanks again!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 15, 2017 17:47*

**+Brian Hughes** I've never came across that UI language error before. Basically for me, it comes up saying "USB Key is not plugged" & then loads Corel Draw (with the plugin in the top right & also Windows system tray icon).



Does CorelDraw/CorelLaser stay open when that error pops up & takes you to a webpage?


---
**Brian Hughes** *February 15, 2017 18:25*

**+Yuusuf Sallahuddin** Ah, perhaps that's my problem.  I believe that I tested opening CorelLaser while I was still downloading CorelDraw and hadn't installed it yet.  Does CorelLaser rely on CorelDraw to be open already?  Or does it open CorelDraw when it opens?  Maybe it's looking for UI information from CorelDraw? I believe CorelLaser closes after the UI error and loads the page.  I left CorelDraw installing this morning and will check tonight.

Yusuuf, thank you so much for helping.  It is impressive how many people you help in this forum, and it can be the difference between success or failure.  I appreciate it!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 00:41*

**+Brian Hughes** Yeah, CorelLaser opens CorelDraw. Wiithout CorelDraw I have no idea what it would do (but probably fail since it is not a standalone software).



There is another software option that came with the K40 called LaserDRW, but personally I would advise against using it (as it is a lot less functional, e.g. can only use BMP/JPG etc images for both cutting & engraving, no vector images like SVG for cutting with higher precision). Also, not many people in this community use it so there will be less help available if you encounter an error or issue.


---
**Brian Hughes** *February 16, 2017 03:46*

**+Yuusuf Sallahuddin** It worked!  When I start CorelLaser, it tells me that the engraving device isn't connected, but then loads Corel Draw and the toolbar for CorelLaser in the upper right hand corner.  So I believe that means that this VirtualBox solution is going to work on my Mac.  Then it should recognize the USB key when I insert it, as well as the USB connection to the K40, so those errors should go away.  Time to go make an offer on the K40 I saw.  Thanks again!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 16, 2017 04:19*

**+Brian Hughes** That's good news. Just for reference sake, when I purchased my K40 (around August 2015) it was about AU$550. Ended up getting a $50 refund for alignment issues so any problems with your machine, notify the seller as they generally offer pretty decent partial refunds to assist with the costs of rectifying the issues. There was a period (just before Chinese New Year) where K40s were ridiculously priced. Not sure if they have dropped since, but I'd imagine so. Just keep an eye out for the ones around the price range I paid (I think some here got them around US$300-350 odd).


---
*Imported from [Google+](https://plus.google.com/113283736370890620439/posts/8kW2SCtyx8J) &mdash; content and formatting may not be reliable*
