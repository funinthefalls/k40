---
layout: post
title: "Hi, someone build or buy a rotary attachment for glass engraving??"
date: January 28, 2016 15:27
category: "Hardware and Laser settings"
author: "Alessandro Zupo"
---
Hi, someone build or buy a rotary attachment for glass engraving??





**"Alessandro Zupo"**

---
---
**Coherent** *January 28, 2016 16:40*

Lightobject sells a small one but it will not connect directly to the stock driver board, you need a dsp controller & separate stepper driver. I've seen plans and info for folks who have built their own so you may find more info if you search the web. I'm not sure as to whether or not  one can be controlled easily using a smoothie or ramps controller... maybe someone who has installed one of these controllers can offer more in that regard.


---
**Alessandro Zupo** *January 28, 2016 16:49*

The electronics is not the problem.... the mechanics is the problem.... ever!! :(


---
**Stephane Buisson** *January 28, 2016 17:06*

don't forget the soft, I don't have it but I heard laserdraw manage rotary. (double check it)


---
**Coherent** *January 28, 2016 17:09*

There was a member on here a while back who cut the main parts out of thin plywood with his laser and built a fairly simple one. He obtained a similar spec'd stepper motor and wired it in place of the Y axis. His photos showed good results using it. Unfortunately he closed his account on this forum and deleted all his posts here and on thingiverse. One of the current members here may have downloaded the plans and info when it was available and be willing to pass it on?


---
**Stephane Buisson** *January 28, 2016 17:43*

yep it was alreadygone, he change of name, but didn't reappear in community list. so the post posts disappear too. but the photos was still there in the photo sections before disappearing recently too.

the last one you can see is in my pdf

[https://plus.google.com/117750252531506832328/posts/NMModyZPhrL](https://plus.google.com/117750252531506832328/posts/NMModyZPhrL)


---
**Scott Thorne** *January 29, 2016 10:27*

Laser draw does support the rotary attachment, my engraver came with one, although I've only used it a few times engraving wine glasses for friends, it works great but I think it only interfaces with the dsp controller not the software because there are no settings in laser draw for a rotary attachment, the y axis motor is unhooked and the rotary fixture is connected in its place.


---
**Stephane Buisson** *January 29, 2016 10:52*

**+Peter van der Walt** an option for Laserweb maybe ...


---
**Anthony Bolgar** *January 30, 2016 11:17*

Here is alreadygones files, I uploaded them to thigiverse.

[http://www.thingiverse.com/thing:1304278](http://www.thingiverse.com/thing:1304278)


---
*Imported from [Google+](https://plus.google.com/103855001849930569119/posts/ZyWxdRgqdAZ) &mdash; content and formatting may not be reliable*
