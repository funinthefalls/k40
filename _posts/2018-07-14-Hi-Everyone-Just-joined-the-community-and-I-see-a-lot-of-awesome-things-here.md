---
layout: post
title: "Hi Everyone, Just joined the community and I see a lot of awesome things here"
date: July 14, 2018 16:55
category: "Original software and hardware issues"
author: "G7"
---
Hi Everyone,



Just joined the community and I see a lot of awesome things here. Last night while running the engraver, the power supply seems to have burnt out. I am trying to find a replacement near Orange County, NY. Does anyone have any suggestions?





**"G7"**

---
---
**Don Kleinschnitz Jr.** *July 14, 2018 20:34*

What power supply... the Laser Power Supply?

If so you can find these on Amazon and Ebay.

Pay attention to the connectors on yours an get a compatible one. There are at least two types.



Post a picture of the bad supply please.



Why do you think the LPS is dead?



Check out my blog lots of info on these supplies...

[donsthings.blogspot.com - Don's Things](https://donsthings.blogspot.com/)


---
**G7** *July 15, 2018 14:30*

**+Don Kleinschnitz** Hey thanks for the feedback.We were working on room signs on 1/8" acrylic. 4 passes, Fast and high power to avoid melting the acrylic and reducing the letter sizes too much. By the third pass the laser was not working. It had been running for about an hour. We had 2 electricians check it out and both determined the power supply is not working.


---
**G7** *July 15, 2018 14:32*

**+Don Kleinschnitz** specifically this part.![images/a13c486b24948f892915b5acb32181fe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a13c486b24948f892915b5acb32181fe.jpeg)


---
**Don Kleinschnitz Jr.** *July 15, 2018 17:18*

**+G7** 



NOTE: <b>THIS POWER SUPPLY OUTPUTS LETHAL VOLTAGES AND SHOULD NOT BE OPENED OR REPAIRED BY ANYONE NOT EXPERIENCED WORKING WITH HIGH VOLTAGES!</b>



That is the High Voltage Transformer which you can get from Ebay. There are different models.



Look at my blog there are procedures and parts listed. Just insure you get the right type.



[donsthings.blogspot.com - Troubleshooting a K40 Laser Power System](https://donsthings.blogspot.com/2018/05/troubleshooting-k40-laser-power-system.html)


---
**G7** *July 15, 2018 17:25*

Thanks for the warning. We had professional electricians looking at it, well equipped.Will check it out.


---
*Imported from [Google+](https://plus.google.com/109407960884705143978/posts/H7qGR6v5d8R) &mdash; content and formatting may not be reliable*
