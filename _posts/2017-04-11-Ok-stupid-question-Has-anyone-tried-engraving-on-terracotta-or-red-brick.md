---
layout: post
title: "Ok, stupid question. Has anyone tried engraving on terracotta or red brick?"
date: April 11, 2017 21:17
category: "Materials and settings"
author: "Jeff Bovee"
---
Ok, stupid question.  Has anyone tried engraving on terracotta or red brick?  Any ideas about this even if you haven't tried it would be helpful.  Many thanks in advance.





**"Jeff Bovee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2017 22:42*

No, but it sounds like a cool idea. Some have engraved on stone such as slate, so maybe they have some ideas of settings to try.


---
**HalfNormal** *April 12, 2017 03:23*

People engrave on rocks, granite and slate. I do not see why it is not possible. The contrast might not be that great. A lot of people will tape off an area, engrave off the tape and then paint the exposed area.


---
**Jeff Bovee** *April 12, 2017 15:17*

Thanks for the input.  I'm going to give it a try on a sample piece.  My plan is to use it to etch my uncle's image onto an urn I am making for my aunt.  He was a brick layer and she saw the urn I made for my dad and told me how my uncle was still in the plastic box that the ashes come in and after thinking about it, I can't let that stand.


---
*Imported from [Google+](https://plus.google.com/115193415731522119744/posts/S88XTnfWwXH) &mdash; content and formatting may not be reliable*
