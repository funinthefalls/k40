---
layout: post
title: "Anyone have plans for an adjustable Z table and also a rotary attachment for engraving glass bottles?"
date: September 21, 2015 07:07
category: "External links&#x3a; Blog, forum, etc"
author: "Anthony Bolgar"
---
Anyone have plans for an adjustable Z table and also a rotary attachment for engraving glass bottles?





**"Anthony Bolgar"**

---
---
**William Steele** *September 21, 2015 10:03*

LightObject already sells a z lift table.  



[http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx](http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx)


---
**William Steele** *September 21, 2015 10:05*

They also have a rotary attachment.



[http://www.lightobject.com/Mini-two-axis-rotary-Ideal-for-K40-engraving-laser-machine-P941.aspx](http://www.lightobject.com/Mini-two-axis-rotary-Ideal-for-K40-engraving-laser-machine-P941.aspx)


---
**Anthony Bolgar** *September 21, 2015 12:34*

I saw those but they are kind of pricey, was hoping for a DIY solution.


---
**Chris M** *September 22, 2015 14:20*

There's a rotary attachment on Thingiverse [http://www.thingiverse.com/thing:299018](http://www.thingiverse.com/thing:299018)


---
**William Steele** *September 22, 2015 15:20*

Very cool!!!  I'll be building one of these tonight!


---
**Al Tamo** *September 22, 2015 21:34*

El rotary no funciona en k40 verdad?


---
**Anthony Bolgar** *September 24, 2015 00:14*

Josef, I am interested.


---
**Anthony Bolgar** *September 24, 2015 00:55*

Thank you very much.


---
**Joey Fitzpatrick** *September 24, 2015 02:46*

The rotary attachment that is on thingyverse requires an easy driver to drive the stepper(assuming your using a stepper with different specs than the K40 y-axis motor)  Has anyone used this design yet.  If so, how are the connections made to the main board.( is it connected directly to the Y-axis wiring or does it need to be connected to the stepper driver inputs on the main board.  Both the easy driver and the Lihuiyu  mainboard use the same stepper driver. (Allego A3967)

The main difference is that the easy driver has an adjustable current pot.


---
**Al Tamo** *September 26, 2015 08:32*

**+Joey Fitzpatrick** pero funciona con la tarjeta por defecto de la k40? Con la lihuiyu añadiéndole el easy driver?


---
**Joey Fitzpatrick** *September 26, 2015 13:41*

This is the easy driver I was referring to.    [https://www.sparkfun.com/products/12779](https://www.sparkfun.com/products/12779)


---
**Anthony Bolgar** *September 29, 2015 00:16*

Josef, do you have pictures and the CAD drawing yet?


---
**Anthony Bolgar** *September 30, 2015 02:27*

Thanks Josef


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/L4VDrvpiEEP) &mdash; content and formatting may not be reliable*
