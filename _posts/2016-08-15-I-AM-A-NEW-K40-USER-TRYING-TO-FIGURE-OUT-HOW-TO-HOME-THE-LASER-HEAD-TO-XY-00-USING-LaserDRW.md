---
layout: post
title: "I AM A NEW K40 USER TRYING TO FIGURE OUT HOW TO HOME THE LASER HEAD TO X,Y 0,0 USING LaserDRW"
date: August 15, 2016 11:32
category: "Hardware and Laser settings"
author: "kevin burke"
---
I AM A NEW K40 USER TRYING TO FIGURE OUT HOW TO HOME THE LASER HEAD TO X,Y 0,0 USING LaserDRW. UPON SENDING A FILE, MY LASER HEAD SLAMS REPEATEDLY AGAINST THE RIGHT RAIL BEFORE STARTING THE JOB (WITH TRACKING ERRORS) AND RETURNS THERE AFTERWARDS. HELP!!





**"kevin burke"**

---
---
**Jim Hatch** *August 15, 2016 12:13*

Did you put the Machine ID and Device settings in correctly. Failing to home right is a typical problem for new setups. 



The machine entry is the only one with an "M2" in it. The ID is screen printed (16?) alphanumeric characters printed on the board. You can also set your bed size in the settings screen so you don't try to travel too far right & down.


---
*Imported from [Google+](https://plus.google.com/109636808956174571385/posts/21X74wEtB6K) &mdash; content and formatting may not be reliable*
