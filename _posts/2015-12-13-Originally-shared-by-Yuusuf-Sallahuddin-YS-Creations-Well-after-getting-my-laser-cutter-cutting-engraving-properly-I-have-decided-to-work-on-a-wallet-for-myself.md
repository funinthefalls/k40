---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Well after getting my laser cutter cutting & engraving properly I have decided to work on a wallet for myself"
date: December 13, 2015 13:28
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Well after getting my laser cutter cutting & engraving properly I have decided to work on a wallet for myself.



It's a work in progress, as I've never done my own wallet-inner before. So I've designed one that allows 8 card slots, 1 notes slot & 2 little pockets behind the cards. I personally dislike having coins in my wallet (hence no coin pocket in this one).



So here's some progress photos. More to come later.



![images/6e288b64e6cadbdb57616401f5afc1d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e288b64e6cadbdb57616401f5afc1d4.jpeg)
![images/c7691d9cf7ed13142e4e3aba7b2be294.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c7691d9cf7ed13142e4e3aba7b2be294.jpeg)
![images/831abc0af902752a51c39ca431307cfd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/831abc0af902752a51c39ca431307cfd.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Scott Thorne** *December 13, 2015 15:21*

Dude...Looks awesome!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2015 16:58*

**+Scott Thorne** Thanks. I'm very happy it's working well now.


---
**Scott Thorne** *December 13, 2015 17:26*

Good job man....keep up the good work!


---
**Anthony Coafield** *December 13, 2015 21:16*

Looking great. Once I'm finished with Christmas gifts (left it late this year) I want to try one of these. I might have to pick your brain on how to go about it (and where to get the materials from)...



[http://www.artofmanliness.com/2015/11/20/detectives-wallet/](http://www.artofmanliness.com/2015/11/20/detectives-wallet/)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2015 02:06*

**+Anthony Coafield** Sure, that's a reasonably simply but cool design. I'd be happy to give some advice when you're interested.


---
**Anthony Coafield** *December 14, 2015 03:43*

Thanks man. I'll hit you up in the new year. 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/LZhqovVLDp9) &mdash; content and formatting may not be reliable*
