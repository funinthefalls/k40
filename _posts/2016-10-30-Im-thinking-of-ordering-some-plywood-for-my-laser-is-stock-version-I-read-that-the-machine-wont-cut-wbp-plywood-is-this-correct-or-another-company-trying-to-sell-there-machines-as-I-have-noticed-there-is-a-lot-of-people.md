---
layout: post
title: "I'm thinking of ordering some plywood for my laser is stock version I read that the machine won't cut wbp plywood is this correct or another company trying to sell there machines as I have noticed there is a lot of people"
date: October 30, 2016 10:16
category: "Discussion"
author: "Rob Turner"
---
I'm thinking of ordering some plywood for my laser is stock version I read that the machine won't cut wbp plywood is this correct or another company trying to sell there machines as I have noticed there is a lot of people saying the laser won't cut certain stuff but then promot their product 





**"Rob Turner"**

---
---
**Scott Marshall** *October 30, 2016 11:17*

I've bought from KC hardwoods several times, and am happy with the wood. I have no stake in any supplier, but shopped around and settled here. I know plywood fairly well as I've been building model aircraft for many years. The industry standard is Midwest brand ply, and it' decent stuff (they have a 'lite' ply for aircraft - you don't want that for regular laser work).

The stuff I get from KC is uniform, flat and cuts well. You do have to buy a fair amount, but it's less than you think and you can go through it pretty fast.

The standard package is 45 12x12 sheets of 1/8 for about 50 bucks. That includes shipping.

I've bee using it for my planes lately and it is every bit as strong and durable as the Midwest.



Look for KC Hardwoods on ebay. I had an invoice around because I gave the address to someone a few weeks ago, but can't find it now. You can buy direct from them. They are in Toledo Ohio if my memory serves.



Scott




---
**Alex Krause** *October 30, 2016 12:15*

I posted a source for baltic birch ply a month or two ago it came out to around 1$ per sheet 12"x12" squares and people on the Facebook group swear up and down by it...It's the glue that is used to fill voids that causes the problem with cutting... I have found that ply I have purchased from lowes and home depot are pretty hit and miss with the quality of cutting


---
**Jim Hatch** *October 30, 2016 13:57*

Like Alex says, it's the glues that cause most of the problems. I've had good results from Home Depot/Lowes and problems as well. Even Baltic Birch I get can occasionally cause an issue (usually only a problem with living hinge cuts) if I get an odd piece. 



I get mine from Woodcraft since there's a local store and I can get full sheets of 30x60 BB there reasonably priced.


---
**Cesar Tolentino** *November 01, 2016 21:10*

I always use the 5mm Lauan ply from home depot and have no issues with it. 4 passes at 800mm/min @ 8ma power.  I also bought iron ply but have not tried cutting it yet


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/FA18kegKk4y) &mdash; content and formatting may not be reliable*
