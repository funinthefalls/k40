---
layout: post
title: "I am finally able to get LaserDrw to engrave and cut the basic templates (circle, square etc) but It won't import a file"
date: September 10, 2016 15:47
category: "Original software and hardware issues"
author: "Bruce Golling"
---
I am finally able to get LaserDrw to engrave and cut the basic templates (circle, square etc) but It won't import a file.  When I try to import a bmp all the dialogs come up okay but when I click on the open file button, nothing happens.

I even tried to paste into the program from the clipboard but LaserDrw doesn't appear to connect to win xp clipboard.

Does anyone have an idea what's going on here.







**"Bruce Golling"**

---
---
**Jim Hatch** *September 10, 2016 16:21*

Try to save it as a b&w 1-bit bmp before importing.


---
*Imported from [Google+](https://plus.google.com/105808932307807066174/posts/b7rmDrXz9XW) &mdash; content and formatting may not be reliable*
