---
layout: post
title: "Today our community reach a milestone !"
date: February 07, 2016 10:19
category: "Discussion"
author: "Stephane Buisson"
---
Today our community reach a milestone ! 1000 members. Bravo to you.



Please post only subject of interest for all, remember Posting is like to be on a stage in front of 1000 persons, so not a good place for Me Myself & I ...  please don't overload.

We try to organise the subject by category, to ease search.

Thank you for your understanding.

comments in a post are not limited, enjoy.

![images/ab89257c0d7eaa74c7719677187641dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab89257c0d7eaa74c7719677187641dc.jpeg)



**"Stephane Buisson"**

---
---
**Stephane Buisson** *February 07, 2016 10:20*

comment field for stat purpose:

creation date, birthday 7/11/2014

community reach 100 members on 12/2/2015

community reach 200 members on 11/5/2015 

community reach 300 members on 16/7/2015 

community reach 400 members on 9/9/2015 

community reach 500 members on 16/10/2015 

community reach 800 members on 5/1/2016 

community reach 900 members on 25/1/2016

community reach 1000 members on 7/2/2016

community reach 1250 members on 25/3/2016

community reach 1500 members on 6/5/2016


---
**Gary McKinnon** *March 30, 2016 12:32*

Woohoo :) I haven't disappeared, Stephane, just waiting until my workroom is sorted out.


---
**Gunnar Stefansson** *May 09, 2016 10:56*

1530 Really great that this group is growing... There's just so much good knowledge here.


---
**Gary McKinnon** *May 09, 2016 10:59*

Great news.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/1xs24FLtQAi) &mdash; content and formatting may not be reliable*
