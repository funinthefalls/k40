---
layout: post
title: "My new lamp shade. Made from amazon prime deliveries"
date: May 16, 2016 13:50
category: "Object produced with laser"
author: "Ben Walker"
---
My new lamp shade.   Made from amazon prime deliveries.   Really like the play of light. 

![images/8ad27d1be2574b7f00367f6c73d3339f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ad27d1be2574b7f00367f6c73d3339f.jpeg)



**"Ben Walker"**

---


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/JK35TfytUHp) &mdash; content and formatting may not be reliable*
