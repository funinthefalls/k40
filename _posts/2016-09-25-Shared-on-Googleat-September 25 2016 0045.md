---
layout: post
title: "Shared on September 25, 2016 00:45...\n"
date: September 25, 2016 00:45
category: "Object produced with laser"
author: "Amer Al fk"
---




![images/6ea3f0110cdde2e154234289b784fd97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ea3f0110cdde2e154234289b784fd97.jpeg)
![images/2f6ace4ce51ec2fde7bc913f42a018cc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f6ace4ce51ec2fde7bc913f42a018cc.jpeg)
![images/3ae0bcca2fba2fdc30173876485297fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ae0bcca2fba2fdc30173876485297fd.jpeg)
![images/91b71d6fb5c0bd8c708c40e4ed63058a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/91b71d6fb5c0bd8c708c40e4ed63058a.jpeg)

**"Amer Al fk"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 25, 2016 12:31*

Wow, that's beautiful bro. Nicely done :)


---
**Amer Al fk** *September 25, 2016 13:50*

Thanks bro :)


---
*Imported from [Google+](https://plus.google.com/101804487986287829578/posts/UCXnuwEqubA) &mdash; content and formatting may not be reliable*
