---
layout: post
title: "Got my AWC 708 lite, for a good deal and fast shipping by FedEx ."
date: February 18, 2016 04:39
category: "Hardware and Laser settings"
author: "varun s"
---
Got my AWC 708 lite, for a good deal and fast shipping by FedEx . Its was $260+30ish for FedEx.

![images/08c3dde51ea55294b3740017da105128.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08c3dde51ea55294b3740017da105128.jpeg)



**"varun s"**

---
---
**varun s** *February 18, 2016 05:19*

Actually cheapest I would say! Will post the store link so anyone willing to buy from Aliexpress can do so.


---
**Michael Schroeter (MiSc)** *February 18, 2016 16:36*

**+varun s**​ Very cheap, I am curious what the shipping to germany will cost.﻿


---
**varun s** *February 18, 2016 17:00*

Bought it from this seller [http://www.aliexpress.com/store/product/China-Hot-sale-AWC-708C-Lite-laser-engraving-cutting-machine-controller-anywell/613094_32519171579.html](http://www.aliexpress.com/store/product/China-Hot-sale-AWC-708C-Lite-laser-engraving-cutting-machine-controller-anywell/613094_32519171579.html)  ask the seller to check shipping for you and adjust your cart amount to that :-) and ask for May Long (Seller)she knows stuff she sells.


---
**Jose A. S** *February 20, 2016 05:31*

I am trying to connect this controler to this drivers DRV8825. Can anybody help me with the diagrams? 


---
*Imported from [Google+](https://plus.google.com/116786516372429379291/posts/NzaYdXKomk4) &mdash; content and formatting may not be reliable*
