---
layout: post
title: "What do people use for the honeycomb cutting tables and is it possible to use a Z table with one"
date: May 13, 2016 20:30
category: "Hardware and Laser settings"
author: "Eric Rihm"
---
What do people use for the honeycomb cutting tables and is it possible to  use a Z table with one





**"Eric Rihm"**

---
---
**Jim Hatch** *May 13, 2016 20:45*

I don't use a honeycomb myself. Spent about $10 for some Strong-ties mending plates from the lumber store. They're flat galvanized steel with sharp points you're supposed to hammer into two boards to help hold them together. I have them glued to a piece of MDF with the points up. On top of that I have a thin piece of expanded aluminum to create a nice flat surface. It sits on various spacers I've got for the common material sizes I use.



Others have made nail beds (drive nails through a piece of plywood or MDF and flip it over points up so you can rest your material on it).



A Z-axis will support honeycomb. You can get that from Amazon, eBay and LightObject. I'll probably add LO's powered Z-axis to my K40 this summer and will likely go the honeycomb route then.


---
**Gunnar Stefansson** *May 13, 2016 21:10*

For my laser bed I use the grid of a office lamp the old school square ones that have some thin aluminium sheets, I just thought the honeycomb ones were so expensive as my bed is 800mmx800mm and I'd need to buy multiple as they didn't come in that size...


---
**Trực Chính** *May 13, 2016 21:41*

**+Gunnar Stefansson** where did you get metal grid? I looked for metal but only found plastic at Home depot.


---
**3D Laser** *May 13, 2016 22:04*

Eric I have a honeycomb bed from light objects sitting on top of a lab Jack that can be manually adjusted up and down it works great for my purposes 


---
**Gunnar Stefansson** *May 14, 2016 08:18*

**+Trực Chính** you need to find them in the scrapyard or someone selling them, search for "light fixture grid" on google images. thats the kind of grid I'm using.


---
**Nathaniel Swartz** *May 14, 2016 12:26*

**+Corey Budwine**​ how big is the platform on your lab Jack?


---
**HP Persson** *May 14, 2016 20:24*

I´m using 1" nails in a pattern on my bed, good enough and doesnt reflect the laser at all.

But i do 99,9% cutting, no engraving almost.



Pic: -> [http://wopr.nu/laser/spikebed.jpg](http://wopr.nu/laser/spikebed.jpg)

In this pic is also my adjustable laser head. I have a K40D, another head than most K40´s, so i could change it to my own design.


---
**Brandon Smith** *May 21, 2016 19:11*

I am running the same setup as corey, light objects honeycomb with lab Jack. My jack is the 4x4 inch.


---
**3D Laser** *May 21, 2016 19:17*

**+Nathaniel Swartz**  I have an 8 inch lab Jack 


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/7d2vXvd7zm3) &mdash; content and formatting may not be reliable*
