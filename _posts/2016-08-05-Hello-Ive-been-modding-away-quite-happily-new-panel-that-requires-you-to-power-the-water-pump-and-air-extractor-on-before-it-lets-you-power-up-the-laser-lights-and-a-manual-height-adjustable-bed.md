---
layout: post
title: "Hello.. I've been modding away quite happily, new panel that requires you to power the water pump and air extractor on before it lets you power up the laser, lights and a manual height adjustable bed"
date: August 05, 2016 09:34
category: "Modification"
author: "Peter Gisby"
---
Hello.. 

I've been modding away quite happily, new panel that requires you to power the water pump and air extractor on before it lets you power up the laser, lights and a manual height adjustable bed. 

I've got the chain and pipe in for air assist and have recently fitted the LO air assist head with a nice new lens.

My question to the group is this.. What is the best option for a pump for the air assist system?



I have tried a couple of small compressors, one is for an air-brush and the other for a domestic spray gun, but they keep overheating and cutting out. I've also tried a 250LPM fish tank pump, which runs forever without overheating but doesn't seem to have the same effect on the cut as the higher pressure devices.



Can someone explain, in simple terms, what is required for good air assist - PSI or LPM, and which pump / compressor / source is favoured, good bonus if it's relatively quiet and fits in with the PSU.



All help very much appreciated.



Thanks.

Peter.





**"Peter Gisby"**

---
---
**Phillip Conroy** *August 05, 2016 10:17*

I too gryed fish tank compressers $90 au and could not get results i wanted,hooked up my garage 2hp 50litrr tank compressor 30meters away in s seperate brick garage and have never looked back.Be aware that useing this type of compressor/tank setup has its plusses and negetives,plusses are with a watertrap/pressure regulator i can agjust the flow to just what i need-for eteching bugger all ,vor cutting 3mm mdf 10psi-

negrtives are i had problems of water trap catching 95% of water in the air and allowing 5% to hit the focal lens causing a mosture covered lens after only 20 min of cutting.

I fixed this by making a 25mm diamenter by 1 meter long pvc pipe filled with silca gel[water absorbjng beads] and running the low pressure air  out ov the regulator througt the pvc pipe then the air assist hoss.

So far i have not had any moisgure on the focal lens aftrr 50 hours of cutting


---
**Phillip Conroy** *August 05, 2016 10:20*

Forgot to add that the silca gel beads i brougt are renewable by microwaving them in a cloth bag for 5minutes ,or untill they change back to original vcolor.also added a automatic air tank valve that drains the water from tank every 15minutes for 2 seconds


---
**Bart Libert** *August 05, 2016 10:54*

I see on ebay you can buy a lot og these beads. Can you please tell me what color your beads have when they are fresh and what color if they are saturated ?




---
**Phillip Conroy** *August 05, 2016 11:49*

10% of them are blue


---
**Phillip Conroy** *August 05, 2016 11:50*

Clear color when saturated


---
**Bart Libert** *August 05, 2016 12:13*

and how much gel do you use in that pvc pipe ? what weight aprox?




---
**Phillip Conroy** *August 05, 2016 15:23*

1/2 kg -total weihhf 935grams-6mm clear pipe goimg from water trap at laser cutter to pvc pipe than 6mm jose ftom pvc pop pipe to air  assist.Air pumps that do not store the compressed air do not have this problem as water condensors out of the airr when it comes out of the air tank as the air expands and cools at the same time and cooler air can not hold as much  moistue as hot air.

To test this just get any spray can and release any ov the contentsor 1 minute,you will notice can is cold at the end of the time.


---
**Phillip Conroy** *August 05, 2016 15:32*

Using about 10 psi when cutting 3mm mdf and a pin bed made up of 12 10mm by 15mm long cinder shaped magnets with 65mm wood screw held on to the top of the magnets by magnetisium and a removable metal tray i can cut 3mm mdf with out any smoke getting on to the bottom sidevof theut mdf ,very clean cut on the bottom and the remoable tray allows me to keep the laser bed clean by just removing the tray and magnets/screws combos once a week and using hot water and a brush clean then very quickly and easyily


---
**Phillip Conroy** *August 05, 2016 15:37*

I know what weight of the silcia gel fillid pvc pipe weighs as i weight it every feweeks and will recharge the silca gel ina microwave when the weight is over 1200 grams


---
**Bart Libert** *August 05, 2016 15:38*

So it will absorb +- 1200-935 = 265grams of water ? Would you have a picture of how clean 3mm mdf cut is ?




---
**Phillip Conroy** *August 05, 2016 15:52*

Of hand can not remmber how mch water 1/2 kg of silca gel will absorb [google it],goto my other posts to see pitures of my setup and pictures of cut mdf.or see my ebay listings [http://www.ebay.com.au/itm/182203016356?ssPageName=STRK:MESELX:IT&_trksid=p3984.m1555.l2649](http://www.ebay.com.au/itm/182203016356?ssPageName=STRK:MESELX:IT&_trksid=p3984.m1555.l2649) their is a slight mark on the mdf from where the smoke could not escape properly near the metal that remains from stock cutting bed tray


---
**Bart Libert** *August 05, 2016 16:10*

You make nice things. And I see you sell too on ebay. Nice job ! I had a business about 2 years ago selling personaliserd stuff online here in Belgium. Not anymore now... maybe again in future. Your mdf is cut quite nice. It could be made even nicer with pulsed cutting, but to be honest it is allready very nicely done ;)




---
**Pippins McGee** *August 05, 2016 23:47*

**+Bart Libert** what is pulsed cutting and what can be made nicer about the cut?


---
**Bart Libert** *August 06, 2016 08:17*

Pulsed cutting (sometimes called ppi) is a system where you don't let the laserbeam on constantly at a certain power setting. Instead you let the laser shoot x pulses of each 3 milliseconds for a certain distance. For example if I cut plywood at 800PPI then I let the laser shoot 800 times a 3ms shot nicely divided over 1 inch of distance. This allows you to enourmously tweak power usage so you get no burnt edges at all. Especially in corners. With the laser constant ON the steppers slow down in corners and by default you apply more heat there. Not with pulsed cutting.


---
**Phillip Conroy** *August 06, 2016 09:18*

No burned edges sound great,have you tryed this on mdf


---
**Phillip Conroy** *August 06, 2016 09:23*

When i first got laser cutter instead of testing properly ,idiot me brought 2000 sheets of 2mm thick cardboad [ one tom]to use in laser cutter,turns out that it left carbon covered edges and wife could not use as the carbon gor everywhere,luckly wife is now using it in scrapbooking busnies-just take 5 years to use it all up....


---
**Bart Libert** *August 06, 2016 10:57*

Mdf is dust and glue. The edges of mdf are always pure black. What I meant was typically plywood and the burned marks on top of your material. I can cut for example 4mm plywood with almost no color change on the sides 


---
**Pippins McGee** *August 06, 2016 22:05*

**+Bart Libert** 

I see! and how does use turn on PPI cutting as opposed to constant? I see no settings for this in corellaser


---
**Bart Libert** *August 07, 2016 07:30*

I dont think this is in corellaser at all. I use it on linuxcnc and am now busy rebuilding is on arduino hardware.




---
**Robi Akerley-McKee** *August 08, 2016 05:48*

It's PPM in inkscape layer name 10 [ feed=1200,ppm=6] is 10 power, feed 1200mm/minute with 6 pulse per mm



For Air Assist I use a vane air pump off a old select comfort air matteress.  I've engraved Granite (bad Robi! burned up a tube) cut 1/4" plywood 6 passes at ~ 20w.


---
*Imported from [Google+](https://plus.google.com/111055748369814573959/posts/DUV7mXF6nDq) &mdash; content and formatting may not be reliable*
