---
layout: post
title: "Hi guys, I just bought and set up a K40 laser cutter from eBay"
date: January 28, 2018 17:54
category: "Hardware and Laser settings"
author: "Chris Davis"
---
Hi guys, I just bought and set up a K40 laser cutter from eBay.  I'm sure I have it properly set up, the water is being circulated properly, and I've even added an air assist nozzle.  However, at full power and a speed of 40mm/s, it still takes two full passes to cut through 3mm plywood.  Is this normal?





**"Chris Davis"**

---
---
**joe mckee** *January 28, 2018 18:21*

Sounds reasonable enough ..



Cutting is varied with 



Product type 

Height focus 

Speed 

Alignment

Tube life 



Also look to get an even spread with alignment over the 300x210mm as it's annoying to have some bits cut and some not .



YouTube to find alignment and ramp tests .



Joe


---
**Ned Hill** *January 28, 2018 18:34*

40 mm/s is way too fast for cutting 3mm plywood with a K40. You need to be more around 10 mm/s. 


---
**Ned Hill** *January 28, 2018 18:40*

Also avoid running at full power as much as possible as it shortens the tube life.  The power out put of these machines isn’t linear and the data indicates that the power output starts flattening out after 50%. So it’s recommend to try keeping max to 50% or less as much as possible. 


---
**Chris Davis** *January 28, 2018 18:59*

Thanks guys, that's good info.


---
*Imported from [Google+](https://plus.google.com/113834784715800740788/posts/QoDb4aifqrR) &mdash; content and formatting may not be reliable*
