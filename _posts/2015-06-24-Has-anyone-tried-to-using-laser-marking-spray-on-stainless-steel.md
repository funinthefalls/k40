---
layout: post
title: "Has anyone tried to using laser marking spray on stainless steel?"
date: June 24, 2015 19:46
category: "Materials and settings"
author: "Jason Johnson"
---
Has anyone tried to using laser marking spray on stainless steel? if so, what product did you use for the marking spray? what speed and power did you use to accomplish this? Thanks!!!





**"Jason Johnson"**

---
---
**Michael Bridak (K6GTE)** *July 08, 2015 19:23*

I've used a cheaper alternative to marking spray before on plain old everyday steel. [https://plus.google.com/+MichaelBridak/posts/Yuf639okGo4](https://plus.google.com/+MichaelBridak/posts/Yuf639okGo4) 


---
*Imported from [Google+](https://plus.google.com/114673513688962471823/posts/fRwXFjXpFNL) &mdash; content and formatting may not be reliable*
