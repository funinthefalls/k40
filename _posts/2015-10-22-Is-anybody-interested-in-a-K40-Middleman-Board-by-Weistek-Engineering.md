---
layout: post
title: "Is anybody interested in a K40 Middleman Board (by Weistek Engineering)?"
date: October 22, 2015 20:30
category: "Modification"
author: "Sean Cherven"
---
Is anybody interested in a K40 Middleman Board (by Weistek Engineering)?

If so, I have 3 available right now.



Click Here:

[https://www.spaelectronics.com/shop/k40-middleman-board.html](https://www.spaelectronics.com/shop/k40-middleman-board.html)





**"Sean Cherven"**

---
---
**Stephane Buisson** *October 22, 2015 22:43*

sorry, this post was block by spam filter.

Happy to release it.


---
**Sean Cherven** *October 22, 2015 22:55*

Thanks. Just trying to help people out.


---
**Anthony Bolgar** *October 29, 2015 22:08*

I have 1 to get rid of. If anybody wants it just let me know and I will send it by mail for free.


---
**Mike Hull** *November 18, 2015 11:46*

I need one for our K40 at Blackpool Makerspace in UK, for which I am just accumulating the parts for a Ramps/Arduino conversion.

Are you UK based?


---
**Anthony Bolgar** *November 18, 2015 11:54*

I live in Canada.


---
**Mike Hull** *November 18, 2015 14:39*

Hi Anthony,

Please email me mikehull2008@hotmail.co.uk


---
**Sean Cherven** *December 02, 2015 15:26*

I still have 4 available for immediate shipping.


---
**Kirk Yarina** *December 27, 2015 21:42*

And now you only have 3 :)

Would have preferred that you  take PayPal, but ordered anyway.   Saves me having to make another adapter as part of my PoKeys/Auggie controller conversion project.


---
**Sean Cherven** *December 27, 2015 21:54*

Yeah, I dropped Paypal a long time ago. They caused me too many problems, trying to take my funds away from me, etc. I lost $800 due to them. I just started accepting BitCoins however, so that may help some people.  Anyways, I will get your order shipped out first thing in the morning!


---
**Sebastian Szafran** *February 09, 2016 20:23*

**+Sean Cherven** Do you still have the K40 middleman board available? I need one. Can you post to Poland?


---
**Sean Cherven** *February 09, 2016 21:21*

Yes I can, shipping to poland will be $15.25



Shoot me an email to info@spaelectronics.com with your shipping address, and I will send you an invoice. 


---
**Shannon Haworth** *March 24, 2016 04:01*

I could really use one of those if still available.  I ordered 3 from OSHPark, however they are going to arrive after my brother's visit is over.  The whole point of ordering the K40 was to give us a project to work on.  On the plus side, I'll be paying this forward when mine arrive.  Please shoot me a message at shannon.haworth at the mail system that google provides.  Thanks in advance.


---
**Sean Cherven** *March 24, 2016 04:04*

Yes I have a few more available. If you order tonight, I'll get it shipped out first thing in the morning.


---
**Shannon Haworth** *March 24, 2016 15:01*

Thanks Sean.


---
**Sean Cherven** *March 24, 2016 15:25*

No problem


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/PJfv5EnMy8T) &mdash; content and formatting may not be reliable*
