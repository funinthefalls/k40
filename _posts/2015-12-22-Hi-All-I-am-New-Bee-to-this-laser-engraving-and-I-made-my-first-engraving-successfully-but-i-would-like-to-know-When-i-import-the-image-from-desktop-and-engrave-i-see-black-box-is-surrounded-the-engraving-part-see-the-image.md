---
layout: post
title: "Hi All, I am New Bee to this laser engraving and I made my first engraving successfully but i would like to know When i import the image from desktop and engrave i see black box is surrounded the engraving part .see the image"
date: December 22, 2015 04:01
category: "Discussion"
author: "Raja Rajan"
---
Hi All,



I am New Bee to this laser engraving and I  made my first engraving successfully but i would like to know When i import the image from desktop and engrave i see black box is surrounded the engraving part .see the image ..I just need the anchor and letters should be in black..Any advise..plzz..Thanks in advance!



![images/ab7660f8a7d4e8d6287c597f1ddad5df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab7660f8a7d4e8d6287c597f1ddad5df.jpeg)
![images/770762e7125ba3d3b39d361a6f26e147.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/770762e7125ba3d3b39d361a6f26e147.jpeg)

**"Raja Rajan"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 22, 2015 04:19*

There's an option up the top of the engraving panel, near the dropdown box where it says Engraving that says "Sunken" & tickbox. You tick the sunken option & it should do the letters & anchor engraved down.



(or untick it if it is currently ticked, but I assume it is not).


---
**Raja Rajan** *December 22, 2015 12:49*

Thanks Yussuuf let me try:)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 24, 2015 08:18*

Any luck with getting the engraving the way you wanted **+Raja Rajan**?


---
*Imported from [Google+](https://plus.google.com/110717916181924497071/posts/PxkXG7ivmvD) &mdash; content and formatting may not be reliable*
