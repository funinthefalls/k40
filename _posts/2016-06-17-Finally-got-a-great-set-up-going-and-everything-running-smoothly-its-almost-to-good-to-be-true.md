---
layout: post
title: "Finally got a great set up going and everything running smoothly it's almost to good to be true"
date: June 17, 2016 02:30
category: "Discussion"
author: "3D Laser"
---
Finally got a great set up going and everything running smoothly it's almost to good to be true 

![images/ef6a82e6fa2be9bf593809904cda11e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ef6a82e6fa2be9bf593809904cda11e3.jpeg)



**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:54*

Wow, that looks a lot neater than my setup haha.


---
**Alex Krause** *June 17, 2016 07:13*

**+Yuusuf Sallahuddin**​ Corey is in production mode... has a product he has found a market for and from the looks of it focusing heavily on that one product... much easier when you are focused on production to stay organized than having 10 different project going all the time or just experimenting with different types of material


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 08:01*

**+Alex Krause** Too true. I have yet to find myself a marketable product. Too busy testing this & testing that for the last 9 months haha.


---
**3D Laser** *June 17, 2016 10:59*

Yea I make game trays for only two games at the moment i experiment to make the design better but for the most part stick to what I know I have been asked to branch out to other games and will soon but right now if it's not broke don't fix it know what I mean


---
**Anthony Bolgar** *June 17, 2016 14:28*

Glad you found a profitable use for your laser. Good job **+Corey Budwine** !!


---
**Bruce Garoutte** *June 17, 2016 20:55*

Great stuff you are doing Corey. As popular as these types of games are, I would think that you will do very well with your boards.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/UG2dakUv2ci) &mdash; content and formatting may not be reliable*
