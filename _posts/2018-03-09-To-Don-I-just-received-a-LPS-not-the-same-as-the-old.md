---
layout: post
title: "To Don, I just received a LPS not the same as the old"
date: March 09, 2018 16:24
category: "Discussion"
author: "bbowley2"
---
To Don,

I just received a LPS not the same as the old.

Photo shows new PS on top.  Is it possible to use this supply?  Is there a configuration that will work? 

Thanks,

Bryan



![images/9225469287bacddb7f136272cdd3e91a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9225469287bacddb7f136272cdd3e91a.jpeg)



**"bbowley2"**

---
---
**Don Kleinschnitz Jr.** *March 09, 2018 17:22*

Something like this perhaps:

What controller are you using?

You need a separate 24v and maybe a 5V supply as I don't know the 5V capacity of that supply.



Need to see the connector labels on the new supply.



[https://photos.app.goo.gl/SK8gUErPx6qVwS7F3](https://photos.app.goo.gl/SK8gUErPx6qVwS7F3)


---
**bbowley2** *March 09, 2018 17:33*

I'm using a C3D mini.  The K40 has digital power settings.  Will I need 

to add a pot for current regulation?



The old PS was 24v but I have a separate 24v supply I was going to use 

with an ESTY rotary.  But I shouldn't need it.



The old ps doesn't show a connection to Current Induction to Cathode of 

laser.


---
**Don Kleinschnitz Jr.** *March 09, 2018 18:31*

**+bbowley2** 



...you can use the digital panel or convert to a pot, now is a good time to decide if you want to keep the digital panel.

... you don't have to have a milli-amp meter but I recommend it because its a useful part of monitoring your lasers power. The digital panel itself only sets power but does not monitor it.

If you decide to use a pot there are instructions on my blog.

If you want to continue to use the digital panel I can create you a drawing for wiring it in.



[donsthings.blogspot.com - Understanding the K40 Digital Control Panel???](http://donsthings.blogspot.com/2018/02/understanding-k40-digital-control-panel.html)








---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/UEN2ajMYnfm) &mdash; content and formatting may not be reliable*
