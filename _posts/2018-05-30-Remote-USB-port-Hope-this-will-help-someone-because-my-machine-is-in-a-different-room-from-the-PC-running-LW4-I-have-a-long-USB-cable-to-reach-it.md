---
layout: post
title: "Remote USB port Hope this will help someone- because my machine is in a different room from the PC running LW4 I have a long USB cable to reach it"
date: May 30, 2018 16:04
category: "Modification"
author: "Steven Whitecoff"
---
Remote USB port

Hope this will help someone- because my machine is in a different room from the PC running LW4 I have  a long USB cable to reach it. A while back I had the misfortune to trip over the cable and damaged the USB port on the Cohesion board but the same could happen with the Nano or any board. Instead of replacing the USB port and risking a repeat, I purchased a USB port in a plug recepticle with 6" of wire lead. Then I soldered the leads to the Cohesion board directly and mounted the connector itself in the original access hole. Because it interfered with the Cohesion board I drilled 2 new holes in the controller mount bracket and moved the Cohesion over. This has the extra benefit of making access to the SD card MUCH easier and safer. Now if I trip over the cable I will replace the socket- and do the wire connection this time at the socket to prevent any more soldering on the Cohesion Mini.



![images/ca7ec00ab56e82a1547f969550692c63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca7ec00ab56e82a1547f969550692c63.jpeg)
![images/754632427076238ff467204a72dc8f0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/754632427076238ff467204a72dc8f0f.jpeg)

**"Steven Whitecoff"**

---


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/KLgLMtnsxNA) &mdash; content and formatting may not be reliable*
