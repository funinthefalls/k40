---
layout: post
title: "Has anyone ever replaced the X axis belt on a K40?"
date: May 01, 2016 02:55
category: "Hardware and Laser settings"
author: "ben crawford"
---
Has anyone ever replaced the X axis belt on a K40? I could use some tips because I am worried about alignment.





**"ben crawford"**

---
---
**Jim Hatch** *May 01, 2016 04:39*

Didn't replace it but I did take it all apart when I squared up the frame. It's pretty straightforward. What's your concern? 


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/2a1nfEaxDMu) &mdash; content and formatting may not be reliable*
