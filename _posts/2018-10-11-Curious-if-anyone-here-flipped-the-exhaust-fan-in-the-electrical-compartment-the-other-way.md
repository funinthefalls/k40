---
layout: post
title: "Curious if anyone here flipped the exhaust fan in the electrical compartment the other way"
date: October 11, 2018 20:01
category: "Modification"
author: "James Hensley"
---
Curious if anyone here flipped the exhaust fan in the electrical compartment the other way.  Mine was sucking air from inside the machine and blowing it out the back.  Since the cutting compartment is not really sealed off from the controller board and power supply, I figured it was dump to have two fans both fighting against eachother.  So I turned mine around tonight so it blows air into the machine and the squirrel cage pulls it out.



So far it seems to be doing a better job of keeping the smoke/smell out of the room.  Any thoughts or concerns from anyone?





**"James Hensley"**

---
---
**Joe Alexander** *October 11, 2018 20:06*

makes sense to me, granted that the intake isn't enough to push the smell out through the cracks before the squirrel cage can draw it out :)


---
**James Hensley** *October 11, 2018 20:26*

**+Joe Alexander** I was a little worried about that also, but the original vents on the right side of the case are still open...not to mention all the other small openings & cracks for the pressure to escape from.  I did notice a slight draft from the controller area into the cutting area through a big hole (lighting on another model?).  So that's a good thing I guess.


---
**HalfNormal** *October 11, 2018 20:36*

I did this with mine and it worked out very well. The biggest thing I was worried about was sucking debris from The Cutting area into the electronics area. This made sure that that did not happen.


---
**Chris Hurley** *October 11, 2018 21:35*

Done. Not much difference due to the power of the fan. 


---
**Joe Alexander** *October 11, 2018 22:42*

FYI that hole is so you can access the screws for the belt tensioner on the end of the gantry without removing the panel :)


---
*Imported from [Google+](https://plus.google.com/103868577646463138748/posts/Wmmhs3TYzyv) &mdash; content and formatting may not be reliable*
