---
layout: post
title: "I'm struggling to source an axis belt"
date: October 24, 2016 10:15
category: "Original software and hardware issues"
author: "ian humble"
---
I'm struggling to source an axis belt. Any advice will be appreciated 





**"ian humble"**

---
---
**Jim Hatch** *October 24, 2016 11:25*

LightObject has them.


---
**ian humble** *October 24, 2016 11:53*

Yeah saw that but it's like $21 to post it...


---
**Jim Hatch** *October 24, 2016 12:12*

Are you in the UK? If you post where you are someone local can update this with their local supplier for parts.


---
**Jim Hatch** *October 24, 2016 12:12*

Are you in the UK? If you post where you are someone local can update this with their local supplier for parts.


---
**Bill Keeter** *October 24, 2016 12:24*

I'm assuming they're just the standard GT2 belts also used on 3d printers, right? If so, you can just order it by the feet on Ebay.


---
**ian humble** *October 24, 2016 17:08*

Yeah in the UK...

I never assume. Is that what I need?


---
**Scott Marshall** *October 25, 2016 05:36*

It's NOT GT-2. The X axis is 2.3mm pitch and 4.67mm wide



Y axis is different (WHY??? Who knows???) and is 2mm pitch, but 9.48mm wide.



Wish I knew of someone over there that has it. Try Aliexpress 0-- China is only 7 -10 days from everywhere these days.



Scott


---
**ian humble** *October 30, 2016 14:40*

Weird. Well I should be in the us next month so I'll just get some there I reckon, it can wait. Thanks for you help guys


---
*Imported from [Google+](https://plus.google.com/114003426045732749773/posts/T1CTUAuhNiG) &mdash; content and formatting may not be reliable*
