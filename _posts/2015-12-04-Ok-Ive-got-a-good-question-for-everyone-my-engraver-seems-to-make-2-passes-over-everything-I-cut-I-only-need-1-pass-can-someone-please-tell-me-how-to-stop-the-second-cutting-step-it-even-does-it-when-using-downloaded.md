---
layout: post
title: "Ok I've got a good question for everyone, my engraver seems to make 2 passes over everything I cut, I only need 1 pass, can someone please tell me how to stop the second cutting step, it even does it when using downloaded"
date: December 04, 2015 23:35
category: "Discussion"
author: "Scott Thorne"
---
Ok I've got a good question for everyone, my engraver seems to make 2 passes over everything I cut, I only need 1 pass, can someone please tell me how to stop the second cutting step, it even does it when using downloaded cutting files.





**"Scott Thorne"**

---
---
**Gee Willikers** *December 04, 2015 23:45*

Your software may have multipass cutting enabled. Disabling it varies with the software. 



The other common problem that causes this is the art file containing duplicate shapes. This happens to me a lot using the Adobe Illustrator pathfinder tools and compound paths.


---
**Scott Thorne** *December 04, 2015 23:47*

Ok thanks, but how do I disable it in Corel draw...if possible.


---
**Gee Willikers** *December 04, 2015 23:58*

Well if its a problem with duplicate art in Corel and you can just erase the copies. I don't use Corel so the only way I know how to do it is to grab a shape and move it. If there is the same shape remaining after the move, delete the one you moved. Duplicate art is usually the problem people have.



 If it's the cutting software there should be an option in the layers menu or pop up or whatever your software provides you with. I use a software called lasercad and it's an option in where I set the power and speed for the layer.



Oh yeah if you have a k40 with some kind of digital display it may be an option in there. Mine was originally a straight analog machine so I'm really not familiar with that type of machine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 05, 2015 00:48*

Are you using solid black shapes for what you Cut? If you are using hairlines it will cut on either side of the line.


---
**Scott Thorne** *December 05, 2015 11:45*

**+Yuusuf Sallahuddin**​....no...outlines...maybe it's in the drawings that I'm trying to cut that I've downloaded of the internet.


---
**Scott Thorne** *December 05, 2015 11:46*

**+Jeff G**​....thanks man...I think it's just the drawings that I'm using.


---
**Coherent** *December 05, 2015 15:44*

I doubt it's a problem with the drawing. Objects that are cut need to be filled in with solid black. As Yuusuf stated, even if it's a hairline outline of a shape it will make two passes and cut on both sides of the line no matter how thin.  It also assists the software with cutting inside cuts (white islands within the solid black object) first... like the inside of a "A".


---
**Scott Thorne** *December 05, 2015 15:57*

Ok....got it....thanks you guys....I didn't know that....obviously...lol


---
**Gary McKinnon** *December 05, 2015 18:34*

**+Scott Thorne** I read that the line-width has to be set to 13 in LaserDraw otherwise all is drawn twice, i haven't confirmed this yet.


---
**Scott Thorne** *December 06, 2015 04:14*

**+Gary McKinnon**....I'll check that out tomorrow....thanks.


---
**I Laser** *December 06, 2015 10:21*

Yes it is quite possibly your line thickness. I had the same issues initially but changed the default line thickness to .01mm I believe (not in front of the machine at the moment.).



Just in case you didn't know to set the default just change the thickness without having selected any objects.



Contrary to what others say you do not need to black fill all your objects.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 13:07*

There is also an option for Unidir. I turned that off. Can't remember if when it is on or off, but one option means it goes left-right then right-left (both directions).


---
**Scott Thorne** *December 06, 2015 13:39*

**+I Laser**....**+Yuusuf Sallahuddin**.....thanks for the info...I'll check it out today and let you guys know.


---
**Ben Marshall** *June 16, 2016 15:10*

**+Scott Thorne** Did you ever get this sorted out? I thought that red lines = cut, and black=engrave? I'm having the same problem right now. Second pass sets my material on fire!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:40*

**+Ben Marshall** This is an issue with line thickness. Set your cut line to 0.01mm width. I've seen some different values (I think it was 0.05) but I don't remember what exactly. When set at 0.01 it only cuts once on each line. If the line thickness is more, it is in effect cutting a very thin rectangle instead of just a line.


---
**Ben Marshall** *June 17, 2016 04:41*

Even at "hairline" thickness" in coral draw?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:46*

**+Ben Marshall** Yeah even hairline thickness draws 2 lines. I thought that would be enough too originally. But it isn't. Another option is to have no border & fill the object with black fill colour. However, that only works on closed objects (not individual lines).



So, you have to type into the box 0.01 as it is not available from the list.


---
**Ben Marshall** *June 17, 2016 04:58*

**+Yuusuf Sallahuddin** Correct you are! Went to object properties and had a go. .0001 is the smallest, anything small yields no lines. Although, with lines that thin will required multiple passes. Good to know though for thinner and less dense materials. Thanks!


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/BkePsVyA7jM) &mdash; content and formatting may not be reliable*
