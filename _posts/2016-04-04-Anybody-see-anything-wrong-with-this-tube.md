---
layout: post
title: "Anybody see anything wrong with this tube"
date: April 04, 2016 13:17
category: "Discussion"
author: "george ellison"
---
Anybody see anything wrong with this tube. Took it out as it started to arc from the red cable connector to the case. Just need to be sure that i need to buy a new one before i go and spend £100 + Thanks



![images/d2d8c264e65297657452e8ce08f40a1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2d8c264e65297657452e8ce08f40a1c.jpeg)
![images/eb1055690750242407f704548019fe6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb1055690750242407f704548019fe6b.jpeg)
![images/8ef2fce6cc115aa9f9b631ff479155aa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ef2fce6cc115aa9f9b631ff479155aa.jpeg)
![images/32f0e7f7f0ff8297154349554fd552e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/32f0e7f7f0ff8297154349554fd552e5.jpeg)
![images/fe5d1884055ce0b3c70071fc9c1a2f3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe5d1884055ce0b3c70071fc9c1a2f3e.jpeg)
![images/7876d548482cecab5d94ac2806ba2072.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7876d548482cecab5d94ac2806ba2072.jpeg)
![images/b6a7a1777763fcb308113ce5cf6b5083.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b6a7a1777763fcb308113ce5cf6b5083.jpeg)
![images/651a6be59d4bf750306dd6c59cdeb061.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/651a6be59d4bf750306dd6c59cdeb061.jpeg)

**"george ellison"**

---
---
**Stephane Buisson** *April 04, 2016 20:05*

not sure from the picture, have you a little crack on the glass pipe just under the rubber pipe ? is that enough to have your gas leak ? (that could explain the power will not go through to the other end of the tube, and arcing)


---
**Phillip Conroy** *April 05, 2016 00:38*

does the laser fire at all ,if yes no crack.As for arcing turn the tube so that it is as far from metal as you cqan(remove laser tube lid and try,add more silicon 


---
**Heath Young** *April 05, 2016 02:34*

Does it glow at all or draw any current? If not, there is a chance its cracked and up to air. 18Kv jumps a long way, be careful!


---
**george ellison** *April 06, 2016 08:29*

I have not tried to run a job as that would be too scary. I press the test fire with low power and it sqweals, turn the power up slightly the red cable end arc's with the case and no beam is fired (no mark on material placed under cutting head either) I believe it must have overheated, cracked and lost the gases. Just want to make sure before buying a new one. I will look for the crack mentioned but i must also point out that there are silicone marks on the tube from when i attempted to seal it. Many thanks to all for your help!


---
**Heath Young** *April 07, 2016 01:32*

yeah if it doesn't glow and arc to the case - the tube is toast.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/TJGPD3eNQQb) &mdash; content and formatting may not be reliable*
