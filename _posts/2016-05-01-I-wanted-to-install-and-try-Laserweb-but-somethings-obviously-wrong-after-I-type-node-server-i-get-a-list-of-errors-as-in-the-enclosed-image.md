---
layout: post
title: "I wanted to install and try Laserweb, but something's obviously wrong, after I type \"node server\" i get a list of errors as in the enclosed image"
date: May 01, 2016 15:15
category: "Software"
author: "Mishko Mishko"
---
I wanted to install and try Laserweb, but something's obviously wrong, after I type "node server" i get a list of errors as in the enclosed image.

Can anybody help me with this? Thanks.



![images/cceda0d9ffbdb16d9ff3ee4270e0f2cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cceda0d9ffbdb16d9ff3ee4270e0f2cd.jpeg)
![images/5e0c5ba60c37c10160c564b424c28ec4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e0c5ba60c37c10160c564b424c28ec4.jpeg)

**"Mishko Mishko"**

---
---
**Alex Krause** *May 01, 2016 15:25*

From my understanding Laserweb is an online tool have you tried loading it from the supplied website and not trying to run a local instance of it


---
**Jim Hatch** *May 01, 2016 15:31*

Here are the instructions you need:

2. Type "cd\" (do not type the quotes) 3. Type "git clone [https://github.com/openhardwarecoza/LaserWeb.git](https://github.com/openhardwarecoza/LaserWeb.git)" (do not type the quotes) 4. Type "cd LaserWeb" (do not type the quotes) 5. Type "npm install" (do not type the quotes)



The problem comes from following Windows default locations (C:\users\...) for the install.



Install it to C:\laserweb



Then don't forget step 5 (npm install).



The user directory and its subfolders aren't in the path so the Node app can't find the files it needs.



That's what fixed it for me.


---
**Ariel Yahni (UniKpty)** *May 01, 2016 16:07*

**+Alex Krause**​ LW1 runs locally with a JS server. LW2 is hosted on [github.io](http://github.io)


---
**Jim Hatch** *May 01, 2016 16:10*

**+Peter van der Walt**​ the npm install was the step I forgot. Also in Win 10 the users directory is not part of the path to find programs/executables/resource files. You have to explicitly add it.


---
**Mishko Mishko** *May 01, 2016 16:27*

Thanks guys, obviously the part of the problem was the path, I've reinstalled it to C:\laserweb

I did not forget "npm install", but after that I've got a bunch of errors, and now I do get an interface in Chrome, but with an error, it says: "CRITICAL ERROR:

Laserweb may not work on this computer! 

Try another computer with WebGL support



Try the following:

In the Chrome address bar, type: chrome://flags [Enter]

Enable the Override software Rendering

Restart Chrome and try again

Sorry! :(



I tried that but to no avail.



I don't know how to add images to this post, if someone can help, I have the screenshots of both the CMD and Chrome.



Thanks for your suggestions.


---
**Ariel Yahni (UniKpty)** *May 01, 2016 16:31*

U can use [imgur.com](http://imgur.com) to post images.


---
**Ariel Yahni (UniKpty)** *May 01, 2016 16:33*

try this to test webgl [http://madebyevan.com/webgl-water/](http://madebyevan.com/webgl-water/)


---
**Ariel Yahni (UniKpty)** *May 01, 2016 16:35*

O correct. Forgot about that.


---
**Mishko Mishko** *May 01, 2016 16:42*

OK, here are the screenshots, hope this link is enough? 



[http://imgur.com/a/1SPV9](http://imgur.com/a/1SPV9)



I'm not at home at the moment, so I'm using the PC at my holiday house, not exactly antique, but not the latest hi-tech pearl either, so probably it lacks some features needed for LaserWeb to work...


---
**Mishko Mishko** *May 01, 2016 16:48*

I guess I'll have to try this again when I get back home, don't have any graphic card laying around.



So Peter, your recommendation is to use LW1 unless in case I use it with GRBL?



Thanks


---
**HalfNormal** *May 01, 2016 17:41*

I too had the WebGL issue on an old Dell. Had to add a newer graphics card and the error went away. I did a hardware check before and it stated that the machine supported WebGL2 but the browser did not like it. Updated drivers ect. Still did not work. IE would start without the error but then had other issues that I am sure was part of the WebGL problem. New Graphics Card = no more problems.


---
**Mishko Mishko** *May 01, 2016 17:44*

I did not make any significant mods to my K40 laser so far, but I'm considering different options. If I do decide to change the controller, I'm leaning against the smoothie board, so I wanted to check LaserWeb's interface and features, to get a better insight about what I can expect from this combination during the couple of days off I took. Guess it'll have to wait until I get back home;)

If I was at home the PC wouldn't be a problem, as I have more than enough at home, but the only one I have here is just something I'd thrown together to have a machine for emails etc., not for serious work. 

MB is HP 0A58H,  CPU Intel Core 2 Duo E6300, 4 gigs of DDR2 , the onboard graphics Intel Q965/Q963 Express. I've only installed WIN 10 a month or so ago, so the drivers are as "new" as I could find...

I'm also checking Chromium, but as far as I can see, there's only x86 version, and this machine runs x64 windows. I'm not exactly a programming guru, but I'll try to find some solution...


---
**Ariel Yahni (UniKpty)** *May 01, 2016 18:00*

**+Mishko Mishko**​ I have run x86 software on a x64 before. Try it. Also i don't belive you pc is that bad. What's the current workflow you use? Smoothie runs very good on LW1 and will sure run on LW2


---
**Mishko Mishko** *May 01, 2016 18:39*

Thanks Ariel, will try Chromium x86. I've just tried Chrome Canary, but that doesn't change anything.

This PC is not a pearl, but all the software I need runs just fine, Corel X7, Photoshop CS6, Inventor 2016, Vectric Aspire, Artcam and a bunch of others. I've never even heard about WebGL until today, maybe there's a solution for that one, too, don't know...

I use CorelLaser with Corel X7. At first it looked totally unusable to me, but once I got used to quite a number of stupidities and less than comprehensive interface, I'd say that, end of the day, it does the job. Of course, it leaves much to be desired, that's why I wanted to check other software options. 

I've worked on the Trumph laser for several years, using their older TOPS100 application, but I doubt any of the applications available on the net can compare with it, I'm not even sure that would make much sense for small machines like K14. It had a separate modules for drawing, nesting and technology, with material tables etc...


---
**Ariel Yahni (UniKpty)** *May 01, 2016 19:00*

+Mishko Mishko by the software you run​ it's weird that webgl it's not supported. Do you have it enabled in the browser? [http://superuser.com/questions/836832/how-can-i-enable-webgl-in-my-browser](http://superuser.com/questions/836832/how-can-i-enable-webgl-in-my-browser)


---
**Don Kleinschnitz Jr.** *May 01, 2016 19:21*

FYI I just had this same problem and this fixed mine as well (running Wind10) thanks for the help.




---
**Mishko Mishko** *May 01, 2016 19:35*

**+Peter van der Walt** The one I used, TOPS100, is pretty old and almost DOS-like, Trumph has much more sophisticated (and complicated) applications now, but the old one was easy to use, to the point I still use its drawing module for creating DXF files;) If you want to give it a run, contact me by mail.


---
**Mishko Mishko** *May 01, 2016 19:40*

**+Ariel Yahni** And all the applications run reasonably fast, maybe a tad slower than on my other machines, obviously WebGL is not required for any of them...

I've just downloaded chromium x86, but I have to find out how to install it first, windows installers are just about the extent of my expertise LOL

I've tried everything on that link, doesn't help I'm afraid.

Will report about Chromium if I manage to install it...


---
**Mishko Mishko** *May 01, 2016 19:42*

**+Don Kleinschnitz** Was your problem just a path then, not WebGL?


---
**Mishko Mishko** *May 01, 2016 19:48*

If this is how I need to install Chromium, I guess I'll wait till I get home to try LW on some other machine LOL

[https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md](https://chromium.googlesource.com/chromium/src/+/master/docs/windows_build_instructions.md)


---
**Mishko Mishko** *May 01, 2016 20:54*

OK, I've managed to install Chromium, but the problem stays the same, even after I've enabled two WebGL related options in chrome://flags


---
**Ariel Yahni (UniKpty)** *May 01, 2016 23:11*

**+Mishko Mishko**​ I'm still surprised. 


---
**HalfNormal** *May 02, 2016 20:08*

**+Mishko Mishko** **+Ariel Yahni** As I posted before, I chased this issue for 3 days and finally a new video card fixed the problem. If all the software fixes do not do the trick it is a combination software/hardware with the video card and drivers.


---
**Mishko Mishko** *May 02, 2016 20:36*

**+HalfNormal** I also think it's the card issue. Doesn't make sense to browse the net for a couple of days looking for the solution, as this is not a machine I will ever use to control the laser. And I don+t even have any card yet, so plenty of time to test the software. Thank you all anyway.


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/CSBWTTGX7rD) &mdash; content and formatting may not be reliable*
