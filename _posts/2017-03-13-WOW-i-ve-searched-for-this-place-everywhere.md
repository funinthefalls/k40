---
layout: post
title: "WOW, i ve searched for this place everywhere !!!"
date: March 13, 2017 19:29
category: "Discussion"
author: "Lucian Depold"
---
WOW, i ve searched for this place everywhere !!! going to buy a k40 !!





**"Lucian Depold"**

---
---
**E Caswell** *March 13, 2017 22:31*

Once you have the K40 you will be wondering why on earth you did.. No seriously they are a great machine built by monkeys, but be prepared for some more investment and plenty of time to research on here and have plenty of patients with it to build it right.

The one thing I would recommend first is the safety switch on the lid, once I had run mine and was happy with the set up and it was running right that was my first thing. So easy to forget and the next thing you have a few burns (serious ones as well)



There are all sorts of other upgrades but all depends on the budget.




---
**greg greene** *March 13, 2017 22:43*

Welcome to the madness ! :)  Give Ray at Cohesion 3D a call when your ready to upgrade the controller board.


---
**HalfNormal** *March 14, 2017 00:00*

We have always been here. Where have you been? ;-)


---
**Lucian Depold** *March 14, 2017 08:33*

@halfnormal I was trying to create a laser beam using matchsticks and a lense, did not work out, so: K40...




---
**Lucian Depold** *March 14, 2017 08:39*

**+PopsE Cas** a security switch on the lid will be first in line. do i also need to add something for the lid on the lasertube ??


---
**E Caswell** *March 14, 2017 17:09*

**+Lucian Depold** to be honest I have never bothered with the tube cover as that's only opened when I had to aligne the tube up, screwed it back down now and it's not one I have open very often. But can see the point. With the cover lid being opened and closed quite frequently it's a tendency to get a bit complacent and that lead to accidents. 



When aligning the mirrors and tubes it's a bit like crossing the road, always on the look out and on guard, after all wouldn't want to get hit by a number 10 bus..


---
*Imported from [Google+](https://plus.google.com/105895976959355796398/posts/SPSHuGz1HgN) &mdash; content and formatting may not be reliable*
