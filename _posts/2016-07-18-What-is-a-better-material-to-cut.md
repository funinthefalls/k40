---
layout: post
title: "What is a better material to cut?"
date: July 18, 2016 01:54
category: "Discussion"
author: "3D Laser"
---
What is a better material to cut? And right some comments below.





**"3D Laser"**

---
---
**Anthony Bolgar** *July 18, 2016 02:13*

MDF leaves a gummy mess when used a lot.


---
**Jim Hatch** *July 18, 2016 02:27*

Depends on the job. MDF is messy if a lot is cut. But Birch is more expensive. I don't do much with MDF except for stuff I make for sets for the high school's drama productions.


---
**Phillip Conroy** *July 18, 2016 09:56*

Mdf leaves a gummy mess-that is ehy i use a removable metal tray in laser bed ,and magnets with 65mm screws to hold work,i remove and wash it in hot water ,comes off easy


---
**Imko Beckhoven van** *July 18, 2016 11:52*

Mdf, yes it leaves a gummy mess (the epoxy Holding it together) but its a far more homogeneus material witch adds to consistent cut's. But probbebly its more looks vs price so its a "depends on what you Make" awser from me.


---
**crispin soFat!** *July 20, 2016 09:47*

MDF is gross! Avoid!




---
**I Laser** *July 24, 2016 02:54*

I prefer MDF, cuts more consistently than ply. 



Whilst both use adhesives, I've found the layered approach used in the manufacture of ply to be hit and miss at times when cutting. ie small sections won't cut completely through, whilst other areas could have cut through with less power.



Maybe it's the cheap Chinese ply we get here in Australia.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/brCYSbboaMp) &mdash; content and formatting may not be reliable*
