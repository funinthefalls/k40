---
layout: post
title: "Ok win is the best time to buy the k40 under 400 like I keep seeing that everyone is getting one for under 400 and all I see on eBay is over 500 plz help"
date: January 11, 2017 02:13
category: "Discussion"
author: "3 of a kind"
---
Ok win is the best time to buy the k40 under 400 like I keep seeing that everyone is getting one for under 400 and all I see on eBay is over 500 plz help





**"3 of a kind"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 02:24*

After the Chinese New Year prices should go back down. Wait till February. 


---
**Alex Krause** *January 11, 2017 03:12*

**+Ray Kholodovsky**​ when did you buy yours... It was about the cheapest I've seen them


---
**Ray Kholodovsky (Cohesion3D)** *January 11, 2017 03:17*

June. I paid 340. It was 320 for a bit after that. That's the cheapest I've seen. 


---
**Abe Fouhy** *January 11, 2017 05:45*

I got mine for 200


---
**Alex Krause** *January 11, 2017 05:46*

**+Abe Fouhy**​ how much for shipping?


---
**Abe Fouhy** *January 11, 2017 05:50*

I got it off offerup. Picked it up from a local vendor


---
**Paul de Groot** *January 11, 2017 06:07*

Can't find any k40s on the aussie ebay. A few weeks ago prices went up to 1600 mark. On aliexpress same deal. Shipping cost a bom to Australia. 


---
**Gregory Yates** *January 11, 2017 09:46*

Paul De Groot,

I just ordered from UK 580 to Brisbane Australia 


---
**Stephane Buisson** *January 11, 2017 10:15*

I am sorry to say, we (K40 comunity) have made this device popular.  ;-))

Hopefully China keep up with production.

and why not with a Smoothie or no board version, but that's still a dream.


---
**Paul de Groot** *January 11, 2017 19:57*

**+Gregory Yates** not too bad actually. Thx


---
**Gregory Yates** *January 11, 2017 20:19*

**+Paul de Groot**​, yestetday afternoon. There's a coupon for $20 off that you need to apply (only valid for a couple more days I think) . It still has to arrive, but it's the best price I've seen in a while. 



Look at this on eBay [ebay.co.uk - Details about  CO2 LASER ENGRAVER ENGRAVING MACHINE 40W HIGH PRECISE PRINTING CUTTER POPULAR](http://www.ebay.co.uk/itm/322336499298)


---
**Coherent** *January 11, 2017 23:38*

Like Ray said, wait til after the Chinese New Year. (I think it on Jan 17th this year) It's the biggest annual holiday in China. A lot of business shut down and not just for one day..before and after and lasts up to 23 days, . Also it seems when a new shipment is due in or they have a lot of units on hand, they lower prices to move remaining stock. Most of the US sellers are out of California and they all seem to have the same models, get them on the same cargo ship shipment and sell them for about the same price. 


---
*Imported from [Google+](https://plus.google.com/105856024739491940616/posts/4rZZR3qA9UE) &mdash; content and formatting may not be reliable*
