---
layout: post
title: "Spending my day doing edge lit signs"
date: February 27, 2016 15:02
category: "Material suppliers"
author: "Cam Mayor"
---
Spending my day doing edge lit signs. I found some cheap stands on ebay and they work pretty good with 3mm acrylic.





**"Cam Mayor"**

---
---
**Cam Mayor** *February 27, 2016 15:12*

This is the link to the base. They are about $5-6 US and take 3 AAA batteries. They are advertised for holding menus but the shipped acrylic pulls right out so you can put your own in. Several sellers are selling the same base so you can look around for the fastest shipper or cheapest base. The acrylic inserted in the base is 115 mm wide.

[http://r.ebay.com/NUM2q1](http://r.ebay.com/NUM2q1)


---
**The Technology Channel** *February 28, 2016 17:01*

That's the nuts... nice work...


---
**Purple Orange** *May 31, 2016 16:51*

I can't get the url to work 


---
*Imported from [Google+](https://plus.google.com/110883213653290462996/posts/ibPr7h5uDWX) &mdash; content and formatting may not be reliable*
