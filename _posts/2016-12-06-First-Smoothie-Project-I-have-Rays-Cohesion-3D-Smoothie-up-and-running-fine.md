---
layout: post
title: "First Smoothie Project I have Ray's Cohesion 3D Smoothie up and running fine"
date: December 06, 2016 21:19
category: "Discussion"
author: "greg greene"
---
First Smoothie Project



I have Ray's Cohesion 3D Smoothie up and running fine. I did this project using Inkscape as the drawing editor and LasedWeb 3 as the machine controller.  3mm Plywood box for jewellery, Pouporri  box or whatever you like. Had some problems related either to my lack of experience with  the board or the software that were compounded by a flaky Win 8 USB connection .  Upgraded to Win 10 and though there are still some mysterious disconnections at the end of a job for no apparent reason - there is nothing I cannot work with.  Ray's board is rock solid and Peter and the rest of the folks have given us a wonderful tool in LaserWeb.  I still have a lot to learn but figure if I can learn 3 things a day, one of which is something I forgot from yesterday - I'll get there. If your wondering if a smoothie board and LaserWeb are the way to go - wonder no more - there are at least three flavors of Smoothie out there to chose from - each with their own advantages - price is minimal - no more dongles - no more Corel dying - and the ability to control your beam strength via grey scale - what more could you ask for?



Thanks to all those who lent a hand - and if anyone wants help - just ask.



![images/74a068f5b7275eac2f783444b3df5d08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74a068f5b7275eac2f783444b3df5d08.jpeg)
![images/b09157cc043c605f88c2b60357cbf580.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b09157cc043c605f88c2b60357cbf580.jpeg)

**"greg greene"**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 06, 2016 21:41*

Glad to see you're up and running. 


---
**greg greene** *December 06, 2016 21:47*

Thanks - you folks were a big part of making it happen


---
**Ashley M. Kirchner [Norym]** *December 06, 2016 22:03*

Alcohol "tampons". I'm just gonna leave that here.


---
**Roger Unwin** *December 06, 2016 23:06*

What SW did you use to generate the box sides with finger joints?


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:16*

**+greg greene** Very Nice lion piece but not to be rude or anything I have a very extreme <b>"Obsession with African Lions"</b> and defending them and doing whatever it takes to keep them alive. To the degree that I would go as far as suing the USDA for their zoological park inspection standards.But other than that Good Job, I'm just currently awaiting on some end stops to come in from Amazon.com for my machine.


---
**greg greene** *December 06, 2016 23:23*

This lion is going to a good cause - As a member of our Lions club - we have a silent auction every year to raise money for our projects - so hopefully someone as enthused about lions and what we do will bid it high!!!


---
**greg greene** *December 06, 2016 23:24*

For the finger joint boxes there a number of free to use programs on the net - I used Makercase


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:25*

**+greg greene** Indeed and maybe it will sell, minus the fact that I have been wanting to get full copyright ownership of the African Lion Species. Crazy I now but that goes to show how extreme my obsession is with the animal.


---
**Jonathan Davis (Leo Lion)** *December 06, 2016 23:26*

**+greg greene** alrighty and that program i will look into.


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/9Y3q5YaBsGo) &mdash; content and formatting may not be reliable*
