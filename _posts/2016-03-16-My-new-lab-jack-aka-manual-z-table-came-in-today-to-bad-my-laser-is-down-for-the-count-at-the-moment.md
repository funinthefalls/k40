---
layout: post
title: "My new lab jack aka manual z table came in today to bad my laser is down for the count at the moment"
date: March 16, 2016 23:11
category: "Discussion"
author: "3D Laser"
---
My new lab jack aka manual z table came in today to bad my laser is down for the count at the moment 

![images/f7b267b823ee765cd806275e369e41c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7b267b823ee765cd806275e369e41c0.jpeg)



**"3D Laser"**

---
---
**3D Laser** *March 16, 2016 23:40*

**+Nathan Walkner** my plan is more of a hand crank as small as the k40 is motorized is not needed in my opinion but that more to do with the fact I have no idea how to do it


---
**Casey Cowart** *March 16, 2016 23:45*

Hey **+Corey Budwine**​, did you find a good source to purchase it from?  I like the idea... Does it seem stable enough?


---
**3D Laser** *March 16, 2016 23:54*

**+Casey Cowart** I got it for 48 dollars at [homesciencetools.com](http://homesciencetools.com) I got the 8 inch model as it was the biggest and cheaper than any I could find on eBay once I get it up and running I will give an update 


---
**3D Laser** *March 17, 2016 00:20*

So this might be a fail since my honey comb bed is so thick I get like a half an once of adj space otherwise it's a great idea I might need to get a smaller one so I can get more ask space 


---
**ThantiK** *March 17, 2016 00:26*

My question is, how the heck are you going to reach the hand crank? 


---
**HP Persson** *March 17, 2016 07:41*

Can´t you just reduce the height of it, removing halft height of the scissor parts so it´s only 1 level.

Looks like a easy mod, as the shafts is proably easy to remove and the pattern in the bed is the same as in the lift mechanism.

Sorry for bad english, hope you understand my idea here ;)


---
**I Laser** *March 18, 2016 09:53*

Would have thought so, looks like a couple of split pins holding the center.



Still would like to know how Corey is going to hand crank this thing. Probably will need to remove the honey comb,  use a guide to crank it to height and then replace the bed.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/hM4oJx3KU5S) &mdash; content and formatting may not be reliable*
