---
layout: post
title: "Question for anyone that knows: Can I take this image directly into CorelDraw & straight output to the laser cutter/engraver?"
date: November 06, 2015 07:28
category: "Software"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Question for anyone that knows: Can I take this image directly into CorelDraw & straight output to the laser cutter/engraver? Or do I need to convert to grayscale? Or even do I have to basically redraw as outlines?



Thanks in advance :)

![images/c5473f01ff71432024f84a339a139c2e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5473f01ff71432024f84a339a139c2e.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Sean Cherven** *November 06, 2015 13:28*

I'd like to know as well.


---
**Nathaniel Swartz** *November 06, 2015 13:46*

I know you can take it directly into moshi, so I assume you should be able to in coreldraw.  You might want to take in into a non-vector image editor and turn up the contrast a little bit then gray scale it.  I can go try it on some cardboard later and post pics if you like.


---
**Coherent** *November 06, 2015 13:53*

Change it to a grayscale/black and white. Adjust the contrast as needed. For an interesting effect, (I use photoshop) change it to a bitmap with about 60% dithering. Then reverse it and mirror it... spray one side of a acrylic sheet with black paint. Engrave it through the black paint. Clean it up a bit to remove dust from engraving and then paint over the black and engraving with white paint. It makes a very nice high contrast black and white "photo" suitable for mounting!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2015 14:54*

**+Nathaniel Swartz** Thanks, I was considering doing the same thing, just thought rather than waste my time testing something that I am sure somebody else has already tested I would ask.

**+Marc G** Thanks for those interesting tips Marc. Sounds like a pretty cool idea. Currently I don't work with acrylic sheets or any other materials than leather (although I have tested on cardboard at times too). I will keep that in mind for some interesting effects.


---
**Coherent** *November 06, 2015 18:05*

Here's a link to an "instructable" on the process I mentioned.

[http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/](http://www.instructables.com/id/Enhance-Your-Image-when-Reverse-Laser-Engraving-on/)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 05:20*

**+Marc G** Thanks for that Marc. I can probably use a similar method for what I want to do on leather (minus the paint).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 07:09*

**+Marc G** Thanks again for that Marc. Just ran some tests using that method now & it's looking great. Not quite finished though.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/GgK6xkTZgnd) &mdash; content and formatting may not be reliable*
