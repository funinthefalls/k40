---
layout: post
title: "Anyone here interested in helping to improve the k40_whisperer software?"
date: June 10, 2018 10:27
category: "Discussion"
author: "Eric Lovejoy"
---
Anyone here interested in helping to improve the k40_whisperer software?

I am, though python is not my strong suit. I know a guy who is willing to try to address efficiency in the laser print/ cut times, but he needs the ecoords data... Im not sure if it includes Red, Blue, Black, or if the colour elements are isolated. in each separate function. Any insight into that would be appreciated. Even if its just how to export that data to a file. 



Anyhow, ... Im also working on adding a CLI, and a web interface (via the CLI)

(Command Line Interface)









**"Eric Lovejoy"**

---
---
**Duncan Caine** *June 17, 2018 11:42*

you could ask the author! scorch@scorchworks.com


---
**Eric Lovejoy** *June 17, 2018 11:58*

**+Duncan Caine** Oh I have




---
**Timothy Rothman** *June 30, 2018 16:03*

Definitely contact Scorch, he's been improving his software over time and if the improvements are useful for most people he may integrate it.  I haven't gone back to CorelLaser since installing the drivers to make Whisperer work.  I have a gerbil PWM controller now and it's mounted but not plugged in.  I really want Whisperer to work  with it.  


---
*Imported from [Google+](https://plus.google.com/107285166924069729045/posts/X8mxCuZ7Mv6) &mdash; content and formatting may not be reliable*
