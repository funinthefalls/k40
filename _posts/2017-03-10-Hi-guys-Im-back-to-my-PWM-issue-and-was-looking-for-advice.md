---
layout: post
title: "Hi guys, I'm back to my PWM issue and was looking for advice"
date: March 10, 2017 01:44
category: "Smoothieboard Modification"
author: "Chuck Comito"
---
Hi guys, I'm back to my PWM issue and was looking for advice. I finally brought an oscope home from work to see what my waveforms looked like and like I thought, no matter what my settings in the software I get a full PWM signal to fire my power supply. Right now I have the PWM out going to the L on the LPS.  If I set my power in LW3/4 to 1% or 50% I get 100% output to the laser. Per my understanding, LW3 will vary the power based on the percentage I set and where I set the potentiometer to set a ceiling. Maybe looking at the scope isn't the answer as I'm not exactly an expert with these things. Would love to know your thoughts or any advice. I'm currently running an MKS SBase board with Smoothie Firmware.

Thanks,

Chuck﻿





**"Chuck Comito"**

---
---
**Ariel Yahni (UniKpty)** *March 10, 2017 01:50*

On my azsmz I connect L from LPS to negative pole on the bed MOSFET, and that's it. 


---
**Chuck Comito** *March 10, 2017 01:52*

I have it in pin 2.6 which is also off a mosfet. I can try the negative pin off the bed and see if that maybe produces a better (variable) signal.




---
**Ariel Yahni (UniKpty)** *March 10, 2017 01:53*

I think the trick is the negative pole


---
**Chuck Comito** *March 10, 2017 02:03*

My assumptions are this. Define pin X.X in the smoothie config. Connect said pin to the L input on the LPS. The software would vary the PWM signal hence varying the power supplied to the tube. The pot is in place as a "ceiling" and the software would vary (depending on my settings) between zero and the pot "ceiling". Am I correct so far?


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 02:44*

2.6 isn't a hardware pwm pin. 

2.4 or 2.5 is. 


---
**Chuck Comito** *March 10, 2017 02:52*

I've tried them all ray. Lol. I don't know how I got my config file to where it's at but I think i need someone to look at it or maybe just start from scratch. I'm starting to wonder if I've simply worked myself into a corner and didn't realize it. In my config file I have:

Laser module enable true

Laser module pin 2.5

Laser module max power .8

Min is default is .3

Pwm period 60

Then I have this syntax that I can remember where I got it from:

Switch laser fire true

Switch laser output pin 2.5^

Switch laserfire output type digital

Switch laser input on command m3

Switch laser fire input off command m5



Does this look about right to you guys? I know I didn't type it exactly. Left out all the underscores and separating periods. Thoughts?

Thanks. 


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 02:54*

Disable switch laser fire - set false. 


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 02:54*

Also pwm period can need to be anywhere from 200 - 400 for there to be noticeable laser grayscale but you should see pwm output on a scope anyways. 


---
**Anthony Bolgar** *March 10, 2017 02:55*

Hey **+Ray Kholodovsky**, have you come up with a best practice for connecting the mini to a K40 with the digital POT?


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 02:56*

Yes, exactly the same as every other way. 


---
**Ariel Yahni (UniKpty)** *March 10, 2017 02:56*

does not look good to me. I would start from the default and got from there. Taking in consideration what Ray said above




---
**Chuck Comito** *March 10, 2017 03:05*

So switch fire laser false

Change pwm to 200 to 400 

And everything else should be fine the way I have it?


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 03:06*

I'd second Ariel to grab the stock config file and enable the laser module with 2.5 and the pwm period 200. Done. 


---
**Chuck Comito** *March 10, 2017 03:09*

Ok. Will do. I've stared at this file for so long I can't remember what anything does. All starts to blur together after a while. I think I had small successes with each change and just kept going. I should post my file as an example of what not to do for other new comers. It's a commented out mess!!


---
**Alex Krause** *March 10, 2017 04:10*

The most common problem I have seen with people's config file is forgetting to remove a # which comments out that line in the config file making it have no effect 


---
**frank hernandez** *March 10, 2017 04:11*

Hi Chuck,

I too am running a MKS ..but just received one of Rays Mini boards. My set-up is going to make some people cringe but here it goes.

I use pin 1.23 going to L pin through a level shifter.

Config is as follows:

laser_module_pwm_pin                         1.23^!   

I still have pot wired in.

I can honestly say I have no issues with PWM. It has been this way for probably 3-4 months with no issues.


---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 04:14*

If you've replaced the pot and are running the output of that to IN... I have nothing against it to say. 




---
**Alex Krause** *March 10, 2017 04:31*

Nice engrave **+frank hernandez**​ :P


---
**Alex Krause** *March 10, 2017 04:32*

Hit that with some 500 grit sand paper lightly... And the whites will pop


---
**frank hernandez** *March 10, 2017 04:36*

Thanks for the tip Alex. I just play around basically stuff for grand kids etc. Nothing up to your level. Still learning just a hobby.


---
**Chuck Comito** *March 10, 2017 12:17*

I can go "rays dreaded" level shifter route and try pin 1.23 tonight 😁. Last night I tried 2.5 but I couldn't get it to fire. I turned the pot all the way up and I got a very low laser fire. 


---
**Don Kleinschnitz Jr.** *March 10, 2017 16:02*

If you have a pulse stream going to the L pin as seen on the scope moving to a level shifter is not going to help. If you have a pulse on L something else is wrong. 

Can you post a picture of what you see on the scope on L. BTW the fact that you can see an image when the pot is turned up suggest PWM is working and something else is overriding it.



Some things to consider is voltage levels and the polarity of the PWM signal.



If you can post config file, wiring of both ends of the PWM wire and scope pictures we can make this work. 



One more time .... level shifters may work but they are a poor design choice (for many reasons explained on my blog) and they are just more parts that degrade not gain control of your LPS. 



Many many K40 machines are running single wire open drain configs successfully.

Cheers


---
**Chuck Comito** *March 10, 2017 18:40*

Hi Don, I appreciate your response and I'll post the config file when I get home tonight but I did want to mention that the signal I was looking at did not look like PWM. It was on or off. I did not see any type of square wave. I should also mention that I was using an old analog scope. I have a pretty nice digital one I'll be working with tonight. Hopefully I'll be able to post some good images. Until then I'll stay away from the level shifter. 

One thing I noticed in my config is that it appears that I have 2 functions assigned to the pin I'm using. In the laser section I have pin 2.6 defined. Further down in my config file I saw that switch.fan.output_pin is also 2.6. Perhaps this is my problem?


---
**Chuck Comito** *March 10, 2017 22:59*

Ok, here are some scope images. Remember that I'm not exactly an expert with these things but.. images at 1%, 50% and 100% power. Looks like a good signal from pin 2.4. I'll post my config file next. 

![images/ebc8f2a379516d0c218cc8044d86a0f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ebc8f2a379516d0c218cc8044d86a0f5.jpeg)


---
**Chuck Comito** *March 10, 2017 22:59*

![images/e1282da709c3829457492e1bb7d3124c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1282da709c3829457492e1bb7d3124c.jpeg)


---
**Chuck Comito** *March 10, 2017 22:59*

![images/9ac1c3bbc5107b468135a17ddbcf7539.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9ac1c3bbc5107b468135a17ddbcf7539.jpeg)


---
**Chuck Comito** *March 10, 2017 23:03*

[dropbox.com - config.txt](https://www.dropbox.com/s/zblcvrmpvwuj9ny/config.txt?dl=0)


---
**Chuck Comito** *March 11, 2017 01:39*

This is my wiring. I don't think I added the endstops. All the images are from the web.

[dropbox.com - Laser Wiring.pdf](https://www.dropbox.com/s/6tsj0tyvznyte20/Laser%20Wiring.pdf?dl=0)


---
**Chuck Comito** *March 11, 2017 02:15*

Update: changed the pot and have variable Pwm signals. I noticed that a percentage lower than 30ish won't leave a mark during testing. Are there parameters I should stay between when engraving in laserdraw?


---
**Don Kleinschnitz Jr.** *March 11, 2017 11:51*

**+Chuck Comito** these scope signal look good to me. 

Does your update say that now it is working? 

Are the scope traces before or after the pot change?

The pot was bad?

In the middle of this did you fix the duplicate 2.4 pin assignment I see in your config file? 

<s>-----------------</s>

The power out will be a combination of pot and pwm settings. So that if you set the PWM at 10% and the pot 30% the power will be 3%. 

If your max current was 25 ma then the actual current might be .75 ma which will not likely fire the laser or make a mark.



What do you mean engraving in Laserdraw do you mean Laserweb :)


---
**Chuck Comito** *March 11, 2017 14:02*

Hi  Don, my update says that the PWM is working. The pot was bad for sure. I've replaced it with a 10 turn, 10k pot (all I had). I did not fix the duplicate 2.4 pin as I didn't see it but will do so now. That said, with the pot all the way up, laserweb will not output anything less than 30%. With the pop all the way up the power should be somewhat relative to the software settings (I think?). And yes, I meant LaserWeb.


---
**Chuck Comito** *March 11, 2017 14:05*

I have laser_module_pin set to 2.4 and I have switch.laserfire.output_pin set to 2.4^. These two lines confuse me. Which do I need or should be using?


---
**Don Kleinschnitz Jr.** *March 11, 2017 14:27*

You only need the laser_module_pin set to 2.4. Don't assign 2.4 anywhere else.




---
**Chuck Comito** *March 11, 2017 18:19*

Almost ready to run some tests. I'll report back. Thanks Don!


---
*Imported from [Google+](https://plus.google.com/117314700387764776689/posts/JJakUdzaRLg) &mdash; content and formatting may not be reliable*
