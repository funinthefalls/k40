---
layout: post
title: "I just installed the light object air assist and have a custom nail bed"
date: March 18, 2017 19:58
category: "Air Assist"
author: "craisondigital"
---
I just installed the light object air assist and have a custom nail bed. Here is the lens I bought..



[https://www.amazon.com/gp/product/B01DP2HMPK/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01DP2HMPK/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)



Is this a good lens/FL??



Before the air assist and new lens all worked well.



When I cut now, the lines are a lot thicker and laser is much less power. I think I need to build a lower nail bed. Does anyone know the correct height the material should sit. I cut 1/8" birch.







**"craisondigital"**

---
---
**Ned Hill** *March 18, 2017 20:16*

Check and make sure you installed the lens with the convex (bump) side up.    Theoretically the focus of your lens is 50.8mm so the focus point should be be anywhere from  the surface of the wood to half way though. I suggest you do a ramp test to establish the exact focus point of your system.


---
**Don Kleinschnitz Jr.** *March 18, 2017 20:27*

Definitely a ramp test, you never know what they sent you. Which lens did you buy and which did you have?


---
**craisondigital** *March 18, 2017 20:43*

Thanks! I did the ramp test and found the correct height! I don't understand though. The lens sits about 70mm from the material now and seems to be cutting and etching fine. The old lens sat about 50, and I thought the new lens (link above) had same focal length?



![images/4658ed1ddeba7904778573ee76b6aae2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4658ed1ddeba7904778573ee76b6aae2.jpeg)


---
**Ned Hill** *March 18, 2017 20:53*

If that's a Light Object head the lens is setting about half way into the knurled section.  So judging by the pic your focus is about 77mm (3in) which means they sent you the wrong lens.




---
**craisondigital** *March 18, 2017 21:05*

That's what I was thinking. Thanks.


---
**Don Kleinschnitz Jr.** *March 18, 2017 21:11*

That order page had multiple choices. What focal length did you check for ordering (should be in your order list). Looks like you got a 63.5mm 3 " lens. 

BTW these lenses are supposed to be really good for cutting because they have a long DOF. 

The spot size is typically .007 with a DOF of 2.3mm. 

Whereas a 50.8mm lens has a spot size of .005 mm with a 1.4mm DOF.

The sides of materials cut with the 3" lens is supposed to be pretty square vs hr glass shaped.

Be interested in if you notice the above to be true.


---
**Ned Hill** *March 18, 2017 21:19*

**+Don Kleinschnitz** that would be interesting to know.  (FYI 63.5mm would be 2.5" ;)


---
**craisondigital** *March 18, 2017 21:20*

Thanks for the info! I'm debating building a new nail bed or buying 50.8 lens. Kinda wanna try this new one with the info u provided. Maybe I'll make a temporary stand to test it. I'll let you know


---
**Chris Hurley** *March 19, 2017 23:56*

Please do as I want to cut deeper items too. 


---
*Imported from [Google+](https://plus.google.com/113025010074387144690/posts/ZczxUJdu21F) &mdash; content and formatting may not be reliable*
