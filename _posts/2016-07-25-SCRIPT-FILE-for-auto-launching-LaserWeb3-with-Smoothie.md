---
layout: post
title: "SCRIPT FILE for auto launching LaserWeb3 with Smoothie"
date: July 25, 2016 19:22
category: "Smoothieboard Modification"
author: "David Cook"
---
SCRIPT FILE for auto launching LaserWeb3 with Smoothie.



1). create a new file in notepad.

2). paste the contents below into it. Change the path according to what you        have.  (this also assumes Chrome is the default browser)

3). Save file as LW3.CMD and select "all files" as type.



 Now I just double click the icon on my desktop and it fires up the Node server and launches chrome and goes to the localhost webpage and LaserWeb3 is ready for action.

**+Peter van der Walt**

**+Custom Creations**



EDIT: removed git pull  from the script. Peter has advised that it was not a good idea to automate the git pull.



copy the contents below into the text file:



CD\

CD C:\Users\dcook\LaserWeb3

start node server-smoothie.js

start chrome [http://localhost:8000/](http://localhost:8000/)









**"David Cook"**

---
---
**Derek Schuetz** *July 25, 2016 19:35*

nvm simple fix


---
**David Cook** *July 25, 2016 19:37*

took me a minute to get it working until I realized that chrome is my default browser so I was able to call it directly


---
**Derek Schuetz** *July 25, 2016 19:37*

Thank you for this


---
**Custom Creations** *July 26, 2016 14:20*

Simple version worked for me



cd \laserweb3

git pull

start node server-smoothie.js

start chrome [http://localhost:8000/](http://localhost:8000/)



Save as LW3.CMD (Or whatever you want to call it) and select All Types (<b>.</b>)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/GUb7EGKBWUu) &mdash; content and formatting may not be reliable*
