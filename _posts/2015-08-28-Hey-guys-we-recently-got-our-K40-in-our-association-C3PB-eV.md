---
layout: post
title: "Hey guys, we recently got our K40 in our association ( C3PB e.V"
date: August 28, 2015 14:56
category: "Hardware and Laser settings"
author: "Andreas Horn"
---
Hey guys,



we recently got our K40 in our association (**+C3PB e.V.**) and have a very basically question.



We have the "analog" model with the analog amperemeter and we are not sure what's the normal operation current for the 40W laser tube - as normal I would declare, what's 100% for the digital model.



The very "interestingly" translated user manual says max. 6mA, the specs sheet says about max 20mA. On the internet I found various numbers between 4mA to 26mA.



So what are your experiences? Till now we didn't dare to use more than 6mA and the cutting of 4mm plywood was quite okay at 6mm/s. So would even more be possible with higher current? Or maybe someone with the digital model and the proper tools can measure which current it delivers to the tube at 100%?!



Thanks a lot in advice! :-)





**"Andreas Horn"**

---
---
**Joey Fitzpatrick** *August 28, 2015 15:27*

The recommended max current for the laser is 18ma (on a K40 Laser)  The tube will operate at higher currents but the tube life will be shortened dramatically.  Also I believe the tube will not fire at currents under 4ma.  Your K40's  min current may be different.


---
**William Steele** *August 29, 2015 00:48*

Agreed, 18ma max for the standard 40 watt tube.


---
**Andreas Horn** *August 29, 2015 11:32*

Nice, thank you for this information!


---
**Andreas Horn** *August 29, 2015 13:05*

**+Carl Duncan** nice idea, till now we marked several points around the potentiometer with a pencil to get reproducible cuts.



Also we tried 10mA yesterday and for our use cases (cutting 4-6mm plywood and 5mm acrylic) it worked much better. We basically could cut this things with one cut at the right speeds :-)


---
*Imported from [Google+](https://plus.google.com/109215046466232858854/posts/X9nasbvkuK2) &mdash; content and formatting may not be reliable*
