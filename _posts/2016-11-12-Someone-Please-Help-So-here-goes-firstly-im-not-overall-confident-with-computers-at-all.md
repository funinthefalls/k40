---
layout: post
title: "Someone Please Help! So here goes. firstly im not overall confident with computers at all"
date: November 12, 2016 15:00
category: "Discussion"
author: "Charlie Thcakeray"
---
Someone Please Help!



So here goes. 



firstly im not overall confident with computers at all. Ive bought a K40 laser cutter (from ebay of course). And dont ask how but ive lost the USB key. Also the DVD that contains CorelLaser and Coreldraw will not download onto my computer (its a reall old vista laptop). do you guys have any advice? is it hard to convert to a smoothie control board, and what software can it handle? is there any other software that i can use instead of Coreldraw? Please let me know, Cheers Charlie.





**"Charlie Thcakeray"**

---
---
**Anthony Bolgar** *November 12, 2016 15:16*

The upgrade is not overly complicated, however you do require a few basic skills to install the smoothieboard. There are plenty of us that would be willing to help you with the process.


---
**Charlie Thcakeray** *November 12, 2016 15:22*

**+Anthony Bolgar** is there a specific smoothieboard that ill need? and is there any free software to use with smoothie?


---
**Anthony Bolgar** *November 12, 2016 15:42*

The best board to use is the 4x, and LaserWeb is great free software that works very well with Smoothie. You can check out the LaserWeb community at [https://plus.google.com/u/0/communities/115879488566665599508](https://plus.google.com/u/0/communities/115879488566665599508)


---
**Charlie Thcakeray** *November 12, 2016 15:48*

**+Anthony Bolgar** is the 4x hard to install or is it a case of just pluging in the wires?


---
**Ariel Yahni (UniKpty)** *November 12, 2016 16:06*

**+Charlie Thcakeray**​ buying the board alone if you are not technically inclined could be heartache. If you need a plug and play solution talk to **+Scott Marshall**​ or **+Ray Kholodovsky**​ as they have a solution for you


---
**Charlie Thcakeray** *November 12, 2016 16:11*

**+Ariel Yahni** i am a qualified control and intrumentation technician i do have a basic understanding. its just the software side of things im not too sure about.


---
**Ariel Yahni (UniKpty)** *November 12, 2016 16:16*

**+Charlie Thcakeray**​ then it should b easy for you. 2 cables for laser power and a couple others for motors


---
**Anthony Bolgar** *November 12, 2016 16:17*

You should have no major issues installing a smoothieboard then. I would say take the plunge, you won't regret it.


---
**Ariel Yahni (UniKpty)** *November 12, 2016 16:21*

Drop here a picture of you current PSU and Stock board to take a look at the connectors


---
**Charlie Thcakeray** *November 12, 2016 17:35*

**+Ariel Yahni** 

![images/a37706da22932e3caccc841532863179.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a37706da22932e3caccc841532863179.jpeg)


---
**Charlie Thcakeray** *November 12, 2016 17:35*

**+Ariel Yahni** 

![images/eae8b7937c46040a116099b31c56dc0a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eae8b7937c46040a116099b31c56dc0a.jpeg)


---
**Ariel Yahni (UniKpty)** *November 12, 2016 17:39*

**+Charlie Thcakeray**​ you see that ribbon?  That's carries 1 stepper and 2 endstops so that need to be spliced into separate connectors so need to get yourself a middleman board ( look here in my collection for details [plus.google.com - K40 Laser Cutter Upgrades](https://plus.google.com/collection/EndMTB))  or if you have not bougth smoothie yet, the two guys above can provide you a board and connectors for that ribbon


---
**Richard Vowles** *November 12, 2016 18:12*

That was very useful for me too **+Ariel Yahni**​ thanks, I have the same ribbon and holidays are coming up, time to finally work on the laser!


---
**Ariel Yahni (UniKpty)** *November 12, 2016 18:15*

We are all glad to help here. You want to join LaserWeb community since thats how the majority who upgrade use to control our laser. And it's getting a major overall soon with new features and better CNC support [LaserWeb / CNCWeb](http://plus.google.com/communities/115879488566665599508)


---
**Charlie Thcakeray** *November 12, 2016 18:16*

**+Ariel Yahni** is there a step-by-step guide anywhere? Thanks for your help :)


---
**Ariel Yahni (UniKpty)** *November 12, 2016 18:16*

**+Charlie Thcakeray**​ for exactly what so I can point you to the correct info?


---
**Charlie Thcakeray** *November 12, 2016 18:50*

**+Ariel Yahni** for a conversion from the standard controller to the smoothieboard with middle man. So like circuit drawings ect.


---
**Ariel Yahni (UniKpty)** *November 12, 2016 19:08*

Look at **+Don Kleinschnitz**​ blog here for details [http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html?m=1](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html?m=1)


---
**Don Kleinschnitz Jr.** *November 12, 2016 23:29*

**+Charlie Thcakeray** here is the latest link to schmatics for my conversion. 

[http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)



[digikey.com - K40-S 2.1](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)


---
**Mark Leino** *November 13, 2016 02:00*

**+Don Kleinschnitz**​​ 

Do exactly as he says, and don't question it. It will work I promise. Did for me! **+Charlie Thcakeray**​


---
**Charlie Thcakeray** *November 13, 2016 09:52*

**+Ariel Yahni** **+Mark Leino**  apart from the middle man connector is there any other information i should know or is it a case of plugging in the other connectors? on that wiring diagram it show a relay board, do i need that?


---
**Ariel Yahni (UniKpty)** *November 13, 2016 10:27*

**+Mark Leino**​ would be more suited to respond but there not much, you could add another psu to drive the board and peripherals just to isolate it from Lpsu. From there the other upgrades available


---
**Don Kleinschnitz Jr.** *November 13, 2016 13:52*

**+Charlie Thcakeray**if you are referring to the relay board on my schematics you do not need that for a basic smoothie conversion. 



You will notice that the relay board it is not connected to the smoothie, It is what I use to control power to the vacuum, assist and other accessories. 



That schematic pertains to my conversion and has more than you need for a basic configuration. 



To get started on a basic conversion you only need the smoothie, and middleman. 



You need to make a choice on power. You can power from the LPS ( in my opinion it is marginal). You can also add higher capacity supplies.



I will drop a simplified diagram here sometime this weekend. 


---
**Anthony Bolgar** *November 13, 2016 14:34*

**+Don Kleinschnitz** Are you using the open drain without a level shifter to control PWM?

**+Charlie Thcakeray** There is a direct simple drop in board available from Cohesion 3D.**+Ray Kholodovsky** designed the board that literally all you have to do is remove the old board, screw the new one into the exact same screw holes, plug in the cables fro the old board, and you are done. The config file comes ready to go, you can do the upgrade in about 1/2 an hour if it is your first time, if you have done it before, it can be done in about 10 -15 minutes. No soldering, splicing, making cables. Just a simple drop in replacement board that has all the different variation of K40 connectors so it will work with any sub model of the K40 family. Here is a link to a post of his explaining the board [https://plus.google.com/u/0/+RayKholodovsky/posts/UrNpWddMZok](https://plus.google.com/u/0/+RayKholodovsky/posts/UrNpWddMZok)


---
**Charlie Thcakeray** *November 13, 2016 14:50*

**+Anthony Bolgar** **+Don Kleinschnitz** you guys are all awsome, really appreciate all your help. it sounds like a really simply process then, i suppose as a newbe it can be abit daunting starting off. So for my k40 smoothie conversion ill just need a smoothie boarded and a middle man. 



Do i need to do and software programing at all? i was planning on using laserweb is it just a case to 'printing' from laserweb or is it much more complicated than that?



Another question, whats the PWM all about? 



Thanks again.


---
**Ariel Yahni (UniKpty)** *November 13, 2016 14:55*

**+Charlie Thcakeray**​ LaserWeb is all about loading your design file and sending to the laser. No programing needed with either smoothie or LaserWeb, just you need a properly setup config file. Don't forget to join the LaserWeb community and start reviewing previous post to grab a feel


---
**Don Kleinschnitz Jr.** *November 13, 2016 16:07*

**+Anthony Bolgar** Yes I am using the OD design show here as are multiple others.

[http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**Don Kleinschnitz Jr.** *November 13, 2016 18:37*

**+Charlie Thcakeray** here is that drawing I promised it uses the 24VDC from the LPS and an onboard regulator



[https://goo.gl/photos/UAK45xKcFascNyyt8](https://goo.gl/photos/UAK45xKcFascNyyt8)﻿



The Cohesion board is a good choice for simpler conversion as **+Anthony Bolgar** suggests.

[photos.google.com - New photo by Don Kleinschnitz](https://goo.gl/photos/UAK45xKcFascNyyt8)


---
**Don Kleinschnitz Jr.** *November 13, 2016 18:52*

**+Charlie Thcakeray** as far as PWM is concerned if your not into electronics the subject can be complex to understand but here is an oversimplified explanation.



The K40 needs a digital interface so that software can tell it how to create (burn) an object. To do this it needs to control two things: 

1. turn the beam on-off and 

2. adjust the power in the beam.

The digital input to the laser power supply does this by turning on its power at a specific level while it dwells at a specific location in x & y. 



So how is the power controlled by a digital input? 

For every dot time the power supply is told to turn on and the controller keeps it on for a period proportional to the power it wants before the beam is moved to a new spot. 

So if within a dot time the laser is held on for 50% of the time it is running at 50% of its max power. 

The power is therefore adjusted by modifying (modulating) how long it is on during its dot time, i.e Pulse with modulation (PWM).


---
*Imported from [Google+](https://plus.google.com/109895480370746543865/posts/gov4XNkp1yV) &mdash; content and formatting may not be reliable*
