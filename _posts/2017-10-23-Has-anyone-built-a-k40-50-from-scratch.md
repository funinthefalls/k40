---
layout: post
title: "Has anyone built a k40/50 from scratch?"
date: October 23, 2017 15:19
category: "Modification"
author: "Frank Fisher"
---
Has anyone built a k40/50 from scratch?  If we manage to get the business off the ground the wife wants to get a backup.  I think as you need to replace half of it anyway (I needed a tube straight away and most of a power supply within weeks) and they come assembled, sealed and aligned badly, with low quality fan and pump, why not build it from scratch with a wooden body (dull metal plate at the bottom at the bottom maybe), sort the airflow, sealing, air assist and extraction properly, and leave room for a bigger cutting area and a 800mm 50W tube.  PS I am more than happy controlling it with the standard board via Corel, I have used that since the 90s anyway.







**"Frank Fisher"**

---
---
**Don Kleinschnitz Jr.** *October 23, 2017 15:33*

Many of us on here have thought about this or even done it. 



There are some builds in Openbuilds for a DIY laser machine.

If I was to do it again (and I may) I would build it from Openbuilds parts and shoot for 100W.

Using the stock controller may be problematic as I do not think you can make the firmware adjustments you need for size and speed and I doubt the board has enough drive.


---
**Alex Krause** *October 24, 2017 03:20*

Freeburn is the name of the project on openbuilds that should get you started in the right direction


---
**Frank Fisher** *October 26, 2017 10:48*

That freeburn project mostly links to [http://wiki.openhardware.co.za](http://wiki.openhardware.co.za) which gets a site not found for me :/




---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/BcY7roCK9kD) &mdash; content and formatting may not be reliable*
