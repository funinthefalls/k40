---
layout: post
title: "With the Holidays around the corner, I thought I'd share some vectors with the community"
date: December 09, 2016 20:30
category: "Discussion"
author: "Madyn3D CNC, LLC"
---
With the Holidays around the corner, I thought I'd share some vectors with the community. A few of these are very simple cuts on a laser with awesome results even in raster, but a quick trace is recomended! Hope you enjoy :)



Unfortunately, I am deeply saddened to have my machine down during this time and I'd absolutely love to maybe get some collaborative thinking to solve this mystery and hopefully publish the results for others in the future. Here is the link to the post if you would like to help. It would be greatly appreciated by me and my family, as this is a very bad time for this to happen. I've been researching for days, translating Chinese schematics, pulling out hair, etc.. please help!  [https://plus.google.com/+vapetech/posts/U1pax4sFSXS](https://plus.google.com/+vapetech/posts/U1pax4sFSXS)



![images/c9c30b1a6fa20619c9d86b2a73538e12.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c9c30b1a6fa20619c9d86b2a73538e12.png)
![images/38da00f0e5676f72d2574f8eab613463.png](https://gitlab.com/funinthefalls/k40/raw/master/images/38da00f0e5676f72d2574f8eab613463.png)
![images/71c4cf18abd024f7886937939164b391.png](https://gitlab.com/funinthefalls/k40/raw/master/images/71c4cf18abd024f7886937939164b391.png)
![images/5c1e447233a801ebadd315dbccf474f1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5c1e447233a801ebadd315dbccf474f1.png)
![images/3334b0b4c16bdb16b71d8af13e23449f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3334b0b4c16bdb16b71d8af13e23449f.png)
![images/4371ad1fd116cef5acb6d0736d46c3d6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4371ad1fd116cef5acb6d0736d46c3d6.png)

**"Madyn3D CNC, LLC"**

---
---
**Madyn3D CNC, LLC** *December 10, 2016 02:36*

[https://plus.google.com/+vapetech/posts/U1pax4sFSXS](https://plus.google.com/+vapetech/posts/U1pax4sFSXS)﻿

#PSU #twentythousandvolts #lasercutting #photons #PCB #flyback 

![images/1d1f3f74b39c11f4e93762f6b179afec.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1d1f3f74b39c11f4e93762f6b179afec.jpeg)


---
**Tony Sobczak** *December 11, 2016 04:55*

Following 


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/eEQdFYFwrPa) &mdash; content and formatting may not be reliable*
