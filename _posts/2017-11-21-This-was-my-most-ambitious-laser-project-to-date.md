---
layout: post
title: "This was my most ambitious laser project to date"
date: November 21, 2017 18:42
category: "Object produced with laser"
author: "Jeff Bovee"
---
This was my most ambitious laser project to date.  It is my recreation of the 1893 Chicago Worlds Fair Ferris Wheel for my daughter's Christmas gift.  Overall dimensions 17" tall by 14" wide and driven by a music box movement.

![images/b9861b3c4df5107e467661f72268d83d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9861b3c4df5107e467661f72268d83d.jpeg)



**"Jeff Bovee"**

---
---
**Josh Rhodes** *November 21, 2017 21:06*

WOW!!


---
**BEN 3D** *November 21, 2017 22:28*

Respect!


---
**Anthony Bolgar** *November 22, 2017 02:31*

Nice job!




---
**Ned Hill** *November 22, 2017 02:33*

Awesome!


---
**Jeff Bovee** *November 22, 2017 13:56*

Thanks guys, appreciate the kind words.


---
**Abe Fouhy** *November 26, 2017 07:29*

Amazing work!


---
**Graham Kendall** *November 26, 2017 22:59*

Absolutely fantastic!!


---
**Bob Damato** *November 27, 2017 19:53*

Wow! Well, now Im going to go home and throw away my LASER...


---
**Mike Albers** *November 28, 2017 18:07*

nice work!  Did you do the design?


---
**Jeff Bovee** *November 28, 2017 20:51*

Yeah, it hadn't been my intention to, but after doing a lot of searching around the web I couldn't find anything close so I decided to go all in and design from scratch. Mind you this is still an artists interpretation.




---
**Mike Albers** *November 29, 2017 00:54*

**+Jeff Bovee** Well, then I tip my hat to you thrice, sir!  Once for designing it, twice for cutting it, and a third time for the patience to put it all together!


---
*Imported from [Google+](https://plus.google.com/115193415731522119744/posts/cdbKboW6DFx) &mdash; content and formatting may not be reliable*
