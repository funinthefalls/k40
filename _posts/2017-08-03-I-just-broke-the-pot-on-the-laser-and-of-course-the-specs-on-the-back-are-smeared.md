---
layout: post
title: "I just broke the pot on the laser and of course the specs on the back are smeared"
date: August 03, 2017 07:19
category: "Original software and hardware issues"
author: "Simon Cook"
---
I just broke the pot on the laser and of course the specs on the back are smeared. It seems to be a 1K linear using the multimeter. Does that sound right?



I think it says 2W too (maybe just ZW) ... does it really need such a high power rating as I only seem to be able to get half Watt ones locally.



Finally, I have no idea about the laser circuit yet so I need to ask, since I am using a smoothie with PWM will I have an induction issue with wire wound as everything off the shelf locally also seems to be wire wound. 





**"Simon Cook"**

---
---
**Jorge Robles** *August 03, 2017 07:24*

Yes is 1k, 2w. Get a multiturn one, 10usd or so


---
**Anthony Bolgar** *August 03, 2017 10:31*

I used a 5K ten turn as a replacement, gives very fine adjustments.


---
**Don Kleinschnitz Jr.** *August 03, 2017 11:24*

1k or 5K pots both are both ok,  I have seen them both in machines and I have checked the LPS circuit they drive.



Wire wound will not cause a problem although your question is confusing in that the PWM should not be connected to the pot. If you are using PWM to the IN signal through a level shifter you should consider the alternate connection using L. Lots of info in the community on it and on my blog.



1/2 watt would be ok as there is very little power dissipation.



I also recommend a multi-turn pot and why you are at it a DVM to help tell you the pots setting.



Alternately you can get multi-turn pots with vernier dials.



more in this post:



[donsthings.blogspot.com - When the Current Regulation Pot Fails!](http://donsthings.blogspot.com/2017/01/when-current-regulation-pot-fails.html)






---
**Simon Cook** *August 03, 2017 12:18*

**+Don Kleinschnitz** Thanks for answering all my stupid my questions. Don't be confused ...just be mindful I'm currently completely ignorant. having yet to work out what's under the lid. I imagined the pot would connect directly with PWM (presumably being FET driven?) but at present it's just a black box to me and I have no real idea what's in there. When I have time and two good hands I hope to settle down to read your blog and trace the wiring to get my head around it.  Thanks for all the great info you've written up.


---
**Don Kleinschnitz Jr.** *August 03, 2017 13:41*

**+Simon Cook** they say the only stupid question is the one your did not ask, I'll second that so don't worry or hesitate to ask anything.



The PWM can be connected to the IN signal with the pot installed but I do not recommend it. I will give you some time to do some reading as to the reason and +/-. In the meantime let me know if I can help.


---
*Imported from [Google+](https://plus.google.com/101890222799169927378/posts/jnvNG3rrTsC) &mdash; content and formatting may not be reliable*
