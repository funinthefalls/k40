---
layout: post
title: "Originally shared by Franco nextime Lanza MarlinKimbra for lasers update to sync with upstream MarlinKimbra where now my patches are integrated"
date: June 01, 2016 15:36
category: "Software"
author: "Franco \u201cnextime\u201d Lanza"
---
<b>Originally shared by Franco “nextime” Lanza</b>



MarlinKimbra for lasers update to sync with upstream MarlinKimbra where now my patches are integrated.



Pre-configured branch for K40 with cooler and flowsensor: 

[https://git.nexlab.net/machinery/MarlinKimbra/tree/k40_flow_cooler](https://git.nexlab.net/machinery/MarlinKimbra/tree/k40_flow_cooler)



Pre-configured branch for K40 with NO cooler and NO flowsensor:

[https://git.nexlab.net/machinery/MarlinKimbra/tree/k40_noflow_nocooler](https://git.nexlab.net/machinery/MarlinKimbra/tree/k40_noflow_nocooler)



Updated pronterface for the new M142 mcode: 

[https://git.nexlab.net/machinery/Printrun](https://git.nexlab.net/machinery/Printrun) 



Update inkscape plugin for new M142 mcode and better rastering:

[https://git.nexlab.net/machinery/laser-gcode-exporter-inkscape-plugin](https://git.nexlab.net/machinery/laser-gcode-exporter-inkscape-plugin)



Next will be the time to add M142 support for coolers to LaserWeb2 :)



And remember, if you like my work on laser edition of MarlinKimbra, inkscape plugin and pronterface, support me by donating or even just by spread the project home:

[https://www.nexlab.net/k40-laser/](https://www.nexlab.net/k40-laser/)





**"Franco \u201cnextime\u201d Lanza"**

---
---
**Franco “nextime” Lanza** *June 01, 2016 16:46*

It's just like the M140 for setting target temperature on bed in 3d printers:



M142 SNN



Where NN can be  any temperature as an int or float (25.5 or 25), or 0 to disable cooling



example: to set cooling:

M142 S25.3



To disable cooling:

M142 S0



The command answer with an "ok" as usual


---
**Franco “nextime” Lanza** *June 01, 2016 19:58*

the feedback for chiller temp and flow sensor is added to the answer of M105, normally used for other temperatures like extruders and/or heatbed in 3dprinters.



It just add "COOL: CXX.X/YY.Y @NNN" for cooler/chiller, where XX.X is the actual temperature in celsius, YY.Y is the target temperature in celsius, NNN is the PWM value ( 0-255 )



It also add add "FLOW: N.NN l/m" where N.NN is the flow value in liters per minute


---
**Franco “nextime” Lanza** *June 01, 2016 20:00*

Example answer to an M105 command:

ok COOL:  C:25.6 /24.0 C@:200 FLOW: 5.86 l/min


---
*Imported from [Google+](https://plus.google.com/+FrancoLanzanextime/posts/5N95eHsWYrq) &mdash; content and formatting may not be reliable*
