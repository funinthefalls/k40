---
layout: post
title: "Decided enough was enough. Having so much trouble with that bloody overlong & ineffective blue ducting inside the machine, it had to come out"
date: February 11, 2017 11:18
category: "Modification"
author: "Pablo Verity"
---
Decided enough was enough. Having so much trouble with that bloody overlong & ineffective blue ducting inside the machine, it had to come out.



So why doesn't everyone get rid of it? I found putting paper on the bed, the sheet would get sucked up into the exhaust (I've upgraded the fan) unless weighed down. Out with the spanners.



Some bolts are fiddly, but do-able with patience.



The whole assembly is only held in with four M6 bolts & nuts. Two at the front (once you get rid of that black cover plate over the stepper motor), & two at the back. No electrics to uncouple, nothing else to unplug.



Undo the bolts at the back outside that hold the duct in place. Between the two (the whole assembly in one piece, & the duct), wiggle it out.



Re-assemble the whole thing. Now is the time to re-align the mirrors, a good idea if you haven't done it before. It just takes time, it's really  not hard at all. There are lots of posts around describing how to do that, but it's a  bit obvious, really. Clean them while you're doing nowt!



My focusing lens was so loose it was about to fall off! Which probably explains the lack of perceived power I was getting. I used scotch tape, instead of post-its, it sticks better to the ally head.



The extra effectiveness of this meant I now have to keep the lid slightly open, to allow sufficient intake of air for the exhaust to work properly.



All in all, a couple of hours well spent in learning that these are really quite well thought out machines, put together by monkeys.



Next is to figure out the best way for me to have an adjustable bed, and to throw away that bloody board...!



Thoughts & ideas welcome.



TTFN.







**"Pablo Verity"**

---
---
**Don Kleinschnitz Jr.** *February 11, 2017 12:06*

My build for your reading enjoyment.

[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Pablo Verity** *March 05, 2017 03:16*

Back up & running, the belts are MXL. A variable height table is next... I'll use the GT2 belt that I've got in abundance..  

:)


---
*Imported from [Google+](https://plus.google.com/108260776734176878360/posts/b2HkYxLkPVi) &mdash; content and formatting may not be reliable*
