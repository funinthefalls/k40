---
layout: post
title: "Ray Kholodovsky and others, can I display any text with help of an arduino to the 12864LCD board?"
date: April 08, 2017 16:20
category: "Modification"
author: "Abe Fouhy"
---
**+Ray Kholodovsky**​ and others, can I display any text with help of an arduino to the 12864LCD board? I was hoping to have one nice clean interface readout instead of two to display my coolant temp, flow and startup check for correct directional flow and flow rate.





**"Abe Fouhy"**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 16:27*

Using the M117 gCode you can "display text you want" on the bottom of the screen. 

But if you're asking if you can hook up an arduino and smoothie to the glcd, answer is no. 


---
**Abe Fouhy** *April 08, 2017 16:34*

**+Ray Kholodovsky** can I push data from the arduino to c3d to the LCD?


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 16:36*

Not really, unless you want to write the firmware into Smoothie for that feature yourself. 






---
**Abe Fouhy** *April 08, 2017 16:39*

Nope! Haha. Maybe I'm asking the wrong qurstions. Do you any ideas for me on how to get the solution I want? 


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 16:44*

Yes, the proper approach would be to have 2 screens. Let Smoothie do what's important, ie running the machine. Let the arduino be a status monitor for your coolant and stuff.  



At most you could wire the arduino to Smoothie (single wire) to pause the job if something seems wrong.  


---
**Abe Fouhy** *April 08, 2017 16:49*

Ok. Just figured you have that big screen, would be nice if it was all in one.


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/fm855gy8Vug) &mdash; content and formatting may not be reliable*
