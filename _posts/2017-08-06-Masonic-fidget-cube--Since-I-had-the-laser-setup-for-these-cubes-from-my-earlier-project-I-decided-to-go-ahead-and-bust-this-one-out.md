---
layout: post
title: "Masonic fidget cube.  Since I had the laser setup for these cubes from my earlier project I decided to go ahead and bust this one out"
date: August 06, 2017 03:54
category: "Object produced with laser"
author: "Ned Hill"
---
Masonic fidget cube. 😀 Since I had the laser setup for these cubes from my earlier project I decided to go ahead and bust this one out. Made from a 1.75" (45mm) black walnut cube with maple wood veneer inlays. Lots of small parts 😜. Finished with a couple of coats of wipe on poly. 



Edit:  Some more details.  Depending on your software and controller board you have a cut operation/file and an engrave operation/file.  With separate files you create one and then copy to create the other so they are the same dimension wise.  I'll post a pic below of what the cut vs engrave looks like.   The veneer I used was about 0.5mm thick so it's like cutting thick paper.  Use a low power and quick cutting speed to prevent charring with the veneer. (I did ~3 ma at 15mm/s) You still get some residue but that's easy to take care of later with sanding.  Because I was cutting so many small pieces I made a thin card board tray to put in the bottom of the bed to catch the pieces.   I set up a holder made of 1/8 ply" to hold the cube in place because I'm doing more and plan to do 4 at a time.  I masked a face of the cube and engraved (10ma at 400mm/s) to a depth of about 0.5mm to match the veneer and removed the mask. Repeated for all faces.  The depth appears to slightly less than the veneer but the bottom the the engrave isn't really smooth and you have small fins of wood stickly up which you can compress the veneer into.  I didn't worry about correcting for the laser kerf, either during cutting or engraving, but you can if you want a tighter fit with the inlays.  I then painted the engraved area of one face with wood glue and hand pressed in the corresponding inlay.  Repeated on the opposite face and tightly clamped the two faces between 2 piece of wood to compress in the inlays into the wood.  I used quick drying gorilla wood glue and repeated for the other 2 sets of faces after each dried for a short time.  The inlays were basically flush at this point but I lightly sanded with 320 grit sandpaper to clean everything up.  Because you have a light wood inlayed into a dark wood don't sand hard or you can embed dust of one into the other that will be impossible to get out.  Clean up the dust of light sanding with compressed air or fine paint brush.  Then go over with a damp Q-tip.   Finish as desired.  





![images/69bc6a44986a5ef628ec4e2ec74cbf51.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69bc6a44986a5ef628ec4e2ec74cbf51.jpeg)
![images/9b390a47a1e3dbf4d27df7d0f9bfbd91.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b390a47a1e3dbf4d27df7d0f9bfbd91.jpeg)
![images/c2a76d157ca7b85993c9326520d48753.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c2a76d157ca7b85993c9326520d48753.jpeg)
![images/b79ffb9fc8ba648775e0549eec934fe1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b79ffb9fc8ba648775e0549eec934fe1.jpeg)
![images/6e381421002dc9b4e47c9fd7f556c7b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e381421002dc9b4e47c9fd7f556c7b5.jpeg)

**"Ned Hill"**

---
---
**Alex Krause** *August 06, 2017 04:07*

Nice inlay work brother!


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 05:30*

Nice! Curious: do you engrave the cube first, deep enough for the inlays?


---
**Stephane Buisson** *August 06, 2017 07:26*

**+Ned Hill** please tell us more on how you did it. I couldn't spot any charing.

deep engrave the maple area, cut the maple and insert it ???


---
**Don Kleinschnitz Jr.** *August 06, 2017 11:55*

Really nice marquetry....on my list which is getting too long...


---
**Don Kleinschnitz Jr.** *August 06, 2017 12:05*

Btw you can increase the fit of the insert pieces by wetting them after inserting,  so they swell a tad. Also works to allow squeeze out when clamping and come back and sand after. 


---
**Ned Hill** *August 06, 2017 16:53*

**+Ashley M. Kirchner** **+Stephane Buisson** I updated the post with more details.  But basically I engraved the cube deep enough for the veneer inlay which I then compressed into the engraving during gluing.  



**+Don Kleinschnitz** Thanks for the tip, I'll give that a try.


---
**Ned Hill** *August 06, 2017 17:00*

Here's a pic of the difference between the cut (top) and engrave (bottom) images.  In the cut images the "Square" has black tick marks that can't really be cut, because they are so small.  So I first engraved them at low power and quick speed before cutting the outline of the inlay.  Even then the engraved ticks are mostly burned through the thin veneer but it doesn't matter since the wood beneath is dark.

![images/ef229c787141106c4be8fa448dad910a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ef229c787141106c4be8fa448dad910a.png)


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 20:52*

Awesome work, thanks for the explanation. Some day I'll try inlays ... but that won't be today. I also want to try cutting those mother of pearl sheets, but I don't know if they can be cut with a laser.


---
**Alex Krause** *August 06, 2017 21:09*

**+Ashley M. Kirchner**​ search for dennis boring on the FB laser group... He lasers those shell inlays for guitars


---
**Ashley M. Kirchner [Norym]** *August 06, 2017 21:21*

Oh awesome, thanks **+Alex Krause**!


---
**Neil Munro** *August 09, 2017 21:20*

Nice,it's a bit beyond me at the moment,just learning, but hope to get there.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/2xZoK53uw5p) &mdash; content and formatting may not be reliable*
