---
layout: post
title: "Thought I'd share this with you... I was making these and selling them on eBay for Father's Day"
date: June 25, 2015 21:40
category: "Object produced with laser"
author: "David Wakely"
---
Thought I'd share this with you... I was making these and selling them on eBay for Father's Day. They flew off the shelves and I could barely keep up with orders!! As you can see I made a jig out of red acrylic and used some sturdy cardboard stands to hold the hammer at the right focus height. I think they came out good. I've got to say I'm still Impressed with this little machine. For the money you really cannot complain. It has its quirks for sure but it's cheap and it can produce half decent stuff!



![images/1b9005febe296dc387ce3413ca482f87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b9005febe296dc387ce3413ca482f87.jpeg)
![images/08f1eb111bab67be30597cc1ea9168a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08f1eb111bab67be30597cc1ea9168a8.jpeg)

**"David Wakely"**

---
---
**Sean Cherven** *June 25, 2015 22:03*

How much were you selling them for?


---
**Jon Bruno** *June 26, 2015 01:38*

Wow that's great Dave.. wow.. you cornered that one.


---
**David Wakely** *June 26, 2015 15:27*

These were selling for £10-£12 including shipping. Im pleased with the result and ive had very good feedback from customers. I am always looking for new ideas to make a buck!


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/9wN33BTMpTr) &mdash; content and formatting may not be reliable*
