---
layout: post
title: "Question about engraving mirrors. I picked up some nice 1'x1', clearance so not spec ) mirror just to play with"
date: April 12, 2016 03:51
category: "Materials and settings"
author: "giavonni palombo"
---
Question about engraving mirrors. I picked up some nice 1'x1', clearance so not spec ) mirror just to play with. I did some engraving and it etched the glass but also  left a gold colored film behind. This flim is hard and doesn't come off. I tried going faster but it left the black on the back also. I also set the machine on cut to see and it removed everything 30mm and 8ma. 



How can I engraved using vectors?  I seen a YouTube a guy doing this but no explanation or tips.





**"giavonni palombo"**

---
---
**Phillip Conroy** *April 12, 2016 07:55*

[https://plus.google.com/113759618124062050643/posts/1YW6LuJWNYQ](https://plus.google.com/113759618124062050643/posts/1YW6LuJWNYQ)

Link to how to convert bitmaps to vectors in under 1 minute-posted a while back


---
**HalfNormal** *April 12, 2016 12:51*

Use painters tape on your cutting area to eliminate overburn.


---
**giavonni palombo** *April 12, 2016 13:20*

Phillip thanks for the link it helped but this is what Im talking about wanting to do. 
{% include youtubePlayer.html id="5g4HeYXfNwA" %}
[https://youtu.be/5g4HeYXfNwA](https://youtu.be/5g4HeYXfNwA)



Halfnormal the gold color is left inside of my engraved area, the outer edges are just fine. I post a picture soon.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 14:33*

I actually tested this very early on when I got the K40.



I used a 2 methods to test.



1st method, I converted the image to vector (using Illustrator Live Trace feature). Then, from there I created a patterned fill of 0.5 x 0.5 mm circles). This method worked, however it vibrated the machine out of alignment. So, technically no good as you will constantly have to realign your mirrors.



2nd method, I did the same image conversion, but instead of using patterned fill of circles, I used a patterned fill of lines, spaced very close together. It worked perfectly & didn't vibrate the machine. However, as you see in that video, the effect is slightly different. Mine, I didn't consider finalising the "mock engrave" with a cut around the edge of the object, so I had jagged edges that looked pixelated. However, with a final cut around the edge, would work perfectly.



Basically, all you need to consider is low power/high speed cut, in order to do this, but convert your images to use a series of lines instead of raster pixels. Never use the engrave option, but use the cut option instead.


---
**I Laser** *April 12, 2016 21:49*

Just wondering, there's been numerous posts regarding the danger having the laser reflect back up to the tube.



Wouldn't engraving/cutting mirror be asking for trouble?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 22:02*

**+I Laser** I have been curious about that also, but it seems that people who have engraved anodized aluminium haven't had any problems as yet. Personally I'd imagine the beam reflecting off the object would lose some focus as it passes back through the lens in the wrong direction. I may be wrong however, just my thoughts.


---
**I Laser** *April 12, 2016 22:29*

Can only assume in regards to mirrors, the glass absorbs the laser prior to it hitting the reflective surface. Someone more knowledgeable can confirm.



Haven't had any experience with aluminium, is it reflective enough?



Just funny with all the concern about what you make your bed out of and then there are people engraving mirrors!


---
**giavonni palombo** *April 13, 2016 00:45*

Thank you Y.S. Did it take long to do the extra work on the design side? So Im wondering if there is any real time saving? 



Im engraving the back of the mirror so Im not worried about reflection, seems impossible, no more than anything black.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 01:13*

Depends on the design software you use. As I use Adobe Illustrator, it was very easy for me to create a Pattern Swatch of lines that were spaced 0.5mm apart. At the time, I was unaware of limitations of the machine & the CorelLaser software, so I would revise the process now.



I would create lines of 0.01mm thickness (because CorelLaser cuts anything thicker twice, once on each side of the line). I would then space them 0.01mm apart. As the kerf (or width of the laser beam) is very small (~1mm for 100mm x 100mm square), this should be enough. Actually, I can go do a test shortly to make sure.



I would say the entire process would take all of about 1 minute to create the pattern swatch & apply it to all the shapes. Also, I would create 2 patterns. One with horizontal lines spaced apart vertically & one with vertical lines spaced apart horizontally. I will do that in my test to show you the difference in result (I am unsure what difference it will make, but it may make some). Also you could do angular lines, or even concentric shapes (e.g. one circle inside another circle inside another circle etc etc until you reach minimum size). I wouldn't really recommend the concentric shapes as cutting extremely small shapes is problematic (i.e. it makes horrible sounding noises in the machine, thus the smallest I have been able to cut with no real problem is 1mm x 1mm circle).



I will do up some examples/tests shortly & post some photos to show you the results.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 03:26*

Turns out I mustnt have done a pattern fill, because Corel interprets that as either solid black or solid white. I must have done something else in past. I will figure it out & let you know.


---
**giavonni palombo** *April 13, 2016 13:46*

Thank you Y.S. taking the time to help. Ill see what I can come up with as well. Now that you pointed out what I should be doing. Im going to try it in Vcarve pro, something simple.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2016 14:22*

**+giavonni palombo** If you get it working, please share your results also. Would love to see what other methods work.


---
**giavonni palombo** *April 15, 2016 02:11*

I will, messed with it a little today. Simple objects seem like they will be easy but complex I not sure about.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 15, 2016 03:34*

**+giavonni palombo** I will give it a go again some other time when I've got some spare time. I will try do something complex also, to see if it is doable. Simple objects are easy enough, as you said, but complex could prove to be problematic. 


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/FVDtLXPQLiG) &mdash; content and formatting may not be reliable*
