---
layout: post
title: "Chiller time!! Got my chiller built up and running"
date: August 23, 2016 06:28
category: "Discussion"
author: "Eric Flynn"
---
Chiller time!!   Got my chiller built up and running.  WOW.  Running tube temp at 15C allowed me to cut 1/8" birch at 18mm/s@8mA tube current.  Yes, the tube current is accurate.  I am not using the stock meter.  Here are some pics.  The squares and twirl were cut at 8mA and 18mm.  Very nice cut quality.  the square still in the sheet in the background was at 22mm/s.  It DID cut mostly through, but didnt fall out, so it doesnt qualify, even though it would push out without tearing out the wood.  The chiller was built out of an old de-humidifier.  I am running the tube closed loop with a radiator in the chiller water, with a pump to flow cool water over the radiator.  This allowed me to keep the dehumidifier mostly stock looking, and fairly neat, plus a closed coolant loop on the tube will keep me from having to change the coolant very often.  If anyone is interested I will post the parts I used, and how I done it.  Very inexpensive to do if you can find a dehumidifier cheap.  About $70 in parts + a junk dehumidifier.

![images/284b873afeeb32bf26cfcc8ef6511232.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/284b873afeeb32bf26cfcc8ef6511232.jpeg)



**"Eric Flynn"**

---
---
**Alex Krause** *August 23, 2016 06:33*

Yes very interested :) have a dehumidifier that wants to be converted 


---
**Phillip Conroy** *August 23, 2016 07:30*

Well done any other mods,is it a stock k40?


---
**Jeff Kes** *August 23, 2016 13:13*

I would love to see more as my son and I have just gotten a K40. 


---
**Stephen Sedgwick** *August 23, 2016 13:17*

Totally interested!!


---
**Eric Flynn** *August 23, 2016 13:17*

I will get a writeup together.  Also, the 15c tube temp has brought the stable lasing current down to 1ma. I done an engraving in birch at 1ma and it turned out great


---
**David Spencer** *August 23, 2016 13:39*

So this brigs up a question, How cool should the tube be? I got my chiller working last night and had the water down to 55f/12c, how cool is to cool


---
**Eric Flynn** *August 23, 2016 13:50*

12c is OK.  . However, you want to be above the dew point. Run it as cool as you can without the tube condensing. Right now, 15c is as low as I could go without condensation forming on the output coupler.


---
**Joe Keneally** *August 23, 2016 14:23*

Awesome work and VERY valuable information to us all!  Thanks!




---
**David Cook** *August 23, 2016 18:31*

Good to know. Time to chill out 


---
**Adam J** *October 06, 2016 12:30*

Would love to see how you modded the dehumidifier to do this. Do finish that write up ;-)


---
**David Cook** *October 06, 2016 14:24*

Would an a fan blowing on the output coupler prevent condensation and allows to go even colder?  Maybe to 40f


---
*Imported from [Google+](https://plus.google.com/107613521854079979112/posts/U8L6wgeNqh5) &mdash; content and formatting may not be reliable*
