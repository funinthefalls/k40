---
layout: post
title: "Advice and experiences please. Do you cut acrylic with or without the surface protection it comes with?"
date: February 21, 2018 10:49
category: "Materials and settings"
author: "Duncan Caine"
---
Advice and experiences please.  Do you cut acrylic with or without the surface protection it comes with?





**"Duncan Caine"**

---
---
**Andy Shilling** *February 21, 2018 11:03*

I cut without and no air assist. I presume the plastic protection would melt and catch light.


---
**Paul Mott** *February 21, 2018 12:15*

I also cut without the protective layer.


---
**Jim Hatch** *February 21, 2018 13:05*

Some acrylic comes with a paper cover - I leave that on for cuts to prevent flashback burning. If I'm engraving, I take it off the top and clean up any smoke residue with denatured alcohol after it's done - faster than weeding. If it's a plastic coating I take it off.


---
**Joel Brondos** *February 22, 2018 01:39*

**+Jim Hatch** Where do you get your acrylic - what kind do you buy and what thickness works well?


---
**Jim Hatch** *February 22, 2018 02:15*

 **+Joel Brondos** I tend to get my standard colors from Amazon. Usually use 3mm or 6mm (1/8" or 1/4"). Also get standoffs there too. It's cast acrylic which I find lasers better than extruded. You can actually find a variety of thicknesses if your project requires it - I sometimes use thinner (.118") for the "glass" in lanterns & tea lights. For exotic styles or colors - two-color or golds/metalluces, etc I go with Inventables, Johnson Plastics or Rowmark.


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/hCUJg46xLTg) &mdash; content and formatting may not be reliable*
