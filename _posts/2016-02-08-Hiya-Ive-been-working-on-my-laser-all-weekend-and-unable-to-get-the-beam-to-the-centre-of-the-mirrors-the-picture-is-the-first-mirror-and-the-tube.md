---
layout: post
title: "Hiya I've been working on my laser all weekend and unable to get the beam to the centre of the mirrors the picture is the first mirror and the tube ."
date: February 08, 2016 11:04
category: "Hardware and Laser settings"
author: "Gary Hunt"
---
Hiya 

I've been working on my laser all weekend and unable to get the beam to the centre of the mirrors the picture is the first mirror and the tube  .

Would this affect the cutting of the laser if they weren't centre 

![images/c8a66e331666f531c3c5f81171b521ce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c8a66e331666f531c3c5f81171b521ce.jpeg)



**"Gary Hunt"**

---
---
**3D Laser** *February 10, 2016 11:55*

I am working on mine as well.  Mine where not center and as it was cutter I was loosing power after the center point on the piece come to find out at that point the laser beam was missing the small hole on the lens nozzle.  I think you will be find if you are able to get constant power and cutting over the length of the piece 


---
**Gary Hunt** *February 10, 2016 17:27*

I am struggling to cut all the way through 3mm mdf still I have to do 2 passes at 15ma and speed @ 3.75 


---
**3D Laser** *February 10, 2016 17:35*

I was getting about 95% through in one pass on 80% power at 9.5 speed


---
**I Laser** *February 11, 2016 05:02*

Being in the center of the mirror is not required. But the beam must hit the same spot on each mirror regardless of the head position.



I've been cutting 3mm MDF 9mA 6.5mm in a single pass. This was without air assist and improved lens (yet to check how this improves things). Although I have always had decent extraction... Smoke is a lasers enemy, so if you have visible smoke in your machine you need to fix that first...


---
**Phillip Conroy** *February 12, 2016 05:54*

i 2 had to do 2 passes on 3mm mdf untill i found my laser focal lens damaged and dirty,changed to new lens now cuts 3mm mdf in i pass,however i do have to clean the focal lens evey 3 hours with a cotton bud dipped in pure alcohol,also found last mirror (just above focal lens ) was damaged as well ,waiting on a new set of lenses


---
**Phillip Conroy** *March 02, 2016 09:09*

Your laser tube is too close to your first mirror- mine is at least 20mm away


---
*Imported from [Google+](https://plus.google.com/117989608188273379074/posts/3ABpx2Szw8f) &mdash; content and formatting may not be reliable*
