---
layout: post
title: "Update on water testing. 1. Conductivity tests of distilled water mixed with aquarium algeacide suggests that algeacide does not change the conductivity of distilled water"
date: March 25, 2017 02:00
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
Update on water testing.

1. Conductivity tests of distilled water mixed with aquarium algeacide suggests that algeacide does not change the conductivity of distilled water.

2. The ongoing testing of virgin tap water with no protection against algae growth, show dramatic increase in conductivity. 



Detailed post here: 

[http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)



Testing data here: [https://docs.google.com/spreadsheets/d/1sImHCr2K4mDRnpdzep_qb5DpMAEQHVOJnwe7Dd7hmH4/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1sImHCr2K4mDRnpdzep_qb5DpMAEQHVOJnwe7Dd7hmH4/edit?usp=sharing)



**+Ned Hill** Notice that the increase in water conductivity for tap has the same slope as the temperature. I checked again and this meter is supposed to be temp. compensated. Coincidence? 



I am now running 3 batches:



1. Algae treated (1 gallon)

2. Virgin tap water (1 gallon)

3. Chlorine treated ( my K40 5 gallons)







**"Don Kleinschnitz Jr."**

---
---
**Alex Krause** *March 25, 2017 02:35*

I have been using ageacide since the beginning and it works very well


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 25, 2017 03:25*

That's interesting to note the trend with the tap water. I've been using tap water since the beginning. Never monitored my temps or anything though, so I have no idea what has been happening. How much algaecide are you guys using per quantity? I use roughly 5 gallons (~20L) for my reservoir so I'm wondering how much algaecide do you put? Based on the algaecide directions?



edit: nevermind, I read the detailed post & see that it is 5 drops per 5 gallons.


---
**E Caswell** *March 25, 2017 10:43*

**+Don Kleinschnitz** interesting reading Don, I started to use Standard auto antifreeze at first,  noticed the "tracking" sound on the tube at times. Investigated on here and since your thread. Hanged it to deionised water with anti algicide.  Ever heard that tracking sound since. Pic below of what I used, quite good as SWMBO already has it in stock for the water features we have in the garden. Not had any algae growing yet so time will tell. Might help UK users to identify what they could use. Not tested the conductivity it can only guess that it works as I have not had the cracking sound since used it. 

![images/efd6b7a9c933efd528983bfbc02cc80a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/efd6b7a9c933efd528983bfbc02cc80a.jpeg)


---
**Don Kleinschnitz Jr.** *March 25, 2017 11:44*

Just as a reminder, I am looking for the donation of a dead tube. 



It seems there is strong anecdotal evidence that reducing water conductivity will remove crackling and screeching sounds (we assume that these are from  arching or LPS electronics). However there are a few cases that coolant did not correct the sounds.



Also there seems to be a rash of cases recently where operating (laser) conditions create enough electrical noise (of some kind) to reset controllers and create havoc. In one case there was high voltage being conducted through the water and into the power system. 



It seems there is enough evidence that the coolant can be part of the electrical circuit of these machines. One working theory is that at high voltage there can be current flow in the coolant from anode to cathode if it is conductive enough. The glass jacket which has a fair amount of area could be viewed as the plates of a capacitor and the water the dielectric, with the cathode at ground? @ 20,000V stranger things have occurred, don't know. 



Here in the US I would like to setup and test conductive fluids in the water jacket of a tube so that we can identify exactly how conductive fluid acts in the real application. I can eventually add this to the HV lab and its tests. Hopefully I can find something out without having to have a good tube.


---
**Don Kleinschnitz Jr.** *March 25, 2017 11:45*

**+PopsE Cas** can you tell us what is in that water treatment i.e take a picture of the labels ingredients?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 25, 2017 14:35*

**+Don Kleinschnitz** I just had a thought reading your reply now about different voltages around the world. Does the 220v or 110v or whatever it is make a difference in any of this? Or the LPS regulates all of that to be a consistent output no matter what your input voltage is?


---
**Don Kleinschnitz Jr.** *March 25, 2017 14:45*

**+Yuusuf Sallahuddin** good question, the LPS uses an offline switcher that converts AC voltage and frequencies to the same internals... ..... the supply should handle all AC input voltages and frequencies and create the same output.


---
**Martin Dillon** *March 25, 2017 15:41*

I was just curious what people do to ground the case.  I notice there is a grounding post on the back but no one has mentioned it.



Also on the high voltage weirdness.  I have a cnc plasma cutter and tried to use my old plasma torch which uses high voltage high frequency start and it would do crazy things to my laptop.  Mostly just mess with the screen.  Never killed the laptop but I ended up getting a newer style torch.


---
**HP Persson** *March 25, 2017 15:52*

Nice work Don! great reading and observations of the coolant.

Seen alot of users who doesn't care about the conductivity and just grounds the water. That made me thinking, the source of the charge is still there, and in earlier tests i did i saw a noticeable lowered power output with conductive water (above 100uS/cm).



Maybe someone more into electrics can solve that question. if grounding the coolant with high conductive water is the same as using water not conductive <10uS/cm



In my theory the source of the charge is still there, grounded though so it isnt building up a charge, but, taking power from the tube?

Not sure if that´s how it´s really works - so i ask now when the topic is up again ;)

Not sure how to measure it to test it myself.




---
**Don Kleinschnitz Jr.** *March 25, 2017 15:55*

**+Martin Dillon** I just insured that the house's grounding was in good shape and then make sure all the internal K40 grounds were secure by measuring them to the ground pin on the input connector. I went as far as soldering all crimps. I have nothing connected to the stud although I am considering grounding the bucket.



The need to use a separate earth ground rod creates religious-like discussions. The main thing is to insure that the ground path back to the source is as short (electrically) as possible. 

Most US construction standards will provide this. Some outbuildings may require a grounding rod, however a grounding rod is only effective if it creates as shorter return path to the source.

 <s>--------</s>

Industrial devices are notorious for creating common mode noise from motor controller especially VFD's. Most industrial quality equipment includes a line filter to control line output and input noise. 



You can add line filters to your machine and to an AC power source feeding things like PC's. There is a lot of complex engineering that goes into filtering noise so your results may vary :)



 [amazon.com - Uxcell AC 115/250V 20A CW4L2-20A-S Noise Suppressor Power EMI Filter: Amazon.com: Industrial & Scientific](https://www.amazon.com/dp/B016EJ5DU2/ref=wl_it_dp_o_pC_S_ttl?_encoding=UTF8&colid=3595WLZ2KRHY7&coliid=I2T1HERXANVSZ)




---
**E Caswell** *March 25, 2017 16:24*

**+Don Kleinschnitz** looked on the bottle and doesn't give what it actually contains. However link here to their website. Looking around it seems that it is a fruit based material. However I can't really help much more than that. 

[hozelock.com - Water Feature Treatment - Hozelock](http://www.hozelock.com/our-products/aquatics/pond-care-aquatics/pond-treatments/water-feature-treatment/#full-details)


---
**Don Kleinschnitz Jr.** *March 25, 2017 16:34*

**+HP Persson** I agree with you on the notion that if there is current flow in the coolant it should represent a loss of power in the laser. In fact a few users cutting was reduced and after coolant change normal cutting was obtained.



If everything is working properly I don't think grounding the tank is necessary although it cannot hurt. Perhaps those that had to ground the tank to eliminate noise also have another problem. It is interesting that they measured >1000v in the coolant. Note: they were using distilled water.



Since you planted this notion in my tiny mind (thanks for that distraction btw :) I have been tinkering with a model of this system as shown in this sketch. Although I am sure this is not correct it is how I like to break the problem down (pun unintended) and start a discussion, sometimes just with myself...



I have been thinking of setting up a dead tube with a coolant source and then seeing if I can get current flow through the coolant from a LPS at different conductivities. That is why I am looking for a dead tube.



One of the things that I noticed on mine is there seemed to be a concentration at the anode when I got the crackling sound like perhaps there is a electrically weak (thin) dielectric area at the anode end. And perhaps if the coolant is two close to ground potential areas of the tube simply breakdown under such HV. I don't know how thick the glass is.



Discussions and ideas always welcome .....



[photos.google.com - New photo by Don Kleinschnitz](https://goo.gl/photos/YhTYHvxxjJm1xNzt9)


---
**Don Kleinschnitz Jr.** *March 25, 2017 16:39*

**+PopsE Cas** thanks I guess in the UK vendors are not required to list ingredients or have a Material Safety Data Sheet? 


---
**crispin soFat!** *March 26, 2017 19:17*

The problem is most likely coming from within the pump! 


---
**HP Persson** *March 27, 2017 06:32*

The pumps can probably be ruled out completely in most cases.

Been testing with other aquarium pump, now i have 12v pumps completely isolated, i even tried with a hand cranked once when i did some testing to make sure it wasn´t the crappy china pump doing it.



Some may have issues with the pumps, but the conductivity in the water from the tube is confirmed.



Not a bad idea to swap out the pump though :)


---
**oliver jackson** *June 07, 2017 19:00*

So thanks to my laziness with checking the water I had arcing today. The water looks fine, but has a slightly slimy texture and my multimeter can now measure a resistance when it didn't before. I've been using distilled water but clearly need an additive. Can I just confirm that at the moment the suggested treatment is aquarium algaecide? And are you using the dose suggested on the bottle?


---
**Don Kleinschnitz Jr.** *June 08, 2017 13:04*

**+oliver jackson** yes the aquarium algaecide is recommended. The dosage I used is in the blog post linked above. I think that dosage is what is recommended on the bottle.

When you replaced the water did the arching stop?


---
**E Caswell** *June 08, 2017 13:50*

**+Don Kleinschnitz** **+oliver jackson** what I have noticed with mine is that the one I have used in not aquarium safe and is. It recommended to use in them. It is a water feature only additive. 

I understand that feature type is much greater concentration mix. Thought it was worth pointing out. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Kp91FfQTWzy) &mdash; content and formatting may not be reliable*
