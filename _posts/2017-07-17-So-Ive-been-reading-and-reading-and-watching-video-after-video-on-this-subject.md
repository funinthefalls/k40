---
layout: post
title: "So I've been reading and reading, and watching video after video on this subject"
date: July 17, 2017 18:19
category: "Discussion"
author: "Christopher Elmhorst"
---
So I've been reading and reading, and watching video after video on this subject. I never realized how sensitive the height and focal point is, so my next project after I set up my new air assist, set up my new water pump and ventilation system, I'm going to start working on a Z-Axis Bed. 



Any of you here make your own yet? Or purchase one pre-fabricated? 





**"Christopher Elmhorst"**

---
---
**Don Kleinschnitz Jr.** *July 17, 2017 19:05*

Search <s>lift table</s>

Here is a manual one I designed but not yet built, original author cited in post.

[donsthings.blogspot.com - K40 Clamping Table](http://donsthings.blogspot.com/2017/02/k40-clamping-table.html)



Here is a stand alone one from LO: [http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)


---
**Joe Alexander** *July 17, 2017 22:34*

[lightobject.com - LightObject](http://lightobject.com) sells one for the K40 but from some I heard its a finicky install, and the motor cannot run off the standard psu in the unit so you'll have to add an external 24v one(most of us do anyways). there is also a couple 3d printed projects on thingiverse, worth taking a look.


---
**Christopher Elmhorst** *July 17, 2017 22:35*

**+Joe Alexander** thank you Joe I will look at it all! What is the extra Power Supply for?? Does the stock one not push enough juice to the bulb? 


---
**Joe Alexander** *July 17, 2017 23:51*

yea the stock one that is built into the laser psu is only 1 amp, barely enough for the x and y motors. the z table draws nearly 2 amps alone so they even specify that you will need a separate psu. I added a 24v 6a one to mine and use it to run x and y and dont use the 24v from the laser psu, but i plan on expanding also with a z table, rotary(prototype built), etc.


---
**Joe Alexander** *July 17, 2017 23:53*

clarification: the laser psu has 24v and 5v out for convenience separate from the laser tube signal. this is referring to motor voltage capability of the laser psu only.


---
**Christopher Elmhorst** *July 17, 2017 23:55*

As I was reading your comment I remembered that I have the stock exhaust and water pump plugged into the machine which is horrible for performance then huh? I got my new air assist parts in the mail today and will be plugging them in elsewhere. Haha


---
**Joe Alexander** *July 18, 2017 02:13*

those just tap off of the 120v/24v incoming so its not terrible, just double check that there in-line with a fuse of some sort for safety. And yea i use an in-line duct fan at the end of line to create a draw, and a 240l/h 4 dollar ebay pump that works well and is super quiet. my air assist is an aquarium compressor with a homemade baffle on the intake for air assist, currently hooked up to a 433mhz wireless relay(still have to wire to board someday). loudest part i hear is the air flow, which isnt too loud at all. can still hear tv at normal levels.


---
**Don Kleinschnitz Jr.** *July 18, 2017 13:30*

#K40ACcontrol



Just FYI: I have mine digitally controlled from an auxiliary panel which can also be controlled from the smoothie (if I want to). 

I currently have it wired to turn on the water when the machine is on. Each AC accessory has its own digitally controlled plug (via a relay) or a switch on the panel.



I used to just put all the AC stuff and the machine on one AC power strip till I got fancy. 



[photos.google.com - New photo by Don Kleinschnitz](https://photos.app.goo.gl/OXbDZofKCiUhx6bN2)

[https://photos.app.goo.gl/E3mtJDD3TeJmDHhl1](https://photos.app.goo.gl/E3mtJDD3TeJmDHhl1)

[https://photos.app.goo.gl/N4gNOkUGljBsUWAG2](https://photos.app.goo.gl/N4gNOkUGljBsUWAG2)

[https://photos.app.goo.gl/dhIVsE0UyomYtLHo2](https://photos.app.goo.gl/dhIVsE0UyomYtLHo2)





Also BTW I run everything off of separate DC supplies. I use the LPS 24V for my lift table and 5V accessories. The lift is probably under-powered a bit but I use it intermittently and infrequently so it doesn't matter. 

All the power in my machine have output fuses.




---
*Imported from [Google+](https://plus.google.com/+ChristopherElmhorst/posts/aZG3tgLJ9Wo) &mdash; content and formatting may not be reliable*
