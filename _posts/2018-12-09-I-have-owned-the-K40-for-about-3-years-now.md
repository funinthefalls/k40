---
layout: post
title: "I have owned the K40 for about 3 years now"
date: December 09, 2018 02:13
category: "Discussion"
author: "FNG Services"
---
I have owned the K40 for about 3 years now. It looks like it’s now on its last leg. I am happy with the hours I got out of it, but now it looks like it probably needs a new tube and or power supply. Also the belts need to be replaced.



Instead of spending more money on the K40, I am looking at getting a Chinese 50w Laser [https://www.ebay.com/itm/50w-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-500mm-x-300mm/283290603911](https://www.ebay.com/itm/50w-CO2-USB-Laser-Engraving-Cutting-Machine-Engraver-Cutter-500mm-x-300mm/283290603911). I am just wondering if any one has had any experience with the shipping on one of these the seller recommends paying him $150 for left gate service from the LTL trucking company. Just wondering if anyone has purchased one and had it delivered to the home and what type of experience with the shipping you might have had. Thank!!





**"FNG Services"**

---
---
**greg greene** *December 09, 2018 02:41*

at 150 lbs - it's a two man job to move it off the truck


---
**HalfNormal** *December 09, 2018 02:54*

I purchased a 60 watt laser. They used a pallet jack to move it into my garage.Most trucking companies will not move it farther than that. the trucking company called prior to delivery and make sure I understood what they were able to do. I did not have to pay anything extra because it was free shipping.


---
**FNG Services** *December 09, 2018 03:25*

Thanks! This listing is for free shipping also, but down in the discription on the listing. It talks about this lift gate service. I was not sure if that was something extra I had to buy from the seller. He has a link in the discription also that takes you to another Ebay listing for this $150 lift gate service. So you can purchase it separately. I figured the freight company would call to set up the delivery and if I need that I would pay them directly.


---
**Timothy Rothman** *December 10, 2018 16:14*

I have a 60W laser which is probably more like 50W, it uses a Ruida controller which is pretty good. My machine is very similar to the one you're considering.  I was cautioned about the power supplies being prone to failure,  tubes not being of high quality, wiring being small gauge and could cause fires, and the rails being not high quality.  So far the only issue I've firsthand observed has been the rails.  With a bigger build volume (mine at 500x700mm) it's more prone to alignment issues.  I seem to need to align mirrors every time I use it. Ask the seller questions about quality of the components and make sure they're not just passing a chinese clone your way.



You'll find Russ a treasure trove of information.  Please keep in mind he was learning as he documented a lot of his information.  I use this for alignment and if my K40 ever needed it, I would adapt this design for the  K40. [rdworkslab.com - Laser RECTAL Thermometer #79 Part 1 - RDWorksLab.com](https://rdworkslab.com/viewtopic.php?f=6&t=937)




---
**Nigel Conroy** *December 10, 2018 17:01*

I have one of these k50's at school and a 60W redsail clone at home. I had to pay for liftgate service for the 60W machine as it was a 4/500lb pallet. (long story how I got it in and out of the basement in my previous house).



Lifting the machine (k50) off the truck is doable if you have the right people but if you don't get the lift gate service.



However I would advise you to not get a machine off ebay if you can avoid it and go directly to the manufacturer. This adds the complexity of arranging delivery from your nearest port and having to deal with any customs that you may incur.



The benefits - you get what you actually order - A grade power supply and tube instead of the factory rejects that the ebayers sell on. I helped a friend who wanted to get into this as part of his business and we got quotes for a machine that was comparable in price to the same on ebay. It'll be more work on the front end but you'll get a better machine on the back end.




---
*Imported from [Google+](https://plus.google.com/110019739874461408161/posts/jFy6FR8m7Dg) &mdash; content and formatting may not be reliable*
