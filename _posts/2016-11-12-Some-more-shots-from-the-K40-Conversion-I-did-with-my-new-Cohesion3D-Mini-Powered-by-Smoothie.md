---
layout: post
title: "Some more shots from the K40 Conversion I did with my new Cohesion3D Mini (Powered by Smoothie)"
date: November 12, 2016 20:27
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Some more shots from the K40 Conversion I did with my new Cohesion3D Mini (Powered by Smoothie).  It really is just swapping a few screws and 4-5 wires to upgrade your machine.  It's the same size and mounting hole pattern as the stock board so it pops right in and we've handled all the known K40 connector types right on board.  

I have the machine without the ribbon cable and with JST connectors on the PSU.  I have boards on the way to **+Carl Fisher** and **+greg greene** with the next one going to **+Kirk Yarina** and hopefully these gentlemen will document their experience with their machine wiring types (ribbon cable, screw terminals on the psu, etc.)



![images/453e316a80c85b723e2e9d9624f61395.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/453e316a80c85b723e2e9d9624f61395.jpeg)
![images/85191e0239e882c688c7a95766a68232.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/85191e0239e882c688c7a95766a68232.jpeg)
![images/7e1eb62723b6dba92b80856f1df94c16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e1eb62723b6dba92b80856f1df94c16.jpeg)
![images/4506b90cc2dce8a470fbe5a850326fa8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4506b90cc2dce8a470fbe5a850326fa8.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**greg greene** *November 12, 2016 20:29*

I'll be happy to Ray :)


---
**Ray Kholodovsky (Cohesion3D)** *November 12, 2016 20:32*

Greg I did a full set of step by step images last night and I'll be sure to have those ready for when you have your boards. 


---
**greg greene** *November 12, 2016 20:34*

Great !


---
**Ariel Yahni (UniKpty)** *November 12, 2016 20:39*

Great job,  happy to see that you brought the idea back to production, this will encourage lots of people to upgrade. 


---
**Ray Kholodovsky (Cohesion3D)** *November 12, 2016 20:42*

Thanks **+Ariel Yahni** I had to make lots of changes from versions 1-3 to make it as easy as possible for users to upgrade. But we're back to hand assembly for now. Remix should be here from factory very soon though!


---
**Ariel Yahni (UniKpty)** *November 12, 2016 21:33*

**+Ray Kholodovsky**​ do you have a msrp for the board? I posted about it on FB and have one user asking. Also ETA


---
**Ray Kholodovsky (Cohesion3D)** *November 12, 2016 21:37*

Right now for hand assembled ones I'm doing a bundle for the laser which is the board, pwm cable, Glcd adapter, and U.S. Shipping for $100. 


---
**Ray Kholodovsky (Cohesion3D)** *November 12, 2016 21:37*

Hand assembled can be shipped in like 1-2 weeks right now. 


---
**Scott Thorne** *November 12, 2016 23:03*

Sweet job bro!


---
**Kirk Yarina** *November 13, 2016 02:29*

What's required for the glcd?  Any recommended sizes or particular displays so I can get one on order?



Many pics planned!



Kirk


---
**Ray Kholodovsky (Cohesion3D)** *November 13, 2016 02:31*

**+Kirk Yarina** it's a 12864 GLCD.

I use this; [https://www.aliexpress.com/item/Free-shipping-3D-Printer-Kit-smart-Parts-Controller-LCD-Module-Display-Monitor-RAMPS-1-4-LCD/32313461135.html?spm=2114.13010208.99999999.312.FZ0M0r](https://www.aliexpress.com/item/Free-shipping-3D-Printer-Kit-smart-Parts-Controller-LCD-Module-Display-Monitor-RAMPS-1-4-LCD/32313461135.html?spm=2114.13010208.99999999.312.FZ0M0r)
It's also available on Amazon in the $10-20 range.


---
**Richard Vowles** *November 13, 2016 03:12*

I would be up for one of those **+Ray Kholodovsky**​


---
**Jonathan Davis (Leo Lion)** *November 13, 2016 04:00*

I purchased the mini version and should soon be on its way to me. 


---
**Kelly S** *November 14, 2016 23:14*

Haven't heard from you in a while, interested in purchasing.  If you could get back that'd be great.  :)


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2016 23:17*

Hi **+Kelly S** and **+Richard Vowles** I'm sorry about the delay in responding.  There's been quite a bit of interest, possibly more than I can make by hand.  I'm currently getting ready to quote the board with manufacturers and then if we can get a few pre-orders together I can have the factory make a batch.


---
**Richard Vowles** *November 14, 2016 23:18*

No problem!


---
**Kelly S** *November 14, 2016 23:21*

No problem, I am just super interested and have the fault of being impatient lol...   have a few things I need to do with the cutter for work and hope to get it going as I am a poor single worker of my start up.  :)  if you'd be interested in making me 2 that would be amazing.  And myself and a friend could test them out.  (He has same model as I do).


---
**Jonathan Davis (Leo Lion)** *November 14, 2016 23:25*

**+Kelly S** I'm setting up the Mini version currently and **+Ray Kholodovsky** is beyond helpful.


---
**Kelly S** *November 14, 2016 23:27*

**+Jonathan Davis** do you have an expected time of release?  :)


---
**Jonathan Davis (Leo Lion)** *November 14, 2016 23:29*

**+Kelly S** Not officially as the boards do take awhile to produce and payment is required upfront.


---
**Ray Kholodovsky (Cohesion3D)** *November 14, 2016 23:31*

**+Kelly S** :) Jonathan is a customer and I'm walking him through the setup right now.  Let me get the next 3 I have to assemble for already paid up people taken care of, and then we will see.  I do feel bad when I can't get the impatient people taken care of right away, **+Steve Anken** and many others also need it now now now.


---
**Ariel Yahni (UniKpty)** *November 15, 2016 00:11*

**+Ray Kholodovsky**​ push push push. We are behind you!!!!! 


---
**Ariel Yahni (UniKpty)** *November 15, 2016 00:20*

**+Jonathan Davis**​​ once you are up and running you need to showcase some of your awesome work in the LaserWeb community﻿.  


---
**Jonathan Davis (Leo Lion)** *November 15, 2016 00:41*

**+Ariel Yahni** I will definitely do that,.


---
**Steve Anken** *November 15, 2016 01:28*

Ray, It's OK. Ryan just called. He's another young one man garage startup who has come a long way in a short time. He can wait but he is really eager to get started.



 I'll set him up with laserweb3 so he can start learning it. Any idea how long before production boards ship? I will probably want two so I can have one ready, I doubt this is the last person I help get started on this revolution. :-)


---
**Ray Kholodovsky (Cohesion3D)** *November 15, 2016 01:28*

**+Steve Anken** I will go send out quote requests now.


---
**Ariel Yahni (UniKpty)** *November 15, 2016 01:29*

Revolution indeed **+Steve Anken**​ #yearofthelaser 


---
**Kelly S** *November 15, 2016 04:01*

**+Steve Anken** could you send me the material you sent the other fine gentleman?  Also a one man show and will be ordering this board tomorrow.   :)


---
**Steve Anken** *November 15, 2016 04:16*

Kelly S, Not sure what you mean. I sent nothing to anybody but I did go to a young man's shop and offered to upgrade his k40 with the board Ray designed, as soon as it is available.


---
**Kelly S** *November 15, 2016 12:43*

Oh I am sorry, misunderstood.


---
**James Rivera** *November 20, 2016 23:02*

**+Ray Kholodovsky** Perhaps I missed you posting this info elsewhere, but how would somebody (me) get their hands on one of these?


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 23:02*

**+James Rivera** we're opening for pre-orders this week for the mini. 


---
**James Rivera** *November 20, 2016 23:03*

**+Ray Kholodovsky** Ok. Is there a web page or link of some kind to order from?


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 23:04*

**+James Rivera** it will be [cohesion3d.com](http://cohesion3d.com) - I'm working on the new site and web store for it now. [cohesion3d.com - Cohesion3D](http://cohesion3d.com)


---
**James Rivera** *November 21, 2016 04:07*

**+Ray Kholodovsky** Please reply to this thread when they're ready to order--I don't want to miss it! :)


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 02:06*

**+Richard Vowles** **+James Rivera** and anyone else interested, the Cohesion3D Mini (and Laser Upgrade Bundle you'd need) are now available for pre-order at [cohesion3d.com](http://cohesion3d.com)  

We also have the ReMix board for sale if you have a higher power application in mind.


---
**Richard Vowles** *November 24, 2016 02:37*

Woot! **+Ray Kholodovsky**​


---
**James Rivera** *November 24, 2016 19:19*

Purchased! Thanks **+Ray Kholodovsky**! :)


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 19:20*

Thanks for your support **+James Rivera** 


---
**Kelly S** *November 24, 2016 19:36*

Happy to see you got your store up and running **+Ray Kholodovsky**!  Hope many people find you and it is a wonderful experience for them as well as yourself.  :)  Will take pictures of when I install mine and make a review from a end user perspective.  :D


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 19:37*

**+Kelly S** thanks! And your rev1 boards got tested today and will ship ASAP. 


---
**Kelly S** *November 24, 2016 19:38*

That is wonderful news, so excited!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/UrNpWddMZok) &mdash; content and formatting may not be reliable*
