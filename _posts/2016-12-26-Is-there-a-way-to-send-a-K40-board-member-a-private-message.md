---
layout: post
title: "Is there a way to send a K40 board member a private message?"
date: December 26, 2016 17:34
category: "Discussion"
author: "timb12957"
---
Is there a way to send a K40 board member a private message?





**"timb12957"**

---
---
**Stephane Buisson** *December 26, 2016 17:39*

when you create a new post, don't share with the community but with that person, it will not be public.


---
**timb12957** *December 26, 2016 17:50*

Thanks!


---
**Robert Selvey** *January 03, 2017 04:21*

**+timb12957**  If you ever need anything just let me know I'm always looking for something to do.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/c4yddTvpruk) &mdash; content and formatting may not be reliable*
