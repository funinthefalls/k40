---
layout: post
title: "I purchased a few aircraft templates from and they are pretty good"
date: May 01, 2017 17:48
category: "Object produced with laser"
author: "Jeff Johnson"
---
I purchased a few aircraft templates from [makecnc.com](http://makecnc.com) and they are pretty good. They come with several file types and sizes and have decent instructions. These are my two favorite warbirds. I had to move things around and make minor adjustments but now they fit perfectly in the 300mmx200mm bed.

![images/c5872e7247b5e8fe64b570c1131db0c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5872e7247b5e8fe64b570c1131db0c7.jpeg)



**"Jeff Johnson"**

---
---
**Jose A. S** *May 01, 2017 20:35*

Could you them with us?


---
**ED Carty** *May 01, 2017 22:28*

Those are very nicely done


---
**Paul de Groot** *May 02, 2017 01:41*

I actually measured 320 x 230mm for the k40. I wonder if there are more derivatives on the market besides the power supply versions.


---
**Jeff Johnson** *May 02, 2017 17:14*

**+Paul de Groot** I've only tested the maximum cutting area by adjusting the max dimensions in the settings and I get 313 x 224. It seems like the starting point could move an inch up and to the side, adding another 25mm to each dimension but I don't know how to do this.




---
**Jeff Johnson** *May 02, 2017 17:18*

**+Jose A. S** Sorry, I can't share the files per their terms. They're well worth the cost, though, and I like the idea of rewarding the creators for their work. This is the link to the F4U. It's 9.95 and other models are cheaper and some cost more. [makecnc.com - WWII The Vought F4U Corsair Plane](http://www.makecnc.com/wwii-the-vought-f4u-corsair-plane.php)




---
**Jose A. S** *May 02, 2017 17:22*

If I buy the template, could you share with me your modified template for a 200x300 machine? :p


---
**Jeff Johnson** *May 02, 2017 18:15*

**+Jose A. S** I'm sending them an email request with this question. Their copyright terms say I can't share any files but it never hurts to ask for special situations.




---
**Jose A. S** *May 02, 2017 18:24*

**+Jeff Johnson**​ thank you Jeff, let me know what was their answer, please


---
**Jeff Johnson** *May 02, 2017 19:46*

**+Jose A. S** I got the reply and they do not allow sharing but will include my modified files in their package. I am going to clean up the files, add a few instructions and submit them. I'll let you know when the model includes the new files.




---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/2k19FdvEUqV) &mdash; content and formatting may not be reliable*
