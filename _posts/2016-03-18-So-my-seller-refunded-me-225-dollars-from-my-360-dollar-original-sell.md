---
layout: post
title: "So my seller refunded me 225 dollars from my 360 dollar original sell"
date: March 18, 2016 01:00
category: "Discussion"
author: "3D Laser"
---
So my seller refunded me 225 dollars from my 360 dollar original sell.  Since I still don't know if it's the PSU or the tube that is shot I ordered another k40 to replace it.  Figured I could always strip one for parts and then I can narrow down which one is blow on my old one.  So now I have two lasers my wife is going to love me.  So question can I run two lasers off the same computer at the same time?





**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 18, 2016 02:46*

That's a decent refund. Glad you have another one on the way & can find out what's up with your current laser. I'd imagine it is possible to have 2 run off the computer at the same time, but you'd need some kind of USB splitter cable (that sends the same signal to both outputs). Then technically you could have both machines plugged in, doing the exact same thing at the same time. Although, I'd assume they also both need to be running the same mainboard, else you will encounter troubles with the computer talking to the machines the same, but the machines interpreting it differently.


---
**I Laser** *March 18, 2016 09:36*

Very decent, considering you've been using the machine for a while now... Not sure about US consumer law, but here you'd be entitled to nothing.



Anyway, I'm running two machines off the same PC using a virtual machine. If your two machines have different boards you may be able to run them without the use of a VM.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 18, 2016 10:43*

**+I Laser** So you're running two instances of the software?


---
**3D Laser** *March 18, 2016 10:56*

They advertised two year warranty on parts and when I contacted them they stated they would either send me the parts in 45 days from China or refund me 150.  The other 75 was because the machine arrived damaged.  And as I have orders to fill it was best to take the money and get a new machine and fix that one later 


---
**David Lancaster** *March 18, 2016 11:54*

With the way the USB works, plus the fact that the stock board's comms is encrypted with a dongle unique to the machine, I doubt you'd be able to run two boards of the same computer at the same time (without resorting to I Laser's solution of a virtual machine).


---
**Anthony Bolgar** *March 18, 2016 14:34*

Peters LaserWeb will let you run multiple machines from the same computer.


---
**I Laser** *March 18, 2016 22:02*

**+Corey Budwine** Yeah just surprised, as usually when a user breaks something it's not covered.



**+Yuusuf Sallahuddin** Yes, two instances of software, os, etc. Though I run it in unity mode, so it looks like it's all running on the same os.



**+David Lancaster** I'd assume if the boards were different you'd be fine. I have two of the same boards (m2 boards) so need to run the second on a vm.


---
**3D Laser** *March 18, 2016 22:16*

**+I Laser** I was surprised to as I emailed them about getting spare parts that would fit the machine stating that the water pump plug hand come undone and the power supply was making weird noises from day one also I didn't know if it was the tube or the power supply that maybe faulty so they sent me the funds to replace one of them.  Great company to do business with


---
**I Laser** *March 18, 2016 22:30*

**+Corey Budwine** that is amazing service, normally you'd receive no response, let alone an offer to refund! Where did you buy it from?


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/59hexVvZ1rx) &mdash; content and formatting may not be reliable*
