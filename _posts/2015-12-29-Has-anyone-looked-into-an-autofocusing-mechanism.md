---
layout: post
title: "Has anyone looked into an autofocusing mechanism ?"
date: December 29, 2015 12:00
category: "Modification"
author: "Gary McKinnon"
---
Has anyone looked into an autofocusing mechanism ?





**"Gary McKinnon"**

---
---
**Stephane Buisson** *December 29, 2015 15:40*

I did have a thought about it, then I asked myself is it worth it ? I came to the following conclusion that modifying a K40 should have a limit, a K40 is a perfect "bundle of parts" to start with, and it's for a modest budget (<500€) to be able to make a decent tools. but let's face it, it's not a professional device, putting time for a amateur with low budget make it a great project, more money wouldn't be reasonable.



for the K40 my approach would be different. as the focal is fixed, I would more imagine a moving table on springs, where you slot a piece of waste material in the corners to lower the bed by the exact thickness of what you want to cut (preserving the focal distance). on the math side you will be wrong by half the thickness, but largely in the tolerance of this device. (cutting 3 mm plywood=> error=1.5/50.8 or less than 3%)



a basic mechanism, could make it perfect.


---
**Stephane Buisson** *December 29, 2015 15:53*

I would be interested by more return on +Scott Thorne  last acquisition, his 50W machine got autofocus built in.


---
**Gary McKinnon** *December 29, 2015 16:30*

Good comments, i ordered a lablifter earlier and will install it at some point.


---
**Coherent** *December 29, 2015 19:06*

**+Stephane Buisson**

 I agree 100% ... upgrades to these should have a cost limit. You can very quickly exceed the cost of a larger import unit with your mods already built in! I do think that the smaller 40 watt machines are an excellent entry point to learn more about laser machines, how they work and what really will suit your needs!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 30, 2015 02:14*

I think **+Stephane Buisson** & **+Coherent** are correct. At some point we have to say "enough is enough" when modding our K40. It's a great little machine to learn on, but eventually it's a better idea to invest the $ into a bigger, better machine. I was talking to my leather supplier about it (as he used to work with laser cutters for years) & he said that the cheap machine is good to start with to help make some $. Then I should save a % of each sale until I have enough to buy a better machine (he recommended Trotec if you are in Australia as Epilog has bad customer support over here, although he recommends Epilog if you are in US because their support is supposedly pretty good over there).


---
**Scott Thorne** *December 30, 2015 10:54*

**+Stephane Buisson**...it doesn't have auto focus....what it has is a small piece of acrylic that you hold under the head while raising the table do that it touches the work piece...kind of cheesy but it works great....I was shocked that it had a motorized table because the ad says that the table is manual up and down...it also has a water safety switch...another surprise.


---
**Stephane Buisson** *December 30, 2015 11:07*

thank you for the precision **+Scott Thorne** ,

could you tell us about your experience in a comparison way K40 vs 'K50', good points, what to improve on K50, and is it worth the price difference? go for 60-80-100W ???


---
**Scott Thorne** *December 30, 2015 11:12*

It definitely has more power, the software is kinda cheesy but I like the color usage for power and speed settings, you can cut, engrave, mark and do dot outlines in one program run just by assigning different colors to the project...it is built great...solid chassis...Great optics...a reci tube...well worth the 1800.00 it cost....I see that Amazon now has it for almost 3000.00.


---
**Scott Thorne** *December 30, 2015 11:48*

One thing I forgot to mention...it came perfectly aligned...I've not had to touch anything....surprisingly enough the lens came installed convex side down...not concave side down like the k40.


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/HNdbpixdag6) &mdash; content and formatting may not be reliable*
