---
layout: post
title: "I've installed my Cohesion Mini control board"
date: June 06, 2017 18:00
category: "Discussion"
author: "bbowley2"
---
I've installed my Cohesion Mini control board.  I've been using V Carve to generate Gcode for my Pyranha CNC machine and it does a great job.  What software can I run my K40 using V Carve Gcode?  





**"bbowley2"**

---
---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2017 21:40*

I have a post processor for Vectric. People have been asking for it. I'll have to dig it up. 


---
**Ray Kholodovsky (Cohesion3D)** *June 06, 2017 21:41*

But to answer your question, you need gCode formatted for the laser and a way to send it (memory card or LaserWeb). 


---
**bbowley2** *June 07, 2017 14:33*

Thanks Ray but for some reason I can't get laserWeb to run my k40.


---
**Ray Kholodovsky (Cohesion3D)** *June 07, 2017 23:20*

Ok, well that would be worth looking into.  Why not post to the LaserWeb group with a lot more details about what's going on...


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/97HiWS4nm5H) &mdash; content and formatting may not be reliable*
