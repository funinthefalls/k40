---
layout: post
title: "Hello again! After fantastic answers yesterday, I am back with todays question!"
date: June 02, 2016 11:16
category: "Software"
author: "Josh Smith"
---
Hello again! After fantastic answers yesterday, I am back with todays question!



I have CorelDraw/Laser installed - but as I dont yet know the software, I drew a part in SketchupPro and exported it as an EPS file. 



I imported this into CorelDraw (and after selecting my machine as the M2, and typing in the long number on the sticker) I sent it over to the laser.



It cut out really nicely that Im pleased with - BUT - its far larger than my originally drawn part. The scaling is off and I cant figure out which point in the chain it went wrong?



Any help would be massively appreciated. 



To confirm - the part was the right size in SketchUp, but the final cut piece was too large.





**"Josh Smith"**

---
---
**Stephane Buisson** *June 02, 2016 11:43*

not using the same software chain, but it's a classic, scale issue.

(not looking for a culprit software). I do accurate cutting by adding a rectangle in SU (supposed scale), then in the last software rescale again with correct values (target one). see video for inspiration 
{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[https://www.youtube.com/watch?v=lbTTPkDEhOg](https://www.youtube.com/watch?v=lbTTPkDEhOg)



I am sure with some test and trials, you will find a way.

(rescale are constant or proportional ?)


---
**Jim Hatch** *June 02, 2016 12:26*

**+Stephane Buisson** That's what I do regardless of which laser I'm using. :-) When going between software using "interchange" formats or ones that one software produces natively but the other uses as an import, copy or even a supported format, if the software versions are not exactly correct (and it's trial & error to figure out which version of software A that matches which version of software B) then I get odd behavior. 



The rectangle trick is the easiest way I've come across to be able to figure out what I need to rescale in the last software - I use a 10mm square because the math is easier but any known sized object works. I also do a point sized circle in the upper left of all layers so when I send each layer to the cutter I get the same origin position which is otherwise an issue sometimes with the K40.


---
**Josh Smith** *June 02, 2016 13:25*

Thanks all! Ill try exporting in DXF next - but what ive been doing is drawing a rectangle to the correct size, and then dragging my imported file to scale the same. I also just select the file and manually change the X/Y. Not ideal, but an easy fix. Cheers all!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 18:05*

I think like others have said here, the issue is upon importing into Corel the scale is out of proportion. I find that whenever I import SVG files, the scale is off & numerous times I have forgotten to adjust it before cutting/engraving to realise half way through that I have just destroyed the piece I was working on. I generally put a rectangle around everything at 300x200mm in my original source file & then when I import into Corel I just select all & scale to 300x200mm. Then everything inside that rectangle scales to the correct size. I then set the border of that rectangle to none & I use this rectangle instead of the 1 pixel dot most put in the top left corner. This helps keep engraves/cuts aligned with each other when I am doing multiple layers.


---
**Don Kleinschnitz Jr.** *June 06, 2016 12:48*

I also use SU and frankly I have given up the awe-full SU/corel/laserdraw tool chain, its why I am moving to Smoothie and LaserWeb so I can get to GCODE then I can see what is being sent to the hardware. 



I have concluded that I can go from SU(free)-export svg or gcode then LaserWeb..NO?

 I even found that the svg exporter I am using is inaccurate by a line weight. I have never gotten the free .dxf exports to work reliably.



**+Peter van der Walt** can you point me to the plugin cited above?


---
**Don Kleinschnitz Jr.** *June 06, 2016 12:49*

**+Peter van der Walt** I found it here: [http://www.guitar-list.com/download-software/convert-sketchup-skp-files-dxf-or-stl](http://www.guitar-list.com/download-software/convert-sketchup-skp-files-dxf-or-stl)

Thanks, I will give it a try.


---
**Don Kleinschnitz Jr.** *June 06, 2016 12:53*

**+Peter van der Walt**  Awesome..... can't wait for my Smoothie. I assume you are converting your yet to arrive K40 to the same :)?


---
*Imported from [Google+](https://plus.google.com/103613958984945836482/posts/K65u7v2TRGo) &mdash; content and formatting may not be reliable*
