---
layout: post
title: "Lowes to the rescue - I picked up a 18\" X 24\" 1/4\" piece of acrylic for $30.00 USD to replace the misrepresented polycarbonate that I tried cutting yesterday"
date: May 16, 2017 18:08
category: "Materials and settings"
author: "Anthony Bolgar"
---
Lowes to the rescue - I picked up a 18" X 24" 1/4" piece of acrylic for $30.00 USD to replace the misrepresented polycarbonate that I tried cutting yesterday.  Not sure if it is cast or extruded, but I will find out soon with a test engrave.





**"Anthony Bolgar"**

---
---
**Jim Hatch** *May 16, 2017 18:43*

All the real Acrylic I've gotten from Lowes and Home Depot has been extruded. I've gotten stuff that was labeled on the shelf  as acrylic but was really polycarbonate (Lexan being the predominant supplier). 


---
**Anthony Bolgar** *May 16, 2017 19:54*

Have you tried cnc milling the polycarbonate? I am hoping I can do so to use up the pieces that the shifty ebayer sent me.


---
**Jim Hatch** *May 16, 2017 19:57*

No. I would expect you'd have to go slow and shallow or risk melting blobs. I ended up tossing it in the bin.


---
**Anthony Bolgar** *May 16, 2017 20:01*

I could try engraving it at full power and then put the melted stinking blob up for sale as modern art ;)


---
**Ariel Yahni (UniKpty)** *May 17, 2017 00:24*

I have milled PC with no issues


---
**Anthony Bolgar** *May 17, 2017 02:42*

Thanks Ariel, good to know.


---
**Steve Clark** *May 17, 2017 21:45*

**+Anthony Bolgar**  Do you Have a Tap Plastics near?  They have bins full of acrylic rems 8x8 inches and different thicknesses for a buck a piece. The staff can show you how to tell which are the cast acrylic.   


---
**Anthony Bolgar** *May 18, 2017 13:46*

Not near me unfortunately.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/DE7gxoAn3s1) &mdash; content and formatting may not be reliable*
