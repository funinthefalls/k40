---
layout: post
title: "Really need some help asap. So I upgraded to a larger 50w dsp machine, and after about 15 minutes of cutting time, the power supply went out"
date: July 08, 2016 01:08
category: "Discussion"
author: "Brandon Smith"
---
Really need some help asap.



So I upgraded to a larger 50w dsp machine, and after about 15 minutes of cutting time, the power supply went out. I am replacing the power supply that the unit had with a nicer power supply from LO. Unfortunately the new power supply wiring terminals are different. I dont want to make any assumptions on wiring up the new PS, so I would appreciate any help from the community so that I can get this machine back up and running (absolutely amazing machine by the way).



The pic with the controller shows the stock PS wired up to the DSP controller. It has one HV red wire that comes out of the other side. Also there is a little loopback wire.



The new PS terminals are slightly different. Only three terminals on the one side. Except that now there is a red HV wire coming out of the other side, as well as a white wire (unknown to me what it is).



Any help would be greatly appreciated. I posted on the LO forum, but did not receive any responses. I would love to write a review on this machine soon also, worth every penny upgrading over the k40 (still have my k40 at this moment). You can see some gorgeous engraving in the last pic right out of the box without any calibration.



![images/1edd07950c7a56b1e243cb463418ae14.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1edd07950c7a56b1e243cb463418ae14.jpeg)
![images/6c95209e9b531f91604105e81289d1e2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c95209e9b531f91604105e81289d1e2.jpeg)
![images/0f862bc0b3357549fe3f221ccd154a97.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f862bc0b3357549fe3f221ccd154a97.jpeg)

**"Brandon Smith"**

---
---
**Scott Thorne** *July 08, 2016 09:47*

I'll take a pic when I get off today....I have the same power supply and machine. 


---
**Scott Marshall** *July 08, 2016 09:51*

The red and white wires are the High voltage to the tube and it's own DC return (ground) don't try to solder them, just twist tightly in place, tape firmly with electrical tape and insulate with 100% clear RTV Silicone (about 1/4 to 3/8 thick is adequate, no bubbles or inclusions)

I'm sure to there.



BUT:

It's hard to give you specific info on the rest without more info, There's generally an enable and operate line which I believe may be  the brown and yellow in the center connector (old supply)



Her's my best shot, tread carefully, this is just educated guessing.



WP is "Water Pump" or "Water Pressure" and usually goes to a flow switch to prevent laser operation when there's no flow. The other side of the flow switch goes to 5V or G (gnd) This is the circuit that you see jumped with the yellow wire on the old supply (middle connector). You can do the same until you install one, or (plug coming) buy one of my 'Water Works" protection boards.



H would be "Trigger High for positive firing logic, and "L" would be trigger low for negative firing logic. I believe "IN" would be the other side of the trigger signal, from your controller board, usually 5V and usually PWM capable.



Ok, here's by best guess hookup.



Pins 1 and 2 old are jumped, this is your water shutdown, pins 3 qand 4 or 3 and 6 depending on the logic of the new supply, you may have documentation to tell you that. If not my bet is it's active high (high signal enables laser, so I'd start jumping pin WP to pin 5V if nothing works, switch it to Wp & G and try again.



Moving on,  the controller output seems to be marked "Lout" which is Laser out or Low out (possibly refering the logic state for the output.) Not sure which. It's connected to (hard to see for sure) a green wire which goes to pin 3 of your old supply. This probably is your Laser fire, and is probably active low logic.

So the green wire on pin 3 old goes to pin 2 new.



Wps is Brn, Pin 5old (water power source?)



Lout is Grn, Pin 3old (laser fire)



Lpwm1 is Brn, seems to dissappear into the wire duct I'm guessing it's not used in the factory setup. (especially if you are using a "power Knob")







In the end,



Jump WP to 5V



Lpwm (brn on the controller to "IN" on the new supply. If you are using the PWM, I do't think you are . just leave " IN open (it may be used for a "test" switch).



Green wire from pin 3old ( and "Lout" of the controller) to "L" Pin 2 new



Gnd is yellow is ground, seems to be conected to pin 4old. Connect this to "G" (pin 4) of the new supply



I think the 5V on the new supply is the output for the hot side of your flow switch, so do not connect it to the controller at all.



That's my best guess, but to be sure at all I need more info, at least new photos with the cover off the panduit (wire tray).



Go easy, you can do harm. you're better off tracing th wiring back to the controller to be sure, if you can trace each wire (there's only 4), I can then tell you for sure where it all goes.



Sorry for the rambling logic, it's a tough trace out with the pix yo posted, they give you just enough info to be pretty sure, but not positive, and If I had a meter and 2 minutes with I could verify the logic (fire high vs fire low, and water shut down, high or low.)



If you can give me pin voltages (0 or 5V) on that middle terminal strip with it running, I can probably give you a solid hookup table.



Hope I helped more than confused,



Scott


---
**Brandon Smith** *July 08, 2016 15:25*

Thank you so much Scott^2 :) Looking forward to getting this machine going again!


---
*Imported from [Google+](https://plus.google.com/109949255412543943799/posts/YPFoPoBSi79) &mdash; content and formatting may not be reliable*
