---
layout: post
title: "I have the lightobject Z table, the locknuts tend to loosen up due to movement of the threaded rod turning and it works ok for a while and then I end up with unequal turning of one or more of the threaded rods resulting in"
date: January 24, 2018 14:10
category: "Discussion"
author: "Kevin Lease"
---
I have the lightobject Z table, the locknuts tend to loosen up due to movement of the threaded rod turning and it works ok for a while and then I end up with unequal turning of one or more of the threaded rods resulting in off perfectly level and it jams and I have to disassemble and reset it.  So I tried using threadlock on the locknuts and then I have had the table lock up when it is near the lower ends of the threaded rods because it seems like it needs a bit of play and if locknuts tight it can't move well either.  I removed the threadlock from the pieces and now back to square one, anyone have any suggestions?





**"Kevin Lease"**

---
---
**Steve Clark** *January 25, 2018 01:04*

The only things I can think of are:



 1) when you tighten the assembly down (if you did) the surface of the case is warped or debris is under it and it's pushing/pulling the frame out of line/square.



2) The z axis frame is out of square...  so you will need to loosen the frame up and re-adjust it. 



3) Maybe a bent support post or burr on the ends

4) One (or more)of the threaded post has a burr or defect on it.


---
**John Plocher** *February 06, 2018 06:41*

**+Kevin Lease** - on mine, one of the threaded rods is slightly warped at one end, which causes binding - I just try to not lower my table to that point...


---
**Kevin Lease** *February 06, 2018 12:19*

Ultimately I decided to remove limit switches off z axis and don’t have z axis home just set height relative to top of z frame for the work piece I am using and that is working well for me


---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/g1H8w8NuRZz) &mdash; content and formatting may not be reliable*
