---
layout: post
title: "Does anyone know if the student/home version of Corel Draw is compatible with Corel laser?"
date: November 19, 2017 06:20
category: "Software"
author: "James Lilly"
---
Does anyone know if the student/home version of Corel Draw is compatible with Corel laser?







**"James Lilly"**

---
---
**Jim Hatch** *November 19, 2017 14:58*

The Home version is not. The educational version is. The problem is that the Home version gets installed in a different directory structure and with a different executable name than the other versions so when you run the CorelLaser install that adds the add-in to the Corel toolbar it can't find the registry and file system entries it's expecting.


---
**Ned Hill** *November 19, 2017 15:48*

Also the K40 Whisperer program is another option to use with the stock controler other than the corellaser plugin.

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/PUKmZrZe7Kv) &mdash; content and formatting may not be reliable*
