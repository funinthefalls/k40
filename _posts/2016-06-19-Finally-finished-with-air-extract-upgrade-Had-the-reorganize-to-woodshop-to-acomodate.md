---
layout: post
title: "Finally finished with air extract upgrade. Had the reorganize to woodshop to acomodate"
date: June 19, 2016 01:20
category: "Modification"
author: "Ben Marshall"
---
Finally finished with air extract upgrade. Had the reorganize to woodshop to acomodate. Added some led lights. Extraction works well seemingly. Will cut some pieces tonight to test for smell in my basement.﻿

![images/edac4bb5a3efa0426c879a4d7f5359f1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/edac4bb5a3efa0426c879a4d7f5359f1.jpeg)



**"Ben Marshall"**

---
---
**Anthony Bolgar** *June 19, 2016 01:54*

You did good hard piping the exhaust, it allows for more airflow than a corrugated pipe.


---
**Ben Marshall** *June 19, 2016 02:00*

It was definitely a head scratcher. Originally I was going to feed it into a radon pipe but didn't want to void any warranty. I had concerns about insulation running it out the window so I used two 6mm plexiglass with thick foam in-between and siliconed everything. I put a screen on the exhaust cover to keep critters out. Eventually I'll but an in-line ball joint to keep out the elements, but those sucker run $50+ so until be purchased next month


---
**Ray Kholodovsky (Cohesion3D)** *June 19, 2016 02:19*

Check my post from just now. I'm going to have to do something similar with that window. I've got round metal ducts. Something about not letting static build up when you have moving air with particles in it. 


---
**Ben Marshall** *June 20, 2016 00:12*

Fume extraction works incredibly well! Can't smell anything unless I'm about 1 ft from the cutter; even then it's a weak scent of burnt wood


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/TmYnY1Jv73G) &mdash; content and formatting may not be reliable*
