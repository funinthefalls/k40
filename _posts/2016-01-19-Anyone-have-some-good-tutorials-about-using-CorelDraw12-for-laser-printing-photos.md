---
layout: post
title: "Anyone have some good tutorials about using CorelDraw12 for laser printing photos?"
date: January 19, 2016 01:19
category: "Discussion"
author: "Joseph Midjette Sr"
---
Anyone have some good tutorials about using CorelDraw12 for laser printing photos? Or LaserDRW? Or know of a free or moderately priced program for uploading photos and printing them, simplified?



I am pretty pleased with the initial outcome and operation of the cheap 40W printer with exception of the software. I think Corel will be ok but I wish it had some extra features like setting the home position and a gui to move the laser head on x and y





**"Joseph Midjette Sr"**

---
---
**Jim Hatch** *January 19, 2016 02:00*

I don't use the Corel that came with my K40 - it's got a virus in one of the installation files and their response was I should "turn off anti-virus". Yeah, like that'll happen. 



I use either Inkscape (open source) or Adobe Illustrator (I think you can still download the older CS2 version for free legally from Adobe). 



Then I pop it into LaserDRW and I can do the head movement control from there. It's an extra step vs controlling it from a Corel plugin but no biggie. I have to do something similar with the makerspace laser so I'm used to it. LaserDRW lets you control head position, head return and also has a repeat function which I'll use when I want to cut something and will need to go over it a couple/three times so I can run the head fast enough not to burn the material.


---
*Imported from [Google+](https://plus.google.com/104424492303580972567/posts/BmkoZ1q1n5h) &mdash; content and formatting may not be reliable*
