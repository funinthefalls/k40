---
layout: post
title: "Hello guys, Could you advise me where I can buy the lens for CO2 laser?"
date: February 04, 2016 18:12
category: "Discussion"
author: "Jose A. S"
---
Hello guys,



Could you advise me where I can buy the lens for CO2 laser?. Good quality and price. 



Thank you!



Regards

![images/685e2e5f7fecfd151922edc98f0f7ffe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/685e2e5f7fecfd151922edc98f0f7ffe.jpeg)



**"Jose A. S"**

---
---
**Jim Hatch** *February 04, 2016 18:29*

Lightobjects. They have a couple of different quality levels. Starts around $20 or so. If you want stock quality you can get ZnSe CO2 focusing lenses on eBay for half that. 



I saw a modder who replaced their stock with the top quality lens from Lightobjects and measured the power delivered to the material and found a 25% improvement. That means you can engrave faster or cut deeper.


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/TBddYhSMk5U) &mdash; content and formatting may not be reliable*
