---
layout: post
title: "Ok after about a month finally installed the Laser Head from LightObject today"
date: January 18, 2016 01:07
category: "Modification"
author: "ChiRag Chaudhari"
---
Ok after about a month finally installed the Laser Head from LightObject today. Had to cut new mount head first because it does not fit in the stock mount its way too big. So instead of drilling hole I decided to just cut mount.



Cutting mount out of wood is not the best idea but its easier than dealing with metal. If you cut one your self see the wood grains they should be parallel to Y axis. Mine are not but got the beam so perfectly aligned first time, I dont want to mess with it. 



Air assist does pretty awesome job, I think it helps the beam to cut deeper and cuts are super clean. I guess no more lens cleaning required :) .



![images/14cec33a214a3e465437a8a94670e11a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/14cec33a214a3e465437a8a94670e11a.jpeg)
![images/ad703ec30f738cf2b05ad03d9012a2f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad703ec30f738cf2b05ad03d9012a2f8.jpeg)
![images/47caad26573924c55f4e33a562b4ad46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47caad26573924c55f4e33a562b4ad46.jpeg)
![images/9aa517a32c04b69dbfdbb0b1c38241b5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9aa517a32c04b69dbfdbb0b1c38241b5.jpeg)
![images/b1474496b479928cd57efe8e69cc7fa4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b1474496b479928cd57efe8e69cc7fa4.jpeg)
![images/3c801436b37fcc5a6439f9c66682c189.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3c801436b37fcc5a6439f9c66682c189.jpeg)

**"ChiRag Chaudhari"**

---
---
**Brooke Hedrick** *January 18, 2016 01:27*

Interesting.  The lightobject head fit my k40 X carriage just right.  I got lucky!  Just goes to show the variability out there.


---
**ChiRag Chaudhari** *January 18, 2016 01:31*

**+Brooke Hedrick** You know that is what I though too, it should just fit right in there, but got disappointed couple of weeks ago. And then had to re alight the head with laser and mirror a bit to get the beam straight.



 Still need to cut the ring that hold the lens right in the center, because stock lens is 12mm and the lightobject head its 20mm i think.


---
**Brooke Hedrick** *January 18, 2016 01:38*

Before I switched to the light object replacement lens that fits perfectly in their head (of course) I just very carefully wiggled the head until the lens sat right.  The cutting strength seemed the same as with the old head so I left it at that.



I can definitely say the lightobject replacement lens has improved my cutting abilities.  My depth of cut is maybe 10-25% better than it was.  In cases where I had to make a 2nd, finishing, pass I no longer need to.



I am learning to make cut/engraving samples on different materials when I change something.


---
**ChiRag Chaudhari** *January 18, 2016 01:43*

**+Brooke Hedrick** Cool thanks for sharing. I was thinking about getting at least one extra lens and new mirrors in case something goes wrong. 


---
**Jim Hatch** *January 18, 2016 01:46*

**+Chirag Chaudhari**​ If you got the same one from Lightobjects I got, the focusing lens is 18mm vs the stock 12mm and the mirror on top is 20mm. If you got their lenses, they're better quality and you'll get more power. Someone posted results of a test of the original lenses and LO's and found 25%+ improvements.


---
**ChiRag Chaudhari** *January 18, 2016 01:48*

**+Jim Hatch** 25% improvement is pretty significant. I say that simply helps extending life of the laser tube. 


---
**Arestotle Thapa** *January 18, 2016 02:33*

**+Chirag Chaudhari** How is the air assist tube routed inside the box? I tried to use this cable carrier chain but it doesn't seem that flexible. May be I need to get smaller size.

[http://www.amazon.com/gp/product/B00843VYGO?psc=1&redirect=true&ref_=oh_aui_detailpage_o03_s00](http://www.amazon.com/gp/product/B00843VYGO?psc=1&redirect=true&ref_=oh_aui_detailpage_o03_s00)


---
**ChiRag Chaudhari** *January 18, 2016 04:21*

**+Arestotle Thapa** For now Is just hanging in the back of the machine and it passed it through the exhaust vent. Head pulls it when it travels towards the front of the machine and tubes weight pulls it back when the head moves back. Its just temporary, but later I am going to use the coiled tube I purchased form HF. 

The idea is use the 1/2" tube from AirPump to machine. Drill hole to attach brass hose barb with 1/4" and then attach one end of  coiled hose to it and another to the laser head.



[http://www.harborfreight.com/1-4-quarter-inch-x-20-ft-coiled-air-hose-97923.html](http://www.harborfreight.com/1-4-quarter-inch-x-20-ft-coiled-air-hose-97923.html)



[http://www.harborfreight.com/14-in-barbed-fitting-68212.html](http://www.harborfreight.com/14-in-barbed-fitting-68212.html)


---
**Arestotle Thapa** *January 18, 2016 04:59*

I'll try the coil. BTW, what compressor are you using?


---
**Jarrid Kerns** *January 18, 2016 05:17*

Why not trim the metal plate and lay it under the wood to help strengthen it


---
**ChiRag Chaudhari** *January 18, 2016 05:33*

**+Arestotle Thapa** I purchased Commercial Aquarium Air Pump 110LPM from amazon for about ~$75. But before that I purchased 3 gal oil less compressor first, way too loud and run out of air in about 4-6 minutes then its about 15-20 psi pressure. Also you will require filter to take moisture out of the air.  Returned that and got 1/5 HP airbrush air compressor its does ok job but get hot and sometimes stops after ~20 minutes to cool down. Quiet and does come with moisture filter. Costs about $75 too. Commercial air pumps can run continuous for year(s), so perfect for our job and its pretty quiet, my stepper makes more noise :P. And we dont really need pressurized air, but we need a lot of air (volume) to push the fumes/smoke away. Also the cone shape of head concentrates the air pretty good to help with better cuts. 

So IMHO air pumps are better than compressor. On the side note if you are fine with noise check out this product. [http://www.amazon.com/gp/product/B000XQK2MW?keywords=electric%20air%20pump&qid=1453095136&ref_=sr_1_1&sr=8-1](http://www.amazon.com/gp/product/B000XQK2MW?keywords=electric%20air%20pump&qid=1453095136&ref_=sr_1_1&sr=8-1)


---
**Arestotle Thapa** *January 18, 2016 13:30*

**+Chirag Chaudhari**  How long does the airbrush stops for cooling? Few seconds or minutes?

I did try the pump in your link but that is really loud. I do have AirMist2000 airbrush. It is quiet but not a whole lot of air!

Thanks for the info!


---
**Jim Hatch** *January 18, 2016 16:20*

I have an Intex pump like the link (usually used to fill air mattresses and the like) and an aquarium/water farm air pump (45lpm) that I'll be comparing. I need to get the LO head and it was backordered so I'm waiting on that test. Should be this weekend though as they said it was in on Friday. I'll post the results.



I will add it initially so it's on when the machine is on but I'm likely to add a relay off the laser circuit so it only turns on when the laser is actually firing. 


---
**Nathaniel Swartz** *January 18, 2016 17:25*

I had this same issue, are you sure the "top" isn't actually two pieces?


---
**I Laser** *January 19, 2016 20:57*

+1 Nathanial, looking at the pic it seems you haven't unscrewed the top!


---
**ChiRag Chaudhari** *January 21, 2016 21:11*

I will check it out later today. But I am pretty sure its all one piece. See the pic no. 4. I may not mess with it for now, as its perfectly aligned. I would rather do some raster testing tonight so they can use some data to perfect the code.


---
**Nathaniel Swartz** *January 21, 2016 21:14*

Mine is totally out of alignment so I can take mine apart and post pics if you like for reference


---
**ChiRag Chaudhari** *January 21, 2016 21:26*

**+Nathaniel Swartz** I will sure do appreciate that. It took me 30+ minutes to get the laser shoot straight down through the nozzle so I really dont want to mess with it at the moment. And that made me realize that laser head with adjustable mirror is totally worth it.


---
**I Laser** *January 29, 2016 09:32*

**+Chirag Chaudhari** it definitely comes apart, I've just received two from lightobject and both are the same.



In pic four, the bottom threaded piece unscrews from the top piece. I'm guessing yours is stuck!


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/HL9byBAkGp4) &mdash; content and formatting may not be reliable*
