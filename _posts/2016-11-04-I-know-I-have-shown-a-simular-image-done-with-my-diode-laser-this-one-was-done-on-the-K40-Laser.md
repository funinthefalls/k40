---
layout: post
title: "I know I have shown a simular image done with my diode laser this one was done on the K40 Laser"
date: November 04, 2016 21:33
category: "Object produced with laser"
author: "Nick Williams"
---
I know I have shown a simular image done with my diode laser this one was done on the K40 Laser. Notice the lack of line striations, and the detail.

![images/f6b29556e27062c4a9c754301be8d792.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6b29556e27062c4a9c754301be8d792.jpeg)



**"Nick Williams"**

---
---
**Ariel Yahni (UniKpty)** *November 04, 2016 22:00*

So no striations is due to low power and overlap or something else


---
**greg greene** *November 04, 2016 22:20*

smoothie board?


---
**Nick Williams** *November 05, 2016 02:11*

**+Ariel Yahni** Its a combination but the one thing your not mentioning is high speed head. 

Max PWM was about 9ma or 50% of full range

Delta y was 0.05 mm, This is the big one

Speed was at 250 mm/sec

**+greg greene** No this was done with an Arduino UNO with a heavily modified version of grbl,  using constant velocity image rastering.


---
**Timo Birnschein** *November 05, 2016 02:14*

**+Nick Williams** Do you have a release date for this already? I would LOVE to try it!


---
**Nick Williams** *November 05, 2016 02:32*

**+Timo Birnschein** It will probably be Feb or March before I release this to the general public. I have PM u with info on how to become a beta tester. Let me know if you did not get the message


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 05, 2016 04:42*

Looks great :)


---
**Paul de Groot** *November 07, 2016 06:01*

Great result ☺


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/ckTPixufUfn) &mdash; content and formatting may not be reliable*
