---
layout: post
title: "Hi, my name is Marcus an im from germany (so please excuse my bad english)..."
date: August 03, 2016 06:32
category: "Smoothieboard Modification"
author: "Marcus B."
---
Hi, my name is Marcus an i´m from germany (so please excuse my bad english)...



I´ve bought a laser from the k40 series and want to upgrade the controller.

Can you tell me the difference between a smoothieboard and a dsp controller like AWC608. Advantages and disadvantages ....



regards marcus





**"Marcus B."**

---
---
**Ariel Yahni (UniKpty)** *August 03, 2016 06:39*

Smoothie is less expensive and you can use LaserWeb3 with it


---
**Marcus B.** *August 03, 2016 06:42*

can i do the same things with it like with a dsp controller, at the smoothie board  homepage they say: engraving not possible at the time. 

Can i do engraving with laser web 3?


---
**Marcus B.** *August 07, 2016 04:43*

Thank you, first i have to change the boards (moshi build out, Smoothie get in)


---
**Alex Hodge** *August 08, 2016 14:59*

**+Marcus B.** Yes you can engrave with Smoothie and LaserWeb3. Though it is quite a bit of work and being open source, you'll be looking to the community for support. I have no experience with the DSP option.


---
*Imported from [Google+](https://plus.google.com/100200951364535471433/posts/S37wPnsa8oX) &mdash; content and formatting may not be reliable*
