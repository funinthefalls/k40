---
layout: post
title: "Hi Everyone - I just got my K40 in today with the standard M2 boards"
date: January 12, 2017 01:04
category: "Original software and hardware issues"
author: "Bjorn Dawson"
---
Hi Everyone - I just got my K40 in today with the standard M2 boards. I've been trying to set it up but cannot seem to connect properly. 



Here's what I've done:

- All of the hardware is set up

- The USB cable is plugged into my computer

- The gold USB dongle is plugged in and flashing green



My computer:

Windows 10 (64-bit)



From the other threads it looks like the encryption USB is working well because LaserDRW 2013.02 opens properly when it is inserted. I have entered all of the machine settings including the Device ID. When I click on "Starting", "Preview" or try to jog the laser cutting head nothing happens. 



Any ideas on how to make this connect properly?





**"Bjorn Dawson"**

---
---
**Gee Willikers** *January 12, 2017 01:20*

Dig around in the software files, look for the laser usb driver.


---
**Bjorn Dawson** *January 12, 2017 02:22*

I found the drivers. On the 64-bit computer they wouldn't install properly but I found and set up a 32-bit computer and they installed. Still not having any luck controlling the cutter though! 



When I plug it in nothing pops up. Is it possible that it's a bad PCB? All of the buttons on the cutter work manually. 


---
**Ned Hill** *January 12, 2017 02:34*

The only person I know off hand using LaserDRW is **+Corey Budwine**.  Maybe he can offer some suggestions.  


---
**Bjorn Dawson** *January 12, 2017 02:36*

Thanks Ned! I don't mind using CorelLaser either, just need to get something going :) 


---
**3D Laser** *January 12, 2017 02:42*

**+Bjorn Dawson** did you check the ribbon cables it might be you have something loose where it's not getting to the cutting head 


---
**Ned Hill** *January 12, 2017 02:43*

Do you have CorelLaser?  If not you can see the links Yuusuf posted in this post. [plus.google.com - Anyone know how i can get a copy of corel laser draw? i reformatted my computer…](https://plus.google.com/101243867028692732848/posts/Edy6DPmZ6Ft)




---
**Bjorn Dawson** *January 12, 2017 02:52*

**+Corey Budwine** I just went through the connections and everything seems good. 



**+Ned Hill** Thanks for the link! I have CorelLaser installed and on the software side it looks like it's working but I still can't get a connection through to the board. 


---
**Ned Hill** *January 12, 2017 02:57*

So just to be clear, you have both CorelLaser and CorelDraw installed?  If so, are you just opening CorelLaser which then opens CorelDraw?


---
**Bjorn Dawson** *January 12, 2017 02:58*

Correct! When I open CorelLaser it then opens CorelDraw and shows the small laser cutting menu on the top right where you can configure your cutter, start an engraving, etc. 


---
**Ned Hill** *January 12, 2017 03:02*

Assuming you have entered the device ID, do you get any response from the machine if you select reset from the plugin toolbar?


---
**Bjorn Dawson** *January 12, 2017 03:05*

Nope, no response using the Reset button in LaserDRW (It seems to be the same screen as CorelLaser)


---
**3D Laser** *January 12, 2017 03:06*

When you turn the laser off and on does the laser home?


---
**Bjorn Dawson** *January 12, 2017 03:07*

Nope it does not home


---
**3D Laser** *January 12, 2017 03:09*

**+Bjorn Dawson** it's either a bad connection or a dead board then in my humble opinion sounds like you will be upgrading sooner rather than later 


---
**Ned Hill** *January 12, 2017 03:19*

Ok, one more idea.  On my machine I have a machine switch which energize the machine but a separate power switch  for the control board.  Do you have something similar?  The front of my machine looks like this.  If the red power switch on the bottom right is off it will not home sort of like what you are saying.

![images/bfe9c98e2c10c592a7f407d2f66e09e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfe9c98e2c10c592a7f407d2f66e09e6.jpeg)


---
**Bjorn Dawson** *January 12, 2017 03:21*

That's what I'm starting to think as well. I just brought out the multimeter and it doesn't look like any power is coming out from the power supply so that could be my issue too.


---
**Bjorn Dawson** *January 12, 2017 03:24*

**+Ned Hill** Yes it's similar to yours. I have tried it with that switch turned on and turned off with no success. 



(Thank you all so much for your help by the way!! I wouldn't have made it this far yet on my own!) 


---
**3D Laser** *January 12, 2017 11:22*

Figure it out


---
**Jim Hatch** *January 12, 2017 12:32*

While you dig into the power side, you should also check the Machine setting for the model not just the board ID (the 16 character alphanumeric from the board). Make sure the model is set to the entry that has "-M2" in it. Should be only one.



Also I run mine off a Win10 64bit PC without any issue so you should not need the 32 bit machine. Get LaserDRW to work first before moving to CorelLaser as it's the shortest path to the machine. If that doesn't work Corel won't either because they use the same driver code.



When you start LaserDRW make sure it tells you it's authorized on the splash screen. Otherwise the USB may not be communicating. I had to get a different non-prerelease hub to make mine work even though the powered one in theory should have been a better option.


---
**Bjorn Dawson** *January 12, 2017 20:30*

Thanks for the suggestions **+Jim Hatch**. 



**+Corey Budwine** A technician from the company that imported it will come take a look at the machine next Tuesday to see if he can figure it out. I'm 99% sure it's a hardware problem though! I'll update the thread once I know what was wrong. 


---
**Ned Hill** *January 12, 2017 20:55*

A technician for a k40?  Wow.   Just out of curiosty, who did you buy this from?


---
**Bjorn Dawson** *January 12, 2017 20:57*

I bought it from this company ([http://www.asc365.com/](http://www.asc365.com/)). Their office is close to me so it's easiest for them to just come fix the machine on the spot since it's brand new and under warranty!

[asc365.com - ASC365.com Screenprintng padprinting sublimation officesupply buttonmaker package stentil cuttingplotter](http://www.asc365.com/)


---
**Bjorn Dawson** *February 08, 2017 01:00*

Conclusion: It turns out that it was the power supply. The company swapped it out and the laser cutter fired up beautifully! Thanks again everyone for you help! 


---
*Imported from [Google+](https://plus.google.com/102353573804091820125/posts/Me3gjigE5KW) &mdash; content and formatting may not be reliable*
