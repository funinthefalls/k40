---
layout: post
title: "Hi all, I've not been an active poster here but reader none the less"
date: January 30, 2019 15:32
category: "Discussion"
author: "Dries Verbruggen"
---
Hi all, I've not been an active poster here but reader none the less. I'm owner of another G+ community called 'Build your own ceramic 3d printer' and just like here at K40 community, we've had a lot of active discussion over the last months about where to go now that G+ shuts down. In case it might be useful for this community (I would be sad to see it go!!!) here are some details on our own plans.



We will move our community to Wikifactory, a social design and production platform born out of WikiHouse that best resembles something like GitHub for hardware. Currently it's more focused on hosting open source hardware projects and stories but we're actively working with them on adding discussion forum like features which will launch in a few weeks.



Here's a link to a post by +Wikifactory Community detailing the new features:

[https://wikifactory.com/@carolportugal/stories/wikifactory-forum-for-digital-fabrication-communities-on-google?fbclid=IwAR1bw8I8d0zpkKA-E3yLB1F12UqHspH0_b0nK8KkiJNOyUrqNuMQL-XIHz0](https://wikifactory.com/@carolportugal/stories/wikifactory-forum-for-digital-fabrication-communities-on-google?fbclid=IwAR1bw8I8d0zpkKA-E3yLB1F12UqHspH0_b0nK8KkiJNOyUrqNuMQL-XIHz0)



Link to our 'Make your own ceramic 3d printer' G+ community:[https://plus.google.com/u/0/communities/109375785524125994679](https://plus.google.com/u/0/communities/109375785524125994679)





**"Dries Verbruggen"**

---
---
**Stephane Buisson** *January 30, 2019 17:51*

HI **+Dries Verbruggen**, I can tell you i did look into wikifactory, but it's owned by a startup which is a bit of a empty shell with no turn over, but hosting fee, so you rely on the owner's injection money. the Ltd is registred in a flat for sell near London Borrough Market.  not enough for me to build trust. the best technical solution would be Discourse, need a linux 64 bit server (but hosting fee) the same server could also host word press backup of G+ community , Reddit is another way to go. We aren't set yet, ...


---
**Stephane Buisson** *January 30, 2019 18:14*

**+Dries Verbruggen** have you seen this post.  [plus.google.com - Hey guys, with Google+ shutting down, I have created a Makers forum that I am...](https://plus.google.com/+AnthonyBolgar/posts/VQ3CearHcQu)



It's quiet tempting, the idea to host several Makers communities on the same plateform offer a lot of advantages. from single member login (one place for all), to hosting cost, moderation, visibility, stronger together, ...



 **+Anthony Bolgar** I will try to contact you at the end of the week. (we do have 6 hours time difference as you know)






---
**Tom Salfield** *January 31, 2019 13:44*

Hi **+Stephane Buisson**,

    

This is Tom, I’m the CEO and co-founder of Wikifactory.



To clarify on some of your concerns. Yes, we are a pre-revenue startup. That said, Wikifactory does not rely on “the owners injection of money”.  We have raised significant finance to develop a social platform for collaborative product development (a “Github for hardware”). We’re now a full-time team of 11 and in the process of expanding our team with another 4-5 people. Our key investors are successful entrepreneurs from software and product innovation ventures, and they are fully committed to the values and long-term mission of Wikifactory.



And yes, a Ltd Company is indeed registered in the UK at the address of my London flat. Back in 2015, we started working on our venture from our "garage" i.e. my flat and at co-working spaces. Since then, we have moved to an office in Madrid and expanded into an international set-up with a parent company in Hong Kong.  We are deeply engaged in the very innovative hardware start-up scene in China and are keen to make one global community and connect to prototyping and production capabilities in China for our community.



You're right that we haven’t come around to share this information properly on our site  (due to the fact we’ve been focusing on the product), and intend to correct this soon. Thanks for prompting it.



I also understand your concern that being a startup means our future is not guaranteed, and thus we will ensure that our users can export their data in a useful way (btw, our export functionality is already more complete than Google Takeouts which merely generates some links to G+ URLs which will soon be 404s).



I’d be happy to connect personally and explore how the forum we are developing with Dries for his community can also benefit yours, as well as address any other concerns you might have.


---
**Anthony Bolgar** *January 31, 2019 21:20*

Forum is up and running at the Maker Forums, join if you want to continue helping eachother, I will make sure it stays sane and polite, just like Stephane has accomplished here. I will event invite Stephane to be a moderator if he wishes :)




---
**Anthony Bolgar** *January 31, 2019 22:02*

[forum.revision13prototypes.com](http://forum.revision13prototypes.com)


---
**Anthony Bolgar** *February 01, 2019 00:14*

**+Stephane Buisson** Look forward to chatting with you.


---
**Tom Salfield** *February 01, 2019 16:00*

**+Anthony Bolgar** is there currently a way to export Google+ forums to Discourse? It seems amazing if no-one has done this yet. Are you aware of an existing exporter/importer? [meta.discourse.org - [bounty] Google+ (private ) communities: export screenscraper + importer - marketplace - Discourse Meta](https://meta.discourse.org/t/bounty-google-private-communities-export-screenscraper-importer/108029)


---
**Dries Verbruggen** *February 01, 2019 20:31*

**+Stephane Buisson** yes, we took that into consideration too. I had a call with them asking about financing etc. Non of us as core members felt we wanted to spend time maintaining a self hosted discourse. This would draw time & resources, especially when technologies evolve, security patches need to be applied etc etc. I have a past in moderate hosting & web design and did not which to return to that continuous stream of patches & updates. But yes, there’s a risk but we rather worked with a small passionate team that was very keen on understanding our needs and share a philosophy than another google who doesn’t give a damn. Ultimately, if WF doesn’t make it, we can always migrate again. But we sure hope that will not be needed. One thing we’ve been considering is starting the use of a custom domain. No matter where we are in 20y the domain will bring you there. 


---
**Dries Verbruggen** *February 01, 2019 20:37*

**+Stephane Buisson** given that I’ve done the exercise of thinking self hosting Discource through, I can only hope **+Anthony Bolgar** did the same and came to the conclusion that he’s willing to back this for the next 5-10years minimum. But how’s that different than putting your faith in a angel backed startup? 

Ps. **+Anthony Bolgar** no offence :) I applaud your initiative! Hope you find the critical mass to make this work. 


---
**Dries Verbruggen** *February 01, 2019 20:38*

**+Stephane Buisson** Reddit was not an option for us


---
**Dries Verbruggen** *February 01, 2019 20:44*

**+Tom Salfield** thanks Tom for pitching in. The ‘who’s behind WF’, ‘where does the financing come from’ & ‘what’s the business model’ we’re my first questions when on the phone with Max the first time. So makes total sense to elaborate on that on the site. 


---
**Anthony Bolgar** *February 01, 2019 21:03*

**+Dries Verbruggen** No offense taken. And yes I have the resources to back this for years. I am just leveraging my existing infrastructure to do the hosting, my costs have no tangible increase from hosting a forum.




---
**Stephane Buisson** *February 02, 2019 11:09*

**+Dries Verbruggen** I have been in the hosting business in London for some years (Interxion), in this business it's only about proven track records and trust to gain new customers.

As tom said WF should have done more on that. Don't be offended if we don't choose WF, it's just life.



As per moderating do you realised I have done that just on daily basis for the last 5 years. Yes a community is time and effort. On the other hand we are building a valuable information bank in a open source spirit.



The all G+ move, is more than a cloud move it's full reconsideration for years to come which take a bit of time on top of just IT stuff.


---
**Dries Verbruggen** *February 02, 2019 13:07*

**+Stephane Buisson** It is absolutely about trust. When WF contacted my offering to help I was rather sceptic since they didn't offer the features we needed (forum) and I couldn't find much backstory or insights. But that trust was gained after some great conversations. The website has been updated since then with more FAQ and what I understand from **+Tom Salfield** is that they will improve that even more.



If it interests you, this is not the first time we have to migrate our community in the last 10Y. I did a write up on our community. So our decision is also based a bit on those previous experiences. I fully agree on the fact that communities like ours build valuable information banks, this is also why I have put a lot of emphasis on the fact that we will actually migrate ALL g+ discussions to WF, not just archive, all threads will be open for continued conversation. This was implemented with the help of WF, I'm not aware of a similar solution for Discourse. So when I talk about time & resources, I mean the time & resources needed for maintaining the platform on top of the maintenance of a community & conversations. The latter I'm more than happy with, the extra burden of hosting, patching, financing etc I rather invest in conversation & knowledge exchange :)


---
*Imported from [Google+](https://plus.google.com/107341271399697599220/posts/3nTBENEtycD) &mdash; content and formatting may not be reliable*
