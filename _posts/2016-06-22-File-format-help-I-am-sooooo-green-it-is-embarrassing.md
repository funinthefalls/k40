---
layout: post
title: "File format help. I am sooooo green it is embarrassing"
date: June 22, 2016 15:20
category: "Software"
author: "Denis Bastien"
---
File format help.  I am sooooo green it is embarrassing.  The shapes I want to cut out of fabric are currently in JPG format.  Do I need to convert them using Inkscape ?  What format should I save then in to be able to cut them ?





**"Denis Bastien"**

---
---
**Pete Sobye** *June 23, 2016 13:44*

What I do is open the file in Corel Draw 12 (came on the CD) then save the file as an EMF (Enhanced Windows Metafile) and then you can import it into the LaserDRW program.


---
**Denis Bastien** *June 23, 2016 21:20*

I will give that a try.  Thanks.


---
*Imported from [Google+](https://plus.google.com/103923212690093219161/posts/hwa2zFCG5Rc) &mdash; content and formatting may not be reliable*
