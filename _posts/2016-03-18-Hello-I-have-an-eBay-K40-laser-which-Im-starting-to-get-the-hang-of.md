---
layout: post
title: "Hello. I have an eBay K40 laser which I'm starting to get the hang of"
date: March 18, 2016 00:00
category: "Discussion"
author: "Richard"
---
Hello. I have an eBay K40 laser which I'm starting to get the hang of. I bought this: [https://www.adafruit.com/products/1058?&main_page=product_info&products_id=1058](https://www.adafruit.com/products/1058?&main_page=product_info&products_id=1058) which makes a laser cross-hair. I figured it would be great for helping alignment for parts and stuff... but for the life of me I cannot figure out how to wire it somehow so it has electricity without requiring a battery (or 3). I don't mind buying/running extra wires and a couple extra components as needed, I just don't know what I need or how to hook it all up. My goal is to add a click-on/click-off switch (as similar as possible to the laser on/off button, not the test button which only works while you hold it) to the "control panel". Any help would be greatly appreciated. Thank you.





**"Richard"**

---
---
**Brien Watson** *March 18, 2016 00:32*

Red wire to the +5 on your power supply  black to negative.  Toggle switch to turn it on and off.


---
**Justin Mitchell** *March 18, 2016 10:24*

Problem with a single laser that projects a cross is that because you have to project from the side, not directly inline with the main beam, the position will move with the height of work piece.  The designs that have been discussed here that use two single line lasers solve this issue.


---
**Justin Mitchell** *March 18, 2016 10:26*

I would welcome suggestions of a good way to add a focal depth indicator to an alignment setup. Perhaps a different coloured laser from another angle?


---
**David Lancaster** *March 18, 2016 12:01*

I saw somewhere where somebody used two "dot" lasers instead of lines.  When they overlapped, that indicated both aimed position and surface focal depth.



A dot laser added to an existing two-line crosshair at 45 degrees would probably work ok.


---
**Richard** *March 18, 2016 19:11*

Thank you for the information. As for the depth indicator, I do not need that right now as my machine does not have an adjustable Z-height, but I will keep that in mind for future upgrades.


---
**Vince Lee** *March 19, 2016 15:14*

I replaced the laser enable switch with a dpdt keyswitch.  I turn mine clockwise for the cutting laser and counterclockwise for a laser sight that swings down into the beam path on a rotational actuator.  Was a pain to align initially but works well now.


---
*Imported from [Google+](https://plus.google.com/117522165219520060657/posts/ZdTJj7YtvSM) &mdash; content and formatting may not be reliable*
