---
layout: post
title: "I'm trying to figure out what the issue is with my lasercutter"
date: February 03, 2017 11:15
category: "Original software and hardware issues"
author: "Eward Somers"
---
I'm trying to figure out what the issue is with my lasercutter. It won't cut through anything. We placed a grbl controller in it and checked the signal to make sure the power was high enough, we also cleaned the lenses and the tube exit itself with alcohol. Also, the mirrors have been aligned, were doublechecking everything just in case but can't seem to find the root of the problem.



I also tested the height, placing a piece at an angle. I tried cutting into acryllic and mdf (both materials that are used in a makerlab for lasercutting so they should be suitable)



The cuts hardly get to a mm depth, almost no difference with engraving depth.



Does anyone have some ideas what could be wrong? We're even starting to doubt the lasertube, the original was broken on arrival and I had to buy a new one from the company but I have no idea how you can test the quality of the tube.



P.S. the same issue was there with the stock controller, and it's a k40





**"Eward Somers"**

---
---
**Anthony Bolgar** *February 03, 2017 11:25*

Sounds like you have a severe alignment issue. I would start by going through the alignment procedure.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 03, 2017 11:44*

Also check your focal lens is oriented correctly.



I'm curious if you could post some photos of cut attempts & ramp test. Might assist someone with helping figure it out.


---
**Eward Somers** *February 03, 2017 11:46*

The lens should be oriented correctly, might try flipping it just for the heck of it. We'll try realigning, cleaning, etc just to be certain and I'll post some pictures of the results


---
*Imported from [Google+](https://plus.google.com/101993772584820352944/posts/GzVCcUZLYM6) &mdash; content and formatting may not be reliable*
