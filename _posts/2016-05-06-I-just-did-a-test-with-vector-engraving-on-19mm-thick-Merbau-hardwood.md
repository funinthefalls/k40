---
layout: post
title: "I just did a test with vector engraving on 19mm thick Merbau hardwood"
date: May 06, 2016 09:01
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I just did a test with vector engraving on 19mm thick Merbau hardwood.



I set the piece in position with the focal depth approximately half way through the 19mm. I did this by removing my cutting bed & putting some blocks of scrap wood underneath with some pop rivets nailed in 15mm deep to support the piece. Then I realised I had to find origin point somehow, so I used magnets to hold my MDF guide in place on the bed support rods.



Settings I used were 4mA @ 25mm/s. 0.5mm wide vertical rectangles spaced 0.25mm apart (for the vector engrave). Covered the entire area with the rectangles & then punched out the logo.



Observations

- I've noticed since switching to CorelDraw x5 that when I import composite paths from Adobe Illustrator 9 format files, they do some really weird behaviour with lines going everywhere. Totally unusable. So instead of using composite path for the vector rectangles, I just grouped them & imported AI9 & it works perfectly.

- 0.5mm wide vertical rectangles spaced 0.25mm apart seems to be a good size & spacing for this particular material. Vertical lines seems to minimise the vibrations & cause less issues.

- Slightly jagged edges, due to the vector engrave process. Could possibly be remedied by doing a final cut in the logo shape (to smooth those edges).

- Using the vector engrave method is so much quicker than normal engrave. I won't even bother with normal engrave any more. This took ~9 minutes to complete (91mm x 91mm).



Finishing

- I cleaned up the piece using straight methylated spirits with a nail brush & scrubbed the whole surface area. If you look closely at the pictures you will see there is a slight difference in the black areas once cleaned up.



Overall I'm pretty happy with the results. I did the SHIELD logo, just for you **+Dennis Fuente** as I recall you saying you liked SHIELD too on one of my previous tests.



![images/47cc8d732fa72dece9b853905a7e357d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47cc8d732fa72dece9b853905a7e357d.jpeg)
![images/3f1cb360d51130cc8c88965bb60f068f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f1cb360d51130cc8c88965bb60f068f.jpeg)
![images/3951e4b201259bc915d9183160138522.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3951e4b201259bc915d9183160138522.jpeg)
![images/327354e28d840b1acf427e74e174c19f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/327354e28d840b1acf427e74e174c19f.jpeg)
![images/2f0a9e9bd526f6504f2c2974b6736dc3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f0a9e9bd526f6504f2c2974b6736dc3.jpeg)
![images/d989cb453970a05384dfbe3380ecdc41.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d989cb453970a05384dfbe3380ecdc41.jpeg)
![images/173b61f202aa3e46a325f82aa6878502.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/173b61f202aa3e46a325f82aa6878502.jpeg)
![images/6e3972b4058b8ad81198c9d62aa446ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e3972b4058b8ad81198c9d62aa446ff.jpeg)
![images/f6907d157c97f0dc71d43fd51c750662.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6907d157c97f0dc71d43fd51c750662.jpeg)
![images/1842d925ac681b0c5762137cacce84b0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1842d925ac681b0c5762137cacce84b0.jpeg)
![images/4d8187b9f5b30b112dbcc24cbd82f402.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d8187b9f5b30b112dbcc24cbd82f402.jpeg)
![images/58dcdf3c5bbe5225d2e194cca6681836.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58dcdf3c5bbe5225d2e194cca6681836.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Gunnar Stefansson** *May 06, 2016 09:08*

Wow... that turned out really great... I'll have to try this out on something aswell... thanks for the great details.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 09:10*

**+Gunnar Stefansson** Yeah, it turned out as I'd hoped, but my hopes were not high for it to turn out as well as it did. I'm fairly satisfied.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 06, 2016 09:13*

Added a couple of extra points to the observations that I forgot to mention. - Time taken & - extra note about the vertical lines for the vectoring.


---
**Jim Hatch** *May 06, 2016 12:42*

I try to use vectors for engraving as you noted it's much faster. Except there are some things that don't "vector" well and a raster engrave will result in a better image. Stuff that you'd look at as a grayscale type of image for instance end up with vector paths that can often just turn into blobs by the time it hits the laser. Trial & error until you get enough under your belt for experience to tell you which way to go.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/jN5XozvAR3r) &mdash; content and formatting may not be reliable*
