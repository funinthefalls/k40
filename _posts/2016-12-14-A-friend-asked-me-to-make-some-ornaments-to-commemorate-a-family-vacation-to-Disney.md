---
layout: post
title: "A friend asked me to make some ornaments to commemorate a family vacation to Disney"
date: December 14, 2016 15:29
category: "Object produced with laser"
author: "Jeff Johnson"
---
A friend asked me to make some ornaments to commemorate a family vacation to Disney. This is what I came up with. they're 125mm wide and 3 layers of 2.5m hardwood plywood.

![images/bea5aaaac2bdb9afd83f95d6cad43ef1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bea5aaaac2bdb9afd83f95d6cad43ef1.jpeg)



**"Jeff Johnson"**

---
---
**greg greene** *December 14, 2016 15:39*

cool - nicely done


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/6ge6mYaQudg) &mdash; content and formatting may not be reliable*
