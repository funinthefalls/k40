---
layout: post
title: "Need some help with Focal Length! UPDATE 7-23-2015 2:26 PM CST I have rechecked my alignment, and noticed a major issue!"
date: July 23, 2015 05:02
category: "External links&#x3a; Blog, forum, etc"
author: "Fadi Kahhaleh"
---
Need some help with Focal Length!



UPDATE 7-23-2015 2:26 PM CST

I have rechecked my alignment, and noticed a major issue!

close to 0,0 I was getting two or three burns rather than a single burn.

It turned out, the 1st Mirror was scattering the beam as it was hitting

very close to the edge, I fixed that and re-aligned all mirrors.

Suddenly I can cut 3mm(1/8") MDF without any issues!!

Will continue to test but it seems that this fixed my issue.

Thank you guys for your comments, it HELPED me!

------------------------------------- End of Update <s>-----------------------------------</s>



My K40 is upgraded with the LightObject DSP controller KIT.

It came with an air-assist nozzle so the whole assemble was changed with the one from LightObject. The Focus lens box states F=50.8, I am assuming 50.8 mm, from the concave portion (being up and straight side facing down)



I am getting uneven cuts or more like, closer to origin good

cut-through and moving outwards to Max_X,Max_Y it doesn't cut-through especially when using wood.



am I looking at an uneven bed scenario that is causing the wood to

be out of the focus range? or is it a different matter?



My initial test shows the bed is level +/- 2mm not sure if that equates to A LOT or acceptable in the world of optics.



Referencing [http://www.parallax-tech.com/faq.htm](http://www.parallax-tech.com/faq.htm)

Depth of field section, I can't calculatethe +/- d value as some of the

variables are unknown to me especially that it is a Chinese laser tube!



Thanks for your feedback.





**"Fadi Kahhaleh"**

---
---
**Stephane Buisson** *July 23, 2015 13:06*

Hi Fadi,



1) From your description (moving outwards to Max_X,Max_Y), I think you could have a mirror adjustment issue. 

-> [http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)



2) Focal, it's the distance from the bottom (just out of the lens) to the middle of the part you want to cut (thickness/2). 



this focal is fixed in our case, and work with different material thickness, don't bother excessive precision for cutting, etching is different matter.



3) Air assist, fume evacuation is also important. does some fume stack in this (Max_X,Max_Y) area weakening the ray ?

distance max from exhaust fan.


---
**Joey Fitzpatrick** *July 23, 2015 13:17*

An easy way to verify your focal point is to place a piece of wood across you bed and elevate one end of it an inch or so higher than the other end. Draw a line in coreldraw and set width to hairline. Set the laser power around 20% and engrave the line. The engraved line will be thinnest at the correct focal point of your lens. Then you can measure from the top of the wood to your cutting head/lens and get your actual focal length


---
**Fadi Kahhaleh** *July 23, 2015 14:52*

**+Stephane Buisson** 

- Although I have done the alignment before, I think it is worth revisiting.

- Not sure about extra fumes or burning, i'll check but as of right now, I do not have a compressor for air assist. looks like I'll have to invest in one.



**+Joey Fitzpatrick** 

Good idea... will try it.



Thank you both.


---
*Imported from [Google+](https://plus.google.com/115068648817066933731/posts/9jEZRHN2jGa) &mdash; content and formatting may not be reliable*
