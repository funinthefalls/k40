---
layout: post
title: "Just messing around with some scrap wood"
date: November 14, 2015 21:30
category: "Object produced with laser"
author: "Scott Thorne"
---
Just messing around with some scrap wood.



![images/5ab38a570f9ab94d57ff411c2b7023db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ab38a570f9ab94d57ff411c2b7023db.jpeg)
![images/42d1f56c5d0c173363bd20bab9d3c63a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/42d1f56c5d0c173363bd20bab9d3c63a.jpeg)

**"Scott Thorne"**

---
---
**Scott Thorne** *November 14, 2015 21:47*

150 speed and around 4 mA....and it's still around 1/8 of an inch deep.


---
**Joey Fitzpatrick** *November 14, 2015 22:35*

Looks like piece of an old pine fence picket.  I use them all the time to test on before engraving on more expensive wood


---
**Scott Thorne** *November 14, 2015 22:36*

Lol...it's a pine pallet slat...

Good eye man.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 14, 2015 23:40*

That's a nice depth of engraving. Looks really cool.


---
**Scott Thorne** *November 15, 2015 01:26*

Thanks yuusuf, I was surprised it turned out that deep because I had the power cut way down...I guess I found the sweet spot with the alignment of the mirrors and lens.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 15, 2015 01:47*

**+Scott Thorne** I'd say that & a slower speed allows it to burn on the same spot for longer. I'll test those settings later on with some scrap leather to see how much difference it makes compared to my normal settings (6mA @ 500mm/s).


---
**Scott Thorne** *November 15, 2015 01:49*

The engrave speed is 150


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 15, 2015 01:58*

**+Scott Thorne** is that 150mm/s?


---
**Scott Thorne** *November 15, 2015 12:44*

Yes yuusuf it's mm/s.


---
**Scott Thorne** *November 15, 2015 17:51*

That was the speed it was set on when I got it, I just did some engraving with it set to 300mm/s and it turned out very well and took half the time...Thanks for the tip Yuusuf!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 15, 2015 19:07*

**+Scott Thorne** You'll find the faster the speed, the lower the depth of the engrave. I've got mine set too fast, so my depth is not what I want. I will have to slow down to something like 300mm/s also.


---
**Scott Thorne** *November 15, 2015 19:37*

I've been checking out my mirror alignment and its off a little...the beam is coming out of the head cocked to one side, cutting at an angle, I'm going to set it up again.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/F5mZ3UBgf53) &mdash; content and formatting may not be reliable*
