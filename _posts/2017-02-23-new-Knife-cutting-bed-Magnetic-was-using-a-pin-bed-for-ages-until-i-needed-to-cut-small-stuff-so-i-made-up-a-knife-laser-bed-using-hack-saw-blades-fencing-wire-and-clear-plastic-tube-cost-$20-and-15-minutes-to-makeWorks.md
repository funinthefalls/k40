---
layout: post
title: "new Knife cutting bed- Magnetic was using a pin bed for ages until i needed to cut small stuff so i made up a knife laser bed using hack saw blades ,fencing wire and clear plastic tube ,cost $20 and 15 minutes to make.Works"
date: February 23, 2017 04:16
category: "Discussion"
author: "Phillip Conroy"
---
new Knife cutting bed- Magnetic

was using a pin bed for ages until i needed to cut small stuff so i made up a knife laser bed using hack saw blades ,fencing wire and clear plastic tube ,cost $20 and 15 minutes to make.Works better than i thought it would with no burn marks or glue on back of cut stuff.Also as made of steel magnets will stick to it>magnets also hold knife bed to old cutting table ,allowing very easy removal and cleaning



![images/af637779eb6e1f6ba0b97e8dce447165.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af637779eb6e1f6ba0b97e8dce447165.jpeg)
![images/120e58c6a52c34806a46ac10af0dd472.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/120e58c6a52c34806a46ac10af0dd472.jpeg)
![images/6de43d29fb219abc804c8ecf164e9c9a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6de43d29fb219abc804c8ecf164e9c9a.jpeg)

**"Phillip Conroy"**

---
---
**Paul de Groot** *February 23, 2017 04:33*

Ingenious ☺


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2017 05:50*

Great idea :) I've been trying to figure out a method like this to be able to still magnet my pieces to it. What would maybe be even cooler is to make it like a grid (by cutting slots in some blades & crossing other slotted blades perpendicular to the base layer). Not sure if necessary, but would help keep the spacing between blades even.


---
**Phillip Conroy** *February 23, 2017 06:28*

i brought the wrong type of hack saw blades -they where too flexible ,they will do but next time will ask ..

To change the spacing just cut shorter peaces of tubing for the spacers.

Cutting slots in the blades will not be easy ,especialy getting it all level.For what i cut gaps of 10 mm between the blades is fine .

I do not know how people with Honeycomb bed keep them clean as i need to clean my bed every week or 20 to 30 hours cutting.

Yuusuf may be better just getting steel plate with the pre drilled holes in it ,just go to your local steel dealer with a magnet and pick the size of holes you want.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2017 10:16*

**+Phillip Conroy** Yeah, I'm currently using a galvanised steel plate with perforated holes in it. It gets dirty as hell & I never clean it haha. I've just put a layer of masking tape over the area I usually cut on.



I guess that changing the spacer size is the go for altering the spacing. Didn't quite notice spacers between the blades originally.


---
**Abe Fouhy** *February 23, 2017 16:44*

Would a new bbq grill work?


---
**Phillip Conroy** *February 23, 2017 17:08*

You need something that does not have flat surfaces that can refect  the beam back,and minimize surface contact with the object being cut.I started out using a pin bed made up of magnets and screws on them,problems are need a lot if cutting small objects ie a row  of 30mm hearts they fall to bottom of bed and get marked when the other heats are cut.pin beds are great for objects 100 X 100 and only one item cut at a time. I will be swapping between the knife bed for multiple small items and for only 1 large item the pin bed.honeycomb beds have the disavantage of being hard to clean and magnets will not stick to them.

It is just a matter of picking the right bed for the job you are doing at the time 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 23, 2017 17:28*

I previously made a bed with inch x inch grid using a very fine fishing tracer wire which I spent an hour weaving through holes I drilled into the frame (L shape aluminium edging). Unfortunately, cannot magnet to it, so it doesn't get any use currently. Would be okay for cutting something stiff with no warp (e.g. acrylic).


---
**Abe Fouhy** *February 23, 2017 18:09*

Interesting. Still a n00b, just curious. Thanks!


---
**Phillip Conroy** *February 23, 2017 18:25*

I sell a lot of stuff to ebay etc and need as clean a cut and clean back of cut item,any bed that has a lot of surface contact especially net to the cut allows the sap / glues and smoke to mark  the back of cut items. 


---
**Abe Fouhy** *February 23, 2017 18:38*

Nice! I have an offer to engrave custom wood cell phone cases for a mall vendor. Any tips?


---
**Phillip Conroy** *February 23, 2017 18:48*

I have 600 hours of cutting mdf and plywood  and about 4 hours of etching .Etching takes to long ,if I need to mark wood I just cut the writing at very low power 3ma and 11mm/second.

If you are going to be doing a lot of engraving upgrade the control board ,mine is still stock board as I like core draw x7 and the board does everything I want .


---
**Abe Fouhy** *February 23, 2017 18:53*

What is a decent price for your work. Ie what's your profit? I'm new to it so I have no idea


---
**Phillip Conroy** *February 23, 2017 19:10*

I do not charge for editing or design of cutting files unless very involved, I aim for $30 to 40 hour of cutting so if something takes 30 minutes to cut $15 to 20,if 15 min to cut $10 ,most of my designing is done while waiting for laser to finish cutter other jobs.Most of my customers are regulars theat resell the cut ltems so aim for $30 hr .A 200mm round door plaque in plywood  with 2 to 4 words I charge $5.50


---
**Abe Fouhy** *February 23, 2017 19:14*

That is very reasonable. Just wondering how much to charge them per phone


---
**Phillip Conroy** *February 23, 2017 19:23*

I buy mdf and ply in the largest sheets I can get from bunnings 2440mm x1200mm off hand ply $25 a sheet and $12 for [mdf.To](http://mdf.To) cut the sheets down to 200mm wide I use a bandsaw I got from super cheap autos for $200 .that gives me 200mm wide by 1200mm long .then I use a board cutter to cut to size for job at hand,much safer and faster the Stanley knife. 

[ebay.com.au - Details about  10mm Thick Laminate Wood/Wooden Floor/Flooring Board Sheet Cutter,Dust Free91210](http://www.ebay.com.au/itm/10mm-Thick-Laminate-Wood-Wooden-Floor-Flooring-Board-Sheet-Cutter-Dust-Free91210-/321968956009?hash=item4af6d86269:g:1lUAAOSw-itXvjDE)


---
**Abe Fouhy** *February 23, 2017 19:25*

Is your machine all stock?


---
**Phillip Conroy** *February 23, 2017 19:36*

Air assist ,led light,water traps,for cooling second hand undersinkwater chiller,8 inch 250 watt exhust fan,digital water temp meter,safety switch on lid,2hp 50 liter aircompressor ,air regulator on laser cutter,water flow switch, stock control board,upgraded mirrors and focal lens as well as 5 spares


---
**Abe Fouhy** *February 23, 2017 19:39*

Haha, so very modified. All thats missing is the z table and controller! So which upgrades would you say are must haves for $300?


---
**Phillip Conroy** *February 23, 2017 20:02*

Water flow switch ,wired in series with laser enable button $5 ( I stuffed a laser tube because pump plug didn't fit ,and stopped pumping water 10 minutes later cracked tube) the worst part of that was I had a flow switch sitting on my workbench for 2 weeks and didn't get around to fitting it before I cracked the tube.also took out power supply......water temp meter $15 ebay ,I run my laser between 10 deg to 20 deg.air assist laser head $65  ebay you can not cut ply or mdf without one.....throw the stock extracting fan away ,do not use any fan under 20 watts $150

[ebay.com.au - Details about  NEW 10" Heavy Duty 2900 RMP 250mm Portable Super-Speed Extraction Fan Ventilator](http://www.ebay.com.au/itm/NEW-10-Heavy-Duty-2900-RMP-250mm-Portable-Super-Speed-Extraction-Fan-Ventilator-/122099884971?hash=item1c6db863ab:g:hBgAAOSwHoFXvmet)


---
**Phillip Conroy** *February 23, 2017 20:02*

Where do you Cal home?


---
**Abe Fouhy** *February 23, 2017 20:50*

Wow that fan is much bigger than I expected. That is really good advise about the flow switch. Should i build an active peltier water chiller, is it needed? Does the head assist in the cutting like a torch or is it to blow stuff out of the way?


---
**Phillip Conroy** *February 23, 2017 21:31*

you can buy tin adaptors to go from 8 inch to 6 inch and then front 6 inch to 4 .  .The fan in link is 10 inch I use a 8 inch one.Air assist manly stops the item catching fire ,as well as keep focal lens clean and helps cutting .NEVER LEAVER LASER WHILE IT IS [CUTTING.Do](http://CUTTING.Do) you have a k40 laser yet?The ebay air assist laser head also uses 18mm focal lens .A lot of people use just a aquarium air pump (large metal case one)do not know model ,I try ed one and didn't like not being able to adjust air flow,as with my shop vac I use 10 psi for cutting and bugger all for engraving.


---
**Phillip Conroy** *February 23, 2017 21:44*

Do not waste your time with the peter device as it will never provides enough cooling ,just freeze 2 liter milk bottles with water and drop into tank to cool water.and keep an eye out on the auction sites for a water chiller undersinkwater, mine cost $20 second hand.p's the water chillers cw3000 is not refrigerated ,it is just a water radar and fan DO NOT BUY [ONE.in](http://ONE.in) winter I plumb my cooling water outside to a 300mm computer radator $30 and 3 computer fans $40 ebay...I also only cut for 3 hours before resting my laser tube. So far I have blow 1 tube and worn on out at 600 hours. A hour meter is also a good idea at only $20 .other water cooling ideas are an old refrigerated water bubbler like in schools etc.also try and keep water tank at same level as laser tube other wise when pump stops water drains out of tube a lowing bubbles to form ,which can become traped when pump starts next time.


---
**Abe Fouhy** *February 23, 2017 23:35*

Awesome instructions! Not sure what an undersinkwater is though. Would an air eliminator we use in residential plumbing work to get rid of bubbles? 


---
**Phillip Conroy** *February 24, 2017 01:01*

under sink water chiller is for kitchen to have a separate tap with chilled water 

[ebay.com.au - Details about  Filtered Water Chiller Unit IC8 & Twin Stage Undersink Water Filter Kit DIY](http://www.ebay.com.au/itm/Filtered-Water-Chiller-Unit-IC8-Twin-Stage-Undersink-Water-Filter-Kit-DIY-/222302857946?hash=item33c2486ada:g:l2gAAOSwawpXsUWs)


---
**Phillip Conroy** *February 24, 2017 01:07*

the problem is that water drains back through the pump as well as the water that has been through the tube return hose


---
**Abe Fouhy** *February 24, 2017 01:15*

Interesting, you got that for $20?! Sweet deal. Looks like the milk jugs for me for awhile 


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/RqBHaF4Se3D) &mdash; content and formatting may not be reliable*
