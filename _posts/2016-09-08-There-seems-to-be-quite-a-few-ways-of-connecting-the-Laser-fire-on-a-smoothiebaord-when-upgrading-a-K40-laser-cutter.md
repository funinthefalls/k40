---
layout: post
title: "There seems to be quite a few ways of connecting the Laser fire on a smoothiebaord when upgrading a K40 laser cutter"
date: September 08, 2016 13:33
category: "Discussion"
author: "Anthony Bolgar"
---
There seems to be quite a few ways of connecting the Laser fire on a smoothiebaord when upgrading a K40 laser cutter. Can people please post their setups here so that I can make a document that will allow people to choose the method for thier upgrade.





**"Anthony Bolgar"**

---
---
**Scott Marshall** *September 08, 2016 14:29*

The 2 I'm familiar with are the "simple" drive:



This involves using a free pin (usually pin 2.4 or pin 2.6) driving a level shifter (the Smoothieboard and many other boards have a 3.3v native logic where the K40 PSU is 5v) and into the K40 LO circuit (pin 4 of thePSU power connector) - this is how my commercial boards work (using a 74LVC1T45 level translator chip from TI)



This method is straightforward, allows for PWM and has no disadvantages that I'm aware of.



And the "Dual Drive":



The other way is to use 2 outputs (opinions vary on the ones to use, but I doubt it matters much as long as there's no interference with other functions).

One output turns on the LO circuit, and the 2nd chops (PWM) the enable line on  the power supply.



PS, I've recently added a 15k pullup resistor to the Logic translator input.

During recent testing of a new gadget, I noticed that the laser can fire if the K40 is powered and the Smoothie has not or is in the process of booting. This occurs as the K40 PSU and ACR PSU "come alive" much faster than the Smoothie, and there is a period where the laser is ready to fire, but the smoothie has not yet booted and pulled the output pin high. During this time the Laser CAN fire at random. (line noise seems to be responsible - simply touching the wire will fire the laser) 



A 15k pullup resistor to the 3.3v ACR power holds the laser OFF during this 'limbo' period, making the whole affair much safer.



Keep this in mind while assembling your own fire circuit.



Scott


---
**Don Kleinschnitz Jr.** *September 08, 2016 14:41*

**+Anthony Bolgar** I was thinking the same thing. I suggest we post connection information and associated configuration information. 



My configuration (which is untested on my machine) but used by others:

*PWM connection: 

-- Port on Smoothie: P2.4

connected to:

-- "L" on laser power supply (the DC power connector)

*Other connections:

-- Lever shifter: not used

-- "IN" pin on LPS: not connected



*Associated configuration: 

-- laser_module_pin                             2.4             

 

*Functions and limitations:

-- Some report that the "Laser Fire" test switch does not work (mystery to me)

-- The max power can be adjusted with the Current Regulation pot independent of smoothie PWM.

-- PWM does not have a DC offset (as when using the IN pin), creates shift in actual power vs smoothie PWM.

-- No level shifter needed

-- Simpler wiring                                                           


---
**Mircea Russu** *September 08, 2016 15:08*

I suggest using the MOSFET outputs of the Smoothieboard and not using a level converter. These MOSFETS are not active at Smoothie boot and can not accidentally fire the laser. 

I currently use a 2 pin setup. 2 MOSFET outputs, one connected to IN with the pot still in place as a pull-up and the other connected to the L input of the LPS. 

I also used a single MOSFET output to the L pin doing PWM directly on the L pin, but reverted to the 2 pin setup without any actual reason. 

I mostly used the machine for cutting and after having some arcing sounds when engraving I tend not to engrave anymore, at least until I finish my cutting jobs I bought the machine for.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 15:50*

I can't comment much more than what Scott said, because he helped me with my setup, but I am using Pin 2.4! (inverted) in my config. Not 100% sure why, but without inverting it the laser was constantly firing. Scott knows for sure, but it's knowledge that is over my head.



**+Mircea Russu** Are the arcing sounds not just the material vaporising? I heard sounds like that a while ago in mine when engraving & placed a webcam in the tube compartment to monitor it. In my case there was no actual arcing & it turned out that it was the sound of the plywood I was engraving fizzling.


---
**Mircea Russu** *September 08, 2016 16:16*

**+Yuusuf Sallahuddin**​ I'm 90% certain it's arcing. It happens when quickly depressing test for alignment. Seems to come from the LPS. I shall get a camera to watch better. Never happens when cutting and that's what I use most. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/4BspKxMTupt) &mdash; content and formatting may not be reliable*
