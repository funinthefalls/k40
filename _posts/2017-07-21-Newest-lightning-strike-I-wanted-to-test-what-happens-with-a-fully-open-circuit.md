---
layout: post
title: "Newest lightning strike: I wanted to test what happens with a fully open circuit"
date: July 21, 2017 15:36
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40LPS



<b>Newest lightning strike:</b>



I wanted to test what happens with a fully open circuit. Notice the acrylic barrier in the gap.

This may be what was wrong with this supply?




**Video content missing for image https://lh3.googleusercontent.com/-QGGUzU6HZ04/WXIfYCOFJKI/AAAAAAAAqRw/0c758p76LWEIkV3vjyLea_1YZ_hhkzeaACJoC/s0/20170720_125037.mp4**
![images/d27325ba6dcc20d0b598d5204d49be05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d27325ba6dcc20d0b598d5204d49be05.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Nate Caine** *July 21, 2017 15:58*

Well that was interesting.  



What was the end of the HV lead doing at that time?  Any corona?

And how about the mA-meter.  (Both were just out of frame.)



Just another reason to be sure everyone's K40's have solid grounding.  (As shipped from the factory, they are shit.)



To aid in your experiments, **+Don Kleinschnitz**​​​ , you may want to put a parallel resistor across you mA meter.  That way, instead of 0-30mA you could change  the range to 0-300ma or even 0-3000mA.  (The resistor math is an exercise left to the student.)


---
**Don Kleinschnitz Jr.** *July 21, 2017 20:29*

**+Nate Caine** The HV lead was just connected to the black plastic post and it had no corona. The meter reads 5-10 ma. Yes I could configure a multi-range amp meter .... 



Grounding is critical and this amount of energy dictates an interlock on the laser compartment door ....


---
**Ned Hill** *July 22, 2017 01:54*

Whoa, would not have expected that to happen.  Very interesting.  Thanks for your work Don.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Dqb4Y8HGpmL) &mdash; content and formatting may not be reliable*
