---
layout: post
title: "Decided to capture the whole 2 hours it takes me to design a simple pattern, cut, and assemble a tea light candle holder"
date: April 10, 2017 02:39
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Decided to capture the whole 2 hours it takes me to design a simple pattern, cut, and assemble a tea light candle holder. Other patterns or designs can take longer of course. This was a simple one.





**"Ashley M. Kirchner [Norym]"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2017 03:09*

Thanks for sharing your process **+Ashley M. Kirchner**. I'm curious what it is that you are using for masking the entire sheet of ply?


---
**Ashley M. Kirchner [Norym]** *April 10, 2017 03:13*

[rtape.com - Conform® Series with RLA®](http://www.rtape.com/product-lines/conform-series-with-rla)


---
**Ashley M. Kirchner [Norym]** *April 10, 2017 03:16*

I get wide rolls so I can do this:

![images/a9a7bf242c68842c5554a92e2bb57dcd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9a7bf242c68842c5554a92e2bb57dcd.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2017 03:40*

Thanks for that. I've been looking around local hardware stores for something to mask with that is wider than 2" with no luck. Maybe something like that is a better option.


---
**Ashley M. Kirchner [Norym]** *April 10, 2017 03:46*

Note on that page that they have different strengths. I use the medium and low tack versions. And you'd have to ask RTape for a local distributor ... I get mine through the print shop I used to work at.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2017 04:12*

**+Ashley M. Kirchner** Thanks again. I'll see if they have a local distributor here.


---
**Ned Hill** *April 11, 2017 20:34*

Great video!


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Rrk86jBPEwN) &mdash; content and formatting may not be reliable*
