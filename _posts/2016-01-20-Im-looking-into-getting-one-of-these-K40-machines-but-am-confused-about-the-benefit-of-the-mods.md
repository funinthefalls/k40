---
layout: post
title: "I'm looking into getting one of these K40 machines but am confused about the benefit of the mods"
date: January 20, 2016 23:12
category: "Modification"
author: "Jonathan Tzeng"
---
I'm looking into getting one of these K40 machines but am confused about the benefit of the mods.



1. What is the purpose of installing the honeycomb?

2. What is the benefit of the different lens types/sizes? Can you quantify this?

3. I understand air assist helps keep the mirror clean. How much is needed to really improve cuts? I have a 45lpm pump, would this do any good?





**"Jonathan Tzeng"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 21, 2016 00:38*

As far as I am aware, the purpose of the honeycomb is to minimise the surface area on the back of the workpiece, thus minimising the possible laser reflections/marks on the backside of the workpiece.



I can't comment on the different lenses as I haven't changed mine.



Air assist also helps keep the workpiece free from catching on fire, minimising charring on the edges. I am using just a small air-pump that is equivalent to a fish-tank air-pump. It is not much pressure, so I have it focused through a ball inflation needle to increase that. As far as the air assist goes, most important thing (in my opinion) is to focus the airflow to right where it is cutting. More airflow means less burn & better dispersal of the smoke away from the workpiece.


---
**3D Laser** *January 21, 2016 00:57*

From what I have been told the lenses on this machine are not the best and can be replaced relatively cheap (less than 100 dollars).  I have plans to upgrade mine with an Air assist when I get it and a new Ramps control board when it comes in.  Another Option is a smoothie board but someone game me a Ramps for free so I am going that route. 



From what I have been told upgrading the board on these machines should be a priority as the software is not the greatest is not that great.


---
**David Cook** *January 21, 2016 20:25*

I'll say that changing the board was the best mod to do first,  But some people are making great things with the stock setup to.   


---
*Imported from [Google+](https://plus.google.com/111839747151212088406/posts/hZvbX4LTH3z) &mdash; content and formatting may not be reliable*
