---
layout: post
title: "Right I know this is a how to question about the Smoothie board but I can not find the answer, possibly because I have the older moshi board installed at the moment"
date: November 10, 2016 22:37
category: "Original software and hardware issues"
author: "Andy Shilling"
---
Right I know this is a how to question about the Smoothie board but I can not find the answer, possibly because I have the older moshi board installed at the moment. 



When I upgrade to the Smoothie will I have to replace my psu and everything else inside the control box? All images I see have one psu and the Smoothie only inside. I'm presuming this board is for the high voltage end of firing the laser and if so will I remove this.

![images/08c6629a3725d1b16d7367245ae75b60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08c6629a3725d1b16d7367245ae75b60.jpeg)



**"Andy Shilling"**

---
---
**Stephane Buisson** *November 11, 2016 07:15*

**+Andy Shilling** agreed with you change the PSU. the one pict here is the very old model (not specially reliable from my readings, and not sure it manage pwm). so you take all off and just keep motors and end stop. connect them on smoothie and new PSU.


---
**Andy Shilling** *November 11, 2016 21:26*

![images/99d5e725be824a3a245c681b6d20423e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/99d5e725be824a3a245c681b6d20423e.jpeg)


---
**Andy Shilling** *November 11, 2016 21:26*

![images/d382c7a9f6130cc1d6fbdf59c2187f03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d382c7a9f6130cc1d6fbdf59c2187f03.jpeg)


---
**Andy Shilling** *November 11, 2016 21:27*

![images/fb7185463a0834bfe9d99507d2766a38.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb7185463a0834bfe9d99507d2766a38.jpeg)


---
**Andy Shilling** *November 11, 2016 21:27*

![images/b0c04b90119b7cced8c6a368d810903e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0c04b90119b7cced8c6a368d810903e.jpeg)


---
**Andy Shilling** *November 11, 2016 21:28*

Sorry for the multiple uploads like this but I couldn't work out how to do them all on one post. 



This is my current psu etc. Thanks for your help guys 


---
**Stephane Buisson** *November 12, 2016 16:19*

example of new PSU, it's 2 type of connectors out there, the white one or the green one (screws). the green is more handy because you don't need a tool for your connectors, both do the same jobs.

[http://www.ebay.com/itm/new-40w-co2-laser-power-supply-ac-110V-115V-220V-240V-laser-engraver-cutter-us-/201713863425?hash=item2ef7154b01:g:dTAAAOSwmLlX2Bt~](http://www.ebay.com/itm/new-40w-co2-laser-power-supply-ac-110V-115V-220V-240V-laser-engraver-cutter-us-/201713863425?hash=item2ef7154b01:g:dTAAAOSwmLlX2Bt~)


---
**Andy Shilling** *November 12, 2016 16:38*

So if I buy the new psu I can then get rid  of everything else and just have that and the Smoothie in the control bay? 


---
**Stephane Buisson** *November 12, 2016 20:17*

yes, just that, as you say

with eventually a level shiter at 2usd.


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/9Sjd2V3tFyF) &mdash; content and formatting may not be reliable*
