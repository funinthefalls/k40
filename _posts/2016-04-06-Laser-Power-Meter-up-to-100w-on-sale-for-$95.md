---
layout: post
title: "Laser Power Meter up to 100w on sale for $95"
date: April 06, 2016 09:09
category: "Discussion"
author: "Phillip Conroy"
---
Laser Power Meter up to 100w on sale for $95[http://www.bell-laser.com/#!product-page/c1iym/1cd1a226-cf6b-7b63-26a7-2d551eb6821a](http://www.bell-laser.com/#!product-page/c1iym/1cd1a226-cf6b-7b63-26a7-2d551eb6821a)

![images/9c3be10b7eb138c4663e58000881b7dd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9c3be10b7eb138c4663e58000881b7dd.png)



**"Phillip Conroy"**

---
---
**Scott Marshall** *April 06, 2016 09:38*

Looks like a pretty decent probe for the money. Thanks for the heads up.



Despite my passion for cool tools, it's hard to justify it for my one laser that I KNOW isn't putting out 40w. Since there's little I can do about it anyway, I'll save the hundred bucks for when my tube dies....



Too bad we're spread rather evenly over the whole planet, It would be worth having if we could mail it around and share. Say 10 bucks gets you in the co-op and we mail it around to all the members. measure you laser and pass it on. A high voltage probe would be another rarely used, but no substitute tool that would be good to 'rent'.



Not practical, just thinking out loud..



Scott


---
**Phillip Conroy** *April 06, 2016 10:42*

I know it js not pratical ,however  i used to work in the air force as an electronics tech and we had to measure just about everyting on radars ,comms gear etc i just like to know as much about lasers as i can .i use my k40 for 3hours a day and need to know how the tube is going  so i can order a replscement just before the old one dies.i read some where iv you store a unused tube for more than 6 months it will be bad for [it.it](http://it.it) would also be good to know how much power  is lost at each mirror.


---
**Stephane Buisson** *April 06, 2016 12:27*

**+Phillip Conroy** if you proceed, please let us know.


---
**Phillip Conroy** *April 06, 2016 12:45*

i have ordered one bugger us to au exchange rate cost me $165 au dollars with post


---
**Scott Thorne** *April 06, 2016 14:35*

**+Phillip Conroy**...I had to take advantage of it also...can't wait until it arrives...I'm curious to see the output of the puri tube.


---
**Loïc Verseau** *April 06, 2016 15:17*

I'd like to try that. 


---
**Jim Hatch** *April 06, 2016 16:49*

**+Scott Thorne**​ I ordered one too :-) Part of it is the geek in me but I'm planning on getting another larger laser this year and would like to be able to correlate my logbook of settings for the K40 and projects to the new one so I don't have to retest everything all over again (exhaustively anyway).


---
**Scott Thorne** *April 06, 2016 17:02*

**+Jim Hatch**....lol...we are all geeks!


---
**Loïc Verseau** *April 06, 2016 17:05*

Stephane, j'en ai acheté un pour tester mon laser (cf: le sujet plus bas). Je pense pas en avoir besoin toute ma vie donc quand j'en aurais plus besoin, j'contacterai arthur ;)


---
**Scott Thorne** *April 09, 2016 00:26*

**+Jim Hatch**...I got a email today saying that the power meter will be sent out next week...they say that they sold way more than they expected to sell. 


---
**Phillip Conroy** *April 13, 2016 09:22*

My power meter arrived today so far tested 10ma =15watts at just after the first mirror,could not get directly to tube as i have eveything taped up to prevent air leaks,will test at full power tom orrow[20ma] and after each mirror-power meter is not rated for the focal lens so will remove for tests


---
**Scott Thorne** *April 13, 2016 10:45*

I've emailed them...waiting on a reply or it to arrive in the post.


---
**Scott Marshall** *April 13, 2016 11:18*

Just measure before the last mirror, (left of the head) it's loss is under 1%, you won't have to take the head apart that way, and I doubt you could see a difference between the readings. Those thermal meters are +- 10%, at least the last one I used was. That was a probe type that plugged into a DMM - and 15 years ago.



That's 150 watts input power giving 10% efficiency,  exactly what I guessed last December, and the number I've been using for cooler calculations. That was a very lucky guesstimate.

 15kv x .010 = 150W. 

Expect 20 Watts @ 20ma (200w input) and if you're willing to put the hammer down, mine will do 29ma, let's call it 30 because we can, 

that's 30w max, with 300w input power.



From the cooling standpoint, that is (300w-30w) 270watts we need to carry away, plus the heat put in by the pump, guessing about 60w for a stock pump, so about 330w we need to get rid of. 



330w x 3.41 = 1125 BTU



In easy form:



5ma = 50w input = 5w output, 153 btu heat generated

10ma =  100w input = 10w output, 306btu heat generated

20ma = 200w input = 20w output, 613btu heat generated

30ma = 300w input = 30w output, 920btu heat generated



I'm measuring vicariously thru your meter, Been wondering what a stock K40 can do. Yours has a stock tube?



Thanks for the info, it's useful, I'm working on a low buck cooler system for those who want to avoid the expensive  CW chillers on ebay.

Drawings out soon. This helps a lot.



Thanks, Scott


---
**Phillip Conroy** *April 13, 2016 14:42*

yes mine has a stock tube,i use a undersink water chiller that was 20 dollars second hand and run it untill water temps hit 15 c than turn off until water is at 20 c than turn on again,i have programable themostats however havent got around to setting them up [just using to read temps].the cw3000 on ebay is just a water radator and not a refridurated workings,water chillers are expencive compared to a bar frifge at less than 200 au dolars,would love to see what path u use to cool your setup


---
**Scott Marshall** *April 13, 2016 15:39*

Good deal on the chiller! Wish I could find one for 20 bucks



I agree the Cws are a waste of money. I've got a $15 Transmission cooler on the right side of mine with the inlet air for the exhaust drawing thru it, keeps up fine in winter, but probably won't when it's 95 in the shop. If I was ambitious I'd dig a pit and make a heat pump type loop from copper tubing. 25' of 3/8 copper buried 3' down would cool a pretty big laser here in the northeast US.

I'm calling those options stage 1 and 2.



Stage 3 of the  system is as simple as a 2nd Temp controller and a glycol loop thru your family freezer - a couple bulkheads in thru the back and you (or the spouse) won't even notice the loop of tubing in the back of the freezer. If that's impractical (or voted down by the head of the house), a cooler with ice/blue ice/dry ice would run it for several days at a filling.



Stage 4 is a converted garage sale or $100 5k BTU window AC unit with the evap coils in a tank with the loop from stage 3 and some custom controls.



I'm almost done with a full set of CAD drawings for a full set of easy upgrades including the cooler options, Interlocks, and auto air/exhaust controls. Several people (ILaser,YS etc) have expressed interest. I may sell parts kits for individual portions or complete controls upgrade if there is an interest.



I used to do this stuff for a living, forced to retire, so now I dabble. Hope somebody can use some of what I throw out.



Thanks for the free test data, budgets tight or I'd buy my own.



Scott


---
**Jim Hatch** *April 23, 2016 22:40*

Got mine in this week and did a quick test today. At 17ma I got 31 watts after 40 seconds testing after the fixed mirror. The instructions say to keep the probe in the unfocused beam for 40.5 seconds but the probe itself says 30 seconds. I went with the instructions. 


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/ABwuV2YnXEJ) &mdash; content and formatting may not be reliable*
