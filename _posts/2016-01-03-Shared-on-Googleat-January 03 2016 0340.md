---
layout: post
title: "Shared on January 03, 2016 03:40...\n"
date: January 03, 2016 03:40
category: "Discussion"
author: "ChiRag Chaudhari"
---


![images/e2f24f805bd2fb7b1920a57dc10178a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e2f24f805bd2fb7b1920a57dc10178a6.jpeg)



**"ChiRag Chaudhari"**

---
---
**ChiRag Chaudhari** *January 03, 2016 03:43*

Gonna turn back storage room at the store into workshop. Spent 3 hours installing outlets. Now need to drill a biggas hole in back wall for exhaust.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 03, 2016 03:52*

Is it your store? That will make for a good way to pass time while waiting for customers/work to do.


---
**Scott Marshall** *January 03, 2016 07:02*

I remember when my shop looked liked that... Around 1995 or so.  All that space just waiting for cool machines (and junk).



Have Fun, Looks Good!


---
**ChiRag Chaudhari** *January 03, 2016 07:20*

I have 2800 sqft store front, then about 6000 sqft of storage behind it, and then there is another 4500 sqft storage behind that. The bigger storage is of course for liqor inventory, but the 4500 all the way in back used to be my 30 yards air gun range. Which I haven't used in last two years. 

Suddenly today I thought let me move my workshop here for the bigger and more messy tools. So yeah its WIP but the laser cutter is plugged in. Oh and found an old frame I can use as a stand for it. Just need wheels for that now. The hardest part is going to be to make hole for exhaust in concrete wall. 



Would love to see some of you guys setup to get more ideas. 


---
**Jim Hatch** *January 03, 2016 14:52*

Well the good news is you really don't need a 4" hole if you're using the stock blower. Just look at its intake - 4 sq inches or so. A 2" hole would be fine. The bad news is the stock blower doesn't pull enough air - better air evacuation would remove smoke and fumes faster and help keep the work pieces cleaner. Removing the little air scoop and/or replacing the blower with a higher rated fan (mine is only rated for about 1.5m3/min).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 03, 2016 15:34*

**+Jim Hatch** Is the 1.5m3/min decent output? I am in need of extra air evacuation also & am curious what sort of airflow I should be aiming for.


---
**Jim Hatch** *January 03, 2016 19:33*

I just checked mine - it's rated at 2.5m3/min. The smoke from cutting birch plywood rises straight up before dissipating so it's doing something but no real airflow is felt at the end of the hose. I have sealed my blower with foam rubber gasket so it's not escaping around the opening of the large plastic housing.



I've seen people put in a pair of Corsair SP120L computer fans - those are rated at 73cfm. The 2.5m3/min fan on the K40 is 87.5cfm so a single Corsair has lower output. 



Right now I think the limiting issue is the intake side of things - there's only that 2" hole (and leakage around the lids) in the bottom that can provide for replacement air for any exhaust air. Plus the restriction of the air scoop (measured again - 8"x3/4" or 6sq inches of cross sectional area) suggests that it could be as much an issue as the output/fan side of things.



Not sure what would be optimum (I'm something of a newbie with the K40 vs big Epilogs with far more robust air & exhaust capability). I'm going to see about taking out the internal air scoop and see what that does before opening up a vent into the front of the machine to provide a direct air path vs trying to get more air from the hole in the bottom (which gets blocked by the work piece anyway). I expect the scoop and front vent are the two biggest potential benefits with a new fan possibly needed.



The volume of the cutter is less than 3.5cuft which says the stock fan should be evacuating the box every 2-3 seconds. That's certainly not happening so I'm thinking it's an intake issue more than an exhaust issue.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 03:12*

**+Jim Hatch** Thanks for that Jim. I was also considering using 120mm PC fans for intake & exhaust. I thought something in the range of 100cfm each and possibly 1-2 at front (intake) & 1-2 at back (exhaust). I was thinking that it is equally important to have airflow below the piece being cut as above it (as I've noticed marks on bottom of cut pieces & also smoke swirls up the front of my machine from beneath the piece).


---
**Jim Hatch** *January 04, 2016 03:14*

You'll get staining on the bottom from both the smoke & the flash from whatever you have supporting the piece - that's why honeycomb aluminum is used so often for the bed.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 03:30*

**+Jim Hatch** Yeah, I'm not too concerned about the staining on the bottom, as it's usually leather I work with & the bottom is the raw side of leather that is hidden a lot of times or lined with other materials. It's the smoke that decides to swirl back to the top & stain the top of the leather that bothers me. I think the exhaust funnel causes problems with the extraction (i.e. severely limited the exit size which renders the stock exhaust fan near useless). I too will remove it (when I get around to it, haha).


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/Y9cgRM2Nyvs) &mdash; content and formatting may not be reliable*
