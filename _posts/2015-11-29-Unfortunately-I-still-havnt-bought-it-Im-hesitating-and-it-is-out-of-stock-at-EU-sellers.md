---
layout: post
title: "Unfortunately I still havn't bought it, I'm hesitating, and it is out of stock at EU sellers..."
date: November 29, 2015 17:01
category: "Materials and settings"
author: "Akos Balog"
---
Unfortunately I still havn't bought it,  I'm hesitating, and it is out of stock at EU sellers... :(

Maybe can someone try to punch holes and make some cuts on some nylon/polyester webbing, if you have some, that you don't need? 



I want to be able to produce holes and cut like on my pictures. Do you think it is possible with this machine?

(Unfortunately I'm not even sure that it is made by laser.)



![images/e9a93a2ebc6fc73d7e4bdba46102d3e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9a93a2ebc6fc73d7e4bdba46102d3e5.jpeg)
![images/747f9c9cb03d54e1b7787de72e114c8b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/747f9c9cb03d54e1b7787de72e114c8b.jpeg)

**"Akos Balog"**

---
---
**Scott Thorne** *November 29, 2015 17:48*

What is the size of the hole....in mm.


---
**Akos Balog** *November 29, 2015 17:50*

2.5mm diameter. The webbing is 1.45 mm thick


---
**Scott Thorne** *November 29, 2015 17:52*

I'm sure it will....it will punch through 3/16 plywood like a hot knife through butter.


---
**Akos Balog** *November 29, 2015 17:55*

It is not a question whether it can cut or not. The question is whether is would seal with the same quality,  and whether it will build up unwanted melted, and thick ridge/edge or not.﻿


---
**Akos Balog** *November 29, 2015 18:07*

To add, it is also a question whether the melting, edge build up can be controlled properly by cutting speed, power, power of air assist or not?


---
**Scott Thorne** *November 30, 2015 00:50*

I tried it out on a nylon belt almost 3mm thick and it cut without melting perfectly...a few squares and a few 2mm holes.


---
*Imported from [Google+](https://plus.google.com/106984452297752036237/posts/G8dcj53UzMk) &mdash; content and formatting may not be reliable*
