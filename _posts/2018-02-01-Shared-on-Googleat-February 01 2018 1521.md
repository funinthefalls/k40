---
layout: post
title: "Shared on February 01, 2018 15:21...\n"
date: February 01, 2018 15:21
category: "External links&#x3a; Blog, forum, etc"
author: "Frederik Deryckere"
---
[http://www.manmademayhem.com/?p=3192](http://www.manmademayhem.com/?p=3192)





**"Frederik Deryckere"**

---
---
**Don Kleinschnitz Jr.** *February 01, 2018 17:44*

Very well done!


---
**Josh Hess** *February 01, 2018 18:03*

This is amazing. I expect I'll be building one once you have plans or a part list put up.


---
**Jim Hatch** *February 01, 2018 18:16*

Nice job!




---
**Ariel Yahni (UniKpty)** *February 01, 2018 22:23*

Indeed very nice


---
**Frederik Deryckere** *February 02, 2018 09:29*

thx!


---
**Anthony Bolgar** *February 03, 2018 22:51*

Fantastic! 


---
**Mitch Newstadt** *February 08, 2018 15:08*

waiting for the parts list myself... I've been looking at doing much the same thing...   I am wondering why the air assist is a challenge... I got an aquarium pump and a [lightobject.com](http://lightobject.com) 18mm Laser Head w/Air Assist. for a total of about $60 including the tubing and shipping.. (probably puts you over your budget but not hugely)


---
**Don Kleinschnitz Jr.** *February 08, 2018 15:39*

**+Mitch Newstadt** I found that an aquarium air pump (at least the one I had) did not create enough air pressure ...


---
**Frederik Deryckere** *February 08, 2018 16:30*

only reason is budget. I wanted to stay within the proposed 500€. Apart from an air pump you'd need some tubing, extra drag chain + bracket etc. All of that is peanuts really but small things add up.



But don't worry, air assist is coming as an optional upgrade, which probably most people would want.




---
**Frederik Deryckere** *February 08, 2018 16:31*

I'm currently building the frame.

![images/8380b5f609e220925001203361d16b38.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8380b5f609e220925001203361d16b38.jpeg)


---
*Imported from [Google+](https://plus.google.com/111378701553042836125/posts/QYZXUbfbivy) &mdash; content and formatting may not be reliable*
