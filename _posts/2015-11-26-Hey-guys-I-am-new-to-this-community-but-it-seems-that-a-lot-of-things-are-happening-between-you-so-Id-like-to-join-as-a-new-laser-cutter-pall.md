---
layout: post
title: "Hey guys! I am new to this community, but it seems that a lot of things are happening between you so I'd like to join as a new laser cutter pall!"
date: November 26, 2015 15:28
category: "Hardware and Laser settings"
author: "Tadas Mikalauskas"
---
Hey guys!



I am new to this community, but it seems that a lot of things are happening between you so I'd like to join as a new laser cutter pall! Ofcourse I have a lot of problems with my new K40 laser and I have solved a lot of them. Unfortunatelly not all, so maybe some of you might help. I'll try to give as much info about what is going on with my machine and add some of the pictures in the comments below to help visualise all.



First of all I've done some adjustments like removing the bed and making it my own with metal grid to allow a proper smoke flow from below the material.



I've got a 3D printed add on the head that holds 2 red beam lasers to point where the laser hits (using 2 so when both of them crosses in one spot at the right focus the co2 laser hits the corrects spot). Also it has an air assist.



Seems that all other additions should not affect the work of laser like light so I'm not gonna start on that.



PROBLEMS:



Sudden Power Drop.

We were having a pretty poor power for a few days we were setting everything up, but didn't actually know how powerfull it should be. Then we noticed one problem (listed below) and decided to check all mirrors realignment and found that the very first mirror was a bit off and realihned all of the mirrors once again and WOLA! A lot more powerfull beam. It all happened late night so today I got up, started the laser. First few cuts were made at 10mA 10mm/s and cut 1.5mm plexiglass (acrylic sort of) well, in one cut, all is fine and we are quite happy BUT.. by checking the cut item we've noticed that the edge was not straight down vertical and the cut was quite fat so we've figured it must be due to focus. So we got the table down so now between the top of the plexiglass and the laser lens is about 58mm (as it should with this type of lens). We stayed with the same power and speed and it couldn't cut through the crylic now. We boosted the power, lowered the speed and the same.. We tried raising the material back up and whatever we do we can't get through 1.5mm acrylic with any power or any speed even going twice. Triet cleaning lenses, checked the allignment (you never know when you've done something). Tried removing the head with pointing lasers and air assist and still the same... Checked the water temperature but it seems to be ok, 17 degrees C. Maybe somebody have had the same issue and might think of something that can be causing the problem? In the picture you can see the first ones to be cut properlly and then tone of others that failed to cut.



One more problem we have is if we cut acrylic there is a tiny laser beam spill and for example if we cut a star, there is an extra slight star to the top left of the original cut. The spill is really weak, but it is enough to melt very slight layer of the acrylic and ruin the piece we are trying to cut. We figured it might be solved by putting a papertape on top of the piece before cutting, but c'mon, these things should not be happening. And in the pictures with power drop you can see a lot of papertape to avoid this. 



So at the moment the biggest problem is a sudden power loss and not figguring what the hell happened. Also all of the mirrors seems clean and without scraches so it doesn't seem to be a problem.



If anyone of you might think of a problem I would be EXTREMELLY thankfull!



Regards,

Tadas.

![images/8e275d91e74e8370666ceecb16f1dc25.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e275d91e74e8370666ceecb16f1dc25.jpeg)



**"Tadas Mikalauskas"**

---
---
**Tadas Mikalauskas** *November 26, 2015 15:30*

It doen't seem that I am able to add photos to the comments..


---
**DIY3DTECH.com** *November 26, 2015 15:52*

Does it lose power at the same location on the bed each time?  If yes the mirrors are likely out of "square" and please note the use of the word square and not alignment.  As I have similar issues as in that if I work in the original area its fine.  However I too enlarged my bed and the further I move away the weaker the beam.  Got some pointers from Scott Thorne on this form about squaring it up and it has improved.  However not perfect, per his post he invest two days and I have only done 20 minutes so you get the idea.  Also from looking at your picture the top cut and the bottom (especially to the right) didn't?  As that was the same issue I have.  As Marc G pointed out in another post, these are sold as rubber stamp engravers in china (they must need a lot of rubber stamps) and the tolerances seem to degrade over larger areas.  Also check out a few of the videos I posted on finding the focal point of your lens as I discovered mine was 1 CM off from what was advertised and has significant aberrations ( which means I ordered a new one).


---
**Coherent** *November 26, 2015 16:01*

Split or multiple laser beams are likely caused by dirty mirrors or lens and should be your first inspection. Additionally if the alignment is out or  a beam is hitting the very edge of a fixture it can cause reflection issues. Of course a cracked burnt or damaged (or defective) mirror or lens could also be the culprit. .


---
**Tadas Mikalauskas** *November 26, 2015 16:48*

The first problem with a power loss actually IS due to beam being out of square! Aftem my post I came back to the mashine and worked it out (most of it) here you are proving my theory! Thank you very much! 



And for multiple beams, I think I will try to buy new mirrors because the last mirror looks like it could be a lot better. Still thank you guys a lot!


---
**Gary McKinnon** *November 26, 2015 17:03*

Many people with older machine have power issues unless the machine is properly grounded using the post at it's rear.



I don't know enough about electronics yet to know how to do this, but i read about people driving 8ft copper pipes into the earth and running a wire, no way i want to do stuff like that.



If you know your electronics you can wire the ground properly internally, i think, without going into the garden !



I did find a post on it that i can't find now but i'll continue to look for it.



Welcome to the group :)


---
**Gary McKinnon** *November 26, 2015 17:05*

Also, laser power drops off over distance travelled, i've read that going beyond the manufacturer's bed are can result in non-vertical cuts, thick cuts etc since you lose power the further away you are as the beam is lengthened.


---
**Gary McKinnon** *November 26, 2015 17:05*

Annoying that we can't post images in replies :(




---
**Gary McKinnon** *November 26, 2015 17:07*

Some info on grounding :



[http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/138554-software.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/138554-software.html) 


---
**Scott Thorne** *November 26, 2015 17:21*

Check the squareness of your x,y rails...most people's are off a few teeth on the belts that drive the carriage and they don't even know it....if it's of then you won't ever be able to align the head with the mirrors, resulting in serious power loss....trust he....I've been through this.


---
**DIY3DTECH.com** *November 26, 2015 19:51*

Scott can you explain more on the "few teeth off" part?    Apologies for being a bit dense, however I am not quite understanding?  As on mine, the X & Y are fixed and I put a machinist square on (on the inside corners) it and it was dead on, however the way is was bolted into the sheet metal chassis was not and since the "first mirror" is anchored to the chassis, as the second mirror moved away it did so at an angle (as the x/y gantry) was off around an 1/8th of inch or little more. So loosening it and moving helped (still not perfect).  Also ordered that new lens today :-)


---
**Scott Thorne** *November 27, 2015 13:09*

If the rail that the head is mounted to is out of square, the only way to square it up is to loosen one of the belts that drive it and jump belt teeth around the drive sprocket to get it square with the stationary rail.


---
**DIY3DTECH.com** *November 27, 2015 18:30*

I see what your saying now Scott about the teeth.  I would thought it would bind up being that far out I built a DIY 2 Watt laser and it uses two Y drive steppers (you can see video on my Youtube channel) and if gets out it makes noise.  Hmmm will have to take a look as I was view his different...


---
**Scott Thorne** *November 27, 2015 18:44*

Mine was it so bad that I couldn't get the laser to fire in the hole in the head when it was extended all the way to the right...it was off over an 1/8 of an inch, no matter what I did I couldn't get it right until I squared up the rail with the head on it.


---
*Imported from [Google+](https://plus.google.com/113213933687083540785/posts/cjbg3wDHwo6) &mdash; content and formatting may not be reliable*
