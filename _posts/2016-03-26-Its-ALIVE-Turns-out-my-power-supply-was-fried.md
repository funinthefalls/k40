---
layout: post
title: "It's ALIVE! Turns out my power supply was fried"
date: March 26, 2016 02:40
category: "Discussion"
author: "3D Laser"
---
It's ALIVE! Turns out my power supply was fried.  Got a new machine for parts and now I am back in business 

![images/1724e883d6917cbf2d3f8e0653fb7592.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1724e883d6917cbf2d3f8e0653fb7592.jpeg)



**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 26, 2016 04:38*

I can imagine combining the two machines together to provide a larger work area, since you have all the extra rails/stepper motors, etc. Bit of grinding on chassis & welding the two together... Hey presto!


---
**3D Laser** *March 26, 2016 04:46*

Honestly I would love to slave the second machine to the same board so that they are mirrored is it possible to split the signal coming from the board to two different machines 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 26, 2016 04:56*

**+Corey Budwine** We recently had a similar discussion on another post (can't recall which one) & if I recall correct "I Laser" mentioned something about using 2 machines from the same computer, but had to use a virtual machine to run the 2nd machine. I'm think it may be possible to run both machines off the same board, but I'm not sure if the board would be capable of powering both machines correctly.


---
**Anton Fosselius** *March 26, 2016 05:51*

In a perfect world it would work, but in reality, the endstops would be braking it. you would have to use custom electronics/software.


---
**Alex Krause** *March 26, 2016 07:26*

**+Yuusuf Sallahuddin**​ I was following that post it requires two instances of the software because of the two boards were identical and required the dongle or something like that to run I remember reading if the main boards wernt identical it would be possible without VM ware 


---
**Anthony Bolgar** *March 26, 2016 11:58*

If you upgrade it to a Smoothieboard, or Ramps/Marlin, you can run as many machines as you want using LaserWeb




---
**Vince Lee** *March 26, 2016 14:38*

I think this should be straightforward.  You can use a slave controller to drive the motors of both units from the same motor signals.  You can hook up ssr"s to split the beam enable in a similar way. You would just have to home one of the cutters manually.  [http://reprap.org/wiki/Build_A_Stripboard_Bipolar_Slave_Stepper_Controller](http://reprap.org/wiki/Build_A_Stripboard_Bipolar_Slave_Stepper_Controller)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/dVbb6RP5VkQ) &mdash; content and formatting may not be reliable*
