---
layout: post
title: "While working on K40 conversion, I have a question related to the Ampermeter connection"
date: August 21, 2016 14:16
category: "Modification"
author: "Sebastian Szafran"
---
While working on K40 conversion, I have a question related to the Ampermeter connection. I plan to connect it between the L- and the yellow wire (yellow wire originally goes from L- to the laser tube, so I will disconnect this wire from L- and connect Ampermeter between L- and wire).



Another question: some builders recommend to cut (shorten) the stock metal part for air exhaust (see picture). What is the best length to be left after cutting? 



![images/cf39dc1564ddccc9bdf1c66ddef49939.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf39dc1564ddccc9bdf1c66ddef49939.jpeg)
![images/9ba167b182cc484a296dffacf2940b7a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9ba167b182cc484a296dffacf2940b7a.jpeg)
![images/249f6544f7cad4c2a187df45bc9294d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/249f6544f7cad4c2a187df45bc9294d6.jpeg)

**"Sebastian Szafran"**

---
---
**Ariel Yahni (UniKpty)** *August 21, 2016 15:06*

I removed my exhaust completely. Mind that for that you need to take the whole gantry, at least in mine. I now fit 12x12 cut parts and a little more


---
**HalfNormal** *August 21, 2016 16:18*

The amp meter needs to be in series/in line with the circuit to work correctly. Just make sure that is still the case when you rewire. 


---
**Sebastian Szafran** *August 21, 2016 18:49*

This is exactly what I am going to do, connect the Ampermeter to L- and to the laser tube negative.



My stock K40 did not have Ampermeter installed, just a current regulator with three 7-segment displays allowing to maintain current percentage (+/-10, +/-1 and +/-0.1% of max laser power). 


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/jfYNC849GDv) &mdash; content and formatting may not be reliable*
