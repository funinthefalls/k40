---
layout: post
title: "So I screwed up and accidentally deleted my post (and Kelly Burns reply) while trying to replace the image"
date: May 12, 2018 16:40
category: "Discussion"
author: "Jake B"
---
So I screwed up and accidentally deleted my post (and **+Kelly Burns** reply) while trying to replace the image.  Thanks **+Kelly Burns** for your reply.  Here's my updated image (which corrects an error in my previous sketch.



I'm setting up my Z-axis this way, and this is how **+Kelly Burns** has/had it setup as well.  (Unfortunately you can't see the reply because I deleted it accidentally)



Original Questions With Kelly's Answers:

Two questions:



Q1. What is the convention for the coordinate system of the Z-Axis?  Is it as I have sketched below? I ask because I've seen the opposite on YouTube.



A1: I set it up like you have, but I believe different commercial machines have it the opposite. 



Q2. Is Z=0 typically the top of the workpiece or is it the top surface of the Z-stage?



A2: Z=0 remains constant based on what you have drawn. Others may interpret it differently, but Z-Offset is set from the current work piece to be equal to the desired Focal Distance. 



I stopped using my homemade Z-Table a while ago, but now that I'm using Lightburn and it has a feature to raise/lower table with each pass for multi-pass cutting, I think I'm going to incorporate it again.





![images/038def4a08de81117a02b5cf09ee1a2c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/038def4a08de81117a02b5cf09ee1a2c.png)



**"Jake B"**

---
---
**Kelly Burns** *May 12, 2018 19:13*

I think you have the basic idea correct, but since Z is a position,in theory,  like X and Y you never have a negative Z position.  Especially if you have either Soft or Hard limits configured.  The issue is the Z-Table moving vs the head moving up and down.  IMO, this confuses things. I'm ADHD and go back and forth between CNC, 3D Printing and Laser. Including building all 3.  Trying to apply concepts of 3D Printing and CNC originally using Firmware intended for 3D Printing causes me to adjust concepts so that I can keep it all straight in my head.
































---
**Kelly Burns** *May 12, 2018 19:14*

Ill try to post some drawings I have when I get home later.  If not, message me and I will send them along. 


---
**Jake B** *May 12, 2018 19:28*

Yeah, I have the same problem with multiple machines.  Negative-Z might make sense on a CNC mill when you find the top of a workplace, it doesn't really make sense for a laser.  It would put the Focus below the surface of the table.  I guess if you were trying to do something with defocus.... but I just include it here for reference.


---
**James Rivera** *May 12, 2018 23:20*

I have several 3D printers. My Printrbot has a z axis that starts at zero when the nozzle touches the bed and z increases as the gantry goes up. I have another one (similar to an Ultimaker) where the z increases as the platform goes down. Basically, the farther away from the tool head, the greater the z number. With the laser, I like to think of an elongated “X”, and the center of the X, where the two lines cross, is the focal point of the laser and also halfway through whatever you are trying to cut. Not sure if this helps, but hopefully it at least clarifies the desired behavior.


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/bDKZKUzYBL3) &mdash; content and formatting may not be reliable*
