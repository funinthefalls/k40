---
layout: post
title: "Anyone know where I can buy some HV wire to extend the wire from my LPS to the tube?"
date: October 03, 2017 19:30
category: "Material suppliers"
author: "Anthony Bolgar"
---
Anyone know where I can buy some HV wire to extend the wire from my LPS to the tube?





**"Anthony Bolgar"**

---
---
**Michael Audette** *October 03, 2017 19:53*

I would route a whole new piece.  Mouser sells it under "Hookup Wire"  but you might have to buy 50-100 feet. Amazon sells shorter lengths (~15 USD for 32.8Ft 22AWG 40KV) I can't recall what the KV rating is - but I wouldn't splice that.


---
**Don Kleinschnitz Jr.** *October 05, 2017 21:41*

I got mine at eBay. You can also use sparkplug wire. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ZWg4MsH8ogS) &mdash; content and formatting may not be reliable*
