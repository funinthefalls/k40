---
layout: post
title: "I have upgraded my laser, have Power Supply, Moshe board, gauge ,posimeter?"
date: April 27, 2016 20:32
category: "Modification"
author: "Jerry Martin"
---
I have upgraded my laser, have Power Supply, Moshe board, gauge ,posimeter?



Make me a reasonable offer!





**"Jerry Martin"**

---
---
**ThantiK** *April 27, 2016 20:37*

The garbage is a great place for these.  (Well, except the power supply)


---
**Joe Keneally** *April 27, 2016 20:52*

Can you post a pic of the power supply?


---
**Lori Martin** *April 27, 2016 22:18*



I will add it as soon asI can figure how!


---
**Lori Martin** *April 28, 2016 02:32*

**+Joe Keneally** 



Posted picture see Lori Martin


---
**Scott Marshall** *April 28, 2016 16:43*

Does the Moshi board have the flat cable connectors on it?, if so, how cheap will you let it go? (just want connectors for Smoothie conversion), and finally are you in US (or within mailing range for a reasonable price)



I've not found 'middleboards" yet, and would like to avoid cannabalizing  my  (Gen 2) existing board



Scott



Oh yeah, what's a posimeter?


---
**Anthony Bolgar** *April 28, 2016 19:17*

Middleman boards are a bout $10 for a set of 3 bare boards, connectors are about 50cents each.l


---
**Jerry Martin** *April 29, 2016 00:51*

**+Joe Keneally**



I have posted a picture of the power supply. It doesn't even have an hour time on it.


---
**Jerry Martin** *April 29, 2016 00:52*

**+Scott Marshall**



I can send you a picture of the board if you want.


---
**Scott Marshall** *April 29, 2016 03:47*

If I can get the source for the middelmanboards from Tony, I won't have to destroy a usable board. Better to sell to somebody that needs a replacement.


---
**Scott Marshall** *April 29, 2016 03:53*

**+Anthony Bolgar**

Sounds Great! Where can I get some?


---
**Anthony Bolgar** *April 29, 2016 05:09*

**+Scott Marshall** [https://oshpark.com/shared_projects/3W1BpcNl](https://oshpark.com/shared_projects/3W1BpcNl) for the boards.

[http://www.digikey.com/product-search/en?KeyWords=A100331-ND&WT.z_header=search_go](http://www.digikey.com/product-search/en?KeyWords=A100331-ND&WT.z_header=search_go) for the ribbon connector.

 The rest of the connections can either be soldered or use pin headers. Hope this helps Scott.


---
**Scott Marshall** *April 29, 2016 10:22*

**+Anthony Bolgar**

Thank You!


---
*Imported from [Google+](https://plus.google.com/111904744658622469563/posts/TZkyaETeNxA) &mdash; content and formatting may not be reliable*
