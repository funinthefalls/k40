---
layout: post
title: "Getting close to finishing up the z-table project"
date: January 14, 2016 05:35
category: "Modification"
author: "ChiRag Chaudhari"
---
Getting close to finishing up the z-table project. I need to get 4" bolts these 3" are bit short. Next up, cut the mounting brackets for the plastic bearings (shower door roller).﻿



Also have changed the design a bit

[https://plus.google.com/+ChiragChaudhari/posts/f1ZYrpYpmAr](https://plus.google.com/+ChiragChaudhari/posts/f1ZYrpYpmAr)



![images/650979298a96ca2bc3251efb4b08042c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/650979298a96ca2bc3251efb4b08042c.jpeg)
![images/190514bcb2fe9358ea4106da46482d5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/190514bcb2fe9358ea4106da46482d5f.jpeg)
![images/9dce32c4966c01ccaea6d7a633389f4a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9dce32c4966c01ccaea6d7a633389f4a.jpeg)
![images/d02f113481f90b4bad373ea2abece8ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d02f113481f90b4bad373ea2abece8ae.jpeg)
![images/cc7c6b1828beb14bd81754c2007d8bf4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc7c6b1828beb14bd81754c2007d8bf4.jpeg)
![images/71f8fac290aabe35aede8ffba010be9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/71f8fac290aabe35aede8ffba010be9e.jpeg)
![images/d1628b737c8427a7db6abadb63170c23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1628b737c8427a7db6abadb63170c23.jpeg)

**"ChiRag Chaudhari"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 14, 2016 06:05*

Looking good. Would love to see it in action once you've completed it.


---
**Tony Schelts** *January 14, 2016 12:52*

Looks very impressive,  once its complete might give it a go myself.  What is the frame made out of and what is the mesh?


---
**ChiRag Chaudhari** *January 14, 2016 16:34*

**+Tony Schelts** The frame is 1/2" W x 1/2" H x 72"' L aluminium channel ($7) from local hardware shop and the mesh is expanded metal sheet from home depot. It is 27" W x 96" L for only like $9. You can make 12 beds out of it. Bed size is ~12"x14".



Aluminum Channel: [http://www.homedepot.com/p/Everbilt-1-2-in-W-x-1-2-in-H-x-96-in-L-Aluminum-C-Channel-with-1-16-in-Thick-802657/204273938](http://www.homedepot.com/p/Everbilt-1-2-in-W-x-1-2-in-H-x-96-in-L-Aluminum-C-Channel-with-1-16-in-Thick-802657/204273938)

Expanded metal sheet: [http://www.homedepot.com/p/27-in-x-8-ft-Steel-Lath-2-5-METAL-LATH/202093395](http://www.homedepot.com/p/27-in-x-8-ft-Steel-Lath-2-5-METAL-LATH/202093395)


---
**Andrew ONeal (Andy-drew)** *January 14, 2016 20:45*

Looks awesome, hey is there any way I could get you to send me copy of a gcode file generated in inkskape plugin for comparison purposes? I can get all projects to move according to gcode but there is no laser fire. 


---
**ChiRag Chaudhari** *January 14, 2016 20:54*

Ya sure man. Will share some via Google drive soon. 


---
**Andrew ONeal (Andy-drew)** *January 14, 2016 20:58*

Cool, just need to see if my output files look like yours in reference to laser controls. Did I read somewhere that you had issues with laser powers set to 10 and had to increase in order to properly fire? Right now I can't get mine to fire


---
**ChiRag Chaudhari** *January 15, 2016 01:12*

**+Andrew ONeal** Hey sorry man took me so long to send you the file, got lil too busy. But here is the link.

[https://drive.google.com/folderview?id=0B8OiQZgP5gD2c1h1cU1mUGhJTFU&usp=sharing](https://drive.google.com/folderview?id=0B8OiQZgP5gD2c1h1cU1mUGhJTFU&usp=sharing)


---
**Andrew ONeal (Andy-drew)** *January 15, 2016 04:13*

Hey thanks man think what I needed to see was in line 2 where M80 controls laser fire, did this file adjust laser intensity on the fly or was it set in stone? Now when you get time send a file used for raster if possible.



Thanks again, hit me up if I can assist you in one area or another.


---
**ChiRag Chaudhari** *January 15, 2016 06:23*

**+Andrew ONeal** Hey happy to help. But hey M80 is actually used to turn on any external peripheral you are using like exhaust or say air assist. For the marlin laser ON and OFF codes are M3 and M5. I think the code I sent you does not have any of those because it uses codes like G01 G03 to turn the laser ON and also gives cordinates to head. And laser power or intensity is set by S50.00 code once until its changed again. So the Sxx.xx sets the value of laser power.


---
**Andrew ONeal (Andy-drew)** *January 15, 2016 07:35*

Ugh can't get laser to fire in response to code. Maybe I connected power supply wrong looking at from front right 

24 G 5v L and  to left of that is the pwm and laser test fire. I originally had it hooked up to to whatever d5 d6 laser from test fire pin, but now have it connected to L on far right of PS to d5. IDK it all seems to work in response to LCD control test fires. Will get you pic tomorrow maybe you can help sort me out of this three month nightmare.


---
**ChiRag Chaudhari** *January 15, 2016 08:04*

**+Andrew ONeal** [https://plus.google.com/u/0/+ChiragChaudhari/posts/gW49XQXTqNi](https://plus.google.com/u/0/+ChiragChaudhari/posts/gW49XQXTqNi)



Check out pic 4 an 5. It should solve your issue for sure.


---
**Andrew ONeal (Andy-drew)** *January 15, 2016 22:57*

**+Chirag Chaudhari** man your awesome, thank you so much. I had laser intensity like that at first then changed it based on another build I saw. So yea easy fix as soon as I get time to get down there. Now the firmware, I used the suggested turnkey tyranny build log-lasercutter-marlin on github and only made a few changes regarding end stops and range max and min. Is it possible for you to possibly send me a copy of you firmware to compare to what I've got? If you do then man I'll be set.



All of this was a first for me regarding codes, coding, and graphic designing lol. I've done tons with APM, NAZE32, osd overlay, and telemetry radios. I've also done allot with flashing ESC’s, and various flight controllers but this laser hobby is an entirely different kind of animal. You ever need anything Quadcopter like hit me up, I buy and sell as wholesaler. Just getting started in the business but still get with me if ya need something man. Thanks again, I should have put all this in a specific post rather than blogging all over yours lol


---
**ChiRag Chaudhari** *January 15, 2016 23:14*

WoooHooo another drone neard. Check me out here ScratchBuilds.com . I mainly deal with Lemon-Rx receivers and build 250 racers. Sold over 100 of them bad boys last year.

Anyway this is going to be piece of cake for you to get it working. Turnkey moded Marlin works great. The only thing I have changed is bed size and laser test fire values. Using the inkscape plugin to generate Gcodes. Also check out **+Peter van der Walt**​ 's LaserWeb. Great software to use along side the cutter. 

Stocking up the beer cooler at the store, will be free in bout an hour. Hit me up on Hangout anytime.




---
**ChiRag Chaudhari** *January 16, 2016 02:36*

**+Andrew ONeal** Just uploaded the firmware I am using in the same folder with gcode


---
**Andrew ONeal (Andy-drew)** *January 16, 2016 03:28*

Awesome, thanks man for everything. Nice site, do you order in bulk or use drop shipping? Man I love lemon-rx so clean, light, easy, and have awesome range. Just bought two ppm 8ch diversity two days ago. I've got 7 kits here to sell and other various items, then will restock.


---
**Andrew ONeal (Andy-drew)** *January 16, 2016 04:55*

Laser web install on win 8 out best installed on win 7?


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/dDrsy7y7rkC) &mdash; content and formatting may not be reliable*
