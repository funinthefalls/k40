---
layout: post
title: "Hey guys, my CorelLaser won't start this morning...any ideas what's wrong?"
date: March 08, 2017 17:07
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hey guys, my CorelLaser won't start this morning...any ideas what's wrong? I can start Corel Draw with no problems. I've already tried restarting the computer and uninstall/reinstalling and I still get the same Windows message "CorelDRAW Laser Driver has stopped working". Any ideas what's wrong?





**"Nathan Thomas"**

---
---
**Phillip Conroy** *March 09, 2017 07:21*

If using windows 7 or above try restoring to an earlier time,as a update could be the problem or reinstall  corel


---
**Abe Fouhy** *March 09, 2017 12:22*

Corel Draw and CorelLaser are two separate applications  (I know you knew that, just stating). The driver is being controlled on your USB root hub interface on your computer. I would not restore your computer as the first option, it will likely mess other stuff up.

First plug in your usb cable and open up the device manager, right click on "computer" ->properties ->device manager. Then look for a driver with a red or yellow error message. If you see it, click that driver and right click to uninstall. Uninstall driver, yes. Then click the action menu and scan for new hardware changes and it should reinstall the driver and have no errors. If it does tag me and send me a screenshot for us to see.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/CUJiyNskzsd) &mdash; content and formatting may not be reliable*
