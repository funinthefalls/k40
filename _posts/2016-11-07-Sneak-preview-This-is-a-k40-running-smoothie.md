---
layout: post
title: "Sneak preview. This is a k40 running smoothie"
date: November 07, 2016 01:39
category: "Object produced with laser"
author: "Ray Kholodovsky (Cohesion3D)"
---
Sneak preview. This is a k40 running smoothie.  Yep, that's all it takes. 

![images/6eb2b5c66b9fd05605a587ca20a317fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6eb2b5c66b9fd05605a587ca20a317fd.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Brandon Satterfield** *November 07, 2016 02:06*

Very cool man! 


---
**Mark Leino** *November 07, 2016 04:47*

Where were you a few hours ago when I was about ready to throw my k40 off the roof? Haha


---
**Ray Kholodovsky (Cohesion3D)** *November 07, 2016 04:49*

**+Mark Leino** I can get you one (hand assembled) if you want to test this. 


---
**Mark Leino** *November 07, 2016 04:51*

I got it running tonight, about 30 minutes ago actually. That looks awesome though. Should make it easy for people in the future. Nice clean install.


---
**George Fetters** *November 07, 2016 18:23*

I am curious about pwm are you using pwm to control the firing of the laser?


---
**Ray Kholodovsky (Cohesion3D)** *November 07, 2016 18:27*

**+George Fetters** we support both configurations of smoothie-fying the k40. 

I use the option where pwm replaces the pot and the L from the power connector is used for on/off. 

It is also possible to keep the pot and use the L for pwm control. The connectors are available on board for both ways. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/4hg7AmLsmnY) &mdash; content and formatting may not be reliable*
