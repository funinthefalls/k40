---
layout: post
title: "HI, I just ordered my K40 and it's on route"
date: June 25, 2015 09:00
category: "Modification"
author: "bart s"
---
HI, 



I just ordered my K40 and it's on route.  I was thinking about reworking it to a Arduino, ramps controller board..  There is some information about it on the google group.  Anybody here that has done this conversion ? 





**"bart s"**

---
---
**Rado Racek** *June 25, 2015 10:23*

Hi, I did it. Here is some video : 
{% include youtubePlayer.html id="cZibHxUpIG4" %}
[https://www.youtube.com/watch?v=cZibHxUpIG4](https://www.youtube.com/watch?v=cZibHxUpIG4)


---
**bart s** *June 25, 2015 23:35*

you used the Marlin firmware ? from here [https://github.com/lansing-makers-network/buildlog-lasercutter-marlin](https://github.com/lansing-makers-network/buildlog-lasercutter-marlin)


---
**Rado Racek** *June 26, 2015 05:30*

yes I used Marlin firmware. Everything is fine but engraving is little bit slow, max 5000mm/min


---
*Imported from [Google+](https://plus.google.com/103442310535462167640/posts/SX4q3XURBiL) &mdash; content and formatting may not be reliable*
