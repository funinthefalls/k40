---
layout: post
title: "My new favorite design tool for my laser?"
date: January 27, 2019 07:29
category: "Software"
author: "James Rivera"
---
My new favorite design tool for my laser? #FreeCAD! 



<b>Details:</b> I use #K40Whisperer and Inkscape files. But designing things to a specified size can be extremely tedious in #Inkscape. FreeCAD to the rescue!  Sketches in FreeCAD can be exported to a "Flattened SVG" file. This works...mediocre with the full models, and has problems with parts that aren't already laid flat. But, you can export a sketch directly!



<b>Directions:</b>

If not already in the XY plane (read: flat) then simply copy the desired sketch, paste it into a new file, switch to the Sketcher workbench, then select Sketch->Reorient sketch... and change to the XY plane. Then, with only that sketch selected in the treeview, select File->Export..., and select "Flattened SVG". Viola! You can now open the file in Inkscape, set the fill/stroke/stroke paint as desired for K40 Whisperer. E.g. 255 red to vector cut, 255 blue to vector engrave, and all 0 (black) to raster engrave. Sweet!





**"James Rivera"**

---
---
**James Rivera** *January 27, 2019 22:17*

minor nit: FreeCAD's export to flattened SVG does seem to create double lines over the exact same spot in some cases. I'm not sure why.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/5m1bwjwk7YS) &mdash; content and formatting may not be reliable*
