---
layout: post
title: "Shared on January 31, 2018 05:16...\n"
date: January 31, 2018 05:16
category: "Object produced with laser"
author: "William Kearns"
---


![images/e3ccb377a63d623245e30a4b93f01fd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3ccb377a63d623245e30a4b93f01fd4.jpeg)



**"William Kearns"**

---
---
**Ned Hill** *January 31, 2018 13:41*

Very nice work.  Reminds me, I've got a clock design that I've been meaning to get back to. :)


---
**Don Kleinschnitz Jr.** *January 31, 2018 15:03*

Time flies? Nice work!


---
**Fly Oz** *February 16, 2018 03:54*

Love the Dragon fly (being a flyfisherman)


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/GG8ewhWeFSg) &mdash; content and formatting may not be reliable*
