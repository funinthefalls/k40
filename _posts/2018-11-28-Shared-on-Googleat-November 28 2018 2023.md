---
layout: post
title: "Shared on November 28, 2018 20:23...\n"
date: November 28, 2018 20:23
category: "Object produced with laser"
author: "Laser Junkie"
---






**"Laser Junkie"**

---
---
**Stephane Buisson** *November 29, 2018 07:06*

What I learn from this video is you can laser off the powder coating after oven, no mark left on the  bare metal.


---
**Laser Junkie** *November 29, 2018 11:22*

**+Stephane Buisson** that is correct. You will need to use an industrial cleaner such as Purple Power to clean the area and rub off with a magic eraser.


---
*Imported from [Google+](https://plus.google.com/107030425065627399268/posts/UYoB4SXGt47) &mdash; content and formatting may not be reliable*
