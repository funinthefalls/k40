---
layout: post
title: "Hi Folks, I'm getting ready to cut some 1/8\" cast acrylic and wanted to double check what settings you all are using"
date: July 23, 2016 14:56
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Hi Folks, 

I'm getting ready to cut some 1/8" cast acrylic and wanted to double check what settings you all are using. 

What I found suggests 100% power at roughly 20mm/sec. How does that sound?





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ariel Yahni (UniKpty)** *July 23, 2016 15:09*

I do 10mms @ 10mA


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2016 15:20*

**+Ariel Yahni** Do you know how much 10mA is in terms of % power for PWM, or what mA 100% power is?


---
**Ulf Stahmer** *July 23, 2016 15:34*

Check +David Richards and I Laser's post on April 24, 2016.  He's got a graph of power in mA vs. percentage.

 [https://plus.google.com/communities/118113483589382049502/s/power%2Fpercentage](https://plus.google.com/communities/118113483589382049502/s/power%2Fpercentage)


---
**Ariel Yahni (UniKpty)** *July 23, 2016 15:38*

I setup my Pot to 10mA and then 100% there


---
**Joe Spanier** *July 23, 2016 15:54*

If you don't know what your psu is outputting at 100% ma wise its probably a good plan to check. I've seen the psu's in those put out 22ma at 100% with the dsp controller. Which is comfy for a 60w tube. Not a 30. 15 is really max if you want it to last a bit. 


---
**Ned Hill** *July 23, 2016 16:15*

The values that David Richard posted are consistent with values I've seen elsewhere. 


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2016 16:21*

Great.  I just finished modifying my Vectric Aspire postprocessor so that I can scale 1-100 spindle speeds down to 0-1 pwm values for smoothie and will be testing shortly.


---
**Steve Anken** *July 23, 2016 17:18*

Ray, you got me on that last one of editing the post processor. What I'd like is a Gcode parser/editor that has tools that can modify existing Gcode. Scaling x, y and z or mapping the z to 0-1 pwm for just two examples that can help us modify existing CNC files. Seems like a natural for a gcode viewer to expose the structure of the file to editing.


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2016 17:22*

Just finished the first cuts.  40% pwm corresponds to 8.5mA on my machine. The needle jumps to 10 and then settles on 8.5.  With air assist that was not enough to cut full through, I had to do 2 passes which was a spot on through cut.


---
**Ariel Yahni (UniKpty)** *July 23, 2016 18:06*

I'm not happy until I can do 1 pass


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2016 18:08*

Which I will be adjusting for in the next job... 80% power would do it,  (more than 15 but less than 20mA) but I may go 60% and a slightly lower speed.


---
**Ariel Yahni (UniKpty)** *July 23, 2016 18:16*

What speed did you did on the last one


---
**Ray Kholodovsky (Cohesion3D)** *July 23, 2016 18:20*

10mm/sec like you said. 


---
**Greg Curtis (pSyONiDe)** *July 24, 2016 00:50*

I do 1/8 cast acrylic as my main project material, I've been running at 10mm/sec with roughly 12-15mA, that would be 90-100% on a PWM control. I agree with others that you should verify mA output at given percentages tube damage can occur.



I have air assist, which is almost a necessity with acrylic as it likes to flare up during cutting.


---
**I Laser** *July 25, 2016 09:28*

I do 3mm acrylic at 10ma 6mm one pass. Though very dependant on your mirrors, lenses and smoke extraction.



I've whacked a multimeter on my machine and in my case readings are consistent with mA meter. YMMV


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/KCMSWskUT7A) &mdash; content and formatting may not be reliable*
