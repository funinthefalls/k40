---
layout: post
title: "Adding to my calendar magnet collection. 25mm in longest dimension and made from 1/8\" alder with mags on the backs"
date: October 06, 2017 01:57
category: "Object produced with laser"
author: "Ned Hill"
---
Adding to my calendar magnet collection.  25mm in longest dimension and made from 1/8" alder with mags on the backs. 

![images/126e169a5c5d8dcfdb5310ad8be37f7a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/126e169a5c5d8dcfdb5310ad8be37f7a.jpeg)



**"Ned Hill"**

---
---
**Ned Hill** *October 06, 2017 02:03*

Just a quick make tonight.  Got something much bigger in the works :D


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/QuSnQqjaVDP) &mdash; content and formatting may not be reliable*
