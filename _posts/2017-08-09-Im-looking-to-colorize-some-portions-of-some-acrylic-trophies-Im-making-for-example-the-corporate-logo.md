---
layout: post
title: "Im looking to colorize some portions of some acrylic trophies Im making, for example the corporate logo"
date: August 09, 2017 21:35
category: "Object produced with laser"
author: "Bob Damato"
---
Im looking to colorize some portions of some acrylic trophies Im making, for example the corporate logo. The  trophies themselves come out amazing, but I want to add that little extra.  The logo should be orange and gray... I was thinking some rub n buff, but they dont seem to have it in orange. Any other ideas for something thats easy to wipe on/off and get the job done?

Bob





**"Bob Damato"**

---
---
**William Kearns** *August 09, 2017 23:04*

Could u laser some tape off leaving a stencil and then fill will paint?


---
**Bob Damato** *August 10, 2017 21:10*

Hi william, I actually tried that, it was a disaster. hahaha. But you do give me an idea!




---
**Don Kleinschnitz Jr.** *August 11, 2017 00:22*

Pretty sure **+Ned Hill**​ does this alot?


---
**Ned Hill** *August 11, 2017 00:56*

I haven't tried it on acrylic but if you use good quality tape it shouldn't be a problem, just get a firm seal.  Hmm.. might be interesting to try with some translucent paints to get a stained glass effect.


---
**Ned Hill** *August 11, 2017 01:16*

I've seen people use regular white glue mixed with some acrylic paint to create faux stained glass pictures on glass and plastic.  Lot's of Youtube vids on this technique.  This would probably be possible on engraved acrylic as well.  Probably could use food coloring as well with the white glue.  Wouldn't be surprised if they made some kind of gel product for this as well.  Peruse the paint section at your local craft/hobby store and see what you can find.  **+Yuusuf Sallahuddin** you got any ideas to throw in?


---
**William Kearns** *August 11, 2017 03:10*

I used to manage the laser engraver/personalization depart for Williams-Sonoma I had them use regular masking tape then we would rub n buff or spray paint the lasered away tape this was on this acrylic bracelet ![images/f49042eea3f6696e6bd003fafe68e6c0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f49042eea3f6696e6bd003fafe68e6c0.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 11, 2017 22:59*

Not much to add, but I would try acrylic paint myself. I like Ned's idea of mixing with glue to make a translucent effect, will have to try that sometime.


---
**Bob Damato** *August 14, 2017 23:55*

Thanks Guys. Not too bad. I upped the power a little to cut through the tape and still mark the acrylic nicely. Came out OK. colorization came out OK too.  But apparently I have  followup question.  Ahem... how do you get tape residue off the acrylic without ruining the acrylic or paint? 

hahaha... Part of my pattern is a very fine checkerboard. It apparently makes one heck of a mess when trying to get it off.




---
**Ned Hill** *August 14, 2017 23:57*

What kind of tape are you using?




---
**Bob Damato** *August 15, 2017 01:56*

**+Ned Hill** Ned, just plain old masking tape.


---
**Ned Hill** *August 15, 2017 02:40*

**+Bob Damato** For tape I recommend 3M delicate surfaces blue painter's tape.  Never had a problem with reside with that. You can get it up  to 1-7/8" wide.  If you want something wider, so you don't have to butt together multiple strips, I recommend Rtape Conform 4075-RLA vinyl sign application tape.  You can get it in a wide variety of widths.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/2HWddBms2Ww) &mdash; content and formatting may not be reliable*
