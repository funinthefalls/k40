---
layout: post
title: "Will a 60w laser punch through a 0.3mm thick sheet of Aluminum?"
date: December 05, 2017 20:21
category: "Discussion"
author: "Bill Keeter"
---
Will a 60w laser punch through a 0.3mm thick sheet of Aluminum? 



I'm planning to use aluminum composite material (ACM) to enclose my custom laser frame and just wantt to make sure the beam can't cut through a wall. It's two sheets of 0.3mm aluminum with a solid plastic core. Very sturdy and light material. Easy to machine and drill holes in. If you guys haven't used it before, it's a great stuff. I have a sheet of it in one of my 3d printers to support a heated bed.





**"Bill Keeter"**

---
---
**greg greene** *December 05, 2017 20:42*

Any spot the beam would hit, it would be so out of focus it might not even heat it up a little


---
**Don Kleinschnitz Jr.** *December 05, 2017 20:44*

I don't know but his is complex question. 



At any distance I would guess that it won't as power drops off pretty fast. Also to my knowledge machines at our wattage cannot cut aluminum?



I would be more worried about its reflective properties at this wavelength.



Where did you get your material from? It sounds interesting.


---
**Bill Keeter** *December 05, 2017 20:58*

**+Don Kleinschnitz** hehe... do I want to share my supplier? hmmm..  The guys at Techshop (before they closed) told me where they buy their materials. [harborsales.net - Harbor Homepage](https://harborsales.net) . I buy my acrylic in large sheets and for a little extra $ they cut it to size for me. Absolutely awesome. It's expensive cus you have to buy those 4x8 foot sheets but in the long run it's far cheaper than buying individual small sheets.



Anyway, back on topic, i'm going to buy a sheet of Black ACM Dibond from them and have it cut to size for my enclosure. Then only have to drill the screw holes. (Going to repeat for the polycarbonate viewing window) 



Datasheet: [https://harborsales.net/Portals/0/docs/Dibond_PDS.pdf](https://harborsales.net/Portals/0/docs/Dibond_PDS.pdf)




---
**Steve Clark** *December 05, 2017 22:16*

The simple answer is no, it won't cut aluminum. I tried to cut .0005 " thick aluminum with the 40 watt... even coated it with black dye... nope...wouldn't touch it. 



However, you may be able to do so with a 100% oxygen gas feed system. But it could get real complicated to do and there is a very serious complication of oxygen gas and the gases coming off of the  plastics in the lamination.



Example: You can make rocket motors out of a plastic round with a hole drilled through the long axis and nozzle in one end feeding oxygen. The first 40 sec or so of this video shows you what I'm talking about.




{% include youtubePlayer.html id="TLPWqCMb7DE" %}
[youtube.com - Hybrid rocket engine with acrylic and gaseous oxygen](https://www.youtube.com/watch?v=TLPWqCMb7DE)  


---
**Mark Brown** *December 07, 2017 23:30*

Aluminum is crazy reflective to co2 wavelength.  I tried cutting a piece of aluminum foil once, it didn't make a mark on the aluminum, but the reflection did scorch a bit of the laser head.


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/SmRG7whswVa) &mdash; content and formatting may not be reliable*
