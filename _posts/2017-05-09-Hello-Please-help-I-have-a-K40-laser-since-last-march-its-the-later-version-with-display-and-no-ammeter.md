---
layout: post
title: "Hello! Please help... I have a K40 laser since last march, it's the later version with display and no ammeter"
date: May 09, 2017 21:02
category: "Original software and hardware issues"
author: "Niels Picciotto"
---
Hello!

Please help... I have a K40 laser since last march, it's the later version with display and no ammeter. It worked perfectly until yesterday evening, then it seemed to be much weaker than before. 

It always had a screeching sound when the laser was on, at every power settings, but the machine was cutting perfectly. Now if I have a power setting lower than 13.3 (of 100) it makes the screeching sound and laser correctly the media; over that setting there is a screeching sound for a brief time and then it mutes. Looking at the laser tube the ray inside it seems to have a slight variation when the sound mutes. The same cardboard (1.5mm thickness) i was cutting at 17 power now can't be cut with values lower than 60. 



I put a small video on youtube with a quick test: 
{% include youtubePlayer.html id="TY5fhplIt7Y" %}
[https://youtu.be/TY5fhplIt7Y](https://youtu.be/TY5fhplIt7Y)

After raising the power setting over 13.3 i pressed some times the laser test button, but after a brief screeching the sound disappears and the laser appears weak. 



What could be the part asking to be changed? 





**"Niels Picciotto"**

---
---
**Adrian Godwin** *May 09, 2017 22:36*

When did you last change the cooling water ?




---
**Mark Brown** *May 10, 2017 00:57*

And what are you using for coolant?


---
**Niels Picciotto** *May 10, 2017 04:41*

Tap water, 10 liters. I tried changing it after this problem but it did not solve it


---
**Adrian Godwin** *May 10, 2017 11:04*

The problem does sound similar to what I was seeing with tap water. I'm just surprised it was OK for so long. Maybe the tube has accumulated some dirt or scale ?


---
**Niels Picciotto** *May 10, 2017 11:13*

I had absolutely no problem for at least two months. I did not cut tons of paper, but also it was not "out of the box"... 

Looking at the tube it seems perfectly clean, and the fresh water did not change the behaviour of the machine.


---
**ALFAHOBBIES** *May 10, 2017 12:37*

I'd switch to distilled water and give it a try. 


---
**Mark Brown** *May 11, 2017 02:05*

I'd also recommend trying distilled.

There's some more info here:

[plus.google.com - COOLING WATER "IT MATTERS" After flailing for a few days with what I thought...](https://plus.google.com/+DonKleinschnitz/posts/jDRGVhd6zqy)


---
**Fernando Bueno** *May 15, 2017 20:05*

**+Niels Picciotto** Have you solved the problem? If so, how did you get it? Today was cutting without problems and suddenly the laser has stopped working. It makes a kind of whistle similar to the one you describe. I was using distilled water and reading the comments you make and **+Don Kleinschnitz** article, I've removed all the water, cleaned the tank to remove the algae and added a water mix with RV antifreeze in a in a ratio of 3: 1 /2. I left a while circulating the water to act and pressing the test button, the whistle continues to sound, but the laser does not work. Does anyone have any ideas?


---
**Niels Picciotto** *May 15, 2017 21:19*

I didn't solve it completely... Draining the tube and the bucket, cleaning everything and filling with distilled water has only stabilized the laser beam, which now is more steady and gives better results. But the screeching sound is still there at every power level under 13.3. Over this level the sound disappear and the laser loose power.

However, I discovered an interesting thing thanks to the ammeter installation. It seems that the power supply is using two ranges with different ratios. There is a power increase from 0 to 20mA if I select a value between 0 and 13.3. If I increase the power by 0.1 the current drops to 1mA and then starts to increase again. 20mA is reached again at a value of 75 displayed on  the machine. 

I suspect this is not a normal behavior, but after installing the ammeter at least I know I'm not destroying the laser with a strong current. 


---
**Don Kleinschnitz Jr.** *May 15, 2017 21:20*

**+Fernando Bueno** I do not recommend using antifreeze I tried it and the conductivity was to high.  I would test with only distilled.



Have you checked for arcs? 



When you say stopped working do you mean the tube does not light up?



Can you post a video of the anode end while operating. 


---
**Fernando Bueno** *May 16, 2017 14:33*

Thank you for your answers.



**+Niels Picciotto**. After analyzing your problem and your answer, along with the rest of the answer, I think the problem that I have is not the same. Yesterday I heard a whistle when I pressed the test button, but today, calmer, what I notice is a slight beep, exactly the same as when I pressed the test button on the power supply. This has made me think that the problem should not be in refrigeration, but in something electronic.



The first thing I have done is to change the power supply, leaving the transformer high voltage and the problem persists. Then I changed the transformer, but now I'm waiting for the heat-insulating paste to dry, so I still do not know if the problem has been solved. If this does not solve it, I would have to think about the tube, but it makes me weird.



One thing that I think is worth doing is to put an ammeter as comments that you have made. I understand that you have to put it in series with the positive cable that goes to the tube. It is right? Could you tell me what features you should have?



**+Don Kleinschnitz**. When I say that the laser does not work, I mean that when I press the test button, nothing happens. There is no light inside the tube and there is no laser beam. That's why I can not make video to show you the problem or analyze if there is an arc in the anode. When the thermal paste dries, I will return to the tests and comment the results.


---
**Niels Picciotto** *May 16, 2017 14:46*

I connected the ammeter in series. I disconnected the L- cable coming from the tube, attached a wire to be able to connect to the ammeter, which is placed outside the cabinet behind the USB port so i could use the same hole for the cables. Then a cable from the ammeter to the L- connector that was used in the original configuration. The ammeter is a 30mA from a popular auction site... 

It works perfectly and should be a standard feature on the digital laser cutters...


---
**Don Kleinschnitz Jr.** *May 16, 2017 15:48*

**+Fernando Bueno** the meter does NOT go on the positive side.

It goes between the cathode and the -L of the supply. Careful it's -L not "L". It's on the left connector. 



Check out this blog post:

[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html?m=1)




---
**Fernando Bueno** *May 16, 2017 16:16*

Thank you for your help. I just bought the ammeter on Amazon and it will arrive tomorrow. Just one doubt. Why is it better to use an analog ammeter than a digital ammeter?



And another doubt more related to the refrigeration of the tube. What is the maximum temperature that should not exceed water? Where I live, in summer it is very hot and the ambient temperature can exceed many days 35º C.


---
**Niels Picciotto** *May 16, 2017 16:25*

I can try to answer with my (limited) knowledge on these subjects.

The analog ammeter is faster in reaction than a digital ammeter, and should be able to show peaks and variations in a quicker and more precise way. 

Ok my K40 laser (and I think many of the digital one) the digital display does not show a current flow, but is a power scale (1-100), so it does not show how much current is flowing in the laser. In the next days I will try to get an excel file with the current flowing at the different power levels. 

The temperature should be maintained at a low level. I have the machine since a few months, so I used it in the winter and spring, but a friend using an older version told me that he prepairs bottles of water in the freezer, and puts them in the water bucket in the summer in order to have the water not heating too much. I will keep an eye on my machine when the temperature increases...


---
**Don Kleinschnitz Jr.** *May 16, 2017 19:17*

**+Niels Picciotto** I agree with your answer the analog meter is more dynamic in it's representation of changes. Also it requires no power to operate and is pretty immune to electrical noise. 


---
**Fernando Bueno** *May 17, 2017 16:43*

**+Don Kleinschnitz** The heat-insulating paste has already dried and the laser has returned to normal operation. Unlike before (when it worked), now has begun to make the comment that says **+Niels Picciotto** As I had added antifreeze to the distilled water, I will replace it with the water alone, to see if the whistle disappears.



This is the video of what happens on the anode side of the tube when I press the test button.



![images/5b5cbb117141365b7f230a15ddf38c66.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b5cbb117141365b7f230a15ddf38c66.jpeg)


---
*Imported from [Google+](https://plus.google.com/115427515119835049016/posts/NCuA3XAprBX) &mdash; content and formatting may not be reliable*
