---
layout: post
title: "I am new to this. One of the mirrors cracked so I need to replace them, how do I find out what size to order"
date: March 28, 2017 00:12
category: "Discussion"
author: "John Stevens"
---
I am new to this. One of the mirrors cracked so I need to replace them, how do I find out what size to order. or does it matter, probably dumb question but this is how you learn, lol,I don't see any parts descriptions or help in this manual any help would be appreciated.





**"John Stevens"**

---
---
**HP Persson** *March 28, 2017 01:33*

Measure one of the others mirrors ;)

20mm mirrors in the k40 are pretty much in everyone, look for SI or MO mirrors, SI a bit "better", MO tougher..

Just keep away from K9 mirrors, faaaaar away.


---
**John Stevens** *March 29, 2017 18:52*

Why stay away from k9? Just curious 


---
**HP Persson** *March 30, 2017 20:27*

**+John Stevens** Bad quality, made cheap, scratches easy, fog up quick and have thinner coating.

Lower reflectance as well

Many K40 arrives from the sellers with these K9 mirrors, people who changed them see a big difference with SI-mirrors :)

The extra few bucks is well worth it.


---
*Imported from [Google+](https://plus.google.com/108124828909436986786/posts/iofSZzjccVZ) &mdash; content and formatting may not be reliable*
