---
layout: post
title: "honorable gentlemen. I replaced the laser tube and the power supply to a 60-watt"
date: November 05, 2016 21:00
category: "Object produced with laser"
author: "yosyp mindak"
---
honorable gentlemen.

I replaced the laser tube and the power supply to a 60-watt.

Since then a laser engraving and quite poor.

What could be the problem?

as if it would receive two signals.

thanks for the help.

![images/064a790e0a6b4b1864edf8037b3601c3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/064a790e0a6b4b1864edf8037b3601c3.jpeg)



**"yosyp mindak"**

---
---
**Scott Thorne** *November 05, 2016 22:10*

That is a bad alignment problem...align mirrors and head...that will solve the problem.


---
**Tony Schelts** *November 05, 2016 22:14*

I agree I had the same problem, its almost like a reflection inside. But when I aligned correctly made all the difference.


---
**yosyp mindak** *November 05, 2016 22:23*

Thank you for the time being.

I will try tomorrow.




---
**Scott Thorne** *November 05, 2016 22:23*

No problem...gl


---
*Imported from [Google+](https://plus.google.com/114202487892954620012/posts/Dsp8yp464dS) &mdash; content and formatting may not be reliable*
