---
layout: post
title: "My new tube arrived today! It was measured at 58W at the second mirror by Scott Thorne when he had it"
date: November 09, 2016 16:59
category: "Modification"
author: "Anthony Bolgar"
---
My new tube arrived today! It was measured at 58W at the second mirror  by **+Scott Thorne** when he had it. I can't thank him enough for this donation. This will really beef up the power on my Redsail LE400 (Like a k40 but better ;) ). Can't wait to get it installed and see what it can really do!





**"Anthony Bolgar"**

---
---
**Scott Thorne** *November 09, 2016 17:01*

Glad i could help bro....i hope it arrived in good shape!


---
**Anthony Bolgar** *November 09, 2016 17:05*

It is on it's way across the border right now, should have it in my hot little hands in about 1/2 hour.


---
**Scott Thorne** *November 09, 2016 17:07*

Lol...ok


---
**Ariel Yahni (UniKpty)** *November 09, 2016 18:05*

Guys I need to measure heat on mine, I'm loosing to much power with no particular reason ( good alignment, clean, cold water, etc)  whats an easy way 


---
**Anthony Bolgar** *November 09, 2016 18:06*

The only accurate way I know of is to use an actual laser power meter.


---
**Scott Thorne** *November 09, 2016 18:27*

I got my meter from bell laser co. For 100.00...goes up to 100 watts


---
**Anthony Bolgar** *November 09, 2016 19:00*

Yeah! Tube arrived in one piece.


---
**Scott Thorne** *November 09, 2016 19:05*

That's good to know...you find the 2 lenses i put in the inner box?


---
**Anthony Bolgar** *November 09, 2016 19:13*

Yup,found them (Thanks for reminding me, they almost got tossed with the bubblewarp)


---
**Scott Thorne** *November 09, 2016 19:27*

Lol


---
**Scott Thorne** *November 09, 2016 19:28*

How are you on the power supply...i have a 50watt.


---
**Scott Thorne** *November 09, 2016 19:29*

I'm about to pull the trigger on a 30 watt galvo head fiber laser...Tracy is gonna kill me...lol


---
**Anthony Bolgar** *November 09, 2016 19:30*

I am pretty sure the one I bought should be OK, need to look up it's specs again to double check. What is the minimum the tube requires?


---
**Anthony Bolgar** *November 09, 2016 19:32*

If you need to get rid of your 50W blue box you know where to find me ;)


---
**Scott Thorne** *November 09, 2016 19:33*

Not sure about the min...max is 18mAs


---
**Scott Thorne** *November 09, 2016 19:34*

The whole machine or the power supply...lol


---
**Anthony Bolgar** *November 09, 2016 19:39*

Whole thing...lol! The PSU I have is more than sufficient if the the max is only 18mA. My PSU will put out  30kV and 25mA


---
**Ariel Yahni (UniKpty)** *November 09, 2016 19:39*

I also have and address you can send it lol, I'll take anything 


---
**Scott Thorne** *November 09, 2016 19:43*

**+Ariel Yahni**...you need the power supply?


---
**Scott Thorne** *November 09, 2016 19:45*

Send me the address via google messanger and pay for the shipping and its yours. 


---
**Scott Thorne** *November 09, 2016 19:57*

My plan was to buy a bigger cutter then i saw the fiberlaser


---
**Ariel Yahni (UniKpty)** *November 09, 2016 20:03*

I want to start think about building a bigger one. Need the challenge. How much is the fiber laser, I have a proposal for doing some special markings and could be a good investment 


---
**Scott Thorne** *November 09, 2016 20:17*

6 grand


---
**Anthony Bolgar** *November 09, 2016 20:18*

A 30 W fibre would be around 15 K40's.....pricey


---
**Scott Thorne** *November 09, 2016 20:20*

With it...speed and accuracy are outstanding


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/1WujBDtpdvd) &mdash; content and formatting may not be reliable*
