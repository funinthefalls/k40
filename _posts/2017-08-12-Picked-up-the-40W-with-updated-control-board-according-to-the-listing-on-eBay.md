---
layout: post
title: "Picked up the 40W with updated control board according to the listing on eBay"
date: August 12, 2017 02:40
category: "Discussion"
author: "Ray Rivera"
---
Picked up the 40W with updated control board according to the listing on eBay. All of a sudden, the Laser Power Display is no longer working. The Laser Switch does work to power on and off the tube, and I can still test fire and engrave. But the LED does not show the power % anymore. Anyone else have this problem? I tried contacting the seller but no response yet. I have tried checking all connections and made sure the water flow meter is working but still no dice.



[http://www.ebay.com/itm/40W-USB-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-New-Control-Board-/262752272249](http://www.ebay.com/itm/40W-USB-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-New-Control-Board-/262752272249)?



![images/89ee6410f574ae1f478c65638c657a05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/89ee6410f574ae1f478c65638c657a05.jpeg)
![images/77162d8c32ce3331d7c2f56c47a9a110.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/77162d8c32ce3331d7c2f56c47a9a110.jpeg)

**"Ray Rivera"**

---
---
**Cris Hawkins** *August 12, 2017 14:39*

There are numerous posts that show how to install a 10 turn pot and miliAmp meter for better control. This may be a solution for you if you can't operate your laser with the existing setup.



Also, I am rewiring my laser to use the afore mentioned pot and meter, so I have an unused digital display board....


---
**Ray Rivera** *August 12, 2017 16:36*

Thanks Cris. From what I can tell the digital adjustments still work, and was able adjust power output. What I am going to try to do while waiting for the ebay seller to get back to me is to add an ammeter according to these instructions and hopefully that will work.



[donsthings.blogspot.com - Adding an Analog Milliamp Meter to a K40](http://donsthings.blogspot.com/2017/04/adding-analog-milliamp-meter-to-k40.html)


---
**Cris Hawkins** *August 12, 2017 18:58*

Yes, that's what I'm doing also....


---
**Ray Rivera** *August 29, 2017 18:30*

Got the replacement board yesterday; DHLed from China to me here in Baltimore. Swapped right in with no problem. Keeping the ammeter wired in though for reasons.


---
*Imported from [Google+](https://plus.google.com/117639914879018784950/posts/i3ts1fAxY87) &mdash; content and formatting may not be reliable*
