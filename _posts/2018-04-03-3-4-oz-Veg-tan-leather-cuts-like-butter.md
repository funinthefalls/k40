---
layout: post
title: "3-4 oz Veg tan leather cuts like butter"
date: April 03, 2018 13:08
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
3-4 oz Veg tan leather cuts like butter. 10mm/s at 10mA.

I still think it will cut a lot faster. 



**+Yuusuf Sallahuddin** this is amazing, thanks for the tip on the leather type.

![images/694ec61d016c841394abf697ab1d7604.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/694ec61d016c841394abf697ab1d7604.jpeg)



**"Ariel Yahni (UniKpty)"**

---


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/hJyA22wWkLM) &mdash; content and formatting may not be reliable*
