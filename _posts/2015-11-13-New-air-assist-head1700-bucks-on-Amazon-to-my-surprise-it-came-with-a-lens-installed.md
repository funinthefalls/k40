---
layout: post
title: "New air assist head....17.00 bucks on Amazon, to my surprise it came with a lens installed"
date: November 13, 2015 14:43
category: "Hardware and Laser settings"
author: "Scott Thorne"
---
New air assist head....17.00 bucks on Amazon, to my surprise it came with a lens installed.

![images/b5aa0861f60b891eb78fdc5114990033.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5aa0861f60b891eb78fdc5114990033.jpeg)



**"Scott Thorne"**

---
---
**Sean Cherven** *November 13, 2015 14:57*

Was or used or new?


---
**Scott Thorne** *November 13, 2015 14:58*

New.


---
**Stephane Buisson** *November 13, 2015 15:07*

**+Scott Thorne** link ? what material (look plastic)?


---
**Scott Thorne** *November 13, 2015 15:08*

It's anodized aluminium.


---
**Stephane Buisson** *November 13, 2015 15:14*

[http://www.amazon.com/18mm-Laser-Assisted-Ideal-Machine/dp/B0094WLPYK](http://www.amazon.com/18mm-Laser-Assisted-Ideal-Machine/dp/B0094WLPYK)



lens & mirror capable (not a commitment)


---
**Scott Thorne** *November 13, 2015 15:22*

That's why I was surprised that a lens was installed...there was no mirror though.


---
**Coherent** *November 13, 2015 17:08*

I think you just got lucky. I bought the same from LO. Same price, no lens likely an error by them or whoever their supplier is. :)


---
**Scott Thorne** *November 13, 2015 17:34*

Could be....I ordered one and its on the way....I didn't think it would have a lens with it so I guess I'll have a spare.


---
**Ashley M. Kirchner [Norym]** *November 13, 2015 17:37*

One question: that head says it accepts an 18mm focus lens. I checked on my machine and it has a 12mm focus lens. So I know it won't fit. Do the heads come with different size holes?


---
**Scott Thorne** *November 13, 2015 18:12*

Yes...it takes a bigger lens


---
**Ashley M. Kirchner [Norym]** *November 13, 2015 18:14*

Looking at LO's web site, they don't appear to have a 12mm head either. So I'm assuming that folks are buying larger focus lenses then?


---
**Scott Thorne** *November 13, 2015 18:37*

Yes 18mm it's supposed to transfer more energy...I wouldn't know I have yet to try mine out.


---
**Coherent** *November 13, 2015 19:42*

You can cut a small 18mm "disk" with a 12mm hole out of just about anything and it will hold the 12mm lens centered in the 18mm head. If you have a  3d printer it's even simpler.


---
**Ashley M. Kirchner [Norym]** *November 13, 2015 19:48*

Does having a larger lens make a whole lot of difference? Say 18mm versus 12mm? Trying to justify the $52 price tag on LO. Easier to cut thicker stuff perhaps? Wood? Fingers ... I mean, I've never done that. Nope, not me. :)


---
**Scott Thorne** *November 13, 2015 19:55*

I get what your saying, I'm headed back to home tomorrow and I'll try it out as soon I get there and try it out....I'm curious about the air assist, but from what I've read it dies make a big difference....then of course buying good US optics also id's going to make a big difference.


---
**Ashley M. Kirchner [Norym]** *November 13, 2015 19:58*

The air assist definitely makes a difference. I'm just curious about the larger lens size.


---
**Scott Thorne** *November 13, 2015 20:33*

I'll let you know tomorrow...lol


---
**Coherent** *November 15, 2015 01:17*

**+Scott Thorne**

Well? Did the new lens make any difference?


---
**Scott Thorne** *November 15, 2015 01:22*

Funny you should ask, it would appear that my girlfriend can't tell an empty hole from a lens...lol..it didn't come with a lens, I was going on what she told me...I knew it was too good to be true, but I ordered one the day I ordered the head and it will be delivered Tuesday so I will get back you you Marc G.


---
**Scott Thorne** *November 15, 2015 01:24*

Also the honeycomb table is huge, it's not 300x200mm....as advertised...it made to change out the whole insert that is inside the x,y frame.


---
**Coherent** *November 15, 2015 12:39*

**+Scott Thorne** I also ordered a lens from LO. The high quality 18mm 50.8, so was curious. I should have mine Mon or Tue also, guess I will know soon if it makes any difference.


---
**Scott Thorne** *November 15, 2015 12:42*

Keep me posted Marc...

If you will....I'm also very curious.


---
**Sean Cherven** *December 02, 2015 15:17*

Any updates on the difference between the lens?


---
**Scott Thorne** *December 02, 2015 15:22*

The 18mm lens seems to cut and engrave a lot better at lower power....I'm very happy with it **+Sean Cherven**​


---
**Ashley M. Kirchner [Norym]** *December 02, 2015 21:47*

So that stands to reason that a larger lens will provide a little bit more power (wider beam path convergence) ...


---
**Scott Thorne** *December 02, 2015 21:53*

**+Ashley M. Kirchner**​...I agree completely...I've experienced the difference first hand my friend and its a big difference...that's only my opinion...others may vary...Lol


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/PWhqfpMeZXx) &mdash; content and formatting may not be reliable*
