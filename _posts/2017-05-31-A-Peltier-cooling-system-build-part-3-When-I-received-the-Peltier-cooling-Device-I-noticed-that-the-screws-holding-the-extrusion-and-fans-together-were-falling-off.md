---
layout: post
title: "A Peltier cooling system build part 3 When I received the Peltier cooling Device I noticed that the screws holding the extrusion and fans together were falling off"
date: May 31, 2017 03:48
category: "Modification"
author: "Steve Clark"
---
A Peltier cooling system build part 3



When I received the Peltier cooling Device I noticed that the screws holding the extrusion and fans together were falling off. They were not going to hold the fans on because the fins were too thin to give a good bite . So much so that with little effort I could pull the fans off by hand.  I went online and ordered some screws that would go all the way through the extrusion and the first mounting holes of the fan.



Then I mounted each fan with two of the old screws and drilled through the extrusion the two remaining holes using the holes in the fan. I mounted the new screws then removed the old ones to drill those out. I did it this way to make sure everything lined up.



[http://www.ebay.com/itm/252803696658?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/252803696658?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



I had some EMI issues when installing Cohesion 3D because of how I had installed a LO Z axis and some other power supplies, so I decided to mount everything I would need outside of the laser cabinet.



 I spent some time trying to find the right size electrical cabinet to mount on the back of my roller stand that was of reasonable price. They are very pricey, close to a hundred  bucks for the size I was looking for.  So I was at a bit of loss as to where to find one.



Shortly after this came up I happen be in Harbor Freight looking for something else when I saw a steel  key lock box just that was just the size I was looking for at $39.99. I also had a 25% off coupon  that brought it down to about 32 bucks. 



[https://www.harborfreight.com/100-hook-key-box-95318.html](https://www.harborfreight.com/100-hook-key-box-95318.html)



I have three DC power supplies that I want to mount, a 24volt,a 12 volt and a 3 volt. I decided to move the power cord down to the electrical cabinet and add two additional 120 volt outlets for the water pump and exhaust fan that would be connected to the power main switch.



 I needed to also mount a solid state relay for the thermostat to Peltier cooling system with its heat sink plus a terminal strip block along with a main power Panel-Mount Circuit Breaker, Rocker Style, Snap-in Mount on the side.  The door was cut out for a fan to draw heat out and holes were drilled in the bottom for intake vents to which I’ll glue some stainless screening on I have in my junk box. Lots of things came out of my junk box for this build.



Now I just need to mount everything , add some Nylon Cable Glands for the wiring and lay out the wiring.  That will be in part 4.





![images/c02afc3a6b202b1bab71b63252d247a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c02afc3a6b202b1bab71b63252d247a2.jpeg)
![images/09df129de3d7d67a8ea0bef0f11b1661.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09df129de3d7d67a8ea0bef0f11b1661.jpeg)
![images/1ebbe5dce45592c6d7b415addef07fd9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ebbe5dce45592c6d7b415addef07fd9.jpeg)
![images/44369b862851b86a4936c93eb7b396df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44369b862851b86a4936c93eb7b396df.jpeg)
![images/44b8ccc2be41c76f8ef5d6fbeb5840ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44b8ccc2be41c76f8ef5d6fbeb5840ac.jpeg)
![images/2865f51214f385c80bfeb1cf2bf1f6af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2865f51214f385c80bfeb1cf2bf1f6af.jpeg)
![images/402decaaf9af1647069575429445cb53.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/402decaaf9af1647069575429445cb53.jpeg)

**"Steve Clark"**

---
---
**Phillip Conroy** *May 31, 2017 08:01*

I would be surprised if 125 watts of cooling will be enough,I am using a refrigeratored under sink Ater chiller rated at 50 watts ,Wichita alow me to run laser flat out for hours with temps around 18 deg c


---
**Steve Clark** *May 31, 2017 17:24*

It’s possible that your right... but I don’t think so. I have allowed for adding another 60 watt Pelier just in case though.



 This would bring my functional cooling wattage up to about 170 watts and that would put me at about 20 watts more cooling than those little 2.4 cu. Ft. to the 4.4 cu. Ft. bar refrigerators. Their output at 115v is  1 to 1.3 amps or about 150 watts max using a “1” power factor. 



With this system, the insulated tank and if necessary pre-cooling on warm days because of a high ambient temp. I’m thinking I will be good. We will find out when it’s all together and tested.




---
**1981therealfury** *May 31, 2017 18:11*

I bought one of the peltier chillers, was supposed to be 2x 72 w peltiers for 144w total... note that this is the power input, not the cooling power output and from what i read they are about 10%-15% efficient so i guess i'm getting about 15-20w of cooling, which just doesn't make much difference in my setup.  It just about offsets the temperature rise from the pump but can not cope with the cooling requirements of my "Chinese 50w (so about 35w) tube.  Pre cooling the water gives me a couple of hours of cutting, but once i run out of ice packs then its game over in short order.



Decided to try an Absorption cooler next.


---
**Phillip Conroy** *May 31, 2017 19:17*

Edit my under sink cooler rated at 500 watts,not 50


---
**Steve Clark** *May 31, 2017 19:56*

**+Phillip Conroy**  500 watts…that’s nice. How is it setup? 


---
**Phillip Conroy** *May 31, 2017 20:53*

Separate pond pump into same bucket,will maintain temperature if cutting 100% of time or I switch it off when temps hit 15 deg if only cutting now and then (file designing)


---
**Phillip Conroy** *June 01, 2017 02:39*

Good luck with the setup ad keep us in the loop


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/AfsFNosqRAw) &mdash; content and formatting may not be reliable*
