---
layout: post
title: "hey guys, I recently got a k40 and after using it for awhile I want to upgrade it to have a better work area and change the board to somethig that isnt so janky"
date: December 14, 2016 04:52
category: "Modification"
author: "Peter Roig"
---
hey guys, I recently got a k40 and after using it for awhile I want to upgrade it to have a better work area and change the board to somethig that isnt so janky. I would love to use illustrator on my mac to control it with some drivers for it. Does anyone have any ideas or recommendations for the control board?





**"Peter Roig"**

---
---
**Anthony Bolgar** *December 14, 2016 04:57*

Smoothieboard for the win. Or a board like **+Ray Kholodovsky** makes, the Cohesion3D board, that runs smoothieware. Just stay away from the MKS Sbase board, it is a poor clone of the smoothieboard that does not follow the open source rules.


---
**Ariel Yahni (UniKpty)** *December 14, 2016 04:59*

+1 for **+Ray Kholodovsky**​ mini board. Easy upgrade, Mac compatible, LaserWeb   supported


---
**Peter Roig** *December 14, 2016 05:14*

Laserweb works with the board out of the box? And I was looking to just use the program and print directly from that. I dont mind if it goes to another program, but I would rather  print it, then it goes to another thing and then starts. Similar to how it is now, except not shitty xD



also any ideas to upgrade the work area to a bigger area and adjustable?


---
**Stephane Buisson** *December 14, 2016 06:43*

Smoothieboard,

 illustrator-> laserweb 

or

 Visicut (has a plugin for illustrator)


---
**greg greene** *December 14, 2016 12:28*

Cohesion 3D from Ray


---
*Imported from [Google+](https://plus.google.com/108692307482755021744/posts/Y1DFyUTEhyd) &mdash; content and formatting may not be reliable*
