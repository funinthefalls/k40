---
layout: post
title: "And the price keeps falling! Not sure if it is a price wars or not but ebay has the basic unit for $318 USD delivered!"
date: July 01, 2016 04:13
category: "Discussion"
author: "HalfNormal"
---
And the price keeps falling!



Not sure if it is a price wars or not but ebay has the basic unit for $318 USD delivered!





**"HalfNormal"**

---
---
**Alex Krause** *July 01, 2016 04:25*

Might have to pick up a second unit :P


---
**Jim Hatch** *July 01, 2016 12:58*

:-) Makes me feel bad for folks like Peter, etc. who are in other countries where it's twice the price.


---
**Joe Spanier** *July 01, 2016 23:54*

I just ordered one for about that. No bells but who cares 


---
**I Laser** *July 02, 2016 01:20*

Those prices haven't made it to Australia either, still sitting at $580 delivered.



Hoping you guys get a better machine than my cheap one is. They really scrimped on build quality, though it works well enough.


---
**Gary Hamilton** *July 03, 2016 23:23*

I got one of the $318 machines.  I have been etching and cutting since Friday night.  Still learning the ins and out of the machine but better than I expected.  I thought I was going to be swapping controls out right away now I am on the fence.  For hobby work it is pretty good.  Again, I still have a steep learning curve to get through but if I could stop sleeping I think I could get there.




---
**Jim Hatch** *July 04, 2016 05:25*

**+Gary Hamilton** No need to hurry on the conversion. (I have a Smoothie and a Ramps board that I haven't gotten around to installing yet). Once you get past the quirks and can use CorelLaser to push straight to the laser you're in pretty good shape for knocking around. Later when you want to get into engrave & cut in the same job, controlling power & speed, etc. you'll want to go Smoothie and LaserWeb. I'll be doing that later this summer. Right now I'm just making things :-)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/iRq9s5Sbdb3) &mdash; content and formatting may not be reliable*
