---
layout: post
title: "I have taken my tube out to find this small hairline crack pointed out by another user on here, it is outlined in the first image for you to see"
date: April 06, 2016 09:06
category: "Discussion"
author: "george ellison"
---
I have taken my tube out to find this small hairline crack pointed out by another user on here, it is outlined in the first image for you to see. This crack does not go all the way through the glass i don't think but is this enough to make the tube pack up? Thanks



![images/5c8af2b18a776418551bf05d4985d8e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c8af2b18a776418551bf05d4985d8e8.jpeg)
![images/b72ca12419278ee17152c696cf680552.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b72ca12419278ee17152c696cf680552.jpeg)

**"george ellison"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 09:17*

That is concerning for sure. I have no experience with tubes giving up the ghost (yet), so can't say much. Only thing I want to mention is that it looks like there was a sticker on the glass at that point at some time? It looks oddly smooth/cleaner than the rest of the glass (to the right in the image).


---
**Scott Marshall** *April 06, 2016 10:10*

It only takes a tiny leak... 

If it's not failed yet, it' won't be long.

Once a crack starts, it's all over but the shouting.



That being said, it looks almost like mineral deposits or something left when the water was drained. I can't actually see the flaw, just where the contamination ends. 

Tiny flaws in glass are hard to see in real life, harder in photos. Can you feel the crack with your fingernail or a razor blade? if so, it's real. 

Maybe flush water through it and see if it changes.



If it still works, run it and see if it lives. If it dies, change it out.


---
**george ellison** *April 06, 2016 10:14*

The tube failed on me that's why i decided to take it out and investigate. The red cable connector starting arcing to the case with no beam firing. You cannot feel the crack with your fingernail and i see what you mean with it being clean on one side and dirty on the other.


---
**Scott Marshall** *April 06, 2016 10:21*

That's what they do when the gas is gone. (it's no longer an easier path thru the tube, so it arcs)



It's a crack.



Sorry for your loss.



Scott


---
**george ellison** *April 06, 2016 11:38*

So it is no firing as it cannot make a connection due to there being no gas? As it has leaked or run out. Well at least i know that it is definitely a new tube i need. Next time i will just make sure the water is always running fine and not use it for too longer periods of time. Do you think when i install a new tube it should fire as new? Many thanks.


---
**Scott Marshall** *April 06, 2016 14:49*

You should be as good as or better than new with a fresh tube (Stock tubes are not known for power) - as long it didn't damage the power supply when it failed.



That seems to happen about 1/3 of the time. 



If it's still arcing, the power supply is still working, so the tube replacement and a careful alignment out to do it.



 You may want to consider a flow switch shutdown circuit to protect the new tube. A lot of people have blown tubes because they forgot to turn on the water or it ran low.



Scott


---
**Dennis Fuente** *April 06, 2016 16:37*

I notice that in the area of the crack you point out i always see on my tube that there is an absence of water i have tried to tilt the machine to fill this area but no cahnge can this be the source of the crack



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 16:45*

**+Dennis Fuente** I noticed a consistent absence of water in that area also on my machine, however I managed to remove it just the other day & have a very small bubble that I cannot remove now. I managed to remove it by tilting the machine, with the pump flowing & sucking on the outlet hose (like a siphon). The area absent of water is 99% filled now.


---
**Dennis Fuente** *April 06, 2016 19:20*

yes it's the same for me just a small bubble at the very end of the tube i guess that's as good as it get's 



Dennis


---
**Scott Marshall** *April 07, 2016 08:16*

The pumps supplied with the K40 are marginal, I did a bucket test on mine and discovered that it only is moving about 1/4gpm through the laser.



I did this before adding a flowwheel and heat exchanger to be sure it would be safe. Good thing I checked.



 I'm running a Little Giant S1200 fountain pump now, and it pushes that bubble right on out as soon as it starts up. The pump is diverting about 80% of it's volume back to tank. It's enough to cool about 5 lasers.



I went overkill on the pump because I got a great deal on it ($30), but a parts washer or machine tool coolant pump would be about perfect, and a new Little Giant commercial duty pump that size runs $50-$70 range. Cheap insurance against cooling related tube issues the way I see it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 08:20*

**+Scott Marshall** Yeah probably well worth the investment of a few $ to protect a more expensive component. What water flow does the S1200 you are running have? I don't want to get something ridiculously overpowered (to the point that it breaks the tube), but I think I should possibly upgrade the pump also.


---
**Scott Marshall** *April 07, 2016 11:58*

[http://www.little-giantpump.com/](http://www.little-giantpump.com/)



The pump I have is 255W, it's good for 868 GPH@1' head and 240 GPH @ 15'. Stainless and bronze, it's a $300 pump, couldn't pass it up for 30 bucks, but it's way too big and has to go. I'm going to try a PWM speed control on it 1st, but I'm in the market for a smaller one. I may put it on my CNC mini mill, it could use the heating and power to push through a filter.



I'm looking for something around 80W that's good for around 120GPH @ 6 feet H20 (2.6PSI). That put you around 2 GPM thru the laser and can be throttled  without wasting too much pump capacity and heating your water a lot. The Little Giant S320 is 89W, 290GPH@1', 81GPH@10'.

That's still pretty big, but the next size down has a shut off of 5.7', and as restrictive as the water system is, is iffy in my opinion. I'd say a 10' shutoff is as small as I'd like to go.



The S320 runs about $150-$170US which is pretty extreme. Ebay has a demo model for $68 right now, if you didn't lice on the other side of the planet.



If you get out of the S series, which are super high end stainless and Bronze (the S1200 is about the size of a TP roll and weighs about 10lbs)



Getting more practical, the LG "Small Submersible" Series has a lot of options. 





[http://www.little-giantpump.com/where_to_buy_little_giant_small_pumps.htm](http://www.little-giantpump.com/where_to_buy_little_giant_small_pumps.htm)



Series 1 pumps (1/150hp) are probably adequate (Better than stock K40 for sure), but have a shutoff around 7' and flows around 200GPH at 1' They would probably give you about .75GPM through the laser depending on how restrictive your system is. With a heat exchanger, long line, flow switch etc, I think a step up is advisable.



Series 2 (1/40hp) 

These are only a few bucks(20) more than Series 1 pumps, and have 3 times the HP, 300GPH @ 1', 205GPH @ 5', and shutoff is up to 11.8' (5.1PSI) so they can easily handle long lengths of tubing, heat exchangers, standard (restrictive)flowswitches, and still keep moving the water at bubble discharging speeds.



The 1/40hp Series 2 pumps are available over here for around $40-50 used and about $100 new.



Beware of the little Giant Black pumps, they are now selling pumps that look to be made by the same company as the stock k40 ones. They advertise 63GPH, but that's @ 1', but shut off @ 2' !!. They're $40, but can't get the water off the floor to the laser. You want the Blue epoxy coated Industrial pumps. They will outlast your laser, and probably you.



Bottomline - as always, my opinion only, I'm sure others will have different views, but a GOOD QUALITY  centrifugal oil filled submersible pump, 1/50HP or larger(>80W) Minimum shutoff pressure of 7', 120GPH @ 5'

That will give you a solid gallon per minute through your tube, even with add-ons. Larger pumps are ok, they can be throttled with an inline valve, but remember the extra wattage contributes to heating your water tank. 



(on that subject I'll be posting my "Home built chiller" using a recycled window AC unit plan in the next few months)



 I like the Little Giant model 2E- XXX (suffix is cord length, screen etc) 

Around $65 in the US



[http://www.ebay.com/itm/LITTLE-GIANT-2E-N-Pump-5-1-2-In-L-3-1-2-In-W-4-3-4-In-H-/331317145027?hash=item4d240a6dc3:g:tqcAAOSwJb9WrCBS](http://www.ebay.com/itm/LITTLE-GIANT-2E-N-Pump-5-1-2-In-L-3-1-2-In-W-4-3-4-In-H-/331317145027?hash=item4d240a6dc3:g:tqcAAOSwJb9WrCBS)









(1PSI=2.3 feet H2O)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 12:07*

**+Scott Marshall** Thanks for that thorough explanation Scott. I see that what you have is definitely overkill, but better than under-rated. And for the $30 bonus price, well worth it. As I work in Litres I had to do quick conversion from gallons, since I have no idea have big a gallon is haha. But, I assume when you're saying Shutoff Pressure of X' you are meaning the vertical flow right? so it will be capable of flowing to X feet high before it is too weak to push it up anymore? I know very minimal about water pump systems. All I remember is from fishtanks, you want to cycle your water about 10 times an hour. But reading through your post, I think I get the gist of it. The pump needs to be rated higher due to the fact that we are feeding it through a tiny tube right?


---
**Scott Marshall** *April 07, 2016 12:09*

My easy conversion is a liter is a quart, 4 quarts/liter per gallon. 



The shutoff spec is how high it can push water before stalling. (rotor slip is using 100% of the flow,  it just sits there and holds the water in the tube.)



Head, or 'shutoff height' converts to psi, 2.3' is 1 PSI. It's is an issue here as it represents how well the pump can move water through a restriction.



Centrifugal pump flow diminishes as the restriction increases, unlike a positive displacement pump (Piston, diaphram) where pressure rises until the pump fails.

If you throttle off a centrifugal pump, it just slips and flows less. It's has advantages and disadvantages. Best here, but the disadvantage is the slip and loss of efficiency.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 12:19*

**+Scott Marshall** So 4 litres to a gallon, that's simple. 



That makes sense about the rotor stalling & just holding the water still in the tube.



Thanks again.


---
**Scott Marshall** *April 07, 2016 12:21*

It can be confusing....


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 12:23*

**+Scott Marshall** Well, I was just thinking a lot of that is over my head at the moment. I think a bit more research into these things is required for me before I understand it. My field of expertise is basically computers. Anything else & I generally have very vague ideas about how it works.


---
**Scott Marshall** *April 07, 2016 12:31*

Once you understand the terms, there's not that much to it.



 Sizing pumps can be more experience than engineering, there's always real world practicality getting in the way. 



I can't even guess how many 'discussions" I've had with fluid engineers thru the years regarding pump sizes. You can't trust manufacturers specs, dirty, small or busy piping is restrictive, that's why I err on the side of plenty extra.

I also have certain manufacturers I know and trust. Little Giant, March, Teel and Gould, depending on what your're doing.



I push the Little Giant because they spec their pumps accurately, they last forever, and you can buy a rebuild kit or seals for every pump they ever made, cheap.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 07, 2016 12:45*

**+Scott Marshall** "Brought to you by Carl's Jr". Just had an amusing thought of that from the movie Idiocracy, where he gets paid every time he says that.



But yeah, in reality a lot of things don't operate at their theoretical specs, due to the real world getting in the way.



I've never heard of those brands, but probably because either they are not as common in Australia or because I don't often have need for water pumps. I'll take your experience & word for it that they are the better manufacturers & see if I can find anything like that locally. There is an irrigation & pump supply shop just around the corner from my home who may stock one of them.


---
**Dennis Fuente** *April 09, 2016 22:54*

Hi Guys so i started working on trying to get all the bubbles out my tube and what i found to work as losing the tube clamps rotating the tube until the outlet pipe was straight up no more bubbles in the tube anywere hope this helps.



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 00:52*

**+Dennis Fuente** Thanks for sharing that Dennis. I checked my tube earlier & it seems that no bubbles anymore for me, but if I get them again I'll try that. Also, if you have your water tank & pump higher than the tube, it will gravity feed into the tube, so when you turn it off/not using it the bubbles won't reappear in the tube. On top of that, I have my outlet tube submerged into the water reservoir as well, so it doesn't create bubbles with the water splashing in.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/cMyLMm4VvY7) &mdash; content and formatting may not be reliable*
