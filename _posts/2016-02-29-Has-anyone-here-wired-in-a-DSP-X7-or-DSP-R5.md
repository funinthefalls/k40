---
layout: post
title: "Has anyone here wired in a DSP X7 or DSP R5?"
date: February 29, 2016 21:48
category: "Modification"
author: "Brien Watson"
---
Has anyone here wired in a DSP X7 or DSP R5?



What wires off of the laser Power supply do the laser firing?





**"Brien Watson"**

---
---
**Brooke Hedrick** *March 01, 2016 13:33*

Yep.  When I had this same question, I Googled for 'k40 laser power supply pinout.'  Here's what I worked from.



[http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-18.html](http://www.cnczone.com/forums/general-laser-engraving-cutting-machine-discussion/256422-cnc-18.html)


---
**Brien Watson** *March 03, 2016 03:36*

Thanks Brooke!  This will be a major help!  Truly appreciate your help! 😃


---
*Imported from [Google+](https://plus.google.com/111917591936954651643/posts/5CrL3C8q9ka) &mdash; content and formatting may not be reliable*
