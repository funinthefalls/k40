---
layout: post
title: "Today only 10% extra off Corel products on their website"
date: July 04, 2016 14:05
category: "Software"
author: "Anthony Bolgar"
---
Today only 10% extra off Corel products on their website. In honour of Canada Day!





**"Anthony Bolgar"**

---
---
**Jim Hatch** *July 04, 2016 14:15*

I thought Canada Day was last week.


---
**Anthony Bolgar** *July 04, 2016 14:18*

Last Day of the sale I guess. Was on the first page of the website a few minutes ago.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/759MJvNcKy6) &mdash; content and formatting may not be reliable*
