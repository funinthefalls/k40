---
layout: post
title: "Incorrect cutting dimensions. Inkscape and K40 Whisperer through an M2 Nano board"
date: May 23, 2018 16:47
category: "Original software and hardware issues"
author: "Duncan Caine"
---
Incorrect cutting dimensions.



Inkscape and K40 Whisperer through an M2 Nano board.



I set up a series of test squares 10mm, 15mm, 20mm, 25mm, 30mm and 35mm.



The X direction dimensions are correct to within approx 0.05mm but the Y direction dimensions are between +0.75mm and +0.90mm. The deviation is not proportionate to size, eg the 10mm is +0.75, the 15, 20 and 25mm are +0.90 and the 30 and 35mm are +0.80



I have increased the cutting area to 610mm x 420mm so I needed new timing belts, and I bought the correct ones from LightObject, and apart from that I am using all the original parts including the stepper motors.



This isn't critical at the moment but if anyone has any ideas please let me know.





**"Duncan Caine"**

---


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/cFDxq6bXXCK) &mdash; content and formatting may not be reliable*
