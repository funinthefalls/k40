---
layout: post
title: "Anyone recommend a good controller upgrade? Is the lightobject Lo x7 a good investment ?"
date: January 29, 2017 23:15
category: "Modification"
author: "Russ \u201cRsty3914\u201d None"
---
Anyone recommend a good controller upgrade?  Is the lightobject Lo x7 a good investment ? 





**"Russ \u201cRsty3914\u201d None"**

---
---
**tyler hallberg** *January 29, 2017 23:24*

Cohesion3d


---
**Don Kleinschnitz Jr.** *January 29, 2017 23:26*

I started down the LO x7 road and glad I did not. Its still proprietary control which diminishes your tool chain options.

I like my smoothie .....


---
**Ariel Yahni (UniKpty)** *January 29, 2017 23:30*

**+Russ None**​ [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://www.cohesion3d.com) by **+Ray Kholodovsky**​


---
**Russ “Rsty3914” None** *January 30, 2017 00:45*

No lcd though ? I have k40 laser with M2nano board...




---
**Brooke Hedrick** *January 30, 2017 02:14*

Lo x7 has been working great for me.  It has been about a year.  I upgraded from stock MoshiBoard and MoshiSoft.  I can't speak to other options as I haven't tried them.



I did the X7 DSP upgrade kit for K40/D40, plus extra motor controller, z bed, and upgraded lens.


---
**Russ “Rsty3914” None** *January 30, 2017 02:31*

Thanks all !


---
**Russ “Rsty3914” None** *January 30, 2017 02:32*

Brooke, do I need to replace stepper motors with the lo x7 kit?


---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 02:46*

**+Russ None** sure the Cohesion3D mini can support an LCD! You would just add the GLCD adapter to the options for the mini laser bundle, and then snag a "reprapdiscount 12864 GLCD" off amazon to use with it. 


---
**Russ “Rsty3914” None** *January 30, 2017 04:22*

I think the lo x7 is overkill...looks like Cohesion may be the way to go...

Do I upgrade stepper motors  too ?  Ethernet available ?


---
**Brooke Hedrick** *January 30, 2017 05:32*

**+Russ None** I didn't replace the x/y motors.  I found step-by-step instructions online on how to switch from stock to the LO 7.  It took a couple weeks of evenings and a weekend to complete.  I did have to go through re-alignment.


---
**Brooke Hedrick** *January 30, 2017 05:36*

If I hadn't gone the LO route, I would have looked more closely at smoothie as it seems to have the best community support on this forum. 


---
**Russ “Rsty3914” None** *January 30, 2017 12:02*

**+Ray Kholodovsky**  Is there a Ethernet Module available for the Cohesion3d card ?  I see the more advanced one has it... I would like to have all the new features and be compatible with the Laser...But rewiring laser is ok...




---
**Ray Kholodovsky (Cohesion3D)** *January 30, 2017 15:51*

**+Russ None** no, you just swap the stock board with the new one. 

Ethernet - it's the "Lan8720" module from China. I don't make my own yet. Laserweb does not support smoothie Ethernet, you would use LaserWeb to set up the job and save the gCode, then the smoothie web interface to upload the gCode file to the board and hit run. 


---
**Scott Thorne** *January 30, 2017 23:20*

Hey **+Ray Kholodovsky**...i need your help bad bro....678-877-0944...i need a 1.7vdc at 12 amps 980nm diode driver....you are the guru name your price man...lol


---
**Russ “Rsty3914” None** *January 31, 2017 02:37*

So if I go down the road with the Cohesion3d board.  What software is required ?

I think I will get the Remix board..Latest one right ?


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 02:51*

The ReMix doesn't have the laser specific connectors on it. So if the machine you get has a ribbon cable, it's more work for you to wire. 

You can use any CNC style software that outputs gCode, we recommend the amazing LaserWeb. 


---
**Russ “Rsty3914” None** *January 31, 2017 02:59*

I have Mach3


---
**Russ “Rsty3914” None** *January 31, 2017 02:59*

anyone got a breakout pic for the Ribbon cable...I think that needs to be replaced anyhow...I can see issues further down the road...


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 03:00*

I recommend LaserWeb. It's free and very well supported and used in the K40 and Cohesion3D communities. 


---
**Russ “Rsty3914” None** *January 31, 2017 03:02*

What is the difference between the mini board and the remix board... ?

I did order the Z table upgrade...


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 03:10*

Both support the range of 3D Printer, CNC's, Lasers, and other machines out there.

Mini will handle a 3D Printer with a Single Extruder and Heated Bed, a CNC, or a laser cutter.  It is specifically the size of the stock M2Nano board in the K40 and has the K40 specific connectors for a drop in upgrade.

ReMix has 6 motor sockets so you can have a 3D Printer with multiple heads, a more advanced machine like a Pick n Place, and it has more outputs for other peripherals as well.  

Other things - ReMix directly supports the Graphic LCD, Mini needs an adapter for it.


---
**Russ “Rsty3914” None** *January 31, 2017 03:51*

Does the remix have better control ?


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 04:00*

Both run the same firmware and microprocessor so the motion output should be the same.  If that's not what you meant please clarify the question.


---
**Russ “Rsty3914” None** *January 31, 2017 04:24*

Exactly...I thought the Remix was the 32 bit processor, but they are the same.  So the stepper breakout boards are for using separate Stepper controller I would assume.  I would need a 2A driver for my Z table. Thanks for the help...I think this is the best way to go.  Cheaper and full of support...

Is the Mini available for order ?


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 04:27*

Yepp, same brains in both 

Indeed, grab an additional external stepper adapter so you can use a "black box driver" (or 2 if you are also thinking rotary, LO says you need an external driver for that as well) 

Mini - we sold out the first batch and the next batch has already been ordered with a 4-6 week lead time. I am expecting to start shipping again late February.  

ReMix is in stock now and ships quickly. 




---
**Russ “Rsty3914” None** *January 31, 2017 18:15*

I will order the Mini when available..

Thanks !


---
**Ray Kholodovsky (Cohesion3D)** *January 31, 2017 18:17*

It is available for order now, and will ship at the previously mentioned February timeframe.  I recommend you place the order sooner rather than later to ensure your place in the batch. 


---
**Russ “Rsty3914” None** *January 31, 2017 18:27*

Got it !

Thanks !


---
**Russ “Rsty3914” None** *January 31, 2017 18:31*

Done !


---
**Russ “Rsty3914” None** *February 03, 2017 18:19*

Completely taken apart the k40 and I want to say I see why the price is so low !   I stiffened the Bottom of the enclosure up with a sheet of 1/4" 8081 aluminum.  No more wobbling...Solid !  Now secondly I need to prevent the belt from falling off on the X axis....


---
**Russ “Rsty3914” None** *February 05, 2017 02:51*

WHere do i find the download for Laserweb4 ?


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2017 02:53*

[laserweb.xyz](http://laserweb.xyz) is for LW3.  This is what is currently being used.  LW4 is still in development.


---
**Russ “Rsty3914” None** *February 05, 2017 02:57*

where is the install file...Im lost !


---
**Ray Kholodovsky (Cohesion3D)** *February 05, 2017 02:58*

Read: [github.com - LaserWeb3](https://github.com/LaserWeb/LaserWeb3/wiki)


---
**Russ “Rsty3914” None** *February 07, 2017 11:52*

Cant wait for my Cohesion3d...


---
**Matthew Wilson** *February 12, 2017 14:19*

So can i buy the cohesion 3d with the glcd adapter and continue to use the stock display until I can eventually buy the lcd display? Basically, is the glcd adapter pre wired onto the cohesion? 




---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 19:02*

Stock display? Do you mean the digital power readout that some machines come with? 

That's completely separate. You should get the GLCD adapter and get the 12864 GLCD off amazon. 


---
**Matthew Wilson** *February 12, 2017 23:16*

I cant afford the glcd display at this time. Can I buy the cohesion 3d with the glcd adapter and use just the cohesion 3d until I can afford the glcd display?


---
**Matthew Wilson** *February 12, 2017 23:19*

My machine came with this

![images/f759d18e0c101211f5a8a476ad9af879.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f759d18e0c101211f5a8a476ad9af879.png)


---
**Ray Kholodovsky (Cohesion3D)** *February 12, 2017 23:53*

You can get the GLCD adapter, it is specifically for the LCD I mentioned. You can certainly run without the display, I do believe it makes things more convenient though and hope you can get one soon. 


---
**Matthew Wilson** *February 13, 2017 19:50*

Last question...for now. Do I buy the cohesion 3d mini with this setup?

![images/74bcbfaf99927976e63cd792d65b5707.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74bcbfaf99927976e63cd792d65b5707.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 19:56*

Yep. That's the board and wiring that my k40 has. :)


---
**Matthew Wilson** *February 15, 2017 04:30*

Ordered the cohesion3d. I also ordered the glcd screen. Can't wait for them to get here. Can't thank you enough Ray Kholodovsky. 


---
**Russ “Rsty3914” None** *February 15, 2017 15:35*

How about lenses ?

I did order that...


---
**Russ “Rsty3914” None** *February 15, 2017 15:36*

Superior Focal lense and mirror set ?


---
*Imported from [Google+](https://plus.google.com/107513244648214755501/posts/7VJVCucnVvQ) &mdash; content and formatting may not be reliable*
