---
layout: post
title: "noob here.. received a k40 laser.. blue and white version last week.."
date: May 13, 2017 13:41
category: "Original software and hardware issues"
author: "Blue Ridge"
---
noob here.. received a k40 laser.. blue and white version last week.. got it setup and tested laser with the 'test' button on the panel.. laser fired.. i did the mirror alignment using the test button and then installed the corel software that was included. I set the mainboard ID number into the software and selected the model number from the drop down box.. plugged in the USB key then the usb cable going to the laser cutter.. at the bottom of my systray a window says 'engraver attached'.. however when i click to engrave and it pops up at the bottom of the systray and says 'sending file to engraver' showing precentage sent..etc.. the laser cutter will start moving the laser head as if it is engraving but the laser never fires.. i've double checked voltages and wiring.. everything seems to be fine.. even sent my photos to a friend who also has the same model and wiring looks identical.. what could be the issue.. ? any help greatly appreciated.. 





**"Blue Ridge"**

---
---
**Don Kleinschnitz Jr.** *May 13, 2017 14:00*

can't comment on the corel setup as I do not use that anymore.



You can test the controllers control of the laser by disconnecting what i on "L" and then grounding that pin. If the laser fires the input to the supply is working and the problem is elsewhere. If it does not something is wrong with the control.



BTW I assume when you run the machine the Laser Switch is on?


---
**Ned Hill** *May 13, 2017 19:30*

Sounds like you have a correct setup for coreldraw.  Can't recall anything in the coreldraw setup that would really replicate this.



One question though, what power are you engraving with?  If it's too low the laser won't fire.


---
**Phillip Conroy** *May 13, 2017 20:02*

My k40 can with the wrong/faulty laser switch [h.it](http://h.it) was a moment tyr switch ie press and hold in  for laser to fire 


---
**Blue Ridge** *May 14, 2017 03:11*

ok i figured it out today with help from a member of a facebook group.. we messaged back and forth with pics of the wiring.. he was testing the voltages and then had me test them too.. found a wire to the main board had no continuity.. i removed the whole main board to test.. narrowed it down to the wire had broke inside the insulation where they crimped it onto the connector pin.. i depinned it.. soldered on a new wire.. tested continuity and then put it back in the connector and everything back together.. tested voltage and finally was getting a reading.. sent a file from corel and it worked.. :) 


---
*Imported from [Google+](https://plus.google.com/116885267382065753636/posts/TvTjRi9uf3J) &mdash; content and formatting may not be reliable*
