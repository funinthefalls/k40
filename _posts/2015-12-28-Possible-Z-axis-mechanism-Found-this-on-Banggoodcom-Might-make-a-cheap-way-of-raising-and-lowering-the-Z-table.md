---
layout: post
title: "Possible Z axis mechanism Found this on Banggood.com Might make a cheap way of raising and lowering the Z table"
date: December 28, 2015 14:00
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Possible Z axis mechanism



Found this on Banggood.com Might make a cheap way of raising and lowering the Z table. Could add a stepper or extend the handle shaft to outside the laser case and put a hand crank on it. Any thoughts from the group?



[http://www.banggood.com/44-inch-Lab-Lift-Lifting-Platforms-Stand-Rack-Scissor-Lab-Lifting-Aluminum-Oxide-p-1022996.html?utm_design=41&utm_source=emarsys&utm_medium=Mail_men163_email&utm_campaign=newsletter-emarsys&utm_content=Echo&sc_src=email_1650804&sc_eh=a3df38e39c7d24321&sc_llid=250897&sc_lid=77188107&sc_uid=NdjrF7Uti7&emst=NdjrF7Uti7_250897_1650804_20](http://www.banggood.com/44-inch-Lab-Lift-Lifting-Platforms-Stand-Rack-Scissor-Lab-Lifting-Aluminum-Oxide-p-1022996.html?utm_design=41&utm_source=emarsys&utm_medium=Mail_men163_email&utm_campaign=newsletter-emarsys&utm_content=Echo&sc_src=email_1650804&sc_eh=a3df38e39c7d24321&sc_llid=250897&sc_lid=77188107&sc_uid=NdjrF7Uti7&emst=NdjrF7Uti7_250897_1650804_20)





**"Anthony Bolgar"**

---
---
**Arestotle Thapa** *December 28, 2015 14:19*

I'm guessing the handle sticks out toward you when you raise the platform so you'd need to have some clearance on the side of the handle. Extending the handle outside of the case will require a vertical cut instead of just one hole. I'd think it will work with the stepper!


---
**Anthony Bolgar** *December 28, 2015 14:24*

The vertical cut wouldn't be too hard to make, and I would line both sides of it with brushes that overlap so no stray laser beam could make it through the slit. The base would also have to be bolted down because most of the Z table would overhang the edges of the platform. I just ordered one so I will let you know how it goes. I might have to get a second one and connect the 2 shafts with pulleys and a belt if one proves to be too weak or wobbly.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2015 15:33*

That's an interesting idea for a Z-table. I'd be interested to see it in action connected to a stepper motor.


---
**Gary McKinnon** *December 29, 2015 11:44*

Nice find.




---
**Gary McKinnon** *December 29, 2015 11:45*

Found some more on Ebay UK :



[http://www.ebay.co.uk/sch/i.html?_from=R40&_trksid=p2050601.m570.l1313.TR0.TRC0.H0.Xlab+lift.TRS0&_nkw=lab+lift&_sacat=0](http://www.ebay.co.uk/sch/i.html?_from=R40&_trksid=p2050601.m570.l1313.TR0.TRC0.H0.Xlab+lift.TRS0&_nkw=lab+lift&_sacat=0)




---
**Coherent** *December 29, 2015 12:27*

It should work. There have been others here who have bought or mentioned them. They are better known as Lab Jacks and come in any size (and price) you want. Like others mentioned the adjustment screw is centered so raises when the table is lifted, however I have seen some more expensive ones that the screw is on the bottom and stays stationary. You'll need a separate stepper driver to motorize it. Or possibly a simpler reversible small geared motor with a 2 way toggle switch. Something like the motors used for microwave ovens come to mind... they are geared slow, are reversible and cheap.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/BhbBMqJNYwZ) &mdash; content and formatting may not be reliable*
