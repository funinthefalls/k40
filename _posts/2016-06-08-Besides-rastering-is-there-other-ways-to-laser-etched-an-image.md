---
layout: post
title: "Besides rastering is there other ways to laser etched an image?"
date: June 08, 2016 03:56
category: "Discussion"
author: "Derek Schuetz"
---
Besides rastering is there other ways to laser etched an image? My tiny diode laser didn't seem to raster but instead just stay on for image and turn off for no image ( can't think of right term right now)





**"Derek Schuetz"**

---
---
**Ariel Yahni (UniKpty)** *June 08, 2016 04:04*

you can do raster engrave, so vector the image and then convert each color into small lines


---
**Derek Schuetz** *June 08, 2016 04:10*

How would I do that in inkscape?


---
**Ariel Yahni (UniKpty)** *June 08, 2016 04:21*

Yes you can in Inkscape. You need something like D in this link [http://graphicdesign.stackexchange.com/questions/39705/cutting-multiple-lines-curves-with-a-closed-path-in-inkscape](http://graphicdesign.stackexchange.com/questions/39705/cutting-multiple-lines-curves-with-a-closed-path-in-inkscape)


---
**Ariel Yahni (UniKpty)** *June 08, 2016 04:21*

There is a way to create multiple parallel lines but don't remember how it's. Called


---
**Derek Schuetz** *June 08, 2016 06:07*

**+Ariel Yahni** that is what I think I want 


---
**Ariel Yahni (UniKpty)** *June 08, 2016 12:17*

**+Derek Schuetz** this is the easy solution

[http://wiki.evilmadscientist.com/Hatch_fill](http://wiki.evilmadscientist.com/Hatch_fill)


---
**HalfNormal** *June 08, 2016 15:55*

**+Derek Schuetz**  [picengrave.com](http://picengrave.com)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/MwxPWYX8LTq) &mdash; content and formatting may not be reliable*
