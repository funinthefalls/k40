---
layout: post
title: "So I am looking at upgrading my k40 to the big red 60w laser on eBay I was wondering if anybody has this laser and if laser web can be used on them"
date: January 17, 2017 01:23
category: "Discussion"
author: "3D Laser"
---
So I am looking at upgrading my k40 to the big red 60w laser on eBay I was wondering if anybody has this laser and if laser web can be used on them 





**"3D Laser"**

---
---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 01:55*

Post pictures of the control board and wiring. Typically the board has to be upgraded to a open source gcode type one for use with LW. 


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 02:00*

I'm sorry, I didn't realize at first that you don't have the machine yet. Can you provide an example link though? 


---
**3D Laser** *January 17, 2017 12:00*

**+Ray Kholodovsky** [ebay.com - Details about  60W CO2 Laser Tube Laser Engraving Engraver Cutting Machine Laser Cutter](http://m.ebay.com/itm/60W-CO2-Laser-Tube-Laser-Engraving-Engraver-Cutting-Machine-Laser-Cutter-/221919093687?hash=item33ab68a3b7:g:6BgAAOSwdIFXxBs8&_trkparms=pageci%253A4e62c40f-dcac-11e6-9efc-74dbd180d04c%257Cparentrq%253Aac4b3c331590a78846953710ffb91160%257Ciid%253A1)


---
**Tony Schelts** *January 17, 2017 13:58*

This looks identical to the one I bought on ebay, I'm in the Uk and it was sent from Germany.  None of the sockets on the back of my machine was any good.  Different size, etc.  There where several small issues I had with mine,  I never used the on board sockets because of the issues I have seen people have online.  The waster sensor kept sending an Error 1 which means the flow of water isnt working well.  After getting help on a forum, it turned out to be the water hose around the tube had been wrapped about 5 times and there was a kink in the tube.  I undind the tube from the inlet and untangled it.  It now works. I have really done upgrades,  It comes with RDWorks and also the standard plugin for corel draw etc. My machine paid for itself in about 2 months. Anyway, you pay for what you get but I'm glad I bought mine. 




---
**3D Laser** *January 17, 2017 14:04*

**+Tony Schelts** just curious what do you make 


---
**Tony Schelts** *January 17, 2017 14:11*

Just things like tee light holders coasters, keyrings bookmarks nothing to difficults also decorative cubes for LED lights and I have just started making clocks. I haven't started selling on line yet.  I was going to a local market most saturdays before xmas averaging 100-160.00 each time.  Not amazing money but worth it. I have a full time job so finging time to make stuff and sell is a hasle at times.  I love my laser even with the small faults.


---
**Ian C** *January 17, 2017 21:08*

Hey. I've got the 80 watt version and like the linked laser uses a Ruida controller and RDworks to communicate. There is various plugins as usual, I can't remember all of them off the top of my head, but there is a Corel one as expected and one for AutoCAD but yet to get this one working. I'm not sure it'll work from Laserweb, I've not tried or even used this software myself. As above there are faults with the machine, much like the K40, but it is a much better machine to. I've just rewired the rear sockets so they are useable and powered by a seperate power lead to avoid load issues. I've got them connected to an SSR and triggered by the wind output on the Ruida controller to switch my air assist and soon to be added extractor fan when I receive my delayed off timer relay.


---
**3D Laser** *January 18, 2017 01:49*

**+Ian C** if I do get this one I might be asking you for help especially with rewiring the sockets 


---
**Ian C** *January 18, 2017 12:05*

**+Corey Budwine** No worries buddy, just give us a shout. I'm still new to the machine, had it 7 weeks now, but getting along fine with it.

![images/64fca76b172232017e9ec2f528b26d60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64fca76b172232017e9ec2f528b26d60.jpeg)


---
**Ian C** *January 18, 2017 12:05*

![images/840b8486cd887c1bd0d62a3a3e8437ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/840b8486cd887c1bd0d62a3a3e8437ad.jpeg)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/2xUZP3yzjM7) &mdash; content and formatting may not be reliable*
