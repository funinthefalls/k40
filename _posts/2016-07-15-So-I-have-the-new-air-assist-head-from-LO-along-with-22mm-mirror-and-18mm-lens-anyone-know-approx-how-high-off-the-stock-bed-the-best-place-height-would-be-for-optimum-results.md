---
layout: post
title: "So I have the new air assist head from LO along with 22mm mirror and 18mm lens - anyone know approx how high off the stock bed the best place height would be for optimum results?"
date: July 15, 2016 23:06
category: "Discussion"
author: "greg greene"
---
So I have the new air assist head from LO along with 22mm mirror and 18mm lens - anyone know approx how high off the stock bed the best place height would be for optimum results?  As a test I ran the ramp and found it to be a finer beam closer to the nozzle - but a hotter beam back at the original deck height.

I also examined the  old mirror and lens and found the old beam was hitting the side of the mirror and off center on the focus lens.  You may want to check yours !





**"greg greene"**

---
---
**Ariel Yahni (UniKpty)** *July 15, 2016 23:43*

The specs high would be on your lens, 50.8mm. Still the ramps test is the best 




---
**greg greene** *July 15, 2016 23:45*

Thanks, but its confusing I expected that the hotter beam would be closer to the 18mm area and not the 50mm area


---
**Ariel Yahni (UniKpty)** *July 15, 2016 23:50*

The best value is where the spot is more concentrated and thus where the focus is. When you says hotter you means a thicker line? If thats so then that where is unfocused, could be hotter but will not cut the same


---
**greg greene** *July 15, 2016 23:55*

Ah - yes that's what I meant - thanks I see how to read it now.


---
**John Austin** *July 20, 2016 05:20*

**+Ariel Yahni**



Could you explain the ramp test? watched a video and all they was laser starting at bottom and going to the top. Not sure what to be looking for.

 


---
**Ariel Yahni (UniKpty)** *July 20, 2016 05:28*

**+John Austin**​ sure. Draw a very thin line, maybe 0.025mm thick. Then place it on the center of your software. Get a piece of Material, I prefer wood and place it so when you run the laser it will go left to right. The most important thing is to elevate that piece of wood on either left or right,  like a ramp. This ramp should just fit between the bed and the laser head. Run the line and inspect. You are looking for the thinnest representation of the line burned. That's the sweet spot ( focus) then you will need to find a way to measure from the bed to the top of the material you want to cut/engrave to be the same distance as off this thin part of the line. Let me know if it's clear now


---
**John Austin** *July 20, 2016 05:31*

**+Ariel Yahni**

 Thanks for the explanation. That helped a lot on understanding what to look for.

I just order the laser and will be here this weekend.


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/dWeR2hqb7Zj) &mdash; content and formatting may not be reliable*
