---
layout: post
title: "Finished all my mods to my K40 just need to find a better adjustable bed that's cheaper then the light object one"
date: June 23, 2016 16:55
category: "Discussion"
author: "Derek Schuetz"
---
Finished all my mods to my K40 just need to find a better adjustable bed that's cheaper then the light object one



![images/e5631b762c06958a1ac076ceee0c1862.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5631b762c06958a1ac076ceee0c1862.jpeg)
![images/e1a4f13b84009097fa45c3d448f37cee.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/e1a4f13b84009097fa45c3d448f37cee.gif)
![images/f80f6ac7d7645db9194e811f0401c157.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f80f6ac7d7645db9194e811f0401c157.jpeg)
![images/7b6a494d1023067ffd6a7dc80e751279.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b6a494d1023067ffd6a7dc80e751279.jpeg)
![images/c98872295f6a77147d3edf83bc1c2772.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c98872295f6a77147d3edf83bc1c2772.jpeg)

**"Derek Schuetz"**

---
---
**Stephen Sedgwick** *June 23, 2016 17:23*

are you using a arduino controller there?


---
**Derek Schuetz** *June 23, 2016 19:51*

**+Stephen Sedgwick** ramps 1.4 


---
**Stephen Sedgwick** *June 23, 2016 20:01*

have you done engraving with it?


---
**Derek Schuetz** *June 23, 2016 20:02*

Labels on the panel and some other small things but I have not figured out inkscape entirely for rastering


---
**Stephen Sedgwick** *June 23, 2016 20:34*

that is something recently someone was saying is rastering might not work like it would with some of the other higher end systems... that is what I am trying to validate as it doesn't have the ability to adjust power on direction changes the same...


---
**Derek Schuetz** *June 23, 2016 21:02*

If you go smoothie it can handle it. Rastering works but you can tell that it has some memory issues


---
**Stephen Sedgwick** *June 23, 2016 21:07*

I am going to be going to smoothie based on conversations with Ray and some others - as he has just got this installed I didn't know if he knew more about it yet as I don't know anyone to specifically talk with about it..


---
**Michael Audette** *June 24, 2016 16:12*

Derek,  What firmware are you running?  Did you need to mod the X/Y limit switches or are you using the stock ones still?  Does is work over USB or SD card only (i.e. load SD from ponterface then cut)?  I'd be curious.  Serial/USB almost always locks it up and sometimes even SD does.  Is it possible for you to send me the hex you have loaded on yours so I can see if it's a compile problem?


---
**Derek Schuetz** *June 24, 2016 16:14*

**+Michael Audette** I'm running Marlin. I do all my printing from SD card. Limit switches are all stock. Can work over USB but I don't like the chance of a stall with a burning laser. And what do you mean by hex


---
**Michael Audette** *June 24, 2016 16:27*

Well,  The hex file uploaded to the Mega from the Arduino IDE (i.e. the firmware binary compiled).   I've been fighting with my K40 w/ ramps 1.4 for a while now.  Manual controls and jogging seem to work...however mastering it doesn't keep the y stepper engaged and occasionally it'll get into a state where it won't respond (even with the SD) - once the program finishes it doesn't seem to want to say the program is done).  I may throw another MEGA at it to see if it's the board.



On the ramps did you need to jumper any of the other endstop connectors?  (Maybe a high-res snap of your board in that area).



I also wouldn't mind seeing your start/end code as that seems to be a factor on my setup as well.   I've been trying to get it to reliably move for a couple weeks now....under Gcode control.  Manually it works fine (moving axis with the firmware).


---
**Derek Schuetz** *June 24, 2016 17:12*

Ya i can send some files when I get home today after 5pm pst


---
**Alex Hodge** *July 20, 2016 15:23*

Derek, are those laser pointer holders available on thingiverse?


---
**Derek Schuetz** *July 20, 2016 19:19*

Ya they are but I had to modify them to hold he lasers I bought and am using it different then the original design


---
**Alex Hodge** *July 20, 2016 19:24*

Still have the STLs? You use 6mm instead of 12mm lasers or what?


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/ExwBYydXsKT) &mdash; content and formatting may not be reliable*
