---
layout: post
title: "# K40Peltier cooling system Steve Clark I started a new post as the other was getting pretty busy"
date: February 09, 2018 16:48
category: "Modification"
author: "Don Kleinschnitz Jr."
---
# K40Peltier cooling system



**+Steve Clark**



I started a new post as the other was getting pretty busy.



Here is a link to the schematic of your cooling system based on your drawing. 



I used yours drawing but did do some mods :).

...Added some more fuses and a automobile fuse module ( I like blade fuses)

...Added some more terminal blocks

...Added part equivalents from amazon (so you can buy from one place)

...Added 2x DVM's to measure peltier voltage and 2x LED indicators to monitor fan voltage

...Added snubber diodes (I guessed at the specs)

...Added modular input plug

...Added and alternate source for the peltier from amazon (pls check and let me know if you think they are ok)



I added most of your actual parts as I could find them on the web, still some to add.



<b>Please review and let me know what you would like changed...</b>



[https://www.digikey.com/schemeit/project/k40-peltier-cooler-FAQ9S78301K0/](https://www.digikey.com/schemeit/project/k40-peltier-cooler-FAQ9S78301K0/)







**"Don Kleinschnitz Jr."**

---
---
**Steve Clark** *February 09, 2018 19:46*

Nice Don ...I'll look it over tonight!


---
**Steve Clark** *February 12, 2018 17:16*

Don, I've gotten tied up on another project (I'm getting paid for-grin)  all of this week. I'll be later than I thought in responding to you. Sorry. I will get back to it as soon as I can.



A couple of notes here:



Your showing a 4 chip peltier unit a little more cooling per bank and 10 bucks more in cost each. The three chip bank seems to come and go on e-bay and both the 3 chip and 4 chip will work with the rest of the system (right?). 



Based on what I've seen in my system so far, someone who lives in cooler climates might see if they could get away with just the one 4 chip bank. It reduces the cost a bit and they could always add the second later. 


---
**Don Kleinschnitz Jr.** *February 12, 2018 17:49*

**+Steve Clark** no worries and no hurry :).


---
**Steve Clark** *February 12, 2018 18:09*

**+Don Kleinschnitz**  whoops your too fast...;) I edited my other post after yours


---
**Don Kleinschnitz Jr.** *February 12, 2018 19:27*

**+Steve Clark** yes I will mod the drawing to show 4 chips/fans and users can add another bank as needed. I do not see that any other mods are needed to the system.


---
**Don Kleinschnitz Jr.** *February 13, 2018 00:07*

**+Steve Clark** I added modules to each bank. I likely mucked something up so please take another look.


---
**Steve Clark** *February 13, 2018 02:12*

**+Don Kleinschnitz** My link shows not change?


---
**Don Kleinschnitz Jr.** *February 13, 2018 04:24*

**+Steve Clark** [https://www.digikey.com/schemeit/project/k40-peltier-cooler-FAQ9S78301K0/](https://www.digikey.com/schemeit/project/k40-peltier-cooler-FAQ9S78301K0/)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/GctVUPr38Pv) &mdash; content and formatting may not be reliable*
