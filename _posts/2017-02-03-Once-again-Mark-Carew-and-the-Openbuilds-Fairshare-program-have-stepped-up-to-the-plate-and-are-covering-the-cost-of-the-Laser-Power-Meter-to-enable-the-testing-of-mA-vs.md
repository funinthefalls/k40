---
layout: post
title: "Once again, Mark Carew and the Openbuilds Fairshare program have stepped up to the plate and are covering the cost of the Laser Power Meter to enable the testing of mA vs"
date: February 03, 2017 09:59
category: "Discussion"
author: "Anthony Bolgar"
---
Once again, **+Mark Carew** and the Openbuilds Fairshare program have stepped up to the plate and are covering the cost of the Laser Power Meter to enable the testing of mA vs. life expectancy of laser tubes.



I have almost completed designing the testing procedure to determine if running a CO2 laser tube at low mA actually prolongs the life of the tube vs running at high mA. The hypothesis is that while running at high mA, the shortened tube life is made up by the increased mm/s higher mA would allow. I have 2 identical machines to run the experiment on, so the data collected should be fairly accurate. This will be a long term project as the tube life may be in the hundreds if not thousands of hours of run time. But I will provide weekly power outputs for both variants, and hopefully we can see an emerging trend quickly.



Once again, thanks to Mark and the Openbuilds team!





**"Anthony Bolgar"**

---
---
**Brandon Satterfield** *February 03, 2017 14:45*

Will stay in tune with this project Anthony.


---
**Anthony Bolgar** *February 03, 2017 16:37*

Data collected from this experiment will be made available on a weekly basis at [CO2 Laser Tube Lifespan](https://plus.google.com/u/0/communities/102696041747630830114) Please feel free to join this community if you are interested in this experiment.


---
**HP Persson** *February 04, 2017 09:16*

I would like to see coolant temperatures being monitored too, if one machine runs with high temps and the other with low, and different mA settings the result will be off.



Aswell as using crappy water will push the results either direction of tube life span, i did a test where we could see up to 40% loss with crappy coolants and the regeneration process of the Co2 from CO and O2.

(still plotting the data into graphs though, nothing to show yet)


---
**Don Kleinschnitz Jr.** *February 04, 2017 13:06*

**+HP Persson** Your are saying that the type of coolant can reduce laser life?

What do you thing the reason and failure mechanism is over good coolant? 

What do you consider crappy coolant?


---
**HP Persson** *February 05, 2017 10:38*

**+Don Kleinschnitz** Yes, Charge of the coolant interferes with the regeneration (recombination) of Co2 inside the tube. 

With a high input power and lowered discharge (as some goes to charge the conductive water) regeneration doesn't happen in the rate needed.



Short description: The laser beam is created by breaking down the molecules in the gas in different steps, the catalyst-discharge recombines the Co2 gas.

The breakdown of molecules is higher than the recombination, weakening the tube.

How quick, how much, i have not tested it, so many variables to that answer.

And this balance isn't 1:1, if it where, we would never have to change our tubes. But conductive water make it worse.



Crappy coolant to me is conductive water, anything above 5-10μS/cm in my personal opinion. At what level it starts to be a issue, i have no answer to it. Yet.



So i´m really excited about the power measuring Anthony preparing for, it may go hand in hand with my results as overpowering is a similar effect (more breakdown, less recombination) :)


---
**Don Kleinschnitz Jr.** *February 05, 2017 14:56*

**+HP Persson** MMMM. Is this a known mechanism or are you concluding this from your results. Do you have a reference I can read up on this phenomena.



The coolant is isolated from the Co2 and ionization by the glass bulb. It would not seem that there would be much current flow through the glass?



I have found evidence in a real failure situation that conductive fluids enable a weak electrical point in the front seal and the tube will actually arc through the seal and then eventually fail. I wonder if these are related? In this case is hasn't to do with the charge of the coolant (other than it is a conductor) but that there is a conductive path created for the high voltage.


---
**HP Persson** *February 05, 2017 17:46*

**+Don Kleinschnitz** I´ll push up a new thread about it tomorrow and i´ll show you some more data ;)

Tube and PSU failures are interesting topic, how we can find more knowledge and prolong our machines, and keep cost down :)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/NmC3obBkRQv) &mdash; content and formatting may not be reliable*
