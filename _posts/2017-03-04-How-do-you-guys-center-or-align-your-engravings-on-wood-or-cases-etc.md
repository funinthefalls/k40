---
layout: post
title: "How do you guys center or align your engravings on wood, or cases etc?"
date: March 04, 2017 04:27
category: "Discussion"
author: "Robert Selvey"
---
How do you guys center or align your engravings on wood, or cases etc? I'm On a stock board using corel laser.





**"Robert Selvey"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 04, 2017 04:44*

When I was still on stock board/CorelLaser I would create a rectangle around the engraving image with no border & no background fill. This rectangle would be the size of the material (e.g. if your phone case is 180 x 80 mm, then the rectangle would be 180 x 80 mm). Then I would centre the engrave image in that rectangle (with the rectangle below the engrave image). Then I'd select it all & send it to the engrave option on CorelLaser. Because it is no border & no background fill, it will not engrave, but CorelLaser plugin still "sees" it for positioning purposes. Same goes for cutting, you can use the no border no background fill rectangle to assist with positioning.


---
**Phillip Conroy** *March 04, 2017 06:16*

You can do the same thing by having a small line in top left corner and design the cut and engravering image as one .to engrave delete what you want to cut,engrave,then undelete the cut lines and delete the engraving lines


---
**Mark Brown** *March 04, 2017 12:56*

[instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](https://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)



Are you trying to engrave then cut, or engrave on already cut pieces?


---
**Robert Selvey** *March 04, 2017 13:33*

How do you align an engraving on the item your engraving ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 05, 2017 02:00*

**+Robert Selvey** In addition to what I've said above, I have placed a rectangular piece of 3mm plywood as far back in the top-left corner & cut from 0x & 0y position as wide & as tall as possible, so I have an L-shaped piece leftover. This is used to push my workpiece up against to ensure that it is always starting in the 0,0 position.


---
**Gavin Dow** *March 09, 2017 15:44*

**+Robert Selvey** I typically draw a box around my engraving roughly the same size or slightly larger than my item, and use a very low power cut to etch a piece of cardboard. This gives me a "fixture line" to put my part into for alignment.

For instance, if I'm doing a round object, I just measure the object, make a circle in CorelDraw that's a hair larger, and center it on my engraving. I run a cut on just the cardboard at 10mm/s and 10-15% power that includes only my  fixture line (and my reference dot in the top left corner, see **+Twelve Foot**'s Instructable link for more info on that). I usually tape my cardboard down before this cut, so it can't move while I load the part.

I just eyeball the part into this circle - it's pretty easy to get this near enough that no one will notice the difference. Lastly, run your engrave pass on whatever settings, and don't forget to include your reference dot so the two passes line up with eachother. Boom!

If you're doing a large lot of parts, I would probably make a fixture using acrylic or wood that I could drop the part(s) into, and removing any possibility of human error. But for one-off jobs, an eyeball is close enough.


---
**Robert Selvey** *March 09, 2017 17:07*

I can't seem to find twelve foots instructions ?




---
**Gavin Dow** *March 09, 2017 18:08*

**+Robert Selvey** [instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/3ZR6RU5M1t2) &mdash; content and formatting may not be reliable*
