---
layout: post
title: "I just bought one of theese on ebay, and got it home yesterday"
date: March 08, 2016 19:21
category: "Hardware and Laser settings"
author: "Martin Larsen"
---
I just bought one of theese on ebay, and got it home yesterday.

It has RDWORKS and CORELsuite12 on a cd, but drivers wont install. :(



Im beginning to think that it doesnt support 64bit OS since my computer wont recognice the cutter, and it is impossible to install the usbdriver from the cd.. Im SO frustrated right now!!!

has Somebody else experienced this?

Perhaps any solutions?

I would love to get on with the next thing: how well is it going to be drawing a line, when the X axis bar is almost 5 degrees of :-)



![images/44c6a38a392973e0f138ffa88ccc1276.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44c6a38a392973e0f138ffa88ccc1276.jpeg)
![images/19b5e6a37766fcf76296250e567fb26a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19b5e6a37766fcf76296250e567fb26a.jpeg)
![images/73c9297d9875d6b5d66915cab05ac868.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73c9297d9875d6b5d66915cab05ac868.jpeg)
![images/ea7446634bf998e9349b0bc6a9685380.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea7446634bf998e9349b0bc6a9685380.jpeg)

**"Martin Larsen"**

---
---
**3D Laser** *March 08, 2016 19:45*

What OS are you running and it looks like you need to take the rails apart and reassemble them to be square.  Contact your eBay seller mine gave me 75.00 dollars because I had a ding it it that knocked it out of square the way it sits I'm thinking you got some work ahead of you before you can get it cutting


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 20:18*

I'm not certain what RDWORKS is, however I am running the CorelDraw12 with the CorelLaser plugin that they gave on Windows 10 Pro, 64-bit, on Bootcamp partition on MacBook Pro 2014. If I recall correct there were minor issues when installing the USB driver as well. I think I had to install via the .inf file, not via any .exe file as it wouldn't work. I can't 100% remember though since it was a while ago.



I would agree with Corey also, your Y axis looks well & truly out of square. I also got a refund from my seller for issues (mine wasn't damaged, just alignment was impossible due to the way they mounted mirror #1, which was solved reasonably easily). I will check my install CD that they sent me later on (when I have an available USB port to plug my DVD drive in) to see if I am right regarding the .inf file. Or if you beat me to it, take a look by right clicking the .inf file for USB driver & try install it that way.



Edit: just thought of another option you could possibly try. If it is a .exe installer, then try run it in Administrator mode, and test different compatibility modes. Possibly one will work.


---
**3D Laser** *March 08, 2016 20:20*

Is this the 40w or 50w machine


---
**Ashley M. Kirchner [Norym]** *March 08, 2016 20:21*

Interesting. I have that same controller, but it came with LaserDRW and I was told RDWorks won't work with it. Huh. Anyway, as others have said, you have some manual work ahead of you to get the rails and gantry squared up. That was the first thing I did when I got mine. I lifted the whole thing out of the machine (4 bolts hold it down), then took it all apart and squared it up. Some of the metal bars were bent out of shape as well.


---
**Martin Larsen** *March 08, 2016 21:17*

**+Corey Budwine** Hi Corey. its the 40W


---
**3D Laser** *March 08, 2016 21:18*

I'm thinking they sent you the wrong software it should be laserdrw


---
**Martin Larsen** *March 08, 2016 21:19*

**+Corey Budwine** Im running windows 7 64 bit.

I contacted the seller this morning. I guess i will have to wait until tomorrow to hear from them with the time difference and all.


---
**Martin Larsen** *March 08, 2016 21:21*

**+Ashley M. Kirchner** Hmm.. I actually wondered if i could have got the wrong cd, since all the manuals described the bigger 50w model???


---
**Martin Larsen** *March 08, 2016 21:30*

**+Yuusuf Sallahuddin** if only i had MINOR issues installing my usb driver :)

I have tried on 3 different computers, even on a virtual xp 32 bit installation just to be sure. i tried to manually install fdtibus.inf file from device manager but it just tells me that this driverfile doesnt work, and that i should check if it is 64 bit compatible... hmm..


---
**Martin Larsen** *March 08, 2016 21:43*

**+Corey Budwine** i will write another mail to seller right now and tell them that i might have gotten the wrong software.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 21:59*

**+Martin Larsen** I did have a copy of all the software from the K40 cd in my google drive, but for some reason it's not there anymore. I can upload it again later & share the folder with you if you would like it temporarily at least until the supplier can provide you with a new copy or whatever they do to resolve your issue.


---
**Martin Larsen** *March 08, 2016 22:12*

**+Yuusuf Sallahuddin** I would absolutely love that! at least i could test it out to see if thats the problem..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 22:22*

**+Martin Larsen** In the process of uploading now. Will post link to the folder here once it's completed.



Here's the folder, still a bit uploading to it at the moment, but you can maybe start grabbing some of the files that are already synced across.

[https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)



I'd suggest you grab all the files in the folder & sub-folders. It's a total of 718mb for all of it.


---
**Martin Larsen** *March 08, 2016 22:24*

Thanks a lot mate :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 22:43*

Here is a printscreen of all the files that are on the CD. Areas that I have drawn around in coloured rectangles designate the contents of that Sub-Folder.



[https://drive.google.com/file/d/0Bzi2h1k_udXwWnYtdEF2SkxHVE0/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWnYtdEF2SkxHVE0/view?usp=sharing)


---
**Martin Larsen** *March 08, 2016 22:56*

Yearh thats nothing like mine. I cant wait to download yours and see if it sorts my problems.

I will check up on this post tomorrow when i get home from work. its late here in Denmark now and i was up till 2 am yesterday trying to get this thing to work. Thanks so much for helping out so far. What a wonderful forum.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2016 23:01*

**+Martin Larsen** You're welcome. By the time you come back tomorrow, then you should find all the files have been uploaded from my end. I hope it sorts your problems out.


---
**HP Persson** *March 09, 2016 00:06*

**+Yuusuf Sallahuddin** I tried downloading it too, it said the files had a virus and could only be handled by the owner.



Wanted to see what differed from my versions, but couldnt download ;)


---
**Scott Thorne** *March 09, 2016 00:09*

The software you got...rdworks...won't work with that board. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 07:47*

**+HP Persson** Maybe I set up the share on the folder incorrectly. I will take a look at it & see what I did wrong. The virus, a lot of people said that the CorelDraw/CorelLaser or whatever on the cd has a virus according to Norton or whatever like that. Personally, virus doesn't bother me as I only use my windows install for the laser cutter (no other files or anything I am concerned about on that install).


---
**Martin Larsen** *March 09, 2016 09:29*

Perhaps HP Persson accidently dragged the files out of the shared folder?

Only 17 files are showing now.


---
**Martin Larsen** *March 09, 2016 09:30*

and i didnt get the rest of it, so still no lasercutting for me :(

And the seller has of course not replyed to any of my mails yet.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 09:33*

**+Martin Larsen** I set it up that people can only View the files (which allows download), so HP Persson shouldn't have been able to drag the files out of the folder. Also, as it syncs to my laptop, it would delete them from there also if that was the case (which it didn't).



I've just compressed all the files into 1 .rar archive (which should make it much easier to download anyway). Should have done that from the beginning. Anyway, it is uploading again now, so hopefully once that completes you are able to access it.


---
**Martin Larsen** *March 09, 2016 09:52*

Thanks a lot Yuusuf.  I really owe you a favor now. 


---
**Martin Larsen** *March 09, 2016 15:40*

ITS ALIIIVE!... Thank you so much Yuusuf for your help.

Now i will go on to aligning the X-axis and align the mirrors.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2016 16:10*

**+Martin Larsen** Awesome, that's great to hear that it is working. Hope all goes well with the alignment/squaring up the rails.


---
**Martin Larsen** *March 09, 2016 20:02*

I spend about 2 hours aligning the whole thing as the laser nearly hit the edge of the first mirror.  But now its up and running,  and it cuts 3mm mdf plate at 10 mm/s and about 80% power in 2 runs.  It generates a lot of smoke,  so i think that i could do it in 1 run if i mount air assist to keep the smoke away from the beam.  


---
**I Laser** *March 12, 2016 09:33*

80% is a worry... Looking at the chassis your k40 looks analog, if it's not and you're setting it at 80% that's pushing about 18mA which is over the tubes spec...


---
**Martin McCarthy (NGB Discos)** *September 15, 2016 21:12*

**+Martin Larsen** hi what did you have to do exactly to get your machine working as i am having the same issue as you, it wont detect on my laptop


---
*Imported from [Google+](https://plus.google.com/105195340721594982075/posts/ZLSf81vYMcy) &mdash; content and formatting may not be reliable*
