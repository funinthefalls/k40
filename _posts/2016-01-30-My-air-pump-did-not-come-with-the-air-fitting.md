---
layout: post
title: "My air pump did not come with the air fitting"
date: January 30, 2016 20:58
category: "Modification"
author: "David Cook"
---
My air pump did not come with the air fitting. So I modeled one up today and 3d printed it lol. A little Teflon thread tape and no leaks. Great flow at the nozzle / laser head. I just cut a wood paint stir stick in half lengthwise. No flames ! Yay. And no soot on the lens. 



![images/95418ef202b0593c5588250dcd970214.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/95418ef202b0593c5588250dcd970214.jpeg)
![images/d72010f1207afe5123a4f5f0ee2b3d5e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d72010f1207afe5123a4f5f0ee2b3d5e.jpeg)
![images/070c5e7ccf54653b2d5068407f3c3e90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/070c5e7ccf54653b2d5068407f3c3e90.jpeg)
![images/d93d53aecf6adb8d2313f822ff8fe123.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d93d53aecf6adb8d2313f822ff8fe123.jpeg)
![images/de21ae34b97668e04c31fbdba9a3ae83.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de21ae34b97668e04c31fbdba9a3ae83.jpeg)
![images/3d10cc34bfc307816eb6fbcadf2770b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d10cc34bfc307816eb6fbcadf2770b2.jpeg)

**"David Cook"**

---
---
**Josh Rhodes** *January 30, 2016 21:01*

I wouldn't trust that to much pressure at all.. the model is awesome though.. are you gonna put it on thingiverse?


---
**David Cook** *January 31, 2016 16:49*

**+Josh Rhodes** I need to add some chamfer under the barbs so it can print without support. Then I can put it on thingiverse 


---
**Joseph Midjette Sr** *January 31, 2016 23:31*

Do you plan on using ABS or PLA?


---
**David Cook** *January 31, 2016 23:35*

I used PLA since that's all I had.   Worked good


---
**Joseph Midjette Sr** *January 31, 2016 23:43*

Cool. Can't wait for the upload to thingiverse so I can print one! 


---
**Joseph Midjette Sr** *January 31, 2016 23:47*

I have been thinking about designing an air assist nozzle with a single adjustable cross-hair laser pointer holder on TinkerCad and 3D printing it.



And also a stepper motor driven adjustable bed.....


---
**David Cook** *February 02, 2016 06:04*

**+Joseph Midjette Sr** here is the file I just uploaded  [http://www.thingiverse.com/thing:1310999](http://www.thingiverse.com/thing:1310999)


---
**Joseph Midjette Sr** *February 02, 2016 13:56*

Cool! Thank you David!


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/LT3CDK6sWgR) &mdash; content and formatting may not be reliable*
