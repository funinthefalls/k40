---
layout: post
title: "Does anyone know what these are made out of and if you need a different focus lens to use them on a K40 engraver cutter?"
date: August 13, 2015 03:20
category: "Discussion"
author: "James McMurray"
---
[http://www.ebay.com/itm/331209909829?_trksid=p2055119.m1438.l2649&var=540400118733&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/331209909829?_trksid=p2055119.m1438.l2649&var=540400118733&ssPageName=STRK%3AMEBIDX%3AIT)



Does anyone know what these are made out of and if you need a different focus lens to use them on a K40 engraver cutter?





**"James McMurray"**

---
---
**David Wakely** *August 13, 2015 06:50*

I would guess they are made from machined aluminium. And they I don't think they would fit onto the stock cutting head for the K40


---
**ThantiK** *August 13, 2015 17:38*

Likely aluminum, and yes you'd need a different focal point.


---
*Imported from [Google+](https://plus.google.com/115183135050042620815/posts/K1TLmLERCmW) &mdash; content and formatting may not be reliable*
