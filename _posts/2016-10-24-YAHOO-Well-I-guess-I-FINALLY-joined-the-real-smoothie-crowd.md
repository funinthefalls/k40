---
layout: post
title: "YAHOO! Well I guess I FINALLY joined the real smoothie crowd"
date: October 24, 2016 02:07
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
YAHOO!

Well I guess I FINALLY joined the real smoothie crowd.



It and LaserWeb are up and running. 



Now I have to start testing end-stops and steppers.



Then the dreaded PWM control.



Does anyone have a list of  LaserWeb configuration values used for their K40? I can figure them out but wanted the "cheat sheet" :)



BTW: do you really have to unplug the USB cable first to keep it from powering the smoothie when you turn off main power? I guess this is what **+Scott Marshall** was talking about needing a switch for? Is there a jumper or something on the board that allows you to disconnect USB power?



This has been a long time coming for me so I am going to find a nice "glass" to celebrate.






**Video content missing for image https://lh3.googleusercontent.com/-EXGB6K3CYBE/WA1s33tV25I/AAAAAAAAfpI/bGFexrnRIbo9V9fItezaKNdAWEAemRh7ACJoC/s0/20161023_200020.mp4.gif**
![images/7ca42b1627562187ff5b9bb9c3662ae5.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/7ca42b1627562187ff5b9bb9c3662ae5.gif)
![images/fe80ee3f0373197a8ad7fa80b889f341.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe80ee3f0373197a8ad7fa80b889f341.jpeg)
![images/f4fc4c49c17ea562b9775d04ac38eaff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4fc4c49c17ea562b9775d04ac38eaff.jpeg)
![images/0296134036a70395762e506ea1f8bfa4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0296134036a70395762e506ea1f8bfa4.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**K** *October 24, 2016 02:24*

Are you using the open drain single pin way of connecting your smoothie to your PSU?


---
**Don Kleinschnitz Jr.** *October 24, 2016 03:05*

Yes but testing starts tomorrow.


---
**Anthony Bolgar** *October 24, 2016 05:08*

I have an issue with mine and the GLCD...If I plug in the USB cable and then turn on the  main powe the LCD will not boot up, just stays bright but no test. If I turn on the main power first the LCD boots up, and then I can plug in the USB cable. It is a major pain to have to keep playing withe the USB cable.


---
**Don Kleinschnitz Jr.** *October 24, 2016 06:09*

I use both 5V regulators


---
**Bill Keeter** *October 24, 2016 12:21*

Where did you get the GLCD shield? I keep checking Uberclock but they're sold out.


---
**Don Kleinschnitz Jr.** *October 24, 2016 13:06*

**+Bill Keeter** It took some time to get it ... but from Uberclock.


---
**Anthony Bolgar** *October 26, 2016 16:02*

**+Peter van der Walt** I have both the smnoothieboard and glcd power regulators installed, both are the 1A version as well.


---
**Bill Keeter** *October 26, 2016 16:12*

**+Anthony Bolgar** **+Peter van der Walt** do you need both regulators? I thought only one of the 5v 1A was needed. 


---
**Anthony Bolgar** *October 26, 2016 16:19*

I don't think both are needed, I just happened to have two so I installed them both.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/TJCibh4oJxC) &mdash; content and formatting may not be reliable*
