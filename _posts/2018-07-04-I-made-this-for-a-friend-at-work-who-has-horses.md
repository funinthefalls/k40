---
layout: post
title: "I made this for a friend at work who has horses"
date: July 04, 2018 22:58
category: "Object produced with laser"
author: "HalfNormal"
---
I made this for a friend at work who has horses.

The tail is real horse tail from her horses.

The horse is made from leather cut on the laser.



![images/40800fbde15533c07848881ebd5f9ae0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40800fbde15533c07848881ebd5f9ae0.jpeg)



**"HalfNormal"**

---
---
**Anthony Bolgar** *July 04, 2018 23:55*

I don't know about people who want to have a horses butt that says "Open Here",......lol




---
**HalfNormal** *July 05, 2018 00:10*

The best part it farts when you open the bottle!


---
**Buhda “Pete” Punk** *July 05, 2018 15:21*

My grandparents had a donkey that sat on the coffee table. Every time you pressed its head down,  the tail lifted and pushed a cigarette from its butt. If you slapped his head hard enough you got a butt missile cigarette . Well at least until I broke his head off. Would love to find one of those so I could 3d print it. 


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/5Y3PDvefcZJ) &mdash; content and formatting may not be reliable*
