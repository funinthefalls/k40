---
layout: post
title: "The Great Air Assist Indecision! Over the last couple of days I have been reading a whole bunch of contradicting information online regards what is more important regards high L/min or high PSI (being units of volume and"
date: May 03, 2016 20:42
category: "Discussion"
author: "Pigeon FX"
---
The Great Air Assist Indecision! 



Over the last couple of days I have been reading a whole bunch of contradicting information online regards what is more important regards high L/min or high PSI (being units of volume and pressure some what muddies the waters when it come to making comparisons).



Some people have said you need High PSI to clear debris out of the cut, while others have said you need just height L/min to stop the beam being obscured by the smoke.......and I can unfortunately see the merit in both arguments!



I have been looking at two options, a Airbrush compressor (higher PSI) and a Aquarium Piston Air Compressor Pump (higher L/min).



So, for cutting something like 3mm Ply, would I be better of using:



Airbrush compressor @ 23L/min (start at 3bar/43 psi, stop at 4bar/57 psi)

or

Piston Air Compressor Pump @ 45L/min (Max 0.18bar/2.6 psi)



My concern with the airbrush compressor is how on earth they put out a constant flow of air without having a reasonable size tank, are they not a little spluttery when run open for a while, and prone to overheat? (thou I keep reading they seem to be recommended a lot on the K40 Facebook page, but I don't have facebook!)



Has anyone done any comparisons, is there any data out there, as right now there just seems to be two camps with different approaches, making it somewhat tricky for a newcomer to take a leap of faith regards ether approach.  (apologies for repeated posts here about air assist, but I see it as a safty issue as much as cutting performance so would like to get my head around it!) 





**"Pigeon FX"**

---
---
**Anthony Bolgar** *May 03, 2016 20:44*

I started with an aquarium pump, but changed it to an airbrush compressor an received better results when cutting. Engraving I did not notice any measurable difference. Hope this helps.


---
**Pigeon FX** *May 03, 2016 20:52*

**+Anthony Bolgar** Thanks Anthony, its good to hear from someone who has tried both. how dose the airbrush compressor keep up regards a constant airflow? in my limited experience with them I have seen quite a flux in output when used for more then a few seconds (or is yours one with a air tank attached as I assume that would help using it in this application?).


---
**Anthony Bolgar** *May 03, 2016 20:54*

Mine has no tank on it, but it is a higher end air brush compressor. So far no issues with keeping up, however I had some heat issues, so I have 2 4" PC fans blowing on the compressor to keep it cool.


---
**Jim Hatch** *May 03, 2016 21:16*

To some extent this is a bit academic (except for low end, small aquarium pumps which don't put out enough pressure or volume). If you can get 15-25PSI out of a pump the volume is going to be sufficient for the laser cutting/engraving purposes. You're constrained volumewise to the capacity of a 1/4" (or so) hose and fitting.


---
**Mishko Mishko** *May 03, 2016 22:06*

I'm thinking of adding a tank and pressure switch to my airbrush compressor (probably an old propane tank and a homemade electronic pressure switch), and some kind of flow regulation at the tank output, but I'm not sure how much that would help, probably some for shorter jobs...


---
**Pigeon FX** *May 03, 2016 22:36*

**+Mishko Mishko** I have come across the AS186 airbrush compressor that has a tank strapped to it and 57 psi/23 litres/min output.......think a tank is a really good move for a small compressor, as it will reduce fluctuating flow and run cooler.......unlike aquarium pumps small compressors just aren't supposed to be run constantly......hence my confusion over people using them for Air Assistance. 


---
**Anthony Bolgar** *May 03, 2016 22:56*

Air brush compressors run continuously while the user is painting. There is no control, other than an on/off switch. Some artists paint for hours at a time without shutting theirs down.


---
**Mishko Mishko** *May 03, 2016 23:06*

**+Pigeon FX** I'm only thinking about using airbrush compressor because I already have one, but I'm also sure it will overheat, they all do, thus the idea about the setup above, and I even have the "tank" and the pressure valve, otherwise I'm not sure I'd opt for that. A friend asked me to start the 90W laser for him, and it came with the pond air pump, so did some Trotec lasers I've seen  before. In my opinion these do a decent job, and don't overheat. I haven't checked the rated pressure, though, and did not do extensive tests with different materials, but it cut acrylic up to 25mm, cardboard, engraved on wood and glass and I did not notice any problems, no flames, no excessive fumes etc., and it doesn't overheat. Noise is also acceptable, lower than the piston airbrush. So I think if I had a choice, I'd choose something like this:

[http://www.amazon.com/dp/B002JPPFJ0/ref=cm_sw_su_dp](http://www.amazon.com/dp/B002JPPFJ0/ref=cm_sw_su_dp)


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/fNsTMzMYWAM) &mdash; content and formatting may not be reliable*
