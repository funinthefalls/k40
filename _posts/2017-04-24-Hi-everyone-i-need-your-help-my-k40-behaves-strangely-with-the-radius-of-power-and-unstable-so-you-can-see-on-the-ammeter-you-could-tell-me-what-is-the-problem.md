---
layout: post
title: "Hi everyone i need your help my k40 behaves strangely with the radius of power and unstable so you can see on the ammeter you could tell me what is the problem?"
date: April 24, 2017 08:25
category: "Original software and hardware issues"
author: "Camarda Neon Salerno"
---
Hi everyone i need your help my k40 behaves strangely with the radius of power and unstable so you can see on the ammeter you could tell me what is the problem? thank you 

![images/9b85c801ba6bfde8be5153b7c672eddf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b85c801ba6bfde8be5153b7c672eddf.jpeg)



**"Camarda Neon Salerno"**

---
---
**Phillip Conroy** *April 24, 2017 09:11*

Could be the power level pot breaking done,try swinging pot front high  to low a few times 


---
**Camarda Neon Salerno** *April 24, 2017 09:12*

This problem also occurs with the unplugged pwm what can these swings cause?


---
**1981therealfury** *April 24, 2017 09:54*

What liquid are you using for coolant on your laser?  I have seen similar issues to this caused by too higher conductivity in the cooling fluid which causes current to leak into the coolant and bypass the gas in the tube.


---
**Camarda Neon Salerno** *April 24, 2017 12:03*

I use 10lt of distilled water can this be the problem?


---
**Ned Hill** *April 24, 2017 12:13*

Distilled water is fine.


---
**Camarda Neon Salerno** *April 24, 2017 12:15*

So what's the problem I'm going through


---
**1981therealfury** *April 24, 2017 12:26*

Ok, well if the coolant isn't the issue then i would look at the current POT.  When my old one went on the K40 the current did fluctuate a few MA, nothing like yours... but it might be the cause of it. If you set the current pot so its just triggering the tube does it still fluctuate wildly or is it a lot more stable?


---
**Camarda Neon Salerno** *April 24, 2017 12:29*

My k40 does not have a potentiometer but a d keypad


---
**Camarda Neon Salerno** *April 24, 2017 12:30*

And even if I unplug the pipe and unstable


---
**1981therealfury** *April 24, 2017 12:41*

Do the numbers change for the % on your digital power controller?  A friend once had an issue with his K40 with the digital controller where one of the pressure sensors failed and was constantly switching power up from 0 to 100% in 10% increments and then once it hits 100% it drops back down to 0 and cycles again.



Other than that i can't think of anything else that would cause this issue except perhaps a failing HV power supply.


---
**Camarda Neon Salerno** *April 24, 2017 12:43*

How do you know HV flyback? I noticed that passing the screwdriver on the fliback emits a spark


---
**Claudio Prezzi** *April 24, 2017 13:33*

Never operate the HV PSU without tube connected! This could destroy the PSU.

To exclude PSU problems, you coud detach the frontpanel from the 3-pin connector on the psu and connect two resistors as voltage devider to the same connector. Something like 2x10kOhm resistors in series between GND (left) and 5V (right), and IN pin (center) connected between the resistors. This creates 2.5V on the  IN pin, which should be the same like pot on half position.



If power is stable with the resistors, then your PSU and tube are ok. If it's still unstable, then either the PSU or the tube have a problem.


---
**Camarda Neon Salerno** *April 24, 2017 14:07*

Sorry I did not understand where and how to link the residences you could send me a photo?




---
**Don Kleinschnitz Jr.** *April 24, 2017 14:30*

**+Camarda Neon Salerno**​ is the video above taken while pushing test from the panel?

Can you post picture of supply. 


---
**Camarda Neon Salerno** *April 24, 2017 15:16*

This is my panel

![images/4483703cdae8c23d44e933bfd4925de6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4483703cdae8c23d44e933bfd4925de6.jpeg)


---
**Camarda Neon Salerno** *April 24, 2017 15:17*

This is my psu

![images/ca732f87524ad98daee07f30f4b7b3dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca732f87524ad98daee07f30f4b7b3dc.jpeg)


---
**Don Kleinschnitz Jr.** *April 24, 2017 17:45*

+Camarda Neon Salerno are you holding the test button for the video above?


---
**Don Kleinschnitz Jr.** *April 24, 2017 17:47*

**+Camarda Neon Salerno** warning, that supply outputs LIFE THREATENING voltages. 


---
**Camarda Neon Salerno** *April 24, 2017 17:56*

I hold down the test button but this does not change even when I cut it I have the same problem


---
**Don Kleinschnitz Jr.** *April 25, 2017 15:03*

**+Camarda Neon Salerno**​ some things to try:

Disconnect to the IN pin on the LPS.

Connect the IN pin on the lps to 5v and push test again to see if you get full and steady power. Don't hold for a long time!

I also would remount the flyback. 

Not sure what the small red wire on right of flyback is? 


---
**Camarda Neon Salerno** *April 26, 2017 10:08*

For pleasure you would send me an illustrative image 


---
**Don Kleinschnitz Jr.** *April 26, 2017 10:54*

**+Camarda Neon Salerno** 



Verify the diagram below is your LPS.

For now ignore the test process on the left of the drawing and pay attention to the P#'s for this test.



Turn off power to the LPS

Remount and rewire the flyback

Disconnect the wires on P2-1(5V) and P2-2(IN)

Connect a wire between P2-1(5V) and P2-2(IN)

Turn on the machine 

Push the test button on the supply while taking a video of the meter as before.



Warning: be careful if the supply has the cover off like seen in the pictures above.

With this test the laser will fire will full power so do not hold the "Test" very long.



If this solves the current fluctuation you likely have a bad pot.

If it does not we will try the next test.



Post results here.....

![images/08a9cc63f4a8d9f26f2734ec7be28eed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/08a9cc63f4a8d9f26f2734ec7be28eed.jpeg)


---
**Camarda Neon Salerno** *April 26, 2017 11:39*

Doing the bridge and the power is stable at 21 mA what and then the problem?


---
**Camarda Neon Salerno** *April 26, 2017 18:57*

please help me


---
**Don Kleinschnitz Jr.** *April 27, 2017 10:40*

**+Camarda Neon Salerno** apparently there is something wrong with either the connection to pin P2-1,2,3 of the LPS or the other end of that cable assembly where it plugs into that control panel card (Right connector on the picture below).

I would look for bad connections or broken solder joints on that board and the corresponding cables that connect to the LPS P2.



If you do not find anything then you may have a board board (pict. below). 



**+HP Persson** do you see anything I have missed?

![images/77a736f1f8defee2b1a969f912f31cbc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/77a736f1f8defee2b1a969f912f31cbc.jpeg)


---
**HP Persson** *April 27, 2017 11:20*

Does the output vary also, like the meter shows? Or if you look at the tube, can you see the beam pulsing ?

To me, it looks like a mA-meter with too big resolution it cannot hold quick enough due to PWM. (one guess). My first meter did similar to this, it was a 100mA meter, it vibrated :)



But if it also pulses on the beam there is a board problem as **+Don Kleinschnitz** showed above. You may need to replace it, or convert to analog (actually pretty easy).


---
**Don Kleinschnitz Jr.** *April 27, 2017 13:29*

**+HP Persson** when he bypasses the control board [IN held at 5VDC] the current is stable. He is also pushing test so the PWM is out of the circuit?


---
**Camarda Neon Salerno** *April 27, 2017 16:37*

I also connected an analog potentiometer to see if it was the fault of the pwm but the same creates oscillations


---
**HP Persson** *April 27, 2017 21:49*

**+Don Kleinschnitz** but if you bridge it, there is no modulation going on? or is it?

Don´t have a oscilloscope so cannot check it myself.



Same pulsing with pot, i still believe the mA-meter is the problem. (too slow meter)



**+Camarda Neon Salerno** if you test engrave or cut, does the beam inside the tube pulse, or can you see on the engraving that the beam is pulsing, with different strength in the engraving? Only look at the result and the tube now, not the mA-meter ;)


---
**Don Kleinschnitz Jr.** *April 28, 2017 01:00*

**+HP Persson** the test pulls IN up to 5v which is full on when the test switch turns the supply on. This overrides the signal on L. 



My understanding is that **+Camarda Neon Salerno**​ is running this test statically with test button. 

Also the meter works fine with the IN pulled high and test button pushed, so logically the meter seems fine?, no?


---
**HP Persson** *April 28, 2017 01:05*

**+Don Kleinschnitz** Yeah, sounds logic. But, if the meter pulses like that, no matter if the signal is modulated by the digital board or a pot, he should se variations on the material cut or engraved too.

That would answer the question if it´s a meter problem, or something else.



Isn´t it the other way around? 0v is full power, 5v is no power?

3 am right now, my brain is in slow mode :P


---
**Don Kleinschnitz Jr.** *April 28, 2017 09:50*

**+Camarda Neon Salerno** i must be interpreting something wrong so lets recap:

1. When you push and hold the "laser test" button on panel the meter fluctuates and there is no job running on the machine at that time. Is this correct?

2. When you disconnect the current wiring and then connect IN to 5VDC, then push and hold the laser test button on the panel the meter does NOT fluctuate. Is this correct?

3. When you install a potentiometer (value?) across the 5vdc-IN-gnd then push and hold the laser test button on the panel the meter DOES fluctuate. Is this correct?


---
**Camarda Neon Salerno** *April 28, 2017 11:05*

1. Si 

2. Si

3. No




---
**Camarda Neon Salerno** *April 28, 2017 11:47*

Both with the potentiometer and with the digital control connected the laser power is unstable while disconnecting power control and I make the bridge that you showed me in the scheme the power and stable at 21 mA




---
**Don Kleinschnitz Jr.** *April 28, 2017 20:59*

**+Camarda Neon Salerno** how did you connect the pot. At the end of the cable or directly to the power supply?


---
**Robi Akerley-McKee** *April 29, 2017 05:32*

check your ballast resistor for burn marks


---
**Camarda Neon Salerno** *April 29, 2017 08:08*

I connected the potentiometer to the pins: P2-3 P2-2 P2-1


---
**Don Kleinschnitz Jr.** *April 29, 2017 11:00*

**+Camarda Neon Salerno** scratching my head:



The pot connected directly to the LPS the current is unstable.

With everything disconnected from the supply and IN connected to 5VDC the the current is stable.





mmmmm.........



With the pot connected directly to the LPS is the current unstable no matter the pot position including all the way to either limit?


---
**Camarda Neon Salerno** *May 02, 2017 18:19*

+Don Kleinschnitz I did not understand your last message


---
**Don Kleinschnitz Jr.** *May 02, 2017 19:56*

**+Camarda Neon Salerno** Sorry I was trying to understand the symptoms better and insure that I was not confused as something does not make sense to me.



1. Conditions under which the current is UNSTABLE when you push the "Test " :



A. The machine wired normally

B. The pot connected directly across the LPS 5V-IN-Gnd with the control panel disconnected from those pins.



2. Conditions under which the current is STABLE when you push the "Test" button:



A. When the IN pin is shorted to the 5VDC.



Is the above correct?



<s>-------</s>



In 1.B is the current unstable in all positions of the pot?






---
**Camarda Neon Salerno** *May 03, 2017 11:35*

+Don Kleinschnitz

Exactly all the points are right.

I was thinking it could be a flyback problem?


---
**Don Kleinschnitz Jr.** *May 03, 2017 16:00*

**+Camarda Neon Salerno**  at this point I wouldn't doubt anything but it does not make sense. Meaning if it was the flyback it would have intermittent operation irrespective of how the IN gets its voltage, i.e. by pot or pulled to 5VDC. 

I think there is still something else we are not seeing?


---
**Camarda Neon Salerno** *May 03, 2017 17:23*

+Don Kleinschnitz

Do I have any idea of some component on the main plate?


---
**Don Kleinschnitz Jr.** *May 04, 2017 13:03*

**+Camarda Neon Salerno** Sorry I do not understand the question.


---
*Imported from [Google+](https://plus.google.com/114112879039371909930/posts/H7WLiXg5ND8) &mdash; content and formatting may not be reliable*
