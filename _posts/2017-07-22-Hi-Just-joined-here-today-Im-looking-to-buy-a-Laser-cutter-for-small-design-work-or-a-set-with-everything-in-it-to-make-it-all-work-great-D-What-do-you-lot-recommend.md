---
layout: post
title: "Hi Just joined here today :) Im looking to buy a Laser cutter for small design work or a set with everything in it to make it all work great :D What do you lot recommend"
date: July 22, 2017 17:48
category: "Discussion"
author: "Michael Lavery"
---
Hi Just joined here today :) Im looking to buy a Laser cutter for small design work or a set with everything in it to make it all work great :D What do you lot recommend. 





**"Michael Lavery"**

---
---
**Ashley M. Kirchner [Norym]** *July 22, 2017 20:38*

First, you'll have to clarify what you consider to be a "set with everything in it". Different strokes for different people.



As the title of the group implies, this is primarily for the Chinese K40 laser machines, there are a bunch of them on eBay and there are a lot of folks here who will gladly tell you which vendors to stay away from and which machines come with additional bits and pieces. So it really comes down to what exactly do you need or want.


---
**Michael Lavery** *July 23, 2017 10:34*

 "set with everything in it" Everything I need to start off :) I want to be able to laser cut on wood and engrave so would the k40 be able to do this for me?  


---
**Stephane Buisson** *July 23, 2017 10:54*

**+Michael Lavery**, wood is an answer.

You should be aware of the limits.

to cut the rule of thumb would be increase the laser tube power by 10w per mm thickness. to start with a k40 (which more a 30/35w than 40) will allow you 3 mm plywood.

size matter k40 is 20x30 cm.

but  k40 is the first price for a laser cutter, fantastic for home tools or initiation.




---
**E Caswell** *July 23, 2017 14:20*

**+Michael Lavery** it also depends on your budget as well. A stock K40 is a good tool but it's not as simple as get it out the box, turn it on and expect it to work like an off the shelf printer.

Laserdraw is an OK software but many upgrade that so they can use openware software to enable cut and engrave in the same job. 

I would recomend you read up this forum and research the Lasweweb CNC, smoothie and Cohesion3D G+ forums. That way you will gather loads of information on what can be done with further additional investment to the K40. 

But be aware the K40 is a manufactured in China with varying degrees of quality control from poor to very very poor. 

You will also need somewhere to operate it to allow the extraction system to take away the nasty gases and fumes to the outside of your room from the laser cutting and raster jobs. 

In all honesty you will read up well and realise what you are buying when you invest your hard earned wedge. 


---
**Don Kleinschnitz Jr.** *July 23, 2017 14:38*

*K40GettingStarted



Start by reading the "Getting.. started or Help" post at the top of this community. Reading all its links will be worth your time in the long run.


---
**Don Kleinschnitz Jr.** *July 24, 2017 12:25*

**+Michael Lavery** let me know if this post and the attached spreadsheet is helpful in your evaluation of the K40?



[plus.google.com - Search - Google+](https://plus.google.com/s/%23K40GettingStarted/top)




---
*Imported from [Google+](https://plus.google.com/107256567343223455588/posts/bg7GqijVGPr) &mdash; content and formatting may not be reliable*
