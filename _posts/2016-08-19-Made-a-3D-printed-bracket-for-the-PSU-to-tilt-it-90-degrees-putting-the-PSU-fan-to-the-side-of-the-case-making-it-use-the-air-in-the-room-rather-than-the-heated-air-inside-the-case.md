---
layout: post
title: "Made a 3D-printed bracket for the PSU to tilt it 90 degrees, putting the PSU fan to the side of the case making it use the air in the room, rather than the heated air inside the case"
date: August 19, 2016 10:57
category: "Modification"
author: "HP Persson"
---
Made a 3D-printed bracket for the PSU to tilt it 90 degrees, putting the PSU fan to the side of the case making it use the air in the room, rather than the heated air inside the case.

I measured a 28c decrease of the PSU temp with this method after 45min of cutting. Will probably extend the lifespan of the PSU too.



Feel free to download and customize as you want.



[http://www.thingiverse.com/thing:1727115](http://www.thingiverse.com/thing:1727115)





**"HP Persson"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 19, 2016 11:20*

Great idea. My k40 case has a 120mm fan at the back of the electronics bay, although I have never seen it working (even though it is wired to the PSU).


---
**HP Persson** *August 19, 2016 13:35*

My compartment only has a 50mm fan, so it got hot inside when i was cutting :)



I´m sure the PSU will live longer with lower temps, easy insurance :)




---
**HP Persson** *August 20, 2016 13:35*

Made a second version, wich is a bracket behind the PSU instead of hanging it.



[http://www.thingiverse.com/thing:1728665](http://www.thingiverse.com/thing:1728665)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/EWJUfaXqgwd) &mdash; content and formatting may not be reliable*
