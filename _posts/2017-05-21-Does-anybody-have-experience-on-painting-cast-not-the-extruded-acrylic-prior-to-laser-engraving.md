---
layout: post
title: "Does anybody have experience on painting cast (not the extruded) acrylic prior to laser engraving?"
date: May 21, 2017 12:24
category: "Discussion"
author: "Sebastian Szafran"
---
Does anybody have experience on painting cast (not the extruded) acrylic prior to laser engraving? What type of acrylic in terms of light transparency is best for back-lit? I still cannot get satisfactory results. 





**"Sebastian Szafran"**

---
---
**Alex Krause** *May 21, 2017 18:30*

What type of project are you working on?


---
**Sebastian Szafran** *May 21, 2017 18:35*

**+Alex Krause** Front panels for electronic, which I want to backlit.


---
**Alex Krause** *May 21, 2017 18:52*

Try to illuminate the edge of the acrylic if possible it will give a more even tone to the lighting... Also automotive acrylic spray paint will probibly be your best choice of paint to use


---
**Sebastian Szafran** *May 22, 2017 20:12*

I cannot get even layers of acrylic paint. I paint with 4-5 layers until paint is fully light 'resistant' and only engraved text is shown, but on the other hand total paint layer thickness makes it harder to engrave evenly. I must say so far that painting is not my favorite thing to do. 


---
**Terry Taylor** *May 22, 2017 21:02*

I used the Rust-Oleum Universal Advanced Formula paint on a project for a customer worked fine.  20 or 3 light coats worked.





![images/d69ac0da8ceed468ebfb6b211a432be5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d69ac0da8ceed468ebfb6b211a432be5.jpeg)


---
*Imported from [Google+](https://plus.google.com/+SebastianSzafran/posts/LYVSfhH7rJf) &mdash; content and formatting may not be reliable*
