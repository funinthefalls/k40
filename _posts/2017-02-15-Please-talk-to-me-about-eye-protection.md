---
layout: post
title: "Please, talk to me about eye protection"
date: February 15, 2017 12:23
category: "Discussion"
author: "oliver jackson"
---
Please, talk to me about eye protection.  My laser will eventually go into a place of work and my boss is concerned about health and safety rules.  I have read lots of EN207 guides.  Problem is I don't really know the spec of the laser.  



If with quality lens and tuning I get my beam diameter down to 0.1-0.2 (maybe optimistic but its possible) then the power density seems to be around 10^8/10^9  (density=power/area).  So we need goggles rated to 10600 (D LB6) if we want to conform to health and safety rules right?



Also, what about the hole in the bottom.  I have the laser on a metal frame, there are white tiles on the floor.  Are people near at risk from reflections?





**"oliver jackson"**

---
---
**Jim Hatch** *February 15, 2017 13:58*

It depends on your locale. Strictly speaking this is a Class 4 laser because there are no interlocks in the machine to prevent the laser from firing when the lid is raised. Even if you add an interlock it's still a Class 4 officially because that's how it's manufactured. In the States, that means OSHA would require glasses for anyone in the same room as the laser when it is operating. There are acceptable accommodations if you fully enclose the device (you'll need to effectively stick it into a CO2 laser proof 6 sided box). That's not the easiest thing to do.



If you are not worried about OSHA regulations, then adding an interlock so the laser will not fire with the lid open should make it safe for use around other people. The hole in the bottom is needed for air intake to replace the air that is being exhausted by the blower and is generally not an issue because the machine is typically placed on a solid surface. With your scenario, you'll need to do something to allow air in but keep the laser light from escaping (which is near impossible if you've got a steel bed, but it is technically possible to get laser reflections so it's something you'll need to deal with).



This is the practical view - it's not a legal one. I am not a lawyer. I don't play one on TV. You should seek legal advice that your company is confident in. A lot will depend on how you're going to use it, who will use it, the location of the machine, how big the company is, etc. as to how much you're going to need to deal with the potential OSHA and regulatory issues.


---
**oliver jackson** *February 15, 2017 23:49*

Thanks for the input.  I think I will persuade him to have it in its own room away from others and a warning light when its firing, meanwhile anyone inside has suitable goggles.


---
*Imported from [Google+](https://plus.google.com/117912755306057047954/posts/QrT3jLfPReq) &mdash; content and formatting may not be reliable*
