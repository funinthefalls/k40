---
layout: post
title: "Smoothie Laserweb Question Certain brands of lasers have software that have the ability to change the pulse frequency of the beam"
date: October 18, 2016 00:01
category: "Smoothieboard Modification"
author: "HalfNormal"
---
Smoothie Laserweb Question 



Certain brands of lasers have software that have the ability to change the pulse frequency of the beam. (How frequently the beam is turned on and off ) this allows for less charing especially on thin paper. Is this a feature that is possible or being considered? 





**"HalfNormal"**

---
---
**Alex Krause** *October 18, 2016 00:44*

You can change the pwm frequency in the smoothie config file


---
**HalfNormal** *October 18, 2016 00:46*

**+Alex Krause**​ I should have said laser enable/fire on off frequency 


---
**Scott Marshall** *October 18, 2016 01:24*

Don't know what you mean, please explain.

I think you are looking to 'give the cut a chance to cool' between pulses?



The PWM action IS produced by switching the laser on and off. (very quickly - it's not usually perceptable)

They're not really separate functions, but are combined



As Alex correctly pointed out PWM freq is fully adjustable by changing 1 parameter in the Config file. That will control the pulses per second - Higher freq = shorter bursts. Most configs have it running rather higher than I think is a good idea (or necessary), but it appears to do no harm. You can drop the freq until your raster engraving starts to show degradation. 



During cutting the beam runs full time, with power being set by the pot. I still have not got the cutting power output in Laserweb to work, but haven't tried to lately. (That drove me nuts for weeks) I will try it again next time I have a board in test (which is what I'm supposed to be doing)


---
**Alex Krause** *October 18, 2016 01:47*

I run mine at around 5khz... a brand new unaltered config file calls for 50khz I believe... as **+Scott Marshall**​ mentioned it's not usually visually noticeable but once you get into a range where humans can hear... there will be a change in pitch... the better your hearing/younger you are the more you will perceive the change 


---
**Don Kleinschnitz Jr.** *October 18, 2016 02:16*

Are you referring to Pulse Position Control? 

When gas lasers are fired a large stored capacitance is dumped to get the gas to ionize. This initial energy is substantially larger than the steady on-state and therefore creates more light and therefore higher cutting power. The theory behind PPC is to turn the laser on and off during vector cutting based on position to get a higher peak power output. So a 40w laser has almost 2x the power for cutting. Since duty cycles are lower the tube also operates cooler. Epilog apparently uses this technique.



This is the best article I have seen so far:

[http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/](http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/)

[buildlog.net - Getting More Power and Cutting Accuracy Out of Your Home Built Laser System at Buildlog.Net Blog](http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/)


---
**HalfNormal** *October 18, 2016 02:31*

**+Alex Krause** **+Scott Marshall**  The PWM that drives the power setting (instead of a pot) is different from a PWM signal to actually turn on the laser (enable). You have two different ways to control energy. There is a write up from a maker space on the subject that I am trying to find for a reference. 


---
**Ariel Yahni (UniKpty)** *October 18, 2016 02:32*

Default config is 20. I have mine at 200. **+Alex Krause**​ what's yours? 


---
**HalfNormal** *October 18, 2016 02:56*

**+Don Kleinschnitz** Yes that is it! It also enables finer control over power.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2016 05:33*

My PWM period is at 200, as per instructions somewhere (make LW wiki?).


---
**Scott Marshall** *October 18, 2016 09:45*

**+Yuusuf Sallahuddin** 

That's 200us (microseconds) and is the duration of a single pulse cycle.



The default number in most of the "off the shelf" Config files I've seen is 20us,  10X faster than this. 



200us is much more reasonable, in my opinion.



To get pulse frequency you invert the number (divide 1 by it) which is 1/.0002 seconds= 5000 or 5khz (as Alex suggested) Remember thjis is cycles per second. To better understand how fast this is, 5000 x 60 = 300,000 pulses per minute. (note: at 20us this is 50khz)



My contention is this is rougher on the power supply and tube than is necessary. I would suggest that you reduce it until you see image degradation and them go back up 20 to 50%. That would cut down significantly on the tube starts which contribute to life reduction at least as much as ultimate current flow. You get the same net power control as at a higher frequency. I believe this frequency was "inherited" from a PWM program intended for electric motor control, and they don't care how fast you switch them on and off (as much - they do see heating from the higher PWM freq due to magnetic inrush currents.



The lower limit on the PWM frequency is where 'stitching' starts to occur. At 200mm/sec and 5khz, you are seeing 25 operations per mm, or the head travels .04mm (40 microns) for each pulse. (at 20us this distance is .004mm or 4 microns)



I believe the best way to find out what frequency is best is to find it experimentally. Once determined for a specific it's a linear function to adjust it to head speed. If 2500hz works best @ 250mm/ sec, then you can say to use 5000hz @ 500mm/sec or 1000hz @ 100mm/sec.



This would be a great project for you guys that have the engraving figured out quite scientifically to a high level, like Alex and Ariel, who did the level/darkness testing. You just may find another level of detail while testing. It's doubtful 5khz is the best freq for all speeds and materials. I'm pretty sure there is room to improve things in these settings.



I wish I had time to do the testing, I really enjoy that sort of thing, but I have myself 'fully entertained' keeping the Upgrade kits and accessories moving thru.



As usual, I probably went deeper here than needed, but hope it helps you engravers. Someday, when things slow down a bit, I'm looking forward to doing some nice Merbau engravings. By then you guys can tell me exactly how to go about it.



Scott



PS when playing in the Config file don't confuse the PWM pulse width with the trigger pulse width.This is the pulse that initiates the start of the PWM cycle and is very brief as to not 'overlap' into the next cycle at high frequencies. This parameter is not present on all Config files, but is usually 1us in the ones I've seen (this is just from memory)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 18, 2016 10:14*

**+Scott Marshall** Next time I get a chance to sit & have a play around with the config file & laser I will have a play around with different PWM period settings & see what happens if I drop it to 20. I'll engrave the same image at 200, 155, 110, 65, 20 pwm_period. That'll give an idea of what happens at different periods & if there is any visible difference between the 5 values.



edit: by the way, your explanation was well worth reading.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/2qbCg4ZapV7) &mdash; content and formatting may not be reliable*
