---
layout: post
title: "Spent the holiday weekend converting to RAMPS with Marlin..."
date: July 06, 2017 01:14
category: "Modification"
author: "Trip Bauer"
---
Spent the holiday weekend converting to RAMPS with Marlin... I was starting work on the new control panel to house the LCD and other items and decided that an LED bar graph of sorts would be cool for the power level indicator.  So, I built one, thinking it would be 0-5v on the PWM pin on the RAMPS board (D6) and things went haywire when I connected it...



Should have tested first, so I hooked up a scope, looks like it's less than 1v total, but the PWM shape didn't seem to match the settings. So S50 in the gcode only looked like maybe 30% on.  



I'm worried about tapping into that pin, not sure if it will affect things, would it be possible to assign to D4 or D11 in pins.h and then a second line in the cpp file to output there as well? Or am I overthinking things?





**"Trip Bauer"**

---


---
*Imported from [Google+](https://plus.google.com/102328363590949771482/posts/RehdLimNWBa) &mdash; content and formatting may not be reliable*
