---
layout: post
title: "Hi Has anyone on here converted an old K40 laser to run on ramps 1.4"
date: May 05, 2016 07:15
category: "Hardware and Laser settings"
author: "Neil Robbins"
---
Hi

Has anyone on here converted an old K40 laser to run on ramps 1.4.

I have an old laser power supply and all the wiring diagrams on the net seem to be for the newer type supply

My connections are marked with J  and don't know which one I have to connect to the ramps 1.4 board. I have a picture of the power supply connections but cant seem to upload it

any help would be handy

Thank you for any help you can give





**"Neil Robbins"**

---
---
**Jean-Baptiste Passant** *May 05, 2016 07:33*

If you can upload the picture to imgur or else I'll do my best to help you.


---
**Neil Robbins** *May 05, 2016 07:36*

I have uploaded the pic but don't know where it is on here


---
**Jean-Baptiste Passant** *May 05, 2016 07:37*

Sadly without it I cannot really help...


---
**Neil Robbins** *May 05, 2016 07:44*

its on here as K40 old power surpply


---
**Neil Robbins** *May 05, 2016 13:52*

[https://plus.google.com/112727076545743143342/posts/Gyib7VkWJsm](https://plus.google.com/112727076545743143342/posts/Gyib7VkWJsm)

this is a link to my power supply


---
**Vince Lee** *May 05, 2016 16:11*

What does your controller board look like?  Even though the power supply is different from current systems, the controller board pinouts may be the same, and you can use the existing cabling to decipher the power supply connectors.  That's also why when I made an adapter board I did so by replicating the controller board connectors.


---
**Neil Robbins** *May 05, 2016 16:25*

I have connected the x and y axis to my Arduino mega 2560 with ramps 1.4 and that bit works fine

 I need to know how to connect to my laser power board to the ramps board. I have 2 white wires which are the test fire and 2 black wires which is the laser on and off switch.. there are 2 red wires which one is laser  and the other is 5v. all the diagrams on the web all show different power supplies to my one so don't know how to connect it to my ramps board.

if you have a diagram if would be really handy


---
**Neil Robbins** *May 05, 2016 16:28*

here is a link to a pic of the board 

 

[https://plus.google.com/112727076545743143342/posts/Gyib7VkWJsm](https://plus.google.com/112727076545743143342/posts/Gyib7VkWJsm)


---
*Imported from [Google+](https://plus.google.com/112727076545743143342/posts/GsojHVoHujw) &mdash; content and formatting may not be reliable*
