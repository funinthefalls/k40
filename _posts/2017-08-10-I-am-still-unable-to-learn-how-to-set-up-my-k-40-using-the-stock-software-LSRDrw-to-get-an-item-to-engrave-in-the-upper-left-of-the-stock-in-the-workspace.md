---
layout: post
title: "I am still unable to learn how to set-up my k-40 using the stock software (LSRDrw) to get an item to engrave in the upper left of the stock in the workspace"
date: August 10, 2017 18:09
category: "Software"
author: "Ron Ferrell"
---
I am still unable to learn how to set-up my k-40 using the stock software (LSRDrw) to get an item to engrave in the upper left of the stock in the workspace.  I have tried changing the parameters in layout a bunch of times and ways but yet the device still cuts/engraves about center left thereby using up way to much material and pissing me off.  How do I get it to start cutting where I want it to start cutting?





**"Ron Ferrell"**

---
---
**Jim Hatch** *August 10, 2017 18:19*

Are you sure you have the model & machine info correct in the settings window? If those aren't correct you won't get correct placement on the material. Also make sure there is no offset specified in the parameters on the engrave pop-up window (about a third of the way down).


---
**Ron Ferrell** *August 10, 2017 20:02*

I'll check both.  Tnx




---
*Imported from [Google+](https://plus.google.com/102342991770341133056/posts/AXZ1eX5rz7f) &mdash; content and formatting may not be reliable*
