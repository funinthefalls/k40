---
layout: post
title: "Hi guys , I am a newbie!!"
date: May 26, 2016 14:35
category: "Original software and hardware issues"
author: "Diva Patterson"
---
Hi guys , I am a newbie!! My K40 arrived today , has come with a standard UK 3 prong plug with a marked 220v kettle type socket on machine , do I need the ground wire? .... just dying to align my mirrors and give it a test ! 

Ta in advance :) Lin





**"Diva Patterson"**

---
---
**Stephane Buisson** *May 26, 2016 14:56*

Welcome here Diva,

double check the wiring from inside (PSU->main connectors), china is a long away, and some K40 got some connections loose on the way. check the potentiometer and vu meter too. If ground is connected via main lead it's should be enough.


---
**Diva Patterson** *May 26, 2016 15:02*

Ahh thank you for replying :) I will google everything you have said and check. :) In that vien it may be a while before I am testing it :)


---
**Diva Patterson** *May 26, 2016 16:14*

May be even longer than I thought the screws holding the power and board and laser area shut in transit just will not come out even with correct shape screw head.... I can tell this machine is going to be a test! Incidentally my fan and oump both have uk plug adapters on them also. Wasnt expecting that bonus!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 19:00*

**+Diva Patterson** I actually chopped off the chinese plugs on the fan/pump & replaced with Australian plugs (couple of $ at local hardware store). The adapters were driving me nuts cos the chinese plugs are loose in the adapters & the slightest bump would cause the fan/pump to not be running.


---
**Diva Patterson** *May 26, 2016 19:34*

Thanks Yuusuf :) my fan and water pump have these and fit well thankfully :) 20160526_202828.jpg

[https://drive.google.com/file/d/0B7bosEdbaEY2aEo1LVYxZm8xeXM/view?usp=drivesdk](https://drive.google.com/file/d/0B7bosEdbaEY2aEo1LVYxZm8xeXM/view?usp=drivesdk) 


---
**Diva Patterson** *May 26, 2016 19:36*

And my actual K40 a full uk power supply 20160526_202946.jpg

[https://drive.google.com/file/d/0B7bosEdbaEY2ZFAtaXhLUmVEb1U/view?usp=drivesdk](https://drive.google.com/file/d/0B7bosEdbaEY2ZFAtaXhLUmVEb1U/view?usp=drivesdk) with socket like this 20160526_203015.jpg

[https://drive.google.com/file/d/0B7bosEdbaEY2LVlod3JGOXZIODQ/view?usp=drivesdk](https://drive.google.com/file/d/0B7bosEdbaEY2LVlod3JGOXZIODQ/view?usp=drivesdk) 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 19:36*

**+Diva Patterson** That's a much better adapter than they gave me. Total junk what I received.


---
**Diva Patterson** *May 26, 2016 19:38*

Have just ordered small metal files to get the packing screws out! Yhen I can actually start getting it set up, alligned and tested. The suspense is killing me ;)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 19:40*

**+Diva Patterson** Do you have a drill? I would drill them out. You don't need them ever in future either. Except maybe as a safety precaution for any children around.


---
**Diva Patterson** *May 26, 2016 19:43*

I am not brave enough, I think because I am  not familiar enough with the machine yet. I have a very small drill which I use to make small holes in ply but its torque is too weak (like my courage)


---
**Hayden DoesGames** *May 30, 2016 08:45*

Did you manage to get her going?  I'm in the market for one of these is there any chance you could share where you purchased it?




---
**Diva Patterson** *May 30, 2016 11:18*

My metal files to open her up should arrive tomorrow so.... fingers crossed  :) Ebay for mine. Not imported but delivered from Heathrow so no duty charges. All uk plugs. As soon as its open and alligned will let you know. I paid £310 all in :)


---
**Diva Patterson** *May 30, 2016 11:19*

Off work tomorrow so if open I will play!


---
**Hayden DoesGames** *May 30, 2016 17:07*

Nice lol totally envying you now lol


---
*Imported from [Google+](https://plus.google.com/115797394159411787863/posts/2sKj7MQRrfU) &mdash; content and formatting may not be reliable*
