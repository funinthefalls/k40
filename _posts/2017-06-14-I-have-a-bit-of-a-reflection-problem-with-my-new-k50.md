---
layout: post
title: "I have a bit of a reflection problem with my new k50"
date: June 14, 2017 14:58
category: "Discussion"
author: "3D Laser"
---
I have a bit of a reflection problem with my new k50.  I use magnets to hold the material up off the bed as well as keeping it straight.  But when the laser cuts through the material it hits the highly reflective aluminum bed causing the back of the material to be scorched by the reflective laser.  I have tried combination of decreasing power and or increasing speed and playing with the focal length but I can't get a good solid cut without scorching the back.



Btw I cutting 3mm birch ply at 50% power at 15mms





**"3D Laser"**

---
---
**Nathan Mueller** *June 14, 2017 18:44*

This is why aluminum honeycomb is used at the bottom of many laser cutters: 



Not sure how much you'll be able to reduce this with a very reflective piece of aluminum down there. Maybe masking tape on the bottom?

[ebay.com - Aluminum Honeycomb: Business & Industrial &#x7c; eBay](http://www.ebay.com/bhp/aluminum-honeycomb)


---
**Ned Hill** *June 15, 2017 00:20*

Probably need some standoffs to raise the piece off the bed to allow the beam to diffuse some.  It would also probably help to rub the aluminum bed with coarse piece of sandpaper to scratch it up good.  This will cause part of the laser energy to be absorbed into the metal and also diffuse the reflected beam more. 


---
**Ned Hill** *June 15, 2017 13:40*

I also see people with bigger lasers sometime using a sacrificial piece of MDF in the bed.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/6o92V63Efcm) &mdash; content and formatting may not be reliable*
