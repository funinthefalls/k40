---
layout: post
title: "Finally got my rotatory working this weekend"
date: June 24, 2018 20:57
category: "Object produced with laser"
author: "HalfNormal"
---
Finally got my rotatory working this weekend.



The cup is an insulated cup from Starbucks. My daughter is into dragonflys and her nickname is JAX.



I was able to make a stabilizer end out of some firm foam I get from work. It is interesting that math trumps common sense! The first one has an outer diameter of the larger end. You would think that this would make the angle of the cup straight but no! You have to add enough extra so that the cup is actually straight. Geometry finally paying off!







![images/8da0d19faf42e3913ab6de9e556bb658.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8da0d19faf42e3913ab6de9e556bb658.jpeg)
![images/2535db7d392f9576e4600b539b91f08a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2535db7d392f9576e4600b539b91f08a.jpeg)

**"HalfNormal"**

---
---
**ED Carty** *June 25, 2018 01:37*

Nicely Done


---
**Kelly Burns** *June 26, 2018 20:11*

You are the only other person I have seen to use Adapter rings besides me.  Due to inconsistencies with some cup/bottle suppliers, I have around 20 Adapters.  


---
**Kelly Burns** *June 26, 2018 20:18*

btw...  The problem with making the ring larger is that is then has different rotation geometry/Math and it can cause slipping or the cup to get crooked.  I make adapter rings the same size as the largest diamager of the cup  and then elevate one end of the rotatry to try and make the printing area as level as possible.  


---
**HalfNormal** *June 26, 2018 22:16*

**+Kelly Burns**   Thanks for the info. I am thinking about making an adjustable stand for the ring side and make it an idler.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/W1Q1d3ZnjwR) &mdash; content and formatting may not be reliable*
