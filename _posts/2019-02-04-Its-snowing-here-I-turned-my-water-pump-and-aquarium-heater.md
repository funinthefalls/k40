---
layout: post
title: "Its snowing here! I turned my water pump and aquarium heater"
date: February 04, 2019 01:44
category: "Discussion"
author: "James Rivera"
---
It’s snowing here! I turned my water pump and aquarium heater. My K40 is in my garage.





**"James Rivera"**

---
---
**Anthony Bolgar** *February 04, 2019 02:29*

I just asked the question of what people do to keep their K40's from freezing over on the new forum. What are the odds of that?


---
**James Rivera** *February 05, 2019 00:22*

It is over 8 inches deep! My sensor says about 15C. I don’t have a big cutting job, but I wish I did! 😉


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/BiCVrKz8FQW) &mdash; content and formatting may not be reliable*
