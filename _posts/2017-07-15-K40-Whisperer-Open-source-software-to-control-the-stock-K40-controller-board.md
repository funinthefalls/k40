---
layout: post
title: "K40 Whisperer: Open source software to control the stock K40 controller board"
date: July 15, 2017 16:41
category: "Software"
author: "Scorch Works"
---
K40 Whisperer: Open source software to control the stock K40 controller board. (EDIT: Only stock boards that work with LaserDRW.)

[http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html)



K40 Whisperer does not required the use of the USB key (dongle) or any of the standard software that comes with the laser cutter (i.e. LaserDRW, Corel Laser).  It will work under Windows, Linux and  MAC (MAC is un-tested).  K40 Whisperer accepts SVG and DXF files as input.

![images/124676b65c6d9ebf7c92e7e79d003453.png](https://gitlab.com/funinthefalls/k40/raw/master/images/124676b65c6d9ebf7c92e7e79d003453.png)



**"Scorch Works"**

---
---
**Don Kleinschnitz Jr.** *July 15, 2017 17:05*

Interesting ..... so you have reverse engineered the communications protocol with the Moshi controller?

 Is that communications Gcode or proprietary?



#K40ControlSoftware 


---
**Scorch Works** *July 15, 2017 17:32*

Yes, I reverse engineered the communication between LaserDRW and the K40.



I didn't think the board they use with LaserDRW was the same as the board used with Moshidraw.  Can you run LaserDRW with a laser cutter that came with MoshiDraw?



The communication format is  proprietary.  It is called "LHYMICRO-GL" (no relation to HPGL) If you save a EGV file from LaserDRW you can see the format.  (at least some of it)


---
**Jorge Robles** *July 15, 2017 17:34*

Well done!


---
**Jim Hatch** *July 15, 2017 17:36*

**+Scorch Works**​ - mine came with a Nano board (M2) and LaserDRW. I think Moshi board machines came with MoshiDraw but I haven't kept up in the past year if they came out with something else for Moshi board based machines.


---
**Jorge Robles** *July 15, 2017 17:37*

**+Scorch Works**​ will it be open source?---- It's Python. YAY! **+Claudio Prezzi** another protocol to the lab!


---
**Stephane Buisson** *July 15, 2017 17:39*

to make it clear to all, your stock board is a Nano. and you don't know if it work with a Moshi.




{% include youtubePlayer.html id="PEUJQFAEDcE" %}
[youtube.com - K40 Whisperer - LaserDRW Replacement](https://www.youtube.com/watch?v=PEUJQFAEDcE)


---
**Nate Caine** *July 15, 2017 17:49*

What great news.  I've seen the **+Scorch Works**​​​ blog before and was much impressed, but <i>this</i> project is a <b>real treat!</b>  



I've already downloaded his source code, and have much to learn there.   On his blog and video he mentions needing to test on other versions of the controller board, so I hope others will step up for that.  (I can't wait to get my machine back online.)  



Even with a few limitations that he mentions, this is a very capable start.  It's such a delight to see work presented in the true spirit of community and open source.   



Congratulations!


---
**Jim Hatch** *July 15, 2017 17:56*

Nice job. Great alternative for those who don't want to make the leap into a whole different controller board and the electronics needed for that conversion.


---
**Ariel Yahni (UniKpty)** *July 15, 2017 18:30*

**+Jorge Robles**​ can this be added to LW?


---
**Jorge Robles** *July 15, 2017 18:30*

Will! I guess :)


---
**Don Kleinschnitz Jr.** *July 15, 2017 18:34*

**+Stephane Buisson** whoops I meant Nano


---
**Don Kleinschnitz Jr.** *July 15, 2017 18:38*

So a Gcode to LHYMICRO-GL module would be interesting then all CNC software would work with it???


---
**Jorge Robles** *July 15, 2017 18:39*

We will explore the idea


---
**HP Persson** *July 15, 2017 19:16*

Testing it with the newer HT Master 5 & 6 controllers that comes in the newer type of K40´s soon, to confirm if it works with the software or not.

Just have to convert from Cohesion3d back to original again :)



Really like this idea! Changes the need for Corel or LaserDrw completely, all thumbs up!




---
**HalfNormal** *July 15, 2017 19:38*

**+Scorch Works** I have a semi-functional nano board I can send you if you need one for offline testing. Everything works except power control to the laser. (not getting a good ground on L)


---
**Scorch Works** *July 15, 2017 21:25*

**+Jorge Robles** Yes, it is open source (GPL)


---
**Scorch Works** *July 15, 2017 21:29*

**+HalfNormal** No thanks,  A semi-functional board would be fun to play with but I don't think I would ever get around to it.


---
**Adrian Godwin** *July 16, 2017 10:50*

Thanks for doing this ! It makes no sense to me that these controller boards should be closed source and locked. 



I tried to run it under Debian and found it reporting 'no PIL loaded'. In one place you've got a specific workaround for Debian, but I found it also necessary to make a similar change in  svg_reader :



    try:

        from PIL import Image

        from PIL import ImageTk

        from PIL import ImageOps

        import _imaging

    except:

        try:

            from PIL.Image import core as _imaging # for debian jessie

        except:

            PIL = False

            print "PIL not loaded in svg_reader"





I'm not sure why this doesn't get loaded by the similar code in K40_whisperer.py - I'm no great expert at python, so there might be a better fix.








---
**Scorch Works** *July 16, 2017 15:51*

**+Adrian Godwin** 

Actually the better fix would be to remove / comment out the lines in svg_reader.py that load tk specific items.

        from PIL import Image

        <b># from PIL import ImageTk</b>

        from PIL import ImageOps

        <b># import _imaging</b>



There is some cleanup to be done in some of the code.  Be aware I am highly suspect of that fix for Debian.  I was trying something and I accidentally left in in the code.  If your system is using that "fix" Python PIL may need to be removed/reinstalled.   



Does the Raster image display correctly for you when you open an SVG file? 


---
**Adrian Godwin** *July 16, 2017 16:18*

No, though it does complain 'units not set in svg file'. I don't have many svg files lying around so it may not be suitable - I should probably make one according to your directions.



I did find some odd comments while googling for directions to install PIL related to _imaging, pillow, and other packages. It sounds as though the dependency should be on pillow instead.


---
**Scorch Works** *July 16, 2017 16:37*

You can use an existing SVG file that errors on the units, you just need to open it and set the units in Inkscape and reload it.



I tried switching it to pillow in the past but I could never get it to work right with Tkinter (could be a personal deficiency).  I did most of my testing in Debian so it does work in Linux if PIL is installed "correctly".  


---
**Claudio Prezzi** *July 17, 2017 08:40*

**+Don Kleinschnitz** You are right. If we could write a gcode to LHYMICRO-GL converter, we could integrate it in lw.comms-server and would not have to change the frontend.


---
**Anthony Bolgar** *July 17, 2017 12:37*

Does the raster engraving function allow for greyscale engraving, or is it dithering only?


---
**Scorch Works** *July 17, 2017 12:51*

K40 whisperer will do greyscale images by dithering the input greyscale image (if the halftone option in "Settings"-"Raster Settings" is set.). 



The stock board does not have the ability to control the laser power.


---
**Anthony Bolgar** *July 17, 2017 12:58*

Thanks for the clarification.


---
**Lars Andersson** *July 22, 2017 18:52*

This is a great improvement over CorelLaser! Many thanks.

Just wondering how I can do multipass cuts?


---
**Scorch Works** *July 22, 2017 19:01*

**+Lars Andersson** I am not sure what you are asking.  Do you want to run the same cut path more than once?


---
**Lars Andersson** *July 22, 2017 19:47*

Yes, I would like to repeat the same cut path say 4 times at a fairly high speed.  This can give cleaner results than a single slow pass.




---
**Scorch Works** *July 22, 2017 19:55*

For now you need to click the vector cut for each time you want it to repeat.  



I don't know if I will add a repeat setting or not.  Clicking the button seems reasonable. It also allows the operator to decide if another pass is needed or not.


---
**Lars Andersson** *August 04, 2017 20:48*

Have used K40Whisperer for a while now. I am impressed. Both cutting and engraving are really useful. 



**+Scorch Works**

One suggestion for improvement. The final "go home"  

after all cutting is finished is done at cut speed. Better if it is done at rapid speed. I tried to look into the src for version 0.03 but it is a little over my head, I could not find where it is done.


---
**Scorch Works** *August 04, 2017 21:57*

**+Lars Andersson**​ Yes. I know that is an issue.

It pains me to see it move back at the cut speed but that problem is related to another minor issue that I am not working on yet.  It will get fixed.  


---
**Adrian Godwin** *August 06, 2017 18:44*

I think I'm missing something here - I'm trying to use Inkscape and K40Whisperer but I can only engrave, not cut.



I have a test file with a  filled rectangle and a few letters of text. I've set the dimensions to mm and shrunk the page to fit the drawing. Both elements are red, confirmed by opening the svg file in firefox : it shows red and confirms the colour is ff0000.



When I open the file in K40Whisperer, the drawing is shown in black,   and only the 'raster engrave' works. Vector engrave or cut report that there are no 'vector objects to cut'.



I'm using K40Whisperer 1.03, Inkscape 0.92 and Windows Vista. I've tried saving the file as 'plain svg', inkscape svc and dxf.



Even with dxf, I get the 'no vector objects' message. It's as though the svg objects are rendered to black bitmaps before  K40Whisperer attempts to use them.



Any ideas what I'm doing wrong ?






---
**Adrian Godwin** *August 06, 2017 19:08*

Here's a link to one of the svg files



[drive.google.com - drawing-2.svg - Google Drive](https://drive.google.com/open?id=0B8q6sqvuGdFfdXpIRzRTUVNCQjA)




---
**Scorch Works** *August 06, 2017 19:13*

**+Adrian Godwin** Red and Blue have no effect for fill colors only the outline/path colors are converted to vector cut/engrave.  So your box should be no fill with red outline. if you just want to cut the outline.



In standard fonts the characters are defined by the outline of the character shapes and the shapes are filled.  So to vector engrave a letter you need to set the outline color to blue (similar to the box).  The one additional thing about text is that in order for the text to be recognized as vector shapes in K40 Whisperer you will need to convert the text to paths in Inkscape. (You do this by selecting the text then select "Path"-"Object to Path" in the Inkscape menu bar.)



Remember standard fonts are outlines so you will be engraving outlines of text unless you use a special font.  You can use "Extensions"-"Render"-"Hershey Text" in Inkscape to access special stick fonts that consist of lines rather than outlines.



(When you use the DXF export from Inkscape the same limitations apply)


---
**Adrian Godwin** *August 06, 2017 19:40*

Success ! Thank you !




---
**Scorch Works** *August 18, 2017 06:53*

**+Lars Andersson** The K40 Whisperer return speed is fixed in version 0.05 which is now available on the web page.


---
**Rod Williams** *November 07, 2017 09:31*

Hi. I just got the K40. I am using the Whisperer .00 program and all of a sudden it STOPPED sending packets to the machine. I have changed the USB cable... Any other suggestions??? Thanks!


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/cCGx9wFP9G7) &mdash; content and formatting may not be reliable*
