---
layout: post
title: "So I have had a rough start with my k40"
date: February 08, 2016 02:53
category: "Discussion"
author: "3D Laser"
---
So I have had a rough start with my k40.  First I had screws holding the panels down stripped so bad I had to drill them out. The I got to cut my first piece and noticed that the laser stopped cutting half way through after some tinkering I realized the mirrors where not aligned so as it was cutting the laser was missing the lens.  Then after all that I saw some damage to the casing that I am afraid will cause the whole machine to be out of Wack. Fun times getting started!



![images/68b0a0940d8045d8cba784db9773c4f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/68b0a0940d8045d8cba784db9773c4f9.jpeg)
![images/b13570917af6d1a6172028f2629b131c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b13570917af6d1a6172028f2629b131c.jpeg)
![images/0a9e18bfed1985064d300e1c83e56adf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a9e18bfed1985064d300e1c83e56adf.jpeg)

**"3D Laser"**

---
---
**Gee Willikers** *February 08, 2016 02:58*

I have my machine screwed down to a sheet of 3/4" MDF. It really helps keep things aligned, adds weight and gives you a stable, flat bottom to support items on.


---
**Ben Walker** *February 09, 2016 15:32*

I am so sorry to see this Corey.  I was rooting for ya the whole time (especially after I made you nervous about ordering it).  It took me a while to crack mine open too.  I didn't drill out the screws but I had to run a couple hardware store trips to get it open.  That said, once you get everything going you are going to be amazed


---
**3D Laser** *February 09, 2016 16:03*

**+Ben Walker** I have done a few practice cuts and worked ok my mirrors or off so as the laser gets about half way over the bed the the beam miss aligns and stops cutting



I got my light objects air assist in today and plan on cutting back the exhaust this weekend to fit a bigger piece of material on the bed﻿


---
**Diva Patterson** *May 26, 2016 16:59*

Mines is the same I cant get either of the packing screws out and dont want to break the laser tube if a bit falls back :( dont know what to do next :( feel deflated and I aint started!


---
**3D Laser** *May 26, 2016 17:07*

**+Diva Patterson**  I used a drill bit drilled it out 


---
**Diva Patterson** *May 26, 2016 17:10*

Its the same on laser tube area too,  i am a little bit anxious about drilling that area in particular!


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/iH9x8he7m1E) &mdash; content and formatting may not be reliable*
