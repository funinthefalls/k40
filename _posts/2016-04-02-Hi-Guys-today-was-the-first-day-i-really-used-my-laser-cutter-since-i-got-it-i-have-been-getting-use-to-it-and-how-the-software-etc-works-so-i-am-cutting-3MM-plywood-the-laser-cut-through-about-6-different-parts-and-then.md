---
layout: post
title: "Hi Guy's today was the first day i really used my laser cutter since i got it i have been getting use to it and how the software etc works so i am cutting 3MM plywood the laser cut through about 6 different parts and then"
date: April 02, 2016 23:02
category: "Discussion"
author: "Dennis Fuente"
---
Hi Guy's today was the first day i really used my laser cutter since i got it i have been getting use to it and how the software etc works so i am cutting 3MM plywood the laser cut through about 6 different parts and then started not cutting all the way through the wood, so on the 8 part i had it turned up to max and it still didn't cut all the way through i did keep cleaning the focal lens after each part and i made sure the cooling water was not hot oh and i did make sure the lens were aglined before i started any cutting as i read this was a biggy on these machine's so what happens dose the laser lesson it's power with use.



Dennis 





**"Dennis Fuente"**

---
---
**Tony Schelts** *April 02, 2016 23:33*

Hello Dennis, I have had my laser for about 7 months and have has similar problems. The answer has always been something obstructing the path of the laser. Your alignment needs to be spot on or very close.  Your lens need to be clean and your mirrors for that matter.  The other thing that caused me problems was my Air assist worked lose and was getting in the way.

You should not be setting your laser to the max its bad for the tube.  I find that I can cut 3mm ply or acrylic at 7mm a second and between 5-7 MA.  Check everything again. 


---
**Dennis Fuente** *April 02, 2016 23:38*

5-7 MA wow i don't have air assist yet i do have a compressor for it but i can't get the fitting's to seal it's a harbor freight air brush model i am running the machine at 10 MMs i did clen all the mirrors and still had the same issue do the mirrors go out of alignment that fast 



Dennis 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 02, 2016 23:51*

**+Dennis Fuente** Unless your machine is vibrating a lot, the mirrors don't lose alignment very quickly.



The lens itself is the one most likely to get dirty & drop the power of the beam, as the smoke rises to it.



The laser tube will die eventually, but it's something like months (I think I read somewhere 10000 hours life, but I could remember incorrectly).


---
**Tony Schelts** *April 03, 2016 07:46*

I guess you have something similar to this.[http://www.ebay.co.uk/itm/Mini-Airbrush-Compressor-Kit-AS18-2-Kit-1-/111186327623?hash=item19e338b847:g:NKEAAOSwgyxWUxLk](http://www.ebay.co.uk/itm/Mini-Airbrush-Compressor-Kit-AS18-2-Kit-1-/111186327623?hash=item19e338b847:g:NKEAAOSwgyxWUxLk). I'm in the uk but they seem to work fine.  I bought a light objects air assist head which is am excellent purchase 


---
**Tony Schelts** *April 03, 2016 07:48*

posted without finishing. These compressors do the trick. 


---
**Dennis Fuente** *April 03, 2016 16:53*

I am going to look at the laser mirrors and lens and again check the alignment today and see what happens thank for the response and help 

Dennis 


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/6cqudTjoVGb) &mdash; content and formatting may not be reliable*
