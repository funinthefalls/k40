---
layout: post
title: "Not sure why this did not show up in the main forum"
date: February 27, 2018 19:00
category: "Discussion"
author: "HalfNormal"
---
Not sure why this did not show up in the main forum. Great troubleshooting and lesson for future troubleshooting movement issues.



<b>Originally shared by Fabiano Ramos</b>



Hello Friends, thx for attention!

 I have this problem on my K40. She came with me from the US and since 2014 I've never put her to work since. Today I had to use it, but Axis X has this problem. The video details exactly the problem:


{% include youtubePlayer.html id="HCwRKhoVGmo" %}
[https://www.youtube.com/watch?v=HCwRKhoVGmo](https://www.youtube.com/watch?v=HCwRKhoVGmo)



I pulled out the Belt of X axis to check the engine status, it stands still, humming.



I bought a new motherboard, and put it in place of the old, same problem. What could it be? The Power Supply voltages are normal ( 5.1v e 24.5v). 



I'm totally in the dark. I have no idea what it can be. Has anyone experienced this and knows how to solve it?









**"HalfNormal"**

---
---
**Ned Hill** *March 01, 2018 04:38*

I'm seeing it now in the "Original (Stock) software and hardware issues" section.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/EDyASNtNBBG) &mdash; content and formatting may not be reliable*
