---
layout: post
title: "This was a White Elephant gift for my brothers Christmas party last night"
date: December 26, 2017 14:45
category: "Object produced with laser"
author: "David Allen Frantz"
---
This was a White Elephant gift for my brother’s Christmas party last night. My sister ended up some how convincing the rightful winner to let her have it. Not sure how that happened LOL. Good to know your work is appreciated I suppose. 😃👍🏻 finished with several coats of General Finishes, Salad Bowl Finish. 

![images/917b0790cebe3e33cad01dd704636336.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/917b0790cebe3e33cad01dd704636336.jpeg)



**"David Allen Frantz"**

---
---
**Ned Hill** *December 26, 2017 15:37*

Lol, not sure why it would be considered a white elephant gift because it looks pretty awesome to me. :)


---
**Don Kleinschnitz Jr.** *December 26, 2017 17:29*

Looks amazing..... btw if you don't want to pay the price of canned salad bowl finish you can use mineral oil. 


---
**David Allen Frantz** *December 26, 2017 21:52*

Yeah I know it was more of a serving platter so it wouldn’t see as much use as a cutting board. And I figured that the salad bowl finish would be lower maintenance. 


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/UCPueaJsELp) &mdash; content and formatting may not be reliable*
