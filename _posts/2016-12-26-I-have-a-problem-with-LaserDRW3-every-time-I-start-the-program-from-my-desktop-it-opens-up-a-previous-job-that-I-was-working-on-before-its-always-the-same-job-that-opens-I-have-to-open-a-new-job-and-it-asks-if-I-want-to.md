---
layout: post
title: "I have a problem with LaserDRW3, every time I start the program from my desktop it opens up a previous job that I was working on before, its always the same job that opens, I have to open a new job and it asks if I want to"
date: December 26, 2016 10:50
category: "Software"
author: "john dakin"
---
I have a problem with LaserDRW3, every time I start the program from my desktop it opens up a previous job that I was working on before, its always the same job that opens, I have to open a new job and it asks if I want to save the previous job, I always say no, the other problem I have is that when I run a job and it finishes it always asks me if I want to continue with the next task?, I don't have a next task, is there any way to restore the program to default settings?, its a bit weird this program, it seems to be a bit buggy





**"john dakin"**

---
---
**Mike Meyer** *December 27, 2016 13:29*

I'll throw my 2 cents in here...I'm using a stock K40 and the CorelDRAW 12 that was included in the software package is, in my opinion, infinitely more capable. It takes a little while to figure out the ying and yang of the program, but once understood, it offers a lot more interesting options than LaserDRAW and it's a lot less buggy.


---
**john dakin** *December 27, 2016 13:33*

Thanks for the info, I tried that but I cant find my way around it very well, how do you get laser draw to actually make the laser cut, I cant find any controls to make the laser work in corel laser draw


---
**Mike Meyer** *December 27, 2016 13:39*

The cutting/engraving controls for CorelLASER are either in the top right corner OR sometimes you have to open them through the CorelLaser icon in the lower right status bar. Right click the icon and the cutting/engraving options will be presented.


---
**john dakin** *December 27, 2016 13:46*

Great, thanks for that I will give it a try, as you say Laserdrw seems to have lots of bugs and problems


---
*Imported from [Google+](https://plus.google.com/110405157088691728751/posts/f9kYS15JWAL) &mdash; content and formatting may not be reliable*
