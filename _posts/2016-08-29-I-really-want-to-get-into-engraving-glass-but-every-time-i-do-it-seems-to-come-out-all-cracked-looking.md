---
layout: post
title: "I really want to get into engraving glass but every time i do it seems to come out all cracked looking"
date: August 29, 2016 14:01
category: "Hardware and Laser settings"
author: "colin grund"
---
I really want to get into engraving glass but every time i do it seems to come out all cracked looking. Is there anyway to prevent this.





**"colin grund"**

---
---
**Alex Krause** *August 29, 2016 14:06*

Engrave thru a wet paper towel


---
**Stephen Sedgwick** *August 29, 2016 15:11*

Wasn't it referred too as damp?  not wet?  -- there is a post in here somewhere about how to etch glass.. and from what I recall they just spritzed on water from a spray bottle to make the paper towel damp.  Might do some searching in this group a little.. or check out indestructible. 


---
**Alex Krause** *August 29, 2016 15:50*

[https://plus.google.com/109807573626040317972/posts/dM8wnbhtcAM](https://plus.google.com/109807573626040317972/posts/dM8wnbhtcAM) I did this with a wet paper towel... it was dry by the time it finished engraving


---
**Joe Spanier** *August 29, 2016 16:19*

Do you have air assist


---
**HalfNormal** *August 29, 2016 23:09*

**+colin grund** This was done on plain glass. Low power and fast engrave speed. It was done awhile ago so I do not remember the exact settings.

[http://i.imgur.com/nbKV97u.jpg](http://i.imgur.com/nbKV97u.jpg)


---
**Jim Hatch** *August 30, 2016 00:02*

You can also use dish soap as a lubricating coolant. 


---
*Imported from [Google+](https://plus.google.com/113779564269879474557/posts/Y667LjPaapS) &mdash; content and formatting may not be reliable*
