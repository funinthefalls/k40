---
layout: post
title: "That's what you call mass production! Using a home made honeycomb which is an MDF frame with a fine aluminium mesh taped on top"
date: September 01, 2015 13:44
category: "Modification"
author: "george ellison"
---
That's what you call mass production! Using a home made honeycomb which is an MDF frame with a fine aluminium mesh taped on top. Slots onto two of the steel rods used for the existing bed and the back bit sits on top of a 90mm cube, works great!

![images/e51154a431055280eea80788cf3cc54f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e51154a431055280eea80788cf3cc54f.jpeg)



**"george ellison"**

---
---
**Flash Laser** *September 02, 2015 00:52*

good job, if you like the flat worktable. you could ask the seller make the honeycomb for you. it is easy and not need extra charge.


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/ZeaCyrXabRT) &mdash; content and formatting may not be reliable*
