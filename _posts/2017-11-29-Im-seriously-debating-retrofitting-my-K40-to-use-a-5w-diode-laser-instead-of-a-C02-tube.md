---
layout: post
title: "I'm seriously debating retrofitting my K40 to use a 5w+ diode laser instead of a C02 tube"
date: November 29, 2017 21:00
category: "Modification"
author: "Bob Buechler"
---
I'm seriously debating retrofitting my K40 to use a 5w+ diode laser instead of a C02 tube. This is because the K40's chassis, shoddy optics hardware and gantry just make mirror alignment a total nightmare. That, and I just don't usually need the power that C02 provides. I'm cutting chipboard, paper and 3mm ply at a maximum. From what I've read so far, a sufficiently powerful diode can handle all of that fine with enough passes. 



Question: Has anyone done this before? If you have, would you be willing to share your experiences, which laser module you ended up going with and why, etc?



TIA!





**"Bob Buechler"**

---
---
**Ariel Yahni (UniKpty)** *November 29, 2017 21:38*

Make sure you account for a Z axis. Diodes being less powerfull need multiple passes at focus.


---
**Bob Buechler** *November 29, 2017 21:40*

**+Ariel Yahni** Ahh. So you mean, the need to adjust the focus after each pass, as the cut moves deeper?


---
**Ariel Yahni (UniKpty)** *November 29, 2017 21:45*

**+Bob Buechler** yes. I had a 3.8w diode attached to my CNC router. I was able to cut 3mm ply and acrylic with multiple passes with multiple Z increments. If you don't then most likely the heat can deform or burn what you are trying to cut.


---
**Bob Buechler** *November 29, 2017 21:47*

**+Ariel Yahni** Good to know! and that makes sense. I imagine a motorized Z table is really the only way to handle that sanely, otherwise you're adjusting Z by hand for almost every pass which would get prohibitive in a hurry. 


---
**LightBurn Software** *November 30, 2017 00:02*

You’ll be more limited in what you can cut as well - anything that diffuses or transmits visible light won’t be touched by a diode, so no cutting acrylic.  I know that’s not on your list, but something to consider.  You’ll find the power awfully underwhelming.  Using a diode for cutting is painful compared to a CO2 laser - personal suggestion is to spend a little effort tightening and aligning the K40 - it’s really not that bad, and will be far superior to a diode for everything but image engraving.


---
**LightBurn Software** *November 30, 2017 00:03*

If you’re still using the stock board you might consider a C3D Mini as well - much smoother, so easier on the alignment, and has software power control.


---
**Bob Buechler** *November 30, 2017 00:04*

**+LightBurn Software** I have spent hours trying to align the mirrors on my K40. Some people get lucky and theirs aren't bad... not in my case though. I've torn my optics down to try and rig something easier, but it's not going well. I've given up entirely on it. I'd pay someone to do it for me if I could find someone local to me. (Seattle area) 


---
**Bob Buechler** *November 30, 2017 00:05*

**+LightBurn Software** Yeah. I have a C3D Mini. It's amazing. Using the K40 isn't the problem... the mirrors/alignment is. 


---
**LightBurn Software** *November 30, 2017 00:08*

I think +Hakkan HP persson has a good guide on doing it.  It takes a bit of time and patience, but I found mine similar to aligning the mirrors on my big laser, and worth it. If I was in the area I’d offer to help. I just think you’ll find a diode really limiting - I started with a 6.5w and gave up quickly.  It burns a lot more than it cuts because it’s slow, and the heat builds up before material vaporizes. You get a lot of charred edges.


---
**Bob Buechler** *November 30, 2017 00:11*

**+LightBurn Software** Good to know. The fact that you started with a 6.5 and gave up on it gives me pause. I have Persson's guide, and it's great, but I don't visualize angle adjustments well, despite hours of banging my head against it. I learn better through hands-on instruction... sadly, that's not too available for in-home cutters yet, it seems. 


---
**LightBurn Software** *November 30, 2017 00:12*

You’re in the same time zone as me - could do it via GChat / video if you have a camera and some time this weekend.


---
**Bob Buechler** *November 30, 2017 00:12*

I have a mirror height issue too, which is making it worse. The second stage mirror is on the stock K40 sheet metal bracket, and that is too flexible, and not perfectly square. So the mirror is too low I think, and  I'm pretty sure it isn't standing up straight either. 


---
**LightBurn Software** *November 30, 2017 00:15*

That can be bent up a little if necessary.  Don’t be afraid to bang it up a little if needed.  They don’t have to be super strong, because they carry very little weight.


---
**Bob Buechler** *November 30, 2017 00:15*

**+LightBurn Software** That's very kind of you. :) I'll toss you a DM to schedule something. Thank you.




---
**LightBurn Software** *November 30, 2017 00:15*

Pretty sure mine is sagging a little too, but the mirror angle was enough to compensate for it.


---
**Bob Buechler** *November 30, 2017 00:17*

**+LightBurn Software** Yeah, it was just too far off in my case. I've tried adding stuff to elevate the mirror, etc... but it's pretty frustrating. 


---
**Don Kleinschnitz Jr.** *November 30, 2017 01:17*

I don't think you are going to be happy with the power on a laser diode and its material sensitivity.

Is your alignment problems stability? i.e. why do you need continual adjustment?

I have thought about new mirror mounts and alignment gauges perhaps now is the time?


---
**Bob Buechler** *November 30, 2017 05:56*

**+Don Kleinschnitz** I've never been able to get it aligned, if I'm honest. The K40 arrived with the second stage mirror just barely able to reflect to the laser head. They basically got just part of the beam to barely bounce off the very top corner edge of the mirror (most hitting the frame around the mirror) and that was enough to land a partial-strength beam on about 25% of the bed. When I added air assist, I needed perfect alignment to even make it through the laser head, and that's when it all stopped. 



What I need is a hands-on instructor to teach me how to align, because even with the great manuals I've found online, I could never solve the second stage problem. The hardware involved is just too cheap, fine tuned adjustments aren't possible, especially if you need to fix a height issue. 



I've raised and lowered the tube, raised and lowered the first mirror, raised and lowered the second mirror, and I could never get all three mirrors aligned (in all three dimensions). 



It's irritating because I know that if the chassis were square, and the hardware the mirrors are mounted to are level and at the same heights, then I wouldn't even need the thumbscrews. And in my case, the fact that even with the thumbscrews, I still can't manage a shot square enough to make it through my air assist nozzle is deeply frustrating.



After many hours of fighting with it, I eventually just gave up and let it sit in my garage.  


---
**LightBurn Software** *November 30, 2017 06:00*

I assume you mean this, when you say second stage? How badly off is yours? It’s not strong metal, so you could easily un-mount it, bend it as close to 90 as you can by hand / pliers, then re-mount and try aligning again.![images/aa739c09e1b9ddfc4f02d4383b5d649f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa739c09e1b9ddfc4f02d4383b5d649f.jpeg)


---
**Bob Buechler** *November 30, 2017 06:26*

Right. That's the one I'm referring to. First (fixed mirror by the tube), Second (on the gantry), Third (the head). It's not just that it might be bent though (I'm not sure it needs much of a tweak), it's that the whole thing needs to come up about half a centimeter. Though it might be off of 90 enough to make it feel like it needs to come up that much. 



At least the gantry itself is level and within 1mm of perfectly square (from what I can tell anyway). 



I pulled off the mount a while back and was working adding on a vertically-adjustable mount made from a piece of scrap linear rail I had lying around. So I'll have to remove that and reattach the mirror to resume my alignment fight. I'll try and get it back to 'stock' soon so I can try align it again. 



And if I haven't said it, if I come off grumpy, I'm just frustrated at the problem (and my inability to solve it), certainly not at you guys. I'm beyond grateful for the assist.


---
**Don Kleinschnitz Jr.** *November 30, 2017 06:33*

Its easy for me to say but I think something is fundamentally off. I don't remember clearly but I think I opened up the slots in my 2cnd mirror mount. 



I know you have probably heard this multiple times. My experience with others revealed that machines that are hard to align have a misalignment of the tube and beam coming from the 1st mirror. If it is not parallel with the gantry your 2cnd and 3rd mirrors are hard  or impossible to align.



I started to work on a system that would make the beam parallelism visible in 3D. 

The problem is that in most alignment schemes you really cannot tell if the beam is exiting the 1st mirror parallel to the carriage. As you say its hard to see it in 3 dimensions.



I had an idea to use multiple targets aligned to a bench on  the carriages plane. The initial experiments are here:



[donsthings.blogspot.com - K40 Optical Model & Alignment Tools](http://donsthings.blogspot.com/2016/06/k40-alignment-tools.html)



This experiment was done with a metal plate but I planned to make acrylic stanchions that sat on the Y frame holding targets. This would provide multiple measurement points along the Y which would unequivocally show if the beam was coming out of mirror 1 parallel to the Y all the way to mirror 2. The same technique would show you if the beam stayed parallel to the x all the way to mirror 3.



I never got the chance to make the acrylic stanchions but I thought the theory of this technique might be helpful to you or give you some new ideas.




---
**Bob Buechler** *November 30, 2017 07:22*

I took a look and something like what you're describing would be great. I think it would help a lot. 


---
**Claudio Prezzi** *November 30, 2017 12:27*

I double Don. The x and y axis bouild a plane. It could be impossible to correctly align the mirrors if the tube is not in the same plane.

I had to shim the tube at one end for about 1mm.


---
**Bob Buechler** *November 30, 2017 15:33*

I've already upgraded my tube mounts so leveling the tube is easy. 


---
**Bob Buechler** *November 30, 2017 15:42*

a while back I designed this as a replacement gantry and tube mount system for the K40. The idea is to cut out the metal compartment holding the tube now, but otherwise leave the chassis alone. that creates the space to drop in this assembly within the existing space. One of the goals of that design was to ensure the tube and all mirrors are on the same plane, by the very nature of the design. In theory it should really help solve that problem, but the build with the motors will cost several hundred dollars, so I've held off out of frustration, mainly.



[https://openbuilds.com/builds/k40-c02-laser-rail-system.5270/](https://openbuilds.com/builds/k40-c02-laser-rail-system.5270/)


---
**Bob Buechler** *November 30, 2017 15:42*

[openbuilds.com - K40 C02 Laser Rail System](https://openbuilds.com/builds/k40-c02-laser-rail-system.5270/)


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/BeHR384meoJ) &mdash; content and formatting may not be reliable*
