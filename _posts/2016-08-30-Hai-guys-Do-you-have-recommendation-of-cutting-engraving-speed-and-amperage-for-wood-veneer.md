---
layout: post
title: "Hai guys, Do you have recommendation of cutting & engraving speed and amperage for wood veneer?"
date: August 30, 2016 11:32
category: "Discussion"
author: "Daniel Susanto"
---
Hai guys, Do you have recommendation of cutting & engraving speed and amperage for wood veneer?

Thanks a bunch!





**"Daniel Susanto"**

---
---
**Bart Libert** *August 30, 2016 11:40*

That all depends a lot on the kind of veneer


---
**Chris Sader** *September 01, 2016 00:41*

**+Bart Libert** help the guy out with some examples? (I've never cut veneer, so I can't offer any suggestions)

**+Daniel Susanto** any additional detail about what type(s) you're trying to cut/engrave?




---
**Bart Libert** *September 01, 2016 05:09*

I also didn't cut veneer yet Chris. I'm just researching the kinds of veneer yet. Also i don't own a 40w but a 80w machine so my settings will not be good for 40w. I would start cutting at 32ipm 90% power on 800PPI so kinda useless for 40w without ppi


---
*Imported from [Google+](https://plus.google.com/100618572267681897497/posts/F7RyTGHRaNe) &mdash; content and formatting may not be reliable*
