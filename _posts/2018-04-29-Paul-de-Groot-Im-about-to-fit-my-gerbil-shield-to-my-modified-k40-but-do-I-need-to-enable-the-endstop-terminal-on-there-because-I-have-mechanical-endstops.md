---
layout: post
title: "Paul de Groot I'm about to fit my gerbil shield to my modified k40 but do I need to enable the endstop terminal on there because I have mechanical endstops?"
date: April 29, 2018 10:12
category: "Modification"
author: "Andy Shilling"
---
**+Paul de Groot** I'm about to fit my gerbil shield to my modified k40 but do I need to enable the endstop terminal on there because I have mechanical endstops? The X is still on the ribbon but Y is a seperate cable. 



Thanks for the lens by the way much appreciated 😉





**"Andy Shilling"**

---
---
**Paul de Groot** *April 29, 2018 10:19*

Hi Andy, the x ribbon cable has the x and y limit endstops. Alternatively you can wire them to the 5 breakout pins on the gerbil shield. Let me know how you go. You can use the ? Command to check/read out the endstops activation when moving the gantry towards the end.


---
**Andy Shilling** *April 29, 2018 10:26*

**+Paul de Groot** thank you. I'll be attacking it later today if all goes well, I have the standard reprap style endstops on the red pcb with three wires which of the 5 breakout pins correspond to the Y axis or is that printed on the board?


---
**Paul de Groot** *April 29, 2018 11:36*

**+Andy Shilling** hi Andy yes you can use that kind of reprap end stops. i have used them for my trotec conversion. You need gnd (ground), signal (middle)x/y, +5v. Those three goto the breakout pins which match ground, x or y and 5v. You might have to invert the limit pin config on gerbil (normally closed - signal is low/gnd. The signal goes to high 5v when the end stop engages or is being hit) i recall $5. You can set the $ settings via any gcode sender. $$ give you a list of all config settings.


---
**Andy Shilling** *April 30, 2018 18:08*

**+Paul De Groot** I'm all connected up now but I think I have no supported firmware at least thats what LW is telling me. Should this have a firmware on it or do I need to flash them myself?


---
**Paul de Groot** *April 30, 2018 22:42*

**+Andy Shilling** the firmware is Grbl with a higher definition of PWM (16 bits instead of 8) which does not impact the g-code or other software. The controller has been personally tested by me and there is no need to re-flash it. I suggest to try out the controller set up via a g-code sender and or Inkscape to confirm its proper functioning. After that is confirmed we can see what's not working in LW (laser web?). I have used laser web and the laser power range is 0 to 1 which needs to be reconfigured in LW into 0 to 1000 or more otherwise the laser is not working. Let me know what you have observed so far so I can help you a bit more.


---
**Andy Shilling** *May 01, 2018 05:51*

I think I found my answer late last night after I had messaged you, I don't have a +5 supply to the shield. I presumed the R4 would take power from the usb connection.



I will correct this tonight when I'm home from work and update you. Thanks for your patience.


---
**Paul de Groot** *May 01, 2018 06:10*

**+Andy Shilling** the laser power supply should have a 5V output that you can use. Using a separate 5V power supply has the risk of creating ground loops. Please let me know how you go with it!


---
**Andy Shilling** *May 05, 2018 08:52*

**+Paul de Groot** Sorry it's taken a while to get back to you, I now get connection to LW (yay) but it seems my motion is backwards I managed to figure this out when I had the cohesion running GRBL so I'm sure i can do that again. 

My biggest hurdle at the moment is if I have the ribbon plugged in I can not use the optional pins for the Y endstop as though they are disabled but when I unplug the ribbon the Y endstop works.



So my question is can I use both the ribbon and pins at the same time or do I need to rethink my wiring to get this all working in harmony?


---
**Paul de Groot** *May 06, 2018 08:11*

**+Andy Shilling** yes you should be able to use both in parallel. Worst case you need to disconnect the x end stop at the gantry x end stop. But I am pretty sure you can leave it in place.


---
**Andy Shilling** *May 06, 2018 14:39*

**+Paul de Groot** ok I have been playing today for a little bit, it seems I can not use the pins for the Y axis and the ribbon connection at the same time and to top it off it also seems my new laser psu can't give enough 5v power for the R4, Pot and digital amp display.



I might look into a 24-5v dropper to power the Gerbil sheild from my main board psu and see how that goes. I am wondering if the lack of 5v power is the reason I am having troublewith the Y axis mechanical end stop, any thoughts?




---
**Paul de Groot** *May 06, 2018 22:58*

**+Andy Shilling** The mainboard and stepper logic uses less than 50mA so it seems odds that the 5V cannot deliver that. The laser PSU generates 24VDC and uses a simple 7805 regulator to create 5V. You can use seperate power supplies as long they have a common ground and nothing else (makes sure when you do that, that you don't use the 5v from the laser power supply, it's logical but double check). Maybe your 24V DC is too weak. I replaced the scotty output diodes on mine to give it a bit more oompf.

The mechanical end switches are normally closed (NC) to ground. So you should be able to use the breakout pins unless you have two NC switches in parallel which would never work (you need to put then in series). Note the X and Y axis do have separate limit switches. You can extend the end switches in both ends of the axis as long they are in series. 


---
**Andy Shilling** *May 07, 2018 11:14*

**+Paul de Groot** Found the problem, my daughter board had a wiring problem that I didn't pick up on, for some reason it was killing the 5v power, I'm presuming sending it to ground.



I have now worked out the motion direction also. I will be giving this a good test later when I have time and will update you once I do.


---
**Andy Shilling** *May 07, 2018 11:57*

Ok not as far as I thought lol.

I have correct motion but my endstops are now not stopping the motion.

I have inverted the endstops and that doesn't seem to make a difference. what else can I do as I seem to be at my limit of knowledge with grbl.


---
**Paul de Groot** *May 07, 2018 22:05*

**+Andy Shilling** this wiki page gives you all the information about the settings 

[github.com - grbl](https://github.com/paulusjacobus/grbl/wiki/Configuring-Gerbil-Grbl-v1.1e)


---
**Paul de Groot** *May 07, 2018 22:10*

The best way to test is to set the laser head in its start position so the limit switches are engaged. Use the "?" command to read out the switches via a gcode sender (CNCjs seems very easy to use). You should see a message "pos xy; xyz" if the limit switches are working or "pos xy" if not.

$3 can be used to invert the motion direction (or swap the Y plug around) and $5 for the limit switches. Please let me know how you are going, hopefully we get it working soon. I am curious to see you cutting and engraving some art. :)


---
**Andy Shilling** *May 07, 2018 22:31*

**+Paul de Groot** thanks I didn't know about the status option I'll check that tomorrow. I now have everything else going the correct way so hopefully this will be the last hurdle before I really get to play.


---
**Andy Shilling** *May 08, 2018 15:36*

**+Paul de Groot** All I get from CNCj is the following when sending ?

Idle|MPos:0.000,0.000,0.000|Bf:15,127|FS:0,0> when $5=1



<Idle|MPos:0.000,0.000,0.000|Bf:15,127|FS:0,0|Pn:XYZ> when $5=0 does this mean anything to you?



This is my settings as they stand



$0=10 (Step pulse time, microseconds)

$1=255 (Step idle delay, milliseconds)

$2=0 (Step pulse invert, mask)

$3=3 (Step direction invert, mask)

$4=0 (Invert step enable pin, boolean)

$5=0 (Invert limit pins, boolean)

$6=0 (Invert probe pin, boolean)

$10=31 (Status report options, mask)

$11=0.010 (Junction deviation, millimeters)

$12=0.002 (Arc tolerance, millimeters)

$13=0 (Report in inches, boolean)

$20=0 (Soft limits enable, boolean)

$21=0 (Hard limits enable, boolean)

$22=1 (Homing cycle enable, boolean)

$23=3 (Homing direction invert, mask)

$24=600.000 (Homing locate feed rate, mm/min)

$25=1000.000 (Homing search seek rate, mm/min)

$26=250 (Homing switch debounce delay, milliseconds)

$27=2.500 (Homing switch pull-off distance, millimeters)

$28=0.000

$30=2048 (Maximum spindle speed, RPM)

$31=5 (Minimum spindle speed, RPM)

$32=1 (Laser-mode enable, boolean)

$100=160.000 (X-axis travel resolution, step/mm)

$101=160.000 (Y-axis travel resolution, step/mm)

$102=250.000 (Z-axis travel resolution, step/mm)

$110=10000.000 (X-axis maximum rate, mm/min)

$111=10000.000 (Y-axis maximum rate, mm/min)

$112=500.000 (Z-axis maximum rate, mm/min)

$120=3000.000 (X-axis acceleration, mm/sec^2)

$121=3000.000 (Y-axis acceleration, mm/sec^2)

$122=10.000 (Z-axis acceleration, mm/sec^2)

$130=320.000 (X-axis maximum travel, millimeters)

$131=230.000 (Y-axis maximum travel, millimeters)

$132=200.000 (Z-axis maximum travel, millimeters)

ok




---
**Andy Shilling** *May 08, 2018 16:25*

I have just realised grbl is not saving my settings when I change them, do I need to do something specific for this because with my old cohesion board I could just put the $3=3 in the command line and execute it and it would save that setting.




---
**Paul de Groot** *May 08, 2018 21:05*

Yes same here. Just enter $=value and that should save your new value. If you follow that with $$ then you should see a new value unless that value is not allowed e.g. enable disable 1 or 0. $5 is such a parm.


---
**Paul de Groot** *May 08, 2018 22:01*

Test if it does it for any field. If so, then I will post out a new one. I have never encountered a failing processor memory but it could be static electricity or just a defect. It would be the first.


---
**Andy Shilling** *May 08, 2018 22:22*

Once again I will check it tomorrow, I have the second board here so if this doesn't work I will put that one in and try that, I'm sure it's just something I'm not doing right as my knowledge of grbl and any sort of programming was 0 before owning a k40. 



I am wondering if the lack of saving the settings is causing this problem though as I have read that you should power cycle for the settings to take effect and as this keeps reverting back to standard settings I'm never going to get a true status reading for the endstops.


---
**Paul de Groot** *May 08, 2018 23:26*

No worries Andy, we will work it out together. It should be easy and straight forward. If it reverts  then there is something wrong with the board.


---
**Andy Shilling** *May 09, 2018 20:11*

**+Paul de Groot** Hello buddy I have been playing again and it seems the settings are being saved which is good but I can not work out why I am having a problem.



With Y endstop activated and sending ? in CNCj if feeds back



Idle|MPos:0.000,0.000,0.000|Bf:15,127|FS:0,0|Pn:Y|Ov:100,100,100



with X endstop activated I get 



Idle|MPos:0.000,0.000,0.000|Bf:15,127|FS:0,0|Pn:XZ>



and with both I get 



<Idle|MPos:0.000,0.000,0.000|Bf:15,127|FS:0,0|Pn:XYZ>



Am I right in thinking they are working fine and if so what is my next move although I get the same readings if I invert it $5=0 or $5=1.






---
**Paul de Groot** *May 09, 2018 22:18*

**+Andy Shilling** you might have a defect switch (normally closed) or it is picking up too much electrical noise from the laser power supply which causes all kind of weird behaviour that you cannot explain. The shield has suppression capacitors build in to deal with that. However in your case it might be not sufficient to suppress the noise. 



Solution is to buy a 100nF capacitor and solder that to the Y limit switch connections and/or install a shielded cable for that switch or try to re-route the Y limit wires first to see if that makes any difference.  Alternatively you can add 100nF capacitor (nano Farad, rated 50Volts) on the power terminals (24V and Gnd, 5V and Gnd). You can buy these capacitors at any Tandy/Radioshack type of store and they cost just a few cents. It's worth to try these tips. 



You can also check whether the problem ceases to exists when you unplug the Y axis motor. Just check the status via "?". If it ceases that the limit wires are picking up the noise from the Y axis stepper motor (so re-route either of them slightly or pull them away from each other). Let me know if that fixes the issue. If it does please take a photo so we can post that on the K40 forum.


---
**Andy Shilling** *May 09, 2018 22:43*

**+Paul de Groot** I'm just off to bed now I just caught your message. Our local supplier over here had just folded so I will order some from eBay, should I go for ceramic or polyester film for the capacitors? I'm guessing either will work.



I actually tried 3 different endstops as I have a few from when I was making my 3d printer but they are all doing the same. I will try the capacitor once they are here, also I will make up a new lead tomorrow and try the X endstop on the pins like the Y to see if that makes any difference. 



One thing I would like to ask is why does the "?" For X endstop show XZ but Y only shows Y within the command line?








---
**Andy Shilling** *May 10, 2018 19:41*

**+Paul de Groot** I HAVE SUCCESS!<b>!</b>!<b>!</b>!<b>!!</b>!<b>!</b>!<b>!</b>!<b>!</b>



The first board must have a problem, I ended up running a new X endstop signal wire to the pins along with the Y endstop and everything work as it should. So I thought the problem must be the daughter board, I checked continuity and everything was fineright down to the ffc connector on the shield.



I then checked continuity on the shield between pin 8 on the ffc and x signal pin and this was fine so the signal must be getting to the pin as intended but X endstop would not work using the ribbon?



As a last ditch effort tonight I swapped out the board to the second one on my order and run the usual $$ inverts that I need and the thing only bloody works as it should. I have no idea whats going on but for now I have a fully motion functioning grbl shield.



I will let you process this and see if you can understand what has happened and how this other one could possibly be wrong lol.


---
**Paul de Groot** *May 10, 2018 22:08*

Hi Andy that’s fabulous. At least you have it working. Let me know if you want me to replace your other board. It would be interesting to understand why that particular limit pin became defect. I test all boards extensively on my k40 to ensure they work well. Let me know So I can ship one out this Monday.


---
**Andy Shilling** *May 11, 2018 06:08*

**+Paul de Groot** Thank you, before we jump into replacing this one is there anything I could do like reflash grbl onto it or any other idea you can come up with?



I just don't understand how/why it will not work when pin 8 and the X endstop pin have continuity. Could it be modified to use the Z endstop pin for the X? I don't have a Z table so that doesn't bother me taking up that slot.


---
**Paul de Groot** *May 11, 2018 07:11*

Reflashing is complex since this is not a standard arduino. It is a different processor, So you need different libraries in the IDE. Than you need re-assign the limit pins. Next to this, I have not installed a usb bootloader on the processor board because of it’s has a small conflict with the laser output pin. The bootloader toggles the pin on start up which makes the laser flash 3 times shortly. People found this very annoying so I left it off. You still can flash the processor via the in circuit program connector but that requires you to have a programmer like St-link, jtag-ice or usbasp. Both cost about $5 plus the effort of installing the libraries and re-assigning the pins in the cpu-map. It’s all doable for me but if you have not done this before, it’s a bit of a learning process. I am happy to teach you in case you want to do more with that skill otherwise I might as well send you a replacement 😀


---
**Andy Shilling** *May 11, 2018 07:31*

After reading what you have just wrote I think it might be a little to much for me, if you are happy to send me a new one I would be very greatful. I will continue trying with this board for another week just to make sure I'm not doing anything wrong but as its a straight swap for the other I can not see what could be different.



Many Thanks for all your help and hopefully I can give you a better news soon.


---
**Andy Shilling** *May 15, 2018 12:35*

**+Paul de Groot** I've now got everything as it should but before I fit my refurbished Z table I thought I'd do test run.

 It seems my laser isn't turning off after running a job, this may be because I'm still running LW as though I am using it with the old cohesion board running grbl but are there any settings I should be looking out for?


---
**Paul de Groot** *May 15, 2018 12:39*

**+Andy Shilling** make sure you configure M5 as the command for turning off the laser in LW.


---
**Andy Shilling** *May 15, 2018 12:45*

I thought I had but I'll double check when I'm home in a few hours, thank you.


---
**Andy Shilling** *May 15, 2018 17:06*

**+Paul de Groot** I already had M5 in there just like I suspected, it is strange because the laser doesn't come on until it is told to but doesn't turn off?



I have worked out I have to disconnect from LW before the laser will turn off so it seems the M5 isn't working.


---
**Paul de Groot** *May 15, 2018 20:46*

**+Andy Shilling** can you try M3 instead? Btw your replacement gerbil is on its way. DHL should have send you a tracking id. If not, let me know and I email you


---
**Andy Shilling** *May 16, 2018 18:09*

Thank you kind sir, I look forward to receiving it. I have now tried M3 and that is doing exactly the same. the laser will not turn off until I disconnect the shield from LW.


---
**Paul de Groot** *May 16, 2018 21:05*

**+Andy Shilling** maybe you can try cncjs and issue the commands manually or stream a tiny gcode program that switches on the laser, draws a line and issues an M5. If that works then we can check which LW config is causing the issue.


---
**Andy Shilling** *May 16, 2018 21:28*

**+Paul de Groot** Im not sure how to go about sending a command manually could you give me something to try. I have used cncjs to turn the laser on and off using M4/on and M3/off buttons on the right hand side of the program but this will not work in LW. I think I will try updating it to see if that helps at all.



I've just seen the laser test button as well but this is not activating the laser.


---
**Andy Shilling** *May 16, 2018 21:46*

Ok I've just updated to the latest LW and the first 3 things I cut actually started and stopped as required, then I tried to raster an image and stopped it a little way in and the laser continued to fire.



I am leaning to the board giving this problem because the only way I can stop the laser is to disconnect LW and reconnect it ( the laser will continue to fire until I reconnect LW) or I pull the L wire from the board.



Is it worth me swapping over the shields between these two boards and seeing what happens then?


---
**Paul de Groot** *May 16, 2018 22:28*

**+Andy Shilling** yes that's definitely worth a try. Installing CNCjs is dead simple. It's an executable that does the install. In the black command area, you can issue gcode commands and it's a great debugging tool.

For example 

$H ; home

G1 x20 y20 F800 ; goto 20,20 at a speed of 800

M4 ; turn on the laser

G1 x40, y20 F800 S255 ; goto 40, 20 at 800, with a laser power of 255

M5 ; turn off

$H ; Home


---
**Paul de Groot** *May 16, 2018 22:30*

Alternatively you can use S0 as laser power zero to turn off the laser, it's not actually turning of but makes it safe that there is no laser beam (since it's infra and you cannot see it. BTW you can use you mobile phone camera to see the laser beam!)




---
**Andy Shilling** *May 18, 2018 10:58*

**+Paul de Groot** Ive just swapped over the sheilds and nothing has changed but what I have now realised is I don't thinkit is the sheild or R4, I have run a couple of test jobs and it seems the only time the laser stays turned on is if the job doesn't complete.



If I abort the job the Laser will continue to fire but if I let it finish it does in deed turn it off, would that be a grbl issue or a LW one?






---
**Paul de Groot** *May 18, 2018 11:03*

Hi Andy I have never encountered such an issue with the board myself. If you can run that same gcode via CNCjs, does it do the same? You can check the gcode in a non run execution mode with gerbil. Issue a C command and then run the gcode. Gerbil will report back whether it encountered an invalid gcode. Let me know your findings. I am curious about it.


---
**Andy Shilling** *May 18, 2018 13:41*

**+Paul de Groot** Both LW and CNCJS run the code fine to completion turning the laser off, but neither of them will kill the laser once I have aborted the job. Here is a link to the G-code file I was using, created in LW. It's just a greyscale eye.



[drive.google.com - eyetest](https://drive.google.com/open?id=1LnGUACzNvayv-ECrJ3mdyc9O_5VuEk_x)


---
**Andy Shilling** *May 18, 2018 15:22*

Here's a random thought, If i abort a job the laser remains on and I have to disconnect and reconnect the control but if I send M3 from the console within LW after aborting the job it will turn the laser off.



It's as though there isn't a link in the LW code to turn the laser off when aborting a job. Should I raise this issue in the LW community to see if anybody else has had this problem?


---
**Paul de Groot** *May 18, 2018 21:05*

**+Andy Shilling** M3 is actually reversing the spindle which translate in turning off. Ideally you should use M5. Did you try to run the gcode in non-execution / check mode? I am stuck here and scratching my head. I would suggest raising it on the awesome.tech k40 forum to see whether someone has encountered the same issue and maybe they figured out how to make that work. It almost sounds like the Laser On relay on the k40 shield is sticking. Did you try the other shield from your daughter? Does it do the same?


---
**Andy Shilling** *May 18, 2018 21:51*

**+Paul de Groot** I have tried both Shields and it happens with both, I have also tried M5 in the g code settings within LW and in the command console and this does nothing. The only way to stop the laser is to complete the job, disconnect and reconnect or run M3 within the console.



How do I run in non-execution mode? I take it that's in cncjs, until you told me about that program I had never heard of it so I don't know my way around it, sorry.



I will get on the forum either tomorrow or Sunday when I get time and see what we can come up with.


---
**Paul de Groot** *May 19, 2018 08:20*

**+Andy Shilling** yes it is in cncjs, where you issue a $C via the command line. Then run the gcode via the run button and gerbil checks the gcode and post back the results. See my wiki [github.com - grbl](https://github.com/paulusjacobus/grbl/wiki/Configuring-Gerbil-Grbl-v1.1e) or issue a $ and it returns the available commands.


---
**Andy Shilling** *May 23, 2018 09:13*

**+Paul de Groot**  Well what can I say, after getting no where with this bloody thing and no response on the forum I decided on a drastic coarse of action last night.



I removed every wire from every possible switch, plug and port I could and replaced the whole lot checking every connection. I also removed and reloaded LW on my pc and ...............





I now have a working K40 laser running on GERBIL. I have no idea what the problem was and what actually fixed it but with the first results I am very happy.i will keep you updated as I continue to play.


---
**Paul de Groot** *May 23, 2018 10:50*

Hi **+Andy Shilling** sometimes it can be just a bad connection or just interference between wires and powersupply. Remember this is the cheapest laser on the market. Some machines are very well put together and others never get to work well. Someone called it a clever design but assembled by monkeys😂


---
**Andy Shilling** *May 23, 2018 11:01*

**+Paul de Groot** I hear you but you doubt yourself when you've already rewired and upgraded the hell out of it lol.


---
**Paul de Groot** *May 23, 2018 21:24*

**+Andy Shilling** so true. My machine is pretty good from start. But i have seen machines that were deadly and sloppy... considering to develop an entire machine as a new product, so I don’t have to support k40 engineering faults like bad psu’s and skewed xy frames. K40 does not pay me but I’m doing their support 😂 


---
**Andy Shilling** *May 23, 2018 21:32*

**+Paul de Groot** 😂 to true, where else can you build a machine and say we've done nothing about safety or training but we'll let thousands of you loose with a potential lethal weapon. 🤯



I think I need to do some more testing as I have been running a few files and it seems something is wrong on one of the motions. When I cut a circle it doesn't join up, on the return it ends up a couple of mm out.


---
**Paul de Groot** *May 23, 2018 23:06*

**+Andy Shilling** that's typical symptom for a skewed frame or loose belts. Or in the cutting plugin, a delay has been set in the user interface. Set this to zero. Can you post a photo of this misalignment?


---
**Andy Shilling** *June 04, 2018 18:10*

**+Paul de Groot** sorry I've not got back to you I have been away, I have done a little testing with the circles but last night my 6 month old psu died on me. I'm not having much luck with this thing at the moment.



From the few tests I managed to run I am not convinced either way where the problem lies but I can not do any more until I rewire the old psu back in or get the supplier to cough up on a replacement.

Once I'm back up and running either way I will get back in contact. Thanks for the replacement board that is sat here on stand by should the need be there ( hopefully not).






---
**Paul de Groot** *June 04, 2018 21:34*

No worries, I am traveling at the moment and will be back on the 9th of June😁


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/LXBdj6fHnK8) &mdash; content and formatting may not be reliable*
