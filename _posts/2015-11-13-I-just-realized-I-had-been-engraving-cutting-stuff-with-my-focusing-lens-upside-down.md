---
layout: post
title: "I just realized I had been engraving/cutting stuff with my focusing lens upside down"
date: November 13, 2015 18:32
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
I just realized I had been engraving/cutting stuff with my focusing lens upside down. Whoops. The difference? The one on the left is with the lens in the correct orientation. The edges are also a lot straighter as opposed to being chamfered (which is what started my whole diagnosis of why that was happening.)

![images/6837e4091447bc684494697f84185ff7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6837e4091447bc684494697f84185ff7.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Scott Thorne** *November 13, 2015 19:18*

What material was that done on? Looks good!


---
**Ashley M. Kirchner [Norym]** *November 13, 2015 19:20*

Fluorescent green acrylic.


---
**Scott Thorne** *November 13, 2015 19:38*

Looks cool...I'm gonna order some.


---
**Ashley M. Kirchner [Norym]** *November 13, 2015 19:43*

I get mine from Inventables.com, cast sheets.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 14, 2015 00:09*

There is a huge difference in the effect on both those pieces. Glad you found the problem :)


---
**Jose A. S** *November 16, 2015 04:12*

Could you share the right orientation of the lens?...graphicaly :) Thank you!


---
**Ashley M. Kirchner [Norym]** *November 16, 2015 04:50*

Look at it. If you can see your reflection, that's the right way UP.


---
**Scott Thorne** *November 16, 2015 10:56*

The curved inward side goes down, I.e the curved outward side goes up.


---
**Jon Bruno** *November 19, 2015 20:55*

**+Scott Thorne** that description isn't really helpful.. I'm not sure what you mean by that?


---
**Scott Thorne** *November 19, 2015 20:59*

The lens is going to be shaped slightly like a bowl, sort of like a contact lens...turn the bowl upside down and insert, concave side down.


---
**Jon Bruno** *November 19, 2015 21:01*

Ahhh.. That I can visualize. Thanks for the clarification


---
**Scott Thorne** *November 19, 2015 21:02*

Lol....anytime Jon....glad I could help.


---
**Ashley M. Kirchner [Norym]** *November 19, 2015 22:05*

Looking at it also helps. You can only see your reflection when looking at it the correct side. That side needs to be up.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 06, 2015 13:08*

Wow, I've been having problems with cutting through leather & finally just found out that my lens was upside-down too. Haha.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Ec1bf1gwmBf) &mdash; content and formatting may not be reliable*
