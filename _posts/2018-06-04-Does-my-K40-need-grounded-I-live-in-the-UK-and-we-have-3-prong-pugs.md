---
layout: post
title: "Does my K40 need grounded? I live in the UK and we have 3 prong pugs"
date: June 04, 2018 14:40
category: "Discussion"
author: "Aaron Cusack"
---
Does my K40 need grounded? I live in the UK and we have 3 prong pugs. We don't usually ground stuff.



![images/f58d183e9c758dc4758f73956c8ddd9c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f58d183e9c758dc4758f73956c8ddd9c.jpeg)
![images/4c7d04e2931f0527438bde61cccec234.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c7d04e2931f0527438bde61cccec234.jpeg)

**"Aaron Cusack"**

---
---
**Dushyant Ahuja** *June 04, 2018 14:53*

Change the cable to a proper 3-pin cable. The one in the photo looks like it had a 2-pin plug and the Earth pin on the adapter looks as if it is just a dummy. 


---
**Ray Kholodovsky (Cohesion3D)** *June 04, 2018 15:19*

You should absolutely ground it. 


---
**Don Kleinschnitz Jr.** *June 04, 2018 16:03*

Did your K40 come with a 2 prong plug like in the photo?

Use a 3 prong plug and insure that the wall outlet has a proper ground. 


---
**BEN 3D** *June 04, 2018 16:31*

Ground will safe your life, in case of a case!


---
**James Rivera** *June 04, 2018 17:51*

Holy shit!  Yes, ground it!  20,000+ volts will KILL you!


---
**Miguel Sánchez** *June 04, 2018 18:22*

Grounding through user body is usually more painful, sometimes even lethal, that is why grounding wire is always a good thing to have connected.


---
**Justin Mitchell** *June 05, 2018 09:50*

Replace the mains lead with a proper british spec one.  test from the earth pin of the plug to the chassis of the K40 to make sure it is internally wired correctly. Then you are good. I would trust the ring main earthing to be better tested and protected (by your RCDs/MCBOs) than a cobbled together seperate earth would be.



or, to put it another way:

Just bin the supplied cable and its death-dapter, they will not be properly earthed and may well be actively dangerous to use, use a good quality standard british spec "kettle lead"  instead.

Then make sure whoever assembled the k40 didnt cut corners, ensure that the earthing wires inside are properly secured and wired, do an end to end test with a multimeter to make sure the chassis and psu grounds are properly wired to the mains lead earth pin.




---
**Aaron Cusack** *June 05, 2018 10:35*

Thanks for the replys, I got a proper 240v lead and plugged it in. Just like the normal UK plug. Thankfully it didn't kill me.


---
**Justin Mitchell** *June 05, 2018 10:47*

**+Aaron Cusack** please do a quick check with a multimeter set to resistance or continuity test, between some bare metal on the chassis and the earth pin on the plug.  just to make sure they didnt leave off the earth wire inside the case


---
**Danko Andruszkiw** *June 07, 2018 07:21*

mine is grounded to the back of the machine directly to a copper rod in the earth 




---
*Imported from [Google+](https://plus.google.com/104714035697130129832/posts/LSFgZP6ARkS) &mdash; content and formatting may not be reliable*
