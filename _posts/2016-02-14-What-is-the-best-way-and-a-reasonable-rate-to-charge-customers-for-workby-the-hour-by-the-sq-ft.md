---
layout: post
title: "What is the best way and a reasonable rate to charge customers for work...by the hour, by the sq ft?"
date: February 14, 2016 21:02
category: "Discussion"
author: "Anthony Bolgar"
---
What is the best way and a reasonable rate to charge customers for work...by the hour, by the sq ft? 





**"Anthony Bolgar"**

---
---
**ThantiK** *February 14, 2016 23:45*

DeltaMaker usually does a small setup fee, plus like 2.00/hr + material @ retail cost.  But that's for 3D prints which take a lot longer than a laser cutter/engraver.


---
**I Laser** *February 15, 2016 02:04*

"2.00/hr" wow that's a super cheap hourly rate, hope they are making up the difference on the other charges...



Depends what you're doing/offering. I just looked up the competition, noted what they were charging and charge accordingly. Most of them have serious laser setups, commericial rent, etc, so have much higher overheads. I decided not to undercut them as I'm not interested in creating a race to the bottom. ;)


---
**ThantiK** *February 15, 2016 05:08*

**+I Laser**​, we do mandatory 48hr burn-in / testing, so it's really just fitting someone's needed print into our testing 


---
**I Laser** *February 15, 2016 05:56*

Ah okay, so it's 2.00/hr with a minimum 48 hours up front regardless. I was wondering how you could possibly cover costs at $2 per hour!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/R4RmesqtefX) &mdash; content and formatting may not be reliable*
