---
layout: post
title: "Anyone have any experience with this pattern generator site?"
date: July 18, 2016 18:23
category: "Software"
author: "Tev Kaber"
---
Anyone have any experience with this pattern generator site?  Wish it were free, but $15 seems reasonable for that range of patterns and tools...



[http://boxdesigner.frag-den-spatz.de/](http://boxdesigner.frag-den-spatz.de/)





**"Tev Kaber"**

---
---
**Ariel Yahni (UniKpty)** *July 18, 2016 18:33*

Nice find. For Boxes, gears there are plenty other free solutions. 


---
**Tev Kaber** *July 18, 2016 19:10*

I've seen a few box generators, but only for really basic boxes. Any good ones out there?


---
**Ariel Yahni (UniKpty)** *July 18, 2016 19:48*

For sure more detailed or intrigued boxes will not be free.  The link above does show some variety of pre-made boxes.  If you draw them make them parametric is not difficult 


---
**Greg Curtis (pSyONiDe)** *July 19, 2016 05:31*

[http://www.makercase.com](http://www.makercase.com)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/ZYLQXV1ToMH) &mdash; content and formatting may not be reliable*
