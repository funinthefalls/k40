---
layout: post
title: "Hi All I just wanted to share some things I have figured out about the K40 Its cold in my workshop..."
date: November 10, 2016 10:42
category: "Discussion"
author: "Surrey Woodsmiths"
---
Hi All



I just wanted to share some things I have figured out about the K40



Its cold in my workshop... really cold!  So I decided to add some basic antifreeze to the water as recommended in various forums.  The next time I used it, it started making a high pitch whistling noise that sounded like it was coming from the PSU. The tone would change with the power and the sound only happened some of the time.

I did a lot of research into this and, before trying to epoxy the coil (as often recommended for this symptom) I swapped the water for fresh de-ionised water and the sound immediately stopped.  Sorted!  

I still have no idea why antifreeze would cause the noise.



Only problem now is that I have no antifreeze, the water will freeze when it gets below 0 deg!



The coolant I used was just standard car mix (cant remember the brand off the top of my head) but it was "as recommended".  Does anybody know what I can put in there?  Has anybody experienced similar issues?





**"Surrey Woodsmiths"**

---
---
**Don Kleinschnitz Jr.** *November 10, 2016 12:18*

Very strange symptoms seems like what is in the coolant shouldn't matter?



Many setups are using aquarium heaters to keep water above freezing while keeping pump on. I am not using a heater but I may have to if I move my system to the garage.

Most reccommend RV antifreeze over car antifreeze. Also Dowfrost, but it's expensive.

Can you point me to a reference on epoxy on coil.

As a stab in the dark, I would check all the grnds  to insure you don't have anything floating. 




---
**Surrey Woodsmiths** *November 10, 2016 12:54*

Indeed!  



In theory it shouldn't matter too much whats in the coolant as it does not come into contact with anything.  It certainly seems to though!



I found a few you tube videos and forum posts from people with similar issues so am not the only one who has experienced this.  I will double check all grounds etc, although its been totally fine after changing back to just de-ionised water so I guess everything is as it should be.



Little bit concerned about leaving pumps on all the time as I may not be in the workshop for a few days at a time.  Will try alternative antifreeze products and see if they make any difference.  Hopefully we have a couple more weeks in the UK before temperatures drop too much






---
**greg greene** *November 10, 2016 13:46*

Do they not sell winter windshield washer fluid over there mate? The stuff we have in Canada is good till -30 or -40 C


---
**greg greene** *November 10, 2016 13:47*

Rubbing alcohol can be added - but it does tend to evaporate


---
**Evan Ford** *November 10, 2016 13:57*

I use RV antifreeze to cool my K40 without issue. It is mixed 5 parts distilled water to 1 part antifreeze. 



RV antifreeze is made from propylene glycol and automotive antifreeze is ethylene glycol with corrosion inhibitors.  Maybe the corrosion inhibitors are metallic? Possibly causing induction?


---
**Cesar Tolentino** *November 10, 2016 14:45*

What about a tiny water heater used in aquarium.  And wrap your container with insulation or blanket. Bottom, side and top.  That's what I do on my fish tank when it's cold outside like yours. 


---
**Cesar Tolentino** *November 10, 2016 14:48*

Please ignore my post. This will solve the water on the container but the water remaining in the tube will still froze.


---
**Don Kleinschnitz Jr.** *November 10, 2016 14:48*

Generic car antifreeze has:

... Ethylene glycol 

... Sodium tetraborate pentahydrate (borax)

[http://www.chemicalbook.com/chemicalproductproperty_en_cb9413179.htm](http://www.chemicalbook.com/chemicalproductproperty_en_cb9413179.htm)



I am not a chemist so I do not know what this means to the situation. Coudnt find anything on conductivity.

[chemicalbook.com - Sodium tetraborate pentahydrate &#x7c; 12179-04-3](http://www.chemicalbook.com/chemicalproductproperty_en_cb9413179.htm)


---
**Surrey Woodsmiths** *November 10, 2016 14:55*

I will try some winter windshield washer fluid next as its probably the easiest to get.  I am still a little baffled why adding car coolant would cause the high pitch noise.  I noticed the noise also came out of the stereo when it was turned on nearby so must be throwing out electrical interference all round the workshop.  


---
**Don Kleinschnitz Jr.** *November 10, 2016 15:05*

Found this: 

[https://www.reddit.com/r/lasercutting/comments/40pauf/cooling_with_antifreeze/](https://www.reddit.com/r/lasercutting/comments/40pauf/cooling_with_antifreeze/)



You can use RV antifreeze with no issues. 1 part RV antifreeze to 3 parts distilled water.

But here's the thing, it will still freeze and you will crack your tube. And then you will cry and have a very bad day. Read the last two paragraphs in this article for more info, and keep in mind the burst points are for copper. Glass with crack much easier. [http://www.jamestowndistributors.com/userportal/document.do?docId=1144](http://www.jamestowndistributors.com/userportal/document.do?docId=1144)

What you need to do is put an aquarium heater in your bucket/tank/etc and set it for 60 degrees and circulate the water constantly. This is the only way to prevent a cracked tube if your space is unheated.


---
**Don Kleinschnitz Jr.** *November 10, 2016 15:10*

Hope these are not annoying but I am interested in this problem:

"The automotive coolants have flourescent dye, which is handy for leak detection, but only make a bigger mess for the laser engraver. I use coolant sold for TIG welding machines. It's very clean and pre-mixed and does not have any dye in it. It is also a very good di-electric, which would not be a bad thing for a laser. You can get it at most better welding suppliers."


---
**greg greene** *November 10, 2016 15:25*

Never thought of that Don - thanks !


---
**Don Kleinschnitz Jr.** *November 10, 2016 15:25*

Looks like the problem may be the conductivity of the anti-freeze especially if it is highly concentrated.

I have seen multiple failures of the epoxy and premature leak down on this forum, 

We have also seen cases where the coolant water was sitting above ground when the laser was firing??



I wonder if they are related?



Brings and interesting question to mind. Should the water be grounded or not? I guess we could connect an isolated water system to ground through an ammeter? 



<s>-------</s>

From a forum:

"I would recommend to NOT use any anti-freeze in the coolant for the 40 or 60 watt laser. There are definite problems with doing that. I haven't had issues with coolant in the 30 watt tubes yet.



My experience is that the coolant becomes conductive at the high voltages. The electricity intended for the laser gas will pierce through the epoxy seal (holding the lens/mirror at the end of the of the laser tube). I can try to explain this. 



1) The primary path... The resistance of the laser gas inside the tube is (Let's use a dumb/easy number..) 100,000 ohms. This allows the arc to jump from anode to cathode... Inside the center chamber of the laser tube.



2) The alternative electrical path is to pierce or conduct through the epoxy...pierce/conduct into the coolant chamber ... Conduct through the coolant to the frame bulkhead fitting (brass piece where you plug in the chiller) ... Then conduct in the metal frame back to the laser power supply. Let's say the alternative path has a resistance of 80,000 ohms. With the coolant flowing, the resistive value is constantly changing.



The result is a current divider between path 1 and path 2. The best case is that you have a power loss in the tube. The worse issue is that you could be creating a hole in the epoxy and are releasing the laser gas from the laser tube.



This all comes from experience.. I do NOT recommend running the laser with antifreeze... I do recommend storage of the laser tube with antifreeze. You need to rinse it out pretty well before you try to use it.



One good thing is that the high voltage is "return grounded" as soon as it hits the bulkhead fitting.



other note... RV antifreeze will grow a nasty mold."








---
**Mark Leino** *November 11, 2016 01:23*

**+Don Kleinschnitz**​ 

That is a great find! 



I would also be concerned with the viscosity of rv antifreeze when temperatures get way down. Possibly the extra viscosity could cause the pump to work harder, generating a high pitched squeal?



 Living in MN, I have found that rv antifreeze DOES freeze when temps are below 15-20F. It doesn't freeze solid but it gets slushy. It's used because it's non toxic (ethanol and water) and can be rinsed out of water lines without leaving a residue (it can stain things though). 



I would be concerned about leaving rv antifreeze in a laser tube. It's not supposed to expand enough when frozen to break water lines. In a glass line, different story.  



Source: I worked at a camper dealer as a service tech.



I never saw rv antifreeze grow mold. 


---
*Imported from [Google+](https://plus.google.com/105599569700790157394/posts/YxGWBuFWkxk) &mdash; content and formatting may not be reliable*
