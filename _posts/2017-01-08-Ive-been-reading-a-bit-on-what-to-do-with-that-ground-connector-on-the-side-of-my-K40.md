---
layout: post
title: "I've been reading a bit on what to do with that ground connector on the side of my K40"
date: January 08, 2017 00:42
category: "Modification"
author: "Dave Durant"
---
I've been reading a bit on what to do with that ground connector on the side of my K40. I'm in the US and a condo in an old factory-type place like 100 years old that was totally gutted & rebuilt maybe 12-13 years ago. 



2 questions from somebody that <b>really</b> does not know electronics. 



1: I'm nearly positive that the electrical here is all up to recent spec and that that 3rd prong on the cord will be grounded. Is there anything easy & safe that I can do to confirm this?



2: The back of the machine looks like it has 2 switched outlets. I'm guessing that when I power the machine on, these also get power. Both of these 2-prong plugs. Would it make sense to convert these to 3-prong plugs and tie the 3rd to the ground so everything is common? Possibly a stupid question - I really have no idea. 



Thanks!





**"Dave Durant"**

---
---
**Don Kleinschnitz Jr.** *January 08, 2017 00:57*

Get one of these to test the sockets: [amazon.com - Ideal Industries 61-501 GFCI Receptacle Tester: Circuit Testers: Amazon.com: Home Improvement](https://www.amazon.com/Ideal-Industries-61-501-Receptacle-Tester/dp/B000NB85LC/ref=sr_1_8?s=hi&ie=UTF8&qid=1483834081&sr=1-8&keywords=plug+tester)



If you are going to plug devices with 3 prong plugs into the outlets convert them to 3 prong and wire them correctly using this to verify they are. Good Idea anyway :).



[https://www.amazon.com/gp/product/B00T7DY54I/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00T7DY54I/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Don Kleinschnitz Jr.** *January 08, 2017 11:14*

Some additional considerations I thought of: insure that the capacity of the machines input plug can handle the full load of your machine + the load of these two outlets when loads are plugged in.

Create a budget by adding up the total current you are going to draw and insure the input plug, sockets and cables are rated for that current. 

Most devices have the current draw labeled on them. If not you can calculate it from the Wattage rating on the device. 

P =Voltage x Current. 

If your main plug has a fuse (if not consider changing it) insure that it is sized for your total load. 

These fuses could be used to separately fuse each external device plug.

[amazon.com - yueton Pack of 10 AC 15A 125V Black Electrical Panel Mounted Screw Cap Fuse Holder - - Amazon.com](https://www.amazon.com/yueton-Black-Electrical-Mounted-Holder/dp/B012CTCWES/ref=sr_1_6?ie=UTF8&qid=1483873477&sr=8-6&keywords=ac+fuse)



Finally while your changing things you might consider how you are going to conveniently turn those 2 loads on and off. 

1. leave things alone and turn them on and off with the main switch 

2. turn them on/off at the load. 

3. add AC switches to the control panel

4. in its most complex configuration add relays that can be remotely controlled. 

I initially was configured like #1 but quickly realized that I wanted certain external devices on/off for certain jobs. i.e sometimes I did not want the air assist on. I ended up with #4. 




---
**Dave Durant** *January 19, 2017 03:27*

Thanks for the info! Got a plug tester thing down at Home Depot, similar to the one you linked, and I do indeed have Kosher 3-pronged plugs. 



For the switched plugs, I was mostly thinking about the water pump - just always having that on if the machine is on. I think air assist and exhaust will probably be on more than the tube but it seems like serious overkill to have them on always...


---
**Don Kleinschnitz Jr.** *January 19, 2017 12:23*

**+Dave Durant** I have switches on my air assist and vacuumn as I do not want them on all the time and for every job. The pump, pointer led and cabinet light I turn on with the main switch. The pump being left off kills the tube so I would rather burn out a pump than a tube.


---
*Imported from [Google+](https://plus.google.com/+DaveDurant/posts/K9AADs3oJER) &mdash; content and formatting may not be reliable*
