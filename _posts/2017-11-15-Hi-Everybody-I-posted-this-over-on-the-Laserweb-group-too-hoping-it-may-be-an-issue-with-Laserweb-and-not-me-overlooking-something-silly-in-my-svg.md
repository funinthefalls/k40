---
layout: post
title: "Hi Everybody, I posted this over on the Laserweb group too hoping it may be an issue with Laserweb and not me overlooking something silly in my svg"
date: November 15, 2017 05:04
category: "Discussion"
author: "Tony Marrocco"
---
Hi Everybody,



I posted this over on the Laserweb group too hoping it may be an issue with Laserweb and not me overlooking something silly in my svg.





Can anybody point me in the right direction as to what the heck I'm doing wrong with this file?   I'm running a K40 with the Cohesion 3d board.



I've looked in corel and inkscape and can't find these extra paths it's generating.  The knife I took from another file I engraved just fine (it was on laserweb3 though), all I did was make it bigger and cut the lines where the letters intersected using corel's "virtual segment delete" tool.  I've also tried to make the file into a bitmap and then trace it to get fresh paths but it isn't working so I feel I'm doing something wrong.  I'm a little bit of a noob with this so I don't doubt it...

 

[https://www.dropbox.com/s/lu22gx3kne0msm6/no%20bitching%20in%20my%20kitchen%20revision.svg?dl=0](https://www.dropbox.com/s/lu22gx3kne0msm6/no%20bitching%20in%20my%20kitchen%20revision.svg?dl=0)



^^^ Svg in question



Thanks ahead of time!



Result of that SVG, extra lines highlighted in yellow.

![images/f5330b19e6f5bd91218331fd0b06bfdb.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f5330b19e6f5bd91218331fd0b06bfdb.png)



**"Tony Marrocco"**

---


---
*Imported from [Google+](https://plus.google.com/112686049554441919913/posts/ix7qHfACKqw) &mdash; content and formatting may not be reliable*
