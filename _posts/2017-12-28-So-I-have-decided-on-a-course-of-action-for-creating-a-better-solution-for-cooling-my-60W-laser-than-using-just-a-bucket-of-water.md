---
layout: post
title: "So I have decided on a course of action for creating a better solution for cooling my 60W laser than using just a bucket of water"
date: December 28, 2017 04:39
category: "Modification"
author: "Anthony Bolgar"
---
So I have decided on a course of action for creating a better solution for cooling my 60W laser than using just a bucket of water. What I have come up with I have nicknamed the CW-3000 Plus. Before you start freaking out with comments like "That is only a radiator, it's a piece of crap, etc, finish reading this first.



What I have planned is an upgradable, cost effective method. It utilizes the basic CW-3000 Chiller connected to an aquarium refrigerted cooler as a 2nd stage. The water leaves the tube, enters the CW-3000 and loses much of the heat through the internal radiator and fan. It is then pumped to the aquarium chiller (link at bottom of post), and from the chiller into the tube. 



This effectively gives you a  W-500 setup for less than the cost of a CW-5000.



You can start with just the CW-3000 and if it is not enough on its own to cool the water, then you can add the aquarium chiller.



You can get a CW-300 for $200 and the aquarium chiller for $230 for a total of $430 which is much less than the CW-5000. I picked up  CW-300 locally today, and have place an order on ebay for the chiller unit. I will let you all know how effective it is or is not once I have the system setup, until the chiller arrives I will just use the CW-3000 on its own.



[https://www.ebay.com/itm/Thermoelectric-water-Chiller-cooling-water-machine-for-Aquarium-Fish-Tank-100L/332421544644?hash=item4d65de3ac4:g:WEQAAOSwdjdZ7KsX](https://www.ebay.com/itm/Thermoelectric-water-Chiller-cooling-water-machine-for-Aquarium-Fish-Tank-100L/332421544644?hash=item4d65de3ac4:g:WEQAAOSwdjdZ7KsX)





**"Anthony Bolgar"**

---
---
**Nick R** *January 09, 2018 04:01*

Please let us know how this goes!




---
**William Travis** *January 30, 2018 23:09*

I too am looking forward to hearing the difference between having one then the second chiller.  Thanks!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/MAfwmDRZorq) &mdash; content and formatting may not be reliable*
