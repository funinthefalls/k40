---
layout: post
title: "Tip for bringing out an etch"
date: September 02, 2018 20:03
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Tip for bringing out an etch




{% include youtubePlayer.html id="watch" %}
[https://www.youtube.com/watch?time_continue=117&v=fFwXT2XgQKs](https://www.youtube.com/watch?time_continue=117&v=fFwXT2XgQKs)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *September 03, 2018 13:23*

I would imagine it is important to apply a good layer of finish  to seal the surface before engraving. 


---
**HalfNormal** *September 03, 2018 13:37*

That is always a good idea.


---
**Ned Hill** *September 09, 2018 19:52*

As someone who does a lot of engraving infill I definitely agree that you would need to seal the wood first to help prevent edge bleed.  Also a shallow engraving will work better.


---
**ottolaser share** *October 10, 2018 13:12*

very nice


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/bobkDvTV29Q) &mdash; content and formatting may not be reliable*
