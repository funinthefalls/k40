---
layout: post
title: "So I've updated my Google Drive folder containing the K40 Software, as I finally got around to downloading a version of CorelDraw x5 today (to test if it works with the CorelLaser plugin)"
date: April 30, 2016 12:31
category: "Software"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I've updated my Google Drive folder containing the K40 Software, as I finally got around to downloading a version of CorelDraw x5 today (to test if it works with the CorelLaser plugin). It does work perfectly (thanks Donna for putting me onto it).



So contained in the folder now is the original K40 Software CD (as a rar file), as well as CorelDraw x5 (rar) & CorelLaser 2013.02 (rar).



These are for anyone running the stock software. I've also included a text file (How To Install & Run - Download & Read Me before dowloading the RAR files) in order to explain the installation process. I suggest you read this first.



In it I have only explained how to install the CorelDraw x5, as CorelDraw12 (in the original K40 Software CD.rar) seems to crash occasionally & I am recommending to use x5 in preference of it. I also explain the install of CorelLaser 2013.02.



I have also modified the English.ini file (to correct spelling & grammatical errors that were driving me nuts, e.g. USB Key not pluged; Engraving machine disconected, etc.) & the Manufacturer.ini file (since looking at a bunch of random Chinese characters doesn't make sense for those of us who don't read Chinese).



Feel free to grab whatever parts you need/interested in.



edit: after a few issues with Google Drive being funny, I've decided to put it into my Dropbox instead.



[https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jose A. S** *April 30, 2016 16:27*

Wow!..Thank you!


---
**Pigeon FX** *April 30, 2016 19:05*

Having a little trouble with the instillation of the CorelLaser Plugin.



(TL;DR No English.ini)



Guide says to Extract [CorelLASER 2013.02.rar] to a new folder.

	Inside the .rar file there will be 2 folders (*) containing some files (-) & 1 file (-):
	- CorelLASER 2013.02.exe

	* Languages

		- English.ini

	* Manufacturer

		- About.bmp

		- Logo.bmp

		- Manufacturer.ini



But.....there is no "CorelLASER 2013.02.rar" though there is a folder named "The latest CDR software" that contains the CorelLASER 2013.02.exe and the Manufacturer folder but no Languages folder or English.ini.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 19:28*

**+Pigeon FX** You obviously grabbed the K40 Software CD.rar file? You don't want that. That is the actual CD that came with the K40 (which has CorelDraw12 on it). Total junk basically.



There is a specific file called "CorelLASER 2013.02.rar" in the shared folder. Also, "COREL DRAW  X5 WITH KEYGEN.rar". You want those 2 files.



Let me know if any more issues, maybe I wrote the ReadMe First file not so clear & need to specify not to download the "K40 Software CD.rar" file.



edit: I updated the ReadMe First file to make it clear not to bother getting the K40 Software CD.rar file. Thanks for pointing that out.


---
**Pigeon FX** *April 30, 2016 19:56*

**+Yuusuf Sallahuddin** I can't see the  "CorelLASER 2013.02.rar" in the shared folder. only "COREL DRAW  X5 WITH KEYGEN.rar" and "K40 Software CD.rar"?



[http://imgur.com/m0voa4d](http://imgur.com/m0voa4d)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 20:17*

**+Pigeon FX** Let me check that & see what is going on. It's there on my pc (in the same folder), so it should be there in the drive. I will check it & get back to you shortly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 20:20*

**+Pigeon FX** Strangely it is in the folder, but for some reason isn't showing. Direct link to that file: [edit removed this link as I deleted that version of the file]



Let me know if it works or not. It does on my end but I am logged into my G Drive. I've set it to anyone with link can view, so hopefully you can get it this time.



edit: I think I figured out what is going on. For some reason G Drive is flagging it as an "infected file". I just downloaded it to check it's contents to make sure it's okay & it seems to be missing the 2 folders. I will redo it & upload it again.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 20:27*

**+Pigeon FX** I've removed that file & uploaded again, with the Languages/Manufacturer files in it.

[https://drive.google.com/file/d/0Bzi2h1k_udXwZklrVFFOUElVSWs/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZklrVFFOUElVSWs/view?usp=sharing)



I've scanned it with Windows Defender & it says it's fine, but not sure what G Drive is flagging in it. [edit: after another download attempt of the new rar file, Google is still flagging it as "infected". I can't see why on my end, but run whatever virus scan you use over it to check it.]


---
**Pigeon FX** *April 30, 2016 20:37*

Just getting the "We're sorry. You can't access this item because it is in violation of our Terms of Service." on the above link? (thank you for putting this together in one place, even if google is being silly).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 20:57*

**+Pigeon FX** Wow, Google is being a bit odd. I can put the file in my Dropbox instead then & I'll place the link here. Was the CorelDraw X5.rar file downloadable?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 21:01*

**+Pigeon FX** Here is a dropbox link: [https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



Should be able to grab the CorelLASER 2013.02.rar from there. The CorelDraw X5.rar will take a bit longer to sync (as it's around 550mb).


---
**Pigeon FX** *April 30, 2016 21:02*

Yes, the  CorelDraw X5.rar, and K40 CD rip were and still are working (just double checked them). just mising that allusive "CorelLASER 2013.02.rar" folder.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 21:03*

**+Pigeon FX** You'll be able to get it from the Dropbox link now with no problems (hopefully).


---
**Pigeon FX** *April 30, 2016 21:33*

Thank you Yuusuf, all files there and working. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 30, 2016 21:57*

**+Pigeon FX** You're welcome. X5 is much better than the old v12 that they originally provided. I still won't use for designing, but it seems more stable on Win10 than the v12 was.


---
**Pigeon FX** *May 01, 2016 01:00*

**+Yuusuf Sallahuddin** Yes, cant see myself using it to design with, but it looks like a good middle main between what I plan on using (Illustrator and Fusion 360) as it seems to support .dxf and .Ai files. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 01:23*

**+Pigeon FX** Yeah I use AI for all my designing then import it in. Been using CD12 up until now, then someone recently mentioned that X5 works, so I decided to have a go with that. No crashes from opening the "Object Browser" gets my vote.


---
**Tony Sobczak** *May 01, 2016 02:28*

Keygen files almost always get flagged and quarantined. Norton is especially good at that.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 05:40*

**+Tony Sobczak** Yeah, I've had the same issue for 10+ years. I stopped using non-original software after dealing with it for so long. It's funny though, even with so many years of keygens & the likes, I've never received a virus from it.


---
**Mishko Mishko** *May 03, 2016 11:38*

**+Yuusuf Sallahuddin** FYI, Corel Draw X7 works fine with CorelLaser, too...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 03, 2016 11:56*

**+Mishko Mishko** Cool. Thanks for sharing that Mishko. Haven't tried it myself.


---
**Rick Tennyson** *May 07, 2016 02:56*

Worked great on my WIN10, thank you!


---
**Rodney Huckstadt** *August 01, 2016 09:34*

not working anymore ?


---
**Rodney Huckstadt** *August 01, 2016 09:34*

corel x5 come up with error 24 and a reinstall won't accept serial number


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 10:31*

**+Rodney Huckstadt** Should be working. Did you follow the instructions for the Keygen correctly? (i.e. have to leave it open while installing & then do the activate stuff at the end).


---
**Rodney Huckstadt** *August 01, 2016 11:10*

sure did, it has stopped working on all my machines ? i will try and do a registry clean and go again


---
**Rodney Huckstadt** *August 01, 2016 11:20*

I keep getting a "the serial number is invalid" error


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 11:46*

Oh, is this after installing that you are getting the "serial number is invalid" issue?



I found that if I open CorelDraw x5 itself (instead of using the CorelLaser to open it) eventually it will decide that it is a pirated version. What I did to prevent that was go into Windows Firewall settings & block access for Corel Draw (i.e. so it cannot connect to internet). If it can't connect to internet, it can't check if the serial is valid or not.


---
**Rodney Huckstadt** *August 01, 2016 11:51*

it is as I install but will try the firewall thing as well


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 11:51*

**+Rodney Huckstadt** [https://1drv.ms/u/s!AqT6a-6Vl15Nh5AfwubiG4PfCttIVQ](https://1drv.ms/u/s!AqT6a-6Vl15Nh5AfwubiG4PfCttIVQ)



That is a link to an image in my OneDrive showing the 2 files to block in the firewall. I am using Win 10. I searched in start menu for "Firewall" then have to choose the "Windows Firewall with Advanced Security" option. Then go to Outbound Rules. If the two files already exist, edit them to be blocked. If they don't exist, create new Rules & block them.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 11:53*

**+Rodney Huckstadt** During install, I am unsure what would cause that. Does it happen even if you keep generating new keys? I am unsure what other help to offer as I have not experienced this myself.


---
**Rodney Huckstadt** *August 01, 2016 11:54*

firewall does not do it,  this error is as I start the installer and it asks the first time for serial number


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 11:57*

Could you try disable your internet connection whilst doing the first install. Maybe it is somehow connecting to the internet to verify the serial before you get a chance to block it. Are you using a serial from the Keygen file? or some other serial


---
**Rodney Huckstadt** *August 01, 2016 12:12*

keygen, and have tried that, will keep trying other ideas , but thanks for the help as well :) can only keep trying and see :)


---
**Rick Tennyson** *August 01, 2016 21:19*

Same thing happening on my end, exactly the same errors. Cant get Corellaser to work after it worked for 6 months.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 01, 2016 21:22*

I will give it a try installing a new user profile on my system later on this evening when I return home & see if I encounter the same errors. I'll report back then any findings.


---
**Rodney Huckstadt** *August 01, 2016 22:35*

cool :)


---
**Rick Tennyson** *August 01, 2016 23:40*

I removed all the Corel files. Reinstalled and it's working under the eval version now, no internet connected. 


---
**Rodney Huckstadt** *August 01, 2016 23:55*

did the same, working for now will see what happens when the evaluation period ends


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 07:01*

Try blocking it in your firewall too. Another option I used in the past was creating a new user profile in Windows everytime whatever trial/eval version ran out. Kept me going until I could get the real version of what I needed. Also, if you open the CorelDraw via the CorelLaser.exe file, I noticed that it never came up with any messages about the trial/eval period.


---
**Rodney Huckstadt** *August 03, 2016 03:26*

never had the trial error, just the cd-key one :) and it is blocked in the firewall and all over the place :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 03:36*

Yeah I just uninstalled & reinstalled to test for another user & I end up getting the CD-key error too. Looking at an X7 Technical Suite/keygen version shortly to see if it works. In the process of downloading the 1.5gb "trial" version now.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 03, 2016 04:00*

Okay, got the X7 Tech Suite working with CorelLaser. Instructions here:



Okay, I've found another way to get it to work with Corel Draw Technical Suite x7 & a different keygen. Currently working.



Get the keygen here:

[http://free.appnee.com/corel-all-products-universal-keygen-by-x-force-for-win-32-64/](http://free.appnee.com/corel-all-products-universal-keygen-by-x-force-for-win-32-64/)



I used the first download link available (from zippyshare).



Then get Corel Draw Technical Suite x7 trial (1.5 gb) here:

[http://www.coreldraw.com/us/free-trials/?topNav=us#trial_cdtsx7](http://www.coreldraw.com/us/free-trials/?topNav=us#trial_cdtsx7)



I chose 64 bit version.



- Install Corel Draw x7, making sure to untick the option for "auto-update", open the keygen when asked for serial number.

- Generate Serial Number, keep the keygen open.

- Upon completion of install, open Corel Draw. It will prompt you for activation. Choose other activation, & phone Corel. Then type the Installation Code into the open keygen (in correct space, making sure to put the -'s).

- Generate Activation Code (make sure you don't generate serial again).



Should be activated then.



Then, you want to block it in Windows Firewall. I use Windows 10, so instructions may differ for your windows, but you want to go into Windows Firewall with Advanced Security.



- Go to Outbound Rules

- Create new rule, type program (you will do this twice).

- Point to the following 2 programs:



C:\Program Files\Corel\CorelDRAW Technical Suite X7\Programs64\CorelDRW.exe



& 



C:\Program Files\Corel\CorelDRAW Technical Suite X7\Programs64\DIM.exe



- Select Block from the list of options for what to do with network connections.

- Name the rules as something like Corel Draw Block & Corel DIM Block.

- Now it shouldn't connect to Corel to verify.


---
**joe ab (thedude)** *August 28, 2016 08:53*

Did the problem with the invalid serial for X5 get corrected? i am still unable to figure this one out.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 28, 2016 10:42*

**+jo ab** No, doesn't work anymore. Not sure why either. I switched to x7. See my post here in the comments before your comment.


---
**Adam J** *September 08, 2016 23:13*

Thanks for this Yuusuf!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 00:45*

**+Pikachupoo** I think this version may longer be hosted or working. I ended up having to switch to x7, although am no longer using that as I have upgraded to smoothie/laserweb.


---
**Adam J** *September 09, 2016 14:02*

Yeah, I'm using your link to x7 too. Had trouble with X5 crashing all the time, X7 is working fine for now, thanks!


---
**joe ab (thedude)** *September 10, 2016 03:21*

what is the best version or coral to use?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 10, 2016 06:32*

**+joe ab** Any version that works really. I used CorelDraw 12, x5 & x7 successfully. Others have used x8 also & I think some other variants. This version of x5 linked here doesn't work with the keygen anymore.


---
**joe ab (thedude)** *September 11, 2016 01:20*

what is the trick to activate X8? Keeps asking to create an account to activate.i did the same firewall permissions as X7.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 11, 2016 10:27*

**+joe ab** I never tried anything above x7, so can't say. Maybe someone else has tried & knows.


---
**Rodney Huckstadt** *September 12, 2016 00:27*

x8 works as well but I only had the trial version, and only for about a day before I went to x7 again


---
**joe ab (thedude)** *December 07, 2016 19:40*

**+Yuusuf Sallahuddin**



can someone host the X7 keygen??



for some reason i can not download it from the site.



Thanks!!


---
**Brandon Martabano** *December 12, 2016 03:10*

when I attempt to open the Keygen it flags as a potential trojan..any info on this?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 04:18*

**+Brandon Martabano** Yes, it flags it for everyone. I have no idea what is actually in the keygen, so use at your own risk. But from my experience, every keygen I've ever used (since prior to year 2000) has always flagged as a virus of some kind.


---
**Brandon Martabano** *December 12, 2016 04:19*

Ok thank you! Any one report any problems after using it? Im very hopeful this will be the resolution to my problem


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2016 05:33*

**+Brandon Martabano** Not that I'm aware of. I used it for a while & then switched up to corel x7, so I can't talk about long term usage, but for the period I used it I had no issues with it.


---
**Ted Schaeffer** *March 09, 2017 03:56*

**+Yuusuf Sallahuddin** The Dropbox link is not working have the files been deleted?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2017 04:20*

**+Ted Schaeffer** Yeah, Dropbox links are gone now. I have CorelDraw x7 hosted on OneDrive. 



You'll need:



CorelDrawTechnicalSuiteX7 file ([https://1drv.ms/u/s!AqT6a-6Vl15Nh7ltP5ugiVeTWu62Lg](https://1drv.ms/u/s!AqT6a-6Vl15Nh7ltP5ugiVeTWu62Lg))



Then activator:



AppNee.com.Corel.x5-x9.All.Products.Universal.Keygen.7z ([https://1drv.ms/u/s!AqT6a-6Vl15Nh7lryA4zpKIb9vWGBw](https://1drv.ms/u/s!AqT6a-6Vl15Nh7lryA4zpKIb9vWGBw))



Then you will also need CorelLaser 2013.02 (fifth in list):



[http://www.lcshenhuilaser.com/download.html](http://www.lcshenhuilaser.com/download.html)



This article provides details for how to activate etc: [http://free.appnee.com/corel-videostudio-pro-ultimate-x7-x8-universal-keygen-for-win/](http://free.appnee.com/corel-videostudio-pro-ultimate-x7-x8-universal-keygen-for-win/)



Hope that helps.


---
**Ted Schaeffer** *March 09, 2017 04:29*

Thank you


---
**Ted Schaeffer** *March 09, 2017 05:28*

**+Yuusuf Sallahuddin** is there a chance you have a copy of the original K40 Software CD.rar?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 09, 2017 08:09*

**+Ted Schaeffer** Indeed I do :) Fortunately I haven't deleted it. It's hosted in my Google Drive:



[drive.google.com - K40 Software CD.rar - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view?usp=sharing)


---
**freebird1963** *June 11, 2017 14:15*

no more corels ? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2017 15:07*

**+freebird1963** It should be still there. Someone else downloaded it from the last link in the comments here just a few days ago.


---
**freebird1963** *June 12, 2017 17:54*

YS says Link not found when I click on it. The K40 comes up but not the corel stuff. Thanks. Good life.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2017 12:59*

**+freebird1963** The dropbox link is definitely gone, but the Google drive link (3 comments above this) still opens for me.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT) &mdash; content and formatting may not be reliable*
