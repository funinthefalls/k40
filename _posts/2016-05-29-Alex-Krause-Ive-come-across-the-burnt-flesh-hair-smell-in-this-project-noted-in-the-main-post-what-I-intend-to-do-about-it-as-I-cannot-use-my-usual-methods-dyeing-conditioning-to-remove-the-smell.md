---
layout: post
title: "Alex Krause I've come across the burnt flesh/hair smell in this project & noted in the main post what I intend to do about it, as I cannot use my usual methods (dyeing/conditioning) to remove the smell"
date: May 29, 2016 11:10
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
**+Alex Krause** I've come across the burnt flesh/hair smell in this project & noted in the main post what I intend to do about it, as I cannot use my usual methods (dyeing/conditioning) to remove the smell. I'll keep you updated on any progress I have with removing the burnt smells.



<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



A test at a slim card wallet, made entirely with suede. Designed to fit credit cards & the largest "standard" business cards (i.e. took the largest international standard width, 91mm & combined with the largest international standard height, 57mm). So it should fit credit cards or business cards from anywhere in the world. It fits 8 credit/bank cards with reasonable ease. Possibly more could fit, but it would be tighter & potentially stretch the suede.



Laser engraved the beehive pattern & name. Smells extremely like burnt hair. As it is suede, I can't use my usual method of dyeing/conditioning to mask this smell, so I'm thinking maybe requires some time stored in a container with something that may either absorb that smell or permeate the suede with a nicer smell.



Pretty happy with the results of this test. Turns out the dimensions of my design were perfect.



![images/0667c5a396dcf18bd6de1f0adb7f5e39.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0667c5a396dcf18bd6de1f0adb7f5e39.jpeg)
![images/7914b88f39f84b9d2f79824afce45405.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7914b88f39f84b9d2f79824afce45405.jpeg)
![images/0d294e4e3ea575987b531e850fd206f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d294e4e3ea575987b531e850fd206f6.jpeg)
![images/83e72e9763ddc0a31304102af778f6a3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83e72e9763ddc0a31304102af778f6a3.jpeg)
![images/5b784df9241f556fcde4198320914e9d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b784df9241f556fcde4198320914e9d.jpeg)
![images/f6808bf516b574878e51c04e530a2d8d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6808bf516b574878e51c04e530a2d8d.jpeg)
![images/db6edca5580de1fc28b99e80c875982d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/db6edca5580de1fc28b99e80c875982d.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 29, 2016 11:27*

**+Susan Moroney** I'll give that a go & see how it responds. Thanks for the suggestion.


---
**Scott Marshall** *May 29, 2016 12:30*

I used to be very involved with the muzzleloading/re-enacting groups and camped traditionally. While that was long before home lasers, when we had a set of 'buckskins' (traditional leather clothing) that accumulated some unwanted "funk", we did like the American Indians did. We smoked them over the campfire. I've  owned a large propane smoker for the last 15 years or so, and just storing stuff in the well used smoker adds a pleasant 'woodsy' aroma to it. While it won't rid your leather goods of a smell, it will at least be a pleasant one.

(As Yuusuf said, at least it's a 'nicer' smell)



One other thought I had was maybe you could try putting the small items in activated charcoal in a plastic tote for a week or so.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 29, 2016 15:41*

**+Scott Marshall** Thanks Scott. Interesting thoughts. I've heard that a cut onion in the refrigerator will remove any odours in the fridge, but I imagine it would impart an onion-y aroma to the suede.


---
**COREY EIB** *June 01, 2016 06:13*

If you record screen sessions of designing this, please post the link. Looks great.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 01, 2016 12:39*

**+COREY EIB** Not sure what you mean by "screen sessions". I didn't actually record anything when designing it. Just the measurements & final engrave/cut files.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 04:19*

UPDATE: on the progress regarding the burnt hair smell.



**+Alex Krause** **+Susan Moroney** 



So what I've done so far is place the piece into a small plastic takeaway style container for 2 days, with lots of crushed Lemon Myrtle leaves (gives a nice lemony smell). This didn't seem to rid it of the burnt hair smell though. Although it smelled nicer, it still smelled of burnt hair.



So, then I've placed it in the same container, after removing the leaves, with some scrunched up newspaper as Susan suggested. It's been in there about a day with that now & I do notice the burnt smell is less than originally, although still partially there. I will leave it another few days & see how it goes.


---
**Alex Krause** *June 02, 2016 04:29*

Try a layer of baking soda in the bottom of the container with a layer of news paper ontop of it﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 05:04*

**+Alex Krause** Righto, will give that a go & see if it helps.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UJFDerpNoDk) &mdash; content and formatting may not be reliable*
