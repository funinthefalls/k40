---
layout: post
title: "Yay Laser arrived in one peace today!"
date: May 04, 2016 21:22
category: "Discussion"
author: "Pigeon FX"
---
Yay Laser arrived in one peace today!  3 quick questions....



1: Anyone know the ID/OD of the water tubing as the 2 feet it came with wont reach my bucket, so want to order some more and can find my calipers! 



2: Do i need to use a distilled water for cooling the laser, or is out the tap fine?



3: is there a chart of recommended speeds and powers for different materials floating about? 





**"Pigeon FX"**

---
---
**Joe Keneally** *May 04, 2016 21:55*

Use distilled water and devise a system with a lid to keep it clean.  It is also a good Idea to add a flow sensor switch in line with the "laser on" pushbutton so if the water flow stops the laser will not fire!


---
**Pigeon FX** *May 04, 2016 22:15*

**+Joe Keneally** Thanks Joe, will get some distilled water and look into flow switch once I have worked out the fitting sizes. 


---
**Alex Hodge** *May 04, 2016 22:16*

8mm ID tubing. 1/4" should work too.


---
**Pigeon FX** *May 04, 2016 22:45*

Thanks Alex. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 04, 2016 22:54*

#3, no chart that I am aware of, although if you check the Materials & Settings section of this group you will find tests people have done & uploaded images/settings used.


---
**Ariel Yahni (UniKpty)** *May 04, 2016 23:10*

Would you guys say this information is a good start?  At the end there is a table  [https://www.reddit.com/r/lasercutting/wiki/k40](https://www.reddit.com/r/lasercutting/wiki/k40)


---
**Jim Hatch** *May 04, 2016 23:19*

**+Ariel Yahni**​ the spoeds look too fast and power too low for the table at the end of that Reddit link. I'm usually in the 10-20mm/s speed range and 12-18ma power for most 1/8" cutting and half that speed and/or multiple passes for 1/4" wood stock.


---
**Jim Hatch** *May 04, 2016 23:21*

Here's a link to an Amazon flow switch. [https://smile.amazon.com/gp/aw/d/B00AKVEGTU/ref=ya_st_dp_summary](https://smile.amazon.com/gp/aw/d/B00AKVEGTU/ref=ya_st_dp_summary)


---
**Pigeon FX** *May 04, 2016 23:26*

**+Jim Hatch** thanks Jim, any recommendation on attachments/fitting as that looks like its 20mm, and plumbing is not my strong suit!  


---
**Joe Keneally** *May 04, 2016 23:37*

**+Jim Hatch** That is the one I have!!


---
**Jim Hatch** *May 04, 2016 23:39*

I just took it to Home Depot with a piece of the hose and made up an adapter from the fittings in the plumbing aisle. 


---
**Pigeon FX** *May 04, 2016 23:46*

**+Jim Hatch** unfortunately there are no hardware stores near me, so i have to order all that sort of jazz off the interwebs :(


---
**Jim Hatch** *May 05, 2016 00:02*

Ah. Let me see what the bits & pieces are. I'm traveling this week though so it'll be a few days. **+Joe Keneally**​ do you have a parts list for yours?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 05, 2016 00:27*

**+Ariel Yahni** Looks good as a basis, but as Jim mentions, those speeds/power settings seem a bit off compared to my own experience. I tend to use 10mA power & 25mm/s & 2 passes for 1/8" (3mm) plywood & it usually does a pretty good job of it. 500mm/min that they suggest is about 8-9mm/s, which seems overly slow & likely to cause excess charring on the edges. I prefer to do extra passes to minimise the charring.


---
**Joe Keneally** *May 05, 2016 00:56*

**+Jim Hatch** Unfortunately I too took my parts to a big box hardware store to test fit stuff. :-(


---
**Alex Krause** *May 05, 2016 02:47*

I need to order a switch I can get some info on this once the switch arrives 


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/SWPtQkh1urs) &mdash; content and formatting may not be reliable*
