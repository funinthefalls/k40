---
layout: post
title: "Hey guys , just got my mirrors all lined up fired up corel and typed Text as a test"
date: July 23, 2016 00:25
category: "Original software and hardware issues"
author: "David Spencer"
---
Hey guys , just got my mirrors all lined up fired up corel and typed Text as a test. clicked on the engrave button on the top, it is moving but very slowly is that normal?





**"David Spencer"**

---
---
**Ariel Yahni (UniKpty)** *July 23, 2016 00:35*

Make sure you selected the correct board type in corellaser properties. It should in M2﻿


---
**David Spencer** *July 23, 2016 00:41*

Thanks I just did that and tried again, seems to be going a lot faster now. But only a straight line


---
**Alex Krause** *July 23, 2016 00:46*

Input the serial number off the controller board in the Corel laser settings


---
**Jim Hatch** *July 23, 2016 00:46*

You also need to match the machine ID with the one on your controller board. It's 16 (I think) alphanumeric characters on a white label on the board.


---
**Ariel Yahni (UniKpty)** *July 23, 2016 00:47*

Unless you have something different from most of us, that should have fixed it.aybe try some of the other options 


---
**David Spencer** *July 23, 2016 00:50*

did all that, still just a straight line. how do i attach a screen shot


---
**Ariel Yahni (UniKpty)** *July 23, 2016 00:51*

Use [imgur.com](http://imgur.com) 


---
**Alex Krause** *July 23, 2016 00:52*

Does the letters show up in the preview window when you go to engrave or is it just a straight line?


---
**David Spencer** *July 23, 2016 01:02*

yes the image is in the preview window


---
**David Spencer** *July 23, 2016 01:06*

[http://imgur.com/bBP8Khg](http://imgur.com/bBP8Khg)


---
**David Spencer** *July 23, 2016 01:08*

[http://imgur.com/l0dsFYe](http://imgur.com/l0dsFYe)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 23, 2016 01:21*

You may have to convert the text to paths in order for it to engrave? I always convert all my text to paths (as I design in Adobe Illustrator) before working with the laser.


---
**Jim Hatch** *July 23, 2016 01:34*

Try inputting the same board & machine settings into LaserDRW. Then repeat in Corel.



If that doesn't work, do it again first by trying a Cut and then an Engrave. Someone here found that the order of the operation windows  (cut vs engrave) you entered the info mattered - cut first then engrave or engrave then cut I don't recall. You don't need to actually cut the thing if I remember right - just needs the software to write the data in some mysterious place.


---
**David Spencer** *July 23, 2016 01:49*

**+Yuusuf Sallahuddin** Good call it didnt like just text. I went into paint typed text saved it as a jpg imported it worked like a charm, thanks for everybodys help


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/ixpM3bSZ9DJ) &mdash; content and formatting may not be reliable*
