---
layout: post
title: "It took me almost a whole day, but after cutting some aluminum away, I think I enlarged the cutting area to achieve approximately 26cm in Y direction, also I squared the gantry"
date: October 09, 2016 20:33
category: "Original software and hardware issues"
author: "Dennis Luinstra"
---
It took me almost a whole day,  but after cutting some aluminum away,  I think I enlarged the cutting area to achieve approximately 26cm in Y direction,  also I squared the gantry. Lots of work! Now I need to tension the belts.



Anyone know what belt tension is best for the K40? Is there a movie available with a sound when pulling the belt sidewards and let it loose? 





**"Dennis Luinstra"**

---
---
**Scott Marshall** *October 10, 2016 23:00*

Snug, but not tight. you should be able to deflect the belt easily by hand, but have nearly zero play if you move the carriage. I just wiggle the carriage (power on software engaged so the motors don't turn) and there should be no perceptible movement with moderate force.



If it "twangs" it's way too tight. Cog belts just need the slack out of them to work well and don't depend on tension for positive drive. 



Hope that helps.



Happy Lasering...



Scott


---
**Dennis Luinstra** *October 11, 2016 21:41*

Thanks Scott! 


---
*Imported from [Google+](https://plus.google.com/101821475060654077048/posts/32SJpMqPtES) &mdash; content and formatting may not be reliable*
