---
layout: post
title: "Anyone ever replaced the mirror mounts for you first and secondary mirrors?"
date: May 02, 2017 08:09
category: "Modification"
author: "Steve Clark"
---
Anyone ever replaced the mirror mounts for you first and secondary mirrors? If so what replacement cells did you use? I find it ridiculous to continue using the ones provided. Yes they work after a way, but it doesn't take much for them to get knocked out of alignment .  Alignment would not be a big deal with a better designed holder.

I was down a lightobject a few weeks back and picked up a pair of 20mm to see if they could be adapted to the k40. I think can but it will require a little work.

They are way above the quality on the laser now using s ball bearing pivot point. Very solid and smooth with fine threads and locking knobs and screws. However, they are bigger and heavier.

There is room to lower the cell to beam center  by building new platforms.and creating a opening and cover to the left side so there is access to the mounts for your fingers. Weight/mass could be an issue but I think I can lighten them on the cnc mill



[http://www.lightobject.com/Pro-20mm-Reflection-Mirror-Mount-for-Co2-Laser-Machine-P589.aspx](http://www.lightobject.com/Pro-20mm-Reflection-Mirror-Mount-for-Co2-Laser-Machine-P589.aspx).  

 





**"Steve Clark"**

---
---
**Anthony Bolgar** *May 02, 2017 10:39*

Those mounts look really good. I am interested to see what you come up with, it has been on my "would be nice to do" list for a while now.


---
**Steven Kalmar** *May 02, 2017 11:44*

Can you post a pic with the new mounts next to the old?  I don't think a little extra weight will matter as that axis doesn't move as fast as the laser head does.  


---
**Adrian Godwin** *May 02, 2017 13:11*

They sound better than the K40 mounts - though those aren't the worst I've seen. But it's not just the adjustment mounts, it's that flimsy bracket that supports the 2nd mount. A milled aluminium one would be much less bouncy.


---
**Steve Clark** *May 03, 2017 00:53*

I agree the brackets are really bad. I'll have to make some on my mill. I weighed the LO mounts and they are at 142 grams without the mirror I'm guessing I can machine enough to get them down to under 100 grams plus getting rid of the steel brackets weight with an aluminum one. I have no idea how much the k40 mounts weight. I'm guessing around 70 or 80 grams with the mirror The steel bracket may weight near the mirror mount's weight.



Here is a picture comparing the size of the two. I've done some measuring and  I believe  will they fit without cutting an opening in the left side but it would be really hard to to get your fingers in there to adjust them so I'll be putting in an access panel.



![images/fda1e808446895669d7f3d4dee06f657.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fda1e808446895669d7f3d4dee06f657.jpeg)


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/H5cYfLh67kE) &mdash; content and formatting may not be reliable*
