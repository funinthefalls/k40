---
layout: post
title: "If you cut wood or Ply with your laser what is the best way of removing the black burnt stuff"
date: November 20, 2015 17:07
category: "Discussion"
author: "Tony Schelts"
---
If you cut wood or Ply with your laser what is the best way of removing the black burnt stuff.  Unless there is a way of not producing it.  I guess its unlikely as its very hot :) 

suggestions and tips please







**"Tony Schelts"**

---
---
**Bill Parker** *November 20, 2015 17:17*

The black on the face you can stop by putting masking tape over your cuts and the only way on the edges is very fine sand paper as far as I know.


---
**Nathaniel Swartz** *November 20, 2015 17:42*

Air assist can help reduce some of the char, but for clean up I like a damp rag, and then wet emery paper


---
**Tony Schelts** *November 20, 2015 18:36*

Thanks Guys


---
**Joey Fitzpatrick** *November 20, 2015 21:52*

You can buy plywood made especially for cutting with a laser.  It is called Laserply.  Laserply will cut much cleaner than plywood purchased from Home Depot or Loews.  The problem lies with the type of glue used to bond the wood layers together.


---
**Tony Schelts** *November 20, 2015 23:01*

Anyone got some good ideas for saleable projects bookmarks etc? I'm in the uk so no competition :)


---
**Stephane Buisson** *November 21, 2015 16:10*

Hi **+Tony Schelts** drop me a mail, for private exchange


---
**I Laser** *November 23, 2015 03:42*

Interested too Stephane, I've added you to circles. I'm in Australia so no competition with either of you :)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/3K3rhyEgmW2) &mdash; content and formatting may not be reliable*
