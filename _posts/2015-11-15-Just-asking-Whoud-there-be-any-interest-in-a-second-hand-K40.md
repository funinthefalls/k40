---
layout: post
title: "Just asking.. Whoud there be any interest in a second hand K40"
date: November 15, 2015 11:04
category: "Hardware and Laser settings"
author: "Imko Beckhoven van"
---
Just asking.. Whoud there be any interest in a second hand K40. Kitted out with dsp, adjustable bed, stepperdrivers ect..



Never got to it to finish fitting the replacement tube. Just cant find the time.. (Proud dad of one 3 year old and twins of one year... You get the picture)



![images/a148e70df2d03047d3e87f1918e0cba9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a148e70df2d03047d3e87f1918e0cba9.jpeg)



**"Imko Beckhoven van"**

---
---
**Mark Clausen** *November 15, 2015 13:04*

I'd be interested... I'm in the market for an hobbyist-grade machine and have been looking at these on eBay.  I'm new to google+, so don't know if there's a way to PM for more details.


---
**Imko Beckhoven van** *November 15, 2015 13:40*

**+Mark Clausen** this is what i did to the stock K40 [http://imkovb.blogspot.nl/2013/11/laser-overhauling.html](http://imkovb.blogspot.nl/2013/11/laser-overhauling.html) so to be faire its a hacked maschine. Let me know if this is what youre looking for.


---
**Mark Clausen** *November 15, 2015 14:09*

I assumed I'd need to perform upgrades to make it more usable. Judging from the apparent quality of your other builds, I'm fairly confident your mods were done well.   The downside is that I wouldn't have the fun of doing it myself... 😕.     Is there a way to discuss more offline?  Items such as cost, location (for shipping), approximate hours on the tube, etc.?


---
**Imko Beckhoven van** *November 15, 2015 18:50*

**+Mark Clausen** pm send


---
**Anthony Coafield** *November 15, 2015 21:26*

Just wanted to say that it does get better. I didn't have twins, but had 3 boys in 3 years, youngest is now a bit over 2 and I may soon have some semblance of a life outside the kids! 


---
**Jose A. S** *November 16, 2015 03:38*

Whats the price?


---
**Imko Beckhoven van** *November 16, 2015 14:12*

I am looking for 50% of the New value of the unit + upgrades. So around 500$ plus shipping 



Just so you know, the DSP upgrade alone (new) is more as 500$ But although the tube is new its not connected!



[http://www.lightobject.com/X7-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P942.aspx](http://www.lightobject.com/X7-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P942.aspx)

[http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx](http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx)


---
**Jose A. S** *November 16, 2015 21:16*

What is your current location?


---
**Imko Beckhoven van** *November 16, 2015 21:17*

The netherlands near eindhoven


---
**Jose A. S** *November 16, 2015 21:19*

That's a shame!...I am in the states :(


---
**Scott Howard** *November 23, 2015 05:08*

have you sold it?


---
**Imko Beckhoven van** *November 23, 2015 06:19*

**+Scott Howard** no 


---
**Scott Howard** *November 23, 2015 06:21*

How much would it be to send to usa?


---
**Imko Beckhoven van** *November 23, 2015 06:41*

**+Scott Howard** ill have to look into the exact number but is big and heavy so my guess is not so cheap.. Do you want me to figure out a more ecxact number?


---
**Jose A. S** *November 23, 2015 14:33*

I am interested to know the shipping to US. 


---
*Imported from [Google+](https://plus.google.com/+ImkoBeckhovenvan/posts/DBjoVn1oqfp) &mdash; content and formatting may not be reliable*
