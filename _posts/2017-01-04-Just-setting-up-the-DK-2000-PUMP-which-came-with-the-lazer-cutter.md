---
layout: post
title: "Just setting up the DK - 2000 PUMP which came with the lazer cutter"
date: January 04, 2017 19:18
category: "Discussion"
author: "Gail Lingard"
---
Just setting up the DK - 2000 PUMP which came with the lazer cutter.  I just want to make sure that I am setting this up properly so that there are no disasters. 



I read a blog which said to place silicon around the brass fitting as the contact is not great? Would you advise this?  



On a video on u-tube the water inlet is attached to the hose but with a different fitting. Have I used the right connector?  a The pump is then placed into a bucket of water, the water outlet hose is then placed into the bucket. 



New to all of this so any advice most welcome. 





**"Gail Lingard"**

---
---
**Nigel Conroy** *January 06, 2017 17:48*

This is the same way I set mine up and have had no issues.


---
**Gail Lingard** *January 06, 2017 17:52*

Great thank you. It seems to be working well.


---
*Imported from [Google+](https://plus.google.com/105833930009921938226/posts/SqJwYwxK7dc) &mdash; content and formatting may not be reliable*
