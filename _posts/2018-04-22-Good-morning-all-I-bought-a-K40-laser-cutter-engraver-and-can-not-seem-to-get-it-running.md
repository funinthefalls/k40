---
layout: post
title: "Good morning all I bought a K40 laser cutter/ engraver and can not seem to get it running"
date: April 22, 2018 06:47
category: "Discussion"
author: "Michelle Kung"
---
Good morning all I bought a K40 laser cutter/ engraver and can not seem to get it running. Laser appears to be working but when doing the test it's not burning onto the wood at all when doing the test. It appears when running the program that it's doing all the right actions just not the actual laser part. Been at this for near 18 hours and I'm like a bear with a sore head. Anyone able to help me?





**"Michelle Kung"**

---
---
**syknarf** *April 22, 2018 12:35*

Does the laser fire with the test button? If so, have you checked the mirrors are correctly aligned? 


---
**Kiah Connor** *April 26, 2018 07:55*

Does your cutter have a seperate switch for laser power? That caught me out initially too.


---
*Imported from [Google+](https://plus.google.com/115666697479946810476/posts/U1XQcBu18wx) &mdash; content and formatting may not be reliable*
