---
layout: post
title: "Hi All... My first post :) I'm from Polish and living in Netherlands"
date: March 07, 2016 14:17
category: "Discussion"
author: "MNO"
---
Hi All...

My first post :) 

I'm from Polish and living in Netherlands. So Europe location.

I'm thinking about buying k40 from link below.



1) What do You think? is it worth ?

2) will I be able to cut 8-10mm Acrylic (2-3 passes?) ? I plan to use mostly 2-3mm acrylic, and 5-8mm sometime. Don't have to be fast i just need clean (but not has to be perfect) cut.



 I know You can get it cheaper but i think it has few things that i would save me some time to mod... And TAX issue...

I'm electronic so no problem to go for ramps etc.  I'm also technical enough to do mods...

Is there any buying guide, what i should look for when buying?



Any advise is appreciated.



Marcin





**"MNO"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 07, 2016 14:34*

That price seems a little high in my opinion, for the advantages you are getting. There doesn't seem to be much better in this system than in the stock k40 that I purchased (except it has honeycomb bed & air assist default in the version you posted). The digital power adjust doesn't really make a great deal of difference, so I am pretty sure for the difference in price (I paid around $550 aud & yours is around $1100 aud; basically double) you could get all those same components (honeycomb/air-assist). Although I'm not certain how tax works for you in Netherlands, so it may work out that even if you bought a cheaper one (like the $550 aud I paid) it could cost you a lot in tax. Would really depend how your tax works as to whether it makes much difference. However, I suppose some of the other members of this group may know a bit more about Europe & be able to help you more than I can.


---
**MNO** *March 07, 2016 14:43*

and what about dual Y axis is it better?


---
**Stephane Buisson** *March 07, 2016 15:33*

try Ebay.DE or [Ebay.co.uk](http://Ebay.co.uk)

take the cheapest one then go for  a Smoothie board.

have a look to more powerful one if you really need it.

take care to bed size if bigger 50w come in 2 sizes.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 07, 2016 15:46*

**+MNO** I'd imagine that the dual Y axis provides for possibly more even movement across the Y direction, however at the same time, it adds more points to possibly fail. But, to be honest, I don't even recall if mine has 1 or 2 Y-axis rails (not without going & checking it thoroughly).


---
**MNO** *March 07, 2016 15:48*

The problem is this one is the cheapest, but i can live with that. I can buy it cheap but then driving around to get it paying tax. My time and nerves are more worth... 



"take care to bed size if bigger 50w come in 2 sizes." - what do You mean by that?

I was thinking k40 (3020) comes with one size only 30x20cm 



And what about acrylic 8mm will i be able to cut it with 40W tube?


---
**The Technology Channel** *March 07, 2016 15:52*

I can cut 15mm acrylic with the same machine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 07, 2016 15:54*

I think what Stephane would be meaning regarding the bed size, is if you are going for a larger 50w laser cutter, they come with 2 different sized cutting beds. So he was meaning to make sure you pay attention to the size of the cutting bed in the specifications before you purchase (if you buy a 50w laser).


---
**MNO** *March 07, 2016 15:55*

Great thank You Nick this information i really needed. Was there some mod necessary to do? How many passes You need to do? How does the edge look like?



Yuusuf i was looking on 4040 50W and 3040 50W but my space is really limited on that.

30x20 for now will be ok.


---
**The Technology Channel** *March 07, 2016 16:05*



Here's a video of it cutting 15mm acrylic with a single pass, at 0.5mm per second, not fast but it does it.


{% include youtubePlayer.html id="RUSFWWDaurU" %}
[https://www.youtube.com/watch?v=RUSFWWDaurU](https://www.youtube.com/watch?v=RUSFWWDaurU)

I cut it the first day I had it, so no mods, just good setup of mirrors and bed alignment.

Would recommend replacing mirrors and lens, cus they are a bit crap, but they do the job.

I have since upgraded my lens and mirrors and now getting much more power to the cutting surface, around 27watts, before only getting about 20watts.






---
**MNO** *March 07, 2016 16:11*

Nick - Perfect it looks good for my use. I will do only 8mm max. :)


---
**The Technology Channel** *March 07, 2016 16:14*

Just go slow and it will cut it fine, remember these are not 40watts at all closer to 30watts maybe, so for 8mm I would use about 2mm per second, should cut it fine.


---
**Stephane Buisson** *March 08, 2016 13:00*

**+MNO** bed size, It was just to let you know I saw 2 differrent 50W ebay offer at roughly the same price. one come with rotary, the other one with a bigger bed.



If you purchase from EU ebay, no tax surprise, (danger with ali express).



go for the cheapest, and save a bit of money for upgrade. for less 200 Euro on top you will be full options.


---
**MNO** *March 12, 2016 12:40*

ok link from aliexperess is dangerous !



i contacted seller he wants that i pay to his bank :( 

I newer buy like that without buyer protection


---
**MNO** *March 15, 2016 20:15*

Anyone knows any seller from Europe?

tax and harbor cost can double cost is not from EU


---
**Stephane Buisson** *March 15, 2016 23:26*

please read again, Ebay UK or Ebay germany

are the main door to EU. verify if the item is already in EU and you don't have taxe surprise.


---
**MNO** *March 17, 2016 23:54*

i did ... i did. went thru whole ebay, de, uk. Nothing there....


---
**Stephane Buisson** *March 18, 2016 08:42*

you are right, they seem to be sold out at the moment. wait few days for the market to refresh.


---
**MNO** *March 29, 2016 15:06*

OK finally got one :)

[http://www.ebay.de/itm/191687087676?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT&autorefresh=true](http://www.ebay.de/itm/191687087676?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT&autorefresh=true)



payd with paypal so should be secured from scam :) - ebay protection 

For security printed out whole ebay page, now i wait should be in a week it comes from Germany


---
**Stephane Buisson** *March 29, 2016 15:58*

well done


---
*Imported from [Google+](https://plus.google.com/111888830486900331563/posts/fjnxjyLJ8k3) &mdash; content and formatting may not be reliable*
