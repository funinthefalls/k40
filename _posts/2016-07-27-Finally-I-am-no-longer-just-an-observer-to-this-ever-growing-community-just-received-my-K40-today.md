---
layout: post
title: "Finally I am no longer just an observer to this ever growing community, just received my K40 today"
date: July 27, 2016 19:38
category: "Discussion"
author: "Paul Hayler"
---
Finally I am no longer just an observer to this ever growing community, just received my K40 today.  Still a lot of checking and tinkering to do.  I would be grateful if anyone has any suggestions and tips as to problems that I should be looking for.





**"Paul Hayler"**

---
---
**Anthony Santoro** *July 27, 2016 21:14*

Hi Paul, 

A good start is to check the mirror alignment. Then check the lens isn't upside down (hump facing up is the correct position). 

Make sure you check for bubbles in your tube too. 


---
**Paul Hayler** *July 28, 2016 00:17*

Cheers Anthony.  Hadn't even given it a thought about the orientation of the lens good to  know.


---
*Imported from [Google+](https://plus.google.com/101910035054606485391/posts/MCaeuVmLmBJ) &mdash; content and formatting may not be reliable*
