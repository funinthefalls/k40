---
layout: post
title: "I bought a K40 laser cutter and i got it firing without issues, now the software is a different story"
date: April 03, 2016 00:09
category: "Software"
author: "ben crawford"
---
I bought a K40 laser cutter and i got it firing without issues, now the software is a different story. The disk with moshidraw is too scratched and i cant get anything off of it. I also have a usb that says "lihuiyu studio labs" and a date that says "2016.02.15" 



Where do i get the software to make my laser cutter work?





**"ben crawford"**

---
---
**Scott Thorne** *April 03, 2016 12:22*

You are probably going to have to contact the seller or try downloading it off of the net.


---
**Vince Lee** *April 03, 2016 15:51*

You can download the software from [http://www.moshidraw.com/English/Support/Update/](http://www.moshidraw.com/English/Support/Update/) .  If you have the red dongle you want moshidraw2015x.  Once inside the app you can run their updater to upgrade to moshidraw2016x.


---
**ben crawford** *April 03, 2016 17:53*

I was able to get the disk to work after i used a disk cleaner, there is no moshidraw only CorelDraw, corelLaser and LaserDRW. Was the ad wrong and this printer uses a diffrent software or will moshidraw also work?


---
**Scott Thorne** *April 03, 2016 18:33*

Look at the control board and see what kind it is....if it says m2 nano...then laser draw and corel laser is what you need...if it's a moshI board then you have to have moshidraw


---
**ben crawford** *April 03, 2016 18:37*

it says M2 nano, so i guess i am need to use CorelDraw. It keeps crashing on me, is there any way to fix that?


---
**Scott Thorne** *April 03, 2016 19:47*

You need to be more specific.


---
**Scott Thorne** *April 03, 2016 19:48*

Corel laser and laser draw is what you need to load up. 


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/bWka82vwWVm) &mdash; content and formatting may not be reliable*
