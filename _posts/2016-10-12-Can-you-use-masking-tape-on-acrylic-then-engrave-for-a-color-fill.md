---
layout: post
title: "Can you use masking tape on acrylic then engrave for a color fill?"
date: October 12, 2016 01:30
category: "Discussion"
author: "3D Laser"
---
Can you use masking tape on acrylic then engrave for a color fill?





**"3D Laser"**

---
---
**Jim Hatch** *October 12, 2016 01:42*

Yes. But there's already masking paper on acrylic so you don't need masking tape. If you've taken that off then go with blue painters tape as it's easier to remove. Once you're done with the engrave use an in roller or similar and roller over the tape to press it down so anything that might have lifted on the edges of the engrave will press down again. That will keep the paint from bleeding.



Or, there are duo color acrylics available where the core is a different color than the surface. You etch the top away and expose the inner core & color.


---
**Alex Krause** *October 12, 2016 01:48*

**+Joe Spanier**​ just did a really awesome color fill trophy I believe he used blue painters tape as the mask


---
**Richard** *October 12, 2016 02:45*

I recommend the blue painters tape as it tends to leave less residue and come off cleaner (from personal experience) than regular masking tape. I personally find that the paper it comes with burns into the engrave, leaving it black, which is seen from the back side but not from the front after adding the paint, so it's up to you whether to just leave it on. Also, I recommend an airbrush (you can get a complete kit for like $10 and an air compressor designed for it for like $25 off Amazon... paints sold separately at your local hobby store). It'll give a more even and (more importantly) thinner coat than using a brush or roller.


---
**Joe Spanier** *October 12, 2016 02:56*

This is blue tape on white acrylic. I dont like leaving the plastic film on extruded acrylic. It tends to melt into hard to remove balls and the edges can be really hard to peel up. This burned off really cleanly and I have yet to have an issue with it on any material I've tried. 

![images/7bdc2298310f8448b8a37dbfa8471574.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7bdc2298310f8448b8a37dbfa8471574.jpeg)


---
**Joe Spanier** *October 12, 2016 02:59*

This is the finished product. Color filled with Testors enamel. I left the blue tape on for the fill process and then used 90% isopropyl to remove any excess or bleeding on the edges. 



Be diligent to remove any residue from the etching process. Take an an Exacto knife and clean around the edges and anything that builds up inside the etched area. It will leave some ugly edges if you dont. 

![images/d3a76bd4c2add524b33a76deddbadd2a.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/d3a76bd4c2add524b33a76deddbadd2a.gif)


---
**Jim Hatch** *October 12, 2016 11:58*

**+Joe Spanier**​ I don't use extruded acrylic. Prefer cast and I don't get the balling melting issue you're getting with extruded.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/dKbpZVRtPzw) &mdash; content and formatting may not be reliable*
