---
layout: post
title: "Can you cut 3/8\" ( edit correction, 1/2\") solid oak at 15mA in 3 passes at 4mm/s?"
date: September 15, 2018 16:30
category: "Discussion"
author: "Jamie Richards"
---
Can you cut 3/8" (<b>edit</b> correction, 1/2") solid oak at 15mA in 3 passes at 4mm/s?



Why, yes, yes you can if everything else in the laser is working correctly.  Oh, and if you're using the 38.1" Fl.  People thought 17mA was too high, so I tried it at 15mA, 10mA and 8ish-mA.  Guess what happened each time?  Sorry for the messiness in my workshop, I've been doing a lot of engraving and need to tidy things up and clean/perform maintenance on the machine.


**Video content missing for image https://lh3.googleusercontent.com/-eRpY5vKzy4Y/W50zq2KDHQI/AAAAAAAEF70/jFCwKSLR2ls3ane5HdWrDl4b-MP8xDGoACJoC/s0/20180915_092254.mp4**
![images/3d5b628a29da9df5a7b0e82601b8b263.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d5b628a29da9df5a7b0e82601b8b263.jpeg)



**"Jamie Richards"**

---
---
**Jamie Richards** *September 15, 2018 16:37*

At 10mA, I about pooped myself.

![images/b3a5cd24c5b3d9a240f6ed4a293e27a1](https://gitlab.com/funinthefalls/k40/raw/master/images/b3a5cd24c5b3d9a240f6ed4a293e27a1)


---
**Jamie Richards** *September 15, 2018 16:39*

Finally, with a little more effort, 8ish. 

![images/76170c46c6dceaa164ae6ad8d9d24d2b](https://gitlab.com/funinthefalls/k40/raw/master/images/76170c46c6dceaa164ae6ad8d9d24d2b)


---
**Don Kleinschnitz Jr.** *September 15, 2018 17:57*

Pretty impressive. For that thickness that's why I have a table saw lol. 


---
**James Rivera** *September 16, 2018 01:00*

Jeez, next up: 2x4...on the 4 side!  LOL!


---
**Jamie Richards** *September 16, 2018 09:11*

I'm one to try it, too, but I doubt it'll make it through.  I'm able to just barely get through the 2 side on a 1x2 after several passes.




---
**Jamie Richards** *September 16, 2018 09:14*

**+Don Kleinschnitz Jr.** 

+Don Kleinschnitz Jr. Hard to do stuff with a table saw you can do with the K40.  I'm going to start making good sized house markers etc. with the 1/2".  This example is 1/4" oak.

![images/708054878f965ad9c4d8ee94a52bacbc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/708054878f965ad9c4d8ee94a52bacbc.jpeg)


---
**Don Kleinschnitz Jr.** *September 16, 2018 12:00*

**+Jamie Richards** very nice. I guess I will have to have a contest between my K40 and Cnc mill :).

You probably already said in another post but where did you get your 38.1 mm lens.


---
**Jamie Richards** *September 16, 2018 12:12*

Amazon.  It was around $22.  I also bought Si mirrors.


---
**Jamie Richards** *September 16, 2018 12:14*

This was a few months back when I had a little extra change in my pocket. lol


---
**Jamie Richards** *September 16, 2018 12:15*

I so badly want a CNC!  Some day...  Sorry about the individual posts rather than one big reply.  I have a habit of doing that sometimes.


---
**Don Kleinschnitz Jr.** *September 16, 2018 12:26*

**+Jamie Richards** do you have a link I am having trouble finding it :)?


---
**Don Kleinschnitz Jr.** *September 16, 2018 12:27*

**+Don Kleinschnitz Jr.** no worries about individual posts they are cheap. I constantly wonder if I can get away with just a CNC, I use it more often cause it has a bigger area.


---
**Jamie Richards** *September 16, 2018 12:37*

I eventually want to build a new frame and expand the size of my gantry to something like 2'x2'.  I'd love to go for 3' on the x, but don't know if I'll have enough room on my work bench.  I can only buy a piece here and there and have to be frugal about it.  Took me awhile just to make my Z, but it cost me less than $60 to do it.  Drilled holes in the gantry and it's pretty solid.  Almost looks like one of the larger lasers that already have that type bed.  I need to fix the idler wheels, or rather how I am supporting them, the screw/bolts are bending from the belt tension.  A piece of metal and shorter bolt distance should do the trick.


---
**Jamie Richards** *September 16, 2018 12:37*

One moment, Don, I'll get it for you.


---
**Jamie Richards** *September 16, 2018 12:40*

[amazon.com - Robot Check](https://www.amazon.com/gp/product/B01DP2HLVK/ref=oh_aui_detailpage_o01_s01?ie=UTF8&psc=1)


---
**Jamie Richards** *September 16, 2018 12:41*

Robot check?




---
**Jamie Richards** *September 16, 2018 12:46*

forgot to mention, I'm using the LightObjects 18mm lens holder. 


---
**Don Kleinschnitz Jr.** *September 16, 2018 13:29*

Thanks I will give that lens a try. I also have the LO head.


---
**Jamie Richards** *September 16, 2018 14:05*

Just found the 12.



[amazon.com - Amazon.com: Cloudray CO2 Laser Focus Lens USA Dia.12mm FL 38.1mm for CO2 Laser Engraver Cutter](https://www.amazon.com/Cloudray-Dia-12mm-38-1mm-Engraver-Cutter/dp/B07B8QCYLD)


---
**James L Phillips** *September 16, 2018 15:46*

cool


---
**James L Phillips** *September 16, 2018 15:49*

[cloudraylaser.com - CVD ZnSe Meniscus Focus Lens for CO2 Laser](https://www.cloudraylaser.com/collections/co2-focusing-lens-1/products/usa-cvd-znse-focus-lens-dia-20mm-fl-50-8-63-5-101-6mm-1-5-5?variant=7645038936115)

$11.00


---
**James Rivera** *September 16, 2018 18:48*

**+Jamie Richards** Amazon links show up like that all the time; I’m not sure if it is google or amazon causing it.


---
**Jamie Richards** *September 16, 2018 21:56*

I'm getting ready to do a much larger monogram name plaque using 1/2" oak.  Oak engraves so nice and dark, I love it!  This piece dwarfs my other one!


---
**Jamie Richards** *September 16, 2018 22:30*

Going for round 2! Since it's half inch thick, I can go deeper and want the lettering and details to really stand out.  This is 15mA at 200mm/s with the 38.1.  that lens really likes to dig.  Water temp at 14.5c. Line interval is set to 0.050mm.![images/c258cc19dce595c05c14fb7ec13cedcf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c258cc19dce595c05c14fb7ec13cedcf.jpeg)


---
**Jamie Richards** *September 17, 2018 09:38*

It's finished, but it took several passes.  Cutting shapes is somewhat more difficult than cutting strait against the grain.  It was with the grain I'm having problems.  Left is 1/4" 15mA, 4mm/s one pass.  Right is 1/2", 15mA,  4mm/s several passes.  ![images/c1c790088337384f42eb356e9b2b38d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1c790088337384f42eb356e9b2b38d1.jpeg)


---
**Jamie Richards** *September 18, 2018 12:11*

I put the nozzle back on my LightObjects lens holder and I'm happy with it now because it's a little closer to the material and my oversized fish pump is working quite well with it.


---
**James Rivera** *September 18, 2018 22:35*

**+Jamie Richards** That’s really freaking good work, BTW.


---
**Jamie Richards** *September 19, 2018 00:53*

I did another test cut with the LO nozzle on and was able to cut a pattern in 1/2" solid oak, 15mA 3mm/s in 2 passes.  So the x problem was the line.  I'm impressed with how well it's working, no flaming!  When I cut the 1/2", I did reduce cutting distance significantly and that seemed to help a lot.![images/764ef6220601e863e197fa4e407966ef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/764ef6220601e863e197fa4e407966ef.jpeg)


---
**salvatore sasakingsoft** *October 03, 2018 20:08*

ciao mi sai dire che cinghia monta la laser k40 ?




---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/HRhGRRPUJus) &mdash; content and formatting may not be reliable*
