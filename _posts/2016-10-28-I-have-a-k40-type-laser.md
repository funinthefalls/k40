---
layout: post
title: "I have a k40 type laser ."
date: October 28, 2016 08:11
category: "Original software and hardware issues"
author: "Don Recardo"
---
I have a k40 type laser . Does the driver board support a 4th axis so i can rotate and engrave round objects 





**"Don Recardo"**

---
---
**Scott Marshall** *October 28, 2016 08:19*

You just use the Y axis and plug it into the roller motor, If the ratios are designed in correctly, that's all that's needed, if they are off you will have to change the stepping ratio (steps /mm)


---
**Don Recardo** *October 28, 2016 08:26*

I feel a bit silly for asking now. As your answer makes perfect sense . 


---
**Scott Marshall** *October 28, 2016 08:54*

Not silly at all.



 It is the logical conclusion until you get into the details.

As with many thinks in life, it's only obvious once you know the trick.



That makes it fairly easy to build a quick homemade roller. All you really need is a spare Y axis motor and some roller/s wheels of the right diameter.  I ground out the math for a fella a while back and he built one in an afternoon. I forget the numbers right now, but they ended up a practical diameter.

If you get there and need help, let me know. 



The main problem is you run out of room real fast in the K40. While you could easily engrave coffee can sized items, there's no room for the rest of the object. 



I solved that problem by cutting the bottom out of my K40 with a body slitter, and had it mounted on a 20 gallon long aquarium stand for a while, which is good for putting large objects in (plus I had one I 'inherited' and it fit quite well, a tad small, but usable. I now have that machine on a folding table and use it for testing, but I really would like to get it set up for engraving large objects.



 I think there's a good market for Glass and wood kitchen canisters, Cat food containers with cute kittys etc would probably sell like crazy on ebay or at craft fairs.



Coffee mugs and beer steins would be possible with just a bit more room, and both would be popular sellers (I think)

With the K40 intact, I think you're pretty limited as to diameter on this sort of stuff. 



 


---
**Don Recardo** *October 28, 2016 09:06*

Thanks for the info . Making the rollers is not a problem for ne as my hobby is model engineering and i have a lathe and milling machine so can build the hardware. I take your point about not having enough room in the case. My plan is to eventually make a laser that will cut objects up to 36" x12" so of course a bigger machine will be built but i wanted to incorporate the abilty to do rotary engraving and thought i might need new electronics but now you pointed out that rotary is so simple I could use my exsiting electronics from the K40. 


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/C5pPo5HsLQS) &mdash; content and formatting may not be reliable*
