---
layout: post
title: "Yuusuf Sallahuddin I wonder if you could help me?"
date: July 14, 2016 19:40
category: "Discussion"
author: "Tony Sobczak"
---
**+Yuusuf Sallahuddin**  I wonder if you could help me?  I'm including a link to a cdr file.  If you try to cut the pieces there are additional straight lines that end up getting cut.  If I save it as an ai file and open it in AI I can't get it to get rid of the extra lines.  I tried outlining the stroke, but that ends up double cutting in Coreldraw.



You explained something similar in a post once before (which I can't seem to find now.)  Could you pleaase help me out with this?



TIA,



Tony







[https://dl.dropboxusercontent.com/u/101980659/From%20SVG.cdr](https://dl.dropboxusercontent.com/u/101980659/From%20SVG.cdr)

﻿





**"Tony Sobczak"**

---
---
**Jim Hatch** *July 14, 2016 20:01*

I'm not Yuusuf & I'm on a plane so can't look at the file but since he's half a world away it might be a bit before he's on. In the meantime, have you made sure all the lines are thin? Selecting them all and changing the stroke weight to .001mm (that's what I use, I think .01mm works too).



Thicker strokes get treated as multiple lines.


---
**Tony Sobczak** *July 14, 2016 20:17*

Yes they are all .001.  




---
**Imnama** *July 14, 2016 21:15*

Do you mean those 4 horizontal and 1 vertical blue line? Theese are helplines. If you disable the view and print icon for help lines on your mainpage object view they should be gone.


---
**Tony Sobczak** *July 15, 2016 03:00*

No not the guides. You have to load them with corellaser to see the additional lines.  They will cut and engrave. ﻿


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 07:22*

Hey Tony. I've just come online so I'll take a look now for you.


---
**Norman Kirby** *July 15, 2016 07:23*

the lies are the same as I am being plagued with, you need to close the loop, and to be able to do this you need to use "Join Curves" but when you us that it joins lines that are not supposed to join and spoils the image, I know I;ve not explained it well but I have posted earlier 2 images of the same problem






---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 07:27*

**+Tony Sobczak** What version of Corel Draw is this? I can't seem to get the file to do anything. When I import it says it can't be imported & when I open it is an empty/blank screen. I'm using CorelDraw x5. I tried opening in Adobe Illustrator also, but it says it doesn't know that file format. Did anyone else manage to open the file?


---
**Jim Bennell (PizzaDeluxe)** *July 15, 2016 07:58*

I think Normal was correct for this problem. I think it will work in this situation but a lil more probably needs to be done to make it cut in the correct order.



I opened the file in x7, I started just randomly picking group12 to edit. I expanded the group and selected all the curves inside. Then press F10 for the shape tool(second option on the left pallet). Now drag over all the objects in that group(select all the points) and go to the top Object menu and choose join curves. Put in 0.1mm as the gap tolerance. This should only join vectors that have open ends within that range. Now your left with the joined objects (12).



Now I'm guessing you want to cut out some of these shapes , and if you where to cut out one of the outer objects before one of the inner objects and there was nothing under it to support it , it would fall and not be able to cut the inner objects. Even with a honey comb table some times things can shift especially with air assist. So I suggest creating multiple layers for the order in which you want things engraved/cut.



I usually start by separating all my objects into layers for engraving/cut1/cut2 etc. I will also make all the objects/curves have no line weight or fill color that I don't want to be engraved or cut. This makes it so you don't have to add in any registration marks to line things up correctly. All you have to do is make sure your cut layers are not visible by using the fill or line weight, turning the layer off with the eye ball doesn't work it will still be engraved or cut. So which ever layer I'm working on will be the only one visible with lines and fill. It's all about workflow!



Also when cutting a vector line I usually will make it filled black with no line. But that's only for closed objects like cutting out a shape. If you just need to cut a line use hairline it should be the absolute smallest the program understands. I'd imagine using .001 would be the same but its probably faster to just choose hairline instead of typing out .001



Good luck! Hope you get it sussed out.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 08:12*

**+Jim Bennell** Hairline doesn't work for lines for most people as it will do the double cutting (once on each side of the line). 0.01 will stop it doing the double cut.


---
**Tony Sobczak** *July 15, 2016 16:15*

That's not the issue.  The issue is the extra lines when you try to cut the pieces.  **+Yuusuf Sallahuddin** helped me out before in using AI to get rid of them.  Like a dummy I never wrote them down and can't find the post from him again.


---
**Tony Sobczak** *July 15, 2016 16:18*

Here's the file in V15 Coreldraw 5.



[https://dl.dropboxusercontent.com/u/101980659/From%20SVG.cdr](https://dl.dropboxusercontent.com/u/101980659/From%20SVG.cdr)


---
**Tony Sobczak** *July 15, 2016 16:28*

**+Jim Bennell** That worked.  I had tried join curves before but gap tolerance was set to 2.54.  Thanks.



**+Yuusuf Sallahuddin**  If you could refresh my memory on doing the same thing in AI, I'd appreciate it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 17:05*

Hey **+Tony Sobczak**. I'll take a look at the different file version & see if it works for me this time. If I recall correct I made a small youtube video on how to fix unjoined paths in AI (yep, found it: 
{% include youtubePlayer.html id="wJMwi_n1MNc" %}
[https://www.youtube.com/watch?v=wJMwi_n1MNc](https://www.youtube.com/watch?v=wJMwi_n1MNc))


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 15, 2016 17:32*

**+Tony Sobczak** I've just taken a look at the new file & can't open in AI, so I saved from CorelDraw to AI CS5 format then opened in AI. Everything looks perfect when I originally looked at it, but then when I changed the line weight to 1mm, I can see that none of the paths are joined at all.



Here is an Adobe Illustrator file that shows what is going on. I made some notes in the file on how to fix it. Ctrl + J (join) is what you're looking for though, after you select two paths end-points that are in the same location.



[https://drive.google.com/file/d/0Bzi2h1k_udXwQ0xKeUJIbWtSV2s/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQ0xKeUJIbWtSV2s/view?usp=sharing)


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/JMheU3ZwG9z) &mdash; content and formatting may not be reliable*
