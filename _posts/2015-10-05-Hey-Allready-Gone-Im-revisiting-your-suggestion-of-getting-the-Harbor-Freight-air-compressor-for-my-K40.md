---
layout: post
title: "Hey Allready Gone , I'm revisiting your suggestion of getting the Harbor Freight air compressor for my K40"
date: October 05, 2015 20:34
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Hey **+Allready Gone**, I'm revisiting your suggestion of getting the Harbor Freight air compressor for my K40. In your previous comment you mentioned that the hose that comes with the compressor fits the air assist nozzle. My question to you is, looking at the picture for the compressor, it looks like the hose has a threaded end. How did you attach this to the nozzle, or did you cut it of? Or did you get a coiled hose for inside the printer itself like I see some of them having? I'm thinking I will likely do the internal part with a coiled hose, connected to a quick disconnect on the back of the machine where I will attach the compressor hose to. Suggestions? Ideas?





**"Ashley M. Kirchner [Norym]"**

---


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/gnE2Z7qcWp4) &mdash; content and formatting may not be reliable*
