---
layout: post
title: "Don Kleinschnitz could I pick your brains please regarding my Tube"
date: December 17, 2017 13:18
category: "Original software and hardware issues"
author: "Andy Shilling"
---
**+Don Kleinschnitz**  could I pick your brains please regarding my Tube.







**"Andy Shilling"**

---
---
**Don Kleinschnitz Jr.** *December 17, 2017 14:46*

Of course but realize that my brain is "slim" pickings :).


---
**Andy Shilling** *December 17, 2017 15:05*

lol. Says a lot about me if I'm asking you then.;)



I was using my laser a fair bit yesterday and today I have woken up to it only firing when the pot is set to about 3/4 ma, anything over that it will not fire. Do you think this will be a tube problem or psu?



I have checked all wiring connections and the pot itself so I'm guessing the fact that it is firing at all means the psu should be fine.  Although I say 3/4ma the amount of power coming out only just marks a piece of card so I'm not sure that is even correct. Any thoughts as your the go to man on these things.


---
**Don Kleinschnitz Jr.** *December 17, 2017 20:06*

**+Andy Shilling** are you saying that it will not fire if the current is >.75ma or are you saying 3/4 position of the pot?


---
**Andy Shilling** *December 17, 2017 20:10*

**+Don Kleinschnitz** Sorry my wording sucks.If fires if my digital meter shows 1.0 or below which means the ammeter shows around 3 to 4 amps. Anything over this it just doesn't fire.




---
**Joe Alexander** *December 17, 2017 20:48*

did you check the output from the pot to the laser psu to make sure it isnt a bad pot? since its only a 0-5v signal its a safe test.


---
**Andy Shilling** *December 17, 2017 21:04*

**+Joe Alexander** I have tested the pot with a multimetre and that seems to working as it should, I replaced a bad one a few months ago. Is there a way to bypass the pot just to be sure?




---
**Joe Alexander** *December 17, 2017 21:05*

yea if you jumper the 5v to in it should fire at full power, but that wont help diagnose why it doesnt fire a low power.


---
**Andy Shilling** *December 17, 2017 21:36*

**+Joe Alexander** Ive just tested it and it makes no difference, I have checked the ohms and the voltage coming out of the pot and they both increase as I turn it up. The laser fires when the digital meter shows below 1v but anything over that it does not. Connecting a 5v supply direct to the psu to trigger firing also does not produce anything.


---
**Don Kleinschnitz Jr.** *December 18, 2017 03:02*

**+Andy Shilling** where did you connect the 5v?


---
**Andy Shilling** *December 18, 2017 06:14*

**+Don Kleinschnitz** to the PSU where the pot signal wire goes to.


---
**Don Kleinschnitz Jr.** *December 18, 2017 12:05*

To summarize, we are saying that the laser fires at low power settings (as indicated by a DVM connected on the pot)  but not at higher power settings? 

Guess its a bad tube or supply? I would guess the supply because I have never seen a tube to only fire at low voltage. Then again ....



Are you using the test button down on the LPS to fire? 

if not turn the pot up full and try the test button down on the supply to see if it behaves differently.



You can try and create an arc to test the supply. Do you want to do that?






---
**Andy Shilling** *December 18, 2017 12:15*

**+Don Kleinschnitz** I'm happy to try the arc if you could explain the safest way to do so. 

 I have ordered a new tube, should be here Wednesday. I went for the tube as this would be the quickest post to arrive here in the uk.


---
**Don Kleinschnitz Jr.** *December 18, 2017 12:47*

#K40LPS testing



Ok. <b>You do this at your own risk.</b>



..Turn off power and unplug the AC cord, let is sit for 30 min.

..Access the laser compartment

..Make and use a "chicken stick" to discharge the anode of the tube

..Pull back the silicon or get access to the anode pin.

..Tape a wire securely, with its end bared, near the anode pin (within 1"). I use black electrical tape wound around the tube to tape the wire down.

..Leave the laser compartment open with interlocks defeated if you have them.

..Now is a good time to set up the video camera well away from the laser compartment but with a good view of the bare wire.

..Plug in the AC and power up the machine

..At the front panel, 

.....Turn the pot full up

.....Enable the laser

.....Start the video

..Then pulse the laser ON for the shortest period possible

..There should be a violent arc.



Post video...




---
**Andy Shilling** *December 18, 2017 12:56*

Ok that's the job for tonight, of you don't hear back from me in either dead or sitting twitching in the corner. Thanks Don I'll report back tonight.


---
**Don Kleinschnitz Jr.** *December 18, 2017 13:12*

**+Andy Shilling** btw while your set up to arc you may want to try at various pot settings.


---
**Andy Shilling** *December 18, 2017 13:20*

**+Don Kleinschnitz** I'm guessing that I (should) get different strength arcs doing that?

If it is the psu would you have an idea on what part failed and would I be better off getting a new one rather than trying to repair it. Also I am thinking if I need to replace it is it worth going to a 60w and nodding the case for a bigger tube?






---
**Don Kleinschnitz Jr.** *December 18, 2017 13:38*

**+Andy Shilling** 

My hypothesis is that the HVT is arcing internally when the power is turned up.

Meaning the HVT is bad. 

You can replace the HVT for $25-30 or a new supply is $70-100. 



If you replace the HVT and that is the problem then that is the cheapest route.

If there is something else wrong with the supply or the tube is bad you will still have to buy a new supply or tube. So its a risk vs budget question. 



In regards to 60W conversion: my plan is to run my tube at its highest power until EOL (mostly cutting). Then replace it and the supply with a 60W configuration. 



If your tube has life left then maybe investing in a HVT will get some more use of the machine. 

If the tube is dead you could go right to the 60W config.

You could also go to a 60W LPS and then change tubes when the 40W is dead.



I have never proven or seen it proven that a 60W LPS  will not damage a 40W tube. However I know that its been done by some in this community. 


---
**Andy Shilling** *December 18, 2017 13:57*

**+Don Kleinschnitz** I will see what happens tonight and assess from there but I think a 60w will be the way to go this year.


---
**Andy Shilling** *December 18, 2017 19:04*

**+Don Kleinschnitz** I just run the arc test and this time anything over 0.7 on the DVM doesn't create a spark so I'm guessing its the HVT that is at fault.



So the question now is do I buy the 60w psu and run it very low for the duration of the tube I have here or do I swap out the HVT? either way they won't arrive this side of the new year now.






---
**Don Kleinschnitz Jr.** *December 18, 2017 22:05*

**+Andy Shilling** ill let you make that call.

i would poll the group and fi d someone runing 60w supply on 40w tube. 


---
**Andy Shilling** *December 18, 2017 23:22*

**+Don Kleinschnitz** thank you for all your help again kind sir, I will let you know what I decide to do. 

This is what I'm thinking of going with but I want to work out the wiring before I commit to the purchase.



Do you have any other options you are looking at fur the 60w upgrade?



Cloudray 60W CO2 Laser Power Supply for CO2 Laser Engraving Cutting Machine HY-T60

 [aliexpress.com - Cloudray 60W CO2 Laser Power Supply for CO2 Laser Engraving Cutting Machine HY-T60](http://s.aliexpress.com/EjaeeyY3?fromSns=Copy) to clipboard 

(from AliExpress Android)


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/HkjnA7APD3B) &mdash; content and formatting may not be reliable*
