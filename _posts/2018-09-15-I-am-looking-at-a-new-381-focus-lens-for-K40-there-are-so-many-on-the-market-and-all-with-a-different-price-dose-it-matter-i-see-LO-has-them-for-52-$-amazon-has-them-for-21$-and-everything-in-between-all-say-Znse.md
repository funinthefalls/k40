---
layout: post
title: "I am looking at a new 38.1 focus lens for K40 there are so many on the market and all with a different price dose it matter i see LO has them for 52 $ amazon has them for 21$ and everything in between all say Znse"
date: September 15, 2018 18:04
category: "Discussion"
author: "Dennis Fuente"
---
I am looking at a new 38.1 focus lens for K40 there are so many on the market and all with a different price dose it matter i see LO has them for 52 $ amazon has them for 21$ and everything in between all say Znse 





**"Dennis Fuente"**

---
---
**James Rivera** *September 16, 2018 00:54*

Caveat emptor.  EDIT: Sorry, that was terse. I bought a lens (50.8mm?) from LightObject and it improved my cutting performance <i>significantly</i> over the lousy stock K40 lens. That being said, I don't want to disparage the cheaper sources. But do your homework; read the reviews, and check their return policy; educate yourself on exactly what it is you are expecting to get, and be prepared to return it if it does not meet your expectations. I have found that a bargain is not a bargain if you have to buy something else to replace it. Good luck.


---
**Jamie Richards** *September 16, 2018 14:10*

I have the 18mm $21 version, but they also have one for the 12mm head, same company.



[amazon.com - Amazon.com: Cloudray CO2 Laser Focus Lens USA Dia.12mm FL 38.1mm for CO2 Laser Engraver Cutter](https://www.amazon.com/Cloudray-Dia-12mm-38-1mm-Engraver-Cutter/dp/B07B8QCYLD)




---
**Dennis Fuente** *September 16, 2018 14:33*

**+Jamie Richards** that's one of the one's i was looking at and amazon takes everything back there are a couple of lens on amazon so the one you have any good ?


---
**Jamie Richards** *September 16, 2018 16:16*

It appears to be very good.  Had it since April.  Depending on type of air assist you have, you may need to clean it more often.


---
**Dennis Fuente** *September 16, 2018 17:04*

**+Jamie Richards**  i have the LO air assist 18 MM lens




---
**Jamie Richards** *September 16, 2018 17:05*

Cool.  


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/6EYGiov63Re) &mdash; content and formatting may not be reliable*
