---
layout: post
title: "I give up. After week of fighting with my MKS-SBASE, PWM still not working"
date: September 07, 2016 05:25
category: "Smoothieboard Modification"
author: "Damian Trejtowicz"
---
I give up.

After week of fighting with my MKS-SBASE, PWM still not working.

All problems start after replacing my burned PSU.

All is connected like was and I not able to set up right my power level.

When i connect pot to do analog controll all work fine.

When i use mksbase to drive power level nothing work fine

I can only setup beetwen 35 and 60% ,in other case i have no power or have 30mA

I used few diffrent level shifters and no progress

To drive laser power i use pin 1.23

Laser on pin is to ground(was trying separate but this time is not working at all pin 0.26)

I was trying few diffrent firmwares(now use one from april 2016,its the latest what working for me on this board).

Lots of changes in configuration and still in the same point

Im start thinking about back to ramps or setup controll on grbl because i have all this boards





**"Damian Trejtowicz"**

---
---
**Alex Krause** *September 07, 2016 05:37*

Should have bought a genuine smoothie board from [robotseed.com - 5XC Smoothieboard 5XC Smoothieboard](http://www.robotseed.com)


---
**Damian Trejtowicz** *September 07, 2016 05:45*

**+Alex Krause**  I know this.

Just wondering what i wrong now,was working before and stop now


---
**Alex Krause** *September 07, 2016 05:48*

My guess is a substandard chip on the board failed


---
**Alex Krause** *September 07, 2016 05:49*

When the psu failed could have taken out components on the board


---
**Damian Trejtowicz** *September 07, 2016 06:08*

So temporary i will back to ramps or grbl.what is better choice?


---
**Mircea Russu** *September 07, 2016 06:49*

Make sure you use the latest master firmware. Connect one of the green connector (mosfet) "-" pin to the L in the rightmost connector at LPS. Leave the pot untouched. Use the corresponding pin (2.4-2.7) for the mosfet used in config "laser_module_pwm_pin 2.X" and make sure you remove the green LED next to the used mosfet.


---
**Damian Trejtowicz** *September 07, 2016 06:54*

I done all this and not working at all.i use latest working firmware on mks-sbase(when i try use latest from github its not flash it)


---
**Mircea Russu** *September 07, 2016 07:01*

**+Damian Trejtowicz** There you go:

![images/7128ba05648365227ee396492bb8ac4e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7128ba05648365227ee396492bb8ac4e.jpeg)


---
**Mircea Russu** *September 07, 2016 07:03*

You don't need any level shifter. You can use a dual pin setup and connect to the LPS IN pin. But you gain nothing more. KISS (keep it simple s.....)


---
**Damian Trejtowicz** *September 07, 2016 07:04*

Looks like its completly diffrent setup like i have .i use pwn pin on middle plug of psu.i need try your setup today.

Do you have any schematics how You connect all,please. Because now i see its lots of diffrent connections over here


---
**Mircea Russu** *September 07, 2016 07:06*

[github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/master/FirmwareBin/firmware.bin)


---
**Damian Trejtowicz** *September 07, 2016 07:09*

I will try this firmware after work.looks like i need rewire all 


---
**Mircea Russu** *September 07, 2016 07:09*

See the posted schematics. I guess you have the steppers and end stops all sorted out. Besides the +24V and GND the only connection between MKS and LPS is the wire drawn in red going from the MOSFET 2.4 ("-") to the rightmost L on the LPS. Make sure the "WP" loop is ok if you have added safety. Make sure the pot is in place as originally wired by manufacturer. Make sure the LED on the MKS is removed or you will put 23V at the L pin an possibly damage the LPS.


---
**Mircea Russu** *September 07, 2016 10:09*

**+Peter van der Walt** master is tested and works. For starters he should stick to something that works. After ironing out quirks with wiring and config parameters he can go (bleeding) edge. 


---
**Damian Trejtowicz** *September 07, 2016 13:06*

I wil try latest edge bin but i doubt im able to flash it.

Dont forget I have shi**y mks -sbase and when i try flash bins its freeze

I know i neet get genuine smoothie but wife is not happy to hear about spending another almost 200 euro for machine what is not working at all




---
**Alex Krause** *September 07, 2016 14:41*

[https://plus.google.com/109807573626040317972/posts/2HEYSVnnKHx](https://plus.google.com/109807573626040317972/posts/2HEYSVnnKHx) another side by side comparison of edge vs master firmware


---
**Mircea Russu** *September 07, 2016 16:06*

**+Damian Trejtowicz**​ try another sd card, higher probability the card is bad or not ejected properly than bad board. 


---
**Mircea Russu** *September 07, 2016 16:06*

**+Peter van der Walt**​ no need to convince me, I'm already edge now, just didn't know it's that stable :)


---
**Robi Akerley-McKee** *September 08, 2016 00:58*

Better off getting a Smoothieboard.  2.4 never worked for me.  the Mosfet 2.5 did. it switch the L pin to ground and made the laser fire. PWM worked off pin 1.23 by using a 3.3 to 5v level conveters. Polulu has one. If you don't use the level converter you only get to 65% power aka 3.3v out of 5v

I go to 4v or 80% which gives me 15mA on the amp gauge


---
**Damian Trejtowicz** *September 10, 2016 09:05*

**+Mircea Russu**  , i know im pest now, but how your config file looks like?i still need switch module?


---
**Mircea Russu** *September 10, 2016 09:23*

**+Damian Trejtowicz**​ take a look [github.com - Smoothieware-K40-config](https://github.com/executivul/Smoothieware-K40-config/blob/master/config.txt)


---
**Damian Trejtowicz** *September 10, 2016 09:26*

**+Mircea Russu** now its not clear enoug for me, why there is two pins? 2.4 and ttl 2.6, i should connect this 2.6 to anything?




---
**Mircea Russu** *September 10, 2016 09:27*

I use the 2 pin setup. If using the diagram I've put earlier just use the pwm line with no "!" After prin number, no need to use the ttl pin.


---
**Mircea Russu** *September 10, 2016 09:32*

You can ue my config but replace the tll and pwm lines with "laser_module_pwm_pin 2.4". That's all. 


---
**Damian Trejtowicz** *September 10, 2016 09:48*

i give up. 

i done all like you show me **+Mircea Russu** and all what i get i laser constantly on an no power controll.

maybe i should try this second diagram with two pins




---
**Mircea Russu** *September 10, 2016 09:58*

Using my config from github? Please paste only the laser_... lines. Did you connect like my schematic? Any other changes to the lps wiring?


---
**Damian Trejtowicz** *September 10, 2016 10:16*

all done like you showed. and when i turn laser on, i my laser is shotting


---
**Damian Trejtowicz** *September 10, 2016 10:23*

:)))) looks like i get to work now,  in my case should be 2.4 not 2.4! like in your config


---
**Mircea Russu** *September 10, 2016 10:44*

Please read my posts again. I've said at least 3-4 times "no !" :) Glad you made it!


---
**Damian Trejtowicz** *September 10, 2016 16:17*

I dont know why but i missed this.Now all works fine without level shifter.just now i need to learn how to use laserweb.how to cut and engrave in one program.

I can only do this in visicut,thru inkscape


---
**Damian Trejtowicz** *September 10, 2016 17:07*

:) so im not dumb as i look ,i figured out this without any help.

**+Peter van der Walt** ,if you still need any laser service i have new company,we offer laser cut,bending and all services [mtlengineering.co.uk - CNC Sheet Metal Machine Repairs Engineers &#x7c; Amada Repair Engineers](http://www.mtlengineering.co.uk/)

We started with new laser cutting departament and sales of lasers




---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/jEDhcFe8svv) &mdash; content and formatting may not be reliable*
