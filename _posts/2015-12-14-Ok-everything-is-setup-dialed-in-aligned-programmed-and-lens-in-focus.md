---
layout: post
title: "Ok everything is setup dialed in aligned programmed and lens in focus"
date: December 14, 2015 03:06
category: "Modification"
author: "Andrew ONeal (Andy-drew)"
---
Ok everything is setup dialed in aligned programmed and lens in focus. everything is fully functional and operational including homing and auto home. I prepared a image in Inkscape to engrave in either wood or acrylic I have moved object to path in Inkscape and for now I am directly connected via USB to laser. I've been working on this for 2 months replacing the electronics and fixing issues found with gantry. This is not my first love mostly I deal with the RC side of things helis planes and multicopters. This was far more of an undertaking than I had imagined mainly due to the program inside via Arduino. I want to finally put this to work and start cutting and engraving pieces however I can't find exact steps to get me from Inkscape to laser. I've read countless forums and blogs in there so many different ways people are doing this. I'm hoping that one of you can you give me direction so I can start having some fun as you all have displayed. I've also recently acquired CorelDraw to do my projects in however I still do not you know how to get project to printer or laser. I've got the turnkey Laser plugin installed and functional and I also have pronterface/slicer installed and functional. I have also connected to the laser successfully via pronterface. I'm sure there's something very simple that I'm missing and again hoping someone can direct me. Bottom line  I promised the wife id have this operational  to create  some small gifts for Christmas  and  I'm still not quite there,  lol  happy wife happy life but right now not so happy haha . Thanks in advance for any advice or direction given.

![images/a1e6cd97d255df53664ef5931fd55b01.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1e6cd97d255df53664ef5931fd55b01.jpeg)



**"Andrew ONeal (Andy-drew)"**

---
---
**Stephane Buisson** *December 14, 2015 10:08*

Well done, I think you have done the most difficult part.

on software side, look into this 2 directions:



- Visicut (print directly without gcode)

[http://hci.rwth-aachen.de/visicut-download](http://hci.rwth-aachen.de/visicut-download)



- Laserweb (?)

[https://github.com/openhardwarecoza/LaserWeb](https://github.com/openhardwarecoza/LaserWeb)

[https://plus.google.com/+PetervanderWalt/posts/gwt782ZSkkN](https://plus.google.com/+PetervanderWalt/posts/gwt782ZSkkN)


---
**Stephane Buisson** *December 14, 2015 10:30*

if you go for visicut :

in Option menu, add (+) lasercutter and pick Marlin, (you have the Marlin firmware for laser cutter installed in your beast ?). and insert the other parrameters.

in Menu Extra, install Inkscape plug in. then go into inkscape, you will see in menu Extension 'lasercut path', "open in Visicut".



So the work flow is now simple.

create or import in Inkscape your model, "open in visicut" (launch Visicut for you, but also plenty of other things like vectorise Fonts, ...), choose a material in the library (you have made), and/or do the settings manually (mapping, position, lasersetting). Execute ("print" directly on your K40, no gcode).



Note for mapping, you have a large choice to organise yourself. I do like the mapping by layers or colors.

like red line is cut, blue is mark, green engrave, etc...


---
**Andrew ONeal (Andy-drew)** *December 14, 2015 20:41*

The marlin software is in place and optimized for the version of k40 I have. I'm the type that likes to know what I'm doing and how it all comes together for end result. Same with rc hobby, while others were flying I spent months in basement learning what makes flight possible via software, firmware, coding, boot loaders, breakout boards, serial, epprom,  and ardupilot flight controller.



Currently everything in place is there following Jeremy of weistek engineering and Dan of 3dprintzothar as well as turnkey tyranny online direction/notes. I've also got air assist set up, additional air flow, led lighting, and laser pointer for guide but want laser at least trying to work before I attach everything again.



OK assume I get this all in place (which is different than everything I've installed to date), what would my process be to take an image of my wife and I and engrave into 1/8-1/4" acrilyc? The work you all put into this community and this hobby is nothing short of amazing. Thank you for the time put into your responses. You stated no gcode, does that mean I don't need pronterface, slicer, or turnkey plugin? If this is the case can I do all model work in Corel in stead of inkskape? Inkskape keeps crashing or locking up,  also gives error when generating gcode for images. Something to do with pll or dll install issues with imagelib, way way way over my head but read on it for four ours or so last night. 



Lastly, how do I set laser power, feed rate, and pulse rate? Using the set up you've suggested.



Thanks again for any assistance.


---
**Stephane Buisson** *December 14, 2015 21:49*

just Visicut should be fine, try it (open source free download), it could be quicker than a long speach.

DL the latest developer version. (1-7-295).

most of the answer will become apparent very quickly.

you can manage a material library with all the settings.

(eg : plywood 3mm, speed m/s, power %)



Inkscape, is fine with me (I am on MAC, sorry no Corel experience neither), maybe you have too much external plugin or some conflictual one. Visicut offer a illustrator plugin too.



you should clearly understand the difference between vector and pixel, vector will be a quick clean cut, pixels will be interpreted line by line (slow), that make a big difference with fonts.



I went for Smoothie, not Marlin, more specific questions should be answered by someone else.



Welcome here by the way.


---
**Andrew ONeal (Andy-drew)** *December 15, 2015 03:03*

Thanks, getting ready to head down to the mad science lab now. Just got shipment in for Quadcopter kits to that I'm working on selling. Lots of irons in the fire. Thanks again for direction.


---
**Andrew ONeal (Andy-drew)** *December 15, 2015 06:31*

Does this work with ramps? It does not say.


---
**Andrew ONeal (Andy-drew)** *December 15, 2015 07:45*

Ok installed adding laser but first issue, mine isn't online with URL and such, I'm connected via com port. Also have sd card option. Seems simple, is visicut and inkskape or Corel allb that's needed? Remember this is all very very new to me and I don't understand all the terms nor their purposes/functions. 


---
**ChiRag Chaudhari** *December 15, 2015 18:27*

I am also RC hobbyist, and yes If I can make some cool coasters and hot pad and stuff, the wife will be very happy OR say less mad about the $400 i spent for this machine and another $200 in bells and whistles.



Currently I am building the Bed so  did not cut/raster so far except some basic cutting of letters and the picture of wolf came in the Turnkey zip file.



Anyways, I am getting pretty good results so far, but I really need to finish building bed so that I can test different settings to figure out what it takes to cut 3mm plywood or acrylic. 



I also have Ramps up and running I can say. Using Turneky plugin to generate gcode and its working so far. I had to invert the direction of Y axes and direction of Y axes home in the code to make it work right. 



I read you saying engrave Image on Acrylic, but i dont think so it will come out great. Engraving on wood works the best IMO as you can get different burn marks to match the color of your photo.



I have tried LawerWeb and it works great, have not tried VisiCut yet. I still like to use SD card better than connecting laser to external computer.


---
**Andrew ONeal (Andy-drew)** *December 15, 2015 22:03*

Are you using Windows 7? When you installed inkskape did you have to install from cmd pmpt? Could you give me the steps you take to start new project(simple is fine), make ready in inkskape ie how to name layer(s) for laser speed, power, ppm, export settings in turnkey gcode exporter? So far I've not cut or burnt anything. I got mine off eBay and it came with no manual, instructions, explorations, it was not aligned, gantry was bent, mirrors and lenses were covered in film or oil, exhaust was not wired right with bare wire exposed, I paid 360, but got refunded a total of 200 which covered all my extra expenses to modify electronics and software. 



Rc hobby took back seat for good two months now need to get back at it and as stated got second shipment of Quadcopter kits last night to sell.


---
**ChiRag Chaudhari** *December 15, 2015 22:15*

My system is Win7 64 bit. But running Inkscape 32 bit version.

1. Download Link: [https://inkscape.org/en/gallery/item/3950/download/](https://inkscape.org/en/gallery/item/3950/download/)



2. Once Installed go to your C:/porgram files/inkscape and RENAME the foler "python" to "python-old"



3. Download this RAR file. Its pre compiled version of Python 2.7. : [https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin/raw/master/files/Python27.rar](https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin/raw/master/files/Python27.rar)



4. Open the archive and copy folder Python27 to c:/program files/inkscape and then rename it to "python" 



5. Now download the Turnkey Plugin form here: [https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin/archive/master.zip](https://github.com/TurnkeyTyranny/laser-gcode-exporter-inkscape-plugin/archive/master.zip)



6. Unzip and copy "turnkeylaser.inx" and "turnkeylaser.py" files to c:\programfiles\inkscape\share\extenstions\



7. Your are done! Run Inkscape and under Extension > Export you should see Turnkey Laser Exporter 



Hope that helps. Or you are fee to buzz! Oh wait now you have to + me "P


---
**Andrew ONeal (Andy-drew)** *December 16, 2015 04:39*

What steps did you take to actually get a job done by laser, do you use as card or connect directly ?


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/DA5dvAuXtyB) &mdash; content and formatting may not be reliable*
