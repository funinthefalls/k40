---
layout: post
title: "I have built a z axis table for my k40"
date: October 09, 2017 18:59
category: "Modification"
author: "Steve Good"
---
I have built a z axis table for my k40. I still need the drive belt. I need a t5 6mm width belt. I would like to get it today or tomorrow so I would prefer to buy it locally if possible. Anyone have any tips on where to find open loop belt without buying online?  I have ordered one from ebay but it will take a few days to get here.  





**"Steve Good"**

---
---
**Chuck Comito** *October 10, 2017 21:05*

I don't think there are any walk in stores that carry this type of equipment. You might get lucky if you have a local granger outlet though. 


---
**Abe Fouhy** *October 19, 2017 08:58*

**+Steve Good** What is an open loop belt?


---
**Chuck Comito** *October 19, 2017 11:43*

Open loop is a spool of belt that isn't connected at the ends. Closed loops would be like the ones you see on your car where it's a continuous loop. 


---
**Abe Fouhy** *October 20, 2017 02:51*

How does that work? Is it connected at each end and just rocks back and forth?


---
**Chuck Comito** *October 20, 2017 11:01*

**+Abe Fouhy**​, I'm not sure what your asking. Try to searching the internet for closed loop timing belt and also open loop timing belt and you'll see what we're talking about. 


---
*Imported from [Google+](https://plus.google.com/106153533504584428744/posts/WzinVvc58c2) &mdash; content and formatting may not be reliable*
