---
layout: post
title: "Personal messaging someone ... After following someone you can easily PM them by going to \"Home\" and entering a post in the \"whats new with you\" box"
date: July 02, 2017 02:48
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40PersonalMessage

<b>Personal messaging someone</b>

... After following someone you can easily PM them by going to "Home" and entering a post in the "whats new with you" box. 

... When the post opens the "blue" area next to your name has a drop down shade from which you can select the destination category. 

... Click "See more" at the bottom and another shade allows you to see other choices including those you are following and your circles.







**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/QnCFS2Poeqo) &mdash; content and formatting may not be reliable*
