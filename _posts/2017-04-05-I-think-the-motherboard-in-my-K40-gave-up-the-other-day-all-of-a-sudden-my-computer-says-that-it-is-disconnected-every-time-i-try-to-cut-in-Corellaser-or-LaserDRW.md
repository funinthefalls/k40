---
layout: post
title: "I think the motherboard in my K40 gave up the other day :/ all of a sudden, my computer says that it is disconnected every time i try to cut in Corellaser or LaserDRW"
date: April 05, 2017 17:24
category: "Discussion"
author: "Martin Larsen"
---
I think the motherboard in my K40 gave up the other day :/

all of a sudden, my computer says that it is disconnected every time i try to cut in Corellaser or LaserDRW.

Im not up for the challenge, so if anyone need a K40 with malfunctioning motherboard(perhaps someone who wants to upgrade) Please let me know.

Its about a year old, and have probably been cutting for 1.5 to 2 hours accumulated...





**"Martin Larsen"**

---
---
**Stephane Buisson** *April 05, 2017 17:33*

It would help if you could say where you are located ;-)) and how much you want for it.



Denmark from your profile.






---
**Ray Kholodovsky (Cohesion3D)** *April 05, 2017 17:57*

You sure? The Cohesion3D Mini should be a fairly simple swap and will upgrade the performance of your machine in the process. 


---
**Ashley M. Kirchner [Norym]** *April 05, 2017 20:05*

Also, have you exhausted all other options such as something as simple as using a different USB port, or USB cable? And if you have, then I highly recommend an upgrade as Ray stated above. Unless you are getting out of owning a laser cutter entirely.


---
*Imported from [Google+](https://plus.google.com/105195340721594982075/posts/6pQribQyZon) &mdash; content and formatting may not be reliable*
