---
layout: post
title: "After tinkering with my K40 for more than a year I finally did my first real project!"
date: November 25, 2018 19:53
category: "Modification"
author: "Don Kleinschnitz Jr."
---
After tinkering with my K40 for more than a year I finally did my first real project!

This project was a hybrid of wood turning and laser engraving.



Recently I have been working on rebuilding the optical path and designing alignment tools.



Once fabricated the tools worked really well and helped me find and understand weaknesses in my optical path and its alignment. Most of my frustration was due to the head not having an adjustable mirror.



Note: I plan to replace the head with a new design that has an adjustable mirror... more later.



Based on the Santa Cookie Plate project I am calling these new optical components and alignment tools a successful implementation.

[https://donsthings.blogspot.com/2018/11/klineworks-k40-alignment-tools.html](https://donsthings.blogspot.com/2018/11/klineworks-k40-alignment-tools.html)







**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *November 25, 2018 22:47*

If you put anything on the plate, you're going to cover up the wonderful artwork.


---
**Don Kleinschnitz Jr.** *November 26, 2018 04:29*

I'm guessing the cookies won't be on the plate long :)!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/GXvrPsTE3R9) &mdash; content and formatting may not be reliable*
