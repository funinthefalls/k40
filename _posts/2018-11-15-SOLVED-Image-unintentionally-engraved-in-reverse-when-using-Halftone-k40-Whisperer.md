---
layout: post
title: "SOLVED Image unintentionally engraved in reverse when using Halftone (k40 Whisperer)"
date: November 15, 2018 03:34
category: "Software"
author: "'Akai' Coit"
---
<b>SOLVED</b>



Image unintentionally engraved in reverse when using Halftone (k40 Whisperer). 



<b>(Problem seems to have gone away with software updates, but still keeping an eye on it.)</b>



So what I'm occasionally running into is when using halftone (to engrave many small images with gradients and shading), the machine will randomly decide to reverse the image engraving. I choose the opton of engraving from bottom to top by default. What will happen is in mid engraving, the machine will switch direction and go from top to bottom, overwritting the already engraved surface. When stopping the process manually and starting a new one, the engraving will still go from top to bottom reversing the image, starting at the original "0" location of the bottom of the image (imagine making a mirrored image below the original image in question). This flaw does not correct itself until I turn off halftone (which I just did when I decided to do another project instead of the one I was just working on). I do not know if turning halftone back on and trying again will give me the same results of the reversed image, but will test shortly so I can give feedback on that specifically. Any suggestion on what I can provide to help troubleshoot this problem?





**"'Akai' Coit"**

---
---
**Scorch Works** *November 16, 2018 19:54*

1. Do you have a sample for for which this problem occurs?  



2. Has it ever happened engraving top to bottom? 


---
**'Akai' Coit** *November 19, 2018 08:56*

2. I have not tested top to bottom, but I will find out this week and get back to you on that.


---
**'Akai' Coit** *November 19, 2018 08:57*

1. Are you asking for a picture of the incorrectly engraved wood?


---
**Scorch Works** *November 20, 2018 02:20*

No, I am looking for a sample input file that has the problem when it is run. (And a settings file with the settings saved from when the problem occured.)  



How common is the problem approximately?  50% of the time 10% of the time or 1% of the time?


---
**'Akai' Coit** *November 20, 2018 06:57*

I can link to the files sometime tomorrow on my Google drive... I'd say it happens maybe 30% of the time. I don't engrave with dithering often (yet).


---
**'Akai' Coit** *November 20, 2018 06:57*

Testing out top to bottom engraving of the same file tomorrow.


---
**Scorch Works** *November 21, 2018 04:10*

**+'Akai' Coit** 30% of the time is a lot.  Sorry you are having this problem.


---
**'Akai' Coit** *November 25, 2018 09:33*

So far, top-down engraving seems to be working perfectly fine. Not one error in halftone and when moving from one object to another, it skips the open space (which it does not do in bottom-up mode). So, for now, I will be sticking with top-down. I wan't call this "fixed" just yet, though. I wanna run with this for a while to see if either of the issues happen at all in top-down mode.


---
**'Akai' Coit** *December 02, 2018 00:00*

6 days with the only issue ever coming up happening today (odd vector issue where system mistook coordinates for one object and tried to derail... did fewer pieces simultaneously and that issue wasn't there). The issues in relation to the original post have not shown up yet.


---
**Scorch Works** *December 03, 2018 05:52*

Typically when people report the laser head trying to move out of the laser working area it has been caused by a feature in the SVG file that was not intended located off of the design area. 


---
**Scorch Works** *December 04, 2018 05:03*

I put out a new version that fixes the problem related to raster engraving not skipping the blank spaces when engraving bottom up.



I have not been able to recreate the problem that results in the gantry reversing direction during raster engraving.



I am really interested in seeing a file and the related settings that can recreate the reversing direction problem if you ever find one.


---
**'Akai' Coit** *December 04, 2018 20:05*

Will definitely let you know. Seems extremely random. But it only seems to kick in when I have a lot of detail involved.


---
**'Akai' Coit** *January 14, 2019 04:41*

Finally got around to updating to the newest version of whisperer. The bottom up engraving issue seems to have completely disappeared but I have a new issue now that I find interesting. I will start a new thread about it so as not to migrate this one away from the core issue I started it for.


---
**'Akai' Coit** *January 14, 2019 04:41*

I believe the current version is .27...?


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/H4xLBV2Kosc) &mdash; content and formatting may not be reliable*
