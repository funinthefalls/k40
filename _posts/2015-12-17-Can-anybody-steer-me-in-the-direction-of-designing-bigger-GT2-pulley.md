---
layout: post
title: "Can anybody steer me in the direction of designing bigger GT2 pulley"
date: December 17, 2015 23:32
category: "Hardware and Laser settings"
author: "ChiRag Chaudhari"
---
Can anybody steer me in the direction of designing bigger GT2 pulley. may be 30 teeth size or so. I spent last two days trying to figure how I can space 30 circles of 1.11mm Diameter each at 2mm apart in a circle. I saw couple of videos of people designing it but in high end CAD program which I dont know how to use of course.



Please help! I am in the making of something super cool!  Trust me ! 





**"ChiRag Chaudhari"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 00:00*

Just so I'm understanding correct, by GT2 pulley you mean something like this [http://openbuildspartstore.com/gt2-2mm-aluminum-timing-pulley-30-tooth/](http://openbuildspartstore.com/gt2-2mm-aluminum-timing-pulley-30-tooth/) ?



This one already is 30 teeth...


---
**ChiRag Chaudhari** *December 18, 2015 00:03*

**+Yuusuf Sallahuddin**  You are absolutely correct. The only reason I can not go with these is because I need really big bore. Let me put my idea on the paper (so that you guys know I suck at drawing too) so you can understand what I am planning to do , why i need bigger bore!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 00:10*

**+Chirag Chaudhari** It would depend how large a bore you require, but I imagine the larger the bore, the less likely that you can do with 30 teeth. Unless you mount it differently from the way it is meant to be mounted. The one I posted has 5mm bore & judging from the design specs, it could be drilled out to anything <13mm that would still allow for structural integrity; so maybe something in the range of 10mm bore by drilling it out (would leave 1.5mm walls).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 00:23*

Regarding spacing the circles 2mm apart in a circle, I would start with a 30 sided shape, with 2mm sides. Basically each 2mm side would be 12 degrees of rotation from the previous (360/30 = 12). Then I would centre the 1.1mm circles in the centre of the 2mm lines. (oops, that would make the centre of the 1.1mm circles 2mm from each other... not 2mm apart from the edges).

(edit: would actually be 1.989mm apart from centres that way, needed adjusting to make 2mm...)


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/2uuMksinQzv) &mdash; content and formatting may not be reliable*
