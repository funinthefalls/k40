---
layout: post
title: "what is the thickest wood the machine can cut?"
date: December 12, 2017 21:37
category: "Materials and settings"
author: "krystle phillips"
---
what is the thickest wood the machine can cut?





**"krystle phillips"**

---
---
**Joe Alexander** *December 12, 2017 21:50*

in one pass? probably wouldn't try to go thicker than 1/2" or so.


---
**Robi Akerley-McKee** *December 12, 2017 22:30*

With the 4" focal point lense (101.6mm) i was cutting 3/8" slowly. With the 2" (50.8mm) I was cutting 1/4 plywood.  You're better off running lower power and make more passes, definately use air assist. 


---
**Steve Clark** *December 13, 2017 18:26*

Here is what I've learned so far about the  

components that control cutting: 



The material and if PWD the glues used also.



1  Beam focal length.



2  Beam power available at a particular spot size and your acceptable cut width above and below that spot size.



3  The ability to move that focal length down (and up) as you cut.



4  Being able to keep your material from catching fire.



5  Number of passes.



6  Ability to have a clear beam pass to the material, i.e. clearing smoke as you cut.



7  The reflectivity (%) of the material at the beams wavelength and beam fringe scatter (Electrophoretic light scattering). 



Here is what looks like a neat little program I just found. I haven't time to try it out, nor do I understand what M2 means in it yet. I'm guessing it has to do with the beam form or intensity plot of the spot.?. For now, I would just leave it at 1.0 or 1.5 and see what you get.  



[controllaser.com - Laser Beam Spot Size and Depth of Focus Calculator](https://www.controllaser.com/resources/laser-beam-spot-size-and-depth-of-focus-calculator/)



I most likely gave you more information than you wanted... but I felt this was good info to get on the site.



In theory (my theory) you should be able to cut about 120% of the distance from the tip of you laser head to the top of the material at point focus. 



So, my distance with my 50.8mm FL gives me about 20.5mm clearance. If I take a piece of Popular,  3/4 of an inch thick, I should be able to cut through it with 3 or 4 passes at around 10ma (feed TBD).



If I get time today I'll try it.    



 


---
**Steve Clark** *December 13, 2017 20:11*

OK…I had some time so I did a test cut in 5/8 (15.9mm) inch Popular. At 11ma, 300mm/min, and three passes with focal point at about center of material. The coolant temp was at 10C. It had no problem going through the Popular. Actually I believe I was high on the power  as 8ma would have also worked. 



I didn’t use the z axis table because there is a slight wobble in them. Someday I plan on rebuilding the LO table I have making it larger and using roller guides to rid the wobble. Then I can raise it up or down without the side to side shift.



So…I do believe it is within the abilities of the 40K to cut ¾” in Popular but In harder woods maybe 5/8” with multiple passes. However, as others have said using the power (milliamps) on the high end can effect tube life. Also, cooling the tube to just above dew point becomes critical.



![images/e67056e4a9d6c5ef487b457a22eac025.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e67056e4a9d6c5ef487b457a22eac025.jpeg)


---
**Joe Alexander** *December 14, 2017 07:27*

im surprised you got your coolant so low as most tubes prefer 15-20°C. but hey its more about the dew point, right? :P


---
**Steve Clark** *December 14, 2017 18:46*

Yes I try to run about about 3C above the dew point but I have to watch it because temp and humidity changes during the day. Since I have a Peltier cooling system with a JLD-612 Temperature Controller I can adjust as needed.



I need to get a better hygrometer though as I'm not trusting the one I have it's getting flaky.




---
*Imported from [Google+](https://plus.google.com/+krystlephillips/posts/S6tkuTRACTb) &mdash; content and formatting may not be reliable*
