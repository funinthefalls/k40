---
layout: post
title: "Hello, I've had my machine a little over 5 weeks"
date: November 15, 2017 02:19
category: "Discussion"
author: "Alex Raguini"
---
Hello,



I've had my machine a little over 5 weeks. I've read everything I can find and watched most videos I can find.  I'm still a little confused. 



As for power setting, 23ma is the rated max but most agree that kills the tube fast. I've heard 10ma to 18ma as recommended.  What does conventional wisdom say?



Lens - I've read concave down and convex down. I know there is a difference in focal length. What is conventional wisdom here? 







**"Alex Raguini"**

---
---
**Jim Hatch** *November 15, 2017 02:45*

18ma-20ma should run about 90-95% power which is a good rule of thumb to limit output so tube life is not prematurely shortened.



Lens goes in with the bump up.


---
**Chris Hurley** *November 15, 2017 02:46*

Round side up. I don't use over 10ma. The allinement is a bitch. Air assist is awesome. 


---
**Alex Raguini** *November 15, 2017 03:27*

I agree about alignment. I spent about two weeks working on just the alignment.  Getting ready for air assist. Working on mods one at a time. 


---
**Alex Raguini** *November 15, 2017 03:55*

One more question - water temp. What is the max water temp before the last should be stopped?


---
**Chris Hurley** *November 15, 2017 04:03*

30c is dead stop for me. My Reservoir is a 15 L tank. Also cut yourself a better extraction vent for the back of the machine. 

![images/0104cbf34cbe80bc261f257e4fa14df0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0104cbf34cbe80bc261f257e4fa14df0.jpeg)


---
**Joe Alexander** *November 15, 2017 04:09*

15-30c for water temp with 30 being max. I usually run around 15-20c(just make sure your above the condensation temperature)


---
**Alex Raguini** *November 15, 2017 04:12*

Thank everyone. 


---
**Ned Hill** *November 17, 2017 15:42*

30C is fairly high IMO.  I would limit it to not more than 25.  I try and keep mine between 18-22C.


---
**Alex Raguini** *November 17, 2017 15:52*

thanks everyone




---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/PizUE27BgAH) &mdash; content and formatting may not be reliable*
