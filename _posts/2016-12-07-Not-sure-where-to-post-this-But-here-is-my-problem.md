---
layout: post
title: "Not sure where to post this. But here is my problem"
date: December 07, 2016 16:55
category: "Original software and hardware issues"
author: "woodknack1"
---
Not sure where to post this. But here is my problem. If I draw up a rectangle in coreldraw lets say 72.6mmx 10.00 and cut it out  I am getting 73.48 on the long end. Why is it doing that? Is there a setting somewhere that can fix this. I know my belts are fine as I'm getting repeatable every time. I know in lasers with DSPcontrollers you can go into the software and compensate for this. The software corellaser is so limited to what you can do, im at a loss. Any help please.





**"woodknack1"**

---
---
**Kelly S** *December 07, 2016 17:05*

Is your focal point set correctly?


---
**Nigel Conroy** *December 07, 2016 17:06*

Could it be to do with a cut in or cut out option?


---
**Stephane Buisson** *December 07, 2016 17:09*

Maybe all the other bugs with Corel will push you to look at other solution like the Smoothie mod. it's frustration like that who initiate the project.


---
**woodknack1** *December 07, 2016 17:24*

ya my focal point is set correctly. I did a ramp test to get it to where it should be.



cut in or cut out option? You lost me here.



Yes I should have bought the 50w with DSP controller for sure.


---
**Phillip Conroy** *December 07, 2016 19:15*

How thick is the line,change to 0.001mm


---
**woodknack1** *December 07, 2016 19:23*

I have it set on hairline.


---
**woodknack1** *December 07, 2016 19:30*

That maybe my problem though. As I just read hairline thickness in coreldraw it .075. I will try changing it to .001 when I get home from work. Thanks.


---
**Kelly S** *December 07, 2016 20:00*

Line width messed with me forever.  I use fusion 360 and I have to use inkscape to change all the lines to .1.  I also ditched corel and upgraded to a smoothie board from c3d and use laser web now, so much better.


---
*Imported from [Google+](https://plus.google.com/107255578063171490994/posts/R22La8Sgbm3) &mdash; content and formatting may not be reliable*
