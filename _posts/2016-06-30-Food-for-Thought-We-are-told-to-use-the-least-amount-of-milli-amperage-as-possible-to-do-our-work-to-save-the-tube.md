---
layout: post
title: "Food for Thought We are told to use the least amount of milli-amperage as possible to do our work to save the tube"
date: June 30, 2016 00:03
category: "Discussion"
author: "HalfNormal"
---
Food for Thought



We are told to use the least amount of milli-amperage as possible to do our work to save the tube. I was just doing some cutting experiments on 3mm birch ply. I cut a 1 inch/24.4 mm circle at different speeds and current settings. So the results are minimum setting on my setup is 4ma @ 3mm/sec = 8.1 seconds of cutting time

9ma @ 12mm/sec = 2.2 seconds of cutting time

So twice the amps at a 1/4 of the time. I wonder which one will lower the life of the tube more and quicker?





**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 00:16*

That's interesting. It would be nice to know which works out more economical. I wonder is it a linear scale or exponential or some other function.


---
**greg greene** *June 30, 2016 00:41*

Which stresses your car engine more, doing the quarter mile in 4 seconds, or doing the quarter mile in 20 seconds?


---
**HalfNormal** *June 30, 2016 00:45*

**+greg greene** True, but do we change the oil in our cars  in respect to mileage/time or speed (which will stress the vehicle more the faster you drive)?


---
**Anthony Bolgar** *June 30, 2016 00:47*

I consider Laser tubes a consumable item ( like sandpaper, paper towels etc.)that will require replacement, and build the cost into anything I do. I would rather be able to make 4 times as many widgets in the same time frame, and use the the money from every 4th widget to buy a new tube when needed. That way I am still ahead of the game, and get jobs done faster.


---
**Jon Bruno** *June 30, 2016 02:44*

I'm more concerned with the shelf life of the two spares I have in the closet.

I don't use my laser enough to worry about  "wearing it out".

I guess I best start building a death ray or something with the other two.




---
**Jon Bruno** *June 30, 2016 13:57*

I will get them out and hook them up to my spare LPSU. I have only had them for a few months but yeah, I don't want them to go to waste.. I wish I knew someone who did glasswork it would be neat to have a schrader valve or something available to recharge them. I wonder if the RECI tubes could have this incorporated into the metal endcap?  <wink>

I know they have refillable 100+ watt glass tubes.. but nothing in the lower size rigs


---
**Jon Bruno** *June 30, 2016 14:42*

That would be something.. 

hmmmm..


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/WbguovwXNvs) &mdash; content and formatting may not be reliable*
