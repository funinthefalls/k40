---
layout: post
title: "Hi Where can I buy material like plexiglass and acrylic to engrave with laser ?"
date: November 24, 2015 02:11
category: "Discussion"
author: "Gilles Letourneau"
---
Hi 



Where can I buy material like plexiglass and acrylic to engrave with laser ?



I live in Canada so a place in Canada or USA is ok.



Tranparent plexiglass is easy to find but I want different color

also other material like plastic for name plate etc..





**"Gilles Letourneau"**

---
---
**Ashley M. Kirchner [Norym]** *November 24, 2015 03:44*

It would be helpful to say where you are located. Sometimes you can get stuff locally, other times you're stuck with ordering online.


---
**Gilles Letourneau** *November 24, 2015 04:36*

I'm in Montreal,


---
**Coherent** *November 24, 2015 12:32*

I'm pretty sure you have a Home Depot or Lowes in Montreal. Or any home improvement/hardware store. Look in the glass & window department.


---
**Gary McKinnon** *November 24, 2015 12:57*

Ebay is great for all sorts of acrylic, cheaper than Amazon.


---
**Stephane Buisson** *November 24, 2015 16:09*

Ebay is great with Paypal buyer's protection scheme. AliExpress should be avoid for the same reason, protection scheme is a scam and do not cover you from bad sellers (they all know it's a flaw in the system and don't want to rectify the situation, allowing the buyer's abuse).


---
**Ashley M. Kirchner [Norym]** *November 24, 2015 17:39*

I get my (cast) acrylic and wood from Inventables.com ...


---
**DIY3DTECH.com** *November 24, 2015 20:04*

Does any one have a preferred eBay seller for acrylic?  I too have bought (I also do CNC machining) however the cost has jumped for shipping to almost double the price as I would like to buy a stack and not pay double for shiping...


---
**Ashley M. Kirchner [Norym]** *November 24, 2015 20:28*

One thing to look for when buying acrylic: you want cast acrylic. It engraves better.


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/LQY4Ku3tENf) &mdash; content and formatting may not be reliable*
