---
layout: post
title: "Following someone You can follow a members post and their collections"
date: July 02, 2017 02:41
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40FollowingSomeone

<b>Following someone</b>

You can follow a members post and their collections. Following a member gets you access to their collections and the use of "hangouts" with them. 

.. Clicking their Avatar in a thread [upper left corner]

.. When their page opens click "FOLLOW"

You can also click "follow" on any of their public collections you want to follow.









**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/hgTuzfPom2F) &mdash; content and formatting may not be reliable*
