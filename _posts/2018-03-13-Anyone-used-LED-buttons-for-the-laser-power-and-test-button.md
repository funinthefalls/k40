---
layout: post
title: "Anyone used LED buttons for the laser power and test button?"
date: March 13, 2018 18:51
category: "Discussion"
author: "Bill Keeter"
---
Anyone used LED buttons for the laser power and test button? I can't figure out how to wire them up so the LED part comes on ONLY when the buttons are active.



Ulincos Push Button Switch U19 (one momentary and one latching)



I can make them work as regular buttons by wiring to NO and C. Which leaves the positive and negative lines (which I think are for the LED) unused. If I follow the manufacturing diagram I don't know where to put the black negative line. As both laser power and test switches only generally use 2 wires.



For reference, I'm building a 60W laser and my Laser PSU wiring follows the attached drawing from Don. I'm just using a C3D board instead. Also including some pics of my custom control panel. Which i'm hoping to install tonight..



![images/f4cd1953fde37d50c44af589f3a78055.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4cd1953fde37d50c44af589f3a78055.jpeg)
![images/2a443528dfbb6a8f37f4be6db30ea304.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a443528dfbb6a8f37f4be6db30ea304.jpeg)
![images/cdd438f259077e1d00861dede0e576f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdd438f259077e1d00861dede0e576f8.jpeg)
![images/62d6949f79ecab035b67117acb14cb7c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/62d6949f79ecab035b67117acb14cb7c.jpeg)
![images/138fbb9b77449fce9fa12fc0410e0bb2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/138fbb9b77449fce9fa12fc0410e0bb2.jpeg)
![images/c3223524c40564429718c4d09922c7a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3223524c40564429718c4d09922c7a2.jpeg)

**"Bill Keeter"**

---
---
**Joe Alexander** *March 13, 2018 19:25*

It looks like you would have to run 1 extra wire, connect the black wires of your buttons together and tie it to the closest negative point (on the laser psu, 24v psu, controller card negative, etc). I'm guessing that the ground wire is just for handling the small amount of current going through the led.


---
**HalfNormal** *March 13, 2018 19:25*

There are a lot of assumptions on how these will be wired. First, the LED does not have it's own contacts to enable. It is relying on the switch contacts and common power to what is being switched. So think of the LED as being wired in series with the switch and power.  Now this all assumes that the power being switched is 12VDC. The way I would wire it up would to use a relay. This serves to isolate the switch and LED functions. Using a DPDT relay, the switch turns on the relay, one set of contacts illuminate the LED and the other completes the path of control.


---
**Bill Keeter** *March 13, 2018 19:34*

FYI, I've used one of these in the past on an old Arcade board running 5V. The LED glowed, it just wasn't as crazy bright as at 12v. 



**+HalfNormal** Thx I'll look into the DPDT relay.


---
**HalfNormal** *March 13, 2018 19:37*

Actually after I posted I realized that a DPST relay will work as well.


---
**Bill Keeter** *March 13, 2018 19:38*

maybe what this guy is doing here? 


{% include youtubePlayer.html id="13spvuJIR98" %}
[youtube.com - 19mm LED latching switch wiring](https://www.youtube.com/watch?v=13spvuJIR98)


---
**Don Kleinschnitz Jr.** *March 13, 2018 19:58*

Here is my <i>back-of-napkin</i> answer. Don't have the time today to check thoroughly ... so check carefully.



Assumptions: 

...The LED's are 12V

... The LED's are isolated from the switch terminals

... The Test Switch is a PB?

... The Laser Switch is alt on-off?

...The drop across D1 will not effect the L signals integrity. Rather not add another diode drop into the L path. 



Operation:



When switch is engaged ground is provided to the LED for that switch.



R1 & R2 could be eliminated if you have a separate 12V supply. Otherwise you can use the LPS 24V supply with resistors to limit current. 



D1 keeps the <i>Laser Test</i> switch from grounding the L signals mosfet output. Precautionary, depending on how L is connected to the smoothie may not need D1. 



To size resistors need to measure the current draw at 12V across the LED. I can calculate these if you provide the 12V current.



5V versions of these switches would eliminate the resistors or a separate 12V supply.







Seems simple enough but I would test this on the bench so make sure that TL and WP are  ground when switches are activated and open when not... verify I did not advise something stupid :(



![images/5f992e3f23c38288a389052393ff8583.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5f992e3f23c38288a389052393ff8583.jpeg)


---
**HalfNormal** *March 13, 2018 20:03*

Don, are all grounds in the PS common?


---
**Don Kleinschnitz Jr.** *March 13, 2018 20:10*

**+HalfNormal** Yup ...


---
**Bill Keeter** *March 13, 2018 20:12*

**+Don Kleinschnitz** I'm running separate 24v and 12v PSU's for this 60W laser on top of the Laser PSU.



Laser Switch wired to WP and G on Laser PSU



Laser Test wired to TH and 5V on Laser PSU



and a random pic of the machine in progress..

![images/1bd7cad2c055b68a1691d4bd4e5f14f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bd7cad2c055b68a1691d4bd4e5f14f9.jpeg)


---
**Don Kleinschnitz Jr.** *March 13, 2018 20:20*

**+Bill Keeter** I changed the Laser Test from TH to TL so you could use this configuration.



Using your LPS 24V will make the switch wiring simpler as they all come from the same place. Also it will isolate any other DC supply from the LPS. 



Of course you could just run the LED's on the 5V and have dim lights. Then again resistors are cheap :)!


---
**Bill Keeter** *March 13, 2018 20:33*

Hmm.. I wonder if the wiring can be done a little simpler. Based on this diagram....



Also here's one of the pushbuttons on Amazon for easier reference. [https://www.amazon.com/gp/product/B06XT29ZFP/](https://www.amazon.com/gp/product/B06XT29ZFP/)





![images/86e46bd0af5ab830ef69a449746525de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/86e46bd0af5ab830ef69a449746525de.jpeg)


---
**Bill Keeter** *March 13, 2018 23:07*

**+Don Kleinschnitz** okay. Had to digest your sketch for a little bit. Not my area of expertise. 



Anyway, this looks like it solves my wiring issue. Just need to get some diodes. Can you help with an amazon link for some good diodes or can I get them at a local auto shop?



Also check the pic below. Not sure my LPS has 24v line I can wire. Could I just use the 5v instead. The LED will still be plenty bright. ![images/bf2896296d14f7e5e315393e8ee331e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf2896296d14f7e5e315393e8ee331e9.jpeg)


---
**Don Kleinschnitz Jr.** *March 13, 2018 23:15*

**+Bill Keeter** how is the drawing I gave you not just as simple :)?

![images/a81332b3a7949b76130d6ea96c293af3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a81332b3a7949b76130d6ea96c293af3.jpeg)


---
**Bill Keeter** *March 13, 2018 23:22*

An example of this push button on 5v with a previous arcade project. ![images/5107bbd1cfca49de443e7a8e45feb5ed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5107bbd1cfca49de443e7a8e45feb5ed.jpeg)


---
**Don Kleinschnitz Jr.** *March 13, 2018 23:31*

**+Bill Keeter** 

The PB LED draws 18ma (.018 amps) @ 12vdc.

I would guess you will draw about 7.5ma (.075amps) @ 5vdc. So your supply will see roughly 15ma for both switches if you use the 5v.



If your happy with the brightness at 5v, sure you can use the 5V as I would expect it can handle 15ma.



Any 1N914 diode should work. Search that # on Amazon there are lots of choices. A local RS, if you can find one, should have them. Again you may not need the diode.



These are momentary switches, which will work fine for the <i>Laser Test</i> but you need an alternate action or rocker switch for the <i>Laser Switch</i> ?? i.e it has to stay on or off ....?


---
**Don Kleinschnitz Jr.** *March 13, 2018 23:35*

**+Bill Keeter** looks ok to me. Your homework is to blink the LASER SWITCH to alert the operator that the laser is enabled !!!!


---
**Bill Keeter** *March 19, 2018 03:27*

**+Don Kleinschnitz** decided to just run a relay for each of these buttons. That way I can physically separate the voltage going to the LED (12V) and WP/G (5V). And I can do the same for the Test fire button using LH.



Also for your other question. They make these LED buttons in both momentary and latching. So seeing the always on RED LED will help remind me "Laser On".



A side benefit of these relays, is that I can still run the laser on switch in series with a physical water flow switch (with it's own LED bulb).


---
**Don Kleinschnitz Jr.** *March 19, 2018 12:32*

**+Bill Keeter** not sure of your exact circuit.  Just note, if your having a relay that indirectly controls the enable circuit that may add more items that can fail in the ON state. It may not be obvious but make sure the circuit is designed for things fail in the "disabled" state :).



I choose not to put anything but a switch in line with laser control safety circuits.



A last hint since enable, fire and L are [internal to the lps] in the leg of an Optocoupler they do not like any voltage drop across a device that may be in series with that circuit. A switch is fine but a LED may create a problem.


---
**Timothy Rothman** *March 20, 2018 05:42*

I've been looking into a similar thing using 12V relays conncected to the switches to drive the lights and also the 12V relays would be used as the control leg to switch on larger 120V relays for things like water pump, air assist, and so on.  Do the power diodes do the same thing?  The things I was thinking of would not be momentary connections but rather continuous.  Thanks!


---
**Don Kleinschnitz Jr.** *March 20, 2018 12:59*

**+Timothy Rothman** I do not see how power diodes would do the same thing. 

Check out the power control tab on this schematic and see if that helps with some ideas. 



[digikey.com - K40-S 2.1](https://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)


---
**Bill Keeter** *March 22, 2018 14:36*

**+Don Kleinschnitz** here's the wiring diagram for the Laser On switch using a relay. Everything defaults to "open". So a failure should stop current flow. 



The wiring would be the same for Laser Test. Just with a momentary switch and wired up to LH and G instead. ![images/2b0c59525b30cf2b315219396b4da31a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b0c59525b30cf2b315219396b4da31a.jpeg)


---
**Don Kleinschnitz Jr.** *March 22, 2018 15:18*

**+Bill Keeter** looks rational but I still do not see an advantage of this approach over the circuit I gave you without relays :) :). 


---
**HalfNormal** *March 22, 2018 15:21*

**+Don Kleinschnitz** total isolation and can use separate power supplies. 


---
**Don Kleinschnitz Jr.** *March 22, 2018 17:22*

**+HalfNormal** total isolation of the switch from the LPS? How do you see that as important? The inputs to the LPS are optically isolated? :). My circuit can also used separate power supplies all they share is a ground?:)


---
**HalfNormal** *March 22, 2018 17:34*

How can it be bad? ;-)

A few more parts, yes.


---
**Bill Keeter** *March 22, 2018 17:40*

**+Don Kleinschnitz**  for myself, I wanted everything separated out for my own sanity. The relay, while a hassle, made it simple for me to mentally go through the process of testing each mechanism by itself. I made a few mistakes with my wiring last week (unrelated to these switches) and blew a few fuses. So now i'm being more careful. 



Please don't misunderstand. Your wiring diagram was very helpful. As I was thinking about these switches all wrong.



One benefit, for myself, with this new wiring diagram is that I plan to run the door switches, water flow switch and Laser On switch all in series. So now with this relay, I'll be able to see the red LED "on" light even when the door is open.


---
**HalfNormal** *March 22, 2018 17:58*

**+Bill Keeter** Not to confuse you more, but one way to make sure you are absolutely  safe with interlocks, is to make the relays self latching and make the power for the relays go through the interlock switches. What this accomplishes is that the relays will drop out when an interlock is activated. Just another idea but not necessary.

[reuk.co.uk - Latching Relay Circuit &#x7c;](http://www.reuk.co.uk/wordpress/electronics/latching-relay-circuit/) 


---
**Don Kleinschnitz Jr.** *March 22, 2018 18:05*

**+Bill Keeter** no worries, I just ask because I like to learn why one or the other approach is attractive to a maker and check that I didn't miss or should learn something. There is always multiple ways to solve a problem.



Your point about the led being on when the  laser sw is in the enabled position and the interlock open .... shouldn't the enable light go out when the interlock is opened .... :).



On mine when an interlock was open and I could not see it, it drove me up the wall. I found a way to put an led in the circuit that was lit anytime the loop was enabled. Its saved me from standing on my head when I thought my LPS or laser was dead.


---
**Don Kleinschnitz Jr.** *March 22, 2018 18:06*

**+HalfNormal** lol ... does >>> "not bad = good?" :).


---
**HalfNormal** *March 22, 2018 18:08*

I will explain if you send a picture of yourself standing on your head! ;-)


---
**Bill Keeter** *March 22, 2018 18:28*

**+Don Kleinschnitz** haha.. yeah, true the interlock and the Laser On LED could get confusing in a hurry. 



Luckily I have another LED (green) i'm setting up that will tell me when the waterflow & interlock switches are closed.


---
**Don Kleinschnitz Jr.** *March 22, 2018 19:37*

**+Bill Keeter** of course ALL of this could be avoided with DPDT switch/pb and parallel interlock switches. And you get true redundancy for free...




---
**Don Kleinschnitz Jr.** *March 22, 2018 19:38*

**+HalfNormal** ...ok now what?

![images/b3cec8ca159c79e2fc1e54b3e63b99e2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3cec8ca159c79e2fc1e54b3e63b99e2.jpeg)


---
**HalfNormal** *March 22, 2018 19:57*

**+Don Kleinschnitz** you have left me speechless! 


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/4ystTbEYGas) &mdash; content and formatting may not be reliable*
