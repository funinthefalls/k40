---
layout: post
title: "**RESOLVED ** - Syntax errors (random.py was likely corrupted in the download) - Run with sudo if having issues initializing (USB error) Recently picked up an \"upgraded\" K40 online and ran into an interesting issue"
date: August 09, 2018 00:18
category: "Software"
author: "'Akai' Coit"
---
<b>**RESOLVED **</b>

- Syntax errors (random.py was likely corrupted in the download)

- Run with sudo if having issues initializing (USB error)



Recently picked up an "upgraded" K40 online and ran into an interesting issue. I couldn't get any of the stock software to function, so I did some research and found K40 whisperer. This software works great in comparison to the stock (which doesn't work at all). The issue I ran into is that I tried setting up K40 whisperer on my Linux box (where I do all my design work) and ran into a python error. I was wondering if anyone here uses K40 whisperer on Linux and could help me troubleshoot said issue. Posting the actual error in the comments.





**"'Akai' Coit"**

---
---
**'Akai' Coit** *August 09, 2018 00:19*

Traceback (most recent call last):

  File "./k40_whisperer.py", line 28, in <module>

    from svg_reader import SVG_READER

  File "/opt/k40/svg_reader.py", line 23, in <module>

    import tempfile, os, sys, shutil

  File "/usr/lib/python2.7/tempfile.py", line 35, in <module>

    from random import Random as _Random

  File "/usr/lib/python2.7/random.py", line 78

SyntaxError: Non-ASCII character '\xf0' in file /usr/lib/python2.7/random.py on line 79, but no encoding declared; see [python.org - PEP 263 -- Defining Python Source Code Encodings](http://python.org/dev/peps/pep-0263/) for details




---
**James Rivera** *August 09, 2018 20:21*

Just a hunch, but this sounds like the version of python may be mismatched.


---
**'Akai' Coit** *August 09, 2018 20:55*

Researching python mismatch issues, but what would you suggest as the best course of action? I have the newest python installed for my distro. Should I dead through the code to see if I spot anything, though I can't say I'm the best for that, I can at least try.


---
**Scorch Works** *August 09, 2018 20:57*

The error says that you have a "Non-ASCII character '\xf0' in file /usr/lib/python2.7/random.py on line 79"  If you get rid of that character in that file this error will go away.  But I assume you did not put that character there in the first place. My guess is that the error will just move to the next non-ASCII character that shouldn't be there.  I don't know how non-ASCII characters ended up in your python install.  You should probably try to remove and reinstall python. 


---
**'Akai' Coit** *August 09, 2018 21:45*

**+Scorch Works** Line 79 is (literally) the word "overlap" with a period at the end. It is part of a paragraph, I'll paste the whole section so what I'm saying makes sense. It was misspelled, so I initially figured that was the problem, but I fixed the spelling error and the issue didn't go away.





class Random(_random.Random):

    """Random number generator base class used by bound module functions.



    Used to instantiate instances of Random to get generators that don't

    share state.  Especially useful for multi-threaded programs, creating

    a different instance of Random for each thread, and using the jumpahead()

    method to ensure that uhe generated seðuences óeen by dach thrdad don'u

    overlap.           [LINE 79]



    Class Random can also be subclassed if you want to use a different bàsic

   ¡generatîr of your own devising: in that!case, override the following

    methods: random(), seed(), getstate(), setstatå() and êumpaheaä().

    Optionally, impmement a!getrandbits() method so that randrange() can cover

    arbitrarily large ranges.



    """




---
**Scorch Works** *August 09, 2018 21:58*

I see at least 2 non ASCII characters in this text "seðuences óeen by dach thrdad don'u"  do you see them "ð" and "ó".  As I guessed there is more than one non-ASCII character in the file.  (they are not on line 79 but they are reported on line 79 because the previous lines are grouped as part of the comment section.)  I don't know why some of the characters in your file seem to be randomly replaced by non-ASCII characters.  If I were you I would try reinstalling python. 


---
**'Akai' Coit** *August 09, 2018 22:27*

So, the issue has shifted. Maybe for the better or for the worse. So I have a newer version of python installed and I'm trying to fix a bunch of small syntax errors in a few of the .py files for k40 whisperer. I've run into a roadblock that I'm not sure how to fix. I'm trying to get it all to work with python 3(.6.6?) and I've come into this syntax error:



File "/opt/k40/bezmisc.py", line 58

    def bezierparameterize(bx0,by0)(bx1,by1)(bx2,by2)(bx3,by3):

                                                                       ^

It doesn't seem to like the parenthesis used here and I'm not sure how to edit this line to move forward... Any suggestions? I will edit those words on "line 79" for the original issue I posted about to see if that fixes that issue and lets me move forward. We shall see what happens.


---
**'Akai' Coit** *August 09, 2018 22:40*

Going to guess that [random.py](http://random.py) was corrupted (riddled with errors like that). So I pasted a clean copy of [random.py](http://random.py) directly from python in place and ALL the errors went away (found a LOT more after that section, and chose that solution after I fixed the 4th or 5th error). Which lead me to the same error I just posted above with [bezmisc.py](http://bezmisc.py) line 58. The original  version of that line was:



def bezierparameterize(((bx0,by0),(bx1,by1),(bx2,by2),(bx3,by3))):


---
**Scorch Works** *August 09, 2018 23:03*

You probably upgraded too much.  You need to stay with a Python version less than 3.0  (You need Python 2.7.xxx)


---
**'Akai' Coit** *August 09, 2018 23:22*

hmm... makes me wonder if I will end up breaking something else by downgrading back to 2.7.x... Will have to see what happens...


---
**'Akai' Coit** *August 10, 2018 00:00*

**+Scorch Works**, finally got past all those hurdles and got it to open. Now to get it to recognize the machine. I followed all the instructions, but it's saying "USB Error: Unable to set USB Device configuration." 



Reboot didn't help. Python is installed and all other necessary packages that I'm aware of. Group has been made (and I added myself to it). So I'm at a loss as to what else could be going on.


---
**Scorch Works** *August 10, 2018 01:00*

It is probably a USB access rights issue. Try running it using sudo.


---
**'Akai' Coit** *August 10, 2018 01:04*

HAHAHAHAAA!!! That was it. I don't know why I didn't consider that. I just got it to initiate... So I think we're good. Thank you for your help!


---
**Scorch Works** *August 11, 2018 15:28*

**+'Akai' Coit** I am glad you got it working.  I just realized that we met last year at your booth at the Madison Mini Maker Faire.  I enjoyed talking with you there.


---
**'Akai' Coit** *August 11, 2018 16:34*

Oh, that is priceless! What are the chances of that!? Well, now that I've got this hurdle out of the way, you may see a LOT more interesting things at my booth at the next Madison Mini Maker Faire. And it will be thanks to your work here! :) I have some interesting plans for working with (and modifying) the K40 I have, as well as getting another fairly soon. Thanks again! And I look forward to the next time we cross paths.


---
*Imported from [Google+](https://plus.google.com/110930401088149267232/posts/Zg3uQVA5dd4) &mdash; content and formatting may not be reliable*
