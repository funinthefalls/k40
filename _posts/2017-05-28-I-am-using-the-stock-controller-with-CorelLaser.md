---
layout: post
title: "I am using the stock controller with CorelLaser"
date: May 28, 2017 00:33
category: "Original software and hardware issues"
author: "Martin Dillon"
---
I am using the stock controller with CorelLaser.  While I was cutting, this error came up 3 times.  It cut all the pieces okay.  It would just pause, I would click retry and it would continue on.  Never shows up when etching.

Anyone else ever had this problem?

![images/d548a4b808b7e43d0e7bb3929734d403.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d548a4b808b7e43d0e7bb3929734d403.png)



**"Martin Dillon"**

---
---
**Ashley M. Kirchner [Norym]** *May 28, 2017 01:19*

It's buffering the data. How slow are you cutting? Basically the software is waiting for the laser controller to process what was sent to it and it times-out waiting.


---
**Martin Dillon** *May 28, 2017 03:20*

I was cutting at 5 mm/s


---
**Joe Alexander** *May 28, 2017 11:26*

are you using a quality usb cable with ferrite on both ends? a bad cable gave me issues for a while.


---
**Ashley M. Kirchner [Norym]** *May 28, 2017 15:26*

That was going to be my next suggestion, replace the USB cable. Shorter if you can. The one mine came with was crap. 


---
**Jeff Johnson** *May 30, 2017 14:55*

I get this error maybe once a month and clicking "Retry" always works for me. I don't know why it happens and since it happens so rarely I've never bothered to troubleshoot.




---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/2LP4zhwAJpR) &mdash; content and formatting may not be reliable*
