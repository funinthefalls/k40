---
layout: post
title: "Guys I cut almost nothing but 3mm hard board (mdf)"
date: November 08, 2017 15:39
category: "Hardware and Laser settings"
author: "Chris Hurley"
---
Guys I cut almost nothing but 3mm hard board (mdf). Any tips on improving cut time? I have forced air. I was thinking of upgrading to a 60w tube worth the cost? 





**"Chris Hurley"**

---
---
**greg greene** *November 08, 2017 17:21*

The 60W tubes have provided some benefit for some, but it won't help if your alignment is not spot on, and it doesn't give you a larger work area.  What times / power are you getting now?


---
**Chris Hurley** *November 08, 2017 18:44*

4 passes at 9mah/12.5ms


---
**Anthony Bolgar** *November 08, 2017 20:17*

Try 5mm/s at 9mA and you should be able to cut through in 1 pass if you have everything aligned properly, 2 passes at most.


---
**Tony Schelts** *November 11, 2017 18:19*

I have a 60watt clone that Updated to a true 60W. I mostly cut 3mm laser ply and acrylic. I cut 3mm ply at 65% 25mm a sec. 6MM ply at 65% 12mm a sec. Alignment makes a massive difference. 

 


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/eTPyUrmYtzD) &mdash; content and formatting may not be reliable*
