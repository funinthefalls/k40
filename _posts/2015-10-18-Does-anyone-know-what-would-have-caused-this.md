---
layout: post
title: "Does anyone know what would have caused this?"
date: October 18, 2015 09:31
category: "Hardware and Laser settings"
author: "Peter"
---
Does anyone know what would have caused this?

[https://drive.google.com/file/d/1JJeBa5Q4_8FJ5VloqNVIFbwc4l6O26b2yA/view?usp=sharing](https://drive.google.com/file/d/1JJeBa5Q4_8FJ5VloqNVIFbwc4l6O26b2yA/view?usp=sharing)

Melted innards of a potentiometer from my supposedly new machine.



How can I make sure the votages, in and out, are correct as I dont know what they shuld really be to start with and not even sure the wiring is correct because of this melt.





**"Peter"**

---
---
**Stephane Buisson** *October 18, 2015 09:42*

Hi **+Peter Jones** ,



the pot is wired between ground and +5v, in the middle is the return going to PSU in. (the goal of the pot being to limit the max current)

I did not check the amp. but the pot is rated 2W.



Poor quality pot seem to be your problem.



My actual Question is should I need to keep one for my smoothie conversion (as physical Max current and possible software error). 


---
**Gregory J Smith** *October 18, 2015 09:59*

To me it looks like it's  melted - and the wipers look overheated. If it was wired wrong with the 5v on the wiper then there would be a lot of current near the end of the range and melting may occur.


---
**Peter** *October 18, 2015 10:08*

Thanks **+Stephane Buisson** 

If i work off this picture as its the closest I can find (mine has one LED and no markings) - [http://www.lordtuber.com/IMG_6758.JPG](http://www.lordtuber.com/IMG_6758.JPG)



My Pin Left goes to GROUND

Pin Right goes to IN

Pin Middle goes to 5V



Assuming the picture is the same as my power supply, readings where 5V is marked on that same picture = 5V

IN = Variable based on a the pot turn

G = 3.8mV steady


---
**Peter** *October 18, 2015 10:09*

**+Gregory J Smith** The wipers do have a nice petrol effect colour to them.

It seems as though it is wired incorrectly and acting like an on/off switch rather than variable resistor. 


---
**Stephane Buisson** *October 18, 2015 17:49*

**+Peter Jones** yes it's from stock,

I did a  post few day ago before taking my K40 apart.

[https://plus.google.com/117750252531506832328/posts/5GARrrVPZUP](https://plus.google.com/117750252531506832328/posts/5GARrrVPZUP)



look the wires colors.


---
**Peter** *October 18, 2015 18:02*

Thanks again. I found these and even printed off the hand written sheet to try and make sense. The power supply is definitely different though. 

I have emailed the seller as I've come to the conclusion then power supply is faulty. Taking readings, I have a constant 4V or 1.666V to the pin 4 on the second block connector. 4V when powered on, 1.6V after I press the test fire button. Even with pot disconnected I think this should be 0V until test fire and set by the pot. 


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/i2cYhDxKyiX) &mdash; content and formatting may not be reliable*
