---
layout: post
title: "FreeCAD laser cut interlocking module"
date: June 29, 2018 15:22
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
FreeCAD laser cut interlocking module




{% include youtubePlayer.html id="YGFIdLpdWXE" %}
[https://www.youtube.com/watch?v=YGFIdLpdWXE](https://www.youtube.com/watch?v=YGFIdLpdWXE)





**"HalfNormal"**

---
---
**James Rivera** *June 29, 2018 17:36*

This looks awesome!! The export to flat alone will save me so much time!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Yyz7CQEzirS) &mdash; content and formatting may not be reliable*
