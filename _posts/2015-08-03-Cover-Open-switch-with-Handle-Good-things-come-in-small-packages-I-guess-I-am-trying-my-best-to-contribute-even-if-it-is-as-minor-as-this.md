---
layout: post
title: "\"Cover Open\" switch with Handle Good things come in small packages I guess, I am trying my best to contribute even if it is as minor as this!"
date: August 03, 2015 03:04
category: "Modification"
author: "Fadi Kahhaleh"
---
"Cover Open" switch with Handle

[http://www.thingiverse.com/thing:953025](http://www.thingiverse.com/thing:953025)



Good things come in small packages I guess, I am trying my best to contribute even if it is as minor as this! :)





**"Fadi Kahhaleh"**

---
---
**Joey Fitzpatrick** *August 03, 2015 17:48*

Thank Fadi  I made one this morning for my K40.  [https://drive.google.com/folderview?id=0B52LpMTsyXnIfnluOW51by1VYm42R0VIZW9ZeUxRU3RHM0ZMTl9rRTQzYXdVREN5bEdNU1k&usp=sharing](https://drive.google.com/folderview?id=0B52LpMTsyXnIfnluOW51by1VYm42R0VIZW9ZeUxRU3RHM0ZMTl9rRTQzYXdVREN5bEdNU1k&usp=sharing)


---
**Fadi Kahhaleh** *August 03, 2015 18:17*

That looks really good. Glad you found it useful.


---
*Imported from [Google+](https://plus.google.com/115068648817066933731/posts/VSTWCS3CMse) &mdash; content and formatting may not be reliable*
