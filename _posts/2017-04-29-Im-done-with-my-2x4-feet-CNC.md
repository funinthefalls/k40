---
layout: post
title: "I'm done with my 2x4 feet CNC"
date: April 29, 2017 19:54
category: "Object produced with laser"
author: "Cesar Tolentino"
---
I'm done with my 2x4 feet CNC. Most parts were cut by my 4 year old k40. It's so fun designing something and then prototyping it without waiting for hourslike 3d printing it. 



Next project is making a bigger laser cutter using EMT pipes as rails or square tubes. Let's see if that's acceptable.



![images/69fe066e6c26b4ee8bbb66d71c000d5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69fe066e6c26b4ee8bbb66d71c000d5b.jpeg)
![images/e16d7ffcd0f254519a05696fad8983cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e16d7ffcd0f254519a05696fad8983cd.jpeg)
![images/8fd1aea06911b89a2af8eec89b11ec7f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fd1aea06911b89a2af8eec89b11ec7f.jpeg)
![images/fa3e4f4768f2704050de2acc601f0556.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa3e4f4768f2704050de2acc601f0556.jpeg)

**"Cesar Tolentino"**

---
---
**Steve Clark** *May 01, 2017 23:08*

Fun project! But I gotta ask who else besides Cesar has what looks like carpet in his work shop? - grin


---
**Cesar Tolentino** *May 02, 2017 00:27*

Haha I have the liberty to work inside the house. Man cave haha.  I just need to make sure the vacuum system works really good. And this dust deputy works really well


---
**Kelly Burns** *May 02, 2017 20:13*

Very cool.  It's like "hardware store CNC" with a Laser Twist. 


---
**Jamie Valentin** *May 05, 2017 02:50*

Awesome, now I want to do this! 


---
*Imported from [Google+](https://plus.google.com/+CesarTolentino/posts/cfa9enuyrd5) &mdash; content and formatting may not be reliable*
