---
layout: post
title: "So I took a blind leap into the laser world, hope you guys can help me get it working"
date: October 31, 2016 16:35
category: "Smoothieboard Modification"
author: "Mark Leino"
---
So I took a blind leap into the laser world, hope you guys can help me get it working. I ordered the machine from ebay, should be here this week. I also order a smoothie board, middleman boards, and the digikey connectors. 



I'm having some difficulty finding "plain English" directions for setting up the middleman boards. I'm somewhat knowledgeable about electronics, but get lost when things get technical. I have done a fair amount of controller swapping from cnc's, so I understand the basics. 



Am i better off not using the middleman set up? Any other parts I would need to get a k40 smoothified?



Thanks, you guys are awesome.



Mark





**"Mark Leino"**

---
---
**Ariel Yahni (UniKpty)** *October 31, 2016 16:58*

Middleman use will depend on what you receive  as there are at least 2 different setup on cabling. I had also ordered them just in case B it  did not need to use them


---
**Anthony Bolgar** *October 31, 2016 18:05*

You might need  level shifter depending on how you wire it up.


---
**Mark Leino** *November 01, 2016 01:20*

had an amazon order, so i learned what a level shifter was and ordered one. Hope i didn't need 3


---
**Mark Leino** *November 02, 2016 22:23*

Well I ordered a level shifter, but I think it's the wrong one. Can anyone confirm this?

![images/ca8e64d133470d5984c00fc0b0ba5841.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca8e64d133470d5984c00fc0b0ba5841.jpeg)


---
**Anthony Bolgar** *November 02, 2016 22:27*

From what I could read it seemed right should be at least 1 channel 5V to 3.3V (Most are multichannel)


---
**Mark Leino** *November 02, 2016 22:30*

Huh, shows up perfectly clear on my screen. 4-channel 5v to 3.3,v for some reason I thought it had to do 3.3 to 5v. Probably doesn't work both ways


---
**Anthony Bolgar** *November 02, 2016 22:37*

Yup, is bidirectional you supply one side with 5V from the PSU, the other side gets the 3.3V from the smoothieboard. Then you put the smoothieboard signal line into the LV side and the opposing pin on the 5V side goes to the Laser PWM input on the powersupply. **+Alex Krause** posted some pictures and a short write up of how his is connected.


---
**Mark Leino** *November 04, 2016 05:20*

**+Anthony Bolgar** I really appreciate all your help. I just went through +Alex Krause posts for the last hour and couldn't find his write up! I was trying to get a message to him privately, but couldn't get any contact info. I know there is a few write ups hanging around, and I know you get asked this like a million times a day, so I thank you for your help. 



I'm just not quite finding all the info I need to get the smoothie [going.My](http://going.My) k40 didn't come with coreldraw, so I am not even going to mess around buying it or trying to download their pirated version. I'm not a complete stranger to wiring up controllers, but the whole laser thing is a whole new world to me. I have all the parts, just missing some info! I would be happy to do write up when I get finished with lots of pictures to help out others as well!



Thank you.


---
**Anthony Bolgar** *November 04, 2016 05:28*

I'll put together some links to the info on the weekend, have knee surgery in 7 hours. I have been planning on writing up a step by step illustrated guide to the conversion, should have that done by next weekend if you can wait that long. If you need to move quicker, just leave a public message in the k40 community (tag his name) asking for some help on the conversion, he will be happy to help.


---
**Ariel Yahni (UniKpty)** *November 04, 2016 05:32*

**+Mark Leino**​ here is the info for doing it via de level shifter [https://plus.google.com/109807573626040317972/posts/CXFk1n4oezp](https://plus.google.com/109807573626040317972/posts/CXFk1n4oezp)


---
**Anthony Bolgar** *November 04, 2016 05:37*

Thanks Ariel, that was the post I wanted him to see.


---
**Mark Leino** *November 04, 2016 05:38*

Thanks a lot guys


---
**Ariel Yahni (UniKpty)** *November 04, 2016 05:41*

There another option without the lever shifter here. [https://plus.google.com/113684285877323403487/posts/99hJJ1Vw32P](https://plus.google.com/113684285877323403487/posts/99hJJ1Vw32P)


---
**Ariel Yahni (UniKpty)** *November 04, 2016 05:43*

The story behind that one  is [https://plus.google.com/113684285877323403487/posts/a44Kq2Ccvto](https://plus.google.com/113684285877323403487/posts/a44Kq2Ccvto)


---
*Imported from [Google+](https://plus.google.com/+MarkLeino/posts/43cLxcaNYn5) &mdash; content and formatting may not be reliable*
