---
layout: post
title: "I was at the plastic shop yesterday to get some material and I noticed this sample"
date: September 14, 2018 17:50
category: "Object produced with laser"
author: "Don Kleinschnitz Jr."
---
I was at the plastic shop yesterday to get some material and I noticed this sample. Note that the tabs have a kerf at each edge, this creates a spring like fit and helps reduce the tolerance needed to get good tab fits.

I had never seen this technique before and thought I would share.

![images/675951516de42c1359016c1f6009aac7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/675951516de42c1359016c1f6009aac7.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Mike Meyer** *September 15, 2018 23:50*

Hmmm...I'm not exactly sure what I'm looking...Did they undercut those small shoulders to each of those tabs? And how did they do that?


---
**Don Kleinschnitz Jr.** *September 16, 2018 03:02*

They cut a kerf in each edge of the tab. They told me that it was part of and app (??) they bought.


---
**Don Kleinschnitz Jr.** *September 24, 2018 12:56*

#K40TabDesign


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/7xf6e7nDyMM) &mdash; content and formatting may not be reliable*
