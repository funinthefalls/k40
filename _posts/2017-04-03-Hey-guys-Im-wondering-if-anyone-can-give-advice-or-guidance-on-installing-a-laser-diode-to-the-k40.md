---
layout: post
title: "Hey guys, I'm wondering if anyone can give advice or guidance on installing a laser diode to the k40?"
date: April 03, 2017 12:37
category: "Discussion"
author: "Nathan Thomas"
---
Hey guys, 

I'm wondering if anyone can give advice or guidance on installing a laser diode to the k40?

Does anyone have it installed themselves?



I mostly cut and engrave thin metals and plastics so I rarely use the center cut hole (Spring adjustment). But I'd like to start doing thicker objects. Problem is there's no real guide if u want to something precisely. 



Just curious how others tackle this issue. Did u install a diode? Or just figure out the coordinates of the center where the spring holder is and move the laser head to that point on the software?



Thanks in advance!





**"Nathan Thomas"**

---
---
**Ned Hill** *April 03, 2017 15:25*

You can always make an origin jig.  Put a piece of plywood big enough to fit into the back left corner of the bed and extend through the home origin position.  Do two perpendicular cuts starting from the origin (0,0).  This will let you place the corner of your cutting piece at the 0,0 position each time.  Plan your cutting/etching in the software relative to the 0,0 position. (see piece of wood in the pic below)

 

Having a positioning laser is nice but not completely necessary.  Some people 3d print diode holders.  I got one off ebay that had a metal mount, just had to mill the clamping ring out a bit to get it to fit over my mirror mount.  I'm sure there are lots of instruction out there for mounting a diode.

![images/5358dea000971e22569d1863d7c291b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5358dea000971e22569d1863d7c291b7.jpeg)


---
**Paul de Groot** *April 04, 2017 02:00*

Make sure you have a laser diode electronics driver since the diode needs a specific voltage and current to operate correctly. 


---
**Nathan Thomas** *April 04, 2017 16:17*

**+Ned Hill** thanks for the pic 😊

Is that a custom table? Looks nothing like my stock table.


---
**Nathan Thomas** *April 04, 2017 16:18*

**+Paul de Groot** i was wondering about that, I was just going to hook it up to a separate battery if I went that route


---
**Nathan Thomas** *April 04, 2017 16:21*

I did something similar which works great for engraving in the top corner. But to use that center hole  (which i covered cause I wasn't planning on using it) I need either a diode or to figure the 0,0 for that point...i believe 

![images/79d3ee1843cd439db6d3b69759a17b73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/79d3ee1843cd439db6d3b69759a17b73.jpeg)


---
**Ned Hill** *April 04, 2017 16:23*

**+Nathan Thomas** yes the bed is custom.  I have some info on the bed here in my K40 collection.  

[plus.google.com - K40 Laser Engraver](https://plus.google.com/u/0/collection/I7D_VB)

There are 2 separate posts there that deal with height adjustment and an upgraded honeycomb bed. It's very basic.  Most people end up just removing the stock bed.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/dZYrynz6wxm) &mdash; content and formatting may not be reliable*
