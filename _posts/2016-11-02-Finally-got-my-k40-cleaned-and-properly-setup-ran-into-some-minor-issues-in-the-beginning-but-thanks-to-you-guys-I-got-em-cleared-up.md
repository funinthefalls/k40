---
layout: post
title: "Finally got my k40 cleaned and properly setup, ran into some minor issues in the beginning but thanks to you guys I got 'em cleared up"
date: November 02, 2016 02:34
category: "Object produced with laser"
author: "Jesse Veltrop"
---
Finally got my k40 cleaned and properly setup, ran into some minor issues in the beginning but thanks to you guys I got 'em cleared up.  Designed and cut this little 4 sided pyramid trinket just as a test, I'm pretty satisfied with how it turned out! :D

![images/0c3c3017e8f412cf090d14af64520611.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c3c3017e8f412cf090d14af64520611.jpeg)



**"Jesse Veltrop"**

---
---
**greg greene** *November 02, 2016 13:37*

Looks great - are you going to post the file in the repository section?


---
*Imported from [Google+](https://plus.google.com/114760861883962577037/posts/WK3EPsGg5o1) &mdash; content and formatting may not be reliable*
