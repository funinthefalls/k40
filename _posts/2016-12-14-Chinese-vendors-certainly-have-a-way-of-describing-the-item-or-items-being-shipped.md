---
layout: post
title: "Chinese vendors certainly have a way of describing the item (or items) being shipped"
date: December 14, 2016 05:34
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Chinese vendors certainly have a way of describing the item (or items) being shipped. The description here is "Daily Necessities" ... oh? My daily dose of coffee? Supplements? What? WHAT? Oh wait, 77.3mm? Now what "daily necessity" do I need that's 77.3mm long?? Can't be a stick to beat up misbehaving machines, it's a small and flat envelope ... OOOooooh! It's the 7x7 cable chain I ordered! Duh! Well I suppose the laser would call it a "daily necessity". :)



![images/74897e88a56c870cee0439cb249ddd94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74897e88a56c870cee0439cb249ddd94.jpeg)
![images/957e0d340f2a8521a3b9cfff4d602d9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/957e0d340f2a8521a3b9cfff4d602d9e.jpeg)

**"Ashley M. Kirchner [Norym]"**

---
---
**Ariel Yahni (UniKpty)** *December 14, 2016 11:40*

That's just plain crazy. In many countrys that could lead to having the package confiscated by the police


---
**HalfNormal** *December 14, 2016 13:03*

Could be to bypass customs quickly. If it were "parts" it might be subject to taxes and customs.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2016 14:39*

That's quite funny. I'm sure we all need cable chains on a daily basis.


---
**Ashley M. Kirchner [Norym]** *December 14, 2016 16:39*

**+HalfNormal**, it's obviously done to skirt customs and inspections on both ends. I've had packages get held up by China Post as well because the description didn't match the contents (I get pretty color decorated tape when they re-seal the package, not like the boring one that US Customs will put on). With the constant flow of items I get delivered, it's always fun to read what they write on.


---
**Ashley M. Kirchner [Norym]** *December 14, 2016 17:33*

**+Ariel Yahni**, I don't know that I'd ever had something get confiscated, but I certainly have had stuff sit at customs (at either end) for an extended time, followed by many e-mails back and forth with the vendor. The first time I ordered 5 reels of SMD LEDs, about 5 or 6 years ago, they sat at the Chinese point of export because the reels were vacuumed sealed and the shape of the reels didn't match the shape of an individual LED, or so the customs agent told me. The description was '5.0mm light units' with a 10k count. The vendor had to send an agent to the customs office and open all the reels infront of them, which of course they couldn't seal back up properly so by the time I received the shipment, the LEDs had absorbed too much moisture. I had to bake them overnight before I could use them. Nowadays they're better educated and know what the reels look like.


---
**Madyn3D CNC, LLC** *December 16, 2016 15:29*

This is how China gets away with import and tax duties. If other countries tried this, it would effect their entire trade deals with the US,  and possible sanctions would be considered. But, for whatever reason, China is allowed to get away with it, thus gaining control of a majority of the US economy.



 This, among other similar loopholes in trade agreements with China that affects us as makers more than you'd think, was a great deal of influence on who I voted for this year ;) 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/7BrNGs4UHaB) &mdash; content and formatting may not be reliable*
