---
layout: post
title: "After about 150 hours of cutting my tube is going die, 18mA, (100mm/min) on meter and I`m not able to cut any ply wood any more"
date: May 27, 2017 20:56
category: "Hardware and Laser settings"
author: "Damian Trejtowicz"
---
After about 150 hours of cutting my tube is going die, 18mA, (100mm/min) on meter and I`m not able to cut any ply wood any more. i can only cut acrylic sheet  3mm with 150mm/min.

Done all alignment, clean up mirrors, lens and no succes

I notice discharge is not strong like was before.

Any one have simmilar problem? 





**"Damian Trejtowicz"**

---
---
**Phillip Conroy** *May 27, 2017 22:02*

How old is the tube


---
**Ashley M. Kirchner [Norym]** *May 27, 2017 22:55*

At 18mA you're overdriving the tube and shortening its lifespan. 16mA max. Personally I never go past 13-14mA. I cut 1/8" wood at 10-11mA 300mm/min easy.


---
**Damian Trejtowicz** *May 27, 2017 23:05*

Two years now


---
**Phillip Conroy** *May 28, 2017 03:30*

Laser tubes have a limited shelf life  and Leak their gasses out with time,my last tube made it to 600 hours and 18 months before losing power over 3 days,somebody needs to collect data on life spans of China tubes to see the average time etc that they die at


---
**Damian Trejtowicz** *May 28, 2017 06:39*

This is what i suspect,gas just gone


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/hDtkUQaGxFV) &mdash; content and formatting may not be reliable*
