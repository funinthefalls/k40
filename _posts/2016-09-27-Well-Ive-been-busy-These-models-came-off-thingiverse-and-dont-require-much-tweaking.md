---
layout: post
title: "Well I've been busy. These models came off thingiverse and don't require much tweaking"
date: September 27, 2016 19:22
category: "Object produced with laser"
author: "Jeff Johnson"
---
Well I've been busy. These models came off thingiverse and don't require much tweaking. Mostly just adding texture and changing the layout to fit 300x200mm sheets of plywood and MDF. The bigger the better! I really enjoy assembling these.



![images/18a5ea06dfb67010cb4d65f319249b73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/18a5ea06dfb67010cb4d65f319249b73.jpeg)
![images/6c6033620b311f685812afcb5fe9421e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c6033620b311f685812afcb5fe9421e.jpeg)
![images/be5060c3bfb66de8da4d6e6e8f49cbfd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be5060c3bfb66de8da4d6e6e8f49cbfd.jpeg)

**"Jeff Johnson"**

---
---
**greg greene** *September 27, 2016 20:44*

Are those the droids your looking for ?


---
**Gunnar Stefansson** *September 27, 2016 21:49*

Looks good. Like the extra details you added


---
**Josh Rhodes** *September 27, 2016 22:10*

My nephew would LOVE this, I should get busy too #christmas


---
**Kris Sturgess** *September 28, 2016 04:05*

Interested in sharing your files?




---
**Jeff Johnson** *September 28, 2016 14:26*

This is a link to the large Tie Fighter.

[drive.google.com - tiefighter.large.zip - Google Drive](https://drive.google.com/file/d/0B751t-yy-x-ncm5EQ2F5NVlHYnc/view?usp=sharing)


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/GRyaJXD2MWP) &mdash; content and formatting may not be reliable*
