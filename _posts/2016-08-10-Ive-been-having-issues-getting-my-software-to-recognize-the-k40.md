---
layout: post
title: "I've been having issues getting my software to recognize the k40"
date: August 10, 2016 19:07
category: "Original software and hardware issues"
author: "Jack Sivak"
---
I've been having issues getting my software to recognize the k40. After going back and forth with "tech support" it doesn't seem like they have any answers. Has anyone else had issues with the stock software that they were able to resolve? Are there any simple guides online about setting it up? From everything I've seen CorelDraw/laserDRW should just recognize the laser, but in my case it never does. Are these sorts of issues resolvable or am I just better off returning it at this point?





**"Jack Sivak"**

---
---
**David Spencer** *August 10, 2016 19:58*

Probably dumb question but, did you plug in the little USB thing?


---
**Jim Hatch** *August 10, 2016 20:02*

Start with LaserDrw. Once you get that working you can see about Corel.



First turn on the laser, make sure the USB dongle is plugged into your PC and then plug the PC into the K40. Fire up LaserDrw and watch the splash screen - it should say it's authenticating. If it fails that step it's not going to talk to the labor. That's the step that checks the USB key.



If authentication is successful then go to the settings page and set the Model and Device ID fields to match the board in your K40. The Model is the entry with -M2 in it (there's only one) if you've got a Nano board in the laser. The Device ID field is screen printed on a white label area on the Nano control board and is Hexadecimal (letters & numbers). I think it's either 13 or 16 characters but don't recall exactly and am not at my machine.



Once you do that, draw a box in LaserDrw and then click on either Engrave or Cut to send it to the laser. Click "Starting" in the popup and see if it works.



Report back here and we'll take it from there.


---
**Jack Sivak** *August 10, 2016 20:11*

**+David Spencer** Yes, it gives you a warning if the USB dongle isn't plugged in.



**+Jim Hatch** I've been trying LaserDRW, but it doesn't seem to work. The splash screen is all in Chinese, and the authentication doesn't fail on startup if the dongle isn't plugged in - it fails when I attempt to engrave ("This operation is not supported" or something). Anyway, I've tried it with everything plugged in, and I set the proper Model that I got from the control board (it is an M2 model). I haven't set the Device ID though - so I can try that when I get home. Aside from that, however, I've tired everything else - create a box (a few boxes and some filled objects, just in case) and did Engrave > Starting. it pauses for a few seconds, then nothing happens.



I'll try setting the Device ID and reporting back - here's hoping it's as simple as that!


---
**Jim Hatch** *August 10, 2016 20:22*

Mostly good news :-) It's recognizing the dongle which is killer if you had a bad one - then you're stuck trying to get a replacement. I'd bet on the Device ID fixing things :-)


---
**Jack Sivak** *August 11, 2016 02:58*

Unfortunately changing the Device ID didn't work. After I press "starting" the program still has a 1 second pause where it's thinking (the little spinning icon), and then nothing happens, same as before.



I uploaded a picture of my control board and settings to [http://imgur.com/a/7EZh8](http://imgur.com/a/7EZh8) in case I missed something, but I think I got both parts right. Any idea what to try next?


---
**Jim Hatch** *August 11, 2016 11:45*

Can you grab a screen cap of what the pop-up cut/engrave window looks like and the LaserDRW screen of what you're trying to engrave?


---
**Jack Sivak** *August 11, 2016 16:56*

Sorry about the low quality - I'm pulling the images from my Ebay messages, but they should still be legible.



Here's the vector I'm trying to engrave: [http://imgur.com/3I64PDh](http://imgur.com/3I64PDh)



Here's the engrave screen: [http://imgur.com/ppwvT24](http://imgur.com/ppwvT24)


---
**Jim Hatch** *August 11, 2016 17:08*

What are those 2 gray diagonal lines behind the letters?


---
**Jack Sivak** *August 11, 2016 22:41*

Just two random lines I added, in case the filled shape didn't have borders or something. The CADB things is just a default shape in LaserDRW.


---
**Jim Hatch** *August 11, 2016 22:58*

Start with a new one drawing. Click the text icon and place some text ("Test" is good) and then send it as an engrave.


---
**Jack Sivak** *August 12, 2016 02:33*

Ok, a solid "my bad" on this one! Your suggestion didn't work, but I noticed one of the cables from the power to the board wasn't plugged in. Plugging that in makes everything work as expected.



Sorry about that! And horray that it actually works and I didn't get a dud!


---
**Jim Hatch** *August 12, 2016 02:52*

Yay. Never give up :-)


---
**Rick Tennyson** *August 16, 2016 19:59*

What did you come up with?


---
**Jack Sivak** *August 16, 2016 20:23*

My solution was double-checking that all the cables were plugged in. I wasn't aware that the x and y axes should zero when the machine is started up (on my machine nothing happened when I turned it on) which would have hinted at there not being power or signal somewhere. After fixing that the software worked as expected and everything runs smoothly. So in my case it was a hardware issue, not a software issue.


---
*Imported from [Google+](https://plus.google.com/106960809851059243398/posts/QGbmBMJCT8b) &mdash; content and formatting may not be reliable*
