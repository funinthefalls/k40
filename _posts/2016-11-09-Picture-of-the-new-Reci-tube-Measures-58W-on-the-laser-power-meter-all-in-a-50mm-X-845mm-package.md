---
layout: post
title: "Picture of the new Reci tube! Measures 58W on the laser power meter, all in a 50mm X 845mm package"
date: November 09, 2016 19:43
category: "Modification"
author: "Anthony Bolgar"
---
Picture of the new Reci tube!

 Measures 58W on the laser power meter, all in a 50mm X 845mm package. Reci makes some of the nicest tubes out there.

![images/63d1ba5981d9e84c6f9af40ce5cc096c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/63d1ba5981d9e84c6f9af40ce5cc096c.jpeg)



**"Anthony Bolgar"**

---
---
**Scott Thorne** *November 10, 2016 13:07*

Beautiful...i miss her already...lol


---
**Jeremie Francois** *November 10, 2016 21:04*

Hey, a laser made with a bottle of wine :)


---
**Beau H** *November 21, 2016 15:21*

**+Anthony Bolgar**​ where did you get this tube from, I am having a hard time finding a reci laser tube that is that length and power level. This tube would be about perfect for the freeburn2 build I am working on, it would only require a couple inch bump out on the side of the machine to support this tube length. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/HJ1N7Mv9BAs) &mdash; content and formatting may not be reliable*
