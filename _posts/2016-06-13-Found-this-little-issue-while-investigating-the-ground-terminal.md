---
layout: post
title: "Found this little issue while investigating the ground terminal"
date: June 13, 2016 16:12
category: "Discussion"
author: "Bruce Garoutte"
---
Found this little issue while investigating the ground terminal. The ground wire was run over the top of the hot wires to the external power sockets. Not liking the potential safety hazard with that, I re-routed it under the wires instead. That's one less thing to have to worry about.



![images/97a708978d67ac6bd4cedd1b0f92a83b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97a708978d67ac6bd4cedd1b0f92a83b.jpeg)
![images/9b8ef07579196386a82870d9a0a4d053.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b8ef07579196386a82870d9a0a4d053.jpeg)

**"Bruce Garoutte"**

---
---
**Anthony Bolgar** *June 13, 2016 18:03*

Don't you just love the colour coding conventions they use?...lol!


---
**Vince Lee** *June 14, 2016 00:25*

How's the electrical connection between the ground terminal to the case?.  It looks painted like mine was.  I ground off the paint around the terminal to make sure it was solid.


---
**Bruce Garoutte** *June 14, 2016 01:40*

I get full continuity between the external ground plug and the grounding post. I would venture a guess that the ground post is making contact to the bare metal inside the hole. All is well by me.


---
**Vince Lee** *June 17, 2016 02:29*

Yeah still seems a bit dodgy to me.  I preferred making a connection where there was direct pressure holding the parts together with a lot of surface area.  Kinda wanted something that was sure to take a lot of current since my life could depend on it.


---
*Imported from [Google+](https://plus.google.com/101697686569322649899/posts/EBufaNU4PtF) &mdash; content and formatting may not be reliable*
