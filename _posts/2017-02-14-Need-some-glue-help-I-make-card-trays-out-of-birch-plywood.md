---
layout: post
title: "Need some glue help. I make card trays out of birch plywood"
date: February 14, 2017 21:57
category: "Discussion"
author: "3D Laser"
---
Need some glue help.  I make card trays out of birch plywood.  I have been using spray glue to Adhere the two layers together however I do get over spray of globes in a few areas making it less than ideal.  Any suggestions on the best way to glue the layers together.  And do it ina way that doesn't takes just a minute or two

![images/926d4848fbbadc986d1ae77cf4fb0c76.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/926d4848fbbadc986d1ae77cf4fb0c76.jpeg)



**"3D Laser"**

---
---
**Roberto Fernandez** *February 14, 2017 22:14*

The cyanoacrylate dries in seconds.

The poliuretano dries in minutes.

The hotmelt 160-180°C glue in seconds.

In none of the above is easy to take off.


---
**Ned Hill** *February 14, 2017 22:25*

Something like this maybe? [amazon.com - Amazon.com: Dap 25336 Weldwood Non-Flammable Contact Cement, Gallon: Home Improvement](https://www.amazon.com/25336-Weldwood-Non-Flammable-Contact-Cement/dp/B0002YVBDO/)

Apply with a small roller or chip brush.


---
**Don Kleinschnitz Jr.** *February 14, 2017 22:27*

Use super glue with activator. Spray one side with activator and one side with the glue. When they mate they are bonded. 


---
**Roberto Fernandez** *February 14, 2017 22:39*

**+Ned Hill**​​ this dap glue has toluene. Therefore it is necessary to work in a ventilated area

**+Don Kleinschnitz**​​​ don't need the activator. Súper glue is an cianocrilate and dries with the air and wood humidity. Water spray can act as an activator.




---
**Ned Hill** *February 14, 2017 22:53*

**+Roberto Fernandez** that dap cement  I link to is the low VOC version.  I know the regular version needs lots of ventilation but I was thinking that this version would need less.


---
**Roberto Fernandez** *February 14, 2017 22:56*

**+Ned Hill** 

![images/98f75dab657a9a53a63a484a327940c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98f75dab657a9a53a63a484a327940c5.jpeg)


---
**Roberto Fernandez** *February 14, 2017 22:58*

Read warnings.


---
**Don Kleinschnitz Jr.** *February 14, 2017 23:07*

**+Roberto Fernandez** activator speeds it up, water can create bubbles :) used them all ....


---
**greg greene** *February 14, 2017 23:12*

white glue can be applied with a paint brush


---
**Jeff** *February 15, 2017 05:18*

Have you tried 3M Super 77 spray adhesive?  I would spray the back of the top cutout layer and then flip it over and place it on the bottom.  You might want to make a jig to align the top and bottom.  Super 77 sets really fast so you pretty much only get over go at it.  I used it for layered vaneer bookmarks and it worked great without warping.


---
**3D Laser** *February 15, 2017 12:50*

**+Jeff Watkins** that is what I have used in the past but the web spay is a pain.  I have been spraying it on paper and then "stamping". The top part into it and then gluing it to the bottom but after a few set ups it gets really messy and globs of glue are everywhere


---
**Don Kleinschnitz Jr.** *February 15, 2017 14:52*

**+Corey Budwine** looking at it again I think that white glue **+greg greene**  (possibly thinned with water) applied with a roller on the back of the frame would work well. Cleans up easy with water.

If you are doing a lot of these, glue the cutouts to a base board in reverse order, flip the frame over on top of them and then spray, roll or brush glue. The cutouts keep the glue from getting where you don't want it....maybe :)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/Zdt12QjpfDr) &mdash; content and formatting may not be reliable*
