---
layout: post
title: "Power Supply Issue... I've recently got a K3020 cutter (k40 clone) and with less than three days under my belt the PSU blew, big bang both the inlet and PSU fuse blew in spectacular fashion, blacken broken glass etc"
date: July 04, 2016 09:02
category: "Original software and hardware issues"
author: "Peter Gisby"
---
Power Supply Issue...

I've recently got a K3020 cutter (k40 clone) and with less than three days under my belt the PSU blew, big bang both the inlet and PSU fuse blew in spectacular fashion, blacken broken glass etc. All of the power lights went out and the machine was dead.... I replaced the fuse in the inlet and now the neon in the power switch and the case cooling fan comes on, still nothing from the supply. So I replaced the fuse in the supply, blew on power on, and keeps blowing even when nothing is connected other than the input lead... I'm therefore guessing there is something wrong with the power supply.

I've been having an email discussion with the supplier who insists on having me purchase a replacement PSU and they will then refund me the costs.. 

Firstly is this normal when dealing with the guys who sell this type of cutter? and secondly what is the best type of PSU to get as I understand, after reading some of the articles here, that the one supplied is not the best for this device.



I would be pushing for a full refund if I was intending to keep the cutter standard but I'm likely to change the controller, having a mac I'd like to drive it natively and not need Parallels, so this is just an early PSU update..



What is the advise of the group??



Thanks.





**"Peter Gisby"**

---
---
**Don Kleinschnitz Jr.** *July 04, 2016 10:25*

I haven't bought a laser PS yet, seems like it a common failure though. Here is the place that I have been going for my  LPS research don't know if their supplies are the best. I think they may be the source of the the stock supplies. Others probably have experience in repurchase.

[http://www.laserpsu.com/index.html](http://www.laserpsu.com/index.html).



Also I would look at:

[http://www.lightobject.com/Laser-Power-Supply-C15.aspx](http://www.lightobject.com/Laser-Power-Supply-C15.aspx)



I figured if mine ever went I would spend the extra to get one that would handle 80W in case I wanted to upgrade my machine. You can modify the enclosure and mount it sticking out the side. 

---------------

Not to be opportunistic of your bad fortune, but If they don't make you send the supply back and you are near me I am looking for a dead supply to pull apart to better understand the input circuit for the group. 



[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface_86.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface_86.html)










---
**Scott Marshall** *July 04, 2016 13:13*

If you're a electronically inclined individual, I may have a cheap fix for you.



The kind of "hard blow" you describe on the fuse is generally caused by the line side rectifier being shorted.



Follow the output side of the fuse, and it will lead you to a diode or fullwave bridge (diode array). The diode is usually a black cylindrical 2 leaded device with a band on 1 end, and the bridge is most often a rectangular device, black, and having 4 leads (2 for ac in, and + and - output)



Check with a meter set on ohms X 1 or diode check mode, the diode should pass current on only 1 direction. If it conducts both directions, it's bad, if you have a bridge, the same test applies, but you have 4 diodes to test. Google a 'Bridge Rectifier" and you'll see the internal diagram. test each diode, looking for any that are shorted.



If you find a bad device, both are replaceable with generic devices. Look on the nameplate and find out what the input current of the supply is, and get a diode or bridge larger then that rating, and just get one rated at 600V, that will assure you it will be adequate. (many times you can find the info or a part number on the device, but not always. The part should cost under $5.



Typical rectifier ratings will be 5A @ 600V. for a 120V  500W power supply (the size range for a 40W laser)



Good luck.



Scott


---
**Peter Gisby** *July 04, 2016 18:28*

Scott, you genius.... the rectifier in the PSU was rated 3A and short circuit across the AC in, the one I was able to replace it with is rated 6A and all of the fuses are rated 5A... This is probably why the original rectifier died before the fuse blew.  



All is now working...



Thanks again..

Peter.


---
**Scott Marshall** *July 05, 2016 01:34*

Learned that one repairing TVs back in the late 70s. I worked at a small TV shop as a teenager thru college. Every time there was a lightning storm we'd do 8 or 10 "rectifier jobs"  over the next few days. The telltale sign was the black fuse.



Glad you got it fixed.



Scott


---
**Don Kleinschnitz Jr.** *July 05, 2016 11:53*

**+Scott Marshall** very nice catch .....


---
*Imported from [Google+](https://plus.google.com/111055748369814573959/posts/jThGq47RHhF) &mdash; content and formatting may not be reliable*
