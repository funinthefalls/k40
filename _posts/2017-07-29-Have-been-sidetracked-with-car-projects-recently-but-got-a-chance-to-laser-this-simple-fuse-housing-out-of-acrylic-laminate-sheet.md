---
layout: post
title: "Have been sidetracked with car projects recently, but got a chance to laser this simple fuse housing out of acrylic laminate sheet"
date: July 29, 2017 00:18
category: "Object produced with laser"
author: "Vince Lee"
---
Have been sidetracked with car projects recently, but got a chance to laser this simple fuse housing out of acrylic laminate sheet.  Love how having a laser helps step up even simple projects to the next level.

![images/465d63c6bbc7ac3c6c82e87053a9a4e7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/465d63c6bbc7ac3c6c82e87053a9a4e7.jpeg)



**"Vince Lee"**

---
---
**Anthony Bolgar** *July 29, 2017 01:04*

Very nice!


---
**nayeem khatib** *July 29, 2017 03:14*

Wow neat job 


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/ihVzjtGPPDe) &mdash; content and formatting may not be reliable*
