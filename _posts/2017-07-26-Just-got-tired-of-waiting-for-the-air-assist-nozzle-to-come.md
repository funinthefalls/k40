---
layout: post
title: "Just got tired of waiting for the air assist nozzle to come"
date: July 26, 2017 09:26
category: "Modification"
author: "Tadas A\u0161"
---
Just got tired of waiting for the air assist nozzle to come. I thought why don't I use a few rubberbands and a spring to use the damn thing allready. Has anybody else done this?



 I got this machine a two weeks ago from aliexpress and still tinkering with it. Put in a led strip for lighting, bought a 30×20cm honeycomb bed, which in my oppinion is a bit too small and ordered parts for air assist.  When I got it the fume extraction propeller thingy was faulty, so I bought a new one, cut down the vent inside a few cm, but everything else was quite well made. That was surprising for me to be honest, because I paid 350€ for it (with shipping, shipped in 6 days to eastern europe) and the despute for the extractor thingy is still processed (I was surprised that ali suggested 102€ back for that plastic crap, the seller hasn't responded yet so I don't have high hopes for the suggested 100)



Sorry for my english (not my native language) 

![images/36bf1819273a36a219ed11ca43266814.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/36bf1819273a36a219ed11ca43266814.jpeg)



**"Tadas A\u0161"**

---
---
**Imnama** *July 26, 2017 12:47*

One of the functions of the air nozzle is  to prevent the smoke getting on you lens. Not sure that works in this configuration.


---
**Tadas Aš** *July 26, 2017 12:48*

Well at least it should blow out any flare-ups.  Right? 


---
**Imnama** *July 26, 2017 12:53*

Yes, that sould work




---
**Don Kleinschnitz Jr.** *July 26, 2017 13:27*

I did something like that when I first started cutting with my stock machine and it worked somewhat. Did not last because the coil-y cord and wire to my led finder kept binding the gantry and the air beam was not stable on the cut.

A more focused air stream and stable gantry connection (drag chain) helped my cutting quality and reliability immensely.  


---
**HP Persson** *July 26, 2017 19:38*

I actually did some massive testing on this, there is a lot of advantages throwing away the nozzle.

You can optimize it a bit more with a thin nozzle for pumping foot balls and similar, will speed up the flow a bit.



When it comes to air assist, its not more important how the air get to the cut, than how much air you throw at it.



I wrote some more about it here. As a side note, my tests showed almost identical results between a nozzle and a pipe. The assist nozzle brings a lot of problems with it though.



More reading, my opinion ;)

[k40laser.se - Air assist - K40 Laser](https://k40laser.se/air-assist/)


---
**Dennis Luinstra** *July 26, 2017 20:57*

Nice idea HP! Currently I have an LO air assist and will try your solution next weekend. 


---
*Imported from [Google+](https://plus.google.com/105123664639790704570/posts/LgwuE35W757) &mdash; content and formatting may not be reliable*
