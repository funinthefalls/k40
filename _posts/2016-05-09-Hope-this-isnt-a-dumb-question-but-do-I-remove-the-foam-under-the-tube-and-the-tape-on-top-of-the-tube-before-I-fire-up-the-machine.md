---
layout: post
title: "Hope this isn't a dumb question but do I remove the foam under the tube and the tape on top of the tube before I fire up the machine?"
date: May 09, 2016 04:31
category: "Hardware and Laser settings"
author: "Alex Krause"
---
Hope this isn't a dumb question but do I remove the foam under the tube and the tape on top of the tube before I fire up the machine?

![images/7e334f06832b9e08f39c5eea84fc671a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e334f06832b9e08f39c5eea84fc671a.jpeg)



**"Alex Krause"**

---
---
**Alex Krause** *May 09, 2016 05:00*

**+Anthony Bolgar**​ **+Yuusuf Sallahuddin**​ 


---
**Gee Willikers** *May 09, 2016 05:03*

You can. Won't hurt anything being there.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 05:35*

Yeah don't leave the foam in there. It's just for packing purposes & I recall the paperwork that came with mine said remove it. Not sure about tape on top of tube. Most tubes have a sticker on them with some sort of tube specs which is safe to leave on it.


---
**Alex Krause** *May 09, 2016 05:41*

Another question is water only suppose to flow thru the inner tube? I have no water in between the smaller tube and the outer tube


---
**Gee Willikers** *May 09, 2016 05:52*

Correct. Water in the water jacket only. Make sure there are no air bubbles. None.


---
**Alex Krause** *May 09, 2016 05:54*

**+Gee Willikers**​ good to know... I have rotated the tube while the pump is running and all other electronics off to purge all air bubbles I will let the pump run for 24 hours to purge any entrapped micro bubbles 


---
**Alex Krause** *May 09, 2016 05:56*

First matter of business tomorrow after work... Cut some sharks... With a fricken laser beam :)


---
**Alex Krause** *May 09, 2016 05:58*

Can someone tell me what the purpose of the coiled pirex tube is?


---
**Gee Willikers** *May 09, 2016 06:04*

Google says gas return tube. I'm assuming its coiled for added length/longer gas cooling time.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 06:15*

Sharks with lasers. Nice.



A point to remember for the water flow is to place your water tank higher than the tube, that way gravity assists in keeping bubbles out of the tube when you have the pump turned off.


---
**Gee Willikers** *May 09, 2016 06:26*

Since you've moved the tube itself, expect the alignment to be off.


---
**HalfNormal** *May 09, 2016 12:41*

**+Alex Krause**  Gee Willikers is correct, if you have been "rotating the tube", you will need to do a full mirror alignment. Read all the information on doing it first. Yuusuf posted a great paper on how to do it. It can takes hours to get it just right so do not get discouraged.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 17:09*

**+HalfNormal** I did? I don't recall posting much about mirror alignment. Maybe I post too much that I don't even remember.



But, there is a great guide for mirror alignment floating around here somewhere on the main page. I believe it is stickied up the top.


---
**HalfNormal** *May 09, 2016 17:14*

**+Yuusuf Sallahuddin** you are correct. It was **+Scott Marshall**. My mistake.


---
**andy creighton** *May 10, 2016 19:09*

I also had to move my tube away from the lens several mm in order to get everything to focus properly. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/9b6apDKSe69) &mdash; content and formatting may not be reliable*
