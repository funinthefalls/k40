---
layout: post
title: "I need a help any recommendation of good air assist for this kind of laser head?"
date: August 13, 2016 07:39
category: "Modification"
author: "Travel Rocket"
---
I need a help any recommendation of good air assist for this kind of laser head? Im new to this :( This cant cut 3mm wood all it can do is make a soot out of the wood. Thanks!



![images/cba5a48d347ea9421ffcf5b7dd99a4d9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cba5a48d347ea9421ffcf5b7dd99a4d9.jpeg)
![images/af2a8e5312ce0ecb155609ecbd5def68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/af2a8e5312ce0ecb155609ecbd5def68.jpeg)

**"Travel Rocket"**

---
---
**Bart Libert** *August 13, 2016 08:20*

Wow that looks like a dangerous head


---
**Phillip Conroy** *August 13, 2016 11:16*

Have not seen air assist for these open laser hdad types,you could try just a hose pointing down fron the side ov the laser head ,hoever as air will not be hitting work at 90 degrease may move work peace around


---
**Jim Hatch** *August 13, 2016 11:47*

Check your lens orientation (bump side up) and focal distance. That and good alignment is the key for good cutting.


---
**Ulf Stahmer** *August 13, 2016 11:52*

I have one of these, but have not added an air assist.  Regarding your comment about cutting, your alignment seems to be off.  I can cut 3 mm MDF and pine in one pass without difficulty.  The open head design makes cleaning the lens and head mirror easy, but it does make alignment of the second mirror (the one on the left hand side of the gantry) more difficult.  But the open head allows me to check my final alignment directly at the lens!  I checked in all 4 corners and was amazed how the laser position varied on the lens, so I had to go back and refine my alignment.


---
**Travel Rocket** *August 13, 2016 14:57*

**+Phillip Conroy** any tips on using air assist materials for this? Just the basic please


---
**Travel Rocket** *August 13, 2016 14:58*

**+Jim Hatch**  how will i know if the lens alignment is ok? Hope you give me a quick advice on this stuff, im new to this. Thanks and Cheers!


---
**Travel Rocket** *August 13, 2016 14:59*

**+Ulf Stahmer**  hi! Thanks for that. Is it possible to guide me on this process? can you do a quick sketch for the alignment of this open head? Thanks!


---
**Jim Hatch** *August 13, 2016 15:21*

Good instructions here: [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



Having an open head isn't different than a closed tube.


---
**Ulf Stahmer** *August 14, 2016 01:49*

**+Jim Hatch**, you're absolutely right, but the open head has no vertical surface to attach masking tape to for alignment.  It is quite tricky to view and compare any alignment marks if the masking tape is placed on the head mirror as it must be placed on the bottom surface.  Other than this issue, the alignment technique is identical.



It is likely also a good idea to make sure that the laser tube and the gantry are parallel in both vertical and horizontal directions.  If they're not, alignment will be challenging.


---
*Imported from [Google+](https://plus.google.com/100865688973733022249/posts/grAj24L9hz6) &mdash; content and formatting may not be reliable*
