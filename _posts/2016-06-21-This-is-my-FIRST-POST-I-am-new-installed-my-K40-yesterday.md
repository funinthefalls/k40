---
layout: post
title: "This is my FIRST POST ! I am new, installed my K40 yesterday"
date: June 21, 2016 17:28
category: "Software"
author: "Denis Bastien"
---
This is my FIRST POST !  I am new, installed my K40 yesterday.  Managed to cut some squares out of printer paper stock.  Some burned, some not.  Playing with settings, speed, etc.  My ultimate goal for now is to cut FABRIC based on shapes I have in JPG FILES.  Yesterday after reading some posts here I discovered that my board info was wrong.  I fixed that.  Now I would like to know if the info on this screen needs to be changed. Especially the top two fields containing unreadable information.



Thanks

Denis Bastien, Ottawa, Canada



![images/f66a720edc75a10e30de082186e35f84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f66a720edc75a10e30de082186e35f84.jpeg)



**"Denis Bastien"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 21, 2016 19:28*

Those top two fields containing unreadable information are nothing to worry about.They mean nothing & do nothing. I changed their values to whatever I felt like putting in there in the .ini file for the machine, but they are not necessary at all.



For fabric, I cut polar fleece with 4mA on the power meter (if you have % gauge instead I have no idea what it would correspond to) & speeds of 25mm/s. I cut thin denim with 8% elastane (if I recall correct) at same settings, but requires me to repeat the cut once (2 cuts). That is just an idea of where to start with your settings for fabric cutting.



Up top where you have 50000.00 mm/s you should change that to a more reasonable number. For cutting, I suggest never going above 30mm/s (especially for complex shapes). You will vibrate your machine out of alignment.



I'm not certain what "Join Gap" is but I have mine set to 0.0. 25.4mm is an entire inch & I'd be concerned of using that value (although never tested it).



Change "Refer" to Top-Left. Change Refer-X & Refer-Y based on how far from the top left corner you want to be beginning your cut (will differ per file you want to cut, although I use 0,0 always & change the refer point in the actual cut file by containing everything I want to cut inside a 300mm x 200mm rectangle).



Repeat value is generally at 1, however sometimes you need to repeat the cut in order to get through a stubborn material.



For Cutting, up the top where it says "Nearest" I would change that to "Inside First". Nearest may be quicker, but can be problematic, especially with fabric as it may shift/get blown once cut. Using inside first means it will cut any shapes that are inside other shapes out first (meaning the inside shapes will always be where you wanted as the material can't move/shrink/lose tautness at this point).



Other than that, I have "Unidir" set to ticked. It allows the laser to fire on both forward & back cuts. It is not necessary & there is some case for not using it, but I can't remember what. Choose to use or not use at your own discretion.


---
**Denis Bastien** *June 21, 2016 21:33*

Thanks.  I will make these correction and give it a try.


---
*Imported from [Google+](https://plus.google.com/103923212690093219161/posts/1JyAC4Nwtr2) &mdash; content and formatting may not be reliable*
