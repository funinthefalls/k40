---
layout: post
title: "Is an engraving like this possible on ceramic tile with the k40?"
date: February 20, 2017 23:46
category: "Object produced with laser"
author: "Arion McCartney"
---
Is an engraving like this possible on ceramic tile with the k40? If so, how would I get the engraved parts black? Thanks!

![images/6e1f5f492e243dc03998f2c3e5162cf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e1f5f492e243dc03998f2c3e5162cf9.jpeg)



**"Arion McCartney"**

---
---
**Anthony Bolgar** *February 20, 2017 23:56*

Paint the tile black, then with low power laser off the paint where you do not want it (Hint....invert the image to a negative before engraving) After it is finished you can spray it with laquer to seal the paint to the tile. Hope this helps!




---
**Ariel Yahni (UniKpty)** *February 20, 2017 23:57*

The tyle is painted black and you invert the photo so the  laser removes the black and reveal the white


---
**Arion McCartney** *February 20, 2017 23:59*

Ah that makes sense. Thanks. On this tile, the engraved parts are black. Maybe they painted it after the engraving and the black color soaked into the engraved portion? 


---
**Jeff** *February 21, 2017 00:12*

Laserbits sells a product called Cermark LMM-6044. It's a tile engraving spray that turns black when lazed.  I believe they have a couple different colors besides black.


---
**Arion McCartney** *February 21, 2017 00:21*

**+Jeff Watkins**​ that is cool stuff, thanks for sharing that info. 


---
**Arion McCartney** *February 21, 2017 00:23*

That cermark is some expensive stuff!


---
**Jeff** *February 21, 2017 00:25*

I've use the Cermark metal marking spray on stainless tumblers and it works great. I haven't tried their ceramic spray yet, but I did see a few finished tiles that look really nice.


---
**Arion McCartney** *February 21, 2017 00:28*

Very cool


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2017 01:13*

I would say that the suggestions to paint the tile black & laser off the paint are pushing it a bit. You would need to dial your settings in just perfect to not accidentally engrave into the tile.



I've tested engraving tiles in the past & the results I had were that the top layer of the tile in the engrave area was removed. I assume with higher power, slower speeds, multiple passes I could probably have got a little more depth (i.e. all the way through the glaze coating).



Personally I would just mask the entire tile, engrave the image I want (with as many passes as necessary to get some decent depth) & then apply paint.



However, after working with mosaics & tiles for a while a year or so ago, there would be another solution. Purchase blank unglazed tiles (we got them about au$2 a piece for 120mm2 tiles), then mask the tile with painter's tape, laser off the desired pattern (I would vector cut it since it's quicker), weed the tape that is not wanted (or if you engraved, it should just disappear in the engraving process), apply ceramic colouring & glaze, get it fired at the tile place.


---
**Arion McCartney** *February 21, 2017 01:51*

**+Yuusuf Sallahuddin** Thanks for that info and suggestion! 




---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2017 02:16*

**+Arion McCartney** You're welcome. Hope it helps.


---
**Ariel Yahni (UniKpty)** *February 22, 2017 20:50*

Take a look at this [https://m.facebook.com/groups/441613082637047?view=permalink&id=834387856692899](https://m.facebook.com/groups/441613082637047?view=permalink&id=834387856692899)


---
**Arion McCartney** *February 22, 2017 20:59*

**+Ariel Yahni**​ thanks for sharing that. That tile is amazing. Very cool process. I was amazed at how beautiful the black painted tile came out. I didn't think it could look that good! 


---
**Ariel Yahni (UniKpty)** *February 22, 2017 21:01*

Here's another one [https://www.facebook.com/groups/441613082637047/permalink/833938723404479/](https://www.facebook.com/groups/441613082637047/permalink/833938723404479/)


---
**Arion McCartney** *February 22, 2017 21:04*

**+Ariel Yahni**​ nice! I'll have to give this a go. My k40 is up and running now. Can't wait to upgrade the cheesy electronics it comes with! 


---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/BeSHVAFXYHr) &mdash; content and formatting may not be reliable*
