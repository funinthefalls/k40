---
layout: post
title: "Tube going out?'?!?! Had my K40 6 months now, can't even remember how many cuts I've made"
date: October 28, 2016 00:24
category: "Hardware and Laser settings"
author: "Ben Marshall"
---
Tube going out?'?!?! Had my K40 6 months now, can't even remember how many cuts I've made. I don't know if I just happened to get a bad batch of birch but 20ma at 7mm/s in two passes with no fallout and lots of marking. Cuts are clean, just not all the way through. Stock everything except A/A head. Maybe upgrade lens and mirrors? I really hope it's not the tube, business is starting to pick up





**"Ben Marshall"**

---
---
**greg greene** *October 28, 2016 00:40*

With all that experience I'm going to assume you keep the mirrors and lens clean - else you would have noticed a problem much earlier - so I know other folks have mentioned a meter that reads the strength of the beam - don't know much more about that yet - but perhaps they could tell you how to get one - or if close lend it to you so you have some concrete data to go on.


---
**Eric Flynn** *October 28, 2016 00:46*

20ma is a bit too hot for these tubes to last long. If you have been running it that high much, you have probably reduced the output of the tube significantly.  18ma is the maximum current they should be driven at, and thats too high for anything other than a short period every once in a while unless you are chilling the coolant. 




---
**Eric Flynn** *October 28, 2016 00:47*

Also, if you are on the stock current meter, they are horribly inaccurate.  An indicated 20ma is likely a bit higher.




---
**Ben Marshall** *October 28, 2016 00:48*

Usually I could get a clean cut at 13-15 mA, but not as of late


---
**Ben Marshall** *October 28, 2016 00:49*

I tried 20ma tonight to see if it would cut through 1/4 birch


---
**Alex Krause** *October 28, 2016 02:07*

Make sure you clean your Fixed mirror (first mirror from the tube) and also the lens on the beam exit end of the tube.... 


---
**3D Laser** *October 28, 2016 02:07*

I am having the same issue I just replaced my mirrors and lenses and still am not getting it to cut right and I think we got ours at about the same time


---
**Ben Marshall** *October 28, 2016 02:08*

**+Corey Budwine** yeah we did, withun the same month if I remember. Wondering if I should upgrade to a larger laser


---
**Ben Marshall** *October 28, 2016 02:09*

**+Alex Krause** checked and cleaned all mirrors and lens, no change 


---
**Ben Marshall** *October 28, 2016 02:09*

**+Corey Budwine** what is the average mA you cut with? 


---
**3D Laser** *October 28, 2016 02:11*

**+Ben Marshall** about 10ma no more than 12


---
**3D Laser** *October 28, 2016 02:11*

The weird this is my power seems great but it doesn't seem to want to focus even with the new lens 


---
**Ben Marshall** *October 28, 2016 02:12*

**+Corey Budwine** lens oriented the right way? Also is it the same size lens as before? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2016 02:32*

**+Corey Budwine** Does your new lens have the same focal point (50.8mm)? Could be that they accidentally shipped you a different focal point lens. Maybe a ramp test is in order to diagnose your focus issue.


---
**I Laser** *October 28, 2016 03:18*

If mirrors are clean and aligned then your tubes going, if not gone. Had the same experience. Over a couple of months I started having issues getting the machine to cut, then it hit a point where it would need so many passes it was ridiculous. 



I ended up buying a laser probe and was stunned the machine was able to cut at all, it was outputting around 10% of what it should have been... The machine was about 6 months old too, so gives weight to the rumours they are sold with dud/seconds tubes.


---
**Ben Marshall** *October 28, 2016 08:29*

**+I Laser** any recommendations for tube sellers? 


---
**I Laser** *October 28, 2016 09:20*

Sorry , better looking elsewhere for a recommendation. :)


---
**adrian miles** *December 18, 2016 09:09*

so when you got a new tube what power was it putting out;?




---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/EcsfxAckjos) &mdash; content and formatting may not be reliable*
