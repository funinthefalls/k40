---
layout: post
title: "Peal and stick felt fir my daughter's girlscout troop"
date: July 05, 2016 01:52
category: "Hardware and Laser settings"
author: "andy creighton"
---
Peal and stick felt fir my daughter's girlscout troop.

As low as the power will go,  but cut great! 



![images/17ae0e0be3845bc1e0592867e48d9b33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/17ae0e0be3845bc1e0592867e48d9b33.jpeg)
![images/0da3016146fdd0f2e625a1eb6e734e1f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0da3016146fdd0f2e625a1eb6e734e1f.jpeg)

**"andy creighton"**

---
---
**Scott Marshall** *July 05, 2016 02:10*

I've found that the cheap Walmart felt is great for making shims and gaskets. 



The Co2 laser just loves the cheap squares from the Wal-mart fabric aisle. as you also found, they cut great with very low power. (probably a good medium for those using Diode lasers)



I didn't know about peel and stick felt though. Ideas abound.



Nice work, I'm sure your daughters troop will be asking for more.



Scott


---
*Imported from [Google+](https://plus.google.com/111039030319838939279/posts/4yqy16M6F4u) &mdash; content and formatting may not be reliable*
