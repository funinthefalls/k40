---
layout: post
title: "Hi everyone! I have the K40 M2 nano and was wondering where you go to change the steps per inch(or mm)"
date: August 09, 2016 04:39
category: "Discussion"
author: "Savannah Champagne"
---
Hi everyone! I have the K40 M2 nano and was wondering where you go to change the steps per inch(or mm). I don't see anything related in the settings when you open them up on corellaser. I'm sure there is a way to do this but after hours are trying to find it I can't see anything.





**"Savannah Champagne"**

---
---
**Alex Krause** *August 09, 2016 04:59*

How far off are you?


---
**Alex Krause** *August 09, 2016 05:00*

Just change your design sizes to compensate for the laser Kerf


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 09, 2016 05:51*

There is an option in the corellaser device initialise settings for Resolution: 1000dpi. In the cutting/engraving window there is an option for Pixel Steps. When you set to 1 pixel step, an image will engrave at 1 dot per pixel (or at 1000/1). If you increase the pixel steps option (e.g. to 2) then you will get a lower resolution (1000/2 dpi = 500dpi).



Not sure if this is what you're looking for.


---
**Savannah Champagne** *August 09, 2016 06:21*

I had to replace the y axis belts is the reason I am asking. I just got everything put back together and didn't have enough time to tonight to test how off I am but a 1 inch square was slightly small. Going to do some more testing tomorrow.



I'm not too experienced with cnc machines but I do know on my tinyg router it is really easy and straight forward to adjust the steps to get things dialed in extremely accurate.


---
**Alex Krause** *August 09, 2016 06:24*

Where did you get your belt from? Reason I ask Light objects claims to have direct replacements for K40 parts


---
**Savannah Champagne** *August 09, 2016 06:31*

I just ordered some cheap gt2 belt from ebay. I didn't have time to do some in depth tests tonight after I got it put together but I believe it is making things a little smaller. Just a few thousands i assume. It might have even been that way with the old belt, I never tested. I just figured there was a way to adjust the steps on the board.


---
*Imported from [Google+](https://plus.google.com/117728217960308356873/posts/T26E41vXFMS) &mdash; content and formatting may not be reliable*
