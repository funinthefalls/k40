---
layout: post
title: "Hello All. Just a quick question. Anyone know of a good program for laser and 3d printers in Ubuntu 15 ?"
date: February 14, 2016 00:43
category: "Discussion"
author: "ED Carty"
---
Hello All. Just a quick question. Anyone know of a good program for laser and 3d printers in Ubuntu 15 ? I have it installed on my laptop works great. Just wondering if there is anything out there?? thank you.



Scott keep up the good work buddy





**"ED Carty"**

---
---
**Ariel Yahni (UniKpty)** *February 14, 2016 00:51*

If you have a modified K40 use [www.laserweb.xyz](http://www.laserweb.xyz)


---
**ED Carty** *February 14, 2016 01:03*

I have a Ultimaker 2. I am watching the forum because i hope to get one in the future.. 


---
**Scott Thorne** *February 14, 2016 14:31*

**+ED Carty**...thanks man...I will!


---
**Phil Willis** *February 15, 2016 13:38*

For 3D printing I use Simplify3d. Win/Mac/Linux



With an unconverted K40 you can only run on Windows. Once converted to Ramps or Smoothie then Laser Web looks good and getting better all the time.


---
*Imported from [Google+](https://plus.google.com/108643379719587743850/posts/dCozwRGaei9) &mdash; content and formatting may not be reliable*
