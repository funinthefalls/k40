---
layout: post
title: "Hey all I made the Cohesion3D mini controller up grade and Im using laser web 4"
date: December 06, 2017 01:50
category: "Modification"
author: "David Allen Frantz"
---
Hey all I made the Cohesion3D mini controller up grade and I’m using laser web 4. Does anyone know if that upgrade makes it possible to do vector etching? Or is it strictly raster?





**"David Allen Frantz"**

---
---
**Joe Alexander** *December 06, 2017 01:59*

the c3d boards run off smoothieware which is capable of both. use svg for vectors or bmp, jpg etc for raster.


---
**David Allen Frantz** *December 06, 2017 03:02*

Awesome! Thanks man!


---
**Jim Fong** *December 06, 2017 03:04*

This was done with LW4 following the vectors instead of raster. ![images/9109524bb3c7c4e206694b478efef845.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9109524bb3c7c4e206694b478efef845.jpeg)


---
**David Allen Frantz** *December 06, 2017 03:08*

Excellent thank you for the example!


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/LUn6L92tt7N) &mdash; content and formatting may not be reliable*
