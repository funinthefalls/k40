---
layout: post
title: "Hoping someone in here can help me"
date: February 18, 2016 11:59
category: "Discussion"
author: "Off-grid Denmark"
---
Hoping someone in here can help me.

I'm building a large laser, utilizing Arduino/RAMPS 1.4 with Turnkey firmware and LaserWeb.

I use 2 nema23 1.5A steppers for my Y axis, because of that i want the Y output to be mirrored to E1 on the RAMPS board.



This feature is implemented in Marlin, but not yet in the Turnkey variation.



How would I get going with doing that myself?

Or has anyone done it, and can give me helpful advice...

And no, I don't have the money to buy a SmoothieBoard.





**"Off-grid Denmark"**

---
---
**Stephane Buisson** *February 18, 2016 12:37*

look strange to me you need powerfull nema23 to move a laser head, are you sure the money is on the right component ?


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2016 12:42*

Noted. 


---
**Off-grid Denmark** *February 18, 2016 17:40*

Peter, I appreciate your concern. :)

But my Y axis is 2.000mm wide, which is why I opted for the dual config.


---
**The Technology Channel** *February 24, 2016 20:56*

It would be fine if you mounted the laser on the Y axis and it moved with it, always inline then even if it skews...




---
*Imported from [Google+](https://plus.google.com/107746484575421920081/posts/2bG8SZn2UTs) &mdash; content and formatting may not be reliable*
