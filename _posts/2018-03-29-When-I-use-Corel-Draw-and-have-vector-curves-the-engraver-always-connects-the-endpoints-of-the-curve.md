---
layout: post
title: "When I use Corel Draw and have vector curves, the engraver always connects the endpoints of the curve"
date: March 29, 2018 08:20
category: "Software"
author: "Troy Cados"
---
When I use Corel Draw and have vector curves, the engraver always connects the endpoints of the curve. Basically always making a closed path. Anyone out there experiencing this and know how to avoid it?





**"Troy Cados"**

---
---
**Ned Hill** *April 06, 2018 16:50*

Sorry just now seeing this for some reason.  You will need to change the engraving output format to Enhanced Windows Metafile (EMF) under coreldraw settings in the plugin.  I think BMP may work as well.  Here's my old post on this.  

[plus.google.com - Output file types with Corel X8 and Corel Laser - Several months ago I moved...](https://plus.google.com/u/0/+NedHill1/posts/F7QhfE7ffXL)


---
*Imported from [Google+](https://plus.google.com/+TroyCados/posts/3LK9r637XMV) &mdash; content and formatting may not be reliable*
