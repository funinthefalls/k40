---
layout: post
title: "A great find from Tweakie at the Openbuilds forum: \"Mach1 Filter is a little known and often overlooked, free, stand-alone, Windows based application which can be used to create the necessary GCode tool-path for producing"
date: October 23, 2016 01:40
category: "Software"
author: "Anthony Bolgar"
---
A great find from Tweakie at the Openbuilds forum:

"Mach1 Filter is a little known and often overlooked, free, stand-alone, Windows based application which can be used to create the necessary GCode tool-path for producing Lithophanes, 3D Relief’s and also laser, raster scan images etc.



It was written a few years ago and is pretty basic (without all the bells and whistles often found in more up-to-date software) but it still does a good job, and can accept HPGL, JPG & BMP format images.



As with most software, to obtain the best results, a little practice is required to judge the correct brightness / contrast / sharpness for the original image but it is quickly mastered and for those wishing to try, it can be downloaded from here : [http://hobbymaro.puhasoft.hu/Tweakie/Mach1Filter.exe](http://hobbymaro.puhasoft.hu/Tweakie/Mach1Filter.exe)





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *October 23, 2016 14:00*

**+Scott Thorne** 


---
**Scott Thorne** *October 23, 2016 14:03*

Thanks brother.


---
**Anthony Bolgar** *October 23, 2016 15:15*

:)


---
**Scott Thorne** *October 23, 2016 19:07*

I tried it but it saves it as a tap file....never heard of it and can't open it.


---
**Anthony Bolgar** *October 23, 2016 19:20*

tap is just a gcode file, rename it as *.gcode


---
**Scott Thorne** *October 23, 2016 19:34*

My software still won't open it....that sucks...lol


---
**Tony Schelts** *October 25, 2016 07:26*

Thanks you 




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/c5HxLQvi96R) &mdash; content and formatting may not be reliable*
