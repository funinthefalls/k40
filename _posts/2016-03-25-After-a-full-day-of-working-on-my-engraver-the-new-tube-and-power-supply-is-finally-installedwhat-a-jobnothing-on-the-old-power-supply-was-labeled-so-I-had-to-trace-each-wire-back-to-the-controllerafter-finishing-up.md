---
layout: post
title: "After a full day of working on my engraver the new tube and power supply is finally installed....what a job...nothing on the old power supply was labeled so I had to trace each wire back to the controller...after finishing up"
date: March 25, 2016 12:37
category: "Modification"
author: "Scott Thorne"
---
After a full day of working on my engraver the new tube and power supply is finally installed....what a job...nothing on the old power supply was labeled so I had to trace each wire back to the controller...after finishing up the pulse button would only fire a short burst...after 2 hours of troubleshooting I found out that I had the setting on the controller to manual and not on continuous....I felt like a complete idiot....glad it's over with...now to the alignment process.



Scott.





**"Scott Thorne"**

---
---
**Coherent** *March 27, 2016 14:43*

Glad you figured it out. It's always the small simple things that cause the big complicated problems it seems.


---
**Scott Thorne** *March 27, 2016 14:52*

**+Coherent**...thanks man...how have you been.....I haven't heard from you in a while. 


---
**Coherent** *March 27, 2016 15:35*

Hi Scott. I bought a new  mill and lathe recently and have been converting the mill to CNC ( thrust bearings, ballscrews etc.), and making all the parts and mounts from scratch, so that's kept me busy. I read through new posts here every few days though. I need to get my laser "re-tuned"  and aligned after moving it to a new location. Perfect tool to cut acrylic guards and chip covers for the other machines! I miss the smell of burning acrylic!


---
**Scott Thorne** *March 27, 2016 21:59*

**+Coherent**....good to hear from you...good luck with your build. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/6zBomqQmLo6) &mdash; content and formatting may not be reliable*
