---
layout: post
title: "Anyone doing double sided engraving? What about cutting images glued onto acrylic?"
date: December 05, 2017 04:04
category: "Discussion"
author: "Jerry Hartmann"
---
Anyone doing double sided engraving? What about cutting images glued onto acrylic? How are you doing the alignment?





**"Jerry Hartmann"**

---
---
**Phillip Conroy** *December 05, 2017 10:25*

I done a few80mm circles double sided etched, etchfirst ,then  cut of circle then i cut 0.5mm circle bigger as circles are not 100% round,then flip over and et h back


---
**Phillip Conroy** *December 05, 2017 10:26*

Without moving the materials you are cutting from,i use magnets to hold work peace


---
**Steve Clark** *December 06, 2017 05:26*

I add reference pin holes or cut the frame first then clamp the cut waste piece down which allows you to drop in you work back to your position. Getting it out needs planning or some pockets cut in the frame to get your fingers to where you can get part out.


---
*Imported from [Google+](https://plus.google.com/105524720582279612505/posts/gXWTQmArfxf) &mdash; content and formatting may not be reliable*
