---
layout: post
title: "Did my first mod :) Microswitch (NO) installed for door lock"
date: April 03, 2017 07:42
category: "Modification"
author: "Claudio Prezzi"
---
Did my first mod :)

Microswitch (NO) installed for door lock. Wired in series with laser switch.



![images/d922621b1116a8e95bbc97078fca3c5b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d922621b1116a8e95bbc97078fca3c5b.jpeg)
![images/f4cb43f845235d612109673a078da5a0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4cb43f845235d612109673a078da5a0.jpeg)
![images/3d93f0d885437b293b5433c199a3341e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3d93f0d885437b293b5433c199a3341e.jpeg)

**"Claudio Prezzi"**

---
---
**Jorge Robles** *April 03, 2017 07:58*

Elegant!!


---
**Stephane Buisson** *April 03, 2017 13:17*

Nice to see your K40 finally arrived. Welcome to the club **+Claudio Prezzi**.



glue ? (can't see any screws)


---
**Claudio Prezzi** *April 03, 2017 14:10*

I got my K40 a week ago. I realy need to thank all supportes for their donations!



Yes, I used hotglue for the switch. It should last, and if not, it's safe anyways because the switch will stay open. ;)


---
**Alex Krause** *April 03, 2017 14:37*

Normally Open contacts FTW!


---
**ALFAHOBBIES** *April 03, 2017 15:47*

Nice and simple. I'm going to do my machine the same way. Thank you for the idea.


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/LuEHmC7GgnZ) &mdash; content and formatting may not be reliable*
