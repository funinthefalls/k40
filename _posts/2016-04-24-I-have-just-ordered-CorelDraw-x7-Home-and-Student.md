---
layout: post
title: "I have just ordered CorelDraw x7 Home and Student"
date: April 24, 2016 21:10
category: "Discussion"
author: "Tony Schelts"
---
I have just ordered CorelDraw x7 Home and Student.  I believe that someone stated that the plugin wont work with the home version. Does anyone know>>??????





**"Tony Schelts"**

---
---
**Donna Gray** *April 24, 2016 22:06*

I downloaded a coralDraw 5x version with keygen for free and it works fine


---
**Donna Gray** *April 24, 2016 22:06*

works fine with coralLaser plugin


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:32*

**+Donna Gray** You wouldn't happen to have a link for that still? Else I'll search around. Would be useful to test & get rid of the horrible CorelDraw12 that came with the k40.


---
**Donna Gray** *April 25, 2016 05:08*

I just did a torrent search for CorelDraw 5x with keygen there is a you tube video on how to get it free


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 05:15*

**+Donna Gray** Sweet as. I'll look for a torrent for it later then. Thanks Donna.


---
**Donna Gray** *April 25, 2016 05:53*

**+Yuusuf Sallahuddin** do you have any you tube videos on using the corelLaser program to make a cutting vector file for the laser cutter I am having trouble using corelLaser to create cutting lines


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 06:54*

**+Donna Gray** No I don't use CorelLaser for making my files. I use Adobe Illustrator & import into it.


---
**Coherent** *April 25, 2016 12:44*

I was never able to get X7 to work with the plugin. X6 and X5 work.


---
**Tony Schelts** *April 25, 2016 20:33*

I used the plugin on x7 commersial version but it timed out and I wasnt going to pay £500+ pounds on it.  So I bought the home and student verions for £68 but now the plugin cant see it??


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/BRUqgTLF9nW) &mdash; content and formatting may not be reliable*
