---
layout: post
title: "Eye protecion for CO2 lasers. To wear or not to wear?"
date: May 16, 2017 01:56
category: "Discussion"
author: "Bruno Ferrarese"
---
Eye protecion for CO2 lasers. To wear or not to wear?



I was thinking... a dog whistle can't hurt the human ear because our "sensors" can't detect that high frequency.

The CO2 laser is an infrared light that wont excite our visual sensors cos like the dog whistle its out of range so we don't see it. 



What do you make of it?





**"Bruno Ferrarese"**

---
---
**Chris Hurley** *May 16, 2017 02:03*

Well you are taking a risk that you don't need too......after all you only have two eyes. 


---
**Joe Alexander** *May 16, 2017 02:30*

except your eyes cornea will focus the beam and fry the optic nerve, dont risk it.


---
**Bruno Ferrarese** *May 16, 2017 02:40*

**+Chris Hurley** Definitely! ,Better be on the safe side. (pun intended) I'm just brainstorming the physics/optics/dynamics around the subject.



**+Joe Alexander** I'm sorry bro but I don't buy that. How come the eyes will focus on some it can't perceive? 






---
**Ned Hill** *May 16, 2017 02:48*

Maybe your eye can't "perceive" the radiation but the eye tissue will definitely know it's there. (cue sizzle sound).


---
**Chris Hurley** *May 16, 2017 03:26*

Radiation is mostly unseen. You can't see the radiation from a microwave ether. Just it's effect. 


---
**Martin Dillon** *May 16, 2017 04:09*

I thought CO2 lasers made Ultraviolet light.  UV is harmful to your eyes focused or not.


---
**Joe Alexander** *May 16, 2017 04:41*

co2 is up in the IR spectrum wheras UV is around 350-500nm co2 is 10600nm


---
**Don Kleinschnitz Jr.** *May 16, 2017 11:39*

Bottom line: you get flashed by this beam your eyes WILL be damaged at some level ......


---
**HalfNormal** *May 16, 2017 12:49*

Only takes a second to blind or partially blind/damage the eye.


---
**Justin Mitchell** *May 16, 2017 15:31*

co2 laser is IR and will be absorbed by the front surface of the eye (cornea) and wont pass through to damage the retina, but those front bits of the eye will crisp up really quickly if hit by 40W of laser, fortunately lots of common materials can block/absorb that light, including see through ones like polycarbonate, acrylic and glass.For more interesting facts read: [repairfaq.org - Sam's Laser FAQ - Carbon Dioxide Lasers](https://www.repairfaq.org/sam/laserco2.htm)


---
**Anthony Bolgar** *May 16, 2017 18:10*

Just be smart...don't stare into the beam! Wear simple safety glasses, keep the door closed and you will be alright.


---
**Tim Gray** *May 16, 2017 20:39*

If you think your ears cant get hurt from a sound pressure you can not hear, then you dont know much about sound.  you can and WILL damage your hearing at extremely high sound pressures of  ultrasonic audio.


---
**Mark Brown** *May 16, 2017 22:26*

My understanding was that your cornea will <b>absorb</b> the light, so you're not going to hurt your optic nerve, but you will burn a hole into your eye.  Of course you aren't going to be staring directly into the beam...


---
**Anthony Bolgar** *May 16, 2017 22:48*

It is very difficult to get a direct eye strike that would damage your eye when dealing with a CO2 laser. The beam is not focused for one, and will scatter the further away from the source you are. I did a test and from 18" away the unfocused beam from my K40 took just over 30 seconds to make a noticable mark on the acrylic safety glasses I use. So unless you are sticking your head inside the laser under the focus lens the is minimal chance of doing any damage to your eyesight, you have a better chance of being hit by lightning. But freak accidents can and do happen, so I always wear the acrylic safety glasses. But buying $100+ laser safety glasses is unnecessary.




---
**Bruno Ferrarese** *May 16, 2017 22:53*

**+Anthony Bolgar** Touché!


---
**Jeff Johnson** *May 17, 2017 18:32*

I assure you that the invisible and unfocused laser beam will burn you at the right distance. Shortly after getting my laser cutter, I stupidly stuck my hand in the path of the beam and my finger suffered an instant burn accompanied by the sound of bacon frying and the smell of burnt skin. Don't risk it!


---
**Adrian Godwin** *May 18, 2017 00:51*

I was maintaining an 80W laser cutter and was a bit worried that the users had allowed the tray under the cutting bed to fill with plywood shapes. So 20cm under the bed was a pile of wood, getting hit by any stray beam that had cut through the next bit of work.



However, the beam is only slightly divergent as it goes around the mirror system. After focussing, it diverges faster once it's passed the focus point.



I ran the laser on to the  woodpile for several seconds and didn't get even a burn mark. I wouldn't put my eye there, but yes, after the lens the power does drop off fairly fast.

  








---
**Don Kleinschnitz Jr.** *May 18, 2017 02:33*

**+Adrian Godwin** the laser beam diverges at a mathematically predictable rate as it leaves the 

tube and continues diverging until it hits the objective lense which refocuses it and the divergence repeats at a different rate. Mirrors have no effect on divergence.



Guys, this discussion make me uncomfortable, it doesn't matter how you damage your eyes its permanent. You can argue that normally protection isn't required. You wear protection and have interlocks for the times when the unexpected and unexplained "a shit" happens.


---
**Gavin Dow** *May 25, 2017 14:02*

I've got a spot in my field of vision from reflected emissions off a part I was cutting before I bought a set of safety glasses, and it's been there for about 2 months. I found that running without glasses, even with the amber-tinted cover in place, my eyes would get sore and I would experience something similar to welders flash the next morning. I'm not talking about direct beam strikes, this is merely the reflection.

I figured if the reflection could do that kind of harm, I needed safety glasses. I picked up a set of the LightObject CO2 glasses, and I just put them on when I turn the laser on, and take them off when I'm done.

Can I say for sure that they work? Well, my eyes don't feel sandy after a couple hours of lasering anymore, and even if that's a placebo, for $35, it's a worthwhile safety measure for me. I've only got one good eye, and I don't intend to fry it with this thing.

Don't be cheap - if you can afford the laser, you can afford the proper protection equipment to keep from destroying your most important sensory organs.


---
**Don Kleinschnitz Jr.** *May 25, 2017 16:05*

**+Gavin Dow** are you saying you damaged your eye from your laser machine? Is it a K40.

What were you cutting?

I ask only because I never heard of an actual event.


---
**Gavin Dow** *May 25, 2017 16:56*

**+Don Kleinschnitz** I don't do any other activities that would leave such a long-lasting spot in my field of vision. It's just a small "floater" that I can only see if I'm looking at a white field (like a blank white computer screen), and it has been slowly fading over the last few weeks. I don't know specifically that it was caused by the laser, but I definitely don't remember it being there before I started using my K40 heavily, some time in late Feb / early March.

I believe I was cutting 1/4" plywood at the time, and due to some learning-curve issues, I was making multiple passes. This means it's entirely possible it was from bright re-emission at a different wavelength, and not the 10400nm itself, but again, I don't know specifically when it happened.

I do know that it was around that same time I started noticing welder's flash symptoms, and ever since I've started using the safety glasses, I do not have those symptoms anymore.

Like I always say, if it seems stupid, but it works, then it's not stupid. I don't know if the glasses help, but at this point, I am not taking any more chances. It's worth the $35 to know that there's something else between my lookin' balls and a laser I can't see.




---
**Don Kleinschnitz Jr.** *May 26, 2017 11:10*

**+Gavin Dow** I agree and the glasses (proper ones) do help. I am glad that you have no apparently permanent damage....


---
*Imported from [Google+](https://plus.google.com/111443543748488809183/posts/EGxR8TmmeBP) &mdash; content and formatting may not be reliable*
