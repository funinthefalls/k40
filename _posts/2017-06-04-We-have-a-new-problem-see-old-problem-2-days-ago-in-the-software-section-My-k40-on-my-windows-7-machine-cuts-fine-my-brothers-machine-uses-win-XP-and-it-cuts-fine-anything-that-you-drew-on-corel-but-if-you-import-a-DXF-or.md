---
layout: post
title: "We have a new problem ( see old problem 2 days ago in the software section ) My k40 on my windows 7 machine cuts fine my brothers machine uses win XP and it cuts fine anything that you drew on corel but if you import a DXF or"
date: June 04, 2017 12:35
category: "Software"
author: "Don Recardo"
---
We have a new problem ( see old problem 2 days ago in the software section )



My k40 on my windows 7 machine cuts fine



my brothers machine uses win XP and it cuts fine anything that you drew 

on corel but if you import a DXF or DWG it will start cutting fine but then start going over the same line maybe 20 or 30 times before moving to a new spot .

It used to cut DXF and DWG fine , this has only just started happening since he re installed the software which I let him borrow ( it was a copy of the disks that came with my laser ( his were smashed in transit from China ) I cant find my original disks and only have the copy I made 



I took my laptop with win 7  over to his laser and it cuts the same file perfectly

so its not a problem with his laser . It has to be his windows 7  or his laser software

 I took my copy of corel and laserdrw 2013.2 and we installed it on his machine and still it does the same problems with DWG and DXF

I now have put a clean instal of win XP on another of my computers and again loaded the corel and laserdrw 2013.2 and this machine also wont handle DXF

or DWG 



it cant be his laser because with my win7 laptop connected to it ,it cuts fine 

it cant be the DXF file because again it cuts fine if we use my win7 laptop

I cant see it being winXP because that was what he always used and it ran fine for a year . It only started playing up after trying to re instal corel and laserdrw 2013.2 

So I guess my laser instal disk is corrupt , does anyone know where I can download a working copy of the install disk that comes with the K40, The disk with corel and the laserdrw 2013 pluggin on it 



Cheers 

Don







**"Don Recardo"**

---
---
**HalfNormal** *June 04, 2017 16:38*

Thanks to **+Yuusuf Sallahuddin** here is a link;

[drive.google.com - K40 Software CD.rar - Google Drive](https://drive.google.com/file/d/0Bzi2h1k_udXwamRBZkduekxlMHM/view)


---
**Don Recardo** *June 04, 2017 19:07*

Yuusuf you are a start. Thats just what I needed Thank you


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2017 08:48*

Is that still hosted there? I remember moving it around between my cloud services so let me know if it doesn't work.


---
**Don Recardo** *June 06, 2017 10:57*

It said that there was a problem with the network but it still downloaded ok thank you


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 06, 2017 12:43*

**+Don Recardo** Thanks for confirming it still works :)


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/VwSzGfJ1XhG) &mdash; content and formatting may not be reliable*
