---
layout: post
title: "hi all quick question, what paint is best for spraying bed to absorb laser instead of reflecting and burning under side of work piece?"
date: August 06, 2016 10:44
category: "Modification"
author: "Pippins McGee"
---
hi all quick question,

what paint is best for spraying bed to absorb laser instead of reflecting and burning under side of work piece?

or is there no type of paint or coating that won't just be burnt away by the laser followed by reflection.

(fyi: am using  steel bedding salvaged from an ironing board)



cheers





**"Pippins McGee"**

---
---
**Anthony Bolgar** *August 06, 2016 11:02*

I think he is asking about the mesh grid itself. You can faux rust it with acid which would make it non reflective.


---
**HalfNormal** *August 06, 2016 14:00*

I find elevating the piece to be cut off the bed surface helps eliminate most reflective burns.


---
**Jim Hatch** *August 06, 2016 14:24*

Halfnormal is right. The most effective way is to get space between the piece and the bed. That's why honeycomb uses thin metal but is mostly air space. Pin beds work for the same reason.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 14:53*

I'm using perforated galvanised steel & I haven't noticed any underside reflection before, although I do notice a lot of gunk builds up on the steel & needs cleaning off regularly (or else that will stain the underside of work).


---
**Jim Hatch** *August 06, 2016 17:59*

**+Yuusuf Sallahuddin**​ you're also getting some nasty stuff in the air from anything that burns the bed 🙂 Galvanized steel gives off toxic fumes when heated or burned. Probably not enough in this situation to worry about but something to be aware of if you don't vent it well or you cut with the lid up a lot.


---
**Phillip Conroy** *August 06, 2016 19:25*

very bad idea to cut with lid open,which is the BIGGEST SAFTEY FAILING of the k40 laser cutters.They should have been made with the $3 switch .it is just not worth the risk of eye damage/fire /fumes/burns


---
**Jim Hatch** *August 06, 2016 19:30*

**+Phillip Conroy**​ Yep - should be the 2nd thing you do to add a lid safety switch. (First is to align the mirrors which is tedious and made more so if you have to keep opening & closing the lid so the switch gets taped down :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 19:33*

**+Jim Hatch** Thanks for pointing that out Jim. I wasn't aware of that & will keep in mind.



I've been operating with my lid open for a while (due to crazy setup I have going with my air-assist & not installing the drag-chain I got yet). But I've basically just been doing engraves, not cuts & I stand nowhere near the machine while it is operating.



**+Phillip Conroy** It is actually quite an unsafe machine, but that's what is great about this community. Plenty of people looking out for everyone else's safety :)


---
**Jim Hatch** *August 06, 2016 20:42*

**+Yuusuf Sallahuddin**​ Had to laugh. You're right - out of the box it is fairly unsafe.



Or as others might suggest, unrestrained by excess nanny safety features that get in the way of easy use.



No one I know keeps the blade guard on their table saw for instance. 😀


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 20:53*

**+Jim Hatch** Who needs a blade guard? Those things just get in the way of your fingers lol


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/D6iyEeaDFud) &mdash; content and formatting may not be reliable*
