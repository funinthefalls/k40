---
layout: post
title: "Ok finally started to use my machine..."
date: April 09, 2016 15:44
category: "Discussion"
author: "MNO"
---
Ok finally started to use my machine...



I'm in love... literally (hope my wife wont read that:) )

That is awesome toy, i fell like a little boy playing with Chinese toy.



Without adjusting anything i just cut 6mm acrylic on 5mA power 3mm/s speed and i even not sure i was in focal length of the lens, just put it in and tried... And edges so smooth.





**"MNO"**

---
---
**Damian Trejtowicz** *April 09, 2016 16:26*

I have the same,only wife throw me away with laser from my mancave in house to sheed in the garden and its so cold over there :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2016 16:56*

**+Damian Trejtowicz** Use your laser to start a fire to keep you warm :D


---
**Jim Hatch** *April 09, 2016 19:20*

Watch out, you'll start to look for things to engrave and cut and start to get tunnel vision. :-D



I also use a 60w laser with an 18x24" bed. I picked up some baltic birch but it was 30"x60" (yeah, odd size I know). I couldn't fit it in the big laser so I cut it into a 24x60 and a 6x60 piece on my table saw. Then went and loaded the long piece into the laser (it has a drop opening front so longer pieces can slide in). I wanted to cut it into 18" sections.



After drawing a cut line in the software, getting the birch into the laser, lining up the edge, resetting the starting point, running a cold test (no laser) I finally got my first cut. Then I repeated the process. 



When I was done I just thought it was the dumbest thing I did all day - I could have run the long piece through the table saw in half a minute to get my 18" sections but I was so focused on cutting it in the laser I didn't consider that there were easier ways to get the same result :-)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 09, 2016 20:34*

**+Jim Hatch** I've noticed that I sometimes do things like that, just for the sake of using the laser. When in fact, there are easier & quicker ways. E.g. drilling a hole with the laser by cutting a circle, versus drilling with a drill/drill-press & bit.


---
**I Laser** *April 09, 2016 22:39*

Lmao, can totally relate. I've done that a few times now...


---
*Imported from [Google+](https://plus.google.com/111888830486900331563/posts/3QVgPk2Dhxq) &mdash; content and formatting may not be reliable*
