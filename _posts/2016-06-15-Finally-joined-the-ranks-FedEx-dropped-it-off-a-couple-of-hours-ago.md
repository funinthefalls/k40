---
layout: post
title: "Finally joined the ranks! FedEx dropped it off a couple of hours ago"
date: June 15, 2016 21:20
category: "Discussion"
author: "Evan Fosmark"
---
Finally joined the ranks! FedEx dropped it off a couple of hours ago. It was really well packed, except the exhaust hose is in awful condition, but have plans on completely replacing that soon. Won't have time to get it running for a few days, but I'm really happy it made it here in one piece! 



Only problem so far is the packing foam is EVERYWHERE, and really tough to clean up since it clings to everything.

![images/a0e1385854b2977d813eb2a0d26f7c12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e1385854b2977d813eb2a0d26f7c12.jpeg)



**"Evan Fosmark"**

---
---
**Christoph E.** *June 15, 2016 21:23*

Damn, you even got the 'nicer' version. :D



Have fun!


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2016 22:09*

Yeah, looks like he got the $400 model with the "upgrades".


---
**Evan Fosmark** *June 15, 2016 22:19*

Got it shipped for about $360. The emergency stop and the other switch seem superfluous, but I'm happy that it has a nice light bar and a good door hinge. I think the only other upgrade is the castors, which are of no use to me, personally.


---
**Evan Fosmark** *June 15, 2016 22:22*

Oh, it also has a temperature readout that's connected to the water hose in the back.


---
**Ray Kholodovsky (Cohesion3D)** *June 15, 2016 22:25*

Sweet!  I paid 338 for the standard offering.  Yeah, we no want laser scooting around on its own.  Does yours have a ribbon cable for the x motor/ endstops?


---
**Christoph E.** *June 15, 2016 22:26*

Arw. Just spent €420 on the old one :D



Now i'm seriously jelous. -.-


---
**Richard Fortune** *June 16, 2016 00:10*

**+Evan Fosmark** who did you buy it from?


---
**Evan Fosmark** *June 16, 2016 06:36*

**+Ray Kholodovsky** yeah it has a ribbon cable that runs along the inside of the case. Is that notable?


---
**Christoph E.** *June 16, 2016 11:43*

Even wheels to drive around the workshop. :D

Have you tested it yet?


---
**Evan Fosmark** *June 17, 2016 04:49*

Got it all set up, but I can't read the DVD that came with it, so I can't install the software or the drivers! Does anyone have a copy that they could send me?


---
**Ray Kholodovsky (Cohesion3D)** *June 17, 2016 04:53*

I already gutted mine with smoothie, didn't even run a job on the stock electronics. I'm sure someone can be of more help. 


---
**fydaman** *July 11, 2016 08:27*

Evan! Send me your contact! I'm ready to buy one! 


---
**Evan Fosmark** *July 11, 2016 15:33*

**+fydaman** check your reddit account. I replied there


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/JENu6m8MyKk) &mdash; content and formatting may not be reliable*
