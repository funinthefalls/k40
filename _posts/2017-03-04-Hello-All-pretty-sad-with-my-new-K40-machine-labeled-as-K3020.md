---
layout: post
title: "Hello All, pretty sad with my new K40 machine (labeled as K3020)"
date: March 04, 2017 11:00
category: "Hardware and Laser settings"
author: "MA Lopez"
---
Hello All,

pretty sad with my new K40 machine (labeled as K3020). I plugged power in and little explosion inside the PSU happened. Opened it and it was the fuse.



Disconnected everything (including the flyback) and only left the 2 AC  wires + ground, changed fuse, did a new test and exploded the fuse again.



Desoldered the yellow box capacitor, desoldered the Flyback transistors, soldered a new fuse and it exploted...



The vendor said is going to send me a new PSU, but it's a bit frustrating not finding the problem... any idea?



Thanks in advance,



MA





![images/29ee25c21636848c4531f629d7f746b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29ee25c21636848c4531f629d7f746b4.jpeg)



**"MA Lopez"**

---
---
**Kostas Filosofou** *March 04, 2017 13:00*

Look at this post.. 

[plus.google.com - Any idea why this has happened ??](https://plus.google.com/+KonstantinosFilosofou/posts/9AF1i2hZjU2)




---
**Don Kleinschnitz Jr.** *March 04, 2017 17:16*

Disclaimer: 

......................

these supplies ... CAN KILL YOU!



If you are not trained and do not have the associated equipment to troubleshoot and repair High Voltage Electronics you should not open the K40 laser power supply and attempt any repair!

..........................

There are many potential causes for failure in these supplies. I most often see fuses blown when the bridge rectifier or the output drive transistors short. 

Given the flyback and output transistors are disconnected my guess would be the bridge rectifier.

See this post for some information on parts but please heed all the warnings.



[donsthings.blogspot.com - K40 LPS repair and test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)


---
**Maker Cut** *March 04, 2017 18:58*

I agree with **+Don Kleinschnitz** on both points. Be careful or you will be in the hospital, and most of the time it is the bridge rectifier in the power supply. The transistor is another option, but I would look at the bridge rectifier first.


---
**Don Kleinschnitz Jr.** *March 04, 2017 21:20*

**+MA Lopez** before this happened did you try any "lasering" and what type coolant are you using?


---
**1981therealfury** *March 05, 2017 00:53*

A friend of mine had the same issue with his new K40 he just got.  Turned out it was a too low spec bridge rectifier that was the issue.  The one in the PSU was a 700v 2amp one.  He swapped it out with a 800v 4amp one and he is now cutting through stuff like a hot knife through butter.  



Like others have said though, if you don't know what you are doing then don't try replacing the bridge rectifier, the PSU can kill if you touch the wrong places.


---
**Don Kleinschnitz Jr.** *March 05, 2017 01:13*

Part #'s and sources are here:

[donsthings.blogspot.com - K40 LPS repair and test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)




---
**MA Lopez** *March 05, 2017 09:26*

**+Konstantinos Filosofou** Thanks for the link. I will follow steps and try.


---
**MA Lopez** *March 05, 2017 09:27*

**+Don Kleinschnitz** Thanks Don, I am going to take out the bridge rectifier and test it, Best regards!


---
**MA Lopez** *March 05, 2017 09:30*

**+Don Kleinschnitz** Hi Don, It did not power up, well, The display did for a fraction of second and the fuse exploded. I am using 20 liters of inorganic 10% car coolant. That's distilled water with a 10% of chemical components to retard freezeing or boiling.


---
**MA Lopez** *March 05, 2017 09:32*

**+1981therealfury** Thanks 1981. I have some skill with electronics and soldering. The test I am doing are with the flyback out. If the PSU turn on, I will measure V before connecting flyback to it or any of the electronics. Each time I power it, after fuse blown I discharge the big capacitors to avoid peaks. Best regards.


---
**Don Kleinschnitz Jr.** *March 05, 2017 12:22*

**+MA Lopez** on a separate note, I advise you to remove the coolant that is using antifreeze. Antifreeze especially the car type is too conductive for this application and we now have proof that it creates arching and stresses the LPS supply and tube over time. I realize this is not your problem now, but it will be when you start running.


---
**MA Lopez** *March 05, 2017 12:36*

**+Don Kleinschnitz** Hi Don, I used it because we are using the same (10%) for some years with a 150W machine, but I am in time for changing it. What so you use? Just distilled water?

Best regards


---
**Don Kleinschnitz Jr.** *March 05, 2017 13:19*

**+MA Lopez** I am changing mine to distilled water and 1oz Clorox to 5 gal water.


---
**1981therealfury** *March 06, 2017 12:24*

**+Don Kleinschnitz**, I thought Bleach was a no go for putting in the coolant tube?  Something to do with its effect on the tube?  Or is it not Clorox bleach you are referring to?


---
**Don Kleinschnitz Jr.** *March 06, 2017 12:34*

**+1981therealfury** check these out:

[plus.google.com - COOLING WATER "IT MATTERS" After flailing for a few days with what I thought...](https://plus.google.com/113684285877323403487/posts/jDRGVhd6zqy)



[http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)


---
**1981therealfury** *March 06, 2017 13:58*

**+Don Kleinschnitz** Thanks, i was following that thread last week before i came down ill and then forgot about it.  Good to see all the extra work that went on to find a solution.  I have my distilled water now so ill flush my system and add a little bleach to the water.  Thanks for pointing me back there :)


---
**MA Lopez** *March 09, 2017 14:49*

Hi mates, I have just received the new PSU from the seller. I have to desolder the old red wire from the CO2 tube to the flyback and solder the new one. Any advices? Thanks and best regards,

MA


---
**Don Kleinschnitz Jr.** *March 09, 2017 15:29*

Disconnect the machine from mains and wait 1hr for the HV supply to bleed down (precautionary). You can touch that area with a grounded wire if you want to be more safe.



Note how the sheath and silicon covers the anode so you can duplicate it. Also note the routing of the HV anode wire.

You are going to peel off the sheath on the anode side. The sheath should be a tube with a slit part way up.



Try and release the sheath without totally destroying it so you can reuse it. Pulling/peeling upward should work but minimize the bending force on the anode connection. Peel off all the old silicon from the anode joint and post. Worse case you have to cut it off but that may necessitate getting a new piece of tubeing.



Your anode wire may be soldered or wrapped around the post. Replace the new wire the same way. If it is solder minimize the heat you use.



Use the tube of white silicon that came with your machine and apply it liberally around the replaced anode wire. Slide the sheath back over the anode wire (like it was before) and silicon. Fill the sheath. if needed, with the silicon and then wait 24hrs to run the machine. 



If the sheath is unusable you will have to find another tube material.



Video on replacing a tube, focus on the removal and replacement of the anode wiring @ about 5:20:


{% include youtubePlayer.html id="YBtMBGnp5pw" %}
[https://www.youtube.com/watch?v=YBtMBGnp5pw](https://www.youtube.com/watch?v=YBtMBGnp5pw)



My blog post on the sujbect:



[http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)




---
**Fernando Bueno** *March 22, 2017 18:57*

**+MA Lopez** He estado leyendo este hilo y las respuestas que te dan. Yo tengo el mismo problema con el fusible y no doy con la causa de que se funda. ¿has conseguido arreglarlo? Me gustaría poder contactar fuera de la comunidad para comentarte algunas cosas sobre el tema.



I apologize to the rest for writing not in English, but I do it with the intention of contacting and following the conversation privately.


---
**MA Lopez** *March 23, 2017 20:12*

**+Don Kleinschnitz** I received a new PSU and changed it. Followed your instructions but also did a silicone cover sealed with hot glue. Now it's working perfect. Thanks for your help.



**+Fernando Bueno** My mail is malopezn@gmail.com (mandame correo y hablamos)




---
**Don Kleinschnitz Jr.** *March 24, 2017 10:24*

**+MA Lopez** excellent. I assume you are not located in the US?

I'd love a picture of the completed connections if possible :).


---
**MA Lopez** *March 24, 2017 12:41*

Hi Don, I am in Spain. I have the back of the machine closed now because of the fan installation and some tape to seal it, but if I open it i take a picture. Thanks and best regards.

MA


---
*Imported from [Google+](https://plus.google.com/109180025053447503546/posts/bFovei5r1EM) &mdash; content and formatting may not be reliable*
