---
layout: post
title: "My k40 upgraded awc608 doesn't cut at a right position"
date: July 09, 2016 06:55
category: "Discussion"
author: "Trinh Ta"
---
My k40 upgraded awc608 doesn't cut at a right position. for example, I used laserCAD to draw a shape at x:40 y:40, but when it cuts, it starts at x:0 y:0. Thanks for your help!





**"Trinh Ta"**

---
---
**greg greene** *July 09, 2016 13:41*

I had the same problem.  Seems like laserDRW sets the top left corner at top left home position so I put my box to lase there in the software grid, then adjust the start position by moving the entire box with the cursor after the machine has homed the head.  The head then moves when I release the mouse to the position I want and quick tap on the test button confirms the postion of the start.  I'm sure there is something I'm not doing right - but this kinda works.


---
*Imported from [Google+](https://plus.google.com/114886757826264619236/posts/6ocY1qVMyBD) &mdash; content and formatting may not be reliable*
