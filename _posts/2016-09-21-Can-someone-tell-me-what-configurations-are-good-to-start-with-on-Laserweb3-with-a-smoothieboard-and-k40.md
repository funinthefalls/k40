---
layout: post
title: "Can someone tell me what configurations are good to start with on Laserweb3 with a smoothieboard and k40?"
date: September 21, 2016 12:52
category: "Smoothieboard Modification"
author: "Bruce Golling"
---
Can someone tell me what configurations are good to start with on Laserweb3 with a smoothieboard and k40?

Also what about the config file on the micro sd?  Does this need to be adjusted?





**"Bruce Golling"**

---
---
**Ariel Yahni (UniKpty)** *September 21, 2016 13:19*

Setting in LW are very straightforward, bellow image of mine

![images/57659d865e6aa10c88f81766f4d0b9a6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/57659d865e6aa10c88f81766f4d0b9a6.png)


---
**Ariel Yahni (UniKpty)** *September 21, 2016 13:19*

![images/f920b933e10a3aa2cd148d0b1d51a081.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f920b933e10a3aa2cd148d0b1d51a081.png)


---
**Ariel Yahni (UniKpty)** *September 21, 2016 13:20*

here is the link to my config. Please read on that thread regarding stepper motors [plus.google.com - My current config file. Beware that i run a azsmz smoothie compatible board…](https://plus.google.com/+ArielYahni/posts/Riexg5WHVs6)


---
*Imported from [Google+](https://plus.google.com/105808932307807066174/posts/EQsGdoj1YTW) &mdash; content and formatting may not be reliable*
