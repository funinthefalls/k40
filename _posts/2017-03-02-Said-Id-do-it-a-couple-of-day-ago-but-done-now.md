---
layout: post
title: "Said I'd do it a couple of day ago but done now"
date: March 02, 2017 19:02
category: "Air Assist"
author: "Steven Whitecoff"
---
Said I'd do it a couple of day ago but done now. Just a quicky out of nylon or similar just to make it fast to make, used a short piece of scrap material just to be sure if the head overruns the work area there will be no collision. The physical stops as delivered will allow it to go where it should not and a bad drawing has sent if out of bounds more than once. A good argument for limit switches on all 4 corners. Have NOT seen a noticable improvement cutting 1/4" epoxy sealed hardwood ply. Still cant cut thru in two passes even though first pass is more than half way. Not really a fair test but I want to know its limits and how power correlates to cut depth etc. 

![images/0c3c5c44368b0614a3d9a4e5782a06dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c3c5c44368b0614a3d9a4e5782a06dd.jpeg)



**"Steven Whitecoff"**

---
---
**Mark Brown** *March 02, 2017 19:59*

Did you turn that?


---
**Steven Whitecoff** *March 03, 2017 16:15*

Yes, I have a Harbor Freight(Seig) Mini lathe and mill, its just a quicky 


---
**Mark Brown** *March 03, 2017 17:53*

At one point I was thinking about doing the exact same thing. But now I'm going to add aiming lasers and maybe a height adjustment too.


---
**Steven Whitecoff** *March 03, 2017 19:48*

I did a quicky just to test what it helps and doesnt. I'm getting all new everything except I'll be using the K40 tube so it was more just to know if I should prioritize air delivery to the new machine and the answer is YES. Its important to get minimal kerf when cutting foam and may help a small amount with wood. No doubt once I have the new setup running I'll go with aiming crosshairs, coolant temp cutoff, coolant conductivity alarm, 6 limit switches...you get the idea. I'm also using closed loop stepper as one more layer of defense against material waste by bad cuts.


---
**Mark Brown** *March 03, 2017 21:23*

What you're building is going to be larger I assume?



My ultimate dream is to build a 2' x 4' CNC that could run a router or the K40 laser.  It may just be a dream, but dreams are fun.  At any rate I'm going to screw around and learn from this in the meantime.



Keep us posted when you start your build.


---
**Steven Whitecoff** *March 04, 2017 04:31*

Yes, waiting for some detail info to be sure but CCM UK has quoted me $789 delivered for 1220x610 workspace. Its some seriously nice kit...this is same in a 1300x900:



[aliexpress.com - Aliexpress.com : Buy CCM W40 06 1390 CO2 laser focus lens mirror single head double head laser mechanical kit XY table linear guide rail from Reliable linear guide rail suppliers on CCM Automation Technology Ltd](https://www.aliexpress.com/store/product/CCM-W40-06-1390-CO2-laser-focus-lens-mirror-single-head-double-head-laser-mechanical-kit/1264013_32777107907.html?spm=2114.12010612.0.0.8oDzKz)


---
**Steven Whitecoff** *March 04, 2017 04:36*

Not much to post on a build, bolt together parts. Make enclosure. Going to use Closed Loop NEMA 23 steppers, K40 tube, pump, Cohesion Smoothie +GLCD. Upsized tube power supply so when tube is worn out, its ready for more watts...and it will have an easy life with the K40 at reduced outpout.


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/d5Xa22fiK7j) &mdash; content and formatting may not be reliable*
