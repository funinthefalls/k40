---
layout: post
title: "Some pictures of my new low-cost \"inlens-laserpointer\""
date: December 13, 2016 11:20
category: "Discussion"
author: "java lang"
---
Some pictures of my new low-cost "inlens-laserpointer". 

Motivation for this expansion: it's very annoying adjusting the mirrors after some mechanical work or reassembling the head after cleaning the lens. Therefore I constructed a simple mechanism to bring a laserpointer into the ray path. All you need is a ballbearing from an old harddisk, a small laserpointer module, a 3V-battery with holder and toggle switch and a mounting block (I used an old 3d printerpart). The laserpointer has two positions, inside-path and outside-path, it can be toggled from the working side, no need to open the tube casing. I used a soft aluminium wire (1.5mm diameter) to adjust the laser and connect the lasermodule with the ballbearing. The ballbearing is practical without backlash, so you can exactly reposition the laser when needed. To mount the block, I had to drill a single hole.

The battery-holder including a switch is mounted with a doublesided tape. Adjusting the laserpointer is relatively easy: make a testshot to a paper, switch OFF the machine, toggle the laserpointer, switch on battery and bend the aluminium wire until you get the spot on the paper. Repeat it on all extreme positions. Once done, you everytime can readjust your mirrors or head without activating the CO2 laser.

Some more pictures are on my website [http://johann.langhofer.net/lasercutter](http://johann.langhofer.net/lasercutter)



![images/521beda88eb5b0c6ed6a67bbb2db6d48.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/521beda88eb5b0c6ed6a67bbb2db6d48.jpeg)
![images/5a3dfcc5fdc4d16c6ab1f0589ed2b926.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a3dfcc5fdc4d16c6ab1f0589ed2b926.jpeg)
![images/2550c24e9a45575788b7c25a95e3198b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2550c24e9a45575788b7c25a95e3198b.jpeg)

**"java lang"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 12:04*

That's nifty. I like the design & position. Next step: motorise it so it can all be done with the press of a button :D


---
**java lang** *December 13, 2016 12:14*

**+Yuusuf Sallahuddin** Thank you. Well I had a motorized version (with a pull magnet, was working very well) but I removed it because for my need this "manual" version is sufficient enough and there is no cabeling, you even don't need a powersupply to adjust :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 12:45*

**+java lang** Yeah, I guess sometimes simpler is better. I just like the idea of pressing a button haha.


---
**java lang** *December 13, 2016 12:47*

**+Yuusuf Sallahuddin** Anyway, you can press the button of the battery :)))))


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 13, 2016 12:49*

**+java lang** Haha, that's true :) So problem solved. Thanks for the share. It's a great idea.


---
**greg greene** *December 13, 2016 14:06*

Great mod !  Thanks for that


---
**Cesar Tolentino** *December 13, 2016 16:30*

Thank you for this wicked idea...


---
**Kelly S** *December 14, 2016 02:02*

Thats a nifty idea right there, will have to try this out sometime. 




---
**Dennis Luinstra** *December 14, 2016 16:52*

Great idea,  thank you for sharing. When I have some time I will also make this handy addition! Do you think I can use a piece of aluminum bar instead of the plastic 3d printer part? Will aluminum near the tube affect the output of the big laser tube? 


---
**java lang** *December 14, 2016 16:56*

**+Dennis Luinstra** Thank you. I think this has no influence because the mirror nearby also is a aluminium block.


---
*Imported from [Google+](https://plus.google.com/111347667569437072694/posts/9xuisPArSwn) &mdash; content and formatting may not be reliable*
