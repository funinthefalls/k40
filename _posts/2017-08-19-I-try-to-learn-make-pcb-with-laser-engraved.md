---
layout: post
title: "I try to learn make pcb with laser engraved"
date: August 19, 2017 19:27
category: "Discussion"
author: "\u0e2d\u0e21\u0e23\u0e0a\u0e31\u0e22 \u0e18\u0e19\u0e18\u0e31\u0e19\u0e22\u0e1a\u0e39\u0e23\u0e13\u0e4c"
---
I try to learn make pcb with laser engraved.



![images/9a9b18f1146935061fa1ddb0e309afa6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a9b18f1146935061fa1ddb0e309afa6.jpeg)
![images/2064120826e0835af6370054ddd65716.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2064120826e0835af6370054ddd65716.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-aDamOAKB934/WZiRKLqpdzI/AAAAAAAAACU/GvMfN8OUDeE_1LpSUZdhaOpu_ClFiEI-wCJoC/s0/VID_20170819_201858.mp4**
![images/5a75ec45480c7ba35ced4d50ce99eff4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a75ec45480c7ba35ced4d50ce99eff4.jpeg)

**"\u0e2d\u0e21\u0e23\u0e0a\u0e31\u0e22 \u0e18\u0e19\u0e18\u0e31\u0e19\u0e22\u0e1a\u0e39\u0e23\u0e13\u0e4c"**

---
---
**Jorge Robles** *August 19, 2017 20:57*

Please detail the process :)


---
**อมรชัย ธนธันยบูรณ์** *August 20, 2017 03:15*

I use auto spray paint flat black, export svg file to engrave software.

15% fire power 240 mm speed.


---
**Anton Fosselius** *August 20, 2017 06:39*

Does it work? Looks awesome :) how do you solder on the black paint?


---
**Jorge Robles** *August 20, 2017 06:43*

I suppose the laser removes the paint, exposing copper. Then you need to go the acid way to remove the exposed copper, and lastly use acetone to remove the remaining car paint?


---
**Anton Fosselius** *August 20, 2017 06:56*

Let me guess. You paint black to get better heat absortion and less reflection, then you remove the paint ?


---
**Jorge Robles** *August 20, 2017 07:01*

i think he is just masking to use acid later


---
**อมรชัย ธนธันยบูรณ์** *August 20, 2017 17:38*

yes I use paint for protect from acid.


---
**Michael Caughey** *August 22, 2017 17:48*

You paint it black.  Use the laser to remove the black where you want to be etched, then etch.  After etching you can use the acetone to remove the paint.  Then you drill the holes if you have through holes.  then add components and solder.  The laser cannot remove the copper.


---
**อมรชัย ธนธันยบูรณ์** *August 22, 2017 18:20*

Yes correct.


---
*Imported from [Google+](https://plus.google.com/113246487765078592316/posts/gcCGqJHiWmf) &mdash; content and formatting may not be reliable*
