---
layout: post
title: "Doe anyone know what the default micro step settings are for the K40 stock with the M2 Nano board, or even where the config file for those setting is hidden :)"
date: September 19, 2016 01:25
category: "Original software and hardware issues"
author: "Rodney Huckstadt"
---
Doe anyone know what the default micro step settings are for the K40 stock with the M2 Nano board, or even where the config file for those setting is hidden :)





**"Rodney Huckstadt"**

---
---
**Ariel Yahni (UniKpty)** *September 19, 2016 01:53*

Is your machine not moving the correct amount? I don't believe i have come across such a setting, considering that you cannot access the board firmware. The closets to that would be i believe pixel steps but that would me more like the gap not the actual movement


---
**Rodney Huckstadt** *September 19, 2016 01:58*

setting up a rotary device , and hunting info for the stepper settings,  not a fan of playing around with just distorting the picture, I figure I put the hard yards in first , it all becomes easy after :)


---
**Ariel Yahni (UniKpty)** *September 19, 2016 02:05*

Good luck. Are you just connecting it to the Y motor pin?


---
**Rodney Huckstadt** *September 19, 2016 02:36*

at the moment, yes,  it works I just need to sort out the settings :) at the moment a 10mm square cuts about 100mm (not that I have measured yet) but it is huge


---
**Ariel Yahni (UniKpty)** *September 19, 2016 02:48*

I understand. I have a rotary that I attached to my smoothieboard as an A axis and works very well once I convert the gcode


---
**Jaime Menchaca** *September 19, 2016 04:45*

which motor did you guys use i am looking into building a rotary  but cant find any info on my current motors,  i want to use something  as close to stock as possible﻿


---
**Rodney Huckstadt** *September 19, 2016 04:59*

nema 17  is pretty much the stock and I am using a NEMA 16 , same motor just a fraction smaller in size


---
**Jaime Menchaca** *September 19, 2016 05:37*

Any specific  specs  i should look for ?


---
**Ariel Yahni (UniKpty)** *September 19, 2016 12:40*

I'm using a Nema 17


---
**Scott Marshall** *September 20, 2016 02:16*

2 Phase bipolar (4 lead - can use 6 lead or even 8 lead in a pinch) , 1.8 degree per step (200 steps/rev) Less than 2A (which that size motor will be anyway)


---
*Imported from [Google+](https://plus.google.com/109243783228349869845/posts/ZisGee8VZgH) &mdash; content and formatting may not be reliable*
