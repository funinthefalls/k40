---
layout: post
title: "Is there a possibility to wire a safety switch to the lid, that turns off the laser?"
date: August 17, 2016 16:14
category: "Modification"
author: "Nico Neumann"
---
Is there a possibility to wire a safety switch to the lid, that turns off the laser?

If not for the original control board, can you add one that works with a smoothieboard?

Thank you for your replies!





**"Nico Neumann"**

---
---
**Alex Krause** *August 17, 2016 16:16*

Just add in a N/O limit switch in series with the Laser switch 


---
**Nico Neumann** *August 17, 2016 18:12*

Thank you!


---
*Imported from [Google+](https://plus.google.com/106888540859762729476/posts/bYAh4Qo2ESj) &mdash; content and formatting may not be reliable*
