---
layout: post
title: "My stock exhaust fan bit the dust..."
date: April 27, 2017 04:14
category: "Modification"
author: "Alex Krause"
---
My stock exhaust fan bit the dust... Anyone have a good link for an inline duct fan that works really well... THANKS IN ADVANCE :)





**"Alex Krause"**

---
---
**Alex Krause** *April 27, 2017 04:16*

**+Anthony Bolgar**​


---
**Joe Alexander** *April 27, 2017 05:35*

I currently use one of those 240cfm duct "booster" fans but find it insufficient when I do acrylic or similarly noxious stuff. I'll be lurkin' over this thread ;P


---
**Alex Krause** *April 27, 2017 05:36*

I ended up disassembling the stock blower to keep it limping along until I can get a replacement 


---
**Claudio Prezzi** *April 27, 2017 07:42*

We use something like this on our Lasersaur [hydroponics.eu - Air Extraction Kit - Fan + Carbon Filter - Ø 15CM -520 m3/h](https://www.hydroponics.eu/fans-and-filters-c-29/air-treatment-extraction-kit-with-filters-s-347/air-extraction-kit--fan--carbon-filter--15cm-520-m3h-25550.html)


---
**Claudio Prezzi** *April 27, 2017 07:48*

Needs an additional Pre Filter so the Carbon Filter doesn't get clogged too quickly.


---
**Don Kleinschnitz Jr.** *April 27, 2017 10:54*

**+Alex Krause** been happy with this lots of air and pretty quiet. Also large diameter. I use it with  floor heating ducts and ducting from Lowes. 





[amazon.com - VenTech IF6 6" Inline Duct Fan 440 CFM: Built In Household Ventilation Fans: Amazon.com: Industrial & Scientific](https://www.amazon.com/gp/product/B004YXDQZU/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**ALFAHOBBIES** *April 27, 2017 15:59*

I have been running a 240cfm inline fan and can smell smoke. I have it mounted in a wood frame in the window so it pulls but I don't think it is enough cfm. I am only using 4" line so maybe it doesn't flow enough cfm. Thinking of using one like Don with 440 cfm and 6" duct line all the way from the machine to the outside.


---
**Darryl Kegg** *April 27, 2017 21:03*

Anyone tried this one ?      



[https://www.amazon.com/gp/product/B06ZXWN3BG/ref=ox_sc_sfl_title_2?ie=UTF8&psc=1&smid=A2AW0W4FKP16S5](https://www.amazon.com/gp/product/B06ZXWN3BG/ref=ox_sc_sfl_title_2?ie=UTF8&psc=1&smid=A2AW0W4FKP16S5)


---
**HP Persson** *April 27, 2017 22:08*

I use a 110cfm fan with no smell, no noise, no problems.

Installing a exhaust fan is so much more than just buying a big ass fan.

But, buying a big fan skips the steps to optimize the air in your machine, but you get a more expensive fan and more noise.



Some tips and tricks

- Seal the machine, either tape or hot glue, seal all cracks

- Add intake fans to serve the exhaust with a positive pressure.

- Put the fan at the end of the hose, not behind the machine as most fans sucks good, but sucks at pressurizing the tube.



Scenario, a machine without no optimizations

Your exhaust has to do three things.

1: Suck air inside the machine past the impeller

2: Create a static pressure to push the air to the end of the hose

3: Overcome the negative pressure it just made, pulling in air into the machine.



With some simple fixes you could triple the efficiency of the exhaust with very small fixes and money.



A perfect setup, would look something like this

1: Exhaust fan at the end of the hose, maybe a short one the last bit out the window.

2: Intake fans slightly less than the exhaust in airflow, to prevent them pushing out smoke.

3: Seal the machine so air only can be delivered trough the intake fans. Aim to get a flow trough the case, from the front and out the back. 

As little turbulence as possible.

Sucking air from the side and pulling it in the back may create turbulence giving you problem when engraving and less efficiency of the exhaust.



With this setup your exhaust is doing one thing - sucking air. You also remove the strain from it to pull the machine case to negative pressure and at the same time create positive pressure on the other side of the impeller to push it trough the hose.

Very few fans are engineered to do both.



If you control the environment and air flow, there is no need for big noisy fans.

If you are ok with noisy big fans, or already have one in your shop, sure - use it. But if you looking for quiet operations, you need to look on more stuff than the fan :)



I can take a phone call sitting beside my machine running on full power, they will hear my laptop fans, not the laser :)



Rant over :)


---
**ALFAHOBBIES** *April 27, 2017 22:25*

Good information HP. I wonder if you could post some pics of your setup? Makes a lot of sense. I sealed every hole I could find. I have a newer k40 it has vents in the front of the door. Wonder if you sealed around the door?


---
**HP Persson** *April 27, 2017 22:32*

**+ALFAHOBBIES** I have the same case type, added two 50mm fans in the holes in the lid, and added rubber seal around the lid. Similar to the kind you have in windows and balcony doors to keep it sealed. Adhesive on the back, D-shaped.



Not much pics to show at the moment, machine in pieces. More tinkering than actually using it :P Trying to get my autofocus to work.



Here´s a pic of my exhaust fan though, while i was testing the pressure differences having it behind the machine or at the end of the hose. (do not put it as the pic shows :) )

![images/3a849d2a650f79a2dd5cceda188b887b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3a849d2a650f79a2dd5cceda188b887b.jpeg)


---
**HP Persson** *April 27, 2017 22:32*

**+ALFAHOBBIES** Here´s my case, same as yours right?

![images/97ad254cde8ae5b39a1062b4d5710baf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97ad254cde8ae5b39a1062b4d5710baf.jpeg)


---
**Joe Alexander** *April 27, 2017 22:45*

nice control panel! i see 2 temp gauges, i assume one is the water reservoir. Where's the other one reading from? And all those switches?(guessing laser pointer, air assist, ventilation, lights, water pump?)


---
**HP Persson** *April 27, 2017 22:52*

**+Joe Alexander** Actually three temp displays, one reading tank temp, one reading the cooling loop temp, as i have two pumps and two loops, one going to the tube, the other trough a peltier and radiator, and the third meter reading the PSU temp. The empty hole above the meters was for a digital mA-meter, but never installed it.



The two square buttons runs my autofocus. Other switches are as you said, lights, pointer, fans, cooling loop and last one is a cool down, a relay with timer running the water for 15min after machine is turned off.

This was my prototype panel, will change it in the future as i have converted to **+Don Kleinschnitz** suggestion on a improved potentiometer (multiturn with display) :)


---
**ALFAHOBBIES** *April 27, 2017 23:02*

I like how you did the 50mm fans. Nice and neat. I also have Don's potentiometer and display setup. It works great and is so simple to install. 


---
**Joe Alexander** *April 27, 2017 23:10*

here's my current control panel, still need to re-do it though not happy with the fit on some of the components yet. ma meter tied to pot for a 0--5v readout and temp meter not attached yet (its on the water tank itself atm) Yeas thats a RPi strapped to the side of it running lw-comm server :)

![images/dc21676370e3488e05cc9595022dad5e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc21676370e3488e05cc9595022dad5e.jpeg)


---
**HP Persson** *April 27, 2017 23:11*

**+Joe Alexander** Nice one, have to remake mine too, to fit the display for the Cohesion conversion. Nice and tidy setup you got there :)


---
**Ulf Stahmer** *April 28, 2017 04:02*

I agree with what **+HP Persson** writes. But I'd like to add that effort should be made to keep your exhaust length as straight and short as possible. I ditched my flex tube and replaced it with steel ducting. This reduces friction in the exhaust and improves airflow. I'm still using the stock fan with very good success.


---
**Anthony Bolgar** *April 29, 2017 20:35*

**+Alex Krause** sorry for not getting back to you sooner, recovering from pneumonia (again)_ I use a 12V 4" bilge blower that putsd out around 300CFM. It is strong enough to have both K40s hooked up to it at the same time.


---
**Nathan Thomas** *April 30, 2017 11:05*

 To piggy back on what **+HP Persson**​ said....I have a "big ass fan" that I transplanted from an old epilog I have. Very noisy...which I'm used to and it's in my Shop so not  bothering anyone. 



The main issue is that it's too powerful for the K40. It's at 660 cfm....granted I don't smell or even see smoke. Don't even need an air assist. But I posted a couple weeks ago about wavy lines in my engraving. I believe that's the culprit. A little too much suction. 



I'm still trying to find a good fix without replacing it. 🤔😐


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/ZbCinS5grKM) &mdash; content and formatting may not be reliable*
