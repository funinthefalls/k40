---
layout: post
title: "Hi ! I will receive my laser monday, and still haven't ordered some pieces"
date: March 13, 2016 09:39
category: "Modification"
author: "Jean-Baptiste Passant"
---
Hi !



I will receive my laser monday, and still haven't ordered some pieces.



Is this kit compatible and is it any good ? 



[http://www.ebay.fr/itm/DIY-CO2-Laser-Head-Si-Mirror-Znse-Focus-Lens-Engraver-Cutter-Mount-Part-/252176956189](http://www.ebay.fr/itm/DIY-CO2-Laser-Head-Si-Mirror-Znse-Focus-Lens-Engraver-Cutter-Mount-Part-/252176956189)



Thank you !





**"Jean-Baptiste Passant"**

---
---
**Jean-Baptiste Passant** *March 13, 2016 10:54*

**+Peter van der Walt** Thank you !

It was the perferct pack, and cheaper than lightobjects too :(


---
**Stephane Buisson** *March 13, 2016 11:26*

Bonjour **+Jean-Baptiste Passant** and welcome here.

n`hesite pas a faire des recherches par themes, tu trouveras plein de reponses a tes questions (pour les mirrors & lens

autour de debut novembre category "hardware and laser settings").

Sinon demande moi.


---
**Andrew ONeal (Andy-drew)** *March 15, 2016 05:32*

+Peter van der Walt I have a question that I haven't been able to find any answers to after hours of searching online. I've also posted about it in several posts here in the community. I have a few software programs I use with my k40 yours being one of them. In the Marlin firmware there is a place where #define LASER_PWM 50000 // STANDS FOR HERTZ VALUE. The thing im finding is having to change this value to 25000 which is needed for other programs I want to run. Is there a way I can modify this #define LASER_PWM 50000 // STANDS FOR HERTZ VALUE to allow for a range like for example, #define LASER_PWM 0-50000 // STANDS FOR HERTZ VALUE? Is there a way to do this? Also could you suggest a good starting place to eliminate a low memory available warning I'm getting after compiling/uploading my firmware? Email or message me directly if you don't want to post here. Sorry Jean-Baptiste Passant for blogging all this on your post.


---
**Jean-Baptiste Passant** *March 15, 2016 07:25*

**+Andrew ONeal** No problem, Maybe you can wire a pot and define LASER_PWM from this pot 0% being 0 and 100% being 50000, this allow manual tuning, or you can probably change you starting gcode and put define LASER_PWM in it (like temp for a 3d printer)


---
**Andrew ONeal (Andy-drew)** *March 16, 2016 03:14*

Jean-Baptiste Passant that's an awesome angle that I had not considered. Do you think it would work too define laser_PWM VALUE in gcode? Would something else need to be done for this to work or simply add to gcode? I'm not much of a coder so what would I put in exactly? Also what do you mean by wire a pot? Sorry I'm still really new to the code side of ask this lol. 


---
**Jean-Baptiste Passant** *March 17, 2016 07:45*

Yes, I think you can define laser_PWM Value in the gcode, as 3D printer do with temperature with command M104, this is IMO the best solution.



If you still want to see the trimpot solution here it is : [https://www.arduino.cc/en/tutorial/potentiometer](https://www.arduino.cc/en/tutorial/potentiometer)


---
**Andrew ONeal (Andy-drew)** *March 17, 2016 16:46*

In order to use the m104 I would need to rewire to fan output?


---
**Andrew ONeal (Andy-drew)** *March 18, 2016 04:59*

Mary have solved this seems all works in all programs as it should. Although still not entirely sure why HERTZ VALUE affects programs differently, is this value changing the power getting to machine, laser, steppers? Again not certain about any of this lol but I'm learning. My fix was simple enough but mimics other code in Marlin firmware. I simply defined laser_PWM (50000/2) // HERTZ 



Any thoughts?


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/2r9ZyWbBvL1) &mdash; content and formatting may not be reliable*
