---
layout: post
title: "USA $78.00 10\" Windows 10 tablet at Walmart (Sorry everyone else!) I found a RCA Cambio 10.1\" 2-in-1 Tablet 32GB Intel Atom Z3735F Quad-Core Processor Windows 10 at Walmart for $78"
date: May 15, 2016 14:59
category: "Discussion"
author: "HalfNormal"
---
USA $78.00 10" Windows 10 tablet at Walmart

(Sorry everyone else!)



I found a RCA Cambio 10.1" 2-in-1 Tablet 32GB Intel Atom Z3735F Quad-Core Processor Windows 10 at Walmart for $78. It includes a keyboard with touchpad. It was on the website but not on display in the store. I asked for them to check under the tablet display and they found it. Not all stores have it in stock. The Walmart website will tell you if it is in stock. Make sure you search for the $78.00 price. For some reason it will show up for $99.00 on their site. I have found it very snappy and the display is very good at all angles. The best part is that it sports a full size USB connection.





**"HalfNormal"**

---
---
**Alex Krause** *May 15, 2016 15:15*

That is what I have been running mine on


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 15, 2016 15:21*

That's a decent price. I'm looking at the stick-pc versions for my eventual k40 dedicated "computer".


---
**HalfNormal** *May 15, 2016 15:26*

**+Alex Krause** I am glad to see that it works so well on your K40. I bought it last night and have not had time to run it on the K40.

**+Peter van der Walt** Thanks for the link.


---
**ThantiK** *May 15, 2016 19:46*

The problem with those is that that they run Windows. And only windows. 


---
**HalfNormal** *May 15, 2016 23:11*

**+ThantiK** Still can't beat the price. Almost disposable and a great form factor when you do not have room for a bigger computer. Bottom line, you get what you pay for.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/gS79sSZ7ULC) &mdash; content and formatting may not be reliable*
