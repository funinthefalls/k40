---
layout: post
title: "Star Wars Millennium Falcon. I think I'll end up tweaking this a little more but it turned out really well for my first cut"
date: January 20, 2017 18:08
category: "Object produced with laser"
author: "Jeff Johnson"
---
Star Wars Millennium Falcon. I think I'll end up tweaking this a little more but it turned out really well for my first cut. 





**"Jeff Johnson"**

---
---
**Bill Keeter** *January 22, 2017 20:15*

thanks for sharing these. I have a couple family members that will go nuts when I make them one.


---
**Jeff Johnson** *January 22, 2017 21:09*

You're welcome.  I'll be updating the design tomorrow to add more detail and landing gear to the bottom. 


---
**Jeff Johnson** *January 23, 2017 20:06*

The design has been tweaked and updated on Thingiverse. I added a lot more detail to the bottom, including landing gear.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/4eymV7Q7GdU) &mdash; content and formatting may not be reliable*
