---
layout: post
title: "This turned out great....was messing around with some different speed and power settings"
date: January 16, 2016 13:00
category: "Object produced with laser"
author: "Scott Thorne"
---
This turned out great....was messing around with some different speed and power settings.

![images/5540ecc5338695e6f606dceae644bad7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5540ecc5338695e6f606dceae644bad7.jpeg)



**"Scott Thorne"**

---
---
**Todd Miller** *January 17, 2016 01:19*

This looks awesome !



Job well done, would you be

willing to share this image ?



I'd like to play with it...


---
**Scott Thorne** *January 17, 2016 05:41*

Sure....want me to email it to you?


---
**Todd Miller** *January 17, 2016 20:43*

yes that would be nice.  litterbox99   gmail   com


---
**Scott Thorne** *January 17, 2016 23:31*

Check your email...I just sent them.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/RmVZfMXZVgd) &mdash; content and formatting may not be reliable*
