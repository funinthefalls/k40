---
layout: post
title: "I've noticed my K40 is cutting deeper on the left of the unit than on the right"
date: February 25, 2018 04:09
category: "Hardware and Laser settings"
author: "Seon Rozenblum"
---
I've noticed my K40 is cutting deeper on the left of the unit than on the right. It doesn't matter what height I am cutting at, it's the same issue. By quite a bit. I am having to take the power from 60% up to 80% to get the same cut on the right as I do at 60% on the left.



So I am assuming it's the precision of the beam is getting off centre the more it does to the right of the unit? So the laser becomes less focused?



I can't think of what else it could be. The laser base it level, the height of the laser head on the far left and far right are the same.



Any thoughts?







**"Seon Rozenblum"**

---
---
**Don Kleinschnitz Jr.** *February 25, 2018 04:50*

Alignment....... the beam may not be exiting the laser parallel to the gantry and or the beam is not truely parallel X or Y such that it is climbing and going out of focus?

Have you looked at the spot where it hits the mirror when in all 4 corners?


---
**Kevin Lease** *February 25, 2018 04:57*

If the beam focus is off a bit it can have this effect

Check where beam hits opening on laser head at extremes of x axis, it should be at same spot

Make sure beam remains reasonably centered on lens too


---
**Seon Rozenblum** *February 25, 2018 05:06*

Yeah, my laser is WAY out of alignment.. even from first mirror (at the back next to the laser) to the first on the gantry.



When the head is home, the spot is hitting higher than when it's as far away on the Y as possible (0 on X ).



Going to be a royal pain to align this :(


---
**Seon Rozenblum** *February 25, 2018 07:29*

Wow, I've gone from out of alignment, but could almost cut through acrylic (2mm) to can't hardly even scorch acrylic in my hours of tweaking the alignment.



I finally have what looks like a dot that hits perfectly at the same position from all 4 corners of the extreme axis, but it wont cut anything.



Also, to get it to be as accurate as it is, I had to lift the height of the mirror on the gantry by 2mm (I made a 3d bit to go between the metal holder and the motor underneath, as the laser was hitting the very edge of the mirror without it.



But the laser is also hitting the very top of the hole on the laser head, rather than nearer to the middle.



I cant get it to hit anywhere near the center and have it no fall off up to 2-3mm in any direction on the 4 corners.



Am stumped.



Going to watch more videos of people aligning their K40s to see if there is something obvious I missed.


---
**Seon Rozenblum** *February 25, 2018 08:31*

Oh, I took a deep breath, and started again, adjusted every mirror including the one next to the laser. I got it much better now, but to cut 2mm black acrylic, I had to slow it to 6mm/s and 70%. I prob could have made it a tad faster, but it cut it evenly and it fell out when the cut was done.



BTW, the item I am cutting is 90mm wide, so pushing the sweet spot of the cutter for sure on the X axis.



I'll keep tweaking over the next week, but for now I am able to cut the 2 pieces I needed for a project - that I bought the K40 for!




---
**Seon Rozenblum** *February 25, 2018 08:39*

The precision of the cut is amazing. Better than I expected actually. That smudge on the bottom is just my finger print and light reflection - doh!



Tomorrow I try to cut clear and frosted clear parts.

![images/47421ec3a013f75881e32c45a49c565e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/47421ec3a013f75881e32c45a49c565e.jpeg)


---
**Don Kleinschnitz Jr.** *February 25, 2018 15:20*

**+Seon Rozenblum** folks that have this much trouble often have a laser tube (itself) that is not parallel to the gantry.


---
**Seon Rozenblum** *February 25, 2018 20:55*

**+Don Kleinschnitz** Hmm, I had not considered that. I feel like I am so out of my depth on this... but I will work thought it.



Though it did cut on first powerup - when i looked at the alignment before tweaking, none of the mirrors had the laser hitting anywhere near the centre of the mirrors. The last mirror, the laser was 1mm away from the edge of the mirror :(


---
**HalfNormal** *February 25, 2018 23:03*

Make sure the part/table is parallel with the gantry. Before I built my own adjustable table, I would constantly run out of focus due to a mm or 2 drop or raise of the item to be cut.


---
**Seon Rozenblum** *February 25, 2018 23:06*

**+HalfNormal** Yeah I can see the gantry is out from the side of the unit by about 1-2mm at the closest point to the back, but I am unsure how to adjust that easily without pulling all of the internals (cutting plate etc) out... and that's planned for a future "mod" so I'll tackle it then I guess.


---
**HalfNormal** *February 25, 2018 23:10*

A simple fix is to measure one corner of the object you are going to cut and shim it so that you have the same distance at all four corners. A few mm is  the difference between cutting and not cutting.


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/3wV2A5wMfSP) &mdash; content and formatting may not be reliable*
