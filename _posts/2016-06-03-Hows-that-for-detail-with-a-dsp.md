---
layout: post
title: "How's that for detail with a dsp?"
date: June 03, 2016 17:17
category: "Object produced with laser"
author: "Scott Thorne"
---
How's that for detail with a dsp?

![images/4fe715696c3ca03bda6ae9488d6bea72.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4fe715696c3ca03bda6ae9488d6bea72.jpeg)



**"Scott Thorne"**

---
---
**Ariel Yahni (UniKpty)** *June 03, 2016 17:34*

Dammmmm


---
**Alex Krause** *June 03, 2016 18:59*

Alder?


---
**Mr Shahrouz** *June 03, 2016 19:48*

Very good

What is your mashin & software ?


---
**Scott Thorne** *June 03, 2016 19:48*

**+Alex Krause**...yes...lol...I bought some more alder from amazon.


---
**Scott Thorne** *June 03, 2016 19:51*

**+Mr Shahrouz**...software is rdcam...laserworks....photo edited with paint shop pro....grayscale cut from 72 to 210 at 500 dpi.


---
**Scott Thorne** *June 03, 2016 19:52*

Grayscale edited with histogram...sorry...left that out. 


---
**Mr Shahrouz** *June 03, 2016 20:00*

You guys are so great creative and active


---
**Mr Shahrouz** *June 03, 2016 20:02*

I just started doing it and really love it


---
**Mr Shahrouz** *June 03, 2016 20:06*

Thank you


---
**David Cook** *June 04, 2016 01:22*

Wow


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/f3oouC18vGh) &mdash; content and formatting may not be reliable*
