---
layout: post
title: "Having issues here I got my new laser tube installed first one was broke anyways I can test fire the laser and get the beam on the table"
date: December 23, 2015 02:19
category: "Discussion"
author: "Lawrence Simm"
---
Having issues here I got my new laser tube installed first one was broke anyways I can test fire the laser and get the beam on the table.    However when I go to engrave something the MA jump all around on the gauge and it won't engrave ???





Any solutions.  Thanks 





**"Lawrence Simm"**

---
---
**Brooke Hedrick** *December 23, 2015 13:37*

When I am engraving, the ma jumps around a lot on my digital display.  My thought is the engraving can be turning the laser on and off very frequently.



What happens if you do a cut instead?



Have you checked your mirror alignment since replacing the tube?



I found by watching a lot of YouTube videos that just seeing a light doesn't mean it is aligned very well.



The beam can be just barely making it into the head, onto the lens, and getting a small fraction of the power to the material.


---
**Scott Marshall** *December 23, 2015 16:15*

It sounds like a bad connection that is being disturbed by the vibration of the steppers. I'd suggest a detailed inspection of all the connections. Make sure any connectors are seated completely, terminal screws are tight and things like that. These have a reputation for poor wiring practices, combine that with the long trip and things work loose quite frequently.



If nothing shows up that way, you may have to power on the laser and start gently tapping the control board, power supplies etc with a screwdriver handle while watching the meter. Make sure you are safe about it, and use an insulated tool, laser glasses and stay away from dangerous areas. Keep the cover down if possible. These don't have interlocks and have plenty of voltage and laser energy to cause serious injury or death, so if you're not sure you can do it safely, Don't do it. 



I've worked around dangerous machinery for years, but still hesitate to suggest to other people some of my methods. Banging around in an area with live HV and IR beams is not for the careless or uncertain person, so only proceed with that approach if you have the experience and are comfortable with the situation. 

It looks like 90% of the circuitry can be accessed with the laser area closed. Start with the control board and power supply, your problem is likely there, and check the connections on the switches as well, they come loose in shipping often, which is likely the sort of problem you will find.



Good luck, and be safe. 



Scott


---
**I Laser** *December 28, 2015 09:50*

My gauge jumps around like crazy too when engraving. The gauge is steady when cutting, or pressing the test button.


---
**Coherent** *December 29, 2015 19:19*

**+Brooke Hedrick**

 I have both a LED and analog meter on mine. The LED rarely registers anything when engraving. It's just cycling on and off to fast to register. Like others when it cuts, it provides a steady value. It sound like you may have mirror and/or table alignment issues.


---
*Imported from [Google+](https://plus.google.com/105668845795384171497/posts/c7T22ggZsLR) &mdash; content and formatting may not be reliable*
