---
layout: post
title: "Dont Follow Your Passion! A video from Mike Rowe, star of \"Dirty Jobs\""
date: July 18, 2016 18:18
category: "Discussion"
author: "HalfNormal"
---
Don’t Follow Your Passion! A video from Mike Rowe, star of "Dirty Jobs" 


{% include youtubePlayer.html id="CVEuPmVAb8o" %}
[https://www.youtube.com/watch?v=CVEuPmVAb8o](https://www.youtube.com/watch?v=CVEuPmVAb8o)





**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 18, 2016 19:02*

Wow, that advice would have been handy for me 15 years ago. Although never too late to learn & use the new knowledge. Thanks for sharing **+HalfNormal**.


---
**I Laser** *July 24, 2016 02:49*

So hold on, he points out that there are too many people with diplomas and degrees vying for a few limited 'careers'. Yet the whole presentation is sponsored by a university?!?



I also wonder how many of the quoted 5.8 million jobs that have a skills shortage are menial roles that few people see as a career.



I guess it's quite pertinent to the situation I find myself in. Having had a few different 'careers' and my qualifications limited to those sectors I'm now yet again looking at studying to move into another area. I would happily take a job up instead if I felt it had long term opportunities other than management roles. The lucrative opportunities, touted by this video, that few people want haven't been easy to find. Well at least not for me anyway!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 24, 2016 07:31*

**+I Laser** Funny you mention the university sponsorship of the video, I did notice that at the end. For me I find that me following my "passions" was probably a bit of a bad move considering the extremely limited options in those areas when I was studying them (at least in Australia). Albeit, I never completed any of my studies (got too bored and/or changed my mind). Should have focused on any job & kept my passions for hobbies like he suggests. I'd say you're right that majority of the skills shortage jobs are probably in menial job areas. Strangely, I believe that here in Australia we have a job shortage rather than a skills shortage. For unqualified jobs I apply for (due to lack of qualification in any field) I find that there are so many others going for the same job that I can't even get in the door for an interview. And in qualified jobs, people I know with qualifications are competing against plenty of other people too. But, I personally believe some kind of degree/qualification would benefit anyone in looking for work, at least here in Australia, as the competition is a lot lower than the competition for menial jobs.


---
**HalfNormal** *July 24, 2016 17:06*

**+I Laser** **+Yuusuf Sallahuddin** Yes it is a University sponsored video but they are pointing out that a "degree" does not mean a job especially if it is in a subject that has too many people vying for the same position or is in a subject that has no career potential. 



I too was in the same boat. My "passion" is electronics and computers. I worked in that profession for over 20 years. The money and opportunities started to dry up quickly. I then went back to college in my mid 40's and acquired a degree in nursing which I am enjoying now. I have been able to combine my electronics and computer skills with my nursing and have a wonderful job that I am sure I will retire with. 



My passion is now my hobby that I am able to enjoy without the constraints of working on what I am told and not what I want! ;-p


---
**I Laser** *August 04, 2016 03:04*

I suppose it was kind of refreshing, if in a backward sort of way lol. 



Uni's here are more interested in their bank balances, There are plenty of government subsidised degrees available with absolutely no prospects of employment for graduates!



Degree's are also so prevalent that their value is now much lower than it once was. That's progress I guess...


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/6MqSWofYwLQ) &mdash; content and formatting may not be reliable*
