---
layout: post
title: "I removed the frame (thanks for advice Ashley M"
date: November 30, 2015 17:14
category: "Modification"
author: "Gary McKinnon"
---
I removed the frame (thanks for advice **+Ashley M. Kirchner**), removed the horrible ducting and have begun removing the support posts for the old bed. I think i'll get some threaded brass rod in there instead and rig a poor man's z-table.



Have been lazy and ordered a premade honeycomb bed.



The connecter between the stepper and board can't be unplugged at either end so i'll have to keep the frame near the chassis while modding.



![images/074cbfa41bc5df0cf4c9253f04df7cd4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/074cbfa41bc5df0cf4c9253f04df7cd4.jpeg)
![images/cc6d76f32488a5690bdefea73e691b9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc6d76f32488a5690bdefea73e691b9e.jpeg)
![images/58d26eaa76fb486ac614802a7472a4eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/58d26eaa76fb486ac614802a7472a4eb.jpeg)
![images/ea0650a49fe77aa45aefad4d8caf696e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea0650a49fe77aa45aefad4d8caf696e.jpeg)
![images/bed255f934c1693d5126e682a2ca4155.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bed255f934c1693d5126e682a2ca4155.jpeg)
![images/df6edbc040bae2df10addf4c97ee98a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df6edbc040bae2df10addf4c97ee98a2.jpeg)

**"Gary McKinnon"**

---
---
**Ashley M. Kirchner [Norym]** *November 30, 2015 20:51*

The ribbon cable is held in place by friction pins inside of that white connector. Use the blue "tab" to pull on it.


---
*Imported from [Google+](https://plus.google.com/+GaryMcKinnonUFO/posts/JNWffKNKZZR) &mdash; content and formatting may not be reliable*
