---
layout: post
title: "Any idea why this has happened ??"
date: December 30, 2016 09:15
category: "Modification"
author: "Kostas Filosofou"
---
Any idea why this has happened ?? 







![images/2f6ed6ef1fca6aa30bc923e782e3bf31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f6ed6ef1fca6aa30bc923e782e3bf31.jpeg)
![images/7d5e59c84e5db77f86c7ac98a8384eef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d5e59c84e5db77f86c7ac98a8384eef.jpeg)

**"Kostas Filosofou"**

---
---
**Kostas Filosofou** *December 30, 2016 09:41*

were can we find schematics for this power supplies ?  


---
**Kostas Filosofou** *December 30, 2016 10:59*

**+Peter van der Walt**​ thanks a lot


---
**Don Kleinschnitz Jr.** *December 30, 2016 13:58*

**+Konstantinos Filosofou** this is a pretty common failure on these supplies. I assume that that is the fuse that is blown.

If this is a new machine insure that your mains are correct and supply is set for the right voltage.

Most of the time the problem is shorts in (see schematic):

...Rectifier module (left of the diodes D2-3, 6-9)-

...D2-3, 6-9

...Flyback transistors N1-2 -




---
**Kostas Filosofou** *December 30, 2016 15:28*

**+Don Kleinschnitz** yes is the fuse .. but what's cause this failure? The machine worked fine for about 3-5 in total ... I will check the parts you suggested.. thanks!

..but is repairable or it's start to falling apart piece by piece ?


---
**Don Kleinschnitz Jr.** *December 30, 2016 17:05*

we don't really know because we are not keeping track of what failed during replacement and most are just buying a new one. One theory is that the H Switch is missing snubber diodes.


---
**Kostas Filosofou** *December 30, 2016 17:10*

**+Don Kleinschnitz**  I find a Transistor that was fried .. i don't know if that is the reason but i was lucky to find a good one in some old scrap boards i have ... i have yet replace it, i will try later  to see if the board works again.

![images/0bdbbc79db220971f30175dc2b14e166.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0bdbbc79db220971f30175dc2b14e166.jpeg)


---
**Don Kleinschnitz Jr.** *December 30, 2016 17:36*

This one?

[http://dalincom.ru/datasheet/5L0380R%20(KA5L0380R).pdf](http://dalincom.ru/datasheet/5L0380R%20(KA5L0380R).pdf)



BTW I don't think that is a transistor?

Off line switcher PWM control ....?


---
**Kostas Filosofou** *December 30, 2016 17:38*

Yes the 5L0380R .... I don't know either i am not electronic :) 


---
**Don Kleinschnitz Jr.** *December 30, 2016 17:54*

**+Konstantinos Filosofou** its actually an offline switching IC :). How do you know it was bad?


---
**Kostas Filosofou** *December 30, 2016 18:42*

**+Don Kleinschnitz**  I saw the burn marks in the casing ... but anyway it was not the problem .. repair it and connected in the power .. still the same .. when i power it on drops the relay in my cellar...


---
**Kostas Filosofou** *January 03, 2017 17:57*

As the **+Don Kleinschnitz** mention i change all the parts that possibly was failed and the PSU works again and the laser also but ... the drive board don't , i get 12 and 5v from the PSU but when turn on the PSU the motors didn't go in home position and the computer didn't find the machine through usb. Any ideas ?



This is my board

![images/69cdf0b0bfe90ff1e7cb74a4e84e30e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/69cdf0b0bfe90ff1e7cb74a4e84e30e8.jpeg)


---
**Don Kleinschnitz Jr.** *January 03, 2017 18:31*

**+Konstantinos Filosofou** Your laser power supply is now working :). What parts did you replace (so I can keep track for others). How do you know it is working, test switch fires it :).

The DC power to the board should be 5vdc and 24vdc not 5vdc and 12vdc? Did you mistype?


---
**Kostas Filosofou** *January 03, 2017 19:24*

**+Don Kleinschnitz**​​ yes 24 and 5v I was mistyped.. I changed the rectifier module and the electrolytic capacitor ( the yellow one behind the fuse)


---
**Don Kleinschnitz Jr.** *January 03, 2017 19:27*

**+Konstantinos Filosofou** sounds like the controller board has a problem. I dont have a schematic for that .....?


---
**Kostas Filosofou** *January 03, 2017 19:38*

**+Don Kleinschnitz** did you thing is repairable? In web i can't find anything


---
**Don Kleinschnitz Jr.** *January 03, 2017 20:06*

**+Konstantinos Filosofou** most just buy another from ebay etc.

The parts are all surface mount so it delicate to repair and I do not have a schematic so it hard to identify the circuit.



You could do some superficial measurements like the VDD (5V) and 24V on VBB1& 2, on the stepper drivers.

 

The fact that it won't boot the USB leads me to believe the processor is not running. But that could be power is missing somewhere.



Here is the A4988 stepper data sheet.

[pololu.com - www.pololu.com/file/download/a4988_DMOS_microstepping_driver_with_translator.pdf?file_id=0J450](https://www.pololu.com/file/download/a4988_DMOS_microstepping_driver_with_translator.pdf?file_id=0J450)



Is there any damage on the board, charred parts etc. 


---
**Kostas Filosofou** *January 03, 2017 21:01*

**+Don Kleinschnitz**​​ i take a look but I also believe that is very difficult to repair with only the basic tools I have... Anyway maybe it's time to look for alternative Smoothie based boards for example..


---
**Don Kleinschnitz Jr.** *January 03, 2017 23:54*

**+Konstantinos Filosofou** this is exactly why I converted my machine. No schematics, no support and the software was crap.

Check out Cohesion 3D. **+Ray Kholodovsky** for a drop in choice.

**+Ray Kholodovsky** somewhere in all these referrals must be a free board for me ...lol.


---
**Kostas Filosofou** *January 04, 2017 00:38*

**+Don Kleinschnitz** I have already contacted Ray :)


---
**Kostas Filosofou** *January 04, 2017 14:40*

**+Don Kleinschnitz**  for your archives... 

[a-d-k.de - www.a-d-k.de/data/dat20150611_213938/reengineering_pfade.jpg](http://www.a-d-k.de/data/dat20150611_213938/reengineering_pfade.jpg)


---
**Don Kleinschnitz Jr.** *January 04, 2017 15:19*

**+Konstantinos Filosofou** wow thanks


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/9AF1i2hZjU2) &mdash; content and formatting may not be reliable*
