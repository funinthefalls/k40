---
layout: post
title: "On this 5000 Members celebration days, let's also have a think for Laserweb community >3000 members Cohesion 3D community with near 1000 members Both have been pro active to make our Hobby more enjoyable !"
date: May 10, 2018 14:06
category: "Discussion"
author: "Stephane Buisson"
---
On this 5000 Members celebration days, let's also have a think for 



Laserweb community >3000 members

Cohesion 3D community with near 1000 members



Both have been pro active to make our Hobby more enjoyable !



[https://plus.google.com/u/0/communities/116261877707124667493](https://plus.google.com/u/0/communities/116261877707124667493)

[https://plus.google.com/u/0/communities/115879488566665599508](https://plus.google.com/u/0/communities/115879488566665599508)



Special thanks to +Don Kleinschnitz, for his amazing work [http://donsthings.blogspot.fr/](http://donsthings.blogspot.fr/)



Special thanks to **+Arthur Wolf**, **+Peter van der Walt**, **+Ariel Yahni**,  **+Anthony Bolgar**, and many others (sorry to not be able to name you all) to have made this great community.





**"Stephane Buisson"**

---
---
**Don Kleinschnitz Jr.** *May 10, 2018 17:31*

Thanks for the mention, more to come now that I am back from vacation. 

Thanks also to those who have donated $$ so that I can fund the research work!



I would also add thanks to the LightBurn team for such an easy to use software product.



and ... the K40 Whisperer for the first stock compatible software.



This is a fun, productive and knowledgeable community!


---
**Anthony Bolgar** *May 11, 2018 15:04*

It has been an amazing ride, I have gone from a total newbie who was having trouble with his K40, to someone that can answer most any question. Like **+Don Kleinschnitz**,  I would like to thank all those who donate $ for research and development, as well as all the members who have contributed to the knowledge base. A few years ago the K40 was a really poor way to enter the hobby, and now the much maligned K40 can now hang with machines 10-20 times it price with a little time and money in upgrades. Kudos to **+Stephane Buisson** for starting this G+ group!


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/LjxLqQPgMSP) &mdash; content and formatting may not be reliable*
