---
layout: post
title: "Currently using LightBurn Beta 0.5.12 and a Cohesion3D Mini Laser"
date: February 04, 2018 00:02
category: "Software"
author: "Richard Vieira"
---
Currently using LightBurn Beta 0.5.12 and a Cohesion3D Mini Laser. Half way through some of my engravings the graphic LCD will show idle and LighBurn will still show busy but the laser is not doing anything. Anyone else have this issue?







**"Richard Vieira"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 04, 2018 01:33*

Need more details. A screenshot of your program window would be a good start. 



Tagging **+LightBurn Software** 


---
**Richard Vieira** *February 04, 2018 01:58*

![images/1ab12e57999c55a15ba0b53fdfad041b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1ab12e57999c55a15ba0b53fdfad041b.png)


---
**Richard Vieira** *February 04, 2018 01:58*

![images/12cd2c1dccbdafd1ce477d8d76ad08a6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/12cd2c1dccbdafd1ce477d8d76ad08a6.png)


---
**Richard Vieira** *February 04, 2018 02:00*

Only difference is where it shows the machine is disconnected the timer was still counting and the line indicating amount completer was about 50%  across.


---
**Richard Vieira** *February 04, 2018 16:34*

![images/7c8a4a5ce3a3d6498a1e37b6e5c74064.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7c8a4a5ce3a3d6498a1e37b6e5c74064.png)


---
**Richard Vieira** *February 04, 2018 16:35*

Tried again this morning it made it a little further. software shows busy but LCD display shows idle. 


---
**Jim Fong** *February 04, 2018 17:20*

Could be USB disconnect issues. Try a better quality or shorter USB cable. Preferably one with ferrites.  



Mine works on either USB2 or 3 but try a different port too.  Lots of different USB chipsets, not all work as reliable as others. 



Try a different pc/laptop if you have one available. 



I know some users had issues with noise on AC power line. Caused by larger appliances on the same circuit. Try a different circuit. 



Process of elimination.......


---
**Richard Vieira** *February 04, 2018 19:03*

Thanks for the suggestion. I will try the USB cable first with ferrites 


---
**Richard Vieira** *February 07, 2018 01:59*

Tried various usb cables and this issue continued. Changed computers and now the issue is gone. I am a bit disappointed as the computer I switched to is very old and I don’t care for it. Nevertheless I can engrave large projects now without timing out. 


---
**Richard Vieira** *February 07, 2018 02:11*

FYI- DELL XPS tablet did not work. MS Surface Pro 1 did work.

**+LightBurn Software**


---
**Warren Eames** *February 10, 2018 06:42*

Richard I have had this problem since I upgraded to the Cohesion 3d board. I have changed the USB cable (at least 3 different cables), the one I am using has the ferrite on it. I have Lightburn 0.5.14 and am now using GRBL but it was the same with the Smoothieware. I am using win 10 & would like to stay with win 10. I have tried isolating the power, not using a wireless mouse, turning off other machines & mice that are nearby. I have noticed that I can not change the "Port Settings" & the Bits per second is 57600. Other Port settings I have are 9600. When I say I can not change it I mean that once I change it & go out of that window & go back in it has reverted to what is was before : 57600. The problems you were having are exactly the same as I. Your fix was an older computer, is that correct?


---
**Richard Vieira** *February 15, 2018 22:58*

Yes, ultimately changing to an old computer worked for me.




---
*Imported from [Google+](https://plus.google.com/116764978409385040525/posts/Gr2H6EUHKSo) &mdash; content and formatting may not be reliable*
