---
layout: post
title: "I know that this has been posted before, but i have not for it, so like a dumb ass i am going to ask it again"
date: April 02, 2018 17:34
category: "Modification"
author: "Dan Stuettgen"
---
I know that this has been posted before, but i have not for it, so like a dumb ass i am going to ask it again.  When you install the lid laser interupt swith.  Which wire is the one that  should be used.  I am using the LO E5 DSP controller.





**"Dan Stuettgen"**

---
---
**Don Kleinschnitz Jr.** *April 02, 2018 18:11*

The lid switch should be in series with the laser enable of the LPS.


---
**Dan Stuettgen** *April 05, 2018 15:57*

So as i understand it.  I would connect the lid interupt swith to the WP and Gnd on the power supply. And connect the flow sensor to the WP and Gnd on the DSP E5 controller.  That way it will interupt the laser if the lid us opened as well if the water pump stops working.


---
**Don Kleinschnitz Jr.** *April 05, 2018 16:28*

**+Dan Stuettgen** normally these switches are all wired in series between ground and WP. When any one of them opens the laser is disabled.

...Laser Enable Sw on the panel

...Water flow switch

...Water temp switch

...Cover interlocks



I do not use the E5 controller so I cannot accurately advise you on that. However the enable loop (show below) usually is not connected to the controller (DSP) in any way as this loop must be fail proof. 



I am also unaware of what LPS you are using so the exact terminals may be different.



Below is a general diagram for wiring a LPS interlock loop. 



[https://photos.app.goo.gl/FARLgpEEU7uJlIgE2](https://photos.app.goo.gl/FARLgpEEU7uJlIgE2)


---
*Imported from [Google+](https://plus.google.com/116093665495871650574/posts/QZyohQrVXRN) &mdash; content and formatting may not be reliable*
