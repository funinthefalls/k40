---
layout: post
title: "Hi, After some time of un-use, as I can't seem to get decent wood cuts no matter what I try, I've tried the machine again..."
date: October 29, 2016 06:04
category: "Hardware and Laser settings"
author: "Peter Gisby"
---
Hi,



After some time of un-use, as I can't seem to get decent wood cuts no matter what I try, I've tried the machine again...

Things are better since upgrading the head to the Air Assist one but still things are not great. 

Normally the MDF takes about three passes to slice straight through, which it did at the top of the item. I've since been testing the bed height and cleaning / realigning mirrors but the output stays the same. See the first picture..



It's like there is a area where the intensity of the laser is reduced somehow.



The second picture is what I've ended up with...



Any hints as to what causing this and how I can eliminate the issue. 



Thanks.



![images/afa44f225add3200edba4b0803911ade.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afa44f225add3200edba4b0803911ade.jpeg)
![images/f45a57cecd5a30b08f89002f1d7614e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f45a57cecd5a30b08f89002f1d7614e5.jpeg)

**"Peter Gisby"**

---
---
**Scott Marshall** *October 29, 2016 07:13*

Did your wood draw moisture while waiting?



Normally I'd expect focus or alignment issues, but the lack of use points to maybe MDF drawing in water.  It's rather like a sponge in damp locations.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 29, 2016 07:15*

Beam maybe hitting the interior wall of your air-assist head. Check the head is allowing the beam to exit in the centre at all points on your bed.


---
**greg greene** *October 29, 2016 13:54*

Check also that there are no twists or bends in the rails that tilt the head so that the beam hits the nozzle wall at those areas.


---
*Imported from [Google+](https://plus.google.com/111055748369814573959/posts/6CMUKWUKW34) &mdash; content and formatting may not be reliable*
