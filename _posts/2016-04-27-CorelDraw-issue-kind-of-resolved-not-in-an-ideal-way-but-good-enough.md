---
layout: post
title: "CorelDraw issue kind of resolved not in an ideal way but good enough"
date: April 27, 2016 22:09
category: "Discussion"
author: "Tony Schelts"
---
CorelDraw issue kind of resolved not in an ideal way but good enough.  It appears you cant use CorelLaser with CorelDraw Home and Student.  I cant work round this.  I have Installed X6 Corporate with keyjen. This doesn't have email look up as does x7 x8 .  I will create in x7 save as x6 and use it that way.  What a palava





**"Tony Schelts"**

---
---
**Donna Gray** *April 27, 2016 22:46*

I must have been lucky I downloaded the corelDraw 5x with key gen for free and lucky it works I can't understand why I have laserdrw as I don't use it


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 28, 2016 01:31*

**+Donna Gray** LaserDrw is not necessary if you are using the CorelDraw/CorelLaser combination. LaserDrw is a freestanding app that you could use, but I find that it's design features are poor compared to existing software (i.e. CorelDraw). But then again, I don't use CorelDraw to design anyway.


---
**Rick Tennyson** *May 01, 2016 13:58*

I have the same issue:"The Corel Draw Ver=11 software not installed!"? My laser worked a good 6 months before getting this prompt. I have tried removing and reinstalling from step 1, still the same issue. 

I need some help.

I have Corel 4 on Windows 10 and it worked 6 months before this message.  Any advise?

Thanks






---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 22:20*

**+Rick Tennyson** Uninstall CorelLASER & reinstall it. I just installed CorelDraw x5 the other day. Already had CorelLASER installed (as I'd previously been using CorelDraw 12). Once I uninstalled CD12 & CorelLASER, then I reinstalled CorelLASER, it seemed to work for me.



Oh, sorry, I just registered you said you already tried removing/reinstalling.


---
**Tony Schelts** *May 01, 2016 22:26*

I installed x6 corporate and thought i might get away with it. but after 3 day it has decided its an illegal copy even though i blocked it in my firewall.  sucks


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 22:46*

**+Tony Schelts** That sux. Maybe try find a different copy of it that kills the activation attempts or whatever it is that has caused it to determine it is pirated.


---
**Tony Schelts** *May 01, 2016 22:50*

What sucks is that i have a Legit copy of coreldraw x7 but its home and student, and CorelLaser doesn't work with it.  Wish I knew what it was looking for.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 22:55*

**+Tony Schelts** Yeah, it's quite problematic. If you want, check this link ([https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XcCXxJgh3hT)) I just posted the other day. You will find a copy of x5 in there (with keygen/activation method that works fine). 


---
**Tony Schelts** *May 01, 2016 23:11*

Yuusuf where are you, Are you in the UK. times look pretty similar.  Anyway do you know if it times out at all>>>?

Thanks



Im going to be now.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 23:12*

**+Tony Schelts**  Unfortunately not in the UK. I'm in Australia.



Do I know if what times out sorry?

edit: I'm guessing you mean if after a few days/weeks it decides it is pirated? If so, I'm unsure as have only been running it a couple of days so far.


---
**Tony Schelts** *May 01, 2016 23:20*

Just noticed you live near the gold coast, My neice lives somewhere over there as a Nurse. Anyway just downloaded you dropbox will try it in a few days.. Thanks and good night.



Tony


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 01, 2016 23:33*

**+Tony Schelts** Cool, have a good one. Hope it works out for you. Yeah, I'm in the Gold Coast.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/dU4vSUqZgtd) &mdash; content and formatting may not be reliable*
