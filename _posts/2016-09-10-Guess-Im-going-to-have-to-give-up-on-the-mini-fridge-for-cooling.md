---
layout: post
title: "Guess I'm going to have to give up on the mini fridge for cooling"
date: September 10, 2016 00:35
category: "Discussion"
author: "Robert Selvey"
---
Guess I'm going to have to give up on the mini fridge for cooling. It's causing too much condensation on the tube and I'm sure that can't be good. 





**"Robert Selvey"**

---
---
**Ariel Yahni (UniKpty)** *September 10, 2016 00:39*

ohh really. I was going to get myself one of those. Guess ill get myself some copper tubing and build a readiator


---
**greg greene** *September 10, 2016 00:46*

As long as the beer stays cold - it isn't a complete loss!


---
**Robert Selvey** *September 10, 2016 00:59*

Being out in the garage hot and the water being so cold it was dripping.


---
**Scott Marshall** *September 10, 2016 01:00*

Don't give up, that's a good viable solution. 

I've played with running a heat exchanger loop thru my big fridge. It works well (too well as you also found) 



Install a bypass and mix chilled water with ambient (tank water). If you split the line and run only a percentage of the water thru the fridge, you can control the temp. (an old plumbers trick to keep toilet tanks from sweating is to blend in a touch of hot water with the inlet - it pays to watch the 'true' masters...)



(or you can get sophisticated and run a temp controller, water to glycol exchanger to cool the tank water without sending it into the fridge at all - I've got a kit planned for something similar. If your're interested in playing with a prototype, let me know.)





There's a lot of ways. You could install an inline t-stat and keep the fridge at 40F, or whatever it takes. (just leave the stock tstat turned down and it won't operate - you won't even have to cut the fridge up)



The super low tech approach would be to leave the fridge door open a bit...


---
**Robert Selvey** *September 10, 2016 01:54*

What do you think would be the best way to cool it in a 90f garage ?


---
**Ariel Yahni (UniKpty)** *September 10, 2016 01:55*

interested in this solution as im on average at the same temp


---
**Robert Selvey** *September 10, 2016 02:01*

The condensation with the mini fridge on the tube was crazy, when I would do a test fire it was like the beam was messed up focus wise do to the condensation at the end of the tube I would have to use a cue tip to dry up the end of the tube.


---
**Scott Marshall** *September 10, 2016 03:49*

It's the ambient humidity that you're fighting. A dehumidifier would be the best  (Well a nice air conditioned room would be best, but we can't have everything). When the humidity is high enough, anything below room temp(even 1 degree) is going to cause the condensation. Air movement will help a bit, a muffin fan on the critical part (HV end) of the tube may help as a low buck improvement. In some industrial machines Like Electron Scanning Microscopes, they bond insulation right to the chamber so there is no air gap.



 No air, no water, thus no condensation, the solid insulation displaces the air and allows the exposed surface to remain at near room temp.



Regular closed cell pipe insulation comes to mind as a possible solution here - it's polypro or PE and is a good high voltage insulator, won't absorb moisture and bonds well with RTV. 



I'd try dryfitting a section (maybe 8" long) of PP insulation around the power lead etc, and then bond it in place using clear RTV, being sure to seal all the edges down (if you can't get the entire surface glued down)

Stay away from the butadine rubber insulation, it's not a good electrical insulator and can hold moisture. Butadine is common in larger sizes of pipe insulation.  (Butadine is usually black and soft, where the Polypro or PE is gray, stiffer and slippery surfaced)



Leave the beam opening as small as you can. I'm hoping it will be warm enough and have very little airflow, so not get condensation on it. If you have a "tunnel" of dead air, it should warm up and stay put (well, it's worth hoping)



You could probably get away with closed cell sheet if that's all you can find, and it may be easier to fit up.



That's my best guess on a solution for a humid room that can't be de humidified.



Scott


---
**Vince Lee** *September 10, 2016 14:21*

If it were me I think I might try having the fridge run on a separate piping loop from the water reservoir on it's own water pump.  I'd hook up the pump with a thermostat so it only came on when the tank temp exceeded the room temp by a few degrees.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/Qm3jcxYXyJZ) &mdash; content and formatting may not be reliable*
