---
layout: post
title: "My x axis has started to make a little vibrating noise when the head moves, I'm guessing it could be the tension of the belt so who do people gauge how tight it should be or is it just a case of trial and error?"
date: February 04, 2017 20:31
category: "Original software and hardware issues"
author: "Andy Shilling"
---
My x axis has started to make a little vibrating noise when the head moves, I'm guessing it could be the tension of the belt so who do people gauge how tight it should be or is it just a case of trial and error?  





**"Andy Shilling"**

---


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/7QA3xPaseJd) &mdash; content and formatting may not be reliable*
