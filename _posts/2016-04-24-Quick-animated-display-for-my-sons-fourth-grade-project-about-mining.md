---
layout: post
title: "Quick animated display for my son's fourth grade project about mining"
date: April 24, 2016 16:48
category: "Object produced with laser"
author: "David Cook"
---
Quick animated display for my son's fourth grade project about mining. 10 minutes in solid works. 10 minutes in inkscape. And 2 minutes on the laser. I use PETG filament to make the rivets for the joints, this is 3mm fluorescent green cast acrylic. 





**"David Cook"**

---
---
**Damian Trejtowicz** *April 24, 2016 17:51*

3d printer and K40 are great team.you can do magic if you use both of them.i like your project


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:28*

That's awesome. I wish this sort of thing was readily available when I was growing up as a child. Great work.


---
**David Cook** *April 25, 2016 03:09*

**+Yuusuf Sallahuddin** thanks.  Our kids are exposed to such awesome things these days.  When I was a kid I could only dream of such things. 


---
**I Laser** *April 25, 2016 03:34*

Oh come on, the Atari 2600 was way better than 3D printers, tablets, laser cutters, etc :P



Nice job by the way. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 03:34*

**+David Cook** Totally true. Also would be cool to have a dad like you that is capable of making awesome little gadgets like this for his projects.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/dGMnwLhRGSr) &mdash; content and formatting may not be reliable*
