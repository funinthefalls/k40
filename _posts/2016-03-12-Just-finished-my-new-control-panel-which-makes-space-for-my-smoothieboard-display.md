---
layout: post
title: "Just finished my new control panel which makes space for my smoothieboard display"
date: March 12, 2016 18:14
category: "Modification"
author: "Vince Lee"
---
Just finished my new control panel which makes space for my smoothieboard display.  Underneath hangs mounts for the board itself and the connection to my magnetic water flow sensor.  It's awesome how handy a laser cutter is for doing this sort of thing! Now just need to wait for my custom adapter board to come in.



![images/55829c1800f2a3fc046fe1d9426b55cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55829c1800f2a3fc046fe1d9426b55cd.jpeg)
![images/c8a628c9f2168a5dc5d7dac0a6491d6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c8a628c9f2168a5dc5d7dac0a6491d6c.jpeg)
![images/514b06cc98fde7423f11ead829ef0a3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/514b06cc98fde7423f11ead829ef0a3a.jpeg)

**"Vince Lee"**

---
---
**Gee Willikers** *March 12, 2016 18:21*

What does the solid state relay control?


---
**Vince Lee** *March 12, 2016 18:24*

**+Gee Willikers** it replaces the old beam enable switch and is currently driven both by the water flow sensor and a small Pwm board that I can enable to cut dashed lines for easy paper perforations.


---
**Alex Krause** *March 12, 2016 19:49*

What material is this made from?


---
**Vince Lee** *March 12, 2016 19:54*

**+Alex Krause** it's a copper-on-black colored acrylic laminate sheet I got from inventables, backed up with another panel of clear acrylic for strength.


---
**Richard Vowles** *March 12, 2016 20:34*

What's that strange F measurement in the bottom left corner? Looks very nice by the way :-) 


---
**Vince Lee** *March 12, 2016 20:50*

**+Richard Vowles** it's a temperature sensor I got from dealextreme for $3.  The probe is taped to the outside of the water reservoir.  And thanks.


---
**Josh Rhodes** *March 13, 2016 03:07*

This reminds me, in an extreme way, of case nodding 15 years ago. 


---
**Josh Rhodes** *March 13, 2016 03:08*

Don't get me wrong.. it's awesome. I've still got a case modding itch.﻿


---
**Vince Lee** *March 13, 2016 05:58*

**+Josh Rhodes** heh. in a landfill somewhere there is a pc case with faux finished black marble paint and my fingerprints on it ;)


---
**Josh Rhodes** *March 13, 2016 08:05*

I painted mine glossy black too.. but it was cold and wet outside and It made for a weak sticky paint job.. haven't spray painted the first thing since then.


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/UBRM5rSR6qj) &mdash; content and formatting may not be reliable*
