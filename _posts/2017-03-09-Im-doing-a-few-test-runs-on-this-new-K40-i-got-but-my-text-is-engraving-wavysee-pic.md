---
layout: post
title: "I'm doing a few test runs on this new K40 i got, but my text is engraving wavy....see pic"
date: March 09, 2017 22:03
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
I'm doing a few test runs on this new K40 i got, but my text is engraving wavy....see pic.

Anyone have any ideas why this could be happening?

![images/e8668f87f965d0fa89e4205d1632c6da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e8668f87f965d0fa89e4205d1632c6da.jpeg)



**"Nathan Thomas"**

---
---
**Nathan Thomas** *March 09, 2017 22:19*

Tried a different font and it is better except the L's??? Same size plate, same position on the table 

![images/281f94bd119d1cd8c719e7f4ed1b8f71.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/281f94bd119d1cd8c719e7f4ed1b8f71.jpeg)


---
**Nathan Thomas** *March 09, 2017 22:29*

3 different fonts...

![images/3bb240f7056d4bea1202e26947e188bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3bb240f7056d4bea1202e26947e188bf.jpeg)


---
**Wolfmanjm** *March 09, 2017 22:45*

could be a loose belt, I know my X belt was very loose, you can tighten them up.


---
**Nathan Thomas** *March 09, 2017 22:57*

**+Wolfmanjm** which screws tightens it?


---
**Nathan Thomas** *March 09, 2017 22:57*

Would a loose x belt explain why some fonts engrave clean and others dont?


---
**Wolfmanjm** *March 09, 2017 23:06*

hard to say the effects as you get backlash effectively. So it depends on which way it is going. IIRC the screws are on the left side accessible through the hole in the PSU side. Don't over tighten though.


---
**Nathan Thomas** *March 09, 2017 23:23*

**+Wolfmanjm** ok x belt...are there any vids or images on how to do it? I don't want to loosen or tighten the wrong screws


---
**Wolfmanjm** *March 09, 2017 23:26*

not that I know of, I just followed the x belt and you can see where the idler is attached by screws which pull the idler in when you tighten the screws


---
**Nathan Thomas** *March 09, 2017 23:38*

**+Wolfmanjm** would it happen to be any of these? On the right side from inside the large hole 

![images/1b43109237904cbb206d99618e287f5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b43109237904cbb206d99618e287f5d.jpeg)


---
**Wolfmanjm** *March 09, 2017 23:50*

I think it was one or two of those :)


---
**Nathan Thomas** *March 10, 2017 00:26*

**+Wolfmanjm** thank you sir, I appreciate your guidance. :)


---
**Nathan Thomas** *March 10, 2017 00:27*

For the record it was the 2 phillips head screws on my machine...in case some has the same question down the line


---
**Abe Fouhy** *March 10, 2017 08:34*

Did tightening the belt resolve the issue?


---
**Don Kleinschnitz Jr.** *March 10, 2017 15:44*

**+Nathan Thomas** See this post with a link to a video. I have same problem so will be interested in your solution.



[plus.google.com - Well I got a lot of: .... "Don, do you ever do anything with your laser but t...](https://plus.google.com/113684285877323403487/posts/LnK1AVbMFgk)


---
**Wolfmanjm** *March 10, 2017 18:56*

if it isn't loose belts, then another one came to mind... I know that if the axis are not 100% perpendicular to the incoming laser path, then as the axis move the beam does change its position. I think Don has mentioned this. so maybe the wobble is due to the beam moving slightly as it exits the final mirror?


---
**Russ “Rsty3914” None** *March 11, 2017 22:01*

I had to add a bracket to keep the idler bracket square, too much tension on idler will knock the belt off...


---
**Nathan Thomas** *March 12, 2017 12:04*

**+Abe Fouhy** yes, tightening the 2 phillips head screws seemed to resolve the problem 


---
**Nathan Thomas** *March 12, 2017 12:08*

I'm going to run more tests today, but the last couple were ok. Weird thing was it only seemed to happen at that spot (about 4" out from top corner). I will check squareness again as suggested above but all seemed square when it arrived. The belts were a little loose I guess, but wasn't extremely loose like I saw some ppl post when they first got their machines. If I don't have probs again today I'll consider that the resolution. 


---
**Nathan Thomas** *March 12, 2017 12:18*

Also there seems to be a relationship I hadnt realized before  between speed - power - belt tension. Always knew S/P relationship but belt tension is another because as I tightened it I had to adjust S/P to get a good engraving. So that's something else to consider.


---
**Abe Fouhy** *March 12, 2017 21:25*

Fyi - Standard belt tension on pulley devices is 1/64" × the length from pulley centers. 


---
**Don Kleinschnitz Jr.** *March 12, 2017 22:45*

**+Abe Fouhy** what is that measurement of? 


---
**Abe Fouhy** *March 12, 2017 22:49*

Standard belt tension for machines using pulleys, this value is how much deflection of the belt you should have if you put your finger on the belt and push down on the belt in the middle of the distance between pulleys 


---
**Don Kleinschnitz Jr.** *March 12, 2017 22:58*

**+Abe Fouhy** thanks that what I figured. 


---
**Abe Fouhy** *March 12, 2017 23:01*

**+Don Kleinschnitz** You bet, just glad to help in some small way here after all the awesome help


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/X3mAzUFYKeg) &mdash; content and formatting may not be reliable*
