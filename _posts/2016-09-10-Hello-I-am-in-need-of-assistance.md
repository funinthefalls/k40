---
layout: post
title: "Hello. I am in need of assistance"
date: September 10, 2016 23:12
category: "Discussion"
author: "Rod Presnell"
---
Hello.  I am in need of assistance.  I recently purchased the k40 laser engraver from eBay.  When I got the machine the power supply was bad.  I got a replacement from the seller.  My problem is the power supply is not the same and there were no wiring diagram.  I rewired it based on the positions of the wires on the original power supply.  I now have power to the power supply and the power switch and led strip lights work.  Nothing else works.  If I push the test button on the mother board on the power supply the laser fires.  Can anyone help me with my problem?





**"Rod Presnell"**

---
---
**greg greene** *September 10, 2016 23:31*

post a pic of the power supply as there are different models and we need to know which one you got


---
**Rod Presnell** *September 11, 2016 00:13*

This pic is the new power supply.

![images/5fc2061c8b17ba07f8f7e4ba7c02aa7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5fc2061c8b17ba07f8f7e4ba7c02aa7d.jpeg)


---
**Rod Presnell** *September 11, 2016 00:14*

This is the original power supply.

![images/9e0a13c8d5d83f12949a8465609d01b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e0a13c8d5d83f12949a8465609d01b3.jpeg)


---
**Ariel Yahni (UniKpty)** *September 11, 2016 00:31*

What exactly does "nothing works". What are you doing exactly. Are you absolutely sure the cables are where they suppose to be? Go again one by one.


---
**Rod Presnell** *September 11, 2016 00:37*

Nothing else means the amperage meter the fire and test  buttons do not work.


---
**Scott Marshall** *September 11, 2016 03:06*

Middle Green connector -

Pin 1 Gnd

Pin 2 P+

Pin 3 K+

Pin 4 Gnd 

Pin 5 (pot wiper, 0-5v)

Pin 6 5v Power



Enable Switch pins 1 & 2

Test Switch pins 1  & 3

Pot Pins 4,5,6



If you have a separate wire going to your M2nano, be careful, there are both 24 and 5v lines that you can't mix up, or you'll fry the M2nano and both endstops if you put 24v on the 5v line. 

(pins 1-4 of the right hand connector are 24v,Ground, 5V and LO (laser fire, active low) Beware they are REVERSED at the M2nano.



If you pull the cover and connector shells, you'l see this is the exact same power supply, but with different connectors installed. It seems the K40 people like to switch them up just to keep us guessing, but it's the EXACT same power supply, and you could solder the correct connectors right in if it were worth it. (it's not)



If you need any more help, send me a message at

 ALL-TEKSYSTEMS.com 

and I'll do the best I can to help you get running again.



Scott


---
**Rod Presnell** *September 11, 2016 21:04*

Thanks Scott.  That helped out alot.  I traced all the wires and was able to hook everything up correctly.  One question I have is, when you press the test laser button, does the laser switch need to be pressed down as well?


---
**Ariel Yahni (UniKpty)** *September 11, 2016 21:10*

**+Rod Presnell** Yes laser switch is the "ON" button, depressed the laser will not fire


---
**Scott Marshall** *September 11, 2016 21:31*

Glad you got it running!

Cool day, 2 lasers back up...


---
**Rod Presnell** *September 11, 2016 23:12*

Now I just have to figure out how to use the software.


---
*Imported from [Google+](https://plus.google.com/103834600584593086520/posts/RuiLJn9gdJH) &mdash; content and formatting may not be reliable*
