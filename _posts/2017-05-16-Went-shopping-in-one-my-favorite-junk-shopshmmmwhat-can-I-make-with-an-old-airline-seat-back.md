---
layout: post
title: "Went shopping in one my favorite junk shops...hmmm...what can I make with an old airline seat back?"
date: May 16, 2017 01:23
category: "Object produced with laser"
author: "Mike Meyer"
---
Went shopping in one my favorite junk shops...hmmm...what can I make with an old airline seat back? Plenty of good leather there...need a new wallet; might as well see what I can laze together...Couldn't quite figure out the sewing thing with multiple layers of leather, but it's was fun playing with a new medium.



![images/6171606249dea3884a692fceb12a3e74.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6171606249dea3884a692fceb12a3e74.jpeg)
![images/5153cd7db17887207cdd9765020de5fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5153cd7db17887207cdd9765020de5fd.jpeg)
![images/f6dcd112dc4688d008f863ceee6d407b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6dcd112dc4688d008f863ceee6d407b.jpeg)

**"Mike Meyer"**

---
---
**Ned Hill** *May 16, 2017 01:29*

Nice.  **+Yuusuf Sallahuddin** is your man for leather related questions.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 16, 2017 01:45*

That's pretty nice with the contrasting colours. Did you hand stitch or machine stitch it?


---
**Mike Meyer** *May 16, 2017 02:29*

I machine stitched it with wifey's entry level Singer, but as you can see, my sewing ability will not get me a job in tailor shop.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 16, 2017 06:37*

**+Mike Meyer** Still a reasonably nice job. Try with a thicker thread (embroidery thread is decent thickness) or even handstitch (with two blunt needles). If you're interested in tutorials for it I can source some decent ones for you.


---
**Andy Shilling** *May 16, 2017 12:17*

I agree with **+Yuusuf Sallahuddin**​ thicker thread maybe go as far as nylon bonded thread as much more durable. If the leather is really thick try punching holes with a red hot needle first, makes life much easier.


---
**HalfNormal** *May 16, 2017 12:54*

There is also upholstery thread. It is quite durable too. 


---
**Andy Shilling** *May 16, 2017 13:01*

**+HalfNormal**​ that's what I was talking about with nylon bonded, it's what is used for top stitching leather on sofas and auto trimming.


---
**HalfNormal** *May 16, 2017 13:03*

**+Andy Shilling** Thanks for clarifying. Never too old to learn! ;-)


---
**Andy Shilling** *May 16, 2017 13:08*

**+HalfNormal**​ I'm just glad 24 years of being in the upholstery trade had enabled me to help somebody. Just take it as thanks for you guys teaching me how to use my laser.


---
**Mike Meyer** *May 16, 2017 13:13*

Ah...so tell me Andy; is it really possible to machine sew leather with a dinky little machine or do you have to use a serger? As I am by nature a dumpster diver, I've got some more repurposed leather ideas, but can't justify a serger...


---
**Andy Shilling** *May 16, 2017 13:34*

**+Mike Meyer**​​ I personally wouldn't try it if the leather is really thick or if you need multiple layers but if you take your time you may be able to stitch it on a domestic machine. 



I'm presuming your in the usa, so not sure if you could find an old hand turned singer or something in a yard sale as the trend to be very hard wearing and as it's hand operated you can slowly punch the needle through the leather.



Another option is to skife the back of the leather in the areas your want to stitch. This is the process of removing thin layers of the suede side with a sharp blade to make the leather thinner on those parts making it easier to stitch.


---
**Mike Meyer** *May 16, 2017 14:31*

My thanks to everyone for the feedback...now if I can avoid sewing my fingers together...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 16, 2017 14:36*

Another option for joining leather is lacing. Using a 2-3mm wide piece of thinned out leather. Can give a really nice effect & hides edges of the leather quite well.


---
**Andy Shilling** *May 16, 2017 14:36*

You'll only do it once 😉


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/FkPNKB98gMm) &mdash; content and formatting may not be reliable*
