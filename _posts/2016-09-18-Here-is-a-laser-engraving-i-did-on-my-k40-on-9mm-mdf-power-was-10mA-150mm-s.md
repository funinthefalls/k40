---
layout: post
title: "Here is a laser engraving i did on my k40 on 9mm mdf, power was 10mA @ 150mm/s"
date: September 18, 2016 14:48
category: "Object produced with laser"
author: "Camby Crafts"
---
Here is a laser engraving i did on my k40 on 9mm mdf, power was 10mA @ 150mm/s

![images/9b07e9b400fc4926eef31b38c83376b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b07e9b400fc4926eef31b38c83376b8.jpeg)



**"Camby Crafts"**

---


---
*Imported from [Google+](https://plus.google.com/106976397512104337515/posts/a1jVfETmrmu) &mdash; content and formatting may not be reliable*
