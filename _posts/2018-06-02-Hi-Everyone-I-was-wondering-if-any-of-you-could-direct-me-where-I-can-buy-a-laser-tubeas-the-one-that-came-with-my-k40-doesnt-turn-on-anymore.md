---
layout: post
title: "Hi Everyone, I was wondering if any of you could direct me where I can buy a laser tube?...as the one that came with my k40 doesnt turn on anymore..."
date: June 02, 2018 20:46
category: "Original software and hardware issues"
author: "Andrei Voicu"
---
Hi Everyone, 

I was wondering if any of you could direct me where I can buy a laser tube?...as the one that came with my k40 doesnt turn on anymore... 



Thank You.





**"Andrei Voicu"**

---
---
**Don Kleinschnitz Jr.** *June 02, 2018 21:08*

Do you know it's the tube not the LPS?


---
**Andrei Voicu** *June 03, 2018 06:41*

I don't know what the LPS is, but the laser wont turn on anymore... thing is i used it a lot in the last 6 months, probably more than it's life expentacy of 1000 hours... that's why i think it's the laser... also before it stopped working it started doing a weird sound, like a bee


---
**Don Kleinschnitz Jr.** *June 03, 2018 13:33*

The lps is the laser power supply. It is the high voltage for the tube. It often fails before the tube. 

That sound is often the High Voltage Transformer in the LPS going.



Take a look at this guide to troubleshooting and see if its something you can use to find out if the supply is working.



<b>DO NOT open or tinker around in the LPS it outputs lethal voltage.</b>



[drive.google.com - Troubleshooting A K40 Laser Power Subsystem 1.0.pdf](https://drive.google.com/file/d/1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh/view?usp=sharing)






---
**Chuck Comito** *June 03, 2018 15:54*

**+Andrei Voicu**  typically the tube will start to depreciate and it is noticeable as far as power output and color. A good laser tube when fired will appear pink or purple in color. When it is dying it will appear white. You'll also notice that it takes a lot more passes to cut than it did when it was new. Based on the "bee" sound you noticed I'd say that **+Don Kleinschnitz** is correct in a power supply issue. When mine went out it was also making that sound. That sound is the fly back transformer failing. 


---
*Imported from [Google+](https://plus.google.com/113571233865169446047/posts/FEUSSdfiYX4) &mdash; content and formatting may not be reliable*
