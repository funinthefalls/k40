---
layout: post
title: "Ok, i was writting an essay on what doesn't work since I installed my smoothieboard but a list will be easier to understand: -Laser does not fire unless I press the test fire button (and it then fire even when it shouldn't.)"
date: April 19, 2016 14:53
category: "Smoothieboard Modification"
author: "Jean-Baptiste Passant"
---
Ok, i was writting an essay on what doesn't work since I installed my smoothieboard but a list will be easier to understand:



-Laser does not fire unless I press the test fire button (and it then fire even when it shouldn't.) PWM seems to work though.

-Y axis is going in the wrong way when using LaserWeb but is ok when using PronterFace, it goes down to 60mm, and then go up when it should go down.

-LCD does not work, not a problem atm.



Beside that everything seems to be ok and movements are smoother than ever (I get some vibrations but it's ok).



I removed the potentiometer and the test fire button, they are not necessary when using Smoothieboard





**"Jean-Baptiste Passant"**

---
---
**Jim Hatch** *April 19, 2016 15:54*

What software are you using to drive it?


---
**Stephane Buisson** *April 19, 2016 16:08*

+Jean-Baptiste, hola pas si vite 'papillon'.

le pot sert toujours comme limiteur de courrant max. afin de preserver ton tube. on est d'accord la gestion de l'intensite se fait par le pwm, mais pas pour le test fire. de plus tu peux toujours afiner manuellement durant un job.

un %(pwm)x%(pot), pas si mal enfin de compte de le garder.

j'ai pas mal bagarer avec des boucles de masses. j'ai opter pour revenir directement sur le pwm avec un level shifter, ce qui a resiolu le pb.

pour le LCD, les cheap chinese version on inverser les connecteurs.

inverser Y est facile directement dans le config file.

as tu essayer Visicut ?

bon courage, et ++


---
**Alex Hodge** *April 19, 2016 16:18*

Man, google doesn't do a very good job translating french!


---
**Stephane Buisson** *April 19, 2016 16:33*

**+Alex Hodge** I know sorry, I have the same problem with italian on daily basis.



nothing particular, just comment about ground loop, advise to keep the pot as current max ceiling to save the tube. and chinese LCD invert the connectors.


---
**Jean-Baptiste Passant** *April 19, 2016 16:37*

**+Jim Hatch** I use LaserWeb

**+Stephane Buisson** Effectivement, je vais remettre le Pot dans ce cas. Pour le LCD c'est ce que je m'imaginais, en le branchant a l'envers j'ai le retroeclairage. logique...

Pour ce qui est du moteur, je l'inverse pour qu'il fonctionne dans LaserWeb ou PronterFace ? Pourquoi la logique n'est pas la même dans les deux cas ? Encore merci de ton aide !

**+Alex Hodge** Non, google trad ca ne marche pas a tout les coups :(


---
**Stephane Buisson** *April 19, 2016 17:00*

**+Jean-Baptiste Passant** branche dans l'ordre, usb to power the smoothie puis le k40.

apres tu peux debrancher usb si tu veux (5v sur k40 alim). j'ai du garbage sur lcd si pas dans cet ordre. si pas usb pas de contraste tu vois rien. RJ45 pour acquerir l'ip qui s'affiche sur lcd, (sinon tu vas galerer a faire le setting dans visicut).

tu peux inverser au moteur, ou dans smoothie config. je trouve bizarre que pronterface and laserweb soit dans un sens different.


---
**Alex Hodge** *April 19, 2016 17:45*

**+Stephane Buisson** No worries. I'm just super interested in this particular conversation and the translation is just terrible. I'm waiting for my middleman board so I can finish wiring up my MKS-SBase board (smoothie based). I was hoping to gain some insight from this conversation is all. I don't expect you guys to converse in English for me, I'll just post my questions when they come up and hope you two will be able to help me out! :)


---
**Jim Hatch** *April 19, 2016 20:39*

**+Alex Hodge**​ I'll be doing the same when my Smoothie 4x arrives so I'm keeping an eye on everyone else's experience.


---
**Stephane Buisson** *April 20, 2016 07:04*

OK guys, I am sure Jean Baptiste will not worry to keep on in english, as it seem we all speak this language.


---
**Jean-Baptiste Passant** *April 20, 2016 07:54*

If anyone is interested, once I get things sorted out I can take some photos of the whole setup and wiring.


---
**Alex Hodge** *April 20, 2016 14:30*

Thanks Jean Baptiste. I'm sure that will be very helpful. It seems like a lot of us are deciding to switch out our controller right now and most of us with a smoothie based board of some kind. 


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/8fiUp2DijyD) &mdash; content and formatting may not be reliable*
