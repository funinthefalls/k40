---
layout: post
title: "A coworker plays the ukulele so I thought it would be fun to make one for her"
date: November 08, 2016 17:19
category: "Object produced with laser"
author: "Jeff Johnson"
---
A coworker plays the ukulele so I thought it would be fun to make one for her. This is the first draft but it's been tweaked some in the files. There are now 5 layers instead of 4. The strings are held in by tooth picks and you can resize the holes for your needs. 

The .CDR file has 3 pages laid out for 200x100 sheets and it will make 2 ukuleles. I did it this way because I buy my plywood and MDF pre cut in 12x12 inch squares and cut them into 12x8 and 12x4 pieces. I have a lot of 12x4 inch pieces laying around and I'm trying to use them. There are no instructions with the file but it's pretty obvious. Just be careful to line up the stacked pieces as precisely as you can.

[https://drive.google.com/open?id=0B751t-yy-x-nWnNiOUhManRibGM](https://drive.google.com/open?id=0B751t-yy-x-nWnNiOUhManRibGM)

![images/6c99e46d8ac0597bca5e7e1d83b5d31f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c99e46d8ac0597bca5e7e1d83b5d31f.jpeg)



**"Jeff Johnson"**

---
---
**Jonathan Davis (Leo Lion)** *November 08, 2016 17:33*

I'm betting with a bit of tweaking it could be a very much usable instrument.


---
**David Cook** *November 28, 2016 17:06*

so cool




---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/Qkobnx9dgq8) &mdash; content and formatting may not be reliable*
