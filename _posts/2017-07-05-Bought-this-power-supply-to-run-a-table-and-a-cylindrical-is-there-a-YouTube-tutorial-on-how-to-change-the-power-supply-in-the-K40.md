---
layout: post
title: "Bought this power supply to run a table and a cylindrical is there a YouTube tutorial on how to change the power supply in the K40"
date: July 05, 2017 20:48
category: "Modification"
author: "William Kearns"
---
Bought this power supply to run a table and a cylindrical is there a  YouTube tutorial on how to change the power supply in the K40





**"William Kearns"**

---
---
**Steve Clark** *July 06, 2017 00:28*

What PS? Have a picture? Info?


---
**William Kearns** *July 06, 2017 00:43*

**+Steve Clark** ![images/2b3cc7a205f0b301fcf69163daceab57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b3cc7a205f0b301fcf69163daceab57.jpeg)


---
**William Kearns** *July 06, 2017 00:44*

And replace with this![images/f6624e02651361b868e4449e383d5571.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f6624e02651361b868e4449e383d5571.png)


---
**Steve Clark** *July 06, 2017 01:14*

OK, when you say you replaced it are you meaning your using it for a Z axis table? I'm not well versed in  using that for the rotary hooked up to the control. That said, do you have an aftermarket controller such as  Cohesion3D? I know that can be done with the C3D.



I do have a z axis table hooked up and using the same or similar power supply to power it but I'm just running it in a manual up and down push button mode until I have time to convert it over to my C3D.



This may be getting beyond me to help or even comment. I'm a little concerned as messing with the K40 PS is very dangerous as it has very high CAN KILL voltages coming out of it to driving the laser tube.  




---
**William Kearns** *July 06, 2017 01:19*

**+Steve Clark**  I do have a C3D board haven't installed it yet but Ray suggested that the old ps would be inadequate to power both Z and rotary 


---
**Don Kleinschnitz Jr.** *July 06, 2017 01:42*

**+William Kearns** I am guessing that you only want to replace the 24vdc portion of the LPS, leaving it in place for the Laser HV???


---
**William Kearns** *July 06, 2017 01:44*

**+Don Kleinschnitz** Yes that is what I want to achieve 


---
**Steve Clark** *July 06, 2017 03:39*

I'm interested in this also...


---
**William Kearns** *July 06, 2017 03:42*

**+Steve Clark**  I think this is the answer the 24volts needs to be supplied from another source ![images/a4c24ec4576987938274005fcfc68d00.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a4c24ec4576987938274005fcfc68d00.png)


---
**William Kearns** *July 06, 2017 03:43*

So I got this to supply power to the adapters![images/07e8725d10d9597a78998d1f319d9d99.png](https://gitlab.com/funinthefalls/k40/raw/master/images/07e8725d10d9597a78998d1f319d9d99.png)


---
**Don Kleinschnitz Jr.** *July 06, 2017 13:18*

**+William Kearns** the stock LASER POWER only provides 24V @ 1 amp so another stepper (Z or rotary) cannot be added to it. Most add a 24VDC supply and run everything on it as I think the diagram above intends. Where did the above diagram come from?



The 5V needed for the adapter enable lines could come from the 5V on the LPS or 5v on the C3D board if that is indeed 5V?



Make sure that the 24v and its associated ground are run from the supply to each load, do not daisy chain. One ground to frame at the supply.



Check in with **+Ray Kholodovsky** ......


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 13:24*

Already had this chat yesterday, thoroughly lectured on "ways to blow the lpsu", and yes we have a wiring diagram.  


---
**Don Kleinschnitz Jr.** *July 06, 2017 13:34*

**+Ray Kholodovsky** ok, just trying to help.....


---
**Ray Kholodovsky (Cohesion3D)** *July 06, 2017 13:37*

Yes, please continue helping! Just telling you what we already did. 


---
*Imported from [Google+](https://plus.google.com/114761217771223959474/posts/81HBoo518DL) &mdash; content and formatting may not be reliable*
