---
layout: post
title: "Finally finished my solar powered wind spinner"
date: November 03, 2018 14:07
category: "Object produced with laser"
author: "Mike Meyer"
---
Finally finished my solar powered wind spinner. Turned out to be kind of a complicated little bugger, but I pushed it across the finish line. Now that it's all clean and shiny, I don't want to put it outside! LOL!



![images/1f7944b72d1f00f415a77d8a0cac4c1b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f7944b72d1f00f415a77d8a0cac4c1b.jpeg)
![images/dbc80abae95464aec7d789bc041198f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dbc80abae95464aec7d789bc041198f8.jpeg)
![images/1658b3129b89126232af693fb7284a35.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1658b3129b89126232af693fb7284a35.jpeg)
![images/9884eb78ab01fe0003001c8b8dda556b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9884eb78ab01fe0003001c8b8dda556b.jpeg)

**"Mike Meyer"**

---
---
**James Rivera** *November 03, 2018 18:29*

Nice! Does it shine at night?


---
**Mike Meyer** *November 03, 2018 21:35*

Sure does; look at the 4th photo...There are a set of brushes that I installed in the reverse side of the prop which allows it to spin and light up...you can see the 2 copper rings in a couple of the pictures...took a while to sort that one out.


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/Q6RaEc341Hy) &mdash; content and formatting may not be reliable*
