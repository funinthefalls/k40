---
layout: post
title: "I'm running through my first K40 mirror alignment and I've quickly discovered that the beam is barely hitting the inside edge of the Y axis mirror (most of the beam is hitting the metal frame around the mirror) and it's also"
date: March 31, 2017 14:13
category: "Modification"
author: "Bob Buechler"
---
I'm running through my first K40 mirror alignment and I've quickly discovered that the beam is barely hitting the inside edge of the Y axis mirror (most of the beam is hitting the metal frame around the mirror) and it's also got a bit of a scattered pattern to it.



The first issue I will probably be able to fix by adjusting the mirror and/or mount position, but the second issue (scatter) is probably a bad fixed mirror, because when I test the fixed mirror the beam is a clean dot. It only scatters when bouncing off that mirror. I'll try pulling it out and cleaning it but I'm not hopeful. 



Which leads me to my question: Can anyone recommend a good source for quality replacement mirrors? Anyone found better mounts, while we're at it?





**"Bob Buechler"**

---
---
**Joe Alexander** *March 31, 2017 14:26*

I've heard hard drive platters can make comparable mirrors for free, otherwise i'd go to [lightobject.com - LightObject](http://lightobject.com) or eBay.


---
**Bob Buechler** *March 31, 2017 15:13*

I'm frankly shocked anything actually made it to the lens at all.![images/275d15b73abfbad5a49a9638e2b57c54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/275d15b73abfbad5a49a9638e2b57c54.jpeg)


---
**Jim Hatch** *March 31, 2017 15:18*

LightObject has mirrors. The price is low enough that it's not worth the effort of trying to make my own.


---
**Bob Buechler** *March 31, 2017 15:58*

**+Jim Hatch** As I've never bought replacements for the K40 before, what size and type should I get?


---
**Craig “Insane Cheese” Antos** *March 31, 2017 16:18*

They should be 20mm, and iirc the stock head is 12mm lens. I gather a lot of people switch to the light object head which is 18mm lens.


---
**Bob Buechler** *March 31, 2017 16:20*

Yeah. Re the lens, I did too. Currently running the LO air assist with an 18mm lens.



LO lists three different material types for 20mm mirrors: Gold plated, Moly, or Si. Which to choose? 


---
**Craig “Insane Cheese” Antos** *March 31, 2017 16:23*

Mo usually seems to be recommend because it's a bit tougher, Si because it's slightly more reflective. Not sure about the gold.


---
**Bob Buechler** *March 31, 2017 16:23*

I assume then that there are trade offs to consider as well?


---
**Craig “Insane Cheese” Antos** *March 31, 2017 16:26*

Probably less reflection/weaker. I've only just got my K40 and am waiting on the shipping for my parts :( 


---
**Jim Hatch** *March 31, 2017 17:18*

I think it's 20mm but I'd have to check my machine.



Gold is your basic low power (a 40W is a low power laser) machine mirror, pretty easy to make so is pretty cheap (half the price of alternatives).



Mo are good for higher power - as Craig said, tougher mirror. But the 40W doesn't really get much out of that - the beam intensity of a 100W is what makes the extra durability useful.



Si mirrors are slightly better at lower powers in terms of transmissivity than Mo mirrors, but like Mo about twice the cost of a gold mirror. If you're just doing one and starting out, I'd go with gold. Once you decide what you'll do with the machine and start to upgrade it I'd switch them all to Si so you get full benefit of the improved optics otherwise you're still hampered a bit by the other 2 in the beam path.



Or upgrade one at a time but realize it'll be the last one that seems to make the biggest difference.


---
**Bob Buechler** *March 31, 2017 19:06*

Thanks, **+Jim Hatch**. Great advice. I'm already at the upgrade stage... I will mostly be cutting with this, so the added power gain, however slight, will be welcome. I will probably upgrade them to Si's one at a time over the next few weeks (depending on the shipping cost). 


---
**Bob Buechler** *March 31, 2017 19:07*

FWIW, LO offers a (currently out of stock) K40 optics upgrade kit that includes 20mm mirrors, so I'm pretty sure 20mm is correct.


---
**Bob Buechler** *March 31, 2017 19:25*

FYI: Found these on Amazon. Even if they're lower quality, they'll still be better than the stock mirrors. They're cheaper per piece than LO, and shipping included via Prime (plus I can return them via Amazon's policies if they suck, arrive scratched, etc), and I have them on Monday, which is nice. Anyway, thought I'd share the find:



[amazon.com - Amazon.com: TEN-HIGH 3 pieces Diam 20mm Silicon mirrors for CO2 Laser Cutting](https://www.amazon.com/gp/product/B01N2K25QM/)


---
**Jim Hatch** *March 31, 2017 20:14*

Good find. 


---
**Bob Buechler** *April 03, 2017 03:42*

After typical newbie trial and error, I've made a lot of progress on the stock optics while I wait for my upgrade set to arrive. 



Turns out the stock mirrors are predictably subpar. Heavy oxidation and some caked on substance that even Goo Gone couldn't dissolve. Thankfully the center of the mirrors aren't impacted so with some cleaning and proper rotation, I was able to get a serviceable surface to work with, assuming I solved alignment.



After a few hours of tinkering, I was able to at least get a full strength beam into the lens, but it's not yet straight (cuts aren't straight down, but have a slight angle to them). 



To get the beam to center, I had to raise both the fixed and Y axis mirrors slightly. The fixed needed about 1mm, and the Y needed about 4. Logically, that probably means I also have to raise the laser head as well so I don't have to angle the beam from the Y mirror so significantly to land it in the center of the head's aperture. It's that angle that I think is causing the cutting problem. 



But, seeing as I'm new at this and have just been self-researching and experimenting as I go, I welcome all corrections and suggestions. 



The first person who markets some miracle solution to streamline alignments will make a mint. But I guess even if you mounted a laser pointer diode on the front of the tube, the exact laser path is still so variable that it probably wouldn't help make it much faster.



Anyway, after today's round of iterations, I can now cut 5mm birch ply in about 3 passes at 250mm/min at 30% power in LW4 (with true max capped at 80% on the machine). 



Unfortunately, the remaining alignment issues are limiting the laser's useful area to about the closest third of the total K40 cutting area (100mm x 100mm ish). After that the beam pretty rapidly fades off. Still work to be done, clearly. Assuming I'm even doing it right at all, of course. 


---
*Imported from [Google+](https://plus.google.com/+BobBuechler/posts/JzgHj1EGsxm) &mdash; content and formatting may not be reliable*
