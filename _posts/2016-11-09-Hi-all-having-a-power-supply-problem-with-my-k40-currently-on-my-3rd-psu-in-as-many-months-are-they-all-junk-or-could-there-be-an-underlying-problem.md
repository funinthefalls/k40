---
layout: post
title: "Hi all, having a power supply problem with my k40, currently on my 3rd psu in as many months, are they all junk or could there be an underlying problem?"
date: November 09, 2016 22:48
category: "Original software and hardware issues"
author: "Andy Parker"
---
Hi all, having a power supply problem with my k40, currently on my 3rd psu in as many months, are they all junk or could there be an underlying problem? Thanks in advance﻿





**"Andy Parker"**

---
---
**Anthony Bolgar** *November 09, 2016 23:00*

3 in 3 months seems a little excessive. I would start looking for the root cause in places other than the PSU. First thing to check for is arcing anywhere along the length of the high voltage circuit. This will fry a PSU quite quickly.


---
**Anthony Bolgar** *November 09, 2016 23:01*

**+Don Kleinschnitz** any thoughts?


---
**Andy Parker** *November 09, 2016 23:03*

no arcing, they just start giving a little squeal at the start of a cut then randomly die after a few weeks.


---
**Paul de Groot** *November 09, 2016 23:29*

Andy these psu's don't have current protection.  So if one of the steppers is taking too much current  then the 24v diode shorts and the whole thing tanks. So check if the carriage runs smoothly in x and y axis. If not correct it. The good news is that you can repair the power supply and you could wire a 1 A fuse in the 24v line to protect it. Where do you live? I'm in Sydney so happy to help if you are located near me otherwise you need to find someone closer to you.


---
**Andy Parker** *November 09, 2016 23:46*

It's just the hv that seems to quit, the carage still runs but the laser stops firing.  I still have one of the faulty supplies so if I can fix it I would love  some more info. In Cheshire U.K. So a bit far to nip over to Sydney :) thanks for the offer though.








---
**Don Kleinschnitz Jr.** *November 09, 2016 23:50*

Can you take one of the supplies cover off and take pictures of the components. Look for something charred. 


---
**Andy Parker** *November 10, 2016 00:09*

I will take some photos in the daylight tomorrow. I didn't see any charred or swollen components but I did just check the internal fuse and have a quick glance over the rest of it


---
**Paul de Groot** *November 10, 2016 01:12*

**+Andy Parker** you're welcome☺


---
**Andrew Brincat** *November 10, 2016 09:38*

The squeel you mentioned is what my old k40 was doing when it was leaking power somewhere. I would check cables and insulation 


---
**Andy Parker** *November 10, 2016 18:28*

Just added a short vid of the tube, i have also noticed that when it happens the monitor on my pc blacks out. They are both on the same ring main but not on the same socket. I have also taken a few pics of the inside of the last psu that i will post shortly.


---
**Andy Parker** *November 10, 2016 19:10*

Pics as promised ![images/2b6020d999550637a67b91bfbfd72a46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b6020d999550637a67b91bfbfd72a46.jpeg)


---
**Andy Parker** *November 10, 2016 19:11*

![images/48d657fea8f353647399a2f7ed44bc91.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/48d657fea8f353647399a2f7ed44bc91.jpeg)


---
**Andy Parker** *November 10, 2016 19:12*

![images/7f8b80d5c51b5f7c56c8bf92c25c04af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f8b80d5c51b5f7c56c8bf92c25c04af.jpeg)


---
**Don Kleinschnitz Jr.** *November 10, 2016 19:52*

As you said nothing obviously failed from the picture.

Wish I lived closer...

Have you measured the 5 and 24v?

Does the red pwr led come on when you plug it in?

Did you replace the drive transistors I.e. those on the back side of the board.

Do you have an oscilloscope?




---
**Andy Parker** *November 10, 2016 19:59*

I haven't measured the 5 and 24v as I assumed it was the flyback the power light does come on and it happily drives the stepper motors, just no output to the tube.  I don't have anything fancier than a multimeter unfortunately. I have not replaced anything on this psu, its straight out of the machine. Thanks  again


---
**Paul de Groot** *November 10, 2016 20:22*

**+Andy Parker** the 5 and 24v are driven off one circuit and the high voltage circuit is dependent on the 24v circuit. The red led indicates that the driver ic works. Measure as **+Don Kleinschnitz** says the 24v first, if that doesn't work then measure with an ohm meter whether the big fat diode close to the 24v 5v socket is a short in both directions. If so then replace it with a new one, i can send you one if needed. Btw i do have the exact same power supply😀 Repaired it more then once 😞


---
**Don Kleinschnitz Jr.** *November 10, 2016 20:37*

BTw you really should do this with it connected to your laser tube and stay away far from the output. 

I say this to be ultra safe. But if you are only testing the DC and you have nothing but the AC connected to the input you should be ok. 


---
**Don Kleinschnitz Jr.** *November 10, 2016 20:54*

It's a stab in the dark. It's possible that the 24v is not the right value, drives the steppers but not the HV section. 

It's strange the transistors are on the bottom and their solder connections looked a bit cold. 


---
**Paul de Groot** *November 10, 2016 21:30*

**+Don Kleinschnitz**​ The 24v section is fed to a 7812 regulator which turns it into 12v. This is fed to the driver ua494 chip which drives the hv section. If the 24v breaks every goes dead. 


---
**Paul de Groot** *November 10, 2016 21:33*

The mosfets are ideed soldered in that weird position but they did that so they can be screwed to the bottom of the psu case. There is a small thermal insulation between the mosfets and the case. Not my ideal way of engineering rather would use a isolated heatsink. But hey it's made in China 😁


---
**Don Kleinschnitz Jr.** *November 10, 2016 22:08*

**+Paul de Groot** I was imagining it wasn't dead just not 24v.


---
**Andy Parker** *November 10, 2016 23:00*

**+Paul de Groot** what values should i be getting from that diode? 


---
**Paul de Groot** *November 10, 2016 23:20*

The diode next to 7812 regulator shouls be in just 1 or less than 1 ohm in one direction and infinitive in the opposite direction. If short in both directions than its broken and must be replaced


---
**Andy Parker** *November 11, 2016 08:52*

Thanks Paul, i will try and get it tested later on today if i get the chance. I will let you know how it goes :)


---
**Don Kleinschnitz Jr.** *November 11, 2016 13:10*

**+Andy Parker** can I impose on you again? I am researching LPS's for the community and I discovered that even without an actual supply I can trace the circuits if I have pictures that show me enough of the top and bottom of the board. 

The picts you sent enabled me to realize that I can do this. I wanted to fill in what we are missing from the inputs on **+Paul de Groot** schematic but I cannot see some of the part #'s and labels on the top of the board. Can you take a few reasonably close photos that shows the board from above and at angles so that I can see the labels and part #s. I will post these pictures along with the other information to the group.


---
**Andy Parker** *November 11, 2016 15:13*

No problem,  if you can highlight the parts you would like better pics of I will do my best to get the photo you need. I have to say though that I can't read any of the numbers without the aid of a magnifying glass!  Gotta love getting old 🤓


---
*Imported from [Google+](https://plus.google.com/100121420236877030725/posts/iqg1zkeh8iK) &mdash; content and formatting may not be reliable*
