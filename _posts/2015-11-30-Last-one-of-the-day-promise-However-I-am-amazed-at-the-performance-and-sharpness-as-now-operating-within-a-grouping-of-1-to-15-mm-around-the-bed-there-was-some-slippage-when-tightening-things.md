---
layout: post
title: "Last one of the day promise! However I am amazed at the performance and sharpness as now operating within a grouping of 1 to 1.5 mm around the bed (there was some slippage when tightening things)"
date: November 30, 2015 23:03
category: "Hardware and Laser settings"
author: "DIY3DTECH.com"
---
Last one of the day promise! However  I am amazed at the performance and sharpness as now operating within a grouping of 1 to 1.5 mm around the bed (there was some slippage when tightening things).  Could not be happier!





**"DIY3DTECH.com"**

---
---
**Stephane Buisson** *December 01, 2015 09:15*

Not the  last of the day but the last post of the week !

Use comments, don't do a new post each time.


---
**Stephane Buisson** *December 01, 2015 09:16*

**+DIY3DTECH.com**  (your last chance to understand)


---
**DIY3DTECH.com** *December 01, 2015 12:40*

Sorry than I won't post here any longer I don't see the issue.  If you think there is commercial value here isn't I just like sharing the videos and do this on other forums (for CNC & 3D print) and they don't have issues so I am at a loss why the problem as YouTube provides a button to share to this forum (since I am a member).  So don't worry it will be my last post period here don't need the grief... 


---
**Coherent** *December 01, 2015 14:16*

That's too bad. I found the posts and the videos informative. I can understand the goal of combining posts to stick with specific subjects, but generally these were all different. This seems to be a very well mannered group who has no negative comments, arguments or feedback which is  a nice to see contrast to many such group discussions.  DIY3TECH seems to have taken quite a bit of time to make these videos and share them with us. I see no personal gain on his part, just a willingness to share.  Is the site limited on web space? Is there cost per post to the group starter/owner? There are no options to search the forum etc or really categorize posts. I see this format as more of a "blog" than a forum and to dissuade any active member from postings that are applicable unfair to other members that appreciate them.


---
**Stephane Buisson** *December 01, 2015 16:47*

**+Marc G**  You could see a community as a public space for chat, or as a place to find informations and idea. if informations are over diluted, you just can't find anything, removing the main substance. Trust me it take a lot of time to keep things together.

I try to explain to DIY3DTECH, he can't see the point, and he is gone by himself. good luck to him for promoting his channels.


---
**Nick Williams** *December 02, 2015 03:11*

I agree with Marc. I have found the posts by DIY... To be useful and informative. To my knowledge there are no commercial products being sold. I am at a loss, these groups live and die based on content. To chase off such an enthusiastic member seems counter productive to the community??????


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/eAk7fLXxbei) &mdash; content and formatting may not be reliable*
