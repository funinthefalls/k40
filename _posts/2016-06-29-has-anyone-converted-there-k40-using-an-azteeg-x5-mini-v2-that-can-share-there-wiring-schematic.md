---
layout: post
title: "has anyone converted there k40 using an azteeg x5 mini v2 that can share there wiring schematic?"
date: June 29, 2016 05:00
category: "Discussion"
author: "Derek Schuetz"
---
has anyone converted there k40 using an azteeg x5 mini v2 that can share there wiring schematic?





**"Derek Schuetz"**

---
---
**Stephane Buisson** *June 29, 2016 11:01*

**+David Cook**  maybe ... (not sure about v2)


---
**David Cook** *June 29, 2016 14:21*

Hello,  I am not using the X5 anymore, but here is the Schematic I put together. [https://drive.google.com/file/d/0ByNecjb0RD-TbTUydkM0SUJ1cGM/view?usp=sharing](https://drive.google.com/file/d/0ByNecjb0RD-TbTUydkM0SUJ1cGM/view?usp=sharing)


---
**Derek Schuetz** *June 29, 2016 14:25*

**+David Cook** that is basically perfect my only question is how do I connect the end stops since it is only 1 wire each that share a ground? Also the wiring for a level shifter I have never used one prior


---
**David Cook** *June 29, 2016 14:35*

Hi Derek,  here is the guide I followed for hooking up the level shifter  [https://learn.sparkfun.com/tutorials/bi-directional-logic-level-converter-hookup-guide](https://learn.sparkfun.com/tutorials/bi-directional-logic-level-converter-hookup-guide)



as for the single ground, you can make a Y-connection.  My laser had the endstops on a ribbon cable,  so I unsoldered the ribbon cable connector from my Moshi Board and just made a Y-connection from the GND pin  to the X5.



I blew-up my X5 while probing the PWM signal while it was running  ( my probe shorted 2 pins and the magic smoke blew out of the top of the processor lol )   now I'm using a smoothie clone board


---
**Derek Schuetz** *June 29, 2016 14:47*

**+David Cook** I have my machine currently hooked up to ramps and my ribbon cable all wired up. on ramps you only use the one  x and y enstop wire then run a 5v wire and ground to somewhere else on the board


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/2FQNK1cJwM2) &mdash; content and formatting may not be reliable*
