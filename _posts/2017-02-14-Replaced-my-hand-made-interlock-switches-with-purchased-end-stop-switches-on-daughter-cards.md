---
layout: post
title: "Replaced my hand made interlock switches with purchased end stop switches on daughter cards"
date: February 14, 2017 18:41
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Replaced my hand made interlock switches with purchased end stop switches on daughter cards.



Design, fab and installation added into this post:



[http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html](http://donsthings.blogspot.com/2016/11/k40-s-interlock-breakout-board.html)



Cooler yet get a load of the embedded 3D SketchUp viewer at the end of the blog post..







![images/8fbe10f75de455f0fcac24dd73e94c46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8fbe10f75de455f0fcac24dd73e94c46.jpeg)
![images/befacea084203222272cf120be93bb31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/befacea084203222272cf120be93bb31.jpeg)

**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/cVHPZ3ENVdZ) &mdash; content and formatting may not be reliable*
