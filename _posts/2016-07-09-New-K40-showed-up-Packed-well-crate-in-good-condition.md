---
layout: post
title: "New K40 showed up. Packed well, crate in good condition"
date: July 09, 2016 00:35
category: "Discussion"
author: "Joe Spanier"
---
New K40 showed up. Packed well, crate in good condition. Laser looked good. After checking some details its not the best k40 I've seen. But certainly not the worst lol. 



![images/462139e860ab9676f3c684f37bc52234.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/462139e860ab9676f3c684f37bc52234.jpeg)
![images/04640e0358589e6aa9944c04f19a65b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04640e0358589e6aa9944c04f19a65b7.jpeg)
![images/96836c2ac3bb7b578d58934b6bfde94f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96836c2ac3bb7b578d58934b6bfde94f.jpeg)
![images/2de21bb67fd5fce25f1ddf4762a09066.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2de21bb67fd5fce25f1ddf4762a09066.jpeg)
![images/1e2cd0d69909a8ab065cc47fcc7220ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e2cd0d69909a8ab065cc47fcc7220ff.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-JcPU1fNshZY/V4BGw18B5xI/AAAAAAAA2NM/B4ZVdz98dlQAH8mFajUJb1U_L4j23Bp2A/s0/VID_20160708_165418.mp4.gif**
![images/128d909550b580fad5c3ea38131218f5.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/128d909550b580fad5c3ea38131218f5.gif)
![images/017163585689630b6adbcb06a7faa781.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/017163585689630b6adbcb06a7faa781.jpeg)

**"Joe Spanier"**

---
---
**Gary McKinnon** *July 09, 2016 01:06*

Do you have the nano board ?


---
**Joe Spanier** *July 09, 2016 01:22*

whaz that. Been out of the k40 game a while.


---
**Joe Spanier** *July 09, 2016 01:29*

yup I have the nano board


---
**Gary McKinnon** *July 09, 2016 01:29*

Cool, it's the best so far.


---
**Gary McKinnon** *July 09, 2016 01:31*

This group is hard to search because Google Communities isn't great for communities! Standard forum software is far superior.


---
**Joe Spanier** *July 09, 2016 01:34*

It will probably stay in the laser until the kids go to bed. Then Smoothie board goes in. This is my LaserWeb Test bed  #yearofthelaser  !!!!


---
**Gary McKinnon** *July 09, 2016 01:54*

**+Joe Spanier**

New hashtag !


---
**Gary McKinnon** *July 09, 2016 01:54*

Going to sleep now, 3am here in UK :) Good luck and Enjoy :)


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/AeJib8j97YV) &mdash; content and formatting may not be reliable*
