---
layout: post
title: "Is there anywhere in the Uk where you can buy the parts needed to upgrade the K40"
date: April 26, 2016 23:05
category: "Smoothieboard Modification"
author: "Tony Schelts"
---
Is there anywhere in the Uk where you can buy the parts needed to upgrade the K40. as in smothieboard or equivalent???





**"Tony Schelts"**

---
---
**Scott Marshall** *April 26, 2016 23:33*

[http://robotseed.com/index.php](http://robotseed.com/index.php)


---
**Stephane Buisson** *April 27, 2016 05:44*

just on the other side of the channel (Brest), i am sure **+Arthur Wolf** will be able to send you one quickly.


---
**Tony Schelts** *April 27, 2016 05:49*

Thanks


---
**Darren Steele** *April 27, 2016 06:54*

Well done Tony, a smothieboard upgrade is on my todo list and I'm always having problems sourcing bits.  I recently bought a new focal length lens from China via eBay ... and am not impressed with it at all :(


---
**Tony Schelts** *April 27, 2016 08:16*

Robotseed don't sell the 3x, anymore is the 4x the next best suggestion.  This is a contemplation rather than a definite decision.  As I have only done woodwork or driving my whole life so electronichs is a bit scarey.


---
**Scott Marshall** *April 27, 2016 14:05*

The 4x is the same except it has one more motor driver installed and some additional goodies worth having. It has 2 heavy duty FET outputs  in addition to the 2 light duty ones on the 3X, and a variety of I/O options not on the 3X. In addition to the Ethernet connnection (worth the price difference right there, as some software available for the Smoothie requires it), it has I2C, Uart and other common serial ports commonly used for talking to other boards like a Arduino, various displays etc you may want to use (I'm thinking a LCD screen and keypad would be nice add-ons, then you could run it as a standalone if you wish.)



The boards are all the same and you CAN add on the components down the road, but most of the work requires surface mount work, which isn't exactly fun even if you do have the equipment. 

I went with a 5X, the price difference was only about $50us over the 3X and it's fully 'Loaded" with all available options installed (5 2A outputs, 5 10A outputs, 5 2A Stepper drivers, and a full compliment of other I/O. 



If I recall correctly,  when I spoke with Mr Wolf, he mentioned they were discontinuing the 3X, so I'd imagine once stock is gone, they will be unavailable (if there are any still floating around). I'm not 100% sure of that, it was a few weeks back, but I know I heard it somewhere.....



I didn't know Arthur Wolf sold them directly, I thought it was Uberclock in the US, and Robotseed in Europe.



If you can get one direct, as Stephane suggested, all the better.



 Mr Wolf is a regular, answering questions almost every day over on the Smoothie forum, you can't get beat that for support. I'm pretty sure I've seen him here from time to time. In any case, that's one reason I went with the Smoothie, I know I can get help if I need it. 



If you're concerned about getting it working, I don't think you can get better support with any other board or Laser controller. I've looked into it, getting ready to swap over, and the only catches are the laser drive and the physical connection to the flat 'FFP' cables, and I may have a source for connectors. I heard of someone making "middleman" boards that you plug the cables into, and they break out into terminal strips. I haven't found them yet....



Scott


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/JPqFokwLvjk) &mdash; content and formatting may not be reliable*
