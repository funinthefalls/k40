---
layout: post
title: "Out of curiousity, is there a visual guide to what's going wrong with your engraves/cuts?"
date: March 31, 2017 01:46
category: "Discussion"
author: "Craig \u201cInsane Cheese\u201d Antos"
---
Out of curiousity, is there a visual guide to what's going wrong with your engraves/cuts? Something like the one that Simplyfy3d have for 3d printing:



[https://www.simplify3d.com/support/print-quality-troubleshooting/](https://www.simplify3d.com/support/print-quality-troubleshooting/)





**"Craig \u201cInsane Cheese\u201d Antos"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 31, 2017 05:23*

Not that I've seen, but it would be handy.


---
*Imported from [Google+](https://plus.google.com/114851766323577673740/posts/CkmCFiL4H5x) &mdash; content and formatting may not be reliable*
