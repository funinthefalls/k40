---
layout: post
title: "I found this chiller at my local university surplus outlet"
date: June 09, 2018 00:04
category: "Modification"
author: "HalfNormal"
---
I found this chiller at my local university surplus outlet. Not bad for $5.00!

Now I need to add a water temp gauge. My simple monitor has now gotten a little more complicated!



![images/55f967a3b5c70bec81b9ea014c36a1f8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55f967a3b5c70bec81b9ea014c36a1f8.jpeg)



**"HalfNormal"**

---
---
**Anthony Bolgar** *June 09, 2018 00:40*

They have any more?


---
**HalfNormal** *June 09, 2018 00:59*

**+Anthony Bolgar** Sorry, this was the only one. Would have bought all they had if they had more!


---
**Jorge Robles** *June 09, 2018 11:33*

Lucky guy!


---
**Timothy Rothman** *June 10, 2018 01:40*

Does it have a controller.  Usually there is a pair of control wires going to a relay.  If it did not come with a controller there are a lot of options available.



For "monitoring" I'm planning on just using a temperature interlock using a NC thermal control switch at 25C.  So anything above 25C will be open circuit.   The controller will be set somewhere between 18 and 20C.  For redundancy you could run a couple in series.  I think most people run all the interlocks from the Water Protect (or test button) with everything in series.



I picked up a couple of pond coolers from a local craigslist ad for $50.  Guy claimed they ran before taken out of service in the ad, but when I asked him he said he could not guarantee they'd work.  He seemed very honest.  First one I got running with remote controller as it was a minor  issue with one of the thermocouples.  The second one had clearly had issues including the relay having water damage and they'ed messed with the wiring.  I suspect I can get the second one running.  Both use refrigerant and are overkill for the K40 application.  I plan on using a modern controller.  




---
**HalfNormal** *June 10, 2018 03:24*

It does not have a controller. I need to hook something up similar to what you said


---
**Eric Lovejoy** *June 10, 2018 10:28*

I found some swag at a university too!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/gHvCPKXx3LV) &mdash; content and formatting may not be reliable*
