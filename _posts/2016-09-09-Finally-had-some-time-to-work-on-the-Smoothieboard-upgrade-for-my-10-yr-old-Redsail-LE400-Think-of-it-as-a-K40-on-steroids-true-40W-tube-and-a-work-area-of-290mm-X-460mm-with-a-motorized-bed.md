---
layout: post
title: "Finally had some time to work on the Smoothieboard upgrade for my 10 yr old Redsail LE400 (Think of it as a K40 on steroids, true 40W tube and a work area of 290mm X 460mm with a motorized bed)"
date: September 09, 2016 01:50
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
Finally had some time to work on the Smoothieboard upgrade for my 10 yr old Redsail LE400 (Think of it as a K40 on steroids, true 40W tube and a work area of 290mm X 460mm with a motorized bed). Managed to make substantial progress, just need to hook up the LCD touchscreen, POT,  PWM input, and set up the config file. Hopefully I can get this baby buttoned up in the next few days. Then it will be time to smoothify my K40 which is currently running a Ramps setup. And of course, work contimues on the LaserWeb test rig (an Openbuilds OX derivative with a sweet 1.5kw water cooled spindle) So many machines, so little time. :)





**"Anthony Bolgar"**

---


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/7uHp7JjQE8a) &mdash; content and formatting may not be reliable*
