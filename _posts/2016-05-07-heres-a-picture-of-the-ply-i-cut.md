---
layout: post
title: "heres a picture of the ply i cut"
date: May 07, 2016 23:10
category: "Discussion"
author: "Dennis Fuente"
---
heres a picture of the ply i cut

![images/853d5943d35862bd7a13df93d700333c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/853d5943d35862bd7a13df93d700333c.jpeg)



**"Dennis Fuente"**

---
---
**I Laser** *May 07, 2016 23:56*

As **+Yuusuf Sallahuddin** mentioned, you should be able to cut through 3mm ply.



How many passes, looks pretty black on the bottom? What sort of table you got in there?



Sounds like you're in the same boat as me, I checked alignment, focal length, cleaned everything, put in new mirrors (homemade), etc and nothing improved it.



Now with a meter I can definitely see there's an issue with the output power of the tube. So it wouldn't have mattered what I did.



A bit of a cross post, but I had read that it can be quite detrimental to leave a tube idle for 6+ months. Can anyone confirm this?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 00:24*

What power settings are you using **+Dennis Fuente**? Seems to me really black on the edges as **+I Laser** mentions. I'd suggest dropping your power (it creates a finer cutting point). Also, what kind of plywood is it? I am using a really cheap plywood. Can you do just a single dot fire (with the Test Fire button) using 4mA power. You can hold it down for say 1 second. I'd like to compare the result of that with what happens when I do a single dot @ 4mA. Maybe do a few different power settings & I will do a comparison test/photo up for you to check. (e.g. 4/6/8/10mA).


---
**Dennis Fuente** *May 08, 2016 01:07*

i can't begain to say how many test i have made trying to cut the ply lots of ruined wood laying about i need to make my parts thats' why i bought it so i don't have to run my expensive to operate CNC mill


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 01:28*

**+Dennis Fuente** Yeah totally understandable. I wonder what region you are in & if there is anyone in this group nearby that could assist/check it out & see what's going on.


---
**Alex Krause** *May 08, 2016 02:54*

**+Dennis Fuente**​ what kind of CNC mill do you have? Just wondering why you consider it more expensive to run than the laser?


---
**I Laser** *May 08, 2016 07:58*

I always run my tests with a very small square or circle up the top of the medium, tweak the settings a few times and then run the job.



You should not have that much issue cutting, as previously mentioned I have the same issues at the moment with one of my machines and it seems to be the tube.



If it's a relatively new machine maybe try contacting the seller? The fact the machine was dusty would make you think it was sitting around for a considerable amount of time!



As asked, how many passes at what power did you achieve the above picture?


---
**Dennis Fuente** *May 08, 2016 16:15*

Hi guys so lets start off with the CNC mill it's a 2002 Lagun 4 axis 5 1/2 HP 3 phase cat 40 pneumatic head mill so for making small wood parts not really the way to go it can certainly do the job but running cost are high, the above ply was cut at 18 MA and 1 pass, the ply is a 3 MM popular wood 3 layer cheap plywood hobby grade, i am in Calif USA the SF bay area, after finding the lens issue the machine seams to be doing better in cutting thanks guys for all your input and help


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 18:53*

**+Dennis Fuente** 18mA seems to be the reason the black charring is so much then. Have a go at much lower settings (now you've got your lens issue sorted) & try something like 10mA @ 25mm/s @ 2-3 passes. See how that goes for you. Hopefully with the lens issue sorted you can get better results.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/3kL8P3pfbnP) &mdash; content and formatting may not be reliable*
