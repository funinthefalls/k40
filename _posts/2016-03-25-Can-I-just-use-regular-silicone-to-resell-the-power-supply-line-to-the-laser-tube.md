---
layout: post
title: "Can I just use regular silicone to resell the power supply line to the laser tube?"
date: March 25, 2016 18:58
category: "Discussion"
author: "3D Laser"
---
Can I just use regular silicone to resell the power supply line to the laser tube?





**"3D Laser"**

---
---
**Scott Thorne** *March 25, 2016 19:31*

Yes...it works better than the runny stuff they send with the engraver.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/8sQrELnbiY6) &mdash; content and formatting may not be reliable*
