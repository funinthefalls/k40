---
layout: post
title: "I can confirm this is compatible with the k40 ribbon"
date: February 06, 2017 18:23
category: "Material suppliers"
author: "Jorge Robles"
---
I can confirm this is compatible with the k40 ribbon. (EU users)



[http://www.tme.eu/es/details/ds1020-01-12bt1/conectores-ffc-fpc-raster-125mm/connfly/](http://www.tme.eu/es/details/ds1020-01-12bt1/conectores-ffc-fpc-raster-125mm/connfly/)





**"Jorge Robles"**

---
---
**Don Kleinschnitz Jr.** *February 06, 2017 21:16*

Sorry but I cannot read all of the specs. Looks the same but do not see any dimensions.



See if this blog post helps you do the comparison. See part #s at the bottom.

[donsthings.blogspot.com - K40 Optical Endstops](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)


---
**Jorge Robles** *February 06, 2017 21:22*

Well, i suppose i have to bet for it anyway, not much lose though. $2 against $20 fare fee. Thanks **+Don Kleinschnitz** 


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/dAeLiwqocxU) &mdash; content and formatting may not be reliable*
