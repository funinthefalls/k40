---
layout: post
title: "Need help. Why its not cut out everywhere?"
date: April 07, 2018 21:31
category: "Object produced with laser"
author: "Lukasz Krawiec"
---
Need help. Why its not cut out everywhere? What im doing wrong? 



![images/dd9a1bd826d60fc1e24814a03543e4a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd9a1bd826d60fc1e24814a03543e4a7.jpeg)
![images/ff316e8b3dc440b687420b9fe4d6caf3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff316e8b3dc440b687420b9fe4d6caf3.jpeg)

**"Lukasz Krawiec"**

---
---
**Phillip Conroy** *April 07, 2018 22:20*

Decrease cutting speed,or Increase power as slight varattions in wood density and or mirror alignment out slightly. 

What setting did you use to cug


---
**Tony Sobczak** *April 07, 2018 22:38*

Glue in wood


---
**Lukasz Krawiec** *April 08, 2018 07:18*

**+Phillip Conroy** 

Thank you for answer

Speed: 25units (mm/s?)

Power; around 50%. 14mV

Material: Plywood 3mm 


---
**Sebastian Szafran** *April 08, 2018 20:40*

I would equally guess poor quality tube and possibly mirrors misalignment.


---
**HalfNormal** *April 08, 2018 21:24*

I would slow the speed way down. 


---
*Imported from [Google+](https://plus.google.com/111883238663104300289/posts/jc9EzjoBMvh) &mdash; content and formatting may not be reliable*
