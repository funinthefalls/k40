---
layout: post
title: "So far, I'm preparing the interlocks for the k40"
date: February 06, 2017 18:19
category: "Modification"
author: "Jorge Robles"
---
So far, I'm preparing the interlocks for the k40. 

What should be the best wire to  cut? PSU 24V, L, IN, 

Relay AC? (only could be done if already got USB input)







**"Jorge Robles"**

---
---
**Andy Shilling** *February 06, 2017 19:12*

You want to use the P+ and G as these arm the laser. It's easy to add additional interlocks using these pins. 




---
**Jorge Robles** *February 06, 2017 19:13*

+Andy Shilling: Between P+ and G? Thanks!


---
**Andy Shilling** *February 06, 2017 19:16*

Yes if you are not sure where they are just follow the two wires from your laser switch.  Use some sort of microswitch on the cabinet door and wire it in series with the laser switch, that way you have to arm the laser and shut the cabinet before the laser will fire.


---
**Andy Shilling** *February 06, 2017 19:19*

 Look up the interlock chapter it explains it far better than I can.

[donsthings.blogspot.co.uk - A Total K40-S Conversion Reference](http://donsthings.blogspot.co.uk/2016/11/the-k40-total-conversion.html)


---
**Jorge Robles** *February 06, 2017 19:22*

I did, just overlook the p+ in the pic (so small) Thanks again ;D


---
*Imported from [Google+](https://plus.google.com/113562432484049167641/posts/RE8dPB7eKTL) &mdash; content and formatting may not be reliable*
