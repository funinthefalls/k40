---
layout: post
title: "Is this going to be a problem, I can't seem to get it to go out the exit tube even if I tilt the machine and rotate the tube"
date: July 11, 2016 14:29
category: "Discussion"
author: "george ellison"
---
Is this going to be a problem, I can't seem to get it to go out the exit tube even if I tilt the machine and rotate the tube.

![images/afc8fd248bffa142942870b28c075e2e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/afc8fd248bffa142942870b28c075e2e.jpeg)



**"george ellison"**

---
---
**Jim Hatch** *July 11, 2016 14:43*

It might if it travels to the metal fitting. Try elevating the water source above your machine and run the pump. 


---
**Stephen Sedgwick** *July 11, 2016 14:43*

That is just in the cooling area that should be fine...


---
**Derek Schuetz** *July 11, 2016 14:45*

That won't cause any issues


---
**Jim Hatch** *July 11, 2016 14:48*

Air bubbles don't move in your machines?


---
**Derek Schuetz** *July 11, 2016 14:51*

Eventually but I fire if with bigger then that 


---
**Jim Hatch** *July 11, 2016 14:56*

I used to do that but when I was messing around with my alignment one weekend I figured I'd just suck it up and get rid of it. Ended up standing the machine on its end and moving the cooling bucket above it until I got rid of the last air bubble. Then put it back and did my alignment.



I figured I want maximum life from the tube and don't want to be surprised by it frying - until I get my next one I'd have limited backup to finish a project waiting for a new tube.


---
**Derek Schuetz** *July 11, 2016 15:06*

An air bubble of that size won't cause any issue


---
**greg greene** *July 11, 2016 15:06*

It will eventually dissolve and move out - no problem when the water gets warm from firing the laser


---
**ThantiK** *July 11, 2016 16:48*

I had an air bubble like that, ran the pump overnight, and it was gone the next morning.  It is safe, and will dissolve on its own over time.  Try to make sure your system is set up so that there is no turbulence/air bubbles introduced into the reservoir.


---
**Craig** *August 24, 2016 09:32*

Well now we know where ThantiK's air bubble went to !


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/fizTQUCrJem) &mdash; content and formatting may not be reliable*
