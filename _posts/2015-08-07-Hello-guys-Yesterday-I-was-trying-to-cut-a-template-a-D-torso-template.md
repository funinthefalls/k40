---
layout: post
title: "Hello guys, Yesterday, I was trying to cut a template, a D torso template"
date: August 07, 2015 14:52
category: "Discussion"
author: "Jose A. S"
---
Hello guys,



Yesterday, I was trying to cut a template, a D torso template. I am trying to mimic the quality of its work with cardboard, it is amazing. But, look at the straight line, is from my machine and the other cut shapes are from the original pattern. My cut line looks wider and burns the material. Do you know what can I do to reduce this thickness of the cut line?...I am using a airbrush compressor to reduce burns during the process. Do I need a mirror with better quality?



Regards



Jose

![images/888bfd9f78cfa9e7bce75190c242fee5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/888bfd9f78cfa9e7bce75190c242fee5.jpeg)



**"Jose A. S"**

---
---
**Fadi Kahhaleh** *August 07, 2015 16:10*

This would most likely indicate a wrong distance between the object and laser. (i.e your setup is not using the actual focal length you are either too close or too far from the laser (lens) and the beam is not really focused)


---
**David Wakely** *August 07, 2015 16:51*

I'd say **+Fadi Kahhaleh** is right. Does look to be focus issue. to find the "sweet spot" remove the cutting bed and place a sloped piece of wood. one end should be below the cutting bed and the other end should be just high enough for the cutting head to pass over safely. once you've done that get the machine to cut a straight line along the wood. The part of the line that is the thinnest and cleanest is the focus point. measure the distance from the cutting head to that point and bingo you have you foul length! for ease cut yourself a price of wood or acrylic the same height and use it as a guide. Good luck :)


---
**Jose A. S** *August 07, 2015 17:52*

Thank you for your advice, guys!...I will try it!...Another question just to be sure my lens is in the right position...the concavity of the the mirror on the head your be up or down? 


---
**Fadi Kahhaleh** *August 07, 2015 18:40*

Concave part should be up. (afaik)


---
**Haotian Laser** *August 08, 2015 02:48*

Hi Jose, what is the thickness of your  template? We have one mixed co2 laser cutting machine, which can cut 1.5-2mm stainless steel and non-metal materials.  Another one is cnc plasma metal cutting machine, which can cut 20mm stainless steel. Do you have interest? My email is amber@htlasercnc.com, pls feel free to contact me.


---
**Jose A. S** *August 08, 2015 15:11*

**+Haotian Laser** the thickness is 2mm, but this is cardboard not sheet metal. Which is the price for those machines?


---
**Clifford Livesey** *August 09, 2015 18:46*

Have you made sure it not trying to cut a fine line as this is too thick. Needs to be 0.001


---
**Haotian Laser** *August 10, 2015 01:24*

hi jose, thanks for your asking. Can you tell me your email id? I will send machine offer to your email. My email is amber@htlasercnc.com and skype is live:18365894301, pls contact me directly. Thanks


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/7VHxR1JbwL9) &mdash; content and formatting may not be reliable*
