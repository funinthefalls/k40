---
layout: post
title: "Just a play around testing etching on the reverse side of a cheap $2 99mm2 mirror"
date: June 05, 2016 09:25
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just a play around testing etching on the reverse side of a cheap $2 99mm2 mirror.



I stuffed up my alignment (as I had already set it in the file & decided to add extra in CorelLaser to the refer X & Y when it was already done).



Settings used...



To start with, I used 4mA @ 100mm/s @ 5 pixel steps.



Then it didn't look like it had done anything, so I did again @ 4mA @ 100mm/s @ 1 pixel step.



Then it still didn't look like it had done anything so I ran 3 passes of 10mA @ 100mm/s @ 5 pixel steps.



Still didn't look like it had done anything so I decided to pick it up. Turns out it had done something & probably had after the first run.



So, then I did the rose...



1 pass @ 10mA @ 100mm/s @ 5 pixel steps. Could probably have done with either 2 passes or slower speed or lower pixel steps as some of the detail didn't quite get transferred through the reflective backing (only looks like it etched half way through the backing in a few of the thinner spots of the design).



Finished it all off with a quick coat of red acrylic paint over the etched area on the backing.



![images/3fd7d711c582d60ae7de8c1f6f831ff3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3fd7d711c582d60ae7de8c1f6f831ff3.jpeg)
![images/3c1e096aa154acd866d35b0a5186ddff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3c1e096aa154acd866d35b0a5186ddff.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jim Hatch** *June 05, 2016 13:37*

Great idea with the paint to bring out the etch!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 20:38*

**+Jim Hatch**  I got the idea from someone ages ago who did this process manually by hand & only just remembered it. Thought it would be much easier to do with the laser than by hand. Cool thing is you could use multiple colours of paint quite easily for a really nice effect.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/26dVCEnSLDN) &mdash; content and formatting may not be reliable*
