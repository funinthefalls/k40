---
layout: post
title: "Hi all, in the process of adding some safety features to my K40 (The newer one with the big stop button) and I'd like to install a switch to kill the laser if the lid is lifted"
date: October 10, 2017 06:24
category: "Modification"
author: "Anthony Coafield"
---
Hi all, in the process of adding some safety features to my K40 (The newer one with the big stop button) and I'd like to install a switch to kill the laser if the lid is lifted. The wiring example I found online is for the older version with a physical test switch, but mine is wired to the board. Does anyone know which wire I should cut to install it?





**"Anthony Coafield"**

---
---
**Anthony Bolgar** *October 10, 2017 17:25*

WP is how it is marked on my PSU


---
**Paul de Groot** *October 10, 2017 21:41*

I believe that is the right wire loop. Maybe that's why it is called wp?


---
**Anthony Bolgar** *October 10, 2017 22:28*

It stands for Water Protect


---
**Anthony Coafield** *October 10, 2017 23:55*

Thank you. I'll give it a shot!


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/emMYfeW1gv8) &mdash; content and formatting may not be reliable*
