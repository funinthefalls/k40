---
layout: post
title: "Hi. A couple of months ago, I modified my K40 with a Cohesion3D mini, and replaced the panel on the machine to include a LCD display, an ampmeter and other upgrades"
date: April 11, 2018 14:20
category: "Modification"
author: "syknarf"
---
Hi. A couple of months ago, I modified my K40 with a Cohesion3D mini, and replaced the panel on the machine to include a LCD display, an ampmeter and other upgrades.

This is the result. I upload the design on Thingiverse, and you can get it here: [https://www.thingiverse.com/thing:2722965](https://www.thingiverse.com/thing:2722965)



![images/c2798872870950ee0a6483ca40c961a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c2798872870950ee0a6483ca40c961a2.jpeg)



**"syknarf"**

---


---
*Imported from [Google+](https://plus.google.com/113055105026792621247/posts/UJqZ9j6XxHs) &mdash; content and formatting may not be reliable*
