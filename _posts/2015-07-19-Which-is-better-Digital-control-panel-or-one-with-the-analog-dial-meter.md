---
layout: post
title: "Which is better? Digital control panel or one with the analog dial meter?"
date: July 19, 2015 13:29
category: "Discussion"
author: "12vLedLights"
---
Which is better?  Digital control panel or one with the analog dial meter?  Is there anything I should look out for when buying one of these machines.  I know (or think I know) what I'm getting into, but before I pull the trigger and order one, I would like to know which one is better?  Both are pretty close to the same price.  Is the digital one a newer model or newer board?

![images/cfc2b0c920bd70539003dcb7916aa11f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cfc2b0c920bd70539003dcb7916aa11f.jpeg)



**"12vLedLights"**

---
---
**David Richards (djrm)** *July 19, 2015 14:26*

I agree the digital board looks more modern but I don't think it is any easier to use. What made me decide to go for the digital option was I thought it would be more recent. I have added an analogue meter into the laser current circuit to check the current as the percentage numbers on the digital scale are uncalibrated and meaningless(70% is about 18mA). I think I was ?lucky? and got a system with Corel+Laserdraw software and not Moshidraw which nobody seems to have a good opinion of. Having said that I fully expect to replace all the control electronic before long but I haven't found it absolutely necessary just yet.  n.b. There are serial input terminals to the front panel controller board but I haven't found out what they can be used for yet. I think they are to control the current setting but it may not be enabled in the front panel software - it isn't in the laserdraw software either. hth David.


---
**Stephane Buisson** *July 19, 2015 19:10*

I do agree with **+david richards** ,  the digital version don't bring much, more importantly you should go for the one with Corel+Laserdraw software . that will make it usable before a possible hardware update. With the update the control panel will go, so it doesn't matter digital or not.



the latest "analogue"one (vue meter) have a pwm laser psu, witch is the important feature (see post 11/11/2014) for hardware update.


---
**Al Tamo** *July 19, 2015 19:50*

Yo tengo la analógica, comprada recientemente también, y también funciona con laserdraw y corel.


---
**Fadi Kahhaleh** *July 20, 2015 01:24*

I couldn't agree more with what has been said by **+Stephane Buisson** and **+david richards** 

Software and PWM PSU (plus for example the availability of limit switches, clear plexi or red plexi...etc) makes more towards leaning for one and not the other.



I'll give you 3months before you venture into creating your own panel and possibly do some hardware upgrades as well :D


---
**12vLedLights** *July 20, 2015 02:06*

Thanks for all the response and info.  I dont think it will be 3 months though.  Already know I will be upgrading soon ;)


---
**Joey Fitzpatrick** *July 20, 2015 12:53*

I agree with everyone who has commented.  Personally,  I purchased the one that was cheapest.  This saved me some money for the upgrades I wanted to make.  I bought the analog version with a Laserdraw mainboard and a PWM Power supply.  I added a digital current meter that is more accurate than the one that comes on the digital display K40.(I dont believe that the digital display that comes with the K40 is a current meter,rather a percentage display)  I bought a digital  mA current meter from Lightobject.com for $9.00.  It was nice to have a working Laser Cutter to make the panels and other parts before upgrading to  a Ramps1.4/Marlin setup.  Good Luck with your upcoming Purchase.  I noticed that these units are selling on ebay now for around $350 here in the US.


---
**Jason Barnett** *July 20, 2015 20:20*

They are both crap. Just buy the cheapest one and plan on replacing the controller with RAMPS or, better yet, a Smoothieboard and you will be happy. 

I got the Moshidraw version and didn't last two weeks before stealing the controller from my 3d printer, to install in my K40. Extremely happy that I did.


---
**Eric Parker** *July 23, 2015 19:08*

Analog, for certain.  It actually has the milliammeter, which is the important bit.


---
**12vLedLights** *July 23, 2015 22:16*

Ordered the analog with laserdrw.  Should be here Monday.


---
*Imported from [Google+](https://plus.google.com/112959548641483506197/posts/Du1gbkUF9tS) &mdash; content and formatting may not be reliable*
