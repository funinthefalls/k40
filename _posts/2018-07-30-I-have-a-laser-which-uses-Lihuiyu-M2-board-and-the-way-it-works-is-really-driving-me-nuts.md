---
layout: post
title: "I have a laser which uses Lihuiyu M2 board and the way it works is really driving me nuts"
date: July 30, 2018 22:02
category: "Original software and hardware issues"
author: "George Walker"
---
I have a laser which uses Lihuiyu M2 board and the way it works is really driving me nuts.



Everything works great, except one thing - each time I turn it on:

1.) it never returns to 0, 0 (upper-left corner in my case) upon turning it on and during initialization

2.) it moves 8-15 cm to the right each time the machine is turned on making the machine think it's origing is moved too, which leads 

3.) translations repeat each time the machine is turned on, and the only thing that helps is if I move the head manually to real 0,0 ; otherwise, it eventually hits the end (since sensors are only on upper and left side) and wants to go over



I played with the settings and the translations do change from time to time, but there is really no rule. I change settings to one parameters, then another, and when I return to previous settings, it behaves completely different than first time it was on those very same settings.



Here is the link to the video: 
{% include youtubePlayer.html id="CQrzYw9qiyk" %}
[https://youtu.be/CQrzYw9qiyk](https://youtu.be/CQrzYw9qiyk)



I played with all the settings, but to no avail. I tried even using another computer, but it didn't cut it.

I am fairly certain that the problem lies somewhere in the laser itself, since it manifests only during the initialization, so my guess is either the cables on limit sensor or stepper motor have gone bad or the motherboard is faulty. I doubt it is a power supply, but who knows?



Anyway, did anyone ever run into such problem and how should I proceed in dealing with this problem?



#K40brokeorigin #K40brokeinitialization

.. Machine type: K40

.. Controller type: Lihuiyu M2

.. Firmware: stock [will update]

.. Firmware config: [will update once I get back home]

.. Problem tenacity: it was always present since the start 

.. Suspected source of problem: motherboard, PSU, sensor; came with machine

.. Symptoms: after turning the machine on, the machine zero is moved and the machine thinks its machine zero is further away than it really easy

.. Photos/video: 





**"George Walker"**

---
---
**Kelly Burns** *July 31, 2018 00:20*

Why do you say firmware is “Smoothie, GRBL, etc”?  That’s a stock controller running proprietary firmware.   



Are they optical or mechanical limit switches?  


---
**Torsten M** *July 31, 2018 07:40*

The movement looks quite similar to my effect two weeks ago. Check the connections to and from the m2 board. I have had one cable not connected inside a plug. 


---
**George Walker** *July 31, 2018 10:05*

**+Kelly Burns** Sorry, I forgot to edit that. :/

The sensors seem to be electromagnetic, but I'll get back to you as soon as I return home as I am away from home now.


---
**George Walker** *July 31, 2018 10:06*

**+Torsten M** I have checked the connections on motherboard side and everything seemed fine, however I am no electrician, so I can't really tell much. I will definitely check all the cables as I am running out of options anyway. How did your problems manifest?


---
**Torsten M** *July 31, 2018 13:09*

Here is my story.... Check out the helpful checklist there are linked there. Doesn‘t matter to be electrician or not. To measure some voltages and and connections from point A to B is very helpful and mandatory sometimes. [plus.google.com - In fact of a stupid mistake I have broken my K40 :-( First of all does anybod...](https://plus.google.com/104585701321643651218/posts/am7JiNqo4sV)


---
*Imported from [Google+](https://plus.google.com/100284103816028575354/posts/8UwzCcQXh3M) &mdash; content and formatting may not be reliable*
