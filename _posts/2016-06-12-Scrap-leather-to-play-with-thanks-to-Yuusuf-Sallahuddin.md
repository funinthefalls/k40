---
layout: post
title: "Scrap leather to play with thanks to Yuusuf Sallahuddin !"
date: June 12, 2016 23:37
category: "Material suppliers"
author: "HalfNormal"
---
Scrap leather to play with thanks to **+Yuusuf Sallahuddin**!



To all the USA yanks here, I purchased a 5 lb box of leather scraps from [http://www.brettunsvillage.com/leather/scrap/scraps.htm](http://www.brettunsvillage.com/leather/scrap/scraps.htm) for $35.00 delivered.

They sent a great variety of pieces in various colors and weight. Will be able to make a few bracelets and key fobs. Here is a pic of what I received.



![images/a2c18cd617e4fbb1d0f7d90ba4c9a5d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a2c18cd617e4fbb1d0f7d90ba4c9a5d5.jpeg)



**"HalfNormal"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2016 01:31*

Beware: some leather finishes (dyes/sealers/etc) will be toxic. Make sure you've got your exhaust cranking.



Also, for leather I tend to hold it down with magnets to keep it taut, else you will have up & downs in the material causing focus issues.


---
**Stephane Buisson** *June 13, 2016 08:35*

for UK, ebay is a good place, got 2kg for £5.5 of good scrap (sofa factory), and £14 for 2Kg of large off cut top italian leather, lot to play with. they both have few tons of it. also buy veneer ...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 13, 2016 10:42*

Just working on a leather "slim card holder", that will suit business cards or credit cards, at the moment. Previous test of it (which I went & lost my design file) allowed it to fit 8 credit cards with no dramas. If you want to have a test on it here is the SVG (set the entire group to 300x200mm before cutting, as there is a 300x200mm blank rectangle surrounding it all). Borders are all set to 0.01mm, so there should be no double cut issues. Paths are all closed, stitching holes have been "punched" out of the main shape. You would just then need to learn how to hand-stitch leather (search saddle-stitching).



[https://drive.google.com/file/d/0Bzi2h1k_udXwRVRlc0NVeFZDc1k/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwRVRlc0NVeFZDc1k/view?usp=sharing)


---
**Bruce Garoutte** *June 13, 2016 18:24*

SAWEET!!


---
**Eric Rihm** *June 13, 2016 20:22*

How thick of leather have you been able to work with? Would 1mm be safe?


---
**HalfNormal** *June 13, 2016 22:23*

Have not had problems with any of the samples I have played with so far.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 02:45*

**+Eric Rihm** I use 1-1.5mm natural vegetable tanned kangaroo hide quite a lot & it is the best out of all the leather I've worked with (for laser cutting). It's however not so good for engraving, as I've nearly always burned straight through the leather or at least 75% of the way through.



Also, I've used 1mm lambskin leather & it's a really great material for laser cutting. Very soft & delicate though, so you need to be aware of stitching holes as they will rip if too close to the edge of the material.


---
**Bruce Garoutte** *June 14, 2016 04:44*

Great tip, I'll have to look into that.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/Szp7UMPSMuF) &mdash; content and formatting may not be reliable*
