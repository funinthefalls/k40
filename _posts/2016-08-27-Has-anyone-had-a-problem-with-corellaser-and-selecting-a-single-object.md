---
layout: post
title: "Has anyone had a problem with corellaser and selecting a single object?"
date: August 27, 2016 06:24
category: "Discussion"
author: "Patrick Weitzel"
---
Has anyone had a problem with corellaser and selecting a single object?  If I have 3 objects on a layer and select one to either engrave or cut all objects are selected when the corellaser box opens.  There is no way to have only a single object engrave.  This is with both x6 and x7 on win 10.  I have another computer set up the same way with win 10 and it works normally.  I've tried uninstalling and reinstalling several times both corellaser and coreldraw with no effect.  Any help will be appreciated.





**"Patrick Weitzel"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 27, 2016 06:45*

There is an option somewhere in the settings for it... In the first icon on the toolbar (CorelDraw Settings) there is an option for Engraving data & I have that set to WMF. Then below it you can choose All Pages | Current Page | Only Selected. Same for the Cutting data. I have it also as WMF & set Only Selected in the dropdown below it.


---
**Eric Flynn** *August 27, 2016 15:50*

Yep, thats it.  Make sure "Only Selected" option is used.


---
**Tony Sobczak** *August 27, 2016 16:27*

Thanks guys. I asked this for.my brother in law. 


---
*Imported from [Google+](https://plus.google.com/117761833986443847873/posts/h7Vp7WimrYh) &mdash; content and formatting may not be reliable*
