---
layout: post
title: "So, I've just checked tracking & supposedly my Smoothieboard is with the delivery driver for delivery today"
date: June 14, 2016 03:20
category: "Smoothieboard Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, I've just checked tracking & supposedly my Smoothieboard is with the delivery driver for delivery today. Yay!



Anyway, I've seen that when people are sorting out the Smoothie things like the PSU & original controller board are usually requested. This is my PSU & M2Nano board.



Is there any other photos that will be necessary to assist me getting the Smoothie hooked up?



Also, what other things are necessary to ensure I don't kill the smoothie? I've seen people talking about a level shifter (3.3v or something?). As I'm not very electrically knowledgeable I'm probably going to need a bit of assistance on this one.



![images/27d5b9a39b2f832bff18538f69a5dc01.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27d5b9a39b2f832bff18538f69a5dc01.jpeg)
![images/b8c3f5deb87b99d8c515b4cbd2cb0b75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8c3f5deb87b99d8c515b4cbd2cb0b75.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Alex Krause** *June 14, 2016 03:30*

You will need a middle man board


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 03:49*

**+Alex Krause** You're going to have to excuse my total ignorance, but what exactly does a middleman board do? From my understanding it is something to help bridge the connections between the existing K40 electronics (motors/endstops/etc) & the smoothie. Is that right?


---
**Alex Krause** *June 14, 2016 03:53*

On the picture of your board the lower left corner of your board is a ribbon cable that contains your X axis motor connector and your limit switch connections... you can either get a middle man board or run a new set of wires to those components


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 04:01*

**+Alex Krause** Oh right, I get it. So the middleman is because I have the ribbon cable instead of modular plugs. I'm thinking middleman is probably the way to go for me, as I intend to have a switchable setup for Smoothie/m2Nano, using **+Scott Marshall**'s plug&play kit that he is developing.



Although, is it necessary to have a middleman with your kit Scott? Or does your kit function as a middleman for the ribbon?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 04:33*

**+Susan Moroney** Ooo... That'd be nice. Although, I have no idea how many plugs I have/need. I'd be happy to go with that deal though. I will take a better photo of the plugs & stuff that came with my Smoothie a bit later & then upload/share with you so we can work out what you need/I need out of it.


---
**Alex Krause** *June 14, 2016 04:39*

**+Yuusuf Sallahuddin**​ I believe **+Susan Moroney**​ is referring to this component... [http://www.digikey.com/product-search/en?KeyWords=A100331-ND&WT.z_header=search_go](http://www.digikey.com/product-search/en?KeyWords=A100331-ND&WT.z_header=search_go) 

Please correct me if I'm wrong


---
**Alex Krause** *June 14, 2016 04:48*

Try this link see what shipping is **+Susan Moroney**​ [http://m.aliexpress.com/item/32295502813.html?tracelog=storedetail2mobilesitedetail](http://m.aliexpress.com/item/32295502813.html?tracelog=storedetail2mobilesitedetail)


---
**Alex Krause** *June 14, 2016 05:12*

[http://au.rs-online.com/web/cp/7188772,7188772P/?sra=p&r=t](http://au.rs-online.com/web/cp/7188772,7188772P/?sra=p&r=t)



Found an Australian supplier I believe


---
**Alex Krause** *June 14, 2016 05:43*

**+Susan Moroney**​ try this [https://littlebirdelectronics.com.au/products/sparkfun-voltage-level-translator-breakout-txb0104](https://littlebirdelectronics.com.au/products/sparkfun-voltage-level-translator-breakout-txb0104)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 06:59*

RS Online are a great supplier for electronic pieces. Have used them in the past for finding specific components for a portable gas-fridge's electronics. Original place quoted something ridiculous to replace the part (like $100) when it was a $5 part from RS.



**+Susan Moroney** I will drop you an email. Thanks.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 07:54*

**+Alex Krause** **+Susan Moroney** I've ordered one of those SparkFun logic converter/level shifter that Alex provided the link for. I should have ordered a couple at the same time, since you will also need one Susan. Just a heads up, there is a "lead time of greater than 1 day (i.e. it's coming in from one of our suppliers)" according to their email. It is actually coming from SparkFun themselves, so you might want to get one before your Smoothie arrives. It's about $6.08 + $7.20 postage (to Gold Coast).


---
**Alex Krause** *June 14, 2016 07:57*

Do you guys have Amazon Prime  there in Australia **+Yuusuf Sallahuddin**​


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 08:26*

**+Alex Krause** Not that I'm aware of. From a quick google it looks like it might be coming soon though:

[http://www.businessinsider.com.au/amazon-prime-could-be-launching-in-australia-with-its-new-top-gear-show-2016-2](http://www.businessinsider.com.au/amazon-prime-could-be-launching-in-australia-with-its-new-top-gear-show-2016-2)


---
**Don Kleinschnitz Jr.** *June 14, 2016 12:07*

**+Yuusuf Sallahuddin**I do not think a Middleman is needed if you use the Scots switching board. 

I am working on the PWM problem for my own machine conversion and you can see my research here:

[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.htm](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.htm)

l  am sorry I have not finished testing so I do not know how it fits your timeline. Other parts of the conversion are also developing on other posts in this blog.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 12:21*

**+Don Kleinschnitz** Thanks Don. To be perfectly honest, all of that discussion on your blog regarding the PWM is way over my current level of understanding. But it's great the work you're doing, as those here who know what it means will have a great reference for designing/modifying solutions. I will probably just use the middleman board temporarily until Scott's solution is ready to ship & then I can switch it out I guess for Scott's solution. Then I'll have a middleman on hand for anyone else in Australia that is in need of one.


---
**Don Kleinschnitz Jr.** *June 14, 2016 12:48*

Sorry to slog everyone through the research and I plan to get all that detail condensed to a final method that anyone can use. 

BTW the LPS interface does not really involve the Middleman it is all about to LS or not and where to connect on what LPS :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 13:24*

**+Don Kleinschnitz** I feel like a total noob reading your posts as it all goes over my head. I have no idea what a LPS interface is or what LS is, etc. This is all a major learning curve for me. Thanks for all your explanations though.


---
**Don Kleinschnitz Jr.** *June 14, 2016 13:48*

**+Yuusuf Sallahuddin**     Gosh I am sorry I should not use acronyms. 

LPS = laser power supply

LS= level shifter

I assure you I am on a steep learning curve myself in regard to this K40... it is all very confusing.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 16:14*

**+Don Kleinschnitz** Thanks for that. Makes sense now.


---
**Scott Marshall** *June 14, 2016 17:49*

Alex,



My Board is rather a "Super Middleman"

.

The Plug-in Kit  (Dubbed "ACR' Aftermarket Controller Retrofit kit)

has ALL the connectors you see on the M2Nano board, and a terminal strip with outputs for your Aftermarket board (Smoothieboard or other).

There is a $15 option where it comes pre-wired with cables and all the Smoothieboard connectors installed and marked.



The ACR board has ALL the connectors, including the FFP flat cable connector on it, thus allowing it to work with ALL M2 Nano equipped lasers, even if it's not a K40.

I'm not sure yet on the older Moshi equipped machines, but am working to insure compatibility with them as well.



All you do is unplug your M2nano, Plug those connectors into the ALL-Tek ACR,

Plug the cables coming from the ACR into your Smoothie (one of the cables from the ACR plugs into the new power supply (part of the kit).



The only actual "wiring" required is to connect the new power supply mains into the K40 mains terminal strip.



That's it, fire it up!



There's a more elaborate kit that allows you to keep your M2Nano and switch between it and the Smoothie or what ever controller you chose.

Coming soon!



Scott


---
**Alex Krause** *June 14, 2016 17:54*

**+Scott Marshall**​ are you including logic level shifting as well on board?


---
**Alex Krause** *June 14, 2016 20:01*

**+Scott Marshall**​ keep me in mind for beta testing I have a smoothie and smoothie Derivative board I can test on


---
**Scott Marshall** *June 15, 2016 00:22*

**+Susan Moroney** Susan, I'm working on verifying the prototype boards for both kits, I expect the production run of  the single  in 3-4 weeks, and the Switchable slightly after that. 

I kind of let it slip here, I was planning on keeping it quiet until I had some ready to ship. I'll have final pricing and details on both boards out soon. 



I posted a "pre-release" summary (if you can call any of my writing "summary") this morning, and haven't hear from anyony on it, I think I may have mis-posted it. I will link it here shortly.


---
**Scott Marshall** *June 15, 2016 00:30*

**+Alex Krause** The K40 ACR (Aftermarket Retrofit Controller kit) will include 2 independent bidirectional Logic level shifters and an 800ma 3.3v power supply.



It will translate PWM outputs of 3.3V boards to 5V for stock k40 supplies and most others. ait will also do the reverse, in the event you have a laser supply that uses 3.3v for a 'fire' command.

The 2nd converter is intended for use with an 'enable' command, should you need it.



The configuration is jumper selectable


---
**Don Kleinschnitz Jr.** *June 15, 2016 12:40*

**+Scott Marshall** what if one doesn't want the level shifter control and want to use open collector/drain from the smoothie to the laser power supply. Also I have various 12V devices in my system ( temp controller) is there 12v available?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 14:47*

**+Don Kleinschnitz** Not sure if you have seens Scott's recent post with plenty of info on his kit/psu's :[https://plus.google.com/116005042509875749334/posts/3zv43d34wJa](https://plus.google.com/116005042509875749334/posts/3zv43d34wJa)



In amongst it he mentions that for one of the power supplies (The Big ONE!) "Standard voltages are 5,12,24 (of course) but adjustable. Extra or special voltages can be added, just ask, we'll build it.".


---
**Don Kleinschnitz Jr.** *June 15, 2016 15:15*

**+Yuusuf Sallahuddin** missed that thanks.


---
**Scott Marshall** *June 15, 2016 15:37*

**+Don Kleinschnitz** The Switchable design work has netted improvements I will add to the ACR (single) board as well.



The level translator count is up to 4. (possibly will add more if there's room- I'd like a couple spares for utility use) and they are jumper selectable to allow setting up in all possible desired combinations.



You can set the jumpers to allow inputs to be translated to accept 5 or 3.3v inputs for laser operate (5v input is straight thru to the factory PSU fire pin), and you can even set it up to send 3.3v to the laser fire in the event you have an aftermarket psu with 3.3v inputs. Both sides of the translators are brought to the terminal strip and can be used for testing with a scope or used as "loose" parts if you have them jumpered out of the Fire circuits)

In addition, for improved versitility, the endstop outputs to the aftermarket board are brought out as both 5v and 3.3v (for the Arduino folks)



And lastly, Yes, while it's not standard, I can do 12v logic translators by changing a few devices on the panel. Haven't seen 12V logic since college. Still have the data books though. I'l even do complete one off custom work, it's exactly what i used to do. It may be a little pricey though if it requires a one off board


---
**Tony Sobczak** *June 15, 2016 20:14*

Follow


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3sMrHbN6WF5) &mdash; content and formatting may not be reliable*
