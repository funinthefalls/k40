---
layout: post
title: "Hi, it seems there are no valid search options in google+ interface, also sorry for bad English, not native"
date: May 01, 2017 13:29
category: "Original software and hardware issues"
author: "Linards Zolnerovichs"
---
Hi, it seems there are no valid search options in google+ interface, also sorry for bad English, not native.



Problem is with my K40 - when switched on or when software initializes laser head stopped returning to its home position, upper left corner, instead of it moves right down by 5-7 cm 2 times and then assumes that it is "home"' . It is still possible to work, moving head by hand to appropriate working position till next switching on or reinitialize. It started slowly, time by time, but now every time. machine is 6 month "old".



 Any ideas how to cure this,  i'm afraid it can progress further... ?





**"Linards Zolnerovichs"**

---
---
**Ashley M. Kirchner [Norym]** *May 01, 2017 16:40*

Clean the optical sensor under the gantry, on the left side. For me, a few short blasts with air was enough. It's also possible that the sensor has failed in which case you'll have to replace it. But start with cleaning first. 


---
**Linards Zolnerovichs** *May 01, 2017 17:55*

can you post picture where actually it is and how it looks? did not even know about such sensor...


---
**Ashley M. Kirchner [Norym]** *May 01, 2017 18:08*

Look behind the motor on the left side, there's a sensor behind it, and there's another underneath it.

![images/a4f1bf96d381df6d292707c08db03292.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a4f1bf96d381df6d292707c08db03292.png)


---
**Ashley M. Kirchner [Norym]** *May 01, 2017 18:08*

![images/0f932266cad4f4c8af8de768703ad8d0.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0f932266cad4f4c8af8de768703ad8d0.png)


---
**Linards Zolnerovichs** *May 01, 2017 18:10*

Oh, that worked! Googled and find pics, cleaned, now ok. Thanks, gracias, merci!


---
*Imported from [Google+](https://plus.google.com/106355299250118504150/posts/YzWH4dgj3nJ) &mdash; content and formatting may not be reliable*
