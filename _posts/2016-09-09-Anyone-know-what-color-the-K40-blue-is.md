---
layout: post
title: "Anyone know what color the K40 blue is"
date: September 09, 2016 19:31
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Anyone know what color the K40 blue is.

"Dodger blue"?

Ideas where to get some ?





**"Don Kleinschnitz Jr."**

---
---
**Jim Hatch** *September 09, 2016 20:02*

I'd take the lid off and bring it to Home Depot to get a color match. Then you can get them to mix some up and you can put it on with a brush or get a sprayer for it.



If you have a hot rod shop nearby you can take it to them and they can color match and get you a powder coat that will match if you want to bake the finish on your new extension box.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 20:19*

**+Jim Hatch** Problem is all the lids are the white colour. Just looking at my machine, there seem to be no removable blue parts.


---
**Jim Hatch** *September 09, 2016 22:04*

Oops. You're right. But the exhaust is blue and that's removable.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 10, 2016 06:24*

**+Jim Hatch** Oh yeah, the exhaust is :) I forgot about that because it's long since gone in the bin haha.


---
**Jim Hatch** *September 10, 2016 13:58*

NP. I forgot the top was white - mine has been off the machine for awhile as I have it torn apart again. 🙂


---
**Don Kleinschnitz Jr.** *September 12, 2016 15:02*

Guess what @ Wallmart

![images/c76a12abe38c983a310e81d2d5871d68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c76a12abe38c983a310e81d2d5871d68.jpeg)


---
**Don Kleinschnitz Jr.** *September 12, 2016 15:02*

Close enough

![images/d1ee1f3f5b93e9fe2403e230f09dc6f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1ee1f3f5b93e9fe2403e230f09dc6f7.jpeg)


---
**Jim Hatch** *September 12, 2016 15:04*

Cool. Nice to know.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 16:54*

Wow, nice match. I'd be interested to see the result after you paint it on. Hopefully the lid colour is close to the actual dried paint colour.


---
**Don Kleinschnitz Jr.** *September 15, 2016 13:34*

I'd say it is a match!

![images/66f99cdac91c74a83a3838ccb67dbf99.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/66f99cdac91c74a83a3838ccb67dbf99.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 15, 2016 13:48*

**+Don Kleinschnitz** Wow, that's spot on. Good find :)


---
**Don Kleinschnitz Jr.** *September 15, 2016 14:09*

That color reminds me of my IBM 360 days. [ibmsystem3.nl - www.ibmsystem3.nl/System360/IBM_system_360_44_LCM.jpg](http://www.ibmsystem3.nl/System360/IBM_system_360_44_LCM.jpg). Just FYI I was 15 years old back then lol.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/jicr7gYPJoC) &mdash; content and formatting may not be reliable*
