---
layout: post
title: "Hi all! long time lurker first time posting, since upgrading to smoothie my engraving while being a million times better quality is either too shallow or takes too long than the old pot and on-off method"
date: June 14, 2016 18:40
category: "Smoothieboard Modification"
author: "Marc Rogers"
---
Hi all! long time lurker first time posting, since upgrading to smoothie my engraving while being a million times better quality is either too shallow or takes too long than the old pot and on-off method. looking at the ammeter it's barely touching 5mA when the pwm is set to 100%. 4hours to engrave an A4 sheet of keyrings when it took 45mins before is quite a difference. Cutting is fine, hits 18mA no trouble. I'm using visicut if that makes any difference. Cheers!





**"Marc Rogers"**

---
---
**Don Kleinschnitz Jr.** *June 14, 2016 22:05*

How is the PWM set during engraving? I don't suppose you have access to an oscilloscope :). Can you tell me how your smoothie is connected to the laser power supply.


---
**Vince Lee** *June 14, 2016 22:33*

The current meter is only going to show you an average value when engraving so I would expect it to be lower unless engraving something consistently black.  Also, when grayscale engraving you might want to tweak the image in Photoshop so the dark areas are truly black or nothing with engrave at the power you set in Visicut.  Lastly, the default smoothie max-speed settings in visicut are really low, which would explain why it takes so long to engrave.  I upped my top speed to 12000 mm / min. I typically engrave near 100% speed (200mm/s) and cut around 5% (10mm/s).  If you increase it, you might also need to tweak your acceleration values in the smoothie configuration to get good smooth motion.﻿


---
*Imported from [Google+](https://plus.google.com/100818067744008604646/posts/9Xr2R5UNjSv) &mdash; content and formatting may not be reliable*
