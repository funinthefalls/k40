---
layout: post
title: "Hi Everyone, First post so please be gentle :) I was given my laser cutter as a gift and after a few days of learning how it all works im excited to get into making some proper things"
date: February 03, 2017 16:09
category: "Discussion"
author: "david mcguinn"
---
Hi Everyone,



First post so please be gentle :)



I was given my laser cutter as a gift and after a few days of learning how it all works im excited to get into making some proper things.



I have tried to cut 3mm acrylic at 20% at 5mm and my laser appears to struggle with this, any Ideas i am using Laser Drw am i stuck to using this application or can Move onto a different application?



Thanks guys in advance.





**"david mcguinn"**

---
---
**Austin Baker** *February 03, 2017 22:01*

Hi! I'm pretty new, too. I have 3mm (cast) acrylic as well and tried cutting it a few weekends ago. How deep is it cutting into the surface? How many times do you have to execute the cut before it gets through? LaserDRW is fine for testing purposes -- I'm still using that and Corel with the Laser Plugin, which is much nicer.


---
**Coherent** *February 03, 2017 22:09*

First welcome!

Cutting issues usually have more to do with your laser lens and mirrors not being in proper alignment or the distance from the material to the lens incorrect than the stock software causing it. If everything is adjusted and aligned, you can try increasing the power and/or decreasing the cut speed. You should be able to cut 3mm without any problems. As far as other applications to drive the laser, you're kind of stuck with what comes with the K40 machines until you upgrade to a different driver board setup. A popular upgrade is a smoothieboard based setup.


---
*Imported from [Google+](https://plus.google.com/115599021767829857606/posts/X9CYo3zjBZz) &mdash; content and formatting may not be reliable*
