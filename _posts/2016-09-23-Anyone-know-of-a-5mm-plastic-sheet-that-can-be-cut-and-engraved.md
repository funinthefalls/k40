---
layout: post
title: "Anyone know of a .5mm plastic sheet that can be cut and engraved"
date: September 23, 2016 07:15
category: "Discussion"
author: "Derek Schuetz"
---
Anyone know of a .5mm plastic sheet that can be cut and engraved. I'm using some ABS sheets I have but cutting leads to warping and gross edges. 





**"Derek Schuetz"**

---
---
**Anderson Firmino** *September 23, 2016 07:27*

Mylar?


---
**Scott Marshall** *September 23, 2016 12:58*

Acrylic is the way to go for Co2 Lasering. Cuts clean, engraves to look like glass.

Available in most any color, opacity, thickness. Search the web.


---
**Derek Schuetz** *September 23, 2016 15:24*

**+Scott Marshall** do they make it that thin?


---
**Scott Marshall** *September 23, 2016 15:46*

Search for "laser materials" on the net, and go through the websites of Epilog and the other laser makers for Laserable materials/characteristics tables. There's dozens of them out there.


---
**Scott Marshall** *September 23, 2016 18:28*

For the thin stuff look for Perspex (UK for Acrylic). I've used 1mm, quite sure they go down from there. I'd start with Ebay, or Ayer Sales. I think they sell it in craft shops like Michaels for crafters. I have some Stencils that cut ok and they look to be mylar (Polyester) 



[grafixplastics.com - LaserPro Laser Cuttable Films - Grafix Plastics](http://www.grafixplastics.com/laserpro.asp)


---
**Gage Toschlog** *September 23, 2016 22:54*

can't get quite that thin but [inventables.com - Inventables](http://inventables.com) has tons of acrylic options 




---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/NfXvCwU9pqf) &mdash; content and formatting may not be reliable*
