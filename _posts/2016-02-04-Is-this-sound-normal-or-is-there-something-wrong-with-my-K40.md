---
layout: post
title: "Is this sound normal or is there something wrong with my K40?"
date: February 04, 2016 17:22
category: "Discussion"
author: "Patryk Hebel"
---
Is this sound normal or is there something wrong with my K40?


**Video content missing for image https://lh3.googleusercontent.com/-YkauPUXoHCY/VrOI23WXLCI/AAAAAAAAAMg/5LLhmkQrs3c/s0/20160204_141114.mp4.gif**
![images/0d6399852b7487c73a73e0ed43da711b.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/0d6399852b7487c73a73e0ed43da711b.gif)



**"Patryk Hebel"**

---
---
**Joseph Midjette Sr** *February 04, 2016 18:18*

Just sounds like the relay firing the laser


---
**Patryk Hebel** *February 04, 2016 20:41*

**+Joseph Midjette Sr** Is that bad?


---
**ThantiK** *February 05, 2016 00:55*

No.  Sound is completely normal, your laser is fine.


---
**Patryk Hebel** *February 05, 2016 01:15*

**+ThantiK** Oh ok. Cheers buddy :)


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/dveXGWH3yuP) &mdash; content and formatting may not be reliable*
