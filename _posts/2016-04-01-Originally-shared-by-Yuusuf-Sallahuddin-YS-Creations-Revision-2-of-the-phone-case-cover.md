---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Revision 2 of the phone case/cover"
date: April 01, 2016 13:10
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Revision 2 of the  #Lumia950  phone case/cover.



 #lumia   #microsoft  



![images/34f863f068ce73d76854a3fe2bbcdab9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34f863f068ce73d76854a3fe2bbcdab9.jpeg)
![images/12d8a92b6c3eaedac0a8d14fa483ae60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12d8a92b6c3eaedac0a8d14fa483ae60.jpeg)
![images/ce60ae5725e9a279eab3fd2b86d94ad2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ce60ae5725e9a279eab3fd2b86d94ad2.jpeg)
![images/b8cde28229dbbf8a725c56d6c92c0342.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b8cde28229dbbf8a725c56d6c92c0342.jpeg)
![images/ee2a8927d7a626d2a917a5aa985bc78a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee2a8927d7a626d2a917a5aa985bc78a.jpeg)
![images/4b416e3153f59cc64817f2d47bfdf191.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4b416e3153f59cc64817f2d47bfdf191.jpeg)
![images/72d232e1a3f48278dbedd1dec6519bc4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/72d232e1a3f48278dbedd1dec6519bc4.jpeg)
![images/ab49f4a06b50148a23ca1efea2532009.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ab49f4a06b50148a23ca1efea2532009.jpeg)
![images/c830249aa417ca6fa796b8674c85b990.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c830249aa417ca6fa796b8674c85b990.jpeg)
![images/5c4ca8d26e1c6c8618a38263115cacb2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c4ca8d26e1c6c8618a38263115cacb2.jpeg)
![images/1ff1654d10162f82730d414c5a20478a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ff1654d10162f82730d414c5a20478a.jpeg)
![images/2711933415aff07f29ca42c4a19cd04b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2711933415aff07f29ca42c4a19cd04b.jpeg)
![images/c796107ab5b5b44c414d9852d4b0701e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c796107ab5b5b44c414d9852d4b0701e.jpeg)
![images/96127252741a86494600a5b78a7420ef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96127252741a86494600a5b78a7420ef.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/JPbVgXg4WCw) &mdash; content and formatting may not be reliable*
