---
layout: post
title: "Jon Bruno Here are the files for the control panel, one is a cdr, the other is a svg"
date: May 15, 2016 08:50
category: "Modification"
author: "Anthony Bolgar"
---
**+Jon Bruno** Here are the files for the control panel, one is a cdr, the other is a svg





**"Anthony Bolgar"**

---
---
**Jon Bruno** *May 15, 2016 14:39*

Thanks Anthony! 


---
**HalfNormal** *May 15, 2016 14:53*

Sweet! This is my next project. Will be taking the best of everyone's efforts to make my own. A big thank you for sharing.


---
**Jon Bruno** *May 15, 2016 17:08*

Its a great design


---
**Anthony Bolgar** *May 15, 2016 21:32*

The CDR has the underly size of the displays in yellow lines. The SVG is ready to cut


---
**Anthony Bolgar** *May 16, 2016 23:48*

Thanks for the compliment Jon. I spent about 20 hours playing with different designs before I was happy with this one. I painted the acrylic black before engraving and cutting with the painted side up Then I painted it white to colour in the engraved linbes, making the text really pop when viewed. Since all the paint is on the back side the front side is smooth, and the paint cannot be scraped or chipped off.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/VkpV8Mkw3yg) &mdash; content and formatting may not be reliable*
