---
layout: post
title: "Hey guys just got my k40 and i'm having a problem"
date: January 30, 2016 01:39
category: "Discussion"
author: "Rickm rickm"
---
Hey guys just got my k40 and i'm having a problem. It seems like the servo is skipping but if the machine is off I can move it freely without problems. here is a video link of my problem and help will be appropriated. Thanks

[https://vid.me/53ri](https://vid.me/53ri)
 





**"Rickm rickm"**

---
---
**Anthony Bolgar** *January 30, 2016 13:18*

sounds like something is caught in the rail track. I would check the rails to see if a little piece of something is trapped in them.


---
**Rickm rickm** *January 31, 2016 03:42*

I had checked the rails and their wasnt any thing that I could see. But I did slow the speed down and it worked much better. I guess I was used to my laser diode cnc that operates its cutting feature faster. Thanks for the replies!


---
**I Laser** *January 31, 2016 09:45*

Has the 'Y' belt got enough tension on it? Sounds like it's skipping across the belt to me. What speed were you trying to cut at? Mine starts losing the plot at 45mm/s...



edit: what Carl said about tube life is true also!


---
*Imported from [Google+](https://plus.google.com/117373418762601161189/posts/WVqMo85A4Eo) &mdash; content and formatting may not be reliable*
