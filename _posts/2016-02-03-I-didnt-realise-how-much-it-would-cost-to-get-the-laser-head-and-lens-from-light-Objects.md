---
layout: post
title: "I didn't realise how much it would cost to get the laser head and lens from light Objects"
date: February 03, 2016 12:55
category: "Discussion"
author: "Tony Schelts"
---
I didn't realise how much it would cost to get the laser head and lens from light Objects. I thought  the 21.00 dollars for 1st class was fine.  Now I have to collect it in the uk and pay another 14.80 pence about another 21 dollars.  Anyway not used to what to expect when buying from abroad. 





**"Tony Schelts"**

---
---
**Scott Thorne** *February 03, 2016 14:07*

Wow...that bites...I see how ordering gets expensive in the U.K.


---
**Stephane Buisson** *February 03, 2016 18:18*

you are unlucky for few pences. duty is due over 15gbp.

depending on the change rate you are just over. so you pay a 8GBP fee to postoffice for the custom charge service. (8+6 vat=14GBP). with correct change rate you would have only pay 21usd. (no vat)

You will now 4 next time.


---
**I Laser** *February 03, 2016 21:15*

Ouch, I just got two in the post to Australia and no extra payable. Arrived fairly quick too.



At the moment anything under $1,000 is exempt, though our wonderful government is trying to change that of course.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/hW1wwD6iCdP) &mdash; content and formatting may not be reliable*
