---
layout: post
title: "Hi, while mounting new air assist I have discoverd that (sotck) lens is little chipped off"
date: February 19, 2018 11:40
category: "Hardware and Laser settings"
author: "Ale\u0161 Tome\u010dek (Alandran)"
---
Hi,

while mounting new air assist I have discoverd that (sotck) lens is little chipped off. I am currently in search of replacement. Does anybody here has expirience with cheap lenses from ebay? I know thjat they definitely won't as good as expensive ones, but I have it only for my hobby projects so I don't really want to invest much.

So are the ebay lenses acceptable trade off, if not, how bad they are?

![images/4e67e2d6454bb475e8303a2fd62de517.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e67e2d6454bb475e8303a2fd62de517.jpeg)



**"Ale\u0161 Tome\u010dek (Alandran)"**

---
---
**Andy Shilling** *February 19, 2018 13:00*

As long as it doesn't move in sure you'll be ok for a while. I have used cloudray before from aliexpress and they may be on eBay to. 


---
**Paul Mott** *February 19, 2018 13:16*

Use that lens for as long as it lasts. When you buy another, spend a little extra to get a decent quality lens.


---
**Aleš Tomeček (Alandran)** *February 19, 2018 13:54*

thanks **+Paul Mott**,  do you have  any first hand expirience with using cheaper quality? Burn increases, or power requirements?


---
**Don Kleinschnitz Jr.** *February 19, 2018 16:22*

**+Aleš Tomeček** [plus.google.com - Decided to order a couple USA ZnSe lenses for my K40 and 60W 5040 lasers. Hop...](https://plus.google.com/+AnthonyBolgar/posts/ekvMLVcUR5P)


---
**James Rivera** *February 19, 2018 18:21*

My lens was chipped worse than that. I ordered a new ZnSe lens from LightObject last night. Obviously, I haven’t tried it yet. But I’m optimistic.


---
**Paul Mott** *February 20, 2018 07:35*

**+Aleš Tomeček** Good quality lenses (as opposed to the cheap far eastern variety) will enable smaller focussed spot sizes and this really does make a significant overall improvement to the work quality (My lenses have all been purchased from II-VI Infrared).




---
**Aleš Tomeček (Alandran)** *February 20, 2018 07:43*

**+Paul Mott** thanks for clarification. Would you mind sharing what lenses exactly have you bought? I am honestly little bit lost on which type should I look up. Only suitable diameter I have found is in plano-convex lenses, and that doesn't seem shaped like the stock ones.


---
**Paul Mott** *February 20, 2018 11:16*

**+Aleš Tomeček** I have;



2 x ZnSe plano-convex of ~2”  focal length.

1 x ZnSe plano-convex of ~4”  focal length.

1 x GaAs plano convex of ~2”  focal length.

1 x GaAs meniscus of ~2” focal length.



I can easily and quickly exchange lens (in it’s holder) for different applications and as a comparison the GaAs meniscus is the best, producing by far the smallest spot size. 




---
**Don Kleinschnitz Jr.** *February 20, 2018 12:36*

#K40Optics


---
**Abe Fouhy** *February 21, 2018 17:11*

**+Paul Mott** When/why do you change focal length?


---
**Paul Mott** *February 22, 2018 09:58*

**+Abe Fouhy** The 'focal length' of a lens determines it's 'depth of field' and it is sometimes handy when cutting deeper into a material such as when producing lithophanes. Increasing the depth of field has the downside of increasing dot size but almost everything laser is a trade-off of some sort.


---
**Don Kleinschnitz Jr.** *February 22, 2018 13:24*

Increasing spot size decreases energy density so there is less

 cutting power.



Increasing depth of focus increases the length of the waist of the beam and can reduce the hourglass effect in thick materials.



[http://www.engraversnetwork.com/files/Choosing-the-right-Laser-lens-tip-sheet.pdf](http://www.engraversnetwork.com/files/Choosing-the-right-Laser-lens-tip-sheet.pdf)

........



[http://support.epiloglaser.com/article/8205/42831/focus-lens-101](http://support.epiloglaser.com/article/8205/42831/focus-lens-101)

[https://www.troteclaser.com/en-us/knowledge/tips-for-laser-users/selection-of-the-right-lens](https://www.troteclaser.com/en-us/knowledge/tips-for-laser-users/selection-of-the-right-lens)/





youtube.com - Laser cutter Advanced Tutorial 04 How focal length changes affect the cutting effect


{% include youtubePlayer.html id="YNfNyOqUG0c" %}
[https://www.youtube.com/watch?v=YNfNyOqUG0c](https://www.youtube.com/watch?v=YNfNyOqUG0c)





Note subtitles :)



Some math for fun: [https://www.rp-photonics.com/focal_length.html](https://www.rp-photonics.com/focal_length.html)


---
*Imported from [Google+](https://plus.google.com/+AlesTomecek/posts/adRCWJ1RKPa) &mdash; content and formatting may not be reliable*
