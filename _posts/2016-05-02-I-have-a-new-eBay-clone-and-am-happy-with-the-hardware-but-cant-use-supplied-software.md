---
layout: post
title: "I have a new eBay clone and am happy with the hardware but can't use supplied software"
date: May 02, 2016 05:13
category: "Software"
author: "Paul Townsend"
---
I have a new eBay clone and am happy with the hardware but can't use supplied software.

It is installed on a WinXP laptop and Corellaser with Coreldraw seem to work, as does Laserdrw. 

I can create drawings but can't see how to send them to the laser cutter, should I have a printer driver installed?

I also can't import test drawings into either program. I have tried several drawings off web in both .ai and .crv or .crd format but the programs both return messages about file incompatibility.

Advice please





**"Paul Townsend"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 02, 2016 05:38*

I don't recall installing a driver at all. Just installed CorelDraw & then installed CorelLASER thereafter. You need to run CorelLASER (not CorelDraw) when you want to be able to send to the laser cutter. Select all the elements (that you have drawn) that you wish to cut/engrave & then up the top right hand corner there should be a toolbar with some icons (see here: [https://drive.google.com/file/d/0Bzi2h1k_udXwVzExOXVmNmZKN3M/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwVzExOXVmNmZKN3M/view?usp=sharing)).



You need to make sure you have your device id & board selected correctly also (see here: [https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)). You can find these details on the main controller board (see here: [https://drive.google.com/file/d/0Bzi2h1k_udXwQUxUbmh6bDltYzA/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQUxUbmh6bDltYzA/view?usp=sharing) & here: [https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing)).



Hopefully that can get you going. If not, post again & I'm sure someone will be able to get you sorted.


---
**Mishko Mishko** *May 02, 2016 20:42*

I also didn't install any printer drivers, but I do recall installing FTDI drivers


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 02, 2016 20:55*

**+Mishko Mishko** Are you running stock hardware Mishko? I don't know what FTDI drivers are for, but seems they are maybe for TFT display related to arduino?


---
**Mishko Mishko** *May 02, 2016 22:43*

FTDI drivers are for the USB connection, you can always pick them here: 



[http://www.ftdichip.com/](http://www.ftdichip.com/)



I've got the M2Nano board


---
**Paul Townsend** *May 03, 2016 11:20*

Thanks to Yuusuf for the advice, this got me going, I had not seen those tools at top right.

I have now engraved some simple test rectangles and attempted cutting 4mm mdf.

I have board "Nano" by Julong and the marked id is CD50E42DCDA35009.

This id is correctly identified by CorelDraw which also shows Main board as 6C6879-Laser-B1. I can't tell if that is correct.

Now I am smoking well but have issues with speed setting in the cutting mode. The actual run speeds bear no resemblence to speeds I select and are usually much faster. I tried 10 and 20 and 0.1mm/s and it always ran at around 20mm/s but put the test cut in a different position for the 0.1mm/s setting.

What am I doing wrong?




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 03, 2016 11:29*

**+Paul Townsend** If you're not getting the correct speeds, it is generally an issue with the Main board being set incorrectly. Once you have it set correct it usually fixes up (so far from what I experienced and have seen with many others here too). On the controller board there should be a controller board name/model somewhere which you will need to make sure that is correctly set. If it is, then I am unsure what else could be causing it, but someone else may have some advice on that.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 03, 2016 11:35*

**+Paul Townsend** Actually, I just remembered there is another set of options you need to set.



Next to the Engraving icon, on the left, is an icon that says "CorelDRAW Settings".



You need to go into this one, the "Engraving Data" & "Cutting Data" dropdowns to "WMF - Normal Windows Metafile".



Choose for "Engraving Area" & "Cutting Area" the option "Only Selected".



The tickboxes, I have "Always Optimize Data" ticked to on, "Out Boundsline" also ticked to on. Honestly, no idea what they do. Pixel size is set to 1 [steps:1000dpi]. Default pixel = 1 step if cutting is also ticked on.



See if changing those settings assists with the speed issue.


---
**Donna Gray** *May 03, 2016 20:06*

Paul Townsend I have a nano board and I set mine to m2 setting in the preferences speed was way faster but on my board it said nano m2 don't know if that helps


---
**Paul Townsend** *May 11, 2016 06:15*

**+Donna Gray** Setting the board as M2 did make speeds work correctly so thanks.

I am now successfully cutting my own very basic designs while learning how to choose power and speed.

In my setup with Coreldraw ver11 as supplied with my machine can't import files from other people. I have found a few useful test designs on the web in either .crd, .crv [or.ai](http://or.ai) file format but they won't load. Do I need a later version of Coreldraw?

Currently using an old laptop with WinXP, I could also use other PCs with Win7 or Win10


---
**Donna Gray** *May 11, 2016 08:29*

Paul Townsend I use a program that I purchased called SCAL4 which is short for sure cuts a lot which is a UK program similar to the free inkscape program that I import images etc design and edit then I save as an SVG format which you then import into corelLaser to cut The program I use cost me about $89.95 AUD UK Price was 59.95 I do all my designing in that program I also use this program for my Scan N Cut machine I hope this help


---
**Donna Gray** *May 11, 2016 08:30*

Paul Townsend I am only using a free version of CorelDraw 5 but I only needed a version of CorelDraw to make the CorelLaser program work I do not design in CorelDraw at all


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 09:39*

**+Paul Townsend** I also am using the CorelDraw x5. If you check my posts there is a link to a dropbox folder with a copy of x5 there (non-legit) & you can import most file versions (as it is a newer version of Corel than 11 or 12 that came with the machine). I couldn't import SVG with CD12, but once I switched to x5 I can import SVG with no problems. AI files greater than version 9 don't work. I design all my files in AI, but either save as SVG or AI9 version.


---
*Imported from [Google+](https://plus.google.com/116723644477013102639/posts/A2zrai8v9qY) &mdash; content and formatting may not be reliable*
