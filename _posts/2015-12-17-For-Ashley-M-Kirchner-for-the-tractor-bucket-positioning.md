---
layout: post
title: "For Ashley M. Kirchner for the tractor bucket positioning"
date: December 17, 2015 22:45
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
For **+Ashley M. Kirchner** for the tractor bucket positioning.

![images/58bfeb3fe7dd328e0722a0b5bb8be52f.png](https://gitlab.com/funinthefalls/k40/raw/master/images/58bfeb3fe7dd328e0722a0b5bb8be52f.png)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ashley M. Kirchner [Norym]** *December 18, 2015 01:21*

Yeah, that will break with a little kid, like in a heartbeat.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 03:49*

**+Ashley M. Kirchner** What about having a gear on the axle & having another gear that it clicks into (on another axle) just to create some kind of friction?


---
**Ashley M. Kirchner [Norym]** *December 18, 2015 15:28*

The bits and pieces are relatively small already, between 6 to 10mm, so adding gears might be a bit difficult. I need to spend some time looking at the design and trying to figure out what I can do within the constrained space.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Dp8aXJPZiD9) &mdash; content and formatting may not be reliable*
