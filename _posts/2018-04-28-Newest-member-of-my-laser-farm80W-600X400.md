---
layout: post
title: "Newest member of my laser farm....80W 600X400"
date: April 28, 2018 17:28
category: "Discussion"
author: "Anthony Bolgar"
---
Newest member of my laser farm....80W 600X400

![images/8be206023945191828966be79fbe8ca0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8be206023945191828966be79fbe8ca0.jpeg)



**"Anthony Bolgar"**

---
---
**HalfNormal** *April 28, 2018 18:01*

Sweet! I am jealous! 


---
**Don Kleinschnitz Jr.** *April 28, 2018 19:13*

How much? What controller? NICE!


---
**Anthony Bolgar** *April 28, 2018 20:39*

$800 USD with a Leetro controller. 2 months old.


---
**HalfNormal** *April 28, 2018 20:40*

You stole it!


---
**Anthony Bolgar** *April 28, 2018 20:42*

hehehe......




---
**Anthony Bolgar** *April 28, 2018 20:43*

Runs around 5-7K USD brand new. Could not pass it up. Has a tube extension housing on it already, thinking of putting a 120W tube in.


---
**HalfNormal** *April 28, 2018 20:44*

How did you come across it?


---
**Anthony Bolgar** *April 28, 2018 20:44*

I have an addiction to buying Lasers, this makes #5 (Would be #6 but I actually sold one)


---
**Anthony Bolgar** *April 28, 2018 20:45*

Kijiji (Canadian version of Craigslist run by Ebay)


---
**HalfNormal** *April 28, 2018 20:46*

Some people have all the luck.


---
**Anthony Bolgar** *April 28, 2018 20:53*

Yup, but the horseshoe hurts turned sideways and shoved in really deep :)




---
**HalfNormal** *April 28, 2018 20:58*

I hear that excuse all the time in the operating room!


---
**Dan Stuettgen** *April 28, 2018 22:36*

So  if you don't mind me asking,  what did it set you back.  I have seen some of the 12x20 80 watt ones on ebay for around $1300.00


---
**HalfNormal** *April 28, 2018 22:37*

See above


---
**Anthony Bolgar** *April 28, 2018 23:04*

The ones on Ebay for $1300 have about $1000 in shipping that is not included. Shipping is usually only to nearest seaport, you are responsible for getting it to your place.


---
**Anthony Bolgar** *April 30, 2018 05:53*

Brought it home today. Came with massive air assist pump, rotary (roller type) attachment and a big 6" exhaust fan. Was missing the software dongle for the Leetro controller so I pulled the Leetro and am 3/4 of the way done with installing a Ruida 6442 controller that I had laying around. Was advertised as a 600X400 mm work area, but actual usable is 600X530mm. Z bed moves about 400mm so lots of room for large round things :)


---
**Anthony Bolgar** *April 30, 2018 15:47*

Controller swap is complete, final size of the usable area is 625mm X 450mm with 50mm of Z bed travel. Set up the controller to turn the exhaust and air assist on and off automatically. Checked the power output with my laser power meter and it puts out 83W at 95% power (20mA, tube is rated for 25mA max) And it plays so nicely with LightBurn. I now need to sell my K40 to make some room,  I'll keep my 50W Redsail for engraving, and use my 60W and 80W for cutting.


---
**HalfNormal** *May 01, 2018 03:09*

What was the story behind the laser being so cheap? 


---
**Anthony Bolgar** *May 01, 2018 03:46*

Sign company going out of business.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/EZFkZAdJG4E) &mdash; content and formatting may not be reliable*
