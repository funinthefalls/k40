---
layout: post
title: "Fun with tiles! Found some cheap tiles to play with at the local Habitat for Humanity store"
date: August 19, 2018 00:19
category: "Object produced with laser"
author: "HalfNormal"
---
Fun with tiles!



Found some cheap tiles to play with at the local Habitat for Humanity store.

Burn off the glaze and fill in with a sharpie.



![images/7e5ba8af843b1e1462f499f7e0abd68e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7e5ba8af843b1e1462f499f7e0abd68e.jpeg)



**"HalfNormal"**

---
---
**Sebastian Szafran** *August 19, 2018 07:14*

Nice work.

Why not to add a rectangle representing tile outline with shape centered inside? Engrave rectangle only on plywood or mdf with low power at first pass. Then place the tile directly aligned with engraved outline and the shape will be perfectly centered.


---
**HalfNormal** *August 19, 2018 14:19*

This was my first test. Just wanted to see how it would turn out.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/g6nX25WTdVh) &mdash; content and formatting may not be reliable*
