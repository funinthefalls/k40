---
layout: post
title: "Flaky Homing. I've got the cnc shield an uni installed, including my relays to control 2 additional outlets"
date: April 07, 2018 23:41
category: "Modification"
author: "Tom Traband"
---
Flaky Homing.



I've got the cnc shield an uni installed, including my relays to control 2 additional outlets. I'm also trying out Lightburn. So far I like it, now that I've understood why origin and homing location aren't the same thing.



Steps per mm are dialed in at 158.  X and Y travel in correct directions, and I've edited the grbl source code for 2 axis homing (X then Y), enabled PWM spindle control and successfully uploaded to the Arduino Uno. I then discovered that my K40 mechanical limit switches.are NC, so I am using $5=1. $23 is adjusted to back off switches by 2 mm which does get the axis off the switch lever.



The problem is that the homing behavior is inconsistent. One click of the homing button may result in 60mm X movement and backing off before the switch is hit then the same for Y. A second click may do the same, or the x may home as expected then the Y does a "short run" or may home normally. Or it may take 4 or 5 tries to get to the top left corner.



Any thoughts on how to improve this? It looks like the switches have an additional contact so I can probably change them to NO, although I do prefer the logic behind using NC home/limit switches. I also notice that the motor and limit wiring runs together for a significant distance, so noise may be a factor, but the original nano board never showed any homing problems.





**"Tom Traband"**

---
---
**HalfNormal** *April 08, 2018 21:30*

It sure does sound like a noise issue. Take a look here for discussions on the best way to filter the noise on an arduino/shield combo.



[discuss.inventables.com - Adafruit screw shield with homing noise suppression easy change capacitors - Upgrades - Inventables Community Forum](https://discuss.inventables.com/t/adafruit-screw-shield-with-homing-noise-suppression-easy-change-capacitors/24588/18)


---
**Paul de Groot** *April 09, 2018 02:01*

Did you set the correct pulse timing $0=10, $1=255? You can add 100nf caps on the limit switches to filter the electrical noise out


---
**Tom Traband** *April 09, 2018 04:07*

Homing is working now. I rerouted the endstop cable to get some separation from the stepper cables, but that didn't seem to make any difference, so I went ahead and changed them over to NO instead of NC, and then everything started to work as expected (once I reset $5-0). I figured since I've chosen to go GRBL, I may as well adopt it's defaults if I hit a snag.

$0 is set to 10.

From my reading of GRBL options, setting $1=255 will keep the steppers enabled all the time, even when not being sent a move command. Do I really need that?




---
**Paul de Groot** *April 09, 2018 22:20*

**+Tom Traband** $1 set to 255 ensures that all lines do align. Without it, belt tension or not level machine gantries can cause the gantry to wander a bit with as end result as less accurate cut. With engravings, I have not seen any impact since the steppers are driven continuously hence no wandering.


---
**Tom Traband** *April 10, 2018 12:16*

Thanks for the explanation.  I've got $1=255 now.


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/joMe4h2HKci) &mdash; content and formatting may not be reliable*
