---
layout: post
title: "Hello Everyone I am trying to engrave wood cases, the program that Im using is K40 Whisperer and it is working, only that the vector engrave do it really soft"
date: May 06, 2018 18:53
category: "Materials and settings"
author: "Denisse Bazbaz"
---
Hello Everyone 

I am trying to engrave wood cases, the program that Im using is K40 Whisperer and it is working, only that the vector engrave do it really soft. Also I need that it "vector engrave" all the solid, not only the borders, I understood that it can only be engrave the borders. Can someone help me change settings so that I can engrave hard on the wood? 



Im new to the machine 

Thanks 







**"Denisse Bazbaz"**

---
---
**Ned Hill** *May 06, 2018 21:16*

For solid fills it's going to raster engrave them.  The depth of the engraving is going to be determined by a number of factors.  For machine settings, engraving depth is going to be determined by scan speed, power, and scanline steps.  Slower speed and/or higher power will engrave "harder".  



Scanline steps determines how far the laser moves down for each following raster scanline.  The K40 laser beam typically has a minimum diameter of ~0.15 mm at the focal point.  So having a scanline step of say 0.05mm(0.002in) means you are getting a ~3x overlap on area of the engrave.  Smaller scanline steps gives "harder" engraving since it's in effect giving more "passes", but it comes at the cost of longer engrave times.  



I think K40whisperer has the default scanline step at 0.05mm(0.002in).  I would suggest playing with speed and power first.  



What settings were you using?  Also a pic of your engraving may help as well.






---
*Imported from [Google+](https://plus.google.com/112467917885418951758/posts/124rarfqpUD) &mdash; content and formatting may not be reliable*
