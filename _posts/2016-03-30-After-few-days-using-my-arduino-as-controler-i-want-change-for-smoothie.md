---
layout: post
title: "After few days using my arduino as controler i want change for smoothie"
date: March 30, 2016 18:31
category: "Smoothieboard Modification"
author: "Damian Trejtowicz"
---
After few days using my arduino as controler i want change for smoothie.

So quick question

Is this board is what im looking for?

[http://s.aliexpress.com/e6riiYz6](http://s.aliexpress.com/e6riiYz6) 



3Dpriter Smoothieware controller board MKS SBASE V1.2 opensource 32bit Smoothieboard Arm support Ethernet preinstalled heatsinks





**"Damian Trejtowicz"**

---
---
**Jean-Baptiste Passant** *March 30, 2016 22:16*

This is a smoothieware compatible board, but not a smoothieboard.

A real smoothieboard would be more expensive, I saw some kind of clone on Aliexpress, but I found someone near me getting rid of his Smoothieboard, either way I would go with an original Smoothieboard.

If you prefer a cheaper option, then the MKS board are your best bet (I saw some people using it here and there), if you want a smoothieboard for cheap, check Aliexpress, but I cannot guarantee it will work and be a real one, if you prefer to have support and be sure that you get a working board, go for a real smoothie.


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/EBzNvaMhie1) &mdash; content and formatting may not be reliable*
