---
layout: post
title: "Hi All, How are you guys keeping your water clean as I'm finding a build up of \"slime\" in the pipes"
date: July 16, 2015 14:35
category: "Hardware and Laser settings"
author: "David Wakely"
---
Hi All,



How are you guys keeping your water clean as I'm finding a build up of "slime" in the pipes. Anyone recommend a safe way of cleaning?





**"David Wakely"**

---
---
**Jim Coogan** *July 16, 2015 15:33*

I have a K40 so my cooling system is a 5 gallon can that contains 3 gallons of distilled water and 1 gallon of antifreeze.  The laser is a breeding ground for algae just like a fish tank.  Nice warm water.  The antifreeze prevents it from forming and gives you a nice color to check for flow.  I use what they call RV or Recreational Vehicle antifreeze because it is non-toxic and does not require any special processes for disposal.


---
**Jon Bruno** *July 16, 2015 18:08*

I'm not sure if it's an acceptable practice but I'm running 4.5 gals of distilled water with a cup or so of bleach in it.. I have a hunch it's probably not the best idea and I'm sure there are good reasons not to do so, but my water and tube are still squeaky clean. Does anyone want to tell me why I shouldn't be running it like this, I'm not sure if it's ok or not. vapors? damage?.. any info is welcome


---
**David Wakely** *July 16, 2015 20:53*

I think the antifreeze is probably the solution but I think I'm gonna flush it all through with a mild bleach + water solution unless anyone says that bleach is a massive no no!


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/XCX7WuCWhTk) &mdash; content and formatting may not be reliable*
