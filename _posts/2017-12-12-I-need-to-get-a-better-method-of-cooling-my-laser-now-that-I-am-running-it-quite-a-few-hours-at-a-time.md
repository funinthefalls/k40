---
layout: post
title: "I need to get a better method of cooling my laser now that I am running it quite a few hours at a time"
date: December 12, 2017 02:25
category: "Modification"
author: "Anthony Bolgar"
---
I need to get a better method of cooling my laser now that I am running it quite a few hours at a time. It is for a 60W laser. I know the CW3000's are a radiator and not a chiller, but if run in tandem with a 5 gallon reservoir would it be cable of maintaining the water at the ambient room temperature (in my case it is 20 deg Celcius) ?  I really don't have the money for a CW500 or equivalent right now. What is the general consensus of my proposed plan, would it work or do I really need to get the 5000?



Thanks in advance for any help and advice you can give me.





**"Anthony Bolgar"**

---
---
**Alex Krause** *December 12, 2017 06:14*

I think I had a conversation with **+Bonne Wilce**​ regarding a topic that the cw3000 and the cw5000 maybe some information can be found with Bonne


---
**Adrian Godwin** *December 12, 2017 08:33*

A 60W laser needs to dump about 240W of excess heat. The CW3000 is rated at 50W/C so the temperature of the water should get to about 5 C above ambient. 



A gallon of water is a heatsink that takes about 16kJ to raise the temperature 1 degree. So 240W (240J/s) will raise 5 gallons about 1 deg C in 5 mins. It's actually better than this as the water itself radiates and conducts the heat to the environment, but that's very dependent on what it's held in.



It seems to me that the CW3000 might just about cope as long as ambient isn't above 20C. The reservoir will keep it cool for about 25 mins so will give you a bit of time to notice excessive heating.



Actual performance is likely to be a bit better as the laser doesn't run all the time - there are moves without cutting, and you probably spend a while loading and unloading the cutter.



You could also consider secondhand sources of chillers - eg put the reservoir in a fridge. Be careful not to get the water too cold, though.



I've based this on published figures and estimates, and may have made errors. YMMV. 


---
**Steve Clark** *December 13, 2017 18:39*

Based on your room temperature and the cooling method you are thinking of, I would consider getting one of these>



[amazon.com - Amazon.com : NorthStar Spot Sprayer Tank - 26-Gallon Capacity : Lawn And Garden Sprayer Accessories : Garden & Outdoor](https://www.amazon.com/NorthStar-Spot-Sprayer-Tank-26-Gallon/dp/B0000AX5PW/ref=sr_1_3?ie=UTF8&qid=1513189811&sr=8-3&keywords=25+gallon+water+tank)






---
**Cesar Tolentino** *December 23, 2017 15:28*

Try a tall aquarium. You will be surprised how small the footprint of a tall 20g aquarium.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/1QPDBJHyrAX) &mdash; content and formatting may not be reliable*
