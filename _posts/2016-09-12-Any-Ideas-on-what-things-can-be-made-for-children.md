---
layout: post
title: "Any Ideas on what things can be made for children"
date: September 12, 2016 08:27
category: "Discussion"
author: "Tony Schelts"
---
Any Ideas on what things can be made for children.  What is your experience.  





**"Tony Schelts"**

---
---
**Jim Hatch** *September 12, 2016 12:45*

Those 3D skeleton toys are all the rage here. There's somewhere on the net with instructions on how to convert a 3D model of something (like a dinosaur, dog, etc) into slices for the laser to cut. 



Also look at scrollsaw patterns of toys for kids. You can cut those with a laser and they're easy to scan in and then use the outline function in Corel to trace and cut them.


---
**Alex Krause** *September 12, 2016 14:34*

123d Make is the software I use to turn 3d models into puzzles


---
**Tony Schelts** *September 12, 2016 15:23*

Thanks guys 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 12, 2016 16:57*

You could also consider hanging mobiles like they make for babies.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/ZCAw73YX88S) &mdash; content and formatting may not be reliable*
