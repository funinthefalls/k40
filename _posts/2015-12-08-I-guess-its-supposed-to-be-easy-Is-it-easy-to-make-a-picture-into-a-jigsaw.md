---
layout: post
title: "I guess its supposed to be easy, Is it easy to make a picture into a jigsaw"
date: December 08, 2015 10:14
category: "Discussion"
author: "Tony Schelts"
---
I guess its supposed to be easy,  Is it easy to make a picture into a jigsaw.  do you use a black background with white lines or visa versa??  new to this thanks







**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 08, 2015 11:17*

I would create the shape of pieces in Illustrator & just have multiple shapes in black. I'll post a pic shortly to show.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 08, 2015 11:34*

[https://goo.gl/photos/zFMrchoM9rb3APvr5](https://goo.gl/photos/zFMrchoM9rb3APvr5)



So, if you make all the shapes (like in my quick example), then make them all 100% black background, it should be able to determine there are multiple shapes & thus cut along the edge of each.



I don't think you really need a border colour at all.

Although, I may be wrong...


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/M9XvkRedzFV) &mdash; content and formatting may not be reliable*
