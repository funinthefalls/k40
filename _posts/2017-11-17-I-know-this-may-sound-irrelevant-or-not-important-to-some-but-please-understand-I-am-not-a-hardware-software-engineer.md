---
layout: post
title: "I know this may sound irrelevant or not important to some, but please understand I am not a hardware/software engineer"
date: November 17, 2017 21:37
category: "External links&#x3a; Blog, forum, etc"
author: "Jonathan Davis (Leo Lion)"
---
I know this may sound irrelevant or not important to some, but please understand I am not a hardware/software engineer. I don't know how to code or develop a program. But I create things because of the enjoyment of doing it. 



I am not a professional, but my Elekes maker laser engraving has been issues from the beginning, but with time I have found a solution in the use of a C3D mini controller board. It works but yet due to the nature of the system it's a learning process. As of lately my machine does not know when to stop or understand that it's run out of track. I have end stops but installing them is a problem due to their size. 





**"Jonathan Davis (Leo Lion)"**

---
---
**Chris Hurley** *November 17, 2017 21:47*

Sounds like you work surface is set to large in the software that you're using. 


---
**Jonathan Davis (Leo Lion)** *November 17, 2017 21:49*

**+Chris Hurley** I have been using laser web but it originally shipped with an Arduino based controller connected to the BenBox software program. 


---
**Michael Audette** *November 17, 2017 22:12*

I threw out Benbox and went straight to GRBL 1.1/LaserWeb4 on My Elekes Maker (A3 2.5W).  Works like a charm.  (even with the stock board).  Just have to be aware of your work coordinate system versus the size of the machine.


---
**Jonathan Davis (Leo Lion)** *November 17, 2017 22:47*

**+Michael Audette** indeed but my stock board was defective and Gearbest aka the seller refused to replace it 


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/P2YLKttRCHU) &mdash; content and formatting may not be reliable*
