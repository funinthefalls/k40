---
layout: post
title: "Does that look like a crack"
date: February 05, 2016 14:12
category: "Discussion"
author: "3D Laser"
---
Does that look like a crack

![images/9782fa64f526e4afbd20e2da8c7c37f9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9782fa64f526e4afbd20e2da8c7c37f9.jpeg)



**"3D Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 05, 2016 14:44*

Where exactly are you referring to? Maybe you can tag it with yourself so we know the area you want us to look at?


---
**Joseph Midjette Sr** *February 05, 2016 15:01*

If you are referring to right below where the yellow wire enters the tube, it looks like some leftover glue. If it not then I would say it does look like a crack.


---
**Joseph Midjette Sr** *February 05, 2016 15:03*

Or rather, I think what I am seeing is where the yellow wire is soldered to the end. I don't know if I'm looking where you are talking about...


---
**3D Laser** *February 05, 2016 15:07*

It's right over the metal ring 


---
**3D Laser** *February 05, 2016 15:08*

**+Joseph Midjette Sr** yes that is what I'm talking about 


---
**Jim Hatch** *February 05, 2016 15:40*

Does it move if you rotate the box? It might be an artifact of the water/glass interface. If it rolls when you rotate the box it is likely due to the play of light where the water & glass meet. If it doesn't move it does look like it might be a subsurface crack.


---
**Stephane Buisson** *February 05, 2016 15:45*

Yes it look like a crack (from other post photo), you should keep that under surveillance. more air could go in, accelerating the inevitable destructive process.


---
**David Wakely** *February 05, 2016 16:24*

I'd say it's not a crack, if it is the part to the right of the yellow wire over the metal find then it is a thin wire that is connecting the anode to the metal ring around the end of the tube. Mine has one of these too. If you zoom in and look closely you can see a blob of solder on it about 1/4 of the way in. Hope this helps


---
**Jim Hatch** *February 05, 2016 16:26*

**+David Wakely**​ looking on a bigger monitor, I'd agree :-)


---
**David Wakely** *February 05, 2016 16:26*

Also that giant air bubble in the tube will destroy the tube you don't get rid of it.


---
**3D Laser** *February 05, 2016 16:37*

Now when I blow it up I see that it is the wire it had me scared there for a second 


---
**3D Laser** *February 05, 2016 17:11*

**+David Wakely** I'm having a hard time getting it out


---
**David Wakely** *February 05, 2016 17:51*

Tilt the machine


---
**Joseph Midjette Sr** *February 05, 2016 18:10*

It is the wire soldered to the ring. As for the air bubble. just reverse the hoses on your pump, turn it on for a minute then put the hose back like normal and you should be good


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/2ZPgF37kifB) &mdash; content and formatting may not be reliable*
