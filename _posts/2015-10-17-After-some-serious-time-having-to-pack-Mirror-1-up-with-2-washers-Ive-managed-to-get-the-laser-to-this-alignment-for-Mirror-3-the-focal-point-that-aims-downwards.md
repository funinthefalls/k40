---
layout: post
title: "After some serious time & having to pack Mirror #1 up with 2 washers, I've managed to get the laser to this alignment for Mirror #3 (the focal point that aims downwards)"
date: October 17, 2015 04:35
category: "Hardware and Laser settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
After some serious time & having to pack Mirror #1 up with 2 washers, I've managed to get the laser to this alignment for Mirror #3 (the focal point that aims downwards). Is this enough precision for all 4 corners (Top Left, Top Right, Bottom Left, Bottom Right)??

![images/2983e28663cfdaf727655c50af912132.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2983e28663cfdaf727655c50af912132.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**ThantiK** *October 17, 2015 12:43*

I've certainly operated with less precision with a K40 at my hackerspace which everyone thought they knew how to adjust the mirrors on.


---
**Sean Cherven** *October 17, 2015 12:48*

That's not bad. It'll get you by.


---
**Bill Parker** *October 17, 2015 21:55*

All you need to keep an eye on is it staying so good mine moved every time I  used it and it was all down to the fixings for the mirror mounts they had used odd screws even self tapping screws I tapped all the holes out and put new bolts in them all and it's been fine since.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/YHf3gTcBwE5) &mdash; content and formatting may not be reliable*
