---
layout: post
title: "I'm interested in upgrading my K40 to be able to use LaserWeb by Peter van der Walt ."
date: April 11, 2016 16:09
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
I'm interested in upgrading my K40 to be able to use LaserWeb by **+Peter van der Walt**. What I'm looking for is some assistance in what components I need to get in order to do so. I've seen people mentions RAMPS, Smoothie, etc. and to be honest, I barely understand what any of it is. So I'm looking for advice as to what is required in order to complete the full upgrade to be compatible. Also, I've seen people mention check LaserWeb github or something and again, I barely know what that is.



And specifically for Peter, if there is anything in particular that you would prefer a user get to add functionality or testing for particular devices/hardware, then let me know.



As I know pretty much nothing about all this stuff, I will have to learn it all from scratch, so I have no preferred setup. I'm relying on the advice of all here to guide me in what is best for performance.



One feature that I am very much looking forward to being able to use is having different shapes engraved at different power levels.



Also, a side note, I use Adobe Illustrator for my design phase, and I'm fairly certain I've seen that SVG is importable? Is this correct?



Another side note, my budget for components is not huge per pay, so it will be a purchase bit-by-bit sort of thing (or save for a bit & buy multiple things at same time if they are from the same supplier).



Thanks to everyone in advance for any and all advice given.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Ariel Yahni (UniKpty)** *April 11, 2016 16:29*

**+Yuusuf Sallahuddin**​​ your best option is smoothie. There is a document in this group that talks specifically about upgrading to smoothie. You can import svg for cutting without problems.  Just make sure all objects are path.  Also you will set power level based on color. So if you want different power levels per item just assing a specific color to the path and once imported to LaserWeb it will let you assing power and speed. ﻿In my YouTube account you will find videos of various test I've done and you'll see more or less the features available.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 16:48*

**+Ariel Yahni** Thanks for that Ariel. I'll take a look at your youtube when I've got some spare minutes to watch.


---
**Ariel Yahni (UniKpty)** *April 11, 2016 16:51*

**+Yuusuf Sallahuddin**​ here is the link for the upgrade [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 17:01*

**+Peter van der Walt** Thanks for the very thorough reply Peter. I will take a look at that collection you showed. I guess smoothie of some sort it is. I will have to look into them a bit more to determine what is what.



So basically all I need is the Smoothie controller? If that's the case, I'm sold. I assumed I need heaps of things.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 17:11*

**+Peter van der Walt** Awesome, I have been holding off on this because I thought it was a complicated process involving a whole bunch of circuitry related stuff that is probably beyond my current knowledge. I'll get onto it & hopefully have a smoothie board of some sort on the way soon.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 17:11*

**+Ariel Yahni** Sorry, didn't notice your previous post with the link for upgrade. Thanks for sharing that.


---
**Stephane Buisson** *April 11, 2016 17:14*

**+Yuusuf Sallahuddin** if you need help, i am the author of that link ;-))


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 11, 2016 17:16*

**+Stephane Buisson** Even better. This group is full of amazing members. Thanks for that. When I get to that stage if I need assistance I will check with you.


---
**Josh Rhodes** *April 11, 2016 18:31*

**+Peter van der Walt**​ your reply here should be pinned or stickied or something like that..


---
**I Laser** *April 11, 2016 22:32*

Could be a complete brainfart... So my apologies if it is!



There were recently discussions about angling the laser head to create mitre cuts, bevel effects, etc. 



Given that laserweb can alter speed based on colour, could colour gradients be used to create the same effect?


---
**Ariel Yahni (UniKpty)** *April 11, 2016 22:43*

**+I Laser**​ if you angle the laser and raster the image with the gradients it should be possible to get an angled effect


---
**I Laser** *April 11, 2016 23:14*

**+Ariel Yahni**  My thought is you don't need to angle the head, you just ramp the power up and down to create the effect. 



It wouldn't be overly efficient if you were cutting mitred angles to join together, but might be perfectly fine for beveled text etc.



Interested what **+Peter van der Walt** thinks, I'm sure he'd know the limitations.


---
**Ariel Yahni (UniKpty)** *April 11, 2016 23:22*

**+I Laser**​ defenetly possible, I have done deep engraving with my diode with LaserWeb so it's just playing with the image and the setting in raster. Mind that I like the idea of angle the laser to further the effect. 


---
**Vince Lee** *April 11, 2016 23:31*

**+Yuusuf Sallahuddin** Just to double check, what is your comfort level with tinkering with electronics (soldering, etc)?  I found the conversion straightforward but I've done a lot of this sort of thing.  There is still a fair amount of work wiring together the correct jumpers and headers, etc.  If you have a k40 with a ffc ribbon connector, you'll probably want to MIddleman or similar adapter board, which requires soldering on a connector with tiny closely-spaced pins.  If you need help, there are plenty of folks here willing to help (me included), but you'll want to know what to expect.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 02:52*

**+Vince Lee** Honestly I haven't done a lot of tinkering with electronics/soldering for years. 



Just yesterday I tried rigging up LEDs & resistors to power it from old phone battery, but something went awry. I am not certain whether the resistors were incorrect size (I just trusted the guy at shop to hook me up with correct ones for my purpose) or if I did something wrong. But with resistors in place, the LEDs wouldn't light. I tried various orientations of the LEDs & resistors, positions in the circuit, direction of the current, etc. Nothing worked... until I removed the resistors. Then the LEDs light up. But, I think I killed them haha.



I can't seem to find my reasonable soldering iron, so I was using a horrible old one, but when I find where I've put my reasonable one (or purchase another) I should be a lot more capable of soldering close pins without getting solder everywhere.



So, simply put, I am vaguely comfortable/capable of soldering.



Also, this group is great & has many people willing to help with areas they are familiar with. I'm quite pleased with the participation/assistance of members here.



edit: I just remembered, my K40 has a ribbon cable for the Y-axis stepper motor. Is that what you are referring to that may require a middleman board?


---
**Vince Lee** *April 12, 2016 04:25*

**+Yuusuf Sallahuddin** yes, I recommend a middleman board.  I have a few I never used because I ended up mking my own adapter board with more features.  If u want I can send you one and may even have an extra ribbon connector for it.  Do you by chance have a moshiboard in your k40?  If so, the board I made may be more convenient as it accepts the moshiboard connectors which would simplify your wiring.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 13:49*

**+Vince Lee** I've got the m2 board on my k40. Might be worthwhile getting the middleman board off you as well when we organise the laser glasses plastic stuff.


---
**Vince Lee** *April 12, 2016 22:44*

**+Yuusuf Sallahuddin** A small flat rate box looks like it's $35 USD to Australia.  Is that about right?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 12, 2016 22:50*

**+Vince Lee** Sounds about right for postage to Australia. How much is a small flat rate capable of holding in weight? I assume you are in US?


---
**Vince Lee** *April 14, 2016 04:22*

**+Yuusuf Sallahuddin** yup.  From usps:



Priority Mail International® 

Small Flat Rate Box**

Expected Delivery Date:6 - 10 business days to

many major markets. Retail:$34.95



Value of contents can not exceed $400.00



USPS-Produced Box: 8-5/8" x 5-3/8" x 1-5/8"

Maximum weight 4 pounds






---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 14, 2016 05:24*

Awesome, so with the middleman + the laser lens stuff, about us$40-45? I only really need enough of that lens stuff to make a pair of goggles. Unless, you have lots of excess laser lens stuff?


---
**giavonni palombo** *April 15, 2016 13:08*

Im thinking of the conversion as well. One question why keep the ribbon cable?  Why not wire in the x stepper and put in 2 micro limit switches all with separate wires?


---
**Vince Lee** *April 16, 2016 03:02*

**+giavonni palombo** that's certainly a fine way to go too.  I liked the idea of a fully plug-in replacement.  Not only did it let me do most the work ahead of time without having my engraver out of commission, but in the case of my adapter board it can connect to both the new smoothie board and old moshiboard and switch between the two via dip switches.  As I haven't had time to install and learn all the new software yet, it has been nice to still a few quick jobs in moshi and even afterwards have it as a backup.  Lastly, a fully plug in replacement is probably easier for someone else with less electrical engineering to install.


---
**Dennis Fuente** *April 18, 2016 23:26*

I am looking at getting a smoothie board, can anyone tell if it comes with all the connectors needed to hook it up can i use the same connectors already in the machine i tried to e-mail uberclock but no response so far would anyone know if they have the 3 X boards back in stock.



Thanks 



Dennis 


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/DoFyNaubJSc) &mdash; content and formatting may not be reliable*
