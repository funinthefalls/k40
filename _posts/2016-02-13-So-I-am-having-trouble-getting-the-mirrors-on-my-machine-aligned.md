---
layout: post
title: "So I am having trouble getting the mirrors on my machine aligned"
date: February 13, 2016 16:06
category: "Discussion"
author: "3D Laser"
---
So I am having trouble getting the mirrors on my machine aligned.  I can get them aligned close up but as the machine travels down the rail it goes out of alignment and ideas 





**"3D Laser"**

---
---
**Stephane Buisson** *February 13, 2016 16:30*

check if your rays are parralell to X and Y, then center into last mirror.


---
**3D Laser** *February 13, 2016 16:34*

**+Stephane Buisson** not quite sure what you mean


---
**Stephane Buisson** *February 13, 2016 16:41*

I mean, if you try to correct a deviation , in fact you make it worst. being parrallel to axes, you will be right.

before trying to align, look into tube position parallel to X (look the wrapping foam if any, the holder being to large for the tube), look as well if the head is well vertical (bent ?).

then align the mirrors. several links into community to help you.


---
**HalfNormal** *February 13, 2016 17:12*

I was having trouble adjusting the beam with just the mirrors and then realized that the laser tube itself was not parallel to the gantry.


---
**Jim Hatch** *February 13, 2016 20:20*

Also check the XY gantrys. I need were not initially square.


---
**3D Laser** *February 13, 2016 21:49*

My tube seems to be straight but I do notice my X Y gantrys are not square could that be the issue and if so how do I fix it is there a good YouTube video


---
**3D Laser** *February 14, 2016 01:19*

**+Jim Hatch** how do you square them


---
**I Laser** *February 14, 2016 02:09*

The X gantry on my latest machine was out of whack, I loosened off the belt tensioner on the offending side and skipped the belt a few teeth so it was level.



Note, I had to unbolt the whole gantry first to gain access to the belt tensioner.


---
**Linda Barbakos** *February 16, 2016 18:58*

I always reference this video for mirror alignment 
{% include youtubePlayer.html id="KhhOHUe_b6o" %}
[https://youtu.be/KhhOHUe_b6o](https://youtu.be/KhhOHUe_b6o) see 11:37 minutes for the principles. Hope this helps. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/WgAqJzrdfCc) &mdash; content and formatting may not be reliable*
