---
layout: post
title: "I'm worried about losing this k40 community due to G+ being deleted early 2019"
date: February 01, 2019 10:43
category: "Discussion"
author: "Rene Munsch"
---
I'm worried about losing this k40 community due to G+ being deleted early 2019. Is there already an alternative platform in mind? Where are you going to go? Where will we find all the contents of your K40 Laser-Project skills and projects?





**"Rene Munsch"**

---
---
**krystle phillips** *February 01, 2019 11:27*

I am also worried 


---
**Stephane Buisson** *February 01, 2019 12:06*

Working on it. the idea is a backup of this community to be captured with G+Exporter (see older post), to see if our community could fit into a larger Maker movement as a category of a large Discourse Maker's forums. and use a subdomain (like example k40.Maker'sMovement.net) to host backup, wiki or futur ressources specific to us. **+Rene Munsch** **+krystle phillips**


---
**Stephane Buisson** *February 01, 2019 12:34*

from G+ to owner (wait for early March):

"Google+ Communities

To download data for Communities where you’re an owner or moderator, select Google+ Communities. You will get:



Names and links to Google+ profiles of community owners, moderators, members, applicants, banned members, and invitees

Links to posts shared with the community

Community metadata, including community picture, community settings, content control settings, your role, and community categories

Important: Starting early March 2019, you will also be able to download additional details from public communities, including author, body, and photos for every community post."


---
**Duncan Caine** *February 01, 2019 14:38*

Stephane, will you be emailing everyone with the new solution when you have it?


---
**Bob Damato** *February 01, 2019 16:10*

Does this mean there is a solution in the works? If not I was about to get one going....




---
**Stephane Buisson** *February 01, 2019 17:10*

**+Duncan Caine** I have no access to members mail address. I will post one or more links when ready. 


---
**Jonathan Davis (Leo Lion)** *February 01, 2019 20:13*

Hopefully


---
**Ned Hill** *February 02, 2019 00:12*

**+Stephane Buisson** I would suggest that you might want to do a pinned post about plans for the community or at least let people know that you have something in the works.  I know that you are working on something so I haven't said much, but I think it will help alleviate the concerns of some people in the immediate future. 


---
*Imported from [Google+](https://plus.google.com/111325559173207527657/posts/TdV98Wfr87r) &mdash; content and formatting may not be reliable*
