---
layout: post
title: "If anybody is interested, it's $29.95"
date: September 22, 2016 10:57
category: "External links&#x3a; Blog, forum, etc"
author: "Scott Marshall"
---
If anybody is interested, it's $29.95. 



[http://www.instructables.com/class/Laser-Cutting-Class/](http://www.instructables.com/class/Laser-Cutting-Class/) 





**"Scott Marshall"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 22, 2016 13:56*

I saw that too. Note: it's free if you already have a Premium Membership.


---
**Don Kleinschnitz Jr.** *September 22, 2016 14:56*

I'm wondering what laser that is. I like the head design? is that a autofocus switch on it?


---
**Scott Marshall** *September 22, 2016 20:56*

Thanks Yuusuf, I didn't catch that, I'm tempted to do it, but am already buried. With my luck lately I'd be sick for it.



Don. Yes, it is an autofocus switch on the right there. I'm not sure what brand, but I've seen it before. It's a common design, simple, but it does the job. The plunger is adjustable to match your optics and operates a switch or open contact set. Almost any small microswitch or contact like they use for a touch off on a machine tool should do, you just have to have an adjustable metal plunger. There's some fancy ones I've seem made using the guts of a tactile switch too. The conductive rubber is somewhat forgiving and helps debounce. They're surprisingly repeatable so they say.



I made a cheapie for my milling machine using a flat pogo pin (spring plunger used to test PCBs etc). It's exactly an inch tall so I don't screw the math up. (I then found out that some high end endmills aren't conductive - but I can't afford more then 1 of them anyway)

In the laser you'd want a switch for non metallic materials.

I think I'd go with a quality commercial microswitch, maybe a Cherry keyboard switch. 

Electronically debounce it, have enough overtravel to account for 'accidents' and move in slow. If you're fussy do several touch offs and average. With a laser you could average the corners, but you shouldn't have to unless things are out of true.

It's the same rules as for a milling machine just not as critical.



Seems kind of a lot of work for the gains unless you are just having fun tricking out your laser.



I prefer the have the air assist enclose the lens for protection though. 



If anybody does the course, let me know what I missed. 



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 23, 2016 08:55*

**+Scott Marshall** I believe I have a Premium Account subscription (gifted by Instructables on another account for posting an Instructable) that I might use to do the course (if I have the time too). I'll let you know what is in it if I do it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 25, 2016 13:45*

Just would like to update anyone on this class. Honestly, unless you already have a Premium account at Instructables, it's definitely not worth $29.95 to do it. I just went through all 7 lessons & realistically didn't learn anything that I didn't already learn from the helpful members of this community.


---
**Don Kleinschnitz Jr.** *September 25, 2016 16:28*

Start brain fart;

{

What if we created a set of document(s) that removed all the fud and confusion and sold them (somewhere). Returning the funds to this group for materials needed for special projects, like the work that +Alex is doing.



The moderator gets the funds and we vote on the use of them. 



The docs do not replace these open forums but are an extraction from them. 



Those that buy the docs to shorten the learning curve and this is worth something. 



An example: "All you wanted to know about K40 laser power supplies but didn't want to rediscover".

These are the "Rediscover NOT" series of Ebooks.



They would sell for under $10 each and would be delivered electronically so there is no overhead on the group.

}

end fart;


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/6Dwt4JgQ9ud) &mdash; content and formatting may not be reliable*
