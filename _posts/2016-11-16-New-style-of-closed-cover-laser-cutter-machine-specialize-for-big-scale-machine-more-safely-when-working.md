---
layout: post
title: "New style of closed cover laser cutter machine, specialize for big scale machine, more safely when working"
date: November 16, 2016 02:34
category: "Object produced with laser"
author: "Andy Yan"
---
New style of closed cover laser cutter machine, specialize for big scale machine, more safely when working.



![images/1c43631569fbf5b59f39b4e0aa72d666.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c43631569fbf5b59f39b4e0aa72d666.jpeg)
![images/cd47d3c499cfa1fc42a49e5c93892ca4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd47d3c499cfa1fc42a49e5c93892ca4.jpeg)

**"Andy Yan"**

---


---
*Imported from [Google+](https://plus.google.com/101522722913812335166/posts/WLXAff7kHgY) &mdash; content and formatting may not be reliable*
