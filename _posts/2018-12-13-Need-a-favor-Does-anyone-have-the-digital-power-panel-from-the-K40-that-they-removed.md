---
layout: post
title: "Need a favor. Does anyone have the digital power panel from the K40 that they removed?"
date: December 13, 2018 20:50
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Need a favor.  Does anyone have the digital power panel from the K40 that they removed?  I would need the the panel and the cable that it came with, it's a white plug with 4 wires. 



USA location is preferable. 



Thank you!





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Don Kleinschnitz Jr.** *December 13, 2018 21:20*

Don't know if this is an alternative:    

LIHUIYU CO2 Laser Controller M2 Nano Control Panel for Engraver Cutter DIY 3020 3040 K40 by Cloudray [https://www.amazon.com/dp/B07KD9X9H1/ref=cm_sw_r_cp_apa_i_JZSeCbC2A7KS1](https://www.amazon.com/dp/B07KD9X9H1/ref=cm_sw_r_cp_apa_i_JZSeCbC2A7KS1)

[amazon.com - Robot Check](https://www.amazon.com/dp/B07KD9X9H1/ref=cm_sw_r_cp_apa_i_JZSeCbC2A7KS1)


---
**James Rivera** *December 13, 2018 21:49*

**+Ray Kholodovsky** Would a picture do? I can take one of mine tonight, if that’s any help.


---
**Ray Kholodovsky (Cohesion3D)** *December 13, 2018 22:07*

I have pics already :) 

I'd like one in person to fiddle with. 


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/i4NTJ9qLJoa) &mdash; content and formatting may not be reliable*
