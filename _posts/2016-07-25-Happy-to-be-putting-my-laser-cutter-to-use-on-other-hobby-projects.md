---
layout: post
title: "Happy to be putting my laser cutter to use on other hobby projects"
date: July 25, 2016 18:31
category: "Object produced with laser"
author: "Evan Fosmark"
---
Happy to be putting my laser cutter to use on other hobby projects. Here's a couple of pictures from my CNC conversion on my Proxxon MF70 milling machine. Only have the X-Y table done so far. The K40 laser cutter has been incredibly helpful in cutting the stepper mounts, as it allows you to try different configurations until you find one that works best.



I have a Smoothieboard M4 that'll be used to control the CNC.





**"Evan Fosmark"**

---


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/RPMAP9gYcXm) &mdash; content and formatting may not be reliable*
