---
layout: post
title: "General question what interface hardware and hardware stack would folks recommend?"
date: January 04, 2016 20:05
category: "Hardware and Laser settings"
author: "Peter Shabino"
---
General question what interface hardware and hardware stack would folks recommend?  Have a shell of a laser cutter with good mechanical and optical path but all the electronics are toast. Rebuilt the stepper drivers (standard step and dir inputs) and now looking to replace the driver board (serial interface from the 90's) vs rebuilding it. 





**"Peter Shabino"**

---
---
**Stephane Buisson** *January 05, 2016 00:08*

**+Peter Shabino** , you are at a interesting stage ;-))



a Smoothie board or compatible, including the coming  SmoothieBrainz  from **+Peter van der Walt** based on LCP1769 (need a bit of patience for availability, not out yet).



or older stuff on Ramps/Marlin or even older on Arduino.


---
**Nick Williams** *January 05, 2016 07:44*

Hi Peter,

[https://github.com/nickw89509/LaserInk](https://github.com/nickw89509/LaserInk)

Here is a link to a modified version of grbl running on an every day arduino. I am personally using this on my laser works great for part cutting not so great for image rastering.



[objectworks.co](http://objectworks.co) sells software for generating and sending GCode to this controller.


---
*Imported from [Google+](https://plus.google.com/118078276787468359224/posts/TGMZQbnRmbr) &mdash; content and formatting may not be reliable*
