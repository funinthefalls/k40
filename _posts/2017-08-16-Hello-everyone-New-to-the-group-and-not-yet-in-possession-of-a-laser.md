---
layout: post
title: "Hello everyone, New to the group and not yet in possession of a laser"
date: August 16, 2017 17:23
category: "Discussion"
author: "Red Naxel A"
---
Hello everyone,

New to the group and not yet in possession of a laser. Did some reading in the group but didn't come up with "to be regarded when getting into laser / engraving". Does anybody have a good source of newbie information..."what am I getting into" or equivalent? I have 4 3D printers and been tinkering with this for a year, now it's time to broaden the portfolio. I like to do my own research to not need to ask too many questions, now I need a kick in the right direction. Any information is appreciated. Happy engraving.

Alex



Btw: read the pinned post, but can't seem to get at the info (sidebar with external links) on my mobile. 





**"Red Naxel A"**

---
---
**Ariel Yahni (UniKpty)** *August 16, 2017 18:04*

Without considering the tenchinal differences of 3dp and lasers , I would say that youll need to learn and think in 2D for making complex stuff. Also learning some 2D tools it's important, depending on that you already use for 3D.


---
**Ariel Yahni (UniKpty)** *August 16, 2017 18:06*

In the end it's a process of learning something new, you need patience and time with a little bit of good luck :)


---
**Red Naxel A** *August 16, 2017 18:10*

Hi Ariel, I think I have proven that with my endless tinkering on the printers...Looking forward to getting my head wrapped around this.


---
**Red Naxel A** *August 16, 2017 18:12*

Maybe to make my question more precise: is a k40 or equivalent ready to get into action without any add-ons, or is it necessary to get the surroundings going (I have seen pictures with 5gal water tanks and so on...So again..Without asking a newbie question, anywhere I can go to soak up some more Info? Thx


---
**Ariel Yahni (UniKpty)** *August 16, 2017 18:23*

**+Red Naxel A**​ not the say the contrary but 3dp is not laser cutting, there is a learning curve.



Regarding your question besides what you get in the box on a regular K40 only a water tank or similar and that's it, you can start cutting.



Some info I wrote a while back 

[thebetatester.xyz - What to know before buying laser cutter?](http://www.thebetatester.xyz/2016/09/06/what-to-know-before-buying-a-k40-chinese-laser-cutter/)


---
**Anthony Bolgar** *August 16, 2017 18:27*

Feel free to ask questions, we were all newbies at one time or another. Basic upgrades that I consider mandatory with the K40 would be:



1) Installing safety interlock limit switche for the laser compartment to prevent the laser from firing if the door is open.

2) Installing a water flow interlock so that the laser can not fire if the cooling water is not on.

3)Get a pair of acrylic safety glasses (Standard safety glasses will absorb the wavelength that the K40 puts out).

4) Remove the aluminum material clamp that ships with the machine and replace it with an adjustable bed of some sort.

5) Also, if you remove the internal air exhaust duct, you will gain a little bit of work area.



Optional:



1)Consider upgrading the controller board with a smoothieware or grbl based controller board, this will allow you to run Opensource software such as LaserWeb ([https://github.com/LaserWeb/LaserWeb4](https://github.com/LaserWeb/LaserWeb4)) or Visicut ([https://hci.rwth-aachen.de/visicut](https://hci.rwth-aachen.de/visicut)), which are, in my opinion, much more versatile and user friendly than the stock software that comes with the K40.

2) If you decide to keep the original controller, check out the K40 Whisperer software from Scorchworks ([http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html)), he reverse engineered the K40 controller and developed software that can run it without use of the dongle.



 Hope these suggestions send you off in the right direction.

 Have fun!


---
**Red Naxel A** *August 16, 2017 18:29*

**+Anthony Bolgar** that is great stuff. Thanks, that's what I needed to get started. I think I'm on order now....


---
**Red Naxel A** *August 16, 2017 18:33*

**+Ariel Yahni** thx. Especially the safety is not too be neglected....I think I know where to start. 


---
**Jim Fong** *August 16, 2017 20:23*

One of the better improvements is adding air assist.  


---
**Don Kleinschnitz Jr.** *August 16, 2017 21:37*

Look at the top of this communities posts. Links are pinned there.


---
**Anthony Bolgar** *August 16, 2017 22:31*

Yup, can't believe I forgot about air assist, as Jim said, a huge benefit.


---
**Paul de Groot** *August 17, 2017 00:58*

And upgrade the stock china lens for an USA ZnSe lens.


---
**Red Naxel A** *August 17, 2017 10:22*

**+Paul de Groot** When can i source your Upgrade Kit?


---
**Red Naxel A** *August 17, 2017 10:22*

Thx to all for the suggestions...Will be forming the wish list...


---
**Don Kleinschnitz Jr.** *August 17, 2017 12:52*

I would be interested if you see this as valuable in your decision process:

[plus.google.com - #K40GettingStarted <https://plus.google.com/s/%23K40GettingStarted> Ok, I go...](https://plus.google.com/+DonKleinschnitz/posts/bsa2PUhQhf1)


---
**Red Naxel A** *August 17, 2017 13:03*

**+Don Kleinschnitz** awesome. Thanks.


---
**Ashley M. Kirchner [Norym]** *August 17, 2017 23:01*

As Anthony pointed out, one of the considerations is to upgrade the stock controller with a Smoothie board. My recommendation here is the Cohesion3D Mini laser upgrade bundle. [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



Several folks here use that, myself included. Support can be found on the forum at [https://plus.google.com/u/0/communities/116261877707124667493](https://plus.google.com/u/0/communities/116261877707124667493).


---
**Adrian Godwin** *August 18, 2017 00:59*

I imagine this guy is well known here, but I've just discovered a superb set of articles :



[youtube.com - SarbarMultimedia](https://www.youtube.com/channel/UCqCyShJXqnElPTUnxX0mD5A)



There's a lot of them and they're hugely detailed so they're going to take a while to work through - but I think it will be worthwhile.



I actually came across them while looking for details of a cheap power meter. I'm sure I saw a posting by Don that mentioned how useful that would be, but can't find it now to follow up. So, Don - watch this one :




{% include youtubePlayer.html id="lO9rWlobLZU" %}
[https://www.youtube.com/watch?v=lO9rWlobLZU](https://www.youtube.com/watch?v=lO9rWlobLZU)

 


---
**Don Kleinschnitz Jr.** *August 18, 2017 02:35*

**+Adrian Godwin** this is the do-hicky?


---
**Adrian Godwin** *August 18, 2017 10:13*

**+Don Kleinschnitz** Yes, that's right. It's something that could be made by yourself very easily, but needs calibration against an existing standard.


---
**Don Kleinschnitz Jr.** *August 18, 2017 12:56*

**+Adrian Godwin** yes calibration is needed for accurate measures. I have been tinkering with a peltier module because I am not so much sure we need an absolute measure of power as much as a relative one. 

In most cases I see we need to measure power loss from one optical component to another. My thought was to measure the beam throughout the optical path when it is working well and set that as the base to make future measure from using the same instrument.

My goal is to find something inexpensive, requiring no machining and easy to use.  


---
**Adrian Godwin** *August 18, 2017 13:51*

**+Don Kleinschnitz** Do you find the Peltier offers any improvement over a thermocouple ? I would have thought it more massy and harder to maintain a differential temperature.

 


---
**Don Kleinschnitz Jr.** *August 18, 2017 14:12*

**+Adrian Godwin** I don't know yet if it offers an advantage, I figured that a large area would be easier to hit and more mass would average changes. Don't know :).

I think the tough problem in either case would be the surface that the beam hits and if repeated exposure  to the same spot would cause an error in the reading. 

If I recall the dohicky and commercial units use an anodized coating on aluminum.


---
**Adrian Godwin** *August 18, 2017 14:20*

**+Don Kleinschnitz** Yes, the dohicky is anodised. Which will itself get damaged if the beam is focused on it, though that may not matter too much. I have a commercial sensor that has a rough grey surface at the bottom of a hole - but not as thoroughly buried as a black body radiator.



This is getting a bit off topic for Red's question !




---
**Don Kleinschnitz Jr.** *August 18, 2017 14:37*

**+Adrian Godwin** your right, we can move to a new thread or go to hangouts.


---
**Paul de Groot** *August 20, 2017 03:49*

**+Red Naxel A** you can order it via me. Just send me a pm or goto my website awesome.tech and send me from there an email. I will respond with the orderinf details. Once kickstarter releases the funds i will set up an up an order page on awesome.tech thanks


---
*Imported from [Google+](https://plus.google.com/103055242604375693523/posts/NkjpznCNQ4M) &mdash; content and formatting may not be reliable*
