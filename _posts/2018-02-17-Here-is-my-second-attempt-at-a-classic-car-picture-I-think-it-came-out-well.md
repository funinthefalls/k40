---
layout: post
title: "Here is my second attempt at a classic car picture, I think it came out well"
date: February 17, 2018 21:27
category: "Discussion"
author: "Printin Addiction"
---
Here is my second attempt at a classic car picture, I think it came out well. I couldn't find the correct fender emblem as google searches showed both, and the image I was given I couldn't tell, so I made them magnetic so they can be swapped. 





![images/87ef840896ddb2ff681630298973bcb5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87ef840896ddb2ff681630298973bcb5.jpeg)
![images/e96c1bc69c58ea170496774f23fca291.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e96c1bc69c58ea170496774f23fca291.jpeg)

**"Printin Addiction"**

---


---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/PydK3QQpkJP) &mdash; content and formatting may not be reliable*
