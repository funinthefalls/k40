---
layout: post
title: "anyone on the board have an idea of when ubrerclock will be getting the smotthie boards back in stock"
date: May 23, 2016 23:36
category: "Discussion"
author: "Dennis Fuente"
---
anyone on the board have an idea of when ubrerclock will be getting the smotthie boards back in stock





**"Dennis Fuente"**

---
---
**Anthony Bolgar** *May 24, 2016 00:43*

Rumor has it around 3-4 weeks


---
**Alex Hodge** *May 24, 2016 14:47*

I don't get it, you'd think they'd know by now what kind of demand they have on these boards. Constantly out of stock. I imagine they lose 3/5 customers to either an alternative smoothie based board or a different controller all together (duet perhaps) because of this constant lack of stock. No one wants to wait a month to maybe catch one of the boards before it goes out of stock for another month. Not to mention shipping time on top of it. I just don't get it. 



1. Do a massive run and get a fulfilled through amazon agreement. 

2. Sell tons of boards on Amazon Prime.

3. ???

4. Profit.


---
**Dennis Fuente** *May 24, 2016 16:09*

I agree also as i see it the boards are all set up for 5 axis just not populated for the connectors so why not just order boards and then make the connections for what ever axis board is ordered, also i am wondering if a board go's bad and your machine is down and you need it what you have to wait a month and maybe get a board


---
**Don Kleinschnitz Jr.** *May 25, 2016 03:46*

 I need one also, did a bunch of research got convinced to go laserweb/smoothie and now I cannot buy one .... waiting!


---
**Anthony Bolgar** *May 25, 2016 04:21*

If you can't wait, you can use the MKS Sbase board, it is a 5 axis smoothie derivative board, runs the smoothie firmware.


---
*Imported from [Google+](https://plus.google.com/101129481298958327367/posts/QPf1QJZvrGT) &mdash; content and formatting may not be reliable*
