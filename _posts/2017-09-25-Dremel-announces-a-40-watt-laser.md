---
layout: post
title: "Dremel announces a 40 watt laser"
date: September 25, 2017 02:56
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Dremel announces a 40 watt laser.



[https://www.dremel.com/en_US/digilab-laser-cutter](https://www.dremel.com/en_US/digilab-laser-cutter)





**"HalfNormal"**

---
---
**Ned Hill** *September 25, 2017 03:06*

The next glowforge no doubt. :P


---
**Whosa whatsis** *September 25, 2017 03:07*

This is a rebranding of the FSL Muse.


---
**HalfNormal** *September 25, 2017 03:08*

Glowforge is still a thing? ;-)


---
**Don Kleinschnitz Jr.** *September 25, 2017 03:18*

How do these things get evacuated in an office?


---
**Ned Hill** *September 25, 2017 03:19*

I'm assuming they will have a filter like glowforge is doing.  


---
**Whosa whatsis** *September 25, 2017 03:21*

Glowforge was out in full force at NYMF this weekend, and there were other booths that were using their machines, but it looks like FSL's glowforge clone, under the dremel brand, is getting ready to eat their lunch.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/T3Jna54hoBW) &mdash; content and formatting may not be reliable*
