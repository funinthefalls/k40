---
layout: post
title: "+Peter van der Walt Is adding Smoothieboard support to LaserWeb, a web-based lasercutter control interface"
date: December 16, 2015 16:37
category: "Software"
author: "Arthur Wolf"
---
+Peter van der Walt Is adding Smoothieboard support to LaserWeb, a web-based lasercutter control interface. 



If you use Smoothie in a laser cutter, it'd be awesome if you could try this out ! And report how it went back to him so he can make things even better.



<b>Originally shared by Peter van der Walt</b>



Added basic working Smoothieboard support to Laserweb: 



[https://github.com/openhardwarecoza/LaserWeb/tree/smoothie](https://github.com/openhardwarecoza/LaserWeb/tree/smoothie)



Test and let me know (:!





**"Arthur Wolf"**

---
---
**Stephane Buisson** *December 16, 2015 16:59*

definitely will, but Christmass is approaching fast ...


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/HWHjyGxRFvU) &mdash; content and formatting may not be reliable*
