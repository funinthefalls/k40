---
layout: post
title: "Newbie here.. just ordered my first K40 and willing to use laserweb"
date: November 17, 2017 09:51
category: "Hardware and Laser settings"
author: "Maurizio Martinucci"
---
Newbie here..  just ordered my first K40 and willing to use laserweb. Any tips on specific controller board and accessories?  Thanks!





**"Maurizio Martinucci"**

---
---
**Dushyant Ahuja** *November 17, 2017 11:58*

Try **+Ray Kholodovsky**​ cohesion 3d mini board. I have it and love it.  


---
**ki ki** *November 17, 2017 13:43*

I agree with Dushyant, the Cohesion3d mini board works great and is a straight swap for the nano M2 board I had in mine. Ray gives fantastic support. Be aware of import duties if importing to the EU!


---
**crispin soFat!** *November 17, 2017 18:13*

Coheeeeesh it!


---
**Maurizio Martinucci** *November 17, 2017 18:27*

Cool. Will do!


---
**Anthony Bolgar** *November 17, 2017 21:18*

I have a few C3D boards in lasers and I love the ease of installation.


---
**Maurizio Martinucci** *November 17, 2017 22:05*

That's what I need for a quick start


---
**Maurizio Martinucci** *November 17, 2017 22:07*

Just ordered the Cohesion3D Mini Laser Upgrade Bundle 


---
**Alex Raguini** *November 18, 2017 04:09*

**+Maurizio Martinucci** You won't regret it.  I replaced my m2 nano with the C3D mini laser upgrade.  It has opened a whole new world of capabilities the nano only dreams of.




---
**crispin soFat!** *November 18, 2017 06:20*

[https://plus.google.com/communities/116261877707124667493](https://plus.google.com/communities/116261877707124667493)


---
*Imported from [Google+](https://plus.google.com/117027581754233433770/posts/XLY8pdvKSP7) &mdash; content and formatting may not be reliable*
