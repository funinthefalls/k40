---
layout: post
title: "This is more of a GRBL question than a K40 Question...but since so many have put GRBL boards in their lasers I figured I would ask"
date: October 24, 2017 11:10
category: "Hardware and Laser settings"
author: "Michael Audette"
---
This is more of a GRBL question than a K40 Question...but since so many have put GRBL boards in their lasers I figured I would ask.  I currently have a GRBL 1.1 board and controller in there.  All my step/dir are setup correctly.  



I.e. if I G91 X10 Y10 it moves in the "plus X and plus Y direction 10 mm"



If I home with $H it homes to the correct location (upper left corner)...but then reports the machine position as -300,0 instead of 0,275?  Anyone know what I need to twiddle to make this work correctly?



All other motion works ok...it's just $H and the reported machine location.









**"Michael Audette"**

---
---
**greg greene** *October 24, 2017 12:35*

It may be working correctly, the -300 is relative to the lower left Cartesian position, but others may know more than I about those boards


---
**HalfNormal** *October 24, 2017 18:21*

**+greg greene** is correct. According to the author of GRBL, it is done to keep everything consistent in the world of CNC coordinates. Your only true way to fix this is to move the physical limit switch to the lower left corner. 


---
**Michael Audette** *October 25, 2017 01:49*

Thanks everything else works awesome...except when think my psu is going.  Even at full power only gettIng 10-12 ma.  Such is life.  NothIng I can't change with a little code anyhow.


---
**Paul de Groot** *October 25, 2017 05:38*

**+HalfNormal** or swap the motor y axis in mirror and use the software to mirror the image or pattern you want to engrave or cut. I opted for the last option in my kickstarter since my backers were reluctant to install a 90 degree bracket for the end stop and a flag.


---
**Claudio Prezzi** *October 25, 2017 07:51*

You don't need to change anything at the hardware level! 

Grbl just sets the machine origin (0/0) to top right after homing. So if you have configured $130=300 and $131=200 and home to top left, you will get -300/0 after homing. You can then send <b>G10 L20 P0 X0 Y0 Z0</b> to set that position to 0/0 (work orgin).


---
**Claudio Prezzi** *October 25, 2017 08:02*

Alternatively, you can force grbl to use the homing position as 0/0 by activating the line #define HOMING_FORCE_SET_ORIGIN in config.h.


---
**Claudio Prezzi** *October 25, 2017 08:04*

By the way: <b>G91 X10 Y10</b> is not a complete command. You should also send a move command G0/G1/G2/G3 after the G91, i.e. <b>G91 G0 X10 Y10</b>.


---
**HalfNormal** *October 25, 2017 12:28*

**+Claudio Prezzi**​ All true but the y will still be a negative move and need to be reflected in the code.


---
**Michael Audette** *October 25, 2017 12:42*

Meh...just going to keep it the way it is.  Everything else is working.  When I work  I usually position the laser where I want it and set a work zero anyhow.  The machine moves the way I expect it to with G91 G0 X10 Y10...so it's all good.  Alternatively I could home then force the value I know the home position to be with some GCODE then the absolute moves will put me right where I expect to be.  It's really not bad...RAMPS had a way to change the homing offset so I didn't see this before.  But the comms problems finally drove me nuts (worked great with SD cards though).



I'll just change my HOME GCode to 

G28

G92 X0 Y270 Z0



The machine will be where I expect it to be if I tell it to go (absolute) to X0 Y0 it will be in the lower left corner where I want 0,0 to be 



Now sadly my laser is all working again but only at about 1/2 power (I think the flyback fried or the laser PSU).  It had a couple quick full power blasts that might have been enough for the power supply.


---
*Imported from [Google+](https://plus.google.com/102920303032765220786/posts/ijXKcjXWHku) &mdash; content and formatting may not be reliable*
