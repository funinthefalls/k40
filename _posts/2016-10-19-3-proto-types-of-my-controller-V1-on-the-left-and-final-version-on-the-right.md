---
layout: post
title: "3 proto types of my controller. V1 on the left and final version on the right"
date: October 19, 2016 08:19
category: "Modification"
author: "Paul de Groot"
---
3 proto types of my controller. V1 on the left and final version on the right. Can not be happier with the end result. My current set up is with the middle proto type. I was not happy with the ribbon cable position and the stepper output tracks which i enlarged. Just plug and play.



![images/dd94b8f06b8e11afb4de56b3d696f90e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd94b8f06b8e11afb4de56b3d696f90e.jpeg)
![images/348ea322cb96c0b1c178428fb2ed37ea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/348ea322cb96c0b1c178428fb2ed37ea.jpeg)

**"Paul de Groot"**

---
---
**HalfNormal** *October 19, 2016 12:54*

What microprocessor is your controller based on?


---
**Maxime Favre** *October 19, 2016 14:16*

Looks like an arduino uno pining. 


---
**Don Kleinschnitz Jr.** *October 19, 2016 14:30*

**+Paul de Groot** you have answered my question from over on our LPS discussion thread.



Your LPS is a green-green configuration.



It is not the same supply I am tracing, yours looks like it might be a discrete component version whereas my green-green is smd.



When you get a moment can you see if you have continuity between WP on the middle connector and L on the right (dc power) connector?



I assume your middle connector is H,L,WP,G,IN,5V  and your right connector is 24V, Gnd, 5V, L.


---
**Don Kleinschnitz Jr.** *October 19, 2016 14:34*

**+Paul de Groot** BTW what did you end up using for the PWM control?


---
**Paul de Groot** *October 19, 2016 20:22*

**+HalfNormal** 

![images/6d7b05092bb2b7b77e3d6820ef03feed.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d7b05092bb2b7b77e3d6820ef03feed.jpeg)


---
**Paul de Groot** *October 19, 2016 20:23*

It is based on an atmel xplained mini basically a super Uno pb328pb with 4 timers to allow 16bits engraving....


---
**Paul de Groot** *October 19, 2016 20:23*

**+Maxime Favre** yes it is uno pin compatible!


---
**Paul de Groot** *October 19, 2016 20:26*

**+Don Kleinschnitz** indeed my lps H,L, WPS, IN,5V on the middle connector and 24v, gnd,5v, laser in. Will check if WP in and L are connected thru on the pcb. I suspect so. 


---
**Timo Birnschein** *October 20, 2016 03:42*

I love this!


---
**Paul de Groot** *October 20, 2016 04:01*

**+Timo Birnschein** you are welcome. I was looking at a plug and play solution for people who don't have the skills to replace the nano m2 board or the gutts to chop wires.


---
**Timo Birnschein** *October 20, 2016 04:04*

**+Paul de Groot** I did a similar conversion on mine using an Arduino Nano and a custom driver board not more than 2 weeks ago! It's running grbl and for the first time I have a tool chain that really works with the laser! My solution is plug 'n play as well. We need more of these solutions so that people don't have such a hard time wiring everything up... It's too easy to make mistakes and then ... flames...




---
**Paul de Groot** *October 20, 2016 04:11*

**+Timo Birnschein** fabulous. How did you go with the engraving function? 


---
**Timo Birnschein** *October 20, 2016 04:18*

**+Paul de Groot** I did two major things: First, there is a grbl floating around which was extended by Nick Williams for LaserInk which supports realtime planned S commands for the variable spindle speed. It was an older version so I ported all his changes into the current grbl Master 0.9j which runs perfectly fine on my Nano. Second, I wrote my own gCode sender which is currently with my Chinese Nano copy roughly half the speed of PicSender. So I reach about 2800mm/min at 0.1mm/pixel resolution. That way I can engrave with the K40 at a reasonable speed and have grbl running with it.



In addition - and I hope here comes the cool part - grbl v1.1, which just came out two days ago, supports laser engraving out of the box now! But I haven't tested that yet because mine works nicely for the moment.



You can find all my stuff here: [https://github.com/McNugget6750](https://github.com/McNugget6750)

[github.com - Timo](https://github.com/McNugget6750)


---
**Paul de Groot** *October 20, 2016 04:30*

**+Timo Birnschein** nice to see that! Will try the newest grbl as well. The github cheton gcode sender. It's marvelous and very nice to work with. Especially i can home my machine and use a webcam to position my work pieces without misalignment. Ideal for engraving on objects like wallets. Never have a failing engraving anymore☺


---
**Paul de Groot** *October 20, 2016 04:31*

Https:// [github.com/cheton](http://github.com/cheton) /cnc

[github.com - Build software better, together](http://github.com/)


---
**Nick Williams** *October 20, 2016 20:55*

Very nice:-) how do I get one??


---
**Paul de Groot** *October 20, 2016 21:36*

**+Nick Williams** will build a few and if you like I can drop one in the mail for you so you can play with it and give me feedback😀Just pm your address


---
**Nick Williams** *October 21, 2016 19:52*

**+Paul de Groot** Hey just wanted to check I sent a PM to you yesterday but this was my first PM on Google+ so not sure I did it right please let me know if you didn't get it.

Thanks


---
**Nick Williams** *November 04, 2016 21:41*



**+Paul de Groot** Hey Guys Paul hooked me up :-) Not quite ready to go but soon.



![images/151753fec6e21da490c6030ad09c6e1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/151753fec6e21da490c6030ad09c6e1c.jpeg)


---
**Anthony Bolgar** *April 25, 2017 18:20*

I know this thread is old, but do you have any of your replacement boards for the K40 for sale? I would like to try it out. Thanks.


---
**Paul de Groot** *April 25, 2017 19:09*

I am taking the boards to the san mateo faire so if you happen to be in the usa, I can give you one at the faire.


---
**Anthony Bolgar** *April 25, 2017 19:59*

I live in Niagara Falls, Canada but have a shipping address in Niagara Falls, NY  Would there be any chance of having one shipped to Niagara Falls?


---
**Paul de Groot** *April 25, 2017 21:25*

Yes no problem. After the faire I will drop it off at a mailbox. Pls can pm your address?☺


---
**Anthony Bolgar** *April 26, 2017 02:57*

sounds great, will PM you


---
**Anthony Bolgar** *April 26, 2017 03:04*

I can't find your name in the 20 or so Paul de Groots in Google for some reason. Can you start a PM to me and I will leave the info?


---
**Paul de Groot** *April 26, 2017 04:00*

Sure no problems 


---
**Anthony Bolgar** *April 26, 2017 04:42*

I am not worried about my address being out there, it is my business address so it is already publicly posted. You can send the card to:



Niagara Clock

Anthony Bolgar

1711 Cudaback Avenue

PMB# 5015

Niagara Falls, New York 14303



Please let me know what the cost is and I will paypal you the money. 



Thank you for doing this for me, it is much appreciated.


---
**Paul de Groot** *April 26, 2017 04:55*

**+Anthony Bolgar** no problems. Don't worry about the cost. Hopefully you can provide some publicity about it once you play with it.😊


---
**Anthony Bolgar** *April 26, 2017 05:11*

Thank you very much Paul.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/i7qmtMHkFRX) &mdash; content and formatting may not be reliable*
