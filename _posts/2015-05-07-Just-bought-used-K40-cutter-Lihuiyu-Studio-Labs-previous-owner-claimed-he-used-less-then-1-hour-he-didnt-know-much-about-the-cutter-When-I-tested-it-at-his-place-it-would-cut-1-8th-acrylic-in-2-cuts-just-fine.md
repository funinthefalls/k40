---
layout: post
title: "Just bought used.. K40 cutter \"Lihuiyu Studio Labs\" (previous owner claimed he used less then 1 hour, he didn't know much about the cutter) When I tested it at his place it would cut 1/8th acrylic in 2 cuts just fine..."
date: May 07, 2015 07:52
category: "Materials and settings"
author: "jamie gomez"
---
Just bought used.. K40 cutter "Lihuiyu Studio Labs"  (previous owner claimed he used less then 1 hour, he didn't know much about the cutter) When I tested it at his place it would cut 1/8th acrylic in 2 cuts just fine... after a 6 hour drive coming back home I setup the laser and couldn't cut through.... Im not sure if something has been moved around due to 6 hour drive with obviously had some bumps



The laser draw software on speed 1 does cut really slowly and cut through in 1 pass but I can't replicate effects using coreldraw..

I use auto cad and then transfer my file over but can't figure out how to load it on the lawserdraw software so I can cut. I Really need help getting it to cut again on corel draw





Can someone please help me make this work? What am i doing wrong?  Im a student and put a lot of money into this so I can work on my next business!







Other questions :



1. is there a way to explain the cutting area horizontally ?

2. Water I have been typically keeping cold and putting ice to keep it really cold

3. What DPI settings? I will always be cutting 1/8 or 1/16 acrylic 

4. Does air assist help?





**"jamie gomez"**

---
---
**Stuart Middleton** *May 07, 2015 09:02*

Hi,

In my tests, air assist really works well for wood. It increases cut depth and minimises scorching. I've not tested on acrylic yet.



Also, and others may correct me if I'm wrong, but putting ice in the water may be a bad idea. If the tube is too cold when it fires, it may crack the glass. Someone with a lot more experience than me can either tell you if this is true or not, but my laser instructions specifically said "check there is no ice in the water before turning on, otherwise the tube may break".



Your cutting issues are probably the laser alignment moving during transit. There are plenty of laser alignment tutorials online if you need to do them. But remember, when aligning the laser, if you have the covers open when test firing, always wear goggles!


---
**jamie gomez** *May 07, 2015 18:14*

How often do you change the water? Do I just buy a container with water already in it and put the pump in?


---
**Stuart Middleton** *May 08, 2015 07:13*

I've only had my laser a few months and have only changed the water twice. I have a sealed container for the water so it won't get dirty. Lots of people use distilled water too. I may try that. In theory, in a closed water system, you should never need to change it as everything is clean and dirt can't get in.


---
**jamie gomez** *May 09, 2015 17:18*

how big container do you have? How do I avoid it from getting too hot? Ive heard of people using anti freeze / coolant?  Im running this laser a lot of 100% power, since Im cutting 1/8 and 1/16- I did end up getting to cut.

Mainly 1/16 @1.75 speed but my last concern is keeping it cool


---
**Chris M** *May 10, 2015 21:24*

I'm sure there's something not quite right about your cutter, most likely optical alignment. I just cut some 1/8" acrylic in one pass at 10mm/sec and 50% power.


---
**Stuart Middleton** *May 11, 2015 07:51*

Do you have air assist? What brand of acrylic are you using? What power do you need when cutting 3mm MDF for instance? I need 5mm/sec and full power on this too.


---
**jamie gomez** *May 11, 2015 09:54*

Yea I'm still trying to get a better alignment,should I try my best to get the dot in the complete center?



So far my results are 1/16th acrylic @ 1.75 with 100% power to cut through 

1/8in P95 acrulic at 1 with 100% power to cut through


---
**jamie gomez** *May 11, 2015 09:55*

yes I have air assist. 5MM/S for 1/8 in? Not sure if its because p95 is expensive material mines not cutting?


---
**Chris M** *May 11, 2015 20:54*

I've been cutting 3mm MDF at 10mm/sec and 65% power. Tonight I installed air assist and tried some 3mm acrylic, 10mm/sec at 30% power worked fine. I only have a small piece and the stickers have been discarded. I do have another sheet but it's too expensive to turn to scrap :)



Of course, I don't know what 30% and 65% power on my machine come out at in mA.


---
**Stuart Middleton** *May 12, 2015 08:24*

Hmmm. That's not good. I've aligned the mirrors and cleaned everything and it needs 99.9% to go through 3mm MDF at between 5-10mm/sec depending on if it flames or not while cutting. I've not got air assist yet which seems to help a lot for people. 

I think I need to run some tests and re-check all of my alignments.


---
**Chris M** *May 12, 2015 17:19*

With the air assist, I've been able to cut 3mm MDF at 15mm/sec with 40% power. Surprising improvement, and (from the few small things I've tried) more consistent cuts.


---
*Imported from [Google+](https://plus.google.com/114723275374564353861/posts/B4fvYLgm3NC) &mdash; content and formatting may not be reliable*
