---
layout: post
title: "Yuusuf Sallahuddin This is what I have planned in my head"
date: December 18, 2015 01:13
category: "Hardware and Laser settings"
author: "ChiRag Chaudhari"
---
**+Yuusuf Sallahuddin** This is what I have planned in my head. Well now on paper too...LOL. So it does not have to be 30T pulley, anything that can fit over that shower door roller so the entire thing can be driven by GT2 belt and we have our Z-Table. 







![images/53ceb54de7785e473995d65b182bd3d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/53ceb54de7785e473995d65b182bd3d4.jpeg)
![images/e3c271ec6e8380d66a738bd284900a87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e3c271ec6e8380d66a738bd284900a87.jpeg)

**"ChiRag Chaudhari"**

---
---
**ChiRag Chaudhari** *December 18, 2015 01:18*

On K40 machine there is not much room to play, but I think there will be 1-1.5" movement of z -table so that you can engrave a some thick stuff too.



Here is the link to High Res picstures: [https://drive.google.com/folderview?id=0B6H9KiLgiWqKdVU3RVg1OHlkMU0&usp=sharing](https://drive.google.com/folderview?id=0B6H9KiLgiWqKdVU3RVg1OHlkMU0&usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 01:36*

Drawing is good. I have a little difficulty reading some things to begin, but I figured it out. So, is 32 teeth okay? that will be much easier to work with (as I can draw 2x 16 sided figure, which is very easy). Also, what is the centre bore requirement?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 18, 2015 01:44*

Another question, I am correct that the 2mm spacing is from centre of each circle to centre of next circle?


---
**ChiRag Chaudhari** *December 18, 2015 02:09*

**+Yuusuf Sallahuddin** Let me get back to you in 10 min. Bit busy at store.


---
**ChiRag Chaudhari** *December 18, 2015 02:25*

Here is link to all the pics in high-res:



[https://drive.google.com/folderview?id=0B6H9KiLgiWqKdVU3RVg1OHlkMU0&usp=sharing](https://drive.google.com/folderview?id=0B6H9KiLgiWqKdVU3RVg1OHlkMU0&usp=sharing)


---
**ChiRag Chaudhari** *December 18, 2015 02:43*

**+Yuusuf Sallahuddin** Yes 2mm distance between the center of the circle. Number of teeth can be anything.



oh wait there are few more replies here now, Let me get myself updated!


---
**Kirk Yarina** *December 18, 2015 18:56*

The shape of a GT2 pulley is, I understand, proprietary and not just half circles.  I have some gear design software (gearotic) that will do GT2 pulleys and produce dxf files.  Send me the specs (tooth count and bore size) and I can send you either a dxf or stl file for one that you can try.


---
**ChiRag Chaudhari** *December 18, 2015 19:53*

I dont have dimensions but I was referring to these images to design one myself.

1. [https://www.adafruit.com/datasheets/gt2tooth.jpg](https://www.adafruit.com/datasheets/gt2tooth.jpg)

2. [https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/42aa98f47e8a7d0ca059c570d3d5c3b3/large.JPG](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/42aa98f47e8a7d0ca059c570d3d5c3b3/large.JPG)

3. [http://img.vip.alibaba.com/img/pb/065/823/577/577823065_223.jpg](http://img.vip.alibaba.com/img/pb/065/823/577/577823065_223.jpg)


---
**Kirk Yarina** *December 19, 2015 19:16*

I used a 22mm bore diameter, let me know if you need it changed, it's real easy to do.  The linked (hopefully...) zip file includes both 32 and 22 tooth pulleys (22 is the biggest for that shaft).  HTH



[https://drive.google.com/open?id=0B0WQ4GaVdmNxcThidEFKenIwM1U](https://drive.google.com/open?id=0B0WQ4GaVdmNxcThidEFKenIwM1U)



You'll probably need to scale them (try 99%) to get the teeth to mesh with the belt, to allow for the beam size, so try some test cuts in something cheap to test the fit.  The belt should fit without any stretching or gaps.



Kirk




---
**ChiRag Chaudhari** *December 19, 2015 20:28*

I will check it out soon thanks. I will have to go use laser at university, as my new machine has some issue. It does not produce steady power.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/YgMsqpdCxGN) &mdash; content and formatting may not be reliable*
