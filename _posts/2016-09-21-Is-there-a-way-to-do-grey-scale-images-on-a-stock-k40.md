---
layout: post
title: "Is there a way to do grey scale images on a stock k40?"
date: September 21, 2016 04:24
category: "Discussion"
author: "Cody McNary"
---
Is there a way to do grey scale images on a stock k40? I've been reading for days and have yet to get a clear answer, using the provided laserdrw software





**"Cody McNary"**

---
---
**Alex Krause** *September 21, 2016 04:51*

The closest you can come is to dither an image but as far as doing a pure grayscale engrave you will need to upgrade your controller


---
**Alex Krause** *September 21, 2016 05:29*

**+Cody McNary**​ an example of dithered image you can do with stock controller

![images/16288faece0263fa81ad3b5a3364c08a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16288faece0263fa81ad3b5a3364c08a.jpeg)


---
**Alex Krause** *September 21, 2016 05:30*

That took alot of practice tho so don't get frustrated if you don't get the results you want


---
**Cody McNary** *September 21, 2016 05:50*

Thank you, will you please explain the process of dithering? And do I need to set the laser to low power or higher 


---
**Alex Krause** *September 21, 2016 05:53*

Dither is done in a photo shop program by processing an image to one bit dots... this is the way old magazines and news paper processed images more dots per inch = darker, less dots per inch = lighter... I do all my image rasters between 4-7ma depending on image


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 21, 2016 07:13*

If you use Photoshop, I recently came across an awesome feature that makes turning a full colour image into B&W very easy (retaining the level of detail you wish to keep). That is done using a method called Threshold. I'll drop a before & after pic here to show the results (also with Diffusion Dither for comparison). Note: no pre-processing of the image was done before doing Threshold or Diffusion Dither).

![images/b02747c120276fc0ef6e7188fe79e9e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b02747c120276fc0ef6e7188fe79e9e9.jpeg)


---
**Jim Hatch** *September 21, 2016 12:16*

You can also use Corel to prepare the image. You don't need Photoshop in case you don't have that. Search on YouTube for laser engraving photographs. A few decent videos out there showing the process.


---
**Alex Krause** *September 21, 2016 13:20*

I personally use GimpShop for almost all my photo shop needs


---
**Cody McNary** *September 23, 2016 21:17*

Thank you all very much, I managed to work GIMP2 into dithering an image for me, and have come to the conclusion I need higher resolution photos. The first attempt came out well on an anodized sheet of aluminum at 5ma, just a little grainy. 


---
*Imported from [Google+](https://plus.google.com/114860388792227756325/posts/2CPaTKREGGF) &mdash; content and formatting may not be reliable*
