---
layout: post
title: "1$ per 12\"x12\" sheet not a bad price"
date: September 13, 2016 18:09
category: "Material suppliers"
author: "Alex Krause"
---
1$ per 12"x12" sheet not a bad price





**"Alex Krause"**

---
---
**Anthony Bolgar** *September 13, 2016 18:38*

Great price, shipping on 30pcs is only $13


---
**Alex Krause** *September 13, 2016 18:49*

1/8" X 12" X 12" Baltic Birch Plywood Great for Laser, Cnc, and Scroll Saw. 45pc Woodpeckers® [amazon.com - Robot Check](https://www.amazon.com/dp/B013NT3OAC/ref=cm_sw_r_other_apa_EOe2xbZP64X1P)


---
**Alex Krause** *September 13, 2016 18:50*

That is about 10c cheaper per sheet but you can't pick the quantity you want


---
**Ashley M. Kirchner [Norym]** *September 14, 2016 05:34*

Since the stock K40 does 8x12, I buy $2 12"x24" from Ocooch, then cut them down to 3 pieces of 12x8. $0.67 each. I tried using 12x12 pieces before, and having to flip them to use the remaining 4" just wasn't worth it. A lot of times I don't have pieces small enough to fit on that, nor can it cut something larger than 8" in one axis, so why bother. My opinion of course. YMMV.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/KyaSMpLQHNM) &mdash; content and formatting may not be reliable*
