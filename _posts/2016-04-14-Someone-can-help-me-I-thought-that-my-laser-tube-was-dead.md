---
layout: post
title: "Someone can help me? I thought that my laser tube was dead"
date: April 14, 2016 00:14
category: "Discussion"
author: "Pedro David"
---
Someone can help me? 

I thought that my laser tube was dead. I bought a new tube and also does not work. The machine works but the wattmeter not. 

The laser started to lose power and then died...

Thank you. 





**"Pedro David"**

---
---
**3D Laser** *April 14, 2016 00:27*

If the watt meter doesn't work that is a good sign it's the power supply.  That was my issue anyway 


---
**Pedro David** *April 14, 2016 01:15*

What did you do? bought a new one?


---
**Anthony Bolgar** *April 14, 2016 02:46*

Yes, it sounds like a PSU problem to me as well. Replacements are available on ebay for around $100- $150 .


---
*Imported from [Google+](https://plus.google.com/113379323980322370039/posts/RxN8H8n6GcD) &mdash; content and formatting may not be reliable*
