---
layout: post
title: "Ok, I am inching toward going the Smoothie LaserWeb route for my K40 replacement controller"
date: May 01, 2016 19:48
category: "Smoothieboard Modification"
author: "Don Kleinschnitz Jr."
---
Ok, I am inching toward going the Smoothie LaserWeb route for my K40 replacement controller. 

I have a few remaining questions that are keeping me from completing my conversion design and I am sure there are answers on this forum somewhere, so I apologize for the annoying repeat. 

In any case I plan to keep looking until I get my conversion designed and then plan to document and share this adventure for the next explorer. 

I find that there is a lot of information but not necessarily in one place nor synced with my model (if K40 has such a concept) machine.



In regards to the smoothie conversion from my K40 with M2Nano:



1. Does the K40 DC supply have enough 24 and 5V capacity?

2. Has anyone had to replace laser power supplies to get good PWM control and what supply did you use.

3. Does it support a rotary table and if so what one? Do I unplug the Y stepper and plug the rotary table into that connector?

4. Anybody converted optical endpoint sensors that I can reference for my conversion. Probably will use a middleman board.

5. What is the story on engraving support from what I can see it doesn't? What if I want engraving also :)?

6. Has anyone been using Sketchup with this setup?

7. Does it support a Z table and if so can you operate it from the display panel/LaserWeb. I have a LO lift table that is standalone.

10. I see that you can connect to SSR's from the base board. Can you set it up so these relays turn on a specific general times like, before and after cutting?

11. Is there an interlock circuit that I can use for water flow, water temp, lid interlock etc and does this function stop the lasers operation.



I will eventually answer all these but any help is appreciated .... my final design will document all the answers and be posted.





**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *May 01, 2016 20:55*

All good questions. Connecting another stepper to smoothie is no issue so controlling a Z axis (table)  it's possible. LW2 allready support custom presets so adding focused levels it's easy) 


---
**Don Kleinschnitz Jr.** *May 01, 2016 21:28*

Ok so I can use a spare stepper connector and have that controlled from a preset in LW2?


---
**Ariel Yahni (UniKpty)** *May 01, 2016 22:09*

Yes. So let's say you focus on some material at 15mm from 0,  your create a preset G0 Z15


---
**Don Kleinschnitz Jr.** *May 01, 2016 23:49*

Nice, ..... I'm getting hooked.


---
**Ariel Yahni (UniKpty)** *May 01, 2016 23:57*

All this features and workflow simplification at and affordable price are some of the reasons **+Peter van der Walt**​ started developing LW


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/JV4HBLz9i6q) &mdash; content and formatting may not be reliable*
