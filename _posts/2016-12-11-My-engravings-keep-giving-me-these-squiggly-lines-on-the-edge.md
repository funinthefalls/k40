---
layout: post
title: "My engravings keep giving me these squiggly lines on the edge"
date: December 11, 2016 17:26
category: "Object produced with laser"
author: "Shane Lynch"
---
My engravings keep giving me these squiggly lines on the edge. Any idea how to fix this? 

![images/4e5c8ae3ed486e4752d07244eda5979c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e5c8ae3ed486e4752d07244eda5979c.jpeg)



**"Shane Lynch"**

---
---
**Ned Hill** *December 11, 2016 17:58*

Can you provide some details on your setup? 


---
**Ned Hill** *December 11, 2016 18:10*

Does this happen on other materials as well? Your material looks like leather and the ripple size appears to be on the same order as the material ripple so I'm wondering if it's tied to the material some how.


---
**Jeremie Francois** *December 11, 2016 18:20*

Actually the quiggly lines are quite pretty imho. But I understand that was not planned ;)

More seriously, yes, we would need more info about the driving mechanism.

E.g. it could be non-round (or even dirty) pulleys on belt-driven carriers, as the waves seem to be a cyclic X offset according to the absolute Y position. Check tension, free play at the head, etc.


---
**Alex Krause** *December 11, 2016 18:23*

How fast are you engraving?


---
**Shane Lynch** *December 11, 2016 18:23*

my setup is a standard k40 with no mods (although i just put in a honeycomb bed an hour ago).



Using 2mm veg tan and i had a few different speed settings (350mm, 250m, 175mm) but they all came out with the squiggly edges.


---
**Shane Lynch** *December 11, 2016 18:24*

im going to run the same file on mdf and see what happens.


---
**Alex Krause** *December 11, 2016 18:26*

Make sure your mirrors are secured and not vibrating during motion also make sure you lens is correctly seated in the holder and that the holder is nice and tight I have. Seen this with loose belts, loose mirrors and free floating lenses from not being completely secured 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 11, 2016 18:37*

I've not had these sort of results on leather before, however most of my vegtan leather I have used doesn't seem to contain such large ripples in the finish.



I think Alex is on the money. I have noticed this with speeds that are too high, but the speeds you mention seem fine. I was using up to 500mm/s before I upgraded to smoothie/LW.



Also, I wonder the quality of the original image that you are working with? Is it created in Corel Draw, or an imported jpg or something? Could be the antialising & artefacts on edges of shapes is registering in the plugin as something to engrave? Just some thoughts...


---
**Claudio Prezzi** *December 12, 2016 08:32*

I guess this is done as raster engraving? 

Could it be the same distance than your raster lines have (laser diameter)?


---
**Coherent** *December 12, 2016 14:06*

+1 **+Alex Krause** "Seen this with loose belts, loose mirrors and free floating lenses from not being completely secured" 



Also check that the motors are mounted securely and the pulley set screws are tight on the motor shafts. A pulley that is not secured to the shaft will shift a tiny bit when it changes direction causing this issue.


---
*Imported from [Google+](https://plus.google.com/102016608119814536184/posts/E79E6a8RV3a) &mdash; content and formatting may not be reliable*
