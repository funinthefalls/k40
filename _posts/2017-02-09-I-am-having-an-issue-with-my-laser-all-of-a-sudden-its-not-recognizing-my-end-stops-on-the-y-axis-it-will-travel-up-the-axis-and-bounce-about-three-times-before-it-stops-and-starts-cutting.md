---
layout: post
title: "I am having an issue with my laser all of a sudden it's not recognizing my end stops on the y axis it will travel up the axis and bounce about three times before it stops and starts cutting"
date: February 09, 2017 00:39
category: "Discussion"
author: "3D Laser"
---
I am having an issue with my laser all of a sudden it's not recognizing my end stops on the y axis it will travel up the axis and bounce about three times before it stops and starts cutting.  I have been cutting like crazy so could the debris be causing it not to recognize 





**"3D Laser"**

---
---
**Ariel Yahni (UniKpty)** *February 09, 2017 00:48*

Check for loose connection on the endstops, cable, solder joints. Typically this is the behaviour 


---
**greg greene** *February 09, 2017 01:02*

If your K40 uses Opto end stops - make sure they are clean - a Q tip works.  


---
**3D Laser** *February 09, 2017 01:30*

**+greg greene** how do I tell


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 09, 2017 01:35*

**+Corey Budwine** The optical endstops have a funny U-shape in them that gets registered as reaching the end when something gets into the U. Mechanical endstops are like a switch or button, physically getting pressed (I think).


---
**3D Laser** *February 09, 2017 02:01*

**+Yuusuf Sallahuddin** I have the I shaped ones.  I cleaned it with a bit of rubbing alcohol now it only bounces once then homes but it still bounces any other ideas 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 09, 2017 02:31*

**+Corey Budwine** Unfortunately I don't really know much about these sort of things, so hopefully someone else can weigh in for this.


---
**Anthony Bolgar** *February 09, 2017 14:06*

**+Don Kleinschnitz** knows about the optical stops




---
**Don Kleinschnitz Jr.** *February 09, 2017 18:07*

**+Corey Budwine** first review the sections of the index regarding end stops to become familiar with them.



You can try the static tests linked below but this will not necessary show you a dynamic problem. If these pass we can try something else.

...............................

What controller do you have:

1. Stock K40

2. Converted to something?

........................

Here is an experimental test procedure for static testing end stops



[https://docs.google.com/forms/d/e/1FAIpQLSeJdLiM9SuLu3m7TbTnfGcV2HIlnTFI5mJP8THqdm3QYh88sQ/viewform](https://docs.google.com/forms/d/e/1FAIpQLSeJdLiM9SuLu3m7TbTnfGcV2HIlnTFI5mJP8THqdm3QYh88sQ/viewform)



............................ Index .......................

[donsthings.blogspot.com - A Total K40-S Conversion Reference](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Pablo Verity** *February 14, 2017 21:56*

I've just had the same after a rebuild. It was the metal stops not aligning in the optical slot. Bend & adjust as necessary.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/P97S1Zy42je) &mdash; content and formatting may not be reliable*
