---
layout: post
title: "First attempt at granite"
date: December 28, 2016 00:39
category: "Object produced with laser"
author: "Robert Selvey"
---
First attempt at granite.

![images/03f203439ca60a71470a159230071344.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03f203439ca60a71470a159230071344.jpeg)



**"Robert Selvey"**

---
---
**greg greene** *December 28, 2016 00:45*

speed, power?




---
**Robert Selvey** *December 28, 2016 01:21*

5ma/100mm


---
**Jonathan Davis (Leo Lion)** *December 28, 2016 04:58*

Interesting.


---
**Bob Damato** *December 28, 2016 12:27*

That granite came out a little better than mine. I used the dark granite, and the flakes in it, while look nice, give very inconsistent patterns. 




---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/EvoM5wHk8Ui) &mdash; content and formatting may not be reliable*
