---
layout: post
title: "Strange issue with power dial: Has anybody else experienced a change in positioning with their setting dial?"
date: October 06, 2016 12:22
category: "Original software and hardware issues"
author: "Adam J"
---
Strange issue with power dial: Has anybody else experienced a change in positioning with their setting dial? Where I'd turn the dial for 7ma of power has now become 16ma and so on. I used to have to turn the dial about a 1/6th of it's span to get any laser fire (around 4ma). Machine seems to be working okay, just not sure why it's decided to change like this? The dial knob is not loose or anything. Odd!





**"Adam J"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 12:47*

I've had similar things happen with my dial, especially when I accidentally turned it too far. Seems that once I did that all the positions changed, but it still functions correctly.


---
**Scott Marshall** *October 06, 2016 16:17*

The Potientiometers in the K40 have a problem with the internal wiper coming loose, and are of generally poor quality.



Yours is probably just wearing out, which mine did in a couple of months. A good quality wire wound pot (expensive, but very precise and reliable) or high quality carbon/carbon poly one will do the job too. 



Values vary from 1 K to 10K in the k40s I've run across, and I prefer to go to the low end of the value range.

The pot divides 5 volts down to 0-5 volts and the value only effects the current flow thru the resistance element. this doesn't change how it performs, but pots tend to stay cleaner and last longer with a little current through them, especially thru the wiper.  With the 1K pot, current flow is only 1ma so there's no danger of overload, and it may just last a bit better than the higher value,



To boil it down, a 1K wirewound pot, possibly a 10 turn model with a vernier dial if you like the fine control would be a worthwhile upgrade that will be under 20bucks for even a high end pot. If you want to get fancy, a good 10 turn with a vernier dial will be about $30, Chinese models are available for around 5 dollars, but I don't care for them.



Digikey has a Bourns 10 turn PN 3590s-1-102L 

and a A Precision Electronics PN GU1021S26 both about $15,

Chinese Counterfeits (of the Bourns usually) can be found on Ebay for as low as 2 bucks, some are OK, others not. 



The same rules apply for the vernier knob,  Digi-key has a very nice Vishay Scale Dial for about $15 (Vishay 18A11B10), they also carry a plastic one for about 5 bucks and Chinese versions on Ebay are 2 bucks and up.



If you want a good quality single turn stock replacement, CTS PN 026TB32R102B1B1 is about 5 bucks and will work great, far nicer than the original ever did.



Replacement is simple, but does require soldering. If you need help in that dept, drop me a note, I'll help you out.



Scott


---
**Bob Damato** *October 06, 2016 16:23*

Along these lines Id love to replace my crappy uncalibrated POT with a digital readout. Is this even possible? The way it is now I cant take notes on what works. But with a digital readout I can say "engraving on grade A birch is 32" etc.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 17:19*

**+Scott Marshall** Is it possible to replace the knob-potentiometer with a linear-potentiometer? Like this one: [https://www.seeedstudio.com/Grove-Slide-Potentiometer-p-1196.html](https://www.seeedstudio.com/Grove-Slide-Potentiometer-p-1196.html)



I'd be interested in having a left-right sliding scale rather than a circular scale. Would be much easier to calibrate the markings (instead of having to implement trigonometry into my calculations) & I think it would look nicer.


---
**HalfNormal** *October 06, 2016 19:14*

**+Yuusuf Sallahuddin** A pot is a pot regardless if it is circular or straight. It just has to be the correct value. 


---
**HalfNormal** *October 06, 2016 19:16*

**+Bob Damato** A digital readout can be had by adding a digital volt meter between the wiper and ground side of the pot.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 06, 2016 19:48*

**+HalfNormal** Cool, so as per Scott's response above, if I get a 1K linear-pot then all I do is hook it up the same way the current pot is hooked up & I'm good to go? Sounds like a job for the near future :)


---
**HalfNormal** *October 06, 2016 19:51*

**+Yuusuf Sallahuddin**​  yep. Good to go.


---
**Scott Marshall** *October 06, 2016 20:11*

The vernier is really just a mechanical version of a digital display, and you can go back to XXX any time you wish, within a degree or 2 in 3600 degrees. They actually have digital mechanical counters (about 30 bucks, search Digikey or Mouser, Jameco etc.) If your eyes have problems with small graduations (it happens trust me) or you prefer 0-1000 (which is how most of the $30 ones read)



From what I've seen, a small change (under a ma) isn't very noticable. It probably is if you're doing fine engraving or cutting paper or other delicate work.

I've worked up a bargraph display with 20 leds (0-20ma) and even have parts for it. (I got a deal on the Displays and had to find a use for em) Like everything these days, my health and ACR sales have kept me from working on all these projects I'd like to offer. If anybody wants the LED display and driver IC (bought 100 of those too) and advice on how to build one, I'll set you right up. It's not a beginner project, but most people with kit building experience could work through it. It's all thru hole and I'm intending to offer it as a kit as wll as finished. 

I'm an old Heathkit guy from way back and would like to keep the genre alive. These days people have forgotten how stuff works without a programmer. Discrete electronics have an appeal all it's own, There's programming, but it's built into the design. Besides, small kits are just plain fun to build and you learn a lot. Everybody my age started building kits they didn't understand, but learned from them HOW to design your own stuff.



Anyway on the power pot:

That's all there is to it Yuusuf. Just change out the junk pot for a good one, as fancy as your pocketbook allows....



I Guess we ought to add a "Power Pot" (how's that kit name grab you) to the ALL-Tek lineup. Plug and play of course.

If anyone needs a plug in one, drop me a line.. I'll set you up.



Scott




---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 07, 2016 03:19*

**+Scott Marshall** Power Pot sounds like a good name in my opinion.


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/PY2X3xEUCgx) &mdash; content and formatting may not be reliable*
