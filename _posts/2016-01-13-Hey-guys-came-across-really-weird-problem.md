---
layout: post
title: "Hey guys, came across really weird problem"
date: January 13, 2016 11:17
category: "Software"
author: "Tadas Mikalauskas"
---
Hey guys, came across really weird problem. I reailly believe that it is being caused from the designing piont of view, or the settings, but have no idea what is happening.. All ov a sudden some of designs startded adding additional unneeded lines that are really weir and annoying as hell. Any ideas what might be causing it? Also it started sometimes give me an error that the machine is offline even though it is online and connected...



![images/aa235f28fff31aa82e35e54ec121861f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa235f28fff31aa82e35e54ec121861f.jpeg)
![images/f8fc30bdd40a09e02dc54b710c2133f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8fc30bdd40a09e02dc54b710c2133f6.jpeg)

**"Tadas Mikalauskas"**

---
---
**Tadas Mikalauskas** *January 13, 2016 11:57*

I believe the problem is solved. I had all piece not joined into one curve so the laser joined some of the curves by itself. The post can be deleted or can stay for some people who may face the same simple problem :)


---
**Jim Hatch** *January 13, 2016 12:30*

Which software are you using for design. I've had similar things happen with imported designs and needed to use the cleanup tool to eliminate the duplicate lines where cuts would have just repeated themselves or gone double wide.


---
**Tadas Mikalauskas** *January 13, 2016 12:49*

I'm using CorelLaser, basically CorelDraw. It was an easy solution, just basically convert all outlines into an object and combine.


---
**george ellison** *January 13, 2016 17:16*

I had a similar problem but with less complex designs. I ensured it was a path before exporting as a dxf. Or once imported it into your program, ungroup or break apart. Try playing around with this. 


---
*Imported from [Google+](https://plus.google.com/113213933687083540785/posts/QTsERYHqwb5) &mdash; content and formatting may not be reliable*
