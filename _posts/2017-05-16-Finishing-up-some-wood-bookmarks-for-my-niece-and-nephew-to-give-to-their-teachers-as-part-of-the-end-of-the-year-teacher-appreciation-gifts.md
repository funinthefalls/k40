---
layout: post
title: "Finishing up some wood bookmarks for my niece and nephew to give to their teachers as part of the end of the year teacher appreciation gifts"
date: May 16, 2017 16:38
category: "Object produced with laser"
author: "Ned Hill"
---
Finishing up some wood bookmarks for my niece and nephew to give to their teachers as part of the end of the year teacher appreciation gifts.  Made from 1/8" cherry and finished with 2 coats of wipe on poly.  Etched both sides the same expect for the band teacher who is apparently a big Spiderman fanboy.  My sis-in-law is responsible for the tassels.  :)  



![images/409d30a6795db9b4397d732ce74f18a1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/409d30a6795db9b4397d732ce74f18a1.png)
![images/256d7d99a6c3b9d7cc5de0cc9eb70883.png](https://gitlab.com/funinthefalls/k40/raw/master/images/256d7d99a6c3b9d7cc5de0cc9eb70883.png)
![images/84038de62d378a05ec5f5bba3425b11e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/84038de62d378a05ec5f5bba3425b11e.png)

**"Ned Hill"**

---
---
**Anthony Bolgar** *May 16, 2017 17:46*

Very nicely done, and a gift the teachers can actually use. Great job Ned.


---
**Ned Hill** *May 29, 2017 22:11*

My niece and nephew added beads to the bookmarks.  I like the look. 

![images/135ea328a4b3ee3a0b060ea0d31045f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/135ea328a4b3ee3a0b060ea0d31045f5.jpeg)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/gzCxswCvGZk) &mdash; content and formatting may not be reliable*
