---
layout: post
title: "So far I'm having a lot of fun with this machine"
date: June 25, 2016 20:23
category: "Modification"
author: "Tev Kaber"
---
So far I'm having a lot of fun with this machine. 



The only complaint I have is the fumes.  



I have a replacement hood ([https://www.amazon.com/gp/product/B003NE59HE/](https://www.amazon.com/gp/product/B003NE59HE/)) as well as a replacement blower ([https://www.amazon.com/gp/product/B000O8D0IC/](https://www.amazon.com/gp/product/B000O8D0IC/)).  



Admittedly, my outlet to the basement window is a bit janky: a piece of cardboard with a hole cut in it, with the air hose duct-taped to it, but it seems to get the air pointed outside.



Is there anything else I can do to improve ventilation?  I'm trying not to stink up my basement.





**"Tev Kaber"**

---
---
**Ned Hill** *June 26, 2016 01:25*

The dust horn is what I bought and it works well.  I just got some 5/16" thick self stick rubber foam weather seal and stuck it to the back of the machine where the edge of the hood meets the back.  The foam is thick enough that it just wedges the hood in between the brackets.  Didn't even need to screw it in.  


---
**Ned Hill** *June 26, 2016 01:26*

How are you going to power your fan?


---
**Tev Kaber** *June 26, 2016 01:31*

Yeah, I did the same thing with weather stripping to hold it in place.  I'm using this to power the fan: [https://www.amazon.com/gp/product/B00D7CWSCG/](https://www.amazon.com/gp/product/B00D7CWSCG/)


---
**Tev Kaber** *June 26, 2016 04:40*

Oh man, I figured out the issue - the first time I did some extended cutting, the room filled with smoke. 



For my venting, I had opened a basement window, and put a piece of cardboard with a hole cut in it against the screen, and duct taped the exhaust hose to the cardboard. 



It turns out, there is a <b>second</b> pane of glass in that window, on the outside. So the screen was going...nowhere.  Once I <b>actually</b> opened the window, everything was fine and there were no more fumes.  Derp.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/FWjhEVKTPPd) &mdash; content and formatting may not be reliable*
