---
layout: post
title: "Got an old cellphone with a camera?"
date: October 12, 2016 01:00
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Got an old cellphone with a camera? Stick it in the laser and stream it ... Now I don't have to be glued to the machine and can be working on my other computer in the other room while keeping a (remote) eye on the thing.

![images/77e30fe88ee9f736f4e913b38bea0960.png](https://gitlab.com/funinthefalls/k40/raw/master/images/77e30fe88ee9f736f4e913b38bea0960.png)



**"Ashley M. Kirchner [Norym]"**

---
---
**Ariel Yahni (UniKpty)** *October 12, 2016 01:07*

What a great idea, we all have old phones with network and cameras. and this things are flat. 


---
**Ashley M. Kirchner [Norym]** *October 12, 2016 01:21*

Yup. I use IP Webcam ([https://play.google.com/store/apps/details?id=com.pas.webcam](https://play.google.com/store/apps/details?id=com.pas.webcam)) on my old Android. There are others, including stuff for iOS.


---
**Ariel Yahni (UniKpty)** *October 12, 2016 01:40*

Would you mind when possible place the camera facing the bed with the door closed?  Want to see how much view angle can we get.  I know different phone / camera difference angles but and approximation 


---
**Ashley M. Kirchner [Norym]** *October 12, 2016 01:55*

Eh, that's an old Nexus 4 phone, and on the lid, it gives me about what the hole in the bed is where the stamp clamp used to be. I'd have to hold it up about 5 inches from the lid to see the full 8x12 sheet that I have in the machine.



In that image above, there's about another 1.5" that's out of view. It's literally resting horizontally on the front rail, and the lid is holding it (it won't close all the way, phone is too tall.)


---
**HalfNormal** *October 12, 2016 12:49*

I use an old web cam I picked up for a $1.00


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/QWoz3Av8sDQ) &mdash; content and formatting may not be reliable*
