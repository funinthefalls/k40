---
layout: post
title: "Where are people getting really thin, beautiful wood for cutting?"
date: February 28, 2016 19:39
category: "Materials and settings"
author: "Stuart Rubin"
---
Where are people getting really thin, beautiful wood for cutting? Nothing structural or functional, but for things like "do not disturb" door hanger signs, cut artwork, etc., what are you using? I had no luck at a lumber distributor nor Home Depot. Thanks.





**"Stuart Rubin"**

---
---
**3D Laser** *February 28, 2016 20:42*

I got a box of 40 sheets of birch ply for 50 dollars it looks pretty good if painted or stained but it is ply not a hardwood so I don't know if that is what you need


---
**Bee 3D Gifts** *February 28, 2016 21:23*

I know Michaels Craft Stores have 12 x 12 thin birch sheets we use for some signage.


---
**Jim Hatch** *February 28, 2016 21:39*

Check to see if you have a Woodcraft store nearby. They have a lot of exotic woods available.


---
**Joe Keneally** *February 28, 2016 21:53*

Look for a local cabinet shop and talk to them about their scraps!!


---
**Stuart Rubin** *February 29, 2016 00:24*

**+Bee 3D Gifts** I'll have to check Michaels. How thin is the wood? I'm looking for something that can be thin enough for jewelry. BTW, I looked at your website and like what I saw! Congrats!


---
**Thor Johnson** *February 29, 2016 23:09*

**+Corey Budwine** That sounds perfect... I like my wood stained.  Where from? 


---
**3D Laser** *March 01, 2016 01:06*

**+Thor Johnson**  here is the link [http://www.ebay.com/itm/1-8-3mm-x-12-x-12-Baltic-Birch-Plywood-for-Laser-CNC-and-Scroll-Saw-40-pc/351662631467?_trksid=p2047675.c100011.m1850&_trkparms=aid%3D333008%26algo%3DRIC.MBE%26ao%3D1%26asc%3D35626%26meid%3D9d9a9caeae15459193ec291588446954%26pid%3D100011%26rk%3D1%26rkt%3D10%26mehot%3Dpp%26sd%3D351557878310](http://www.ebay.com/itm/1-8-3mm-x-12-x-12-Baltic-Birch-Plywood-for-Laser-CNC-and-Scroll-Saw-40-pc/351662631467?_trksid=p2047675.c100011.m1850&_trkparms=aid%3D333008%26algo%3DRIC.MBE%26ao%3D1%26asc%3D35626%26meid%3D9d9a9caeae15459193ec291588446954%26pid%3D100011%26rk%3D1%26rkt%3D10%26mehot%3Dpp%26sd%3D351557878310)





they ship fast to 


---
**Bee 3D Gifts** *March 06, 2016 21:13*

**+Stuart Rubin** Thank you! for the complement on our site :)



The specs and dimensions of the Birch Ply is:

1/8 x 12 x 12 in

3.1 x 304 x 304 mm



its like a couple dollars a sheet. 


---
*Imported from [Google+](https://plus.google.com/104069580748008438325/posts/6RuM3PydNWE) &mdash; content and formatting may not be reliable*
