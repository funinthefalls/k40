---
layout: post
title: "My laser is struggling to cut 3mm acrylic"
date: August 22, 2015 23:43
category: "Hardware and Laser settings"
author: "quillford"
---
My laser is struggling to cut 3mm acrylic. I did two passes at 10mm/s with 70% power. It is running marlin. Does anyone have any suggestions? I was able to cut 2mm acrylic at 100% power (unsure of the feedrate).





**"quillford"**

---
---
**ThantiK** *August 23, 2015 00:15*

Sounds like a focus problem to me.  The beam has a focal point and anything past that focal point, the laser is going to lose power.  Normally you'd set your beam to be the thinnest possible on the surface, but in this case you'll want to set it about 1.5mm lower than normal so that the focal point is half way through your material.


---
**quillford** *August 23, 2015 00:50*

I have the acrylic flat against the platform. It is 5cm from the lens. It won't go any lower.


---
**ThantiK** *August 23, 2015 01:08*

Oops, 1.5mm closer, not further


---
**quillford** *August 23, 2015 01:32*

Any recommendations for settings?


---
**Steve Moraski** *August 23, 2015 02:45*

I usually run 5 mm fo


---
**Steve Moraski** *August 23, 2015 02:46*

r the speed at 80% power and about 35psi air assist. 


---
**Jon Bruno** *August 23, 2015 02:52*

Thats a lot of air pressure Steve.. I'm not so sure you need that much ..

it's not a plasma cutter.. lol


---
**quillford** *August 23, 2015 03:01*

My current air assist currently consists of an aquarium pump that provides air in bursts... not sure it will be particularly helpful. 


---
**Jon Bruno** *August 23, 2015 03:04*

Quilford, That's the standard type sent out with the larger machines. I just switched from my GAST pump to a large aquarium unit like they send out to see if it would suffice. It's definitely a lot quieter... I haven't done any heavy tests yet but it seems ok with the light cuts I've made with paper and cardboard. no flare up's or anything...


---
**Jon Bruno** *August 23, 2015 03:06*

As for your trouble.. Dirty or misaligned mirrors are the primary suspects next to focal distance... The basic 3, That's what I call em... even though it doesn't make much sense.


---
**Steve Moraski** *August 23, 2015 04:48*

Jon, what is a good psi to run at?  That happened to be what my regulator was set at from whatever it came off of.  It worked so i didn't mess with it.  


---
**Jon Bruno** *August 23, 2015 13:34*

Its no so much about pressure. You certainly don't want enough to blow the workpiece around.

If you have a nice mellow column of air hitting the kerf that's plenty.

It's helping to prevent flareups and clear the cut of debris.

If you take a drinking straw and blow through it, that's pretty much equivalent to all you need to get the job done.


---
**Flash Laser** *August 24, 2015 00:50*

Hi, I suggest you out out 75% power,0.2m/min, to have a try cut 3mm Acrylic.


---
**quillford** *August 29, 2015 07:14*

I left the paper mask on top of the acrylic. Could that be an issue?


---
*Imported from [Google+](https://plus.google.com/115654175984463400645/posts/66YuJYCzgjN) &mdash; content and formatting may not be reliable*
