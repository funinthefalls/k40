---
layout: post
title: "Could you please support and advice which laser safety glasses I should by for the K40?"
date: March 10, 2017 22:48
category: "Discussion"
author: "Bernd Peck"
---
Could you please support and advice which laser safety glasses I should by for the K40? Thanks!!





**"Bernd Peck"**

---
---
**Steven Whitecoff** *March 10, 2017 23:12*

My research said Honeywell LOTG-CO2 provided the recommended protection for regular use. For laser alignment where you want direct strike protection I'd look for something else. I like them especially since they are US made and have part numbers and ratings engraved right on the lense- makes it easier to spot counterfits. IRC, Amazon @$40 shipped


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2017 23:16*

10600nm wavelength. Not sure if I recall correct, but I think OD7+.


---
**Steven Whitecoff** *March 11, 2017 02:58*

Yes, thats the Honeywell rating. OD>9 @ 190-360nm

OD>5 @ 9000-11100nm

OD>7 @ 10600nm 


---
**Anthony Bolgar** *March 11, 2017 03:01*

As acrylic absorbs the 10600nm wavelength, (that is why we can cut it) all you need is a standard pair of acrylic safety glasses. It takes about 30 seconds to burn through the glasses at a distance of 18" (The beam is very unfocused and spread by then). If you are dumb enough to sit there and wait for the beam to burn through and burn your eye, you deserve to go blind. But you probably won't be blinded permanently as the 10600nm beam affects the lens of your eye, and not the rods and cones at the back (which a 455nm diode laser will damage). If you do get a dead on unprotected strike of your eyeball, it will damage the lens, but a surgeon can replace that for you (they do it all the time in cataract surgery). Why you would get right up close and personal with the beam and turn it on is beyond me, but that is the only realistic way you can damage your eye with a CO2 laser at 10600nm. So in a nutshell, buy a pair of $5.00 acrylic safety glasses.


---
**Bernd Peck** *March 11, 2017 08:21*

Thanks for  all your answers. I had the feeling I need some glasses because if I stand at the cutter to supervise the cutting   process the light emission from the cutting point is significant.

Maybe I just dont look at the cutting the whole to avoid the light :)


---
**Anthony Bolgar** *March 11, 2017 09:46*

I have installed a web cam in Redsail LE400 to watch the process of cutting from.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2017 10:46*

I also had a webcam in mine for watching cutting (although not in currently) & another in the tube compartment to check for arcs (again not in currently). Now I just watch through the window in short bursts, from a distance.


---
**Mark Brown** *March 11, 2017 13:56*

The way I understand it the light coming off the cutting point is worse for your eyes than the laser itself


---
**Steven Whitecoff** *March 12, 2017 01:49*

two people I know who make their living with modest power lasers both made it simple...what are your eyes worth? Both know people with eye damage from lasers, both said the same thing- you cant have too much protection and if you do it long enough, you will at least get a real scare as it too easy to get distracted from being safe and when you least expect it ZAP! At 66 I still have all my fingers and toes despite a life time of exposure to things that munch digits, I'm determine to go to the grave with them AND my eye sight.


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/1Rgw3Bj8feM) &mdash; content and formatting may not be reliable*
