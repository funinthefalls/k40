---
layout: post
title: "what wooden stuff are people in Australia laser cutting and where do you get supplies from?"
date: September 07, 2016 07:28
category: "Discussion"
author: "Phillip Conroy"
---
what wooden stuff are people in Australia laser cutting and where do you get supplies from? as i have only ever cut 3mm mdf  





**"Phillip Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 07, 2016 10:20*

Bunnings, 3mm plywood. Also got some pine 18mm to test engraving on, 19mm merbau hardwood planks. All from Bunnings or Masters.


---
**Phillip Conroy** *September 08, 2016 05:29*

Found some nice redish coloured 3mm ply from miter 10


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 08:24*

**+Phillip Conroy** I've seen Bunnings & Masters have something similar out in the trade wood section. I believe it is a hardwood ply sheet.


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/ciYuNJECgmv) &mdash; content and formatting may not be reliable*
