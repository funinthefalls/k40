---
layout: post
title: "Greetings, I purchased a K40 laser which is assembled and functions"
date: June 01, 2016 06:08
category: "Discussion"
author: "COREY EIB"
---
Greetings, I purchased a K40 laser which is assembled and functions. Now of course I realize I have no skills on using the thing and was hoping to be able to pick up ideas for 'get started' type projects to build skills with the use of the machine. 



I'll be poking around here, trying to learn as much as I can. If anyone has any hot tips for a newbie, such as simple project ideas, tutorials, tasks that build confidence and skills with the machine and the software, I would be very grateful. Thanks



Corey

Waving a hand from Los Angeles, California





**"COREY EIB"**

---
---
**Phillip Conroy** *June 01, 2016 06:56*

On my page i have some good tips [https://plus.google.com/u/0/113759618124062050643/posts?cfem=1](https://plus.google.com/u/0/113759618124062050643/posts?cfem=1)


---
**Diva Patterson** *June 01, 2016 10:28*

Hello corey, I am same!!  Look forward to seeing what you produce. Thanks for the link Philip :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 01, 2016 12:48*

I would suggest taking small pieces of whatever materials you intend to be making things out of & creating some reference "sheets". I generally take a sheet & perform cuts/engraves of 10mm squares at various power levels & speeds to give myself a reference for future as to what is decent. Good way to get started with playing with the machine.


---
**COREY EIB** *June 02, 2016 06:16*

+Yuusuf Sallahuddin (Y.S. Creations) Brilliant Idea Yuusuf, thank you!


---
**COREY EIB** *June 02, 2016 06:16*

**+Yuusuf Sallahuddin**


---
*Imported from [Google+](https://plus.google.com/+COREYEIB1969/posts/cgNzpuwTWkz) &mdash; content and formatting may not be reliable*
