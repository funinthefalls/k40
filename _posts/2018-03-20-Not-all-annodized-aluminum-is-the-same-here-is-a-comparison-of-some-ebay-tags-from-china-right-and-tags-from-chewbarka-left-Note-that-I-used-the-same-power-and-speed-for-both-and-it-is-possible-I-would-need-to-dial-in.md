---
layout: post
title: "Not all annodized aluminum is the same, here is a comparison of some ebay tags from china (right), and tags from chewbarka (left) Note that I used the same power and speed for both, and it is possible I would need to dial in"
date: March 20, 2018 11:08
category: "Material suppliers"
author: "Printin Addiction"
---
Not all annodized aluminum is the same, here is a comparison of some ebay tags from china (right), and tags from chewbarka (left)



Note that I used the same power and speed for both, and it is possible I would need to dial in the settings for the ebay tags to get better results.



The ebay tags were $6.00 usd / 20, and chewbarka tags are $1.00 each, though the ones I purchased were in an bulk assortment of $25/pound. The chewbarka tags were also thicker, and had a smoother texture to them.





![images/ff472252026ab15140bfc8152af068c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff472252026ab15140bfc8152af068c9.jpeg)



**"Printin Addiction"**

---
---
**Ashley M. Kirchner [Norym]** *March 21, 2018 02:10*

Can I ask you what settings you used? Power, speed ... ? The last time I tried Chewbarka tags, they actually came out like the ones on the right. Didn't get anywhere near the white that I'm seeing on the left ones.


---
**Printin Addiction** *March 21, 2018 02:46*

42-44 % power and 90-110mm/sec raster 15-20 mm/sec vector, using K40 whisperer.


---
**Ashley M. Kirchner [Norym]** *March 21, 2018 05:05*

Cool, I'll have to go try it again. Thanks for the info.


---
**Printin Addiction** *March 21, 2018 11:00*

**+Ashley M. Kirchner [Norym]** I also tried molly lube (green can) at 46%, but I dont like to run the machine at that high level for long.





![images/beea95df336dbdc394cffa8be4150f41.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/beea95df336dbdc394cffa8be4150f41.jpeg)


---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/LiHPUwL5MBG) &mdash; content and formatting may not be reliable*
