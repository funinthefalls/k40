---
layout: post
title: "I think I've got a machine with a duff tube or power supply"
date: June 03, 2016 05:26
category: "Discussion"
author: "Ian Ferguson"
---
I think I've got a machine with a duff tube or power supply. Just arcs all the time at anode connection and I note that ammeter not registering any current flow. Also pot is turned up halfway before arcing starts. Any thoughts anybody? Very disappointed with this but knew I was taking a gamble. XY movement and software works fine however. I don't want to fork out for new tube until I know the tube is kaput so to speak. Any way of testing?





**"Ian Ferguson"**

---
---
**Phillip Conroy** *June 03, 2016 06:26*

If it is arcing at least it has hi voltage coming out of the power [supply.is](http://supply.is) the connection at the tube well covered with silcon ,if yes loosen tube and rotate so that the connection is as far away from metal as possible,remove keep tube lid up when testing,

Contact seller for help


---
**Derek Schuetz** *June 03, 2016 07:25*

It's the tube...same thing happened to me just finished installing new tube and I'm working. Contact the seller and ask for reimbursement for new tube and power supply due to warranty. I got $240 back for my $340 machine


---
**Ian Ferguson** *June 03, 2016 16:00*

Thanks for help will contact seller


---
**Ian Ferguson** *June 03, 2016 19:38*

Don't know where to get reliable tube from if indeed it is kaput. Thing that's weird is that the ammeter isn't registering any current at all. I guess that's because the tube is defective.


---
**Derek Schuetz** *June 03, 2016 19:40*

Light object tubes are suppose to be good but with shipping they are 210 I got another off eBay from some China seller with. Warehouse In CA and the water outlet came chipped but its currently working


---
*Imported from [Google+](https://plus.google.com/100713211036434462043/posts/WyoUA2wdD72) &mdash; content and formatting may not be reliable*
