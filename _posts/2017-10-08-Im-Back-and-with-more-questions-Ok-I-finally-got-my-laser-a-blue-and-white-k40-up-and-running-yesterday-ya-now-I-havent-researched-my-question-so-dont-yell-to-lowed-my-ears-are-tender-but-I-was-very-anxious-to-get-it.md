---
layout: post
title: "I'm Back and with more questions. Ok I finally got my laser a blue and white k40 up and running yesterday ya, now I haven't researched my question so don't yell to lowed my ears are tender but, I was very anxious to get it"
date: October 08, 2017 22:57
category: "Discussion"
author: "Padilla's custom leather"
---
I'm Back and with more questions.  Ok I finally got my laser a blue and white k40 up and running yesterday ya, now I haven't researched my question so don't yell to lowed my ears are tender but, I was very anxious to get it going, while was waiting to have the nozzle holder 3d printed I checked the carriage for square to the housing and it is. So when I got the head all mounted up we proceeded to check all mirrors for alignment and everything is in check, so I commenced to try and burn a few images on card stock and was a left a little bewildered.  As the laser progressed through the image it seemed to decrease in power form the initial start up of the image, and I repeated the process a few times.  I then went out today to try and burn the logo that I created using the OEM software on to a piece of leather, and the same thing occurred.  Now for a little more on my set up, I knew from the beginning that I would be making improvements and one of the things I did was add the L.O. air nozzle, the blue one along with a harbor freight air compressor, one of the small one's used for air brushing, now this compressor is not regulated down in air pressure.  So correct me if I am wrong but I cant understand how air pressure would have any affect on the power of the cutting nozzle so correct me if wrong.  so lets start with this, I know some if at least one will hammer me hard so bring it. 





**"Padilla's custom leather"**

---
---
**Joe Alexander** *October 09, 2017 01:04*

first thing I would check is the water temperature. it should be between 15-20°C to rum optimally, and not over 30°C. I find I lose intensity as it climbs. the next would be to check beam alignment in the 4 corners(put a piece of tape on the laser head as it enters it and test fire, the dots should stack). other than that I would then check the laser tube or Laser PSU for any issues.


---
**Printin Addiction** *October 09, 2017 14:36*

Two things, the LO head sets the lense at a different height so your focal point (higher) may be different than with the stock head.



The other thing and this had me pulling my hair out....

The LO head has a smaller diameter shaft than the hole in the mounting plate. I loosened the head mounting nut,  moved the head around in different positions and tested the laser (making sure the hole is parallel and facing the second mirror) until I got a consistent burn, then tightened the head up. 



On my setup there were huge differences in beam intensity, based on where the LO head was positioned in the mount.



If you look in the picture I was able to move it around a few mm in each direction, I think a bit to the right and back was the sweet spot for me.

![images/3976707127d30b7f88dfcc3f160f3d2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3976707127d30b7f88dfcc3f160f3d2a.jpeg)


---
**Padilla's custom leather** *October 09, 2017 17:36*

Thanks for the input, I had not sat down and looked at the operating software much till after the post yesterday, my bad.  What I did deduce from studying the software might actually be a normal operating situation for engraving, after all I am not trying to cut but engrave so I would think that depending on what is being done the power isn't going to be sky high.  Today I plan on going out and do some cutting and see how the machine cuts and responds to various power settings, it is all a learning curve I guess with a very rudimentary system and depends on the operators experience and I have none!!


---
**Printin Addiction** *October 09, 2017 18:10*

Are you using the software that came with the system (that requires the usb dongle). I am using K-40 whisperer exclusively, and only used the supplied software for the initial test only to determine if the laser worked at all.  






---
**Padilla's custom leather** *October 09, 2017 21:53*

yes I am using the software that came with the system one of my next steps was to ask what difference other software such as k40 whisperer would make if any on cutting and engraving? 


---
**Printin Addiction** *October 10, 2017 01:12*

I can speak to the difference as I have not used the original software other than to test the laser one time. K40 whisperer seems very east to use, the only issue I have seen so far is trying to use files made for cutting that dont have the red/blue/grey color scheme.


---
**Padilla's custom leather** *October 11, 2017 19:45*

does switching to whisperer eliminate the need for the dongle?  Or does still require a need for the dongle regardless of the software used.  The only problem that I would have at this time is not being efficient with other software programs to create the images that I wish to create!  I am not sure at this point what type of learning curve it is going to present for me.


---
**Printin Addiction** *October 11, 2017 20:55*

**+Padilla's custom leather** 

You don’t need the dongle with whisperer. I can write up a quick tutorial for using it.I found it pretty easy to use.


---
**Padilla's custom leather** *October 11, 2017 23:59*

I am torn right now having looked at whisperer and what Mr De Groot is producing as I am kind of like all left footed when it comes to some software, I was shown what the machine that is coming on the market can do by taking a photo and then reproducing it with the laser.  after seeing that I was sort of left with a feeling of oh ineptitude if know kind of what I mean hard to esplain.


---
*Imported from [Google+](https://plus.google.com/106971026516976154588/posts/8WDBwkgj89e) &mdash; content and formatting may not be reliable*
