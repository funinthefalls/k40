---
layout: post
title: "Does the supplied CD with corel draw and install work on an apple mac?"
date: January 05, 2017 15:52
category: "Software"
author: "Gail Lingard"
---
Does the supplied CD with corel draw and install work on an apple mac?



Can I use illustrator? 









**"Gail Lingard"**

---
---
**Ariel Yahni (UniKpty)** *January 05, 2017 15:57*

Non of the stock software will work on a mac directly unless you use a virtual machine like parallel desktop or upgrade your stock board and use LaserWeb


---
**Gail Lingard** *January 05, 2017 16:02*

Thank you Ariel. Is there download software and drivers which will make it work or do I need a PC?


---
**Ariel Yahni (UniKpty)** *January 05, 2017 16:06*

If you don't change the stock board you will not be able to natively run on a mac


---
**Gail Lingard** *January 05, 2017 16:08*

I am going to google 'stock board' :) Thank you for you help. 


---
**Ariel Yahni (UniKpty)** *January 05, 2017 16:17*

You can find here where to download ColerDraw and CorelLaser to use on windows Link to Corel draw and corellaser [https://plus.google.com/116150448511980225555/posts/27ny6QGE3Bg](https://plus.google.com/116150448511980225555/posts/27ny6QGE3Bg)


---
**Gail Lingard** *January 05, 2017 17:37*

I now have a pc laptop. Is the disk supplied any use at all? I am just unzipping the rar files. 


---
**Gail Lingard** *January 05, 2017 17:41*

The file has unzipped. Corel Laser has installed. Corel Draw is asking for a serial code? Any ideas?




---
**Ariel Yahni (UniKpty)** *January 05, 2017 17:43*

There should be a code generator inside the zipped file.


---
**Gail Lingard** *January 05, 2017 17:45*

I found the code in a file on the disk. Install is going ahead at the moment. 


---
**Gail Lingard** *January 05, 2017 18:32*

Coral draw did not install properly. Only one percent to go and it froze. 




---
**Ariel Yahni (UniKpty)** *January 05, 2017 18:43*

Wow that's sad


---
**Gail Lingard** *January 05, 2017 19:11*

I know. :( Trying it again. Not the easiest thing to set up. 




---
**Gail Lingard** *January 05, 2017 19:12*

Ok. Coral Draw seems to have installed. Whats next?




---
**Ariel Yahni (UniKpty)** *January 05, 2017 19:33*

CorelLaser, this is the pluging that will run inside corel draw and control the machine


---
**Gail Lingard** *January 05, 2017 19:35*

done.


---
**Gail Lingard** *January 05, 2017 19:37*

i have drawn a lovely circle. red 255. 0.1 and it seem to have made a mark on a piece of wood. well at leat thats a start. Laser alignment time

?


---
**Ariel Yahni (UniKpty)** *January 05, 2017 19:40*

That would be the most important part of all. Good laser aligment. For reference you should at least be cutting 3mm ply at 10mm/s with 10mA of power


---
**Gail Lingard** *January 05, 2017 19:44*

great ok.


---
**Ned Hill** *January 05, 2017 19:46*

**+Gail Lingard** did you set the board ID in device initialization? 


---
**Bill Keeter** *January 05, 2017 20:17*

FYI, you can use Adobe Illustrator files with CorelDraw. You just have to make sure to save them in the Illustrator8 version. 


---
**Gail Lingard** *January 06, 2017 18:12*

**+Ned Hill** Hi Ned, Can you explain what you mean by 'board ID'?. 




---
**Gail Lingard** *January 06, 2017 18:12*

Thanks **+Bill Keeter** thats a great tip. 


---
**Ned Hill** *January 06, 2017 18:59*

**+Gail Lingard**​​ in Corel laser under device initialization your need to set the board type (should be M2nano but will be labeled on your board) and you need to put in the board ID which is a unique number for your board which will be on your board as well (white sticker).


---
**Ned Hill** *January 06, 2017 19:32*

Highlighted is the mainboard type (If your board is labeled as a M2Nano select the M2 board as shown).  Also highlighted is the Device ID which is the number that will be shown on the board on a white sticker.

![images/c72fb72b1b2bba13b871b62ad906c905.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c72fb72b1b2bba13b871b62ad906c905.png)


---
**Gail Lingard** *January 09, 2017 15:03*

Thanks **+Ned Hill** I found the label on the M2 Nano and have input the details in coral draw 12 settings. 

The program is communicating with the machine. The laser is moving but nothing is cutting/ ENGRAVING. I have used laser machines before where the colour 255/0/0 RGB IS THE CUT LINE AT 0.1 but cant see where you would make these settings. The laser is not aligned yet so this may be the issue. I just thought I would ask incase there is something else i am doing wrong? thank you for your help. 




---
**Ned Hill** *January 09, 2017 15:33*

**+Gail Lingard** CorelLaser isn't very sophisticated so cut and engrave operations are separate runs and it doesn't use color for cutlines.  Set the corellaser settings as seen in the pic.  In coreldraw select the elements to engrave and then open the corellaser engrave  panel and set speed and start.  For cutting again select your cut element and go into the corellaser cut panel and set speed.  Note for cutting, the line widths need to be set to 0.01mm or less otherwise it will cut both sides of the line.  As to the laser you will definitely need to check the alignment if the tube is firing and nothing is reaching the bed.

![images/c628cc39d85b0c0a8e420d4986f58e4c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c628cc39d85b0c0a8e420d4986f58e4c.png)


---
**Gail Lingard** *January 09, 2017 16:28*

I had to add some washers to the first mirror base to get the laser to fire in the centre. The second mirror I managed to get the laser to fire in the centre by removing one of the screws on the base plate and moving the mirror far left. The third mirror the laser does not reach at all. (so no chance of any cutting) Confused and wasting lots of time. :( Is there anyone running a business setting up these machines? 


---
**Ned Hill** *January 09, 2017 16:32*

Where is the laser hitting for the 3rd mirror?

Sorry no one doing a business setting these up, one of the downsides to these cheap chinese laser cutters. 


---
**Gail Lingard** *January 09, 2017 16:34*

Its not hitting it at all. Thank you for your help


---
**Ned Hill** *January 09, 2017 16:35*

put a big sheet of paper in front of the mirror to see where the beam is going if it's that far off.


---
**Gail Lingard** *January 09, 2017 16:39*

That worked. I can now see its firing about 5cm right  of the mirror. I take it although my second mirror is firing in the centre its not right? 




---
**Gail Lingard** *January 09, 2017 16:54*

this is what I have done to get the mirror in the centre. 

[drive.google.com - File_000.jpeg - Google Drive](https://drive.google.com/open?id=0BzNubLxU_-XWX0JTRUU4U2xwaDA)


---
**Gail Lingard** *January 09, 2017 16:56*

This is where it is firing for the third mirror. 

[drive.google.com - File_000.jpeg - Google Drive](https://drive.google.com/open?id=0BzNubLxU_-XWNURpZmpNYmNXV2c)


---
**Ned Hill** *January 09, 2017 17:35*

The fact that you are having to remove a screw and pivot the 2nd mirror tells me there is something screwy with the first mirror.  Can you post a pic of the first mirror?


---
**Gail Lingard** *January 09, 2017 17:47*

this is the first mirror





[drive.google.com - File_000.jpeg - Google Drive](https://drive.google.com/open?id=0BzNubLxU_-XWZUhlWFQ3eTFrVlE)


---
**Gail Lingard** *January 09, 2017 17:49*

i have raised the plate with 4 washers under the two screws. 

[drive.google.com - File_002.jpeg - Google Drive](https://drive.google.com/open?id=0BzNubLxU_-XWM00yMkJwVWJBclE)


---
**Ned Hill** *January 09, 2017 18:05*

You probably need to turn the screws on the first mirror in so you have room for adjustment to hit the second mirror.  I know that will throw the alignment on the first mirror out but it will be better to adjust it by drilling out the slots on the mounting some to get it aligned if needed.


---
**Ned Hill** *January 09, 2017 18:11*

Here's what my first mirror looks like.

![images/428b684dfcaaca1b8706c4abf66bed76.png](https://gitlab.com/funinthefalls/k40/raw/master/images/428b684dfcaaca1b8706c4abf66bed76.png)


---
**Gail Lingard** *January 10, 2017 12:30*

**+Ned Hill** got there in the end. Mirrors aligned and cutting! :) excellent stuff. 




---
*Imported from [Google+](https://plus.google.com/105833930009921938226/posts/N5w6BXs4EGN) &mdash; content and formatting may not be reliable*
