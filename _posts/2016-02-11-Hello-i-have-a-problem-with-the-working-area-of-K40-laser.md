---
layout: post
title: "Hello, i have a problem with the working area of K40 laser"
date: February 11, 2016 11:05
category: "Discussion"
author: "Jan Mor\u00e1vek"
---
Hello,  i have a problem with the working area of K40 laser. After a 1 year of use, functional area decreased to about 1/4.



Where is problem, can be bad mirrors, or tube?



Thanks, Jan

![images/880185c194dcfa191a74532441a6c8a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/880185c194dcfa191a74532441a6c8a6.jpeg)



**"Jan Mor\u00e1vek"**

---
---
**Sebastian Szafran** *February 11, 2016 11:29*

You should check mirrors alignment, probably need some tuning. I think laser tube should be fine, as you are able to get enough power on the green area. Further the distance is more deviated the beam will be caused by even slightly misaligned mirrors. 


---
**I Laser** *February 11, 2016 11:29*

Have you aligned the mirrors? Cleaned them and the lens?


---
**3D Laser** *February 11, 2016 13:24*

Mine where like that I pushed the rails all the way to the far end to align and notice that the laster was hitting the lens holder not the hole for the mirror.  Once you get it dead center you shouldn't have a problem 


---
**Joe Spanier** *February 11, 2016 13:37*

Yea that's alignment. You're mirrors could be degrading too but that's alignment


---
*Imported from [Google+](https://plus.google.com/100687838134590423514/posts/icH4nC53nRJ) &mdash; content and formatting may not be reliable*
