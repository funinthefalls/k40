---
layout: post
title: "I am about to embark on the K40 to Smoothie upgrade"
date: October 07, 2016 19:51
category: "Smoothieboard Modification"
author: "K"
---
I am about to embark on the K40 to Smoothie upgrade. At the same time I've rebuilt the bed size inside the case to get me approx. 270x570mm space. I purchased this power supply from Light Object because my original K40 supply flamed up and died. The way I'm understanding the connections on this supply lead me to believe that I need to get a secondary 24v switching power supply for the Azteeg X5 I've got coming. Is that a correct assumption?





**"K"**

---
---
**Alex Krause** *October 07, 2016 20:20*

The less strain you put on the psu the better. The 24v power is for the stepper motors. I believe **+Scott Marshall**​ and **+Don Kleinschnitz**​ would both agree that you will see more longevity with your psu if you use an auxiliary psu to drive the stepper motors.


---
**K** *October 07, 2016 20:26*

Thank you for your reply, Alex!


---
**Alex Krause** *October 07, 2016 20:43*

**+Don Kleinschnitz**​ might be interested in your dead power supply if you still have it 


---
**K** *October 07, 2016 20:44*

Indeed I do. If he wants it it's all his. 


---
**Don Kleinschnitz Jr.** *October 07, 2016 22:28*

**+Kim Stroman** Sweet, how do I get it, where do you live?


---
**K** *October 07, 2016 22:29*

**+Don Kleinschnitz** I live in East Texas. If you're not worried about speed I can just send it via the cheapest shipping method possible.


---
**Don Kleinschnitz Jr.** *October 07, 2016 22:30*

**+Alex Krause** I used an external 24 supply to power my smoothie cause I just don't trust a HV supply whose 24V powers my controller. I am using the 24V from the LPS for my lift table and the 5V for incidentals like the LED pointer.


---
**K** *October 07, 2016 22:53*

I ordered a 24V 5A power supply, figuring that the drivers should only pull 2A max, so it should be enough. 


---
**Steve Brown** *October 10, 2016 08:49*

How did you get that bed size? Move the PSUs etc to outside?


---
**K** *October 10, 2016 11:45*

**+Steve Brown**  Yep. And once things are wired up, my fist job will be to cut a control box with the new bed size. 


---
**Don Kleinschnitz Jr.** *October 10, 2016 11:53*

**+Kim Stroman** Whoops I just noticed the post ref. sending the PS :(.

Contact me on don_kleinschnitz@hotmail.com for an address please, don't care about how long it takes :).


---
**Don Kleinschnitz Jr.** *October 10, 2016 11:54*

BTW I just noticed the specs on the LPS DC capacity: 24V @1 amp, 5V @1 amp. Not much to drive steppers!


---
**Damian Trejtowicz** *October 12, 2016 19:26*

im using separate psu for smoothie and drives, laser psu is only for laser tube


---
**K** *October 15, 2016 03:25*

I didn't want to start a new post, so I'm replying to two of my posts with this: I noticed some interesting behavior tonight. My meter kept jumping, and the pot would inconsistently adjust the current. I zeroed out my Smoothie by mistake, and so I hit the reset button on my X5, and I noticed the meter jumped by 5mA when I did! I tried it twice and that happened. Is this electrical interference or something else?


---
**Don Kleinschnitz Jr.** *October 15, 2016 13:18*

When you say it kept jumping is this during a job or static. I doubt its noise unless you have a grounding problem. I would try unplugging the controls to  the LPS one at a time and do the reset and see what control is causing it. I have heard of others getting a control signal from the smoothie on reset.


---
**K** *October 15, 2016 13:49*

**+Don Kleinschnitz** The pot was being finicky when I was trying to set the current (it was being very sensitive, as in just barely turning the knob to move 5mA, and then it would sort of jump 1-2mA). I ran my job twice. The first time I stopped getting current in the middle of the job, and during that job it jumped from 10mA down to 5ish, then nothing. I turned everything off, checked my connections, then ran it again. 



Then the jumping happened when I ran it again, it would jump 2-3mA. I did notice that my laser red wire was sort of haphazardly laying on top of part of the controller, so I moved it and added a silicone tube over the entire length. Everything's kind of a mess right now since I've not yet built a box. As for grounding, I did feel a slight tingle at one point when the job was running. I double checked my grounding lug and it is tight. I've got that secondary grounding post running to a copper pipe under the sink. 



As for unplugging things, should I start with the things connected to the smoothie? I do have four grounds running into a separate terminal block that I then tied together and ran one line into the PSU because those tiny blocks don't have room for more than one or two wires. I wonder if my issue may be there?


---
**K** *October 15, 2016 13:54*

**+Don Kleinschnitz** also, to add to the above, I noticed in another post that it's recommended to tie the level shifter to the ground on the board, but not the ground on the PSU as well, which I have done. Maybe another issue?


---
**Don Kleinschnitz Jr.** *October 15, 2016 14:13*

what setup (wiring ) did you use for connecting the smoothie to the LPS?


---
**K** *October 15, 2016 14:36*

**+Don Kleinschnitz** Hopefully this isn't a double post, but I can't see my other reply. I have this set up based on the blue box guide on the smoothie website. I have a different PSU though so I had to make some modifications. 

IN on PSU (LightObject says this is their PWM) to PWM on shifter

PWM on shifter to 2.4 on board

5v on PSU to 5V on shifter

3.3v on shifter to 3.3 v on board

Ground to both sides of shifter

Light Object did tell me that their 5V is analog and that the shifter is digital, so I don't need it, but then they cautioned me that it could fry my board, so I kept the shifter. 


---
**K** *October 15, 2016 16:27*

So I took the ground from the PSU to the shifter, and turned on the machine. The pot now works properly. I haven't run a job yet, but I feel like that's a good sign, since it was being really wonky before. The question now is, is it even possible that the ground to the shifter was causing issues?


---
**Don Kleinschnitz Jr.** *October 15, 2016 17:26*

**+Kim Stroman** according to **+Alex Krause** the level shifter cannot be grounded (although I do not understand why :)). 

What is H and L of the power supply connected to?


---
**K** *October 15, 2016 17:58*

The L is to the PWM. I had originally said it was the IN, but I now remember that I couldn't get PWM to work on that pin, despite LightObject saying that was the PWM pin. H is not connected to anything. I cannot get the laser to fire without grounding both sides of the shifter. Also, I spoke to soon. It's being weird again. The pot is again being squirmy and when I hold the test fire for a second the meter will jump up then back down to where it was set.


---
**Don Kleinschnitz Jr.** *October 15, 2016 18:05*

**+Kim Stroman** how is the test fire connected? Try disconnecting the level shifter totally and then see if the pot and test fire works better?


---
**K** *October 15, 2016 18:10*

The test fire is going to L & Ground, the Laser Arm button is going from WP to G (that's also where I'll put the interlock). I disconnected the shifter totally from the PSU, and I still get wonky behavior. At this point I'm wondering if either the pot or the PSU is messed up. 


---
**Don Kleinschnitz Jr.** *October 15, 2016 18:25*

**+Kim Stroman** well the smoothie/level shifter is out of the circuit so it can't be that? 

Do you have the right pot?


---
**K** *October 15, 2016 18:31*

I had to buy a different pot from the stock 10 turn 1K 2W pot. Here's what I bought: [amazon.com - 2Pcs B10K 10K ohm Single Joint Linear Adjustable Ratory Potentiometers: Amazon.com: Industrial & Scientific](https://www.amazon.com/gp/product/B0087ZBS92/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1), and here is what they recommended: Power control VR is 5K ~ 10K 1/2Watt at this link: [http://www.lightobject.info/viewtopic.php?f=16&t=18](http://www.lightobject.info/viewtopic.php?f=16&t=18)


---
**Don Kleinschnitz Jr.** *October 15, 2016 20:10*

**+Kim Stroman** LO uses 5K (looked at the PS manual)  but according the specs 10K should work. Crappy or worn pots can be noisy. You can take it out and connect it to a meter and watch to see if the resistance is erratic when you turn it.... it may or may not show you if its noisy. 


---
**K** *October 15, 2016 20:36*

It does seem flaky. When I measure it as I;m turning the knob, especially when it gets to the top end it will jump down then back up. I thought I had another one around here somewhere, but I can't find it, so I just ordered a 5K, 1/2W pot. I guess I didn't understand the specs. I thought 5K~10K meant a pot in that range.


---
**K** *October 15, 2016 20:40*

+Don Kleinschnitz  That must have been a bad pot. I found my other 10K pot and it goes up to 10k, whereas the one I have on the machine only measures up to 3.9ish. Question on the better one, as I keep my multimeter probes on the connectors and I spin the knob it occasionally fails to read when there's a biggish jump from one number to the next. Is that normal?


---
**K** *October 15, 2016 20:44*

Well, disregard. I had my machine totally powered down when I tested the old pot, but the pot was still plugged in. When I completely remove it from the PSU, I get the same results as the spare pot. It goes to 10 K, but the readout blanks from time to time as I spin the knob.


---
**Don Kleinschnitz Jr.** *October 15, 2016 22:07*

**+Kim Stroman** sometimes poor quality digital meters will pause and read open when the reading is changing.  However it sounds like a poor quality pot to me. 

Do both pots act the same?

It wouldn't hurt to get a better quality one that is a 10 turn wire wound pot. Its not obvious to me what else could be wrong. Also make sure that all connections to the supply are tight.


---
**K** *October 15, 2016 22:10*

**+Don Kleinschnitz** Both pots do sound the same. I'll take a look for a better pot. Thank you for all your help!


---
**K** *October 15, 2016 22:14*

**+Don Kleinschnitz** one more question... when the pot was attached to the powered off PSU why was the upper limit 3.9k, and what would make it jumpy? When it's disconnected completely it doesn't jump up and down and it goes to 10k. 


---
**Don Kleinschnitz Jr.** *October 15, 2016 22:14*

if that does not work see you back here :0.


---
**Don Kleinschnitz Jr.** *October 15, 2016 22:19*

**+Kim Stroman** I cant tell you because I do not know what the pot is connected to inside the ps, however if there is capacitance in the circuit it can give strange readings.

This is an example of why I want to understand what is inside these supplies:). You probably also should be on the LO forum and tell them of the trouble you are having.


---
**K** *October 15, 2016 23:11*

How important is the wattage? I'm having a devil of a time finding a 5k 10 turn wirewound pot that's also 1/2w.


---
**Don Kleinschnitz Jr.** *October 16, 2016 13:06*

I just looked at my K40 and it has a 1K 2w. BTW it reads half that when its in the circuit.

I don't think it has to be 2W (which would be .4A) because according to what I have found so far the pot controls an internal PWM generator in the supply, but again I really do not know exactly what it is hooked to.

This one got moderate reviews (problems with shaft size?).

[https://www.amazon.com/3590S-2-103L-10-Turn-Rotary-Precision-Potentiometer/dp/B00FZLP4RI/ref=sr_1_1?s=industrial&ie=UTF8&qid=1476569028&sr=1-1&keywords=10+turn+potentiometer](https://www.amazon.com/3590S-2-103L-10-Turn-Rotary-Precision-Potentiometer/dp/B00FZLP4RI/ref=sr_1_1?s=industrial&ie=UTF8&qid=1476569028&sr=1-1&keywords=10+turn+potentiometer)

[amazon.com - Amazon.com: 3590S-2-103L 10K Ohm 10-Turn Rotary Wire Wound Precision Potentiometer: Industrial & Scientific](https://www.amazon.com/3590S-2-103L-10-Turn-Rotary-Precision-Potentiometer/dp/B00FZLP4RI/ref=sr_1_1?s=industrial&ie=UTF8&qid=1476569028&sr=1-1&keywords=10+turn+potentiometer)


---
**Don Kleinschnitz Jr.** *October 16, 2016 13:09*

Here is a 5K version.

[https://www.amazon.com/Uxcell-a14111100ux0695-10-Turn-Rotary-Potentiometer/dp/B00VG93UD8/ref=sr_1_2?s=industrial&ie=UTF8&qid=1476623206&sr=1-2&keywords=10+turn+potentiometer](https://www.amazon.com/Uxcell-a14111100ux0695-10-Turn-Rotary-Potentiometer/dp/B00VG93UD8/ref=sr_1_2?s=industrial&ie=UTF8&qid=1476623206&sr=1-2&keywords=10+turn+potentiometer)

[amazon.com - Uxcell a14111100ux0695 10-Turn Rotary Wire Wound Potentiometer, 5K Ohm 6 mm Round Shaft, Blue: Amazon.com: Industrial & Scientific](https://www.amazon.com/Uxcell-a14111100ux0695-10-Turn-Rotary-Potentiometer/dp/B00VG93UD8/ref=sr_1_2?s=industrial&ie=UTF8&qid=1476623206&sr=1-2&keywords=10+turn+potentiometer)


---
**Don Kleinschnitz Jr.** *October 16, 2016 13:13*

This one gives you a vernier dial as well and there is a 10K version.

[https://www.amazon.com/gp/product/B01KMAEWZ6/ref=pd_sbs_328_3?ie=UTF8&psc=1&refRID=HRFYE2KV4EY6WVKAB3E2](https://www.amazon.com/gp/product/B01KMAEWZ6/ref=pd_sbs_328_3?ie=UTF8&psc=1&refRID=HRFYE2KV4EY6WVKAB3E2)

[amazon.com - Farwind 5K Ohm 3590S-2-502L Adjustable Precision Multi-turn Potentiometer With 10 Turn Counting Dial Rotary Knob: Amazon.com: Industrial & Scientific](https://www.amazon.com/gp/product/B01KMAEWZ6/ref=pd_sbs_328_3?ie=UTF8&psc=1&refRID=HRFYE2KV4EY6WVKAB3E2)


---
**Don Kleinschnitz Jr.** *October 16, 2016 13:15*

This is a safe bet and cheaper :)

[http://www.lightobject.com/3pin-10K-ohm-potentiometer--P899.aspx](http://www.lightobject.com/3pin-10K-ohm-potentiometer--P899.aspx)

[lightobject.com - 3pin 10K ohm potentiometer](http://www.lightobject.com/3pin-10K-ohm-potentiometer--P899.aspx)


---
**Don Kleinschnitz Jr.** *October 16, 2016 13:27*

If you want to make sure of quality, I buy from here:



[http://www.digikey.com/product-detail/en/bourns-inc/3590S-1-103L/3590S-1-103L-ND/2534346](http://www.digikey.com/product-detail/en/bourns-inc/3590S-1-103L/3590S-1-103L-ND/2534346)



Note: the amazon items did not have power specified but they look just like the Bournes ones that look the same and are 2W.

There are lots to select from on Digikey but they cost about 2x.



I'm still a bit cautious that we have found the problem because although  noisy pots are endemic of cheap parts I have had  good success buying from Ucell on amazon. I am also surprised that both pots seem crappy.

[digikey.com - MPKES90B14](http://www.digikey.com/product-detail/en/bourns-inc/3590S-1-103L/3590S-1-103L-ND/2534346)


---
**K** *October 16, 2016 15:06*

**+Don Kleinschnitz** I'm impatient so I bought the 5K Bourns knockoff on Amazon. I, too, am suspicious that both pots would be bad and thus my issue. But I did leave the pot out of the circuit last night and I ran quick test square. There was no jumping. It made me remember why I wanted the pot back in though. The top limit mA was stuck at the last place I had it set with the pot, and I've no idea of how to rectify that without a pot. 


---
**K** *October 16, 2016 15:10*

**+Don Kleinschnitz** I received a message from LO (I'd sent one about the whining that the PSU was making), and they're telling me that there may be something wrong with the PSU and to not turn it on until they verify what pins go where (I had to email them how I had things wired up). Unfortunately their forum requires admin approval for new accounts, and still after a week they haven't turned mine active.


---
**Don Kleinschnitz Jr.** *October 16, 2016 15:37*

**+Kim Stroman** it took a while for my account to get established as well. Hang in there. Very well could be the supply is bad and that is why current readings are flaky, and nothing to do with the pot.


---
**K** *October 17, 2016 01:18*

**+Don Kleinschnitz** well LO just told me that they don't think the PSU is bad (I had emailed just an hour ago about the procedure for replacement if the PSU is bad), but that my board isn't compatible and they won't ship a replacement. I've got to send it back for a refund. They never addressed the issue with the pot when the board wasn't connected, so I don't know what to think now. I'm very disappointed in this. 


---
**Don Kleinschnitz Jr.** *October 17, 2016 01:27*

(: Lets see what a better pot does.


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/H4xiEcffUYL) &mdash; content and formatting may not be reliable*
