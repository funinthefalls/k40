---
layout: post
title: "Random question, specifically for those of you that have a metal, essentially \"store bought\", nozzle air attachment to your laser head"
date: June 29, 2016 16:48
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Random question, specifically for those of you that have a metal, essentially "store bought", nozzle air attachment to your laser head. Like the ones that LO sells, or eBay ... question; what's the aperture size at the nozzle end? How small/big is the exit hole? (and for those of you also on the 'Laser Engraving and Cutting' FB group, you only need to respond in one place.)





**"Ashley M. Kirchner [Norym]"**

---
---
**Ned Hill** *June 30, 2016 14:42*

My LO air assist head has a 5mm exit noozle diameter.


---
**Ashley M. Kirchner [Norym]** *June 30, 2016 16:41*

Thanks Ned. From the responses I got on FB, it seems 4-5mm is a standard.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/PXv3yPaX4TK) &mdash; content and formatting may not be reliable*
