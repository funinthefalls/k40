---
layout: post
title: "Did anyone tried engraving glass ? I have a few glass panels I want to try to engrave, but I would like to know what power and speed other use to not waste them"
date: July 11, 2016 17:31
category: "Hardware and Laser settings"
author: "Jean-Baptiste Passant"
---
Did anyone tried engraving glass ?



I have a few glass panels I want to try to engrave, but I would like to know what power and speed other use to not waste them.





**"Jean-Baptiste Passant"**

---
---
**Ariel Yahni (UniKpty)** *July 11, 2016 17:37*

Yes you can. I think I've done 300 to 400 mms with 30 power


---
**Derek Schuetz** *July 11, 2016 17:51*

Is tempered glass ok to engrave?


---
**Ariel Yahni (UniKpty)** *July 11, 2016 18:05*

I believe I have etched some tempered glasses


---
**Alex Krause** *July 12, 2016 02:16*

I have heard of people using a wet paper towel over glass if it is thin to help dissipate the heat and keep the glass from cracking 


---
**Bill Parker** *July 12, 2016 17:40*

If you have not etched glass before be careful as the glass particles go on your hand and if you rub your face it's not nice. I was told to put washing up liquid on the glass and then wash off after etching we did that and it made a much better etch. 


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/2hEJCnd51m5) &mdash; content and formatting may not be reliable*
