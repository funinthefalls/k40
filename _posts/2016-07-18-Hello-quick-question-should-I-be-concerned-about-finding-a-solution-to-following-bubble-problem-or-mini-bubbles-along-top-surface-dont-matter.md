---
layout: post
title: "Hello quick question, should I be concerned about finding a solution to following bubble problem or mini bubbles along top surface don't matter?"
date: July 18, 2016 12:43
category: "Hardware and Laser settings"
author: "Pippins McGee"
---
Hello quick question, should I be concerned about finding a solution to following bubble problem or mini bubbles along top surface don't matter? (even if there's a lot. Is it just a matter of getting a more powerful water pump?) 

Cheers

![images/cde7b8b58e003a11c89109c3255b9bff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cde7b8b58e003a11c89109c3255b9bff.jpeg)



**"Pippins McGee"**

---
---
**Pippins McGee** *July 18, 2016 12:57*

on second thought, i'll see if it clears after i change water out and add alcohol to water.. 

but any input welcome on whether it is a matter for concern or not


---
**Scott Marshall** *July 18, 2016 16:50*

Those look like dissolved gasses being driven out, or local heating from inadequate flow. 



The bubbles themselves aren't really a threat unless they get worse, but would seem to be a symptom of another problem.



If you''d been running a while and it was getting pretty hot, that would make sense, otherwise, make sure the inlet filter on your pump is clear and that it's pumping at it's best. 



My stock pump runs about 2 liters per minute (check and see how full you can fill a gallon jug in 1 minute with the return line, if it's less than 1/2 gal, find out why. I'd consider that about the minimum flow that's safe..



If everything's ok, they should go away in short order. The alcohol will reduce surface tension a lot, and will discourage bubbles from forming.



Hope it helps, Scott


---
**Roberto Fernandez** *July 18, 2016 17:50*

Is it posible to changue the water by etilglicol?. I think that the boiling point of the alcohol can produce bubbles in the water if the water is warm.




---
**Alex Krause** *July 18, 2016 18:07*

One drop of dawn dish soap in the reservior and stir well it will break surface tension of the water but won't make any bubbles 


---
**Alex Krause** *July 18, 2016 18:07*

One drop of dawn dish soap in the reservior and stir well it will break surface tension of the water but won't make any bubbles 


---
**Pippins McGee** *July 19, 2016 03:56*

**+Scott Marshall** I'll test the flow of my pump - it is definitely flowing, it is the pump that came with the machine, its nothing to write home about but I'll check its flow-rate tonight. to ensure its not blocked or breaking down.

will report back flush out out water and swap with new distilled water with the alcohol.



the temperature I don't allow to exceed 24C. Once it reach 24C I add a closed bottle of ice to the tank - reducing it to 21C whilst it cuts.



**+Alex Krause** A drop of dish soap you say!.. makes sense.. where did you get that idea.. If the alcohol alone doesn't improve things I may just try that.. one drop. hard to imagine one drop making a difference but I can imagine more than a drop causing a foamy bubbly mess.



Cheers all will see how I go


---
**Alex Krause** *July 19, 2016 04:07*

**+Pippins McGee**​ I watch the other laser forums as well they say 1 drop per gallon or 2 of water... soap is a surfactant it makes water wetter 


---
**Scott Marshall** *July 19, 2016 08:29*

I agree with Alex, the soap won't hurt anything.



Most any of the common chemicals you add to a laser will reduce the surface tension. It gives the polar water molecules something to besides each other to link to, and  "breaks the chain" so to speak. Small amounts of ethylene glycol (anti-freeze, also in the alcohol family), any soap, and even most bacteriacides will help. My concern is I've not seen anybody with the fine bubbles you've got, and they look a lot like an overheated tube. 



I've never had mine do it, so I have to wonder what's different in your cooling system than mine. (and all the other basically stock systems out there). Maybe you DO have a contaminated system? Is there something in it besides water?



Like I said, verify the flow, temp, and you should know WHY you've got the bubbles forming.



When you know WHY, you'll know how to fix it, and if it warrants concern.



Scott


---
**Pippins McGee** *July 20, 2016 00:59*

**+Scott Marshall**  I tested the flow rate. 40 seconds to fill 1 litre.

so 1 minute would be.. 1 and a half litres? not too bad I guess?



I look very very closely at tube, and I 'think' I see contamination or some sort of organic growth- some sort of white/clear soft stringy substance stuck to the glass surface. very small. can barely see - even right up close. if that exists though at all, even miniaturely, the bubbles can attach to it though right?. May not have happened if I was running the ethanol in my water from the beginning.



Anywho, I dumped 1L of methylated spirits into my 14L full tub of demineralized water (I'll be using distilled water in the next swap)



The bubbles SEEM to be clearing away - and I think maybe the stringy organic growth inside tube MAY be dissolving slowly I'll check again tonight when I get home. It's just so small to see in the first place hard to tell.



Perhaps I should wrap the tubes in something (they are clear tubes that come with the machine) and not use a clear plastic tub? It is near a window I have heard things about sunlight being cause of bad water..



I could also just change the water out more frequently I suppose but 20L of distilled water cost.. $20 each time. $1 P/L at cheapest around here. so i'd rather just keep it clean somehow instead of having to change it often.


---
**Alex Krause** *July 20, 2016 01:46*

I use aquarium algaecide in my set up... it's more PH friendly than many other additives 


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/bG5oj5HThKV) &mdash; content and formatting may not be reliable*
