---
layout: post
title: "Was at the shops before grabbing a new charger for my laptop & I came across these little christmas trees that were adorned with these stars"
date: November 28, 2016 01:41
category: "Discussion"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Was at the shops before grabbing a new charger for my laptop & I came across these little christmas trees that were adorned with these stars. I am almost 100% certain these are just laser cut plywood. I bet the shopping mall paid a fair amount for these too.



Thought someone might be interested in having a go at designing/making something like this for their own tree.



![images/25b85e13ad65e784b20dfa46c34440c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/25b85e13ad65e784b20dfa46c34440c4.jpeg)
![images/041b684aa41c6e1cbbef29b46de9aa08.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/041b684aa41c6e1cbbef29b46de9aa08.jpeg)
![images/535647510a18113fe355d5a8601ef06c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/535647510a18113fe355d5a8601ef06c.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Gunnar Stefansson** *November 28, 2016 23:50*

Awesome... and yes most definetly laser cut or water jet, don't believe a cnc could have that pointy edge from the inner cuts...


---
**Paul de Groot** *November 29, 2016 04:40*

Wonder if they have a good supply of baltic birchwood here down under. I have not been able to get any in Sydney. Ebay charges an arm and a leg for postage from us and uk


---
**Gunnar Stefansson** *November 29, 2016 07:18*

**+Paul de Groot** wow, glad to hear I'm not the only one having that problem! and I'm in Denmark... I can't even get the Plywood which everyone else is using... I'm buying mine in Germany!


---
**Nigel Conroy** *November 30, 2016 16:48*

Yes Laser cut and hot glued together so not much effort put in to join them together


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/eGabvGMJKSe) &mdash; content and formatting may not be reliable*
