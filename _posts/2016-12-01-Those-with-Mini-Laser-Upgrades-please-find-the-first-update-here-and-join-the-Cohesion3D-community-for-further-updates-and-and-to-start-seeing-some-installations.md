---
layout: post
title: "Those with Mini Laser Upgrades please find the first update here and join the Cohesion3D community for further updates and and to start seeing some installations!"
date: December 01, 2016 06:17
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Those with #Cohesion3D Mini Laser Upgrades please find the first update here and join the Cohesion3D community for further updates and and to start seeing some installations!



<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



Update #1 on the boards.  All 100 ReMix have been visually inspected at this point.  We are currently working through testing each one manually.  First few have been good.  We flash the firmware from the MicroSD Card and then do a full functional test to see that all the motors spin, endstops trigger, thermistors read, and mosfets can run heaters. As well as the GLCD and all other fun stuff. 



The MicroSD Cards and A4988 Motor Drivers (for those who added them) have been shipped via DHL and we expect to have them soon. 

Should have more updates after the weekend. 



And as if that wasn't enough, just placed the first order of 100 Mini's with the assembler.  

We said 4-6 week lead time and we launched for sale on Thanksgiving, just about 1 week ago.

Here's a rough idea of how this is going to go:

1 week already passed.

Assembler said 2-3 weeks lead time. Now,  they are first going to assemble 2 production samples for me, perform a functional test on their end, and then ship those boards to me.  I will put one in my K40 and the other is going to **+Joe Spanier** to test in his K40, he has the ribbon cable type and I have the all JST-style connectors type. That will take a little longer while we wait for DHL to deliver the boards to me, I test and send one to Joe, he tests, etc...

Once we are happy with all functionality, I give the go ahead and the rest of the batch gets assembled.

Some time later we get the batch, package up the bundles, and start shipping.

In short, you'll get it, it'll work, and you'll get it as soon as possible. 





![images/ace5932e62792632329d3592c9950c0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ace5932e62792632329d3592c9950c0f.jpeg)
![images/94e8a46110e2f0727616dc0ab9605ed5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94e8a46110e2f0727616dc0ab9605ed5.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Jonathan Davis (Leo Lion)** *December 01, 2016 06:25*

Yeah I still need to put together that build guide for installing it on my type of system.  


---
**Stephane Buisson** *December 01, 2016 07:31*

Thank you **+Ray Kholodovsky** for the update. I think I will take my 4XC out of my K40 (for my OX) and replace it with your kit. do you have any photo of GLCD adapter (installed) ? 

I can do some test for you too. 

(my config is that one [http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide))

Cheers

[smoothieware.org - SmoothK40 Guide - Smoothie Project](http://smoothieware.org/blue-box-guide)


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 07:37*

This is an older one. The new one will go off to the left more so that the Ethernet expansion header is clear. ![images/920ff2379ad30935d8e65332023b9757.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/920ff2379ad30935d8e65332023b9757.jpeg)


---
**John Sturgess** *December 01, 2016 12:33*

I'm looking at upgrading my K40, can I add a third driver to the 3d mini board and control the Z table height through laserweb? 


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 17:22*

**+John Sturgess** sure thing, and even can do a 4th driver for a rotary. 


---
**Coherent** *December 01, 2016 19:18*

Do you have any specific GLCD or source to buy one that you recommend?


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 19:21*

**+Coherent** It is the 12864 RepRapDiscount Graphic LCD.  We don't carry it at the moment.  It is around $10 on aliexpress (I recommend a seller S+S+S+) and in the $15-20 range on Amazon.


---
**Coherent** *December 01, 2016 19:57*

Thanks, just ordered the  GLCD from amazon and the Laser Bundle from you. Looking forward getting it... sooner the better!! : )




---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 20:07*

Cohesion, Coherent.  We're going to get along great!


---
**timne0** *December 23, 2016 20:54*

Ray, I wired in a RAMPS board which involved splitting out the original ribbon.  Is there an easy way to wire it into the new board seeing as it's now individual dupoint connectors?


---
**Ray Kholodovsky (Cohesion3D)** *December 23, 2016 21:06*

**+timne0** sure thing! All the motors and endstops are individually broken out on the mini and remix so you'd be just fine. 


---
**timne0** *December 23, 2016 21:08*

Ok just got a notification the software has been fixed so I'll give ramps another go but failing that I'll place an order in Jan! :)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/PQLmbaQwi1r) &mdash; content and formatting may not be reliable*
