---
layout: post
title: "Saw this on a Facebook K40 user group"
date: August 05, 2018 06:54
category: "Discussion"
author: "James Rivera"
---
Saw this on a Facebook K40 user group. The idea is to continuously sterilize the water so you don’t have to keep replacing it.





**"James Rivera"**

---
---
**Stephane Buisson** *August 05, 2018 08:38*

At the end we will ask ourselves about the efficency of our K40. If you add compressor, fan, pump, lamps on top, we could reach X25 the util laser power.  maybe a little rethink ... of power efficiency.

I say that with in mind, the ultra low cost and hobby style machine the K40 actually is.


---
**Thomas Selbstversuch (Fantasio)** *August 05, 2018 08:49*

The k40 is a basic part. You can mod it to a better laser. 


---
**Don Kleinschnitz Jr.** *August 05, 2018 12:10*

**+Stephane Buisson** not sure I fully grasp your point? :)


---
**Don Kleinschnitz Jr.** *August 05, 2018 12:13*

This assumes that the water conductivity increase is caused by the growth of algae?

**+Ned Hill** is that what you think causes the raise in conductivity? 

If so this might be worth a try.

**+James Rivera** were there any tests of this method cited?


---
**Stephane Buisson** *August 05, 2018 12:25*

**+Don Kleinschnitz** my point is why do we need 1000 W to cut 3mm of plywood ? all devices add up


---
**HalfNormal** *August 05, 2018 14:16*

**+Stephane Buisson**  I think you're confusing the UV light for a laser tube.


---
**HalfNormal** *August 05, 2018 14:21*

They make a set up for ponds that I was considering, but run anywhere from $80 to over $100. I can swap out my water cheaper than that.


---
**Stephane Buisson** *August 05, 2018 14:28*

**+HalfNormal** **+Don Kleinschnitz**

k40 -> 60 w

PC  -> 350 w

air assist Compressor  -> 200 w

mini fridge  -> 100 w

fan exhaust  -> 60 w

inside light  -> 10 w

UV light  -> 7 w

....



don't take every values for it, just the idea. make your own sum.

cooling and computing is the area to get better efficiency.

changing the water by hand would also help.




---
**Ned Hill** *August 05, 2018 14:29*

I hear you **+Stephane Buisson**.  I generally fall into the "more power" mod mind frame, but there is a point to consider the cost of "more power" vs just changing the bucket of water :)



**+Don Kleinschnitz**  As I understand it, algae growth does a couple of things that would affect conductivity.  One, algae consume CO2 and as a result more atmospheric CO2 is pulled into the water and some gets converted to carbonic acid which lowers the pH (more ions).  Second, when some of the algae dies some of the cell contents add to the ionic concentration.  

So yes, adding a UV sterilizer could reduce the need to change the water as much.  I still think you would need to change the water at some point as you will build up ions from things the UV light kills.


---
**Ned Hill** *August 05, 2018 14:55*

I am somewhat skeptical of how well these in tank UV sterilizers, like show above, really work.  I was an aquarium hobbyist and worked in a pet store in college.  The UV sterilizers I dealt with were all flow through designs this used quartz tubes to expose the UV light to the water.  Normal glass will absorb a lot of the higher energy UV photons where as quartz is transparent to a much larger range of UV wavelengths.  So unless those units above have quartz casings, doubt full at the price, they probably aren't that effective.


---
**HalfNormal** *August 05, 2018 15:14*

**+Stephane Buisson**  I apologize, I did not realize you were referring to the total power consumption of all equipment together.


---
**Don Kleinschnitz Jr.** *August 05, 2018 16:42*

**+Stephane Buisson** we should compare the K40 power usage to other power tools & devices that cut and mark. 



..Like a circular saw 1200W

..Table saw 1800W



Neither of the above have the automation and accuracy of the K40 which cost some watts :):)





and....



How many watts did they use to distill the water, so the longer before you discard it the less watts was expended over the usage time. 



...... however I get your point 🙂






---
**Stephane Buisson** *August 07, 2018 07:38*

**+Don Kleinschnitz** 

if you use 1kw of power for 40 w laser power -> your ratio is 25/1 (approximation of course, in fact far more because all other device are still on when not lasering).

circular/table saw do a far more intense job (compare 3mm cut) and are powered just the time needed for the job. I don't think you can compare.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/j9qAJCsrgic) &mdash; content and formatting may not be reliable*
