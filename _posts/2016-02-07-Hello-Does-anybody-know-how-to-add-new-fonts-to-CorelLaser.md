---
layout: post
title: "Hello. Does anybody know how to add new fonts to CorelLaser?"
date: February 07, 2016 13:06
category: "Software"
author: "Patryk Hebel"
---
Hello. Does anybody know how to add new fonts to CorelLaser?





**"Patryk Hebel"**

---
---
**I Laser** *February 08, 2016 03:16*

Just add them to Windows, if they don't work in corel try rebooting your machine. If they still don't work I usually try converting their format using an online conversion service. I've found this one works okay [https://everythingfonts.com/woff-to-ttf](https://everythingfonts.com/woff-to-ttf)


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/jbJBTp9nBd8) &mdash; content and formatting may not be reliable*
