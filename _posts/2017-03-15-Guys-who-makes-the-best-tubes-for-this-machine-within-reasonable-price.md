---
layout: post
title: "Guys who makes the best tubes for this machine within reasonable price?"
date: March 15, 2017 03:01
category: "Modification"
author: "Chris Hurley"
---
Guys who makes the best tubes for this machine within reasonable price? Or is there a big difference? Also when you replace the tube should you solder it or do the glue thing?  





**"Chris Hurley"**

---
---
**Paul de Groot** *March 15, 2017 03:29*

I found reci on ebay delivering a very good tube. It was $280 AUD inclusive of postage and arrived within 4 days. It was well packaged in foam so no broken tube on delivery. You can't solder the wires on the metal post. I spliced the copper wire and twisted both ends together with the metal post between the spliced ends. Ideally you need a proper high voltage one wire connector. I reused the silicon tube cap from the factory and filled it with the supplied silicone. Worked well and waited for 24hrs to ensure the silicone dried before firing up the laser again.


---
**Paul de Groot** *March 15, 2017 03:29*

![images/6c5a8fcc5c5d7ba7b3712c1176eda6e8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c5a8fcc5c5d7ba7b3712c1176eda6e8.jpeg)


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/8qTQUProGLg) &mdash; content and formatting may not be reliable*
