---
layout: post
title: "Hey guys, Question: I'm getting a wavy engraving while just doing a simple text engraving for testing"
date: April 18, 2017 17:24
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hey guys,

Question:

I'm getting a wavy engraving while just doing a simple text engraving for testing. Does anyone have any ideas what could be causing it?

1) I've tightened the belt....could it be anything else?



2) How tight is the belt supposed to be? Real tight...little loose...or what? 



Thanks in advance 

![images/5738baaf46bbad382eac599a77f18d5d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5738baaf46bbad382eac599a77f18d5d.jpeg)



**"Nathan Thomas"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 17:37*

Is your material held firmly in place? Sometimes vibrations can cause the material to shift a little.



Also, in regards to the belt, too tight & mine wouldn't allow the head to move. It would lock up. Too loose & it would skip steps leaving large gaps or shifting the engrave significantly (like 10mm+).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 17:38*

Oh, I think I've also read that the lens could be loose causes issues like this too. Check that it isn't loose. Check mirrors are not loose too.


---
**Nathan Thomas** *April 18, 2017 18:13*

Thanks for the help 😊



The material isn't held firm. So I'll try to tape it down and see if that makes a difference. 



What would a loose mirror look like though? They don't appear to be loose...but is there something that would give it away? What do you look for to determine that?


---
**Nathan Thomas** *April 18, 2017 18:15*

It's weird because this is the only material that I tested and had this result. My cuts on plastic and leather are perfect....straight lines. And so far my engraving on plastic is good also. So I'm baffled by this...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 18:18*

For the loose mirrors, I was meaning the outer ring that holds the mirror in place. It could be loose, but usually just finger tightens well. Also, could be that the alignment adjustment bolts are not tight (you need to make sure the little nuts are locking in place, usually tight up against the back of the mirror mount blocks). I recently had my laser vibrating quite a lot after spending days fixing my alignment only to have my alignment all messed up again because I forgot to tighten those nuts properly. The alignment bolts/nuts probably isn't your issue though, but doesn't hurt to check it too.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 18:20*

Maybe this material is inconsistent in its makeup & some areas are burning more than others? Alternatively the material's top face may not be perfectly even causing slight variations in the focal point (which would cause differences in beam spot size). This difference could equate to the edges looking wobbly like this.


---
**Paul de Groot** *April 18, 2017 23:31*

**+Yuusuf Sallahuddin** yes I have seen this and when you apply tape over it then it works well because you take away the reflection 


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/NgKNyTirF8Y) &mdash; content and formatting may not be reliable*
