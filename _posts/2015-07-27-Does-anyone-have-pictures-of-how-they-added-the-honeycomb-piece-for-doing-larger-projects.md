---
layout: post
title: "Does anyone have pictures of how they added the honeycomb piece for doing larger projects?"
date: July 27, 2015 15:38
category: "Hardware and Laser settings"
author: "Bruce D (Cam & Dad)"
---
Does anyone have pictures of how they added the honeycomb piece for doing larger projects?

![images/fa0eb3e41601be9a248f3cb86e135bcd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa0eb3e41601be9a248f3cb86e135bcd.jpeg)



**"Bruce D (Cam & Dad)"**

---
---
**Jim Coogan** *July 27, 2015 16:30*

I have the K40 Chinese laser so I ripped out the inside bed they have and added wood supports to lift up the honeycomb surface to the right height.  I am in the process of making a scissor jack table for the inside that will make that adjustment a lot easier.  But you can't really use that with the original cutting bed that was installed in the K40.  There's no room.


---
**Bruce D (Cam & Dad)** *July 27, 2015 16:35*

Yeah I need to be able to cut kydex if possible for a job but don't see how


---
**Fadi Kahhaleh** *July 27, 2015 22:08*

in the process of making my own adjustable z-height table... so far got the honeycomb surface.. waiting on aluminum U-trims and time to start testing the design 


---
**Troy Baverstock** *July 28, 2015 04:41*

Is anyone obtaining the honeycomb in Australia? And what price are people paying for it?


---
**Jim Coogan** *July 28, 2015 05:24*

You can find it on Ebay but the same honeycomb is typically used in front of an automobiles radiator.  If you can find an old one you can strip it off and cut it to size or get multiples if you need more.


---
*Imported from [Google+](https://plus.google.com/104630767939271001198/posts/7qV32m3CewN) &mdash; content and formatting may not be reliable*
