---
layout: post
title: "I know some people here have engraved on anodized aluminum"
date: March 26, 2018 01:28
category: "Materials and settings"
author: "James Rivera"
---
I know some people here have engraved on anodized aluminum. I'm wondering if it is possible to engrave on (flat surface) silver or gold jewelry, or are they too reflective?





**"James Rivera"**

---
---
**Joe Alexander** *March 26, 2018 01:40*

the wavelength of co2 can only affect coatings on metal, not etch the metal itself. So if its pure silver or gold probably not, if its plated then maybe.


---
**James Rivera** *March 26, 2018 01:55*

**+Joe Alexander** That's kind of what I was thinking. But I'm wondering if anybody has done it by putting something non-reflective on it (e.g. tape) and engraved it away along with some of the metal beneath it? Probably crazy talk, but I thought it was worth finding out if anyone has done it with a K40.


---
**Don Kleinschnitz Jr.** *March 26, 2018 01:57*

Hint: Really good infared mirrors are gold plated ....


---
**James Rivera** *March 26, 2018 02:00*

**+Don Kleinschnitz** Crap. Yeah, that's why I said this might be crazy talk...


---
**Justin Mitchell** *March 26, 2018 08:25*

Thats not how metal "engraving" works with a co2 laser, none of the metal is removed, its either burning off a coating (eg anodized aluminium) or its fusing molybdenum atoms to the metal (eg cermark)


---
**Chev Chelios** *March 28, 2018 21:14*

Cermark may be the answer here..black but basically permanent.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/LGLk7TqT33D) &mdash; content and formatting may not be reliable*
