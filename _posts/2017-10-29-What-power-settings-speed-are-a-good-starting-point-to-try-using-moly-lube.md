---
layout: post
title: "What power settings/speed are a good starting point to try using moly lube?"
date: October 29, 2017 08:15
category: "Materials and settings"
author: "Anthony Bolgar"
---
What power settings/speed are a good starting point to try using moly lube?





**"Anthony Bolgar"**

---
---
**Arion McCartney** *October 29, 2017 20:38*

I've gotten decent results at 13ma 15mm/s.  I did a test with different speed/power settings on 10 different engraved 5mm squares and that speed/power setting gave me the best results.

![images/4be2e8108009bad77210fc68a6454a7f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4be2e8108009bad77210fc68a6454a7f.jpeg)


---
**Anthony Bolgar** *October 29, 2017 20:50*

Thanks **+Arion McCartney**


---
**Ron Ginger** *October 29, 2017 23:16*

Please tell me more about this. Does the moly adhere to the work, something like power coating the  item? What base materials does this work on? Can you give me a brand name or link to the right moly product?



I have been trying to use my K40 to make brass name plates. i want to coat the brass with some thing, I tried spray paint with no luck, Then burn away the paint  in places, then etch the brass plate to relieve the surface. The I will paint the etched area and polish the brass surface.



My first attempt was just black spray paint- common hardware store stuff. The image looked good, but it did not clear off the paint, unless I scrubbed hard enough to take  off all the paint.



Thanks


---
**Steve Clark** *October 30, 2017 04:36*

Engraving Stainless Steel (Old post of mine)



Last week a machine shop asked me to engrave part numbers on some 300 small 304 Stainless Steel parts they were making for a customer. I had not tried to do this in stainless yet so I suggested doing a few sample parts and then if the customer liked them I would go ahead with the rest. They sign off on them and I ran the rest.

Unfortunately I can’t show you a picture of the part because it is a proprietary design… but I do have a picture of a test piece of material with some engraving on it. 



First get yourself some CRC dry Moly lubricant for 13 bucks with free shipping like this: 



[http://www.ebay.com/itm/282331750428?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/282331750428?_trksid=p2057872.m2749.l2649&ssPageName=STRK:MEBIDX:IT)

Or this stuff if you got lots of money 63 bucks plus shipping :



[cermarksales.com - LMM 14 Black for Metals - 6oz Aerosol Spray - CerMark Sales](https://www.cermarksales.com/product/lmm-14-black-for-metals-6oz-aerosol-spray/)



And a can of Acetone, Nitrile gloves, and a coffee can to wash the spray off after engraving. (Wash off in a well ventilated area)

Spray a light coating let dry and engrave.

The laser power settings were not high, I think I used about 12ma. And I was cycling about three a minute. I charged 35 cents each. Total time doing them about 2 hrs. and made just about a $100 profit as I only used about third of the can of Moly spray. Acetone, about a cups worth. I was able to wash three as it engraved three stacked side by side. 




---
**Steve Clark** *October 30, 2017 04:38*

Here is a picture of the test piece...

![images/93286e2e7452b9ccd3caef3947edd98f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/93286e2e7452b9ccd3caef3947edd98f.jpeg)


---
**Ron Ginger** *October 31, 2017 00:21*

Thanks, how permanent is this marking. Call it engraving but you're not actually cutting into the surface of the metal are you? It seems more like painting or powder coating to make it stick. Will this work for any metal I'm interested in brass




---
**Steve Clark** *October 31, 2017 00:47*

I don't know for sure but I was unable to scrap it off with a exacto blade. Looking at it under the microscope appears to actually bond with the stainless and is kind of like ceramic. The parts I made appear as indistructable as the stainless surface.


---
**Ron Ginger** *October 31, 2017 16:15*

Thanks, I have no luck at all I've tried a number of different setting I've been up to full power and as slow as 5 mm a second and I'm leaving almost no mark on the material. I did Mark some stainless steel but it washed right off in WD40 I'm trying aluminum now and I haven't had any luck with anything sticking to it.




---
**Arion McCartney** *October 31, 2017 16:25*

I'm not sure about any other material other than stainless steel. My engravings are permanently bonded to the metal. It won't scratch off.  I haven't heard of it working on any other material. I see that Cermark sells a product to engrave onto brass... LMM 14. I have no experience with it though. 


---
**Steve Clark** *October 31, 2017 16:31*

ummm? Beam focus point? I was coating 304 ss.  Try some cuts with a piece of material pitched upward on one side to see if the focal point is right. I'd also re check mirror aligment.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/gY2w99jym4E) &mdash; content and formatting may not be reliable*
