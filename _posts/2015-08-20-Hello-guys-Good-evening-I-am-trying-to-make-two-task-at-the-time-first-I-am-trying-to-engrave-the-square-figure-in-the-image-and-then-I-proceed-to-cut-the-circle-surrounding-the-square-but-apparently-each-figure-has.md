---
layout: post
title: "Hello guys, Good evening!. I am trying to make two task at the time, first I am trying to engrave the square figure in the image and then I proceed to cut the circle surrounding the square, but apparently each figure has"
date: August 20, 2015 00:23
category: "Software"
author: "Jose A. S"
---
Hello guys, 



Good evening!. I am trying to make two task at the time, first I am trying to engrave the square figure in the image and then I proceed to cut the circle surrounding the square, but apparently each figure has independent coordinate system. Could you advise me how I must proceed?. I am using corel draw. Thank you in advance! Regards



Jose



![images/52bcaedcb5aba6044b3134959a1f9e3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52bcaedcb5aba6044b3134959a1f9e3a.jpeg)
![images/468f6d6204edd1d3a6ac7d61e27b4824.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/468f6d6204edd1d3a6ac7d61e27b4824.jpeg)

**"Jose A. S"**

---
---
**Stephen Warren** *August 20, 2015 05:24*

I don't know which SW you use, but this trick might work: Add a tiny shape (e.g. tiny 0.01" circle or square) to the very top-left of the page area. Always include this when you cut/engrave. That way, there will always be something at the top/left of the design, so everything will be offset from that point. Otherwise, most LASER SW I've used will make the origin of the LASER movement equal to the top-left most location on the canvas where a shape actually exists to be cut/engraved, whereas what you want is for the origin of the LASER movement to always be the top-left of the canvas, irrespective of how close to that point you've put a shape. SW sucks:-(


---
**Gregory J Smith** *August 22, 2015 13:34*

I use the Coreldraw package and it's quite easy. Setup the software to cut of engrave only selected items. Add a small box or dot the top left hand corner. Select it and the objects you want to engrave, press engrave and do "add task". Then select the shapes you want to cut - include the little reference box, then add task again.  The nachine will engrave first, then ask to go to the next task, then cut. Also, make sure your graphic input is the same for cut and engrave (e.g. both PNG or WMF) otherwise you get small offset. Note if you use PNG then the shapes you want cut must be filled. Good luck!


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/cGTtupiNU9w) &mdash; content and formatting may not be reliable*
