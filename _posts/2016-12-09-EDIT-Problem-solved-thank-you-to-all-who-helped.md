---
layout: post
title: "(EDIT: Problem solved, thank you to all who helped"
date: December 09, 2016 02:43
category: "Hardware and Laser settings"
author: "Madyn3D CNC, LLC"
---
(EDIT: Problem solved, thank you to all who helped. This post hasgarnered some great information so if you are having problems with the laser and you cannot determine whether it's the PSU or the tube, then read through these comments and learn something new.)



In the middle of an engraving, the laser lost all of it's power but continued on it's route. No power at all and the amp meter shows nothing. There's no cracks in the tube, seems perfectly fine, but no visible beam at all. LED and fan is still working on the power supply, and there's no signs of blown cap's, resistors, etc on the PCB. Perhaps a MOSFET went bad? What's the chances of that happening?



I've got the power supply opened up now and I don't see anything unusual.  I'm not thrilled about playing around with the anode and cathode on the tube but I'm wondering if there's a bad connection. 



Anyhow, I can post up some more pics if needed. Does anyone know where I should start with this? I don't want to go purchasing a new tube or power supply just yet. How can I tell if the tube is bad aside from a visual inspection, because visually the tube seems fine and holding water where it should be. Worst time for this to happen, Holidays and all.... please help. 



![images/5d11315af80cc1b4bb1d6d6700f9488f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d11315af80cc1b4bb1d6d6700f9488f.jpeg)
![images/478b14af0078c8a523acf4e3b8112a62.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/478b14af0078c8a523acf4e3b8112a62.jpeg)
![images/085663de9a932a2091a8ab9b83893fab.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/085663de9a932a2091a8ab9b83893fab.jpeg)

**"Madyn3D CNC, LLC"**

---
---
**Joe Alexander** *December 09, 2016 07:43*

Did you try the test button on the Laser PSU to see if it is just a controller issue? Would be the first step I would take :)


---
**Madyn3D CNC, LLC** *December 09, 2016 19:27*

Hi Joe, thank you for the reply. After making this post, I went down to tinker some more and hitting the on-board "TEST" tactile switch was one of the tests I preformed.



 I was hoping it would only be a control switch issue, as that fix would be a piece of cake. But unfortunately the "TEST" switch on-board did not result in anything.



 Any other suggestions? Do you know what the red slide switch is on the side of the blue box power supply?



 I'm wondering if it's some sort of relay or breaker I might have to reset but I don't know what it is because the data sheets for that board are all in Chinese.. :(


---
**Andy Shilling** *December 09, 2016 20:53*

I would imagine the red switch is for 230v- 110v for respective countries. Have you metered all possible outputs from the psu?


---
**Stephane Buisson** *December 09, 2016 21:24*

some tips to help to find out faulty component :


{% include youtubePlayer.html id="dz4ln4_vT3g" %}
[youtube.com - laser tube and power problem test](https://www.youtube.com/watch?v=dz4ln4_vT3g)


{% include youtubePlayer.html id="pXRpQVV7pUE" %}
[https://www.youtube.com/watch?v=pXRpQVV7pUE](https://www.youtube.com/watch?v=pXRpQVV7pUE)



but do not advise those risky method


{% include youtubePlayer.html id="kGsjSRDB0Wk" %}
[https://www.youtube.com/watch?v=kGsjSRDB0Wk](https://www.youtube.com/watch?v=kGsjSRDB0Wk)


---
**Madyn3D CNC, LLC** *December 09, 2016 22:09*

**+Andy Shilling** yes that would make sense to me, as these lasers are distributed to many countries. As for metering the outputs, great call, perhaps I should try that first. My plan was to start testing resistors and caps, particularly the big green resistor on the ground return. I'm eyeballing those MOSFET relays too, but the only method I know of testing FET's is quite the process. I'll meter the outputs soon as I head back down in the shop in a few mins and report back. 



**+Stephane Buisson** I can see the preview on the last video which I recognize, but I'll have to check out the first 2 and see if I've already watched them because I must have watched at least 15 videos so far regarding power supplies, never mind the tube videos lol.. And yes, agreed, I always get a kick out of watching the Chinese assemble these things, swinging around 20,000 volts like it's just a wire to some headphones. Can only imagine the injury statistics in those factories! 


---
**Madyn3D CNC, LLC** *December 09, 2016 23:55*

I got some more photos of my specific model if it helps. Going to test all PSU outputs now...





[imgur.com - Imgur: The most awesome images on the Internet](http://imgur.com/Pimafqh)

[http://imgur.com/w7lePYx](http://imgur.com/w7lePYx)

[http://imgur.com/cnK74MV](http://imgur.com/cnK74MV)


---
**java lang** *December 10, 2016 20:36*

Hopefully I can help you rather than confusing...

First of all, the following steps are actions I personally would do to find the problem, so this is definitely not a recommendation, only to be clear and I hope you understand this.

Where is the main problem? You don't have a tool to measure HighVoltage, right? I come to this point later.

What is my experience as service technican?  at least 90% of all problems are of mechanical nature! 

So, first of all I would check the connection from the tube to the amp and to ground -> should be very near to zero ohms. The problem zones are the tube connection, both screws on the amperemeter, the amperemeter itself and the connection to the PSU. Doblechek this with an OHM-meter (disconnected and discharged of course).

Next is to see if HighVolage is present:

I the company I worked in, we had a HV-Probe. This is a long full isolated "stick" with an output to a multimeter. Maybe you can find an old TV-repairing shop near by you, they could have such a probe and lend it to you (old TVs have comparable voltage ranges on their tubes). If not, as I know, the isolation of air is about 21KV/cm, I would do the "risky" method but in a safely way. Everything unpowered and discharged I would mount a really small wire from ground to lets say 1/2cm or some mm to the HV-connection. Then going to the frontpanel, swich on and press Test, first short, then longer, until you hear or see the spark...or you hear nothing... in this case there is with some certainty NO High voltage. I would then power OFF, and shorten the HV-connection with the wire using a long isolation stick (i.e. Wood). Again, if there is no spark it manifests the previous assumtion.

At his point you should know, on which side the problem is: on the tube side or on the PSU-side.

Maybe it helps a little bit for your decisions...

PS: if the PSU is damaged (what I believe, reading your error report) you can quickchek the MOSFETS if they are shortened (ohmmeter). They should be High-Zero (PSU unplugged and discharged of course)!

[https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view](https://drive.google.com/file/d/0BxRBPjkKUvRUN1Q3OTlqNFhVQkJnYW5VTjRXUWduaEp5Wnd3/view)




---
**Madyn3D CNC, LLC** *December 10, 2016 21:51*

**+java lang** Thank you so much! No I do not have a HV probe but I am looking for other ways to check high voltage and see if it's there.



I am going to try your test now and report back. I also suspect the MOSFET's could be the issue as well. Either way, thank you. Do you know any other methods of checking for HV? My wife just offered to run to the store for packing peanuts so I can preform the "peanut test" I learned back in grade school. Styrofoam packing peanut on a thread! I plan on making a video of the test, she should be back shortly and I'm on it.


---
**Madyn3D CNC, LLC** *December 10, 2016 22:03*

Ammeter is coming back at 3.0Ω , Java, I think you just may have saved the day. Time to get a closer look at this ammeter. You it be wise to bypass/bridge the contacts on the ammeter just to see if the circuit will complete and fire the laser? That's what I'd like to do next if there's no harm in doing so to the laser or PSU


---
**java lang** *December 10, 2016 22:05*

Hmmm, wait, 3.0ohm could be normal...


---
**java lang** *December 10, 2016 22:14*

but shortening the ammeter cannot damage anything, so give it a try


---
**Madyn3D CNC, LLC** *December 10, 2016 22:17*

Yes I think 3.0Ω might be normal resistance for a ammeter like these ones. I just read some articles on testing ammeters, maybe it's not the problem.. :(


---
**java lang** *December 10, 2016 22:20*

Yes, I checked this on my laser, also 3 ohm


---
**java lang** *December 10, 2016 23:06*

Have you the PSU disassembled?

2 things are coming in mind:

1) If a MOSFET is damaged, I would think the fuse also would be blown.

But anyway, check if there is a short on the output of Q6 or Q5. 

2) Another posibillity is that the output transformer has simply "lost connection", i.e. internal a wiring has disconnected or  melted. Carefully discharged, you should measure a resistance from the "hot wire" to ground (if the hot wire connection is accessable).


---
**Madyn3D CNC, LLC** *December 11, 2016 02:30*

**+java lang** Yes, PSU is currently open. I am going to check the FET's resistance and also check the Q6/Q5 as you mentioned. When you say the fuse would be blown if the FET was bad, do you mean the in-line glass fuse on the board? Here is the current state of the laser as we speak...  [http://imgur.com/Q6x7Sya](http://imgur.com/Q6x7Sya)


---
**Madyn3D CNC, LLC** *December 11, 2016 02:34*

![images/9fe5b245836028de8a27af34f5378313.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9fe5b245836028de8a27af34f5378313.jpeg)


---
**Madyn3D CNC, LLC** *December 11, 2016 02:35*

![images/a9ccfcce9eb50f1351dfce8cd870209b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9ccfcce9eb50f1351dfce8cd870209b.jpeg)


---
**Madyn3D CNC, LLC** *December 11, 2016 02:36*

![images/b82155da0ec1e5874fa839989253134d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b82155da0ec1e5874fa839989253134d.jpeg)


---
**Madyn3D CNC, LLC** *December 11, 2016 02:41*

I just took the tube right out, filed off some corrosion on the annode/cathode prongs and cut new splices in the wire. At least I can eliminate bad connection that way. Also, I really am anxious to preform the high voltage test but I'm trying to take advantage of the machine being off for 48+ hrs and do what I have to do now, before I fire it back up and have the cap's and flyback all charged up again. I hate high voltage. 

![images/9aa8ddc8105bb6dc3e33b75dd0affcfe.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9aa8ddc8105bb6dc3e33b75dd0affcfe.jpeg)


---
**Madyn3D CNC, LLC** *December 11, 2016 02:59*

I'm wondering if maybe the diode mentioned in the diagram could be relevant? I circled where it mentions a possible blown diode with overload...  I found Q5/Q6 so disregard those ?'s, they look good and measure out. 

![images/25b8b37bb51cfeb5252c43604f51b6ee.png](https://gitlab.com/funinthefalls/k40/raw/master/images/25b8b37bb51cfeb5252c43604f51b6ee.png)


---
**Madyn3D CNC, LLC** *December 11, 2016 03:09*

Checking the resistance between the hot and ground? As in, the main output coming from the flyback to the laser? That infamous red wire? Check resistance from there to ground? I'm guessing set the ohm range on my DMM to 20M highest setting? 


---
**Don Kleinschnitz Jr.** *December 11, 2016 06:59*

Warning: this voltage is LETHAL.



Most often connection problems on the high side are easily noticed by arcing.



Letting the LPS arc to ground can damage it as this supply is not designed to operate without a load.



Most often failed culprits in order of frequency:

...Input rectifier bridge

...D70-80

...Q1-4



<s>---</s>

If D90 is bad you can tell by reading the 24vdc on the external connector which will be missing. 



Is the green LED on?


---
**Madyn3D CNC, LLC** *December 11, 2016 16:02*

Don, firstly thank you for that guidance, I will be closely inspecting the problem areas you mentioned and report back with hopefully some results.



 Also, I appreciate the warning about the high voltage. I've noticed quite a few K40 owners unaware what kind og voltage the flyback puts out. When the machine was initially shut down, I took precautions, remained aware of my earth, and elevated myself about 14" off the ground by sitting on a plastic milk crate when working close to the flyback. Now the machine has been off for 72+ hours and I'm feeling a little less anxious about a potential charge. 



I am now anxious to inspect the areas and components you mentioned, I'll be back with an update. Thanks again, your technical input is of high value. 



Also, one question on the side for future reference...  the lack of soldered anode/cathode connections on the factory laser tubes. Is there a reason most tubes are connected with just silicone adhesive? I would like to solder the connections back on the tube, is that not recommended for any specific reasons?


---
**Madyn3D CNC, LLC** *December 11, 2016 16:19*

Also Don, just to recap, yes the green LED is illuminated on the PSU when powered on. No damage, cracks, anything unusual on the tube. No power is getting to the tube. I suppose the next big revelation will come when I preform the high voltage "static" test, which will tell me definitively if the problem is in the tube or the PSU. I am taking advantage of the machine being off for 3 days and doing all that I need to do with the machine in it's current discharged state. Today I will be powering it back up once the tube is back on.


---
**Don Kleinschnitz Jr.** *December 11, 2016 17:04*

If you supply was working I am guessing you would have already seen arcing or the laser tube light up. I am guessing the supply is dead, not unusual for a K40.



The anode is not soldered at least because heating that pin may damage the tube. I am not even sure that pin is solder-able although I have read about folks doing it?  If the wire is properly wound on the pin and then insulated with that white gunk, the connection will be fine. The voltage is in excess of 20KV and connections do not have to be gas tight to work ok. If you were having any connection problems you would see arcs or corona given the supply was working. You can try turning out the lights and see if anything is glowing..... sorry but I doubt it :(.

Again, arching the supply to ground may damage it if it is still alive. 


---
**Madyn3D CNC, LLC** *December 12, 2016 15:09*

I want to thank everyone for your help.. I made the decision many months ago to completely quit facebook but one of the drawbacks to that was the info in all the CNC/engineering groups. On the other hand, I ended up finding these google groups that seem to harbor a particular group of makers and engineers whose intellect and experience far surpasses the intellect commonly found on facebook. After all, I do remember reading a study which found that generally, the more successful and happy an individual is, the least likely you are to find them on facebook. I thought about that for quite some time. 



Anyhow, I'm at the conclusion that the issue lies within the tube, and I'm going to purchase a new one today, probably from lightobject unless anyone shoots me a better recommendation on sourcing a 40W co2 tube.



We finally had the machine all put back together last night. We took advantage of this downtime to upgrade everything we could, right down to switching out all the janky, loose nuts and bolts with my own SS M4/M5's w/ washers and thread locker. 



Once the machine was powered back up, we went ahead and preformed the infamous "Peanut Test", in which a Styrofoam packing peanut is tied to a thin cotton thread and suspended in the air. I had the string taped to the kitchen table and carefully maneuvered the laser until the packing peanut was freely hanging in parallel with the HV+ connection to the laser tube. We powered up the laser and sure enough, the high voltage line attracted the peanut and "clung" to the HV wire. I adjusted the trim pot (current regulator) while holding the laser test switch, which resulted in the peanut "dancing" and swaying around, clearly within the field of HV static.



 So what this tells me is that the power supply is indeed putting out sufficient voltage all the way up to the laser tube, and even though there are absolutely NO visible signs of damage to the tube, there must be an issue somewhere within that tube that is not visually identifiable. 



What do you guys think of this peanut test? This is the first time I've actually done it since my grade school science class, except it was more exciting because it was actually a practical test that solved a problem. I think it's a pretty cool concept, and will maybe help a lot of new users to be able to identify whether the problem exists in the PSU or the CO2 tube. 



I took photos along the way and I'm going to do a write up on this whole situation to publish in our blog, in hopes to help others.  When I first got a K40, there was ZERO info out there in the internet. Now, there seems to be a decent amount of info but there's always room for more, and I think this peanut test will be significant and keep people from playing around with flybacks and lethal currents.


---
**Don Kleinschnitz Jr.** *December 12, 2016 15:51*

In regard to the peanut test. It may tell you that you have a good supply. Then again my reservation would be that it would tell you that there is some static charge but that does not mean that there is an appropriate amount of current or voltage being supplied by the supply. As you know peanuts can show you very small charges like when they cling to your clothes. 

I agree we definitely need some safe way to tell if its the tube or the supply when these failures occur without buying expensive test equipment. I have been noodling this for some time.


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/U1pax4sFSXS) &mdash; content and formatting may not be reliable*
