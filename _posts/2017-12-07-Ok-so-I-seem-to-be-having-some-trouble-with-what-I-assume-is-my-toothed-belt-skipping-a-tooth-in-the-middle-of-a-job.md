---
layout: post
title: "Ok so I seem to be having some trouble with what I assume is my toothed belt skipping a tooth in the middle of a job"
date: December 07, 2017 13:32
category: "Hardware and Laser settings"
author: "David Allen Frantz"
---
Ok so I seem to be having some trouble with what I assume is my toothed belt skipping a tooth in the middle of a job. Which of course throws the alignment of my side to side axis off by about 1/8”. Very frustrating. Haven’t had time to look at the belt. Does anyone know if there are replacement belts out there for sale that just drop in? Or has anyone else had this same problem?





**"David Allen Frantz"**

---
---
**Joe Alexander** *December 07, 2017 15:09*

there is a couple of screws you can tighten to adjust the belt on the right hand side of the gantry. You can access them if you line it up with the hole in the panel leading to the power supply. for y axis they are in the back and annoying to reach. Just make sure to not over-tighten them, and remember to also check the set screws(if any) on your motors! :)


---
**David Allen Frantz** *December 07, 2017 15:33*

**+Joe Alexander** Right on man… Thank you so much! I’ll try that as soon as I get home!


---
**Anthony Bolgar** *December 08, 2017 10:41*

If you end up needing a belt. Lightobject sells the proper replacements for the X and Y axis.


---
**David Allen Frantz** *December 08, 2017 11:15*

Great!


---
*Imported from [Google+](https://plus.google.com/+ProfessorExpress/posts/fYGrSD6JnvW) &mdash; content and formatting may not be reliable*
