---
layout: post
title: "Well I got a lot of: ...."
date: March 06, 2017 00:22
category: "Object produced with laser"
author: "Don Kleinschnitz Jr."
---
Well I got a lot of: 

.... "Don, do you ever do anything with your laser but tinker...

lately, even from my wife.



Here is proof that it can do something useful and this is the first time it has run for more than a few minutes.



What I like:

....The vacuum system works really well and its very quiet. My wife's cookies were not drowned out by any smells either. Was not running air assist.

....The power was good, only 5 ma, but I was running slow cause I had LW set wrong.

... The material was easy to place and get in focus with the power bed.

... Generally happy how easy this was to set up an run.



What I don't like:

....To much wiggle for the speed I was going. I haven't optimized anything however and haven't paid much attention to stepper accel/decel values. 

Don't know if the materials burn can cause that wiggle. I suspect not, because the left and right edges are out of sync together. Maybe the material is moving on the bed, that's possible as its sitting on loose pins? 

....The upper right may need optical adjustment or perhaps there is a defect in the wood there.

... I have recurring problems with LW3 not connecting. It takes a combination of smoothie resets and LW refreshes and then it connects. Once it connects it seems ok.



Any tweaking advice is appreciated .....












**Video content missing for image https://lh3.googleusercontent.com/--GEufDTlwYw/WLyrrhk0x0I/AAAAAAAAkfg/8r67V8wgEi00Rd2DykGAKaLuMMleL7Q0QCJoC/s0/20170305_162601.mp4**
![images/3b446647c83951ea82662bc03a405b7b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b446647c83951ea82662bc03a405b7b.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Ned Hill** *March 06, 2017 00:30*

I've seen people say that one of things that can cause this is a loose set screw on a belt pulley.


---
**Alex Krause** *March 06, 2017 00:59*

My pulleys are press fit on the stepper shaft


---
**Ned Hill** *March 06, 2017 01:07*

Yeah I just looked and mine are as well.  Loose belt then?


---
**Ned Hill** *March 06, 2017 01:23*

See this post for ideas [plus.google.com - My engravings keep giving me these squiggly lines on the edge. Any idea how t...](https://plus.google.com/102016608119814536184/posts/E79E6a8RV3a)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 06, 2017 03:12*

In regards to the Smoothie/LW connection issue it has been mentioned on various others with similar issues that the USB cable itself could be at fault. I can't recall specifics, but basically advice is to use a high quality shielded USB cable. Also, I have a feeling that I read the shorter it is the better (but maybe not?).



edit: Side note, it's great to see that you're getting to have a play around now :)


---
**Maker Cut** *March 06, 2017 22:39*

**+Don Kleinschnitz** are you engraving in both directions?  If so, that could be the source of your 'squiggles'. If I remember correctly there should be a setting for the machine parameters that allows you to adjust the start point when engraving bidirectionally to help reduce these. It all depends on the software you are using.


---
**Don Kleinschnitz Jr.** *March 07, 2017 00:40*

**+Maker Cut** yes engraving in both directions. What setting are you referring to and what software?


---
**Maker Cut** *March 07, 2017 00:59*

**+Don Kleinschnitz**​ I was going to ask what software you are using.


---
**Don Kleinschnitz Jr.** *March 07, 2017 01:05*

**+Maker Cut** laserWeb




---
**Alex Krause** *March 07, 2017 03:52*

Have you checked the x axis belt tension **+Don Kleinschnitz**​? The adjustmet for it is the two screws on the right side of the gantry and it functions by pulling the idler pulley further away from the steper motor pulley 


---
**Alex Krause** *March 07, 2017 03:56*

**+Don Kleinschnitz**​ I show how to access it in my video about removing the carriage to change the belt located here 
{% include youtubePlayer.html id="_NRnRwHtjrc" %}
[https://youtu.be/_NRnRwHtjrc](https://youtu.be/_NRnRwHtjrc)


---
**Ned Hill** *March 07, 2017 13:52*

Lol it wasn't until very recently that I realized that what I thought were 2 random holes in the back of the machine where actually access points points to adjust the y-axis drive belt tensioners.  


---
**Don Kleinschnitz Jr.** *March 07, 2017 13:54*

**+Alex Krause** thanks I remember that videos I will check the tension on x.


---
**Don Kleinschnitz Jr.** *March 07, 2017 14:48*

**+Ned Hill** yah, I plugged mine with an air fitting lol ....not knowing.


---
**HP Persson** *March 08, 2017 15:36*

Wow, you actually make stuff with your machine? :)

Mine has been in a mess since christmas, i just find more mods to do... "it will just take a day" :P


---
**Don Kleinschnitz Jr.** *March 08, 2017 15:39*

**+HP Persson** we must be kindred spirits. Seems I like hacking on it more than using it lol. I have always liked making the tools for something more than building the something!




---
**HP Persson** *March 08, 2017 15:43*

Yeah, i like to plan and do strange stuff more than having it fully working.



Like now... "almost" done.. hah

Cohesion3D conversion.

![images/8115ee12719442ce44a1ed61d7fa0dae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8115ee12719442ce44a1ed61d7fa0dae.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/LnK1AVbMFgk) &mdash; content and formatting may not be reliable*
