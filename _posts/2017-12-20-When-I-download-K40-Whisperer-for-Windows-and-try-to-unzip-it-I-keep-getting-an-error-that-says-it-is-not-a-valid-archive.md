---
layout: post
title: "When I download K40 Whisperer for Windows and try to unzip it, I keep getting an error that says it is not a valid archive"
date: December 20, 2017 20:25
category: "Software"
author: "Robert Curtis"
---
When I download K40 Whisperer for Windows and try to unzip it, I keep getting an error that says it is not a valid archive.  Any ideas?





**"Robert Curtis"**

---
---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 01:16*

32 or 64 bit? windows?


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 01:39*

you can try extracting with 7zip if you haven't already. i never liked the embedded zip utilities in Micro$oft Windoze.


---
**Scorch Works** *December 21, 2017 01:54*

Try downloading it again. Something may have gone wrong with the first download.


---
**Robert Curtis** *December 21, 2017 12:52*

Thanks.  I am doing the 64 bit version. The computers here at school have an old version of Winzip.  I'll try 7zip.


---
**Robert Curtis** *December 21, 2017 13:22*

So it seems that the problem is that it is not downloading the complete zip file.  I've tried a bunch of times and and the file being downloaded is only 1kb.  Could that be due to our school's firewall?  I have not seen this problem before when downloading files!


---
**Scorch Works** *December 21, 2017 14:03*

You could try a right clicking on the link and select "save link as" or a similar option depending on the browser you are using.  You could also try a different browser. (e.g. Chrome, Internet Explorer, Firefox, etc.)


---
**Robert Curtis** *December 21, 2017 17:15*

I tried that already.  Very strange.  The IT department is trying to figure it out




---
**Robert Curtis** *December 21, 2017 17:45*

Found out that our gateway is saying it is a Trojan, that's why it is blocking it.  Any ideas?


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 18:02*

try it from here. i uploaded it to my server. if that doesnt work you may have to download it from another location or network

[techbravo.net - techbravo.net/files/K40_Whisperer-0.15_win64.zip](http://techbravo.net/files/K40_Whisperer-0.15_win64.zip)


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 18:07*

as a last resort: try downloading this "png image". it IS the zip file but i renamed it to make it look like a picture. download it and change the name to "whisper.zip" and it should be recognized as a zip file again so you can extract it. if not you will have to go on another outside network to get it

[techbravo.net](http://techbravo.net/files/ext_chg/changemeback.png)


---
**Scorch Works** *December 21, 2017 18:23*

Some anti-virus programs don't like programs built using py2exe which is how the executable is made. 



1.  You can install Python (and the required libraries) and run it that way but that is much more work.  



2. I would guess your IT guy could get the executable downloaded if he was sure there isn't a virus but I am not sure how you go about convincing someone of that.



3. You can use LaserDRW.  It does work.  (It might be interesting getting that installed with your anti-virus software.)



4. Upgrade to a different controller board so you can use other software.


---
**Robert Curtis** *December 21, 2017 19:25*

**+Tech Bravo** Thanks.  That worked!


---
**Tech Bravo (Tech BravoTN)** *December 21, 2017 19:26*

**+Robert Curtis** you are very welcome. from my server or the rename?




---
**Robert Curtis** *December 22, 2017 19:17*

Sorry to ask for so much help, but I am doing the Windows install and the Zadig download is being blocked.  Would you be so kind as to post this as well. like you did with Whisperer?


---
**Robert Curtis** *December 27, 2017 16:46*

**+Tech Bravo** your server!


---
**Tech Bravo (Tech BravoTN)** *December 27, 2017 16:47*

Sorry just saw the issue you had with the driver. Do you still need it?


---
**Tech Bravo (Tech BravoTN)** *December 27, 2017 16:53*

here is the win7+ zadig driver zip: [techbravo.net - techbravo.net/k40_share/zadig-2.3.exe.zip](http://techbravo.net/k40_share/zadig-2.3.exe.zip)




---
**Robert Curtis** *December 27, 2017 19:56*

**+Tech Bravo** Thanks!


---
**Tech Bravo (Tech BravoTN)** *December 27, 2017 20:12*

You are welcome


---
*Imported from [Google+](https://plus.google.com/102533765013675564454/posts/VTMG4SX53RK) &mdash; content and formatting may not be reliable*
