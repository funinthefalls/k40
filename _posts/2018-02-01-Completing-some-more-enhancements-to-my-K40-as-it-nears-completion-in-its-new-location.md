---
layout: post
title: "Completing some more enhancements to my K40 as it nears completion in its new location"
date: February 01, 2018 14:10
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Completing some more enhancements to my K40 as it nears completion in its new location. Does a K40 conversion ever end?



Details in this post:



[http://donsthings.blogspot.com/2018/02/enhanced-k40-temperature-monitoring.html](http://donsthings.blogspot.com/2018/02/enhanced-k40-temperature-monitoring.html)







![images/8a1e10a095872789301704f63529a414.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a1e10a095872789301704f63529a414.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *February 01, 2018 14:35*

The modifications end when you get rid of it!


---
**Adrian Godwin** *February 01, 2018 14:42*

I'm sorry, you're suffering from tool-hoarding. 



It's terminal, but not necessarily life-threatening.



Pretty well all techies end up suffering from it : the first symptoms are buying a piece of tooling that needs fixing so you can repair something else. 



You soon discover that all your spare time is spent maintaining a circle of tools that are used to maintain the others.

 




---
**Paul de Groot** *February 02, 2018 00:49*

It's hard not too overinvest in the k40 but when the tube dies than you realize how much stuff  you added and sits idle.😂


---
**HP Persson** *February 04, 2018 12:12*

Can a upgrade and update of the K40 ever get done? :P


---
**Fly Oz** *February 16, 2018 03:57*

Buy a new tube approx $200? im told


---
**Doug LaRue** *May 29, 2018 03:37*

I'm starting to plan my Smoothieware based upgrade and as a 3D printer builder, it was quickly obvious to use the thermistor inputs for measuring tube output temperature and control the secondary cooling system to regulate the tube reservoir temp. Might as well show the temps on the GLCD display too.  Every build a custom UI for smoothieware?




---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/9VRoAYMXCT8) &mdash; content and formatting may not be reliable*
