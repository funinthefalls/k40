---
layout: post
title: "So I found this guys on a trade show today"
date: March 11, 2018 19:57
category: "Material suppliers"
author: "Ariel Yahni (UniKpty)"
---
So I found this guys on a trade show today. The sell laser engravable cases made from cherry wood and others species. 



Wholesale starts at 6.5 but I'm sure you could get a better price . They are based in Florida

![images/1f6bebf1d7135f3c819027448ba8d676.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f6bebf1d7135f3c819027448ba8d676.jpeg)



**"Ariel Yahni (UniKpty)"**

---
---
**Timothy Rothman** *March 15, 2018 15:51*

I don't see where they sell the cases as raw materials (unengraved).


---
**Ariel Yahni (UniKpty)** *March 15, 2018 15:53*

**+Timothy Rothman** Contact them. Not sure the'll do retail or small qty's


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/QMvhc9XcTdY) &mdash; content and formatting may not be reliable*
