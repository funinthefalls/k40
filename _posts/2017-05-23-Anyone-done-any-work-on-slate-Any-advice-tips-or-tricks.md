---
layout: post
title: "Anyone done any work on slate? Any advice, tips or tricks?"
date: May 23, 2017 23:35
category: "Materials and settings"
author: "Nigel Conroy"
---
Anyone done any work on slate?

Any advice, tips or tricks?



Going to try engrave words on some



Got some from an old school roof, about 170 years old

![images/4afdfa072d14013360bbf6c1321a1a90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4afdfa072d14013360bbf6c1321a1a90.jpeg)



**"Nigel Conroy"**

---
---
**Gee Willikers** *May 23, 2017 23:51*

Yes. Not much to it, it's very forgiving.

![images/8f4e12226f45b88e03d38a12199e6b52.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f4e12226f45b88e03d38a12199e6b52.jpeg)


---
**greg greene** *May 23, 2017 23:54*

excellent medium for lasers - but sometimes hard to find


---
**Steve Clark** *May 24, 2017 00:11*

Yes, I've done a little. The rusty looking spot will burn darker. I only did one pass so maybe more passes will even it out.


---
**Nigel Conroy** *May 24, 2017 00:27*

Yes it was hard to find, but I got about 40 of them.



What about speed and power settings?



I've a 60W tube so I'm just looking for a starting point. 


---
**HalfNormal** *May 24, 2017 12:44*

I did some and they turned out great. I ran slow engrave settings.will need to play to find the best settings for your machine. 


---
**Gavin Dow** *May 25, 2017 14:06*

I've run some before. I use "medium" settings all around, and it seems to produce very good results. Like some others have said, it's very forgiving. Turn the power up for more contrast, down for less, there's no real hard limits in either direction that I've found.

I have had VERY good results putting a thin coat of spray polyurethane on the engraved side. It darkens the slate, but it makes the engraving POP, and should seal up the surface to prevent some degradation.


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/9qf4sTrBcD8) &mdash; content and formatting may not be reliable*
