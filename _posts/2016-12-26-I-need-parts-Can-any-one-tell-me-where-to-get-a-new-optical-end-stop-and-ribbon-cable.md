---
layout: post
title: "I need parts. Can any one tell me where to get a new optical end stop and ribbon cable"
date: December 26, 2016 17:28
category: "Hardware and Laser settings"
author: "timb12957"
---
I need parts. Can any one tell me where to get a new optical end stop and ribbon cable. I have an old post if you care to look it up and read about the issue I am having with my laser. We have already been through many diagnostics and attempts at solving the problem. My latest was to locate and replace the main control board. However that did not solve the issue. So now I would like to find a new end stop and ribbon cable to try. I have included pictures of the parts



![images/e72a17980a5292b0d13a35a0a2d3271c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e72a17980a5292b0d13a35a0a2d3271c.jpeg)
![images/eca3c28e38a5322bb76b9548f2c66b82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eca3c28e38a5322bb76b9548f2c66b82.jpeg)
![images/fe79ceae2ab0ff5a00309d6a0ace5a46.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fe79ceae2ab0ff5a00309d6a0ace5a46.jpeg)

**"timb12957"**

---
---
**Andy Shilling** *December 26, 2016 17:47*

I ordered new optics from eBay to solder onto the board I think the number I searched for was tcst 2103. As for the ribbon look up FFC. Flat flexible cable but you'll need the right pitch and pin number.


---
**timb12957** *December 26, 2016 17:56*

I have obtained a new sensor and replaced the old one on the board. However the problem remained. I was hoping if I replaced the entire board it my resolve the issue.


---
**Andy Shilling** *December 26, 2016 18:07*

I'm sure the board only links the sensor back to the main control board. Unfortunately I'm having a problem with my system so I have now ordered a Cohesion3d board and mechanical end stops to replace these.


---
**Ray Kholodovsky (Cohesion3D)** *December 26, 2016 18:24*

**+Andy Shilling** plenty of people do use the ribbon cable and endstops as the machine comes without problems, FYI. Totally up to you though. 

As for the **+timb12957**, yeah, gut it. Put in endstops and run normal wires to your board. 


---
**Andy Shilling** *December 26, 2016 18:33*

**+Ray Kholodovsky**​ I'm swapping out the end stops because I've had nothing but trouble with them and I think mechanicals would be better suited as I want to do quite a bit of wood work. Getting to the lower optical to clean it is a royal pain. But I do feel in preaching to the vicar here lol. 



Have you had good results with the 2 test boards you received?


---
**Ray Kholodovsky (Cohesion3D)** *December 26, 2016 18:38*

Understood.  

Yeah, they tested out fully!


---
**Andy Shilling** *December 26, 2016 18:40*

Fantastic news buddy.


---
**timb12957** *December 26, 2016 21:11*

Can you share a link and a pic of the mechanicals you are using to replace the optical end stops? What is required to hook them up?


---
**Andy Shilling** *December 26, 2016 23:06*

**+timb12957** these are what I have gone for. [amazon.co.uk - Solu 3pc CNC 3d Printer Mech Endstop Switch for Reprap Makerbot Prusa Mendel Ramps1.4//mechanical End Stop Endstop Switch for CNC 3d Printer Reprap Makerbot//mechanical Endstop for 3d Printer Makerbot Prusa Mendel Reprap CNC Arduino Mega 2560 1280: : Lighting](https://www.amazon.co.uk/d/DIY-Tools/Printer-Endstop-Switch-Reprap-Makerbot-mechanical-Arduino/B01F6WY34A/ref=sr_1_sc_1?ie=UTF8&qid=1482793389&sr=8-1-spell&keywords=Arguing+ends+tops)


---
**timb12957** *December 27, 2016 00:35*

Unfortunately, when I follow the link, it states that currently there is not a seller delivering to the USA


---
**Ray Kholodovsky (Cohesion3D)** *December 27, 2016 00:47*

**+timb12957** We have them in stock at Cohesion3D (quick shipping from NJ): [http://cohesion3d.com/mechanical-endstop/](http://cohesion3d.com/mechanical-endstop/)


---
**timb12957** *December 27, 2016 01:30*

Great, but I have limited knowledge of how to integrate these into my existing setup. My current end stop board has two ribbon cables, and the one 4 position plug. As well, I have to wonder now, with only the optical sensor and 1 resistor on the board,  can this really be what I need to replace. I have replaced the optical sensor already and checked the resistor which checks OK.


---
**Don Kleinschnitz Jr.** *December 27, 2016 02:39*

**+timb12957** this is the same problem we were working earlier this year? Did you see the endstops tester I posted earlier.


---
**timb12957** *December 27, 2016 03:18*

Yes this is the same problem from earlier. I saw the endstop tester you posted. I don't have the greatest understanding of electronics. For one, what is a middleman board? And on another note, I have re checked the continuity of all the connections on the ribbon cable. I have checked all the connections on the end stop board and since the end stop only has the sensor and the resistor which both check ok, it is hard to see how it could be the ribbon or end stop causing the problem. I thought about switching the step motors to see if the issue went to the other axis 


---
**Don Kleinschnitz Jr.** *December 27, 2016 10:18*

**+timb12957** I agree and this has me stumped also. In these cases the best one can do is to try stuff until symptoms show the problem or a failed part is found. Swapping the motors is a good idea although as I recall the axis moves.

Another choice is to buy another stepper and replace each in turn with it. It may be easier than disassembling both axis.

I am willing to help any way I can including testing the end stop boards here although I think you are not in the US?

BTW we tested the main end stop what about the X end stop board under the gantry.


---
**Don Kleinschnitz Jr.** *December 27, 2016 13:39*

Just thinking, any chance something is shorting out the board when it's mounted, like a screw etc?


---
**timb12957** *December 27, 2016 18:44*

I have not tested the X end stop, however it seems to work fine. If I move it out of position, at power up it sets X position just fine. I will check for a possible short on Y axis card.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/fjRWadLR2jK) &mdash; content and formatting may not be reliable*
