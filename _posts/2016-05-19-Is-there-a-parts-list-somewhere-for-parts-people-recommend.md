---
layout: post
title: "Is there a parts list somewhere for parts people recommend?"
date: May 19, 2016 18:38
category: "Air Assist"
author: "Matt Schmidt"
---
Is there a parts list somewhere for parts people recommend? I want to do the air assist head plus a small air pump. Any suggestions on what to get?





**"Matt Schmidt"**

---
---
**Joe Keneally** *May 19, 2016 18:51*

I got a new lens (LSR-LEN18ZN5I) and the air assist nozel replacement (LSR-LH1820B) from LightObjects.com for about 45$ US.  Very happy with them!


---
**Joe Keneally** *May 19, 2016 18:53*

I am using a repurposed pool toy inflator for my air source.  A LITTLE noises but it works and I had it on hand!  :)


---
**Michael Audette** *May 19, 2016 19:42*

I 3D printed a head - it works but the small fish tank pump is just slightly underpowered to extinguish all the flare ups.


---
**Ariel Yahni (UniKpty)** *May 19, 2016 19:47*

**+Matt Schmidt** you can take a look at my collection of K40 Upgrades for a couple of links




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 20, 2016 02:05*

I am using a 3d printed air assist head that I designed, but it had flaws. Got the new design being printed now & should have it in the next few days. It just slots onto the existing laser head (so you can continue using the stock lens/lens mount) & it has 2 intakes for 2 aquarium pumps, to increase the power (as like **+Michael Audette** I found that 1 air pump was way too underpowered for removal of all flareups).



edit: other parts you may want is a better cutting table, z-table (so it can move up & down), inline flow sensor (to make sure the water is always running), door interlock switch (to make sure laser won't fire with door open). I don't have any of these myself, but a lot of people have them & recommend them. For cutting table, there are a few simple designs for manual z-tables around that you could build yourself easily.


---
*Imported from [Google+](https://plus.google.com/113981209816229139995/posts/8ZYtHM15ytD) &mdash; content and formatting may not be reliable*
