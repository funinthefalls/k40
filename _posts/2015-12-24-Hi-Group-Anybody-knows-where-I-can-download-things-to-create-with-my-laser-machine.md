---
layout: post
title: "Hi Group, Anybody knows where I can download things to create with my laser machine ?"
date: December 24, 2015 14:27
category: "Discussion"
author: "Gilles Letourneau"
---
Hi Group,



Anybody knows where I can download things to create with my laser machine ?    





**"Gilles Letourneau"**

---
---
**Stephane Buisson** *December 24, 2015 14:34*

google it, several repository exist. all depend what are you looking for.

thingyverse, Ponoko, youmagine, creative market, and others you could find on below post.



but also very easy to do anything based on any .jpeg pict.


---
**Gilles Letourneau** *December 24, 2015 14:39*

I did a few tests with pictures for engraving and it's working fine.  Now I want to try cutting and assembling like small model or christmas ornements.  


---
**Gary McKinnon** *December 24, 2015 14:48*

Google :



line vector christmas clip art




---
**Gilles Letourneau** *December 24, 2015 16:48*

Ok I found something at Youmagine.  It's a cad file. they are talking about openSCAD and that the output can be in DXF for laser or STL for 3D print.  I will start with that and see what I can do with a DXF file in order to make it work with Corel laser.  


---
*Imported from [Google+](https://plus.google.com/101789423601866268108/posts/WoVaqamqf7e) &mdash; content and formatting may not be reliable*
