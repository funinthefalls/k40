---
layout: post
title: "I've uploaded the design to Thingiverse. Please feel free to download and adjust accordingly"
date: April 29, 2015 12:58
category: "Repository and designs"
author: "David Wakely"
---
I've uploaded the design to Thingiverse. Please feel free to download and adjust accordingly. Make sure that you make sure the panel in your machine matches the measurements of the drawing.



Enjoy!





**"David Wakely"**

---
---
**Stephane Buisson** *April 29, 2015 16:02*

Thank you David !


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/W8NMJpvVJBt) &mdash; content and formatting may not be reliable*
