---
layout: post
title: "I modded my K40 Laser to run from an Arduino Mega 2560 with a RAMPS 1.4 board"
date: January 03, 2015 18:16
category: "Modification"
author: "Eric De Larwelle"
---
I modded my K40 Laser to run from an Arduino Mega 2560 with a RAMPS 1.4 board. I'm currently using pronterface to push the G-code to the laser, but I can also save the G-code on the SD-card to let the laser run independently. 





**"Eric De Larwelle"**

---
---
**Stephane Buisson** *January 04, 2015 11:24*

Welcome Eric, and thank you for sharing your experience.


---
**Tim Fawcett** *January 15, 2015 21:03*

Hi Eric,



I am just preparing to do the same - which software are you using ?


---
**Eric De Larwelle** *January 16, 2015 00:02*

I'm using a modified version of Marlin. The version I'm using came from here .... [http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html](http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html)


---
*Imported from [Google+](https://plus.google.com/+EricDeLarwelle/posts/eZYRXMJqSyP) &mdash; content and formatting may not be reliable*
