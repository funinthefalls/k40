---
layout: post
title: "Hi All, I've been experimenting engraving using LaserDRW and had everything working fine, however when I try with CorelLazer (using Corel 12) it is ridiculously slow"
date: November 05, 2015 11:17
category: "Software"
author: "Anthony Coafield"
---
Hi All, I've been experimenting engraving using LaserDRW and had everything working fine, however when I try with CorelLazer (using Corel 12) it is ridiculously slow. Like 10mm/s slow with what look to be the same settings. The engrave speed is at 300mm/s when I set it to engrave.



EDIT: I just tried to cut and it does that ridiculously fast despite being set to 10mm/s. A 40mm circle in 4 flashes (thus looking nothing like a circle at all). I changed it to 300mm/s and it's nice and slow and looking like a circle now... very odd.



Could it be something in the Corel Data Setting? That seems to be the only thing different in the little shortcut menu between Corel and LaserDRW. 





**"Anthony Coafield"**

---
---
**Phil Willis** *November 05, 2015 11:51*

All I can think of is that the machine selected is the wrong one. However if LaserDraw works then it should be using the same.



I cut 3mm ply at 5mm/s and engrave the same at 400mm/s with 75% and 20% power respectively on an unmodified K40 using Corel Laser.


---
**Coherent** *November 05, 2015 15:21*

**+Phil Willis**

 I agree with Phil... check your machine selection first. Having the wrong model selected will make it do all kinds of weird things because the stepper/gear/steps per unit etc are different. I had similar issues initially and although LaserDrw was working right, when I loaded CorelLaser/Plugin it was a on a different machine setting and had to be changed. Not sure what model laser you have but the latest ones should be set to the model ending in M3.


---
**Joey Fitzpatrick** *November 05, 2015 20:43*

Common problem when the mainboard setting is wrong.  Open corelaser and click the settings tab(in the laser tool bar on top)  open the mainboard pull down and select the proper main board number.  most likely the M2 board.  when corelaser is installed, the default board is for an older board type.


---
**Anthony Coafield** *November 05, 2015 22:18*

Thank you! It was the mainboard setting. I set it all up in LaserDRW but it didn't carry over to CorelLaser, should be good now. 


---
*Imported from [Google+](https://plus.google.com/103162462482197579113/posts/J4Lkcer9jWa) &mdash; content and formatting may not be reliable*
