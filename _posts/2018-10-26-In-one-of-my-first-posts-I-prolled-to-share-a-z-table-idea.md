---
layout: post
title: "In one of my first posts I prolled to share a z- table idea"
date: October 26, 2018 01:18
category: "Modification"
author: "Tom Traband"
---
In one of my first posts I prolled to share a z- table idea.  Well, that one didn't work out but now I finally have a workable solution in place so I'm ready to make good on that promise. Materials are from Amazon or the local hardware store plus scraps on hand.



I removed the knob from a lab jack (which probably wouldn't have involved a band saw and a dremel if I'd realized it was reverse threaded), thread- locked the remaining 6mm nut, added a 10mm socket and a flexible bit extension under the rail, then drilled a hole in the front of the case. Some scrap hot- glued on the floor of the case and the bottom of the cutting platform provides repeatable positioning. The blue handle is a promotional mini screwdriver set from a local tailor but any 1/4" hex handle would work just as well.



If I get crazy someday I suppose I could re-route the extension into the control compartment and add a stepper motor and limit switches but this checks the "gets the job done" and "takes 10 seconds to swap in or out" boxes so I think I'm good for the time being.



![images/d19df09d4da8eea68cfb4acd090343d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d19df09d4da8eea68cfb4acd090343d3.jpeg)
![images/f09ff6844aff40a31f932201aeb86f5c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f09ff6844aff40a31f932201aeb86f5c.jpeg)
![images/a1db9072b7557bb45b0fc92a8d3957c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a1db9072b7557bb45b0fc92a8d3957c4.jpeg)
![images/29697f7fd9d5c03a8837611781d541b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/29697f7fd9d5c03a8837611781d541b4.jpeg)

**"Tom Traband"**

---
---
**John Little** *October 26, 2018 10:59*

Great simple answer, just the kind I like!


---
*Imported from [Google+](https://plus.google.com/105814471613882845513/posts/L9xE2TQF6cm) &mdash; content and formatting may not be reliable*
