---
layout: post
title: "Guys, Do we have a mirror alignment guide so I can use it?"
date: August 03, 2015 16:51
category: "External links&#x3a; Blog, forum, etc"
author: "Jose A. S"
---
Guys,



Do we have a mirror alignment guide so I can use it?



Thks





**"Jose A. S"**

---
---
**Joey Fitzpatrick** *August 03, 2015 17:17*

This is the guide that I like the best.  [http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)  When I align my mirrors, I use thermal receipt paper instead of masking tape.  I set the laser to the lowest possible setting that it will actually fire the laser.  (This keeps the mirrors clean from burnt masking tape fumes)  The laser marks are also smaller on thermal paper than the masking tape.  This will help with getting the mirrors more closely aligned. 


---
**Fadi Kahhaleh** *August 03, 2015 18:27*

I think the document **+Joey Fitzpatrick** shared is great and in depth.



There is also this link:

[http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)



which was originally shared with me thorough comments by Stephane Buisson.


---
**Jose A. S** *August 04, 2015 01:53*

Thank you guys!...I got the aligment but I now it can be improved. This is very dificult!


---
**Joey Fitzpatrick** *August 04, 2015 13:15*

Adjusting the mirrors can be a real pain.  The first time I tried, I spent 2 hours trying to get it right.  I found that every time I try it gets a little easier.  I can align them now in only a few minutes.  The trick for me was remembering what screws to turn to achieve the angle change I wanted and how even small adjustments can result in big changes.  Keep in mind that you will probably never get the alignment perfect.  As long as you get the beam marks as close as you can on all 3 mirrors, the machine will cut and engrave properly. Also, it may not be possible to have the marks exactly dead center on the mirrors.  The only mirror that should be as close to center as possible is #3. Good luck!!!!﻿


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/DCNeWnD5FwH) &mdash; content and formatting may not be reliable*
