---
layout: post
title: "Does anyone know of a good replacement carriage for the K40?"
date: April 19, 2017 00:18
category: "Modification"
author: "Ben Crawford"
---
Does anyone know of a good replacement carriage for the K40? I hate mine and the wheels keep coming lose.





**"Ben Crawford"**

---
---
**Steve Clark** *April 19, 2017 01:38*

When you say the wheels keep coming loose I'm assuming the cap screws are loosening. Get some #222 Loctite  adjust the wheels for proper tightness and let it set overnight... that should fix it. 



I see them on E-Bay and at Lightobject but they are the same design as to original so it would be a waste of money.



Lightobject also has replacement wheels but I think your problem would be fixed with  the Loctite.  


---
*Imported from [Google+](https://plus.google.com/110714520603831442756/posts/hhkdYakhwty) &mdash; content and formatting may not be reliable*
