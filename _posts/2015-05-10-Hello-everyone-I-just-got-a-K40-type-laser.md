---
layout: post
title: "Hello everyone. I just got a K40 type laser"
date: May 10, 2015 05:09
category: "Hardware and Laser settings"
author: "Jim Root"
---
Hello everyone. I just got a K40 type laser. I finally got it set up and the mirrors all aligned. I then did a few tests and it worked real well. Then I shut it down and went to HD to get a piece of acrylic to play with. Started it all back up again and now it moves real slow so everything cuts through even when it is set to engrave. Changing the speed doesn't seem to make any difference at all.



Any ideas?



Thanks

Jim





**"Jim Root"**

---
---
**Stuart Middleton** *May 10, 2015 07:04*

Hey Jim. Check the cut and engrave max speeds in the machine settings. They're under the cog icon on the toolbar for the machine. This will limit the head no matter what you set the speed to on the cut screen.


---
**Chris M** *May 10, 2015 11:16*

Also, check that the mainboard type in the Machine Properties is set correctly for your machine.


---
**Sean Cherven** *May 10, 2015 12:00*

Also the machine serial number must be entered in correctly.


---
**Jim Root** *May 10, 2015 13:27*

Thanks guys! That was quick! It turned out that it was the serial number, although I don't know how it got changed. I never set it in the first place so I'm not sure why it worked as first. 

Anyway it's working now. 


---
**Sean Cherven** *May 10, 2015 13:52*

Yeah a wrong serial number causes intermittent issues for me. Works fine sometimes, other times it don't.


---
**Jim Root** *May 10, 2015 14:03*

Does your machine have the trim pot type power control? I was wondering if it allows for repeatable power settings. I guess what I'm saying is, if I make a mark the corrisponds to 5ma will that position of the knob generally produce 5ma or do I have to "test" fire it each time to set it?



Did that make any sense at all?


---
**Sean Cherven** *May 10, 2015 14:06*

It would likely vary a little bit. Probably +/- 1mA, so it really all depends on how much accuracy you need.


---
**Jim Root** *May 10, 2015 14:08*

Thanks! I'm guessing for what I'm doing right now that is more than accurate enough. I just didn't want to mark it all up if I am not going to gain anything from it. 


---
**Sean Cherven** *May 10, 2015 14:10*

Place a sticker above the pot, and Mark on the sticker to test it first.


---
**Jim Root** *May 10, 2015 14:11*

Well that would be way too smart. :)


---
**Sean Cherven** *May 10, 2015 14:11*

What I would do, is design and print a number scale, and make a list that corresponds to the number scale. 


---
**Jim Root** *May 10, 2015 14:13*

That's a good idea. Then the list could be expanded to speed rate/power in relation to the material being engraved/cut.


---
**Sean Cherven** *May 10, 2015 14:17*

Exactly!


---
*Imported from [Google+](https://plus.google.com/113460554675119546243/posts/8FV6Dx1dLuD) &mdash; content and formatting may not be reliable*
