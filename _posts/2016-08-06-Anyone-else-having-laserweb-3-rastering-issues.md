---
layout: post
title: "Anyone else having laserweb 3 rastering issues?"
date: August 06, 2016 00:31
category: "Discussion"
author: "Derek Schuetz"
---
Anyone else having laserweb 3 rastering issues? It can't seem to generate the gcode I let it run for 2 hours and it only processed 30%





**"Derek Schuetz"**

---
---
**Derek Schuetz** *August 06, 2016 00:31*

**+Peter van der Walt** 


---
**Ariel Yahni (UniKpty)** *August 06, 2016 00:35*

Send me the file I'll test. I runned a raster 5 hours ago before the last commit


---
**Derek Schuetz** *August 06, 2016 00:36*

**+Ariel Yahni** ok will do when I'm back at my desk


---
**Ariel Yahni (UniKpty)** *August 06, 2016 00:37*

If it's the commit you can go back by doing git checkout #commit number 


---
**Derek Schuetz** *August 06, 2016 00:40*

**+Ariel Yahni** you mean roll back update? So I would type "git checkout #commit number" in command prompt?


---
**Ariel Yahni (UniKpty)** *August 06, 2016 00:50*

To return to the latest do git checkout master


---
**Derek Schuetz** *August 06, 2016 02:02*

still no go. heres the image [https://www.dropbox.com/sc/ic3xm7yhkjttefa/AAC27NJ3feHaWHh5SXd8OEQba](https://www.dropbox.com/sc/ic3xm7yhkjttefa/AAC27NJ3feHaWHh5SXd8OEQba)


---
**Derek Schuetz** *August 06, 2016 02:12*

**+Ariel Yahni** just found the bug beam diameter was at .001 (this has worked in all past versions) changed it to .01 and it now generates gcode


---
**Ariel Yahni (UniKpty)** *August 06, 2016 02:12*

Will try it once I get back later tonight. Unless someone else tries earlier


---
**Ariel Yahni (UniKpty)** *August 06, 2016 02:13*

Oh ok. Why would you use a spot size that small? 


---
**Derek Schuetz** *August 06, 2016 02:16*

i did some test of rastering adn at .1 my images were liney? but as i lowered beam diameter the image was much clearer


---
**Ariel Yahni (UniKpty)** *August 06, 2016 02:26*

O really, interesting. I would have thought that's was to small. 


---
**Derek Schuetz** *August 06, 2016 06:24*

**+Peter van der Walt** makes sense


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/YGQk15Vw4KW) &mdash; content and formatting may not be reliable*
