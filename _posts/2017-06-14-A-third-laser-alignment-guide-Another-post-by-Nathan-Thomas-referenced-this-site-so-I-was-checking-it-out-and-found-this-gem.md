---
layout: post
title: "A third laser alignment guide Another post by Nathan Thomas referenced this site so I was checking it out and found this gem"
date: June 14, 2017 12:50
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
A third laser alignment guide



Another post by **+Nathan Thomas** referenced this site so I was checking it out and found this gem.

It is written simply and actually explains what to do if the dots move.

[https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/](https://k40laser.se/lens-mirrors/k40-mirror-alignment-and-leveling/)





**"HalfNormal"**

---
---
**Ariel Yahni (UniKpty)** *June 14, 2017 15:11*

**+HP Persson**​ Is this your work?


---
**HP Persson** *June 14, 2017 15:31*

**+Ariel Yahni** yep, it's mine. Not 100% complete with the guide yet though. 


---
**Ned Hill** *June 19, 2017 21:17*

#K40Alignment  Also added the # to my post on the floating wombat guide **+Don Kleinschnitz**


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/5LpDnupS7fb) &mdash; content and formatting may not be reliable*
