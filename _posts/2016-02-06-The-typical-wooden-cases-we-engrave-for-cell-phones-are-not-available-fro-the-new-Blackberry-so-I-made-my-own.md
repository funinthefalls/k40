---
layout: post
title: "The typical wooden cases we engrave for cell phones are not available fro the new Blackberry, so I made my own"
date: February 06, 2016 23:31
category: "Object produced with laser"
author: "Cam Mayor"
---
The typical wooden cases we engrave for cell phones are not available fro the new Blackberry, so I made my own. Took 4 tries to get the workflow right but it is easily repeatable now.

![images/c2a25125f0f057f0fe8c586f58b55897.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c2a25125f0f057f0fe8c586f58b55897.jpeg)



**"Cam Mayor"**

---


---
*Imported from [Google+](https://plus.google.com/110883213653290462996/posts/KMsE57Uzqhx) &mdash; content and formatting may not be reliable*
