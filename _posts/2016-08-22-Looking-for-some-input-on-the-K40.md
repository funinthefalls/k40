---
layout: post
title: "Looking for some input on the K40"
date: August 22, 2016 17:04
category: "Discussion"
author: "Richard Brungard"
---
Looking for some input on the K40.  I design Model Railroad kits, and currently have to contract with another business to cut for me. I don't do large volume production, each kit run is small, usually 30-50 at a time, and the thickest thing I'm designing with would be 1/4" Basswood. This picture is 1/32 plywood and 3/32 Bass. 



I'd like to be able to do these small runs at home, and at the very least, I'd like to be able to do test cuts for pilot models that I could send to friends for contract cutting. It would also be cool to cut trinkety stuff for around the house or as gifts. 



For disclosure, I'm not an electrical or mechanical engineer, I'm a designer, but I'm also handy, and can figure things out with some research and assistance. 



I'd like to know if a K40 can do what I would need it to do, without a ton of fussing and tinkering. I'm working with some pretty tight tolerances for these kits,  so things like how focused I can get the cuts and etches are important. 



Thanks!

![images/8a7008e4c125699a7cc28292cc0591df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a7008e4c125699a7cc28292cc0591df.jpeg)



**"Richard Brungard"**

---
---
**Jim Hatch** *August 22, 2016 17:13*

How big is the sheet material you need to do this on? You're going to be limited to something around 10"x13" max with a slightly modified K40. If you can fit in that size or break your parts into that size, you'll be good. You should be able to do up to 1/4" Basswood.


---
**Ariel Yahni (UniKpty)** *August 22, 2016 17:18*

**+Richard Brungard**​  the short answer is yes. The long one is : you need to do laser beam alignment and focusing to the material being cut. Tolerances are very good when everything it's in place just mind the sizing of the raw material as this machine is 8"x12" under perfect alignment. Multiple passes could be required for 1/4 material if alignment is not spot on, at least on a 10mms cut speed. This all with stock machine


---
**Gee Willikers** *August 22, 2016 17:49*

Curious - What scale? Buildings or rolling stock?


---
**Robi Akerley-McKee** *August 22, 2016 20:03*

Looks good, though it looks like you could use some more air assist.  If the parts are held down with tabs you can go higher on pressure/flow.


---
**Richard Brungard** *August 23, 2016 17:35*

Thanks for the responses. Since I'm working primarily in HO scale, the cut area shouldn't be an issue. It would be nice to have 12x24, but all the parts sheets are smaller than 6x6. Out of curiosity, since I would be cutting different thicknesses frequently, can I shim to a max height of say 1/4" and add or remove shims for different thickness materials and maintain the focus? I'd be cutting various thicknesses, from .015 Laser board to 1/4". I see that light object has a z axis table, which would be a worthwhile addition for what I would need, I think. 


---
**Jim Hatch** *August 23, 2016 17:41*

**+Richard Brungard** Your shim idea is what I do. My table height is set for 3/4" material. Then if I'm doing 1/4" I slip two sheets of 1/4" ply under the extruded metal sheet I use for the bed. If I'm doing 1/8" material I add another piece of 1/8" ply under the metal sheet. It's pretty quick to do and works well if you normally do a fairly consistent set of sizes of material. If it tends to vary a lot or you want to play with defocused engraves then having a screw adjustable (manual or electric) table makes things faster.


---
**Richard Brungard** *August 24, 2016 14:01*

Thanks Jim. I wasn't sure if the focus would invert after a certain point (> <) causing it to defocus, making it pointless ( I think there is a pun there) but since I'm working in a 1/4" range (probably closer to 1/8" I guess it won't matter much.


---
**Jim Hatch** *August 24, 2016 14:53*

It does defocus on either side of the 50.8mm focal distance from the lens. That's why the laser kerf isn't as small as the laser at the focal point. It's spread out on either side.



You ideally want to put the focal point midway through your material for the cleanest cut. For engraving moving it deeper or higher is good for changing the effect. In acrylic you get smoother engraves with it focused high. With ply you can get deeper engraves if you focus it further into the material.


---
*Imported from [Google+](https://plus.google.com/109914712692409465405/posts/ctP9U5VD5dN) &mdash; content and formatting may not be reliable*
