---
layout: post
title: "Hi, I had a small fire inside my K40"
date: February 11, 2018 12:37
category: "Original software and hardware issues"
author: "Nick Arkell"
---
Hi,



I had a small fire inside my K40. It melted the X axis drive belt and damaged the laser carriage too too so I have replaced them.



The only other part that looks damaged is the gear wheel / pulley for the X axis on the opposite side to the motor. I believe it is a 20 tooth 3mm idler so will order that. However, I cannot figure out whether the pulley bracket holding it is damaged. It is hanging loose and I am unsure if that is just because the belt is not attached or if there should be plastic spacers on those screws holding it in place.



Photos are attached (second shows bracket I am referring to and damaged pulley) so if somebody with a fairly standard K40 could have a quick look at their machine and see what the bracket should be like, that would much appreciated!



Thank you.



![images/283ad8e931b882ba1c2a8ed3977032c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/283ad8e931b882ba1c2a8ed3977032c4.jpeg)
![images/00c5b583f13573f69cc279359f192291.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00c5b583f13573f69cc279359f192291.jpeg)

**"Nick Arkell"**

---
---
**BEN 3D** *February 11, 2018 13:05*

![images/24941684572c5d77aa1e8fdf6cfeda2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24941684572c5d77aa1e8fdf6cfeda2b.jpeg)


---
**BEN 3D** *February 11, 2018 13:06*

![images/820e53112e31b775e07e5977491e890c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/820e53112e31b775e07e5977491e890c.jpeg)


---
**BEN 3D** *February 11, 2018 13:07*

![images/97e0397e002b7b6bb0de9789fed30967.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97e0397e002b7b6bb0de9789fed30967.jpeg)


---
**BEN 3D** *February 11, 2018 13:12*

I attached some pictures of my k40, but I would advice you to create a complete new xy carriage and may a bigger one. Take a look to openbuilds to get inspired. 
{% include youtubePlayer.html id="r9lixZon0og" %}
[https://youtu.be/r9lixZon0og](https://youtu.be/r9lixZon0og)




---
**Anthony Bolgar** *February 11, 2018 15:42*

That is what my bracket looks like on one of my K40's. Minus the smoke damage of course ;)


---
**Nick Arkell** *February 11, 2018 16:20*

**+BEN 3D** Thanks for the tips Ben. I am trying to keep costs down at the moment but considering upgrades in the future so thank you. **+Anthony Bolgar** your photo is not showing. Could you re-post it if possible please? That would be really helpful to see. Thank you.


---
**Kelly Burns** *February 12, 2018 03:55*

That pulley is a tensioner. So with a melted/broken belt it will hang loose. 


---
**Nick Arkell** *February 12, 2018 11:44*

**+Kelly Burns** That's what I assumed. Does it look ok to you then if I just fit a new pulley to that bracket? Thanks.


---
*Imported from [Google+](https://plus.google.com/+NickArkell/posts/eQZupau7dVT) &mdash; content and formatting may not be reliable*
