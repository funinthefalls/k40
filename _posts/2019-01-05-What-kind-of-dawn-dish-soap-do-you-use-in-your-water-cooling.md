---
layout: post
title: "What kind of dawn dish soap do you use in your water cooling?"
date: January 05, 2019 12:41
category: "Discussion"
author: "Sean Cherven"
---
What kind of dawn dish soap do you use in your water cooling? 

There's like 20 different types of dawn dish soap lol.





**"Sean Cherven"**

---
---
**Ned Hill** *January 05, 2019 15:03*

Personally I don’t think you need dish soap. Once you clear the bubbles initially from the tube they don’t typically don’t reform.  If you are adding the tetra algicide it will also act as a surfactant. We typically recommend limiting how much additives you add to the cooling water to limit the conductivity of water, which can cause problems. 


---
**Sean Cherven** *January 05, 2019 15:05*

How much of the tetra algicide should I add per gallon?


---
**Ned Hill** *January 05, 2019 15:08*

About a cap full per 5 gallons is what I use. 


---
**Sean Cherven** *January 05, 2019 15:18*

I read elsewhere to use 1-2 drops per gallon.

So should I use a entire cap full or 1-2 drops per gallon?


---
**Don Kleinschnitz Jr.** *January 05, 2019 16:16*

**+Sean Cherven** 



[donsthings.blogspot.com - K40 Coolant Conductivity Matters!](https://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)



I tried dish detergent and it did not help my bubble problem.


---
**Tom Traband** *January 05, 2019 21:15*

I'm using 7 drops in 3 gallons. 


---
**Sean Cherven** *January 05, 2019 21:27*

**+Tom Traband** 7 drops of dish detergent or tetra algicide?


---
**James Rivera** *January 06, 2019 02:12*

Wow, I would definitely stay away from soap! Bubbles are no good. Just use distilled water and some algaecide. Why do you need anything else?


---
**Sean Cherven** *January 06, 2019 02:21*

**+James Rivera** idk, I just read a lot about people using dawn dish soap, so I was just trying to figure it out.


---
**Tom Traband** *January 06, 2019 13:49*

**+Sean Cherven** algicide. No soap.


---
**Don Kleinschnitz Jr.** *January 06, 2019 18:19*

**+James Rivera** some claim that dawn reduces bubbles (reduces surface friction) and clears them without up ending your machine. I tried it and it did nothing for my bubbles.


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/8ehzwKisVaX) &mdash; content and formatting may not be reliable*
