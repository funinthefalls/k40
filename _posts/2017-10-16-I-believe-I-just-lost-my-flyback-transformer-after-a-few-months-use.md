---
layout: post
title: "I believe I just lost my flyback transformer after a few months use"
date: October 16, 2017 17:02
category: "Original software and hardware issues"
author: "Frank Fisher"
---
I believe I just lost my flyback transformer after a few months use. Anyone know where I can get one <b>yesterday</b> for 40W as estimates are early December from China and a parts supplier in Sweden says next month before he has them :( if need be a full power supply next week is better than a spare part in a month.





**"Frank Fisher"**

---
---
**HalfNormal** *October 16, 2017 18:02*

Ebay has PS. Can normally request expedited shipping.


---
**Frank Fisher** *October 16, 2017 18:04*

I just spent half a day looking for anything other than 'free international shipping' do I have to contact them direct?


---
**HalfNormal** *October 16, 2017 18:06*

More than likely. I know a few of the PS suppliers are in the US and I am sure they would ship to you if you contacted them.


---
**Frank Fisher** *October 16, 2017 18:09*

If you can suggest any names, my ebay attempts to restrict location has failed so far.


---
**HalfNormal** *October 16, 2017 18:14*

[http://www.ebay.com/itm/40W-CO2-Laser-Power-Supply-for-Laser-Cutting-110V-220V-Switch-Green-USA-Stock-/122513729283?hash=item1c86632703:g:Wi8AAOSwlMFZJXj8](http://www.ebay.com/itm/40W-CO2-Laser-Power-Supply-for-Laser-Cutting-110V-220V-Switch-Green-USA-Stock-/122513729283?hash=item1c86632703:g:Wi8AAOSwlMFZJXj8)



[http://www.ebay.com/itm/35W-50W-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-110V-AC-/172848921879?hash=item283e993d17:g:rcoAAOSwoAxZqn-l](http://www.ebay.com/itm/35W-50W-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-110V-AC-/172848921879?hash=item283e993d17:g:rcoAAOSwoAxZqn-l)



[http://www.ebay.com/itm/40W-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-220V-110V-AC-/172847230093?hash=item283e7f6c8d:g:EC4AAOSwo-NZqQO7](http://www.ebay.com/itm/40W-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-220V-110V-AC-/172847230093?hash=item283e7f6c8d:g:EC4AAOSwo-NZqQO7)



[http://www.ebay.com/itm/40W-CO2-Laser-Power-Supply-for-Laser-Engraver-Cutter-Machine-220V-110V-USA-Stock-/122514312988?hash=item1c866c0f1c:g:u4oAAOSwn-tZJWXZ](http://www.ebay.com/itm/40W-CO2-Laser-Power-Supply-for-Laser-Engraver-Cutter-Machine-220V-110V-USA-Stock-/122514312988?hash=item1c866c0f1c:g:u4oAAOSwn-tZJWXZ)




---
**Frank Fisher** *October 16, 2017 18:23*

Thanks.


---
**HalfNormal** *October 16, 2017 19:02*

Let me know if you have any luck.


---
**Frank Fisher** *October 18, 2017 13:50*

Unfortunately an admin deleted the links before I got to my machine 10 mins later, but someone on here had one thanks.


---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/3H5erTky8m3) &mdash; content and formatting may not be reliable*
