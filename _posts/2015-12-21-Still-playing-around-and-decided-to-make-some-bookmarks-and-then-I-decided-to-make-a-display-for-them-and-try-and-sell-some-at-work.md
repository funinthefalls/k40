---
layout: post
title: "Still playing around, and decided to make some bookmarks and then I decided to make a display for them and try and sell some at work"
date: December 21, 2015 20:44
category: "Object produced with laser"
author: "Tony Schelts"
---
Still playing around, and decided to make some bookmarks and then I decided to make a display for them and try and sell some at work. sold quite a few. Here are some pictures and the plans if anyone want to give them a go, thoughts and suggestions on a postcard please :) If you want the corel draw CDR give me a shout



![images/e985a6491af4a309d8cb84f681fe382d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e985a6491af4a309d8cb84f681fe382d.jpeg)
![images/a251bc581a8692aa0eb0f4e24678c3a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a251bc581a8692aa0eb0f4e24678c3a2.jpeg)
![images/45a155eeaf18687838677fd5d1ced2e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45a155eeaf18687838677fd5d1ced2e6.jpeg)
![images/64d583174f06b91d53fec28c4ae6e7d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/64d583174f06b91d53fec28c4ae6e7d2.jpeg)
![images/1b2b2f41ef845bc5356cb7da8cc22ff5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b2b2f41ef845bc5356cb7da8cc22ff5.jpeg)

**"Tony Schelts"**

---
---
**Marek Pár** *December 21, 2015 21:31*

It looks well.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 21:42*

That's great. I like the seahorse bookmarks. Pretty cool looking. Well done on selling a bundle of them.


---
**Anthony Bolgar** *December 22, 2015 00:18*

Very nice, I would love the cdr file.


---
**I Laser** *December 22, 2015 04:24*

+1 CDR :)



Agree with Yuusuf, the seahorses look excellent. How thick is the wood, just thinking it looks a little bulky?


---
**Tony Schelts** *December 22, 2015 09:33*

3mm ply no complaints so far


---
**Tony Sobczak** *February 23, 2016 07:35*

**+Tony Schelts**​ any chance of getting any of the cdrs. Just got my machine a week ago and would like to try some of the out.  Thanks in advance. 


---
**Tony Schelts** *March 11, 2016 00:15*

[https://drive.google.com/folderview?id=0B7mAq4bRA7e-OHNtZ240Ylg5N3c&usp=sharing](https://drive.google.com/folderview?id=0B7mAq4bRA7e-OHNtZ240Ylg5N3c&usp=sharing)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/DEnibowfm35) &mdash; content and formatting may not be reliable*
