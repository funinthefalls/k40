---
layout: post
title: "Got this 3 speed blower from amazon today for 50 bucks"
date: March 22, 2016 03:31
category: "Modification"
author: "Vince Lee"
---
Got this 3 speed blower from amazon today for 50 bucks.  Besides being a handy tool to have around, I thought it might make a nice upgraded blower.

![images/6bcb2205441a6908555e5b2f163edb88.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6bcb2205441a6908555e5b2f163edb88.jpeg)



**"Vince Lee"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 22, 2016 10:15*

That looks pretty decent. What's it's noise factor like? Loud or average?


---
**Anthony Bolgar** *March 22, 2016 11:28*

I have found that the cheapest, but very effective upgrade is to replace the stock exhaust fan with a 4" inline bilge blower (used in boat engine compartments to get rid of fumes, and is only about 45-50db of noise level.). It runs off 12v DC so I used an old ATX PSU to run the blower and the 12v DC LED lights I placed around the perimeter of the inside of the work area lid. I also use the 5v DC output on the ATX PSU to run the Red dot laser pointers I installed to line up the head to the work surface. This way, I am not overloading the laser PSU with these upgrades.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 22, 2016 12:21*

**+Anthony Bolgar** I like that. I was planning to use an old PSU for the same purpose (to power everything that is not stock on the system), but unfortunately I don't have an old PSU, so I have to buy a new/2nd-hand one somewhere. But I will check out a bilge blower to see where I can find one locally. Thanks for the tip.


---
**Tom Spaulding** *March 22, 2016 14:25*

**+Anthony Bolgar**  Would you mind sharing a picture of your blower installation? Thanks


---
**Vince Lee** *March 22, 2016 15:22*

I like the bilge blower idea.  I'll try that if this doesn't work.  The one I saw on Amazon was measured in a review at 5cfm+.  Though this shop vac blower on high is supposed to be 500cfm so I'm hoping for way overkill to keep the compartment smoke free!


---
**Anthony Bolgar** *March 22, 2016 15:26*

My bilge blower moves 370 CFM. It is currently not in use because I am redoing all the duct work to make it neater. When I have it back together I will post some pictures for you Tom.


---
**Vince Lee** *March 22, 2016 17:25*

**+Anthony Bolgar**, Sounds like a great blower and the installation would certainly be neater.  I'll probably still first try what I have since I've already got it, but do you remember the make/model of yours?  With a quick search on Amazon I've found this 240cfm model for 40 bucks which seems ok but not as powerful:  [http://www.amazon.com/-Line-Bilge-Blowers-Size-Seachoice/dp/B000Y860IE/ref=sr_1_26?s=sporting-goods&ie=UTF8&qid=1458667112&sr=1-26&keywords=cfm+bilge+blower](http://www.amazon.com/-Line-Bilge-Blowers-Size-Seachoice/dp/B000Y860IE/ref=sr_1_26?s=sporting-goods&ie=UTF8&qid=1458667112&sr=1-26&keywords=cfm+bilge+blower)


---
**Tom Spaulding** *March 22, 2016 19:55*

**+Anthony Bolgar**  Thanks,, I went ahead and ordered one on ebay to try out. They seem like a good solution for our K40s


---
**Todd Miller** *March 22, 2016 22:10*

That blower will be more than enough for your laser.



I use the same blower for my wacky waving inflatable arm flaling tube man :-)


---
**Vince Lee** *March 22, 2016 23:46*

**+Todd Miller**, that's a great idea.. I should combine the two!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 03:13*

**+Vince Lee** Vent the K40 exhaust out through a wacky waving inflatable arm flailing tube man? Sounds fun :)


---
**Tom Spaulding** *March 24, 2016 17:51*

Wow the $25 bilge fans on ebay really blow!

I'm amazed at how powerful it is at 12v. The thing will push itself along a smooth surface with fan power. Great find Anthony!


---
**Bob Steinbeiser** *March 25, 2016 17:40*

Any idea of how you'll 'plumb' this to the K40?  Doesn't look like it has anyway to attach fittings, etc.


---
**Vince Lee** *March 26, 2016 04:20*

**+Bob Steinbeiser** I cut a large hole in the bottom of the laser cutter and remounted the blower grille inside, clamping the blower sideways beneath the cutter.  I have the cutter in a steamer trunk coffee table I made, so I also rearranged the rack below trunk part to make space.


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/G3KUkDd7Yfm) &mdash; content and formatting may not be reliable*
