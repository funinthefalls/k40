---
layout: post
title: "Does anyone know the type of belts used in the K40 laser?"
date: June 29, 2016 21:08
category: "Discussion"
author: "Mircea Russu"
---
Does anyone know the type of belts used in the K40 laser? Are they GT2 belts?





**"Mircea Russu"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 29, 2016 22:21*

Mostly GT2 from what I recall. Although there are rounded profile & straight profiled GT2s. Mine seemed to be the straight profile if I recall correct.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/CRUf3XRKmW2) &mdash; content and formatting may not be reliable*
