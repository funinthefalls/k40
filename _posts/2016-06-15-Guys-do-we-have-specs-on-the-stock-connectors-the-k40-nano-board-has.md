---
layout: post
title: "Guys do we have specs on the stock connectors the k40 nano board has?"
date: June 15, 2016 22:14
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Guys do we have specs on the stock connectors the k40 nano board has?  I particularly would be interested in the receptacle for the endstop connector (non ribbon cable machine) and the large connector with the 4 blue wires. I can make a tiny breakout pcb for those 2 without cutting anything, just need digikey part numbers or some details.  Thanks!





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Don Kleinschnitz Jr.** *June 15, 2016 22:27*

Here are some resources, careful some of this is guesses :):

[http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html](http://donsthings.blogspot.com/2016/06/k40-s-middleman-board-interconnect.html)

[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface.html)

[http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)

[http://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/](http://www.digikey.com/schemeit/project/k40-wiring-GCKOUK0100Q0/)


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/BdudVdxUzQw) &mdash; content and formatting may not be reliable*
