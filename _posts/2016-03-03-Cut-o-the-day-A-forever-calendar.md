---
layout: post
title: "Cut o the day. A forever calendar"
date: March 03, 2016 19:34
category: "Object produced with laser"
author: "Ben Walker"
---
Cut o the day.  A forever calendar.   This is first prototype.  

![images/173c4e368ea1d255466821f831dff201.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/173c4e368ea1d255466821f831dff201.jpeg)



**"Ben Walker"**

---
---
**ED Carty** *March 03, 2016 19:55*

Sweet design. clever. looks great


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 03, 2016 20:25*

That's a cool idea. Nicely executed too.


---
**3D Laser** *March 04, 2016 00:13*

Would you be willing to share the file 


---
**Ben Walker** *March 04, 2016 14:41*

**+Corey Budwine** sure Corey.   As soon as I get it all perfect (they don't call me the mad scientist for nothing).  The file as it is is confusing because I leave both good and bad on the drawing board as I process.   Then slowly take away what does not work.   


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/5EPUF7NLV2G) &mdash; content and formatting may not be reliable*
