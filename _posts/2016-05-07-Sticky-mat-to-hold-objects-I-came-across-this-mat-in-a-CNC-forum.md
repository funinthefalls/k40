---
layout: post
title: "Sticky mat to hold objects I came across this mat in a CNC forum"
date: May 07, 2016 18:39
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Sticky mat to hold objects



I came across this mat in a CNC forum. The manufacture claims it can be used with a laser. Anyone use something like this before? The 12 inch x 12 inch is about $60.00 USD



[https://www.rowmark.com/seklema/seklema.asp](https://www.rowmark.com/seklema/seklema.asp)









**"HalfNormal"**

---
---
**Ariel Yahni (UniKpty)** *May 07, 2016 19:12*

It looks really interesting


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 19:23*

Looks interesting indeed, but reading the datasheet, it seems they say it can last "many months" or "years". It says it self-heals, but repeated cuts into the material can reduce the effectiveness. Would be great for engraving, where you aren't cutting all the way through the material.


---
**Scott Marshall** *May 07, 2016 21:17*

For that kind of money, it seems you could build a vacuum table. It looks like the sticky mats they use in the airlock door at cleanroom entrances.

I think those are multi-layer, peel away type though.



My silhouette cutter has a sticky mat to feed odd sized pieces thru it, and I "refresh" it with a semi tack spray. (New ones are about 10 bucks) There is several brands, I currently have a can of Aileens Tacky Spray (purple/gold can)right now. Seem you could spray it on almost any surface and get the results.

A $10 can should do about 25-30 12 x 12 mats. Almost disposable pricing. It may be worth a try.



I haven't had problems with stuff moving around unless I have the air turned way up trying something weird. Since I started using luan with brads in it, even high airflows don't move plywood or acrylic.



The spray is easy to try, I may make a couple of "tacky boards" to try when the job comes up. Seems like you  would want to have open centers and stick the workpiece down around the edges, so several different sized "Tacky frames" might be the way to go.



There is no end to the ways to use these machines...


---
**Mishko Mishko** *May 07, 2016 21:20*

I expect this would be similar material to cloth dust rollers, dash phone holders etc... If so, phone mats are cent a dosen, I'd try to use that first, given the price, if indeed there is a benefit when used with laser, I suppose that depends greatly on the task at hand. But with K40, I doubt there will ever be time critical production runs which would justify the investment.



To have it as a toy,  though - absolutely;)



[http://www.ebay.com/itm/5pcs-Car-Magic-Grip-Sticky-Pad-Anti-Non-Slip-Dash-Mat-Board-Phone-Holder-Rubber-/272109482592?hash=item3f5afd3260:g:xZYAAOSwFqJWnZuu](http://www.ebay.com/itm/5pcs-Car-Magic-Grip-Sticky-Pad-Anti-Non-Slip-Dash-Mat-Board-Phone-Holder-Rubber-/272109482592?hash=item3f5afd3260:g:xZYAAOSwFqJWnZuu)



I'd trust it with maybe engraving on a router. I imagine that thickness/area ratio and surface finish of an object define the hold strength.



I used to have something similar years ago for screen printing t-shirts...


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/F16EMdz9N1S) &mdash; content and formatting may not be reliable*
