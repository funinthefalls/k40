---
layout: post
title: "After some discussion with Stephane Buisson , we have decided to move the K40 G+ forum to As the G+ community becomes available to archive we will be making copies that will be stored in a few locations for posterity"
date: February 03, 2019 18:04
category: "Discussion"
author: "Anthony Bolgar"
---
After some discussion with **+Stephane Buisson**, we have decided to move the K40 G+ forum to [https://forum.makerforums.info](https://forum.makerforums.info)



As the G+ community becomes available to archive we will be making copies that will be stored in a few locations for posterity. We are hoping that a method will be found to transfer this knowledge base into a usable format for the members



All the same categories that are on G+ will be available on this new forum, as well as the bonus of hosting other maker communities as well so that there will be a unified log in, and one place to look for information on all types of maker wants, needs, and issues.



So go to the new forum, sign up and start building this new home for the K40 community :)





**"Anthony Bolgar"**

---
---
**HalfNormal** *February 04, 2019 02:29*

**+Stephane Buisson** **+Anthony Bolgar** I am getting this error when asked to activate my new account ["BAD CSRF"] 


---
**Anthony Bolgar** *February 04, 2019 02:31*

**+HalfNormal** Let me have a look. 


---
**Anthony Bolgar** *February 04, 2019 02:32*

Not sure what was going on, but I activated your account for you. Welcome to the dawn of a new era in the K40 community :)


---
**HalfNormal** *February 04, 2019 02:33*

Thanks for the quick fix!


---
**Anthony Bolgar** *February 04, 2019 02:38*

There will be growing pains, I created the forums in less than 12 hours of time, still learning the discourse system myself. And it is all self maintained, could not afford to pay the hosting provider for more than just the basic hosting. I have to configure everything and be tech support as well as system administrator, and moderator. A few hats right now....lol




---
**HalfNormal** *February 04, 2019 02:47*

Believe me, I know how you feel. 


---
**HalfNormal** *February 04, 2019 02:48*

I'm having to do all kinds of stuff every day on my forms to make sure they work and not break something I fixed the day before!


---
**Ned Hill** *February 04, 2019 06:47*

**+Anthony Bolgar** Thanks for setting this up for us.




---
**Ned Hill** *February 04, 2019 18:56*

**+Anthony Bolgar**  Help!  It wouldn't let me create more posts on the new forum. Can't even DM you there.  Lol apparently I reached my new member limit. 

![images/f4edaad0bf7689a8bed3cadcbed231de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4edaad0bf7689a8bed3cadcbed231de.jpeg)


---
**Ned Hill** *February 04, 2019 20:21*

Alright, Trust Level 1!  Thanks :)

![images/55069dc980deb22cdba2f87a9e5f8544.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55069dc980deb22cdba2f87a9e5f8544.jpeg)


---
**Stephen Kongsle** *February 05, 2019 23:58*

I tried signing up over a day ago, and never received my verification email (yes, I've checked my spam folder). Perhaps not filling the (optional) name field borked the process?


---
**Anthony Bolgar** *February 06, 2019 02:05*

What is the username did you use?. I can offline activate you. There were a few glitches with the email server at the beginning.




---
**Stephen Kongsle** *February 06, 2019 04:25*

**+Anthony Bolgar** username: kongorilla


---
**Ned Hill** *February 06, 2019 04:28*

I activated you **+Stephen Kongsle**


---
**Stephen Kongsle** *February 06, 2019 04:52*

Thanks, guys!


---
**krystle phillips** *February 07, 2019 20:49*

Ok, I created my account. Thanks for making this space available.




---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/cEeVfSAUTrZ) &mdash; content and formatting may not be reliable*
