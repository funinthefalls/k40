---
layout: post
title: "So glad that I found this group - - the strangest thing happened today as I was calibrating my new k40"
date: June 22, 2017 05:59
category: "Original software and hardware issues"
author: "Joe Papa"
---
So glad that I found this group - - the strangest thing happened today as I was calibrating my new k40. Unit powered on ok, I got good test pulses and everything was looking promising. The tube cover was open as I was tweaking the back mirror, but I slipped and the cover slammed shut. As soon as the tube cover slammed shut, the power to the machine cut out and it won't turn back on. I checked all of the switches with a multimeter, the switches are still good. getting 110V from the wall, tracing it through the main knob switch, where it hits the power supply at 110V. Green light on power supply board is off. The two wires that connect the power supply to the 'secondary' switch aren't reading any voltage. What the heck happened? I mean, it sounds like the power supply fried, but none of the components seem burnt, and I don't understand how the back cover could have even affected that. Any suggestions from the people who are wiser than me on this unit? 





**"Joe Papa"**

---
---
**Alex Krause** *June 22, 2017 06:51*

Some k40 machines have a fuse built into the 110v socket in the back of the machine where you plug it in check there... 


---
**Don Kleinschnitz Jr.** *June 22, 2017 12:30*

**+Joe Papa**

<b>LPS dead after cover slams:</b>



<b>Warning the K40 LPS contains lethal voltages! Do not attempt to test or repair this supply if you are not trained and experienced with High Voltage circuitry!</b>

....................

 

if:

...the green led down on the LPS is not lit

AND 

...the fan is not turning

 AND 

...the AC input to the supply is present

THEN

 ...the laser supply is not working.



1. Verify the conditions above

2. Post a picture of the supply and the place where you measured the AC input.

3. How old and used is the machine?

4. How do you know that; "but none of the components seem burnt" did you open the power supplies case? If you opened the supply post a picture pls. 



Things you can do if the LPS has AC power but no output: 

... check the axial fuse on the PCB with DVM on ohms to see if it is blown.



For now we might assume the cover slamming is a coincidence until we find a fault. 


---
**Joe Papa** *June 22, 2017 16:00*

I think you nailed it Don - the fuse by the power cord was good, as voltage was passing through the switches. I checked the on board fuse and sure enough it doesn't pass continuity! first ill answer your numbers then ill post the pictures:



1) green LED is off, Fan is not turning, AC input is present (marked A in image below) and output to secondary switch is NOT working (marked B in image below). Continuity on fuse (C) fails. 

2)attached

3)brand new

4)I opened the supply cover but it does appear that the fuse is charred on closer inspection

![images/ca7a68ba62ecb38f28538301db0261a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca7a68ba62ecb38f28538301db0261a2.jpeg)


---
**Joe Papa** *June 22, 2017 16:02*

do you know the specs of the onboard fuse so I can solder/replace it? if this is blow, will there be something else triggering it to blow/is it worth sending it back for a new supply replacement?


---
**Don Kleinschnitz Jr.** *June 22, 2017 18:33*

**+Joe Papa** the specs for parts are on my blog. The fuse is 5A.

Usually the fuse blows because the bridge rectifier shorts, and that fails independently or as result of shorted low voltage switcher. 

If you can I would get a replacement but we can keep working this in the meantime. 

I'm mobile so I will add details later.


---
**Don Kleinschnitz Jr.** *June 23, 2017 03:41*

**+Joe Papa** Here is the post:



[donsthings.blogspot.com - K40 LPS repair and test](http://donsthings.blogspot.com/2017/01/k40-lps-repair-and-test.html)


---
**Don Kleinschnitz Jr.** *June 23, 2017 03:49*

**+Joe Papa** Here are  pictures of the parts that usually go bad and blow the fuse. However the failure can come from other causes. It bothers me that this is a new supply and failed already.



[https://goo.gl/photos/4hRKryK6VjdRyjtP7](https://goo.gl/photos/4hRKryK6VjdRyjtP7)



If you decide to attempt repair we can go into more detail on how to test the parts. Or you can try just replacing them.



![images/e56855aeb1aa3bcd9b38aa29de5d5fda.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e56855aeb1aa3bcd9b38aa29de5d5fda.jpeg)


---
**Joe Papa** *June 23, 2017 14:50*

awesome resources - thank you! I finally heard back from the seller today and they're sending me a new power supply... I'll hook it up and post back when it's in. But I have a feeling I'll need this post in the future... haha


---
*Imported from [Google+](https://plus.google.com/117348868431446199890/posts/WSNfUX8zNuE) &mdash; content and formatting may not be reliable*
