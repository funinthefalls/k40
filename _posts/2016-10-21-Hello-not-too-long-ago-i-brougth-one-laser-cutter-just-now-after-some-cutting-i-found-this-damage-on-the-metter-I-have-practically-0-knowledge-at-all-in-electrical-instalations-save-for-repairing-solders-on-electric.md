---
layout: post
title: "Hello, not too long ago i brougth one laser cutter, just now after some cutting i found this damage on the metter, I have practically 0 knowledge at all in electrical instalations save for repairing solders on electric"
date: October 21, 2016 06:50
category: "Discussion"
author: "4Design Media"
---
Hello, not too long ago i brougth one laser cutter, just now after some cutting i found this damage on the metter, I have practically 0 knowledge at all in electrical instalations save for repairing solders on electric guitars, but I read this machines are to be respected on this regard. The cutter works as if nothing, should i join it back?





**"4Design Media"**

---
---
**4Design Media** *October 21, 2016 06:52*

I forgot to attach the photo, this is tge troubling connection

![images/be8dfab91e931c1ec067d118f0eef4b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be8dfab91e931c1ec067d118f0eef4b3.jpeg)


---
**Alex Krause** *October 21, 2016 07:55*

The mA guage is on the high voltage side of the the psu... it's understandable to be cautious 


---
**4Design Media** *October 21, 2016 10:18*

Yep, I did some homework before buying, so I just want to be sure about it, so sould I just fix it back, nothing else seems out of shape


---
**Don Kleinschnitz Jr.** *October 21, 2016 20:25*

I would fix it as this is the route for the tube current to ground. If it opens you will have problems. Certainly do it with the machine unplugged even though this side of the tube is low voltage and current.


---
**4Design Media** *October 21, 2016 20:29*

Ok thanks, I'll be fixing it rigth away


---
**4Design Media** *October 21, 2016 22:14*

Fixed it, I hope. Thanks a lot. I hope to learn alot from this group

![images/ee098e20c7d0c291069c776f2c555301.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ee098e20c7d0c291069c776f2c555301.jpeg)


---
*Imported from [Google+](https://plus.google.com/103483675804177075286/posts/4yzQ99Lsdj2) &mdash; content and formatting may not be reliable*
