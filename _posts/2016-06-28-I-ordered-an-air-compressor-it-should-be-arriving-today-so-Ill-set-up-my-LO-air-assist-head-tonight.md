---
layout: post
title: "I ordered an air compressor ( ), it should be arriving today, so I'll set up my LO air assist head tonight"
date: June 28, 2016 15:41
category: "Air Assist"
author: "Tev Kaber"
---
I ordered an air compressor ([https://www.amazon.com/gp/product/B002JPRNOU/](https://www.amazon.com/gp/product/B002JPRNOU/)), it should be arriving today, so I'll set up my LO air assist head tonight. Any tips on best setup?  Will I need some kind of valve to regulate airflow?





**"Tev Kaber"**

---
---
**Evan Fosmark** *June 28, 2016 15:49*

I have the 60w version of this pump, and I wish it was a bit more powerful. If you don't have a drag-chain, consider getting some self-retracting poly tubing [http://www.mcmaster.com/#9148t125/=131ula7](http://www.mcmaster.com/#9148t125/=131ula7) That's what I use and it works well.



Note that it's loud and vibrates quite a bit, so you can't keep it on the same structure as the laser. Also it has no direct on/off switch. When I run it I'm currently plugging/unplugging it (about to buy an inline switch, though).



With that said, it makes a world of difference and is certainly worth it!


---
**Tev Kaber** *June 28, 2016 16:00*

Yeah, I also got that self-retracting tubing to use with it.  No on/off switch isn't a problem, since I have a switch panel for all my laser components (I power them on left-to-right, which has a satisfying "powering up the death star" feel).


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/h3nT9PB24UC) &mdash; content and formatting may not be reliable*
