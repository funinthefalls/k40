---
layout: post
title: "Anyone who is waiting for Smoothieboards, I contacted Mr Wolf, and he responded: Hey"
date: March 31, 2016 09:13
category: "Discussion"
author: "Scott Marshall"
---
Anyone who is waiting for Smoothieboards, I contacted Mr Wolf, and he responded:



Hey.



The new batch is ready at the factory, it should ship in a few days.

Then a few days more to travel.

Then a few days more in customs ( up to a week )

Then Uberclock will be in stock again.



Cheers !



This is as of 03/31/16





**"Scott Marshall"**

---
---
**Jim Hatch** *March 31, 2016 12:05*

Well that translates into about a month. Are they going to have the full line or just the high-end 5 stepper version?


---
**Scott Marshall** *March 31, 2016 14:31*

It sounds to me like the full line, but that's the response as it came to me.



The 5 axis is only about $50 more than the 3 axis, and has a lot more I/O, Personally I'd buy the 5x if I could get it. Better to have extra capability than not enough. By the time you buy the motor drivers and FETs to add on later, it'll cost you at least the $50 and then you have to do the work.

Another advantage of extra is if you ever blow an output, you can just switch over, re-map, and your up and running again.

That's my philosophy anyway.



Scott


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/ZhGmBnqLkDH) &mdash; content and formatting may not be reliable*
