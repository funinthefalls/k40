---
layout: post
title: "just brought a second hand 50 w 500mm x 300mm laser cutter that i need the driver for corel draw and a manual,can anyone help"
date: March 27, 2017 07:46
category: "Discussion"
author: "Phillip Conroy"
---
just brought a second hand 50 w 500mm x 300mm laser cutter that i need the driver for corel draw and a manual,can anyone help  

![images/5e02ea733764e0d18137056730a16052.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5e02ea733764e0d18137056730a16052.jpeg)



**"Phillip Conroy"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 09:30*

Wouldn't happen to be the same one that comes with the K40 would it?


---
**Phillip Conroy** *March 27, 2017 09:58*

Upgraded to corel draw x7 ,found a driver ,need a manual for machine,maim board is dsp controller and can cut files from usb stick,computer usb and nework


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 27, 2017 10:17*

**+Phillip Conroy** Good news. I wish you good luck getting it to run.


---
**Nigel Conroy** *March 27, 2017 12:25*

**+Phillip Conroy**

Nice name....

I have the same machine. Here's a shared googe drive folder with all the manuals that I received

I call it a K50 but seems to be referred to as a 350 in the manuals.



[drive.google.com - K50 Manuals - Google Drive](https://drive.google.com/drive/folders/0B_KUNLUaXRMoa2ZhN1hvZWNqVEE?usp=sharing)



Let me know if I can help with anything.




---
**Alex Krause** *March 27, 2017 16:04*

You need Rdworks


---
**Phillip Conroy** *March 27, 2017 19:00*

overwhelmed by the dsp control,coming from a k40  i think it will take awhile to learn  


---
**Nigel Conroy** *March 27, 2017 19:14*

Did you get the latest version of RDworks?

Looks like it's at 8.01.22


---
**Stephane Buisson** *March 28, 2017 10:11*

how does the PSU look like ? Smoothieable ?


---
**Phillip Conroy** *March 29, 2017 02:58*

Go rdworks plug in to work with corel draw x7,can cut ok just haven't figured out etching


---
**Phillip Conroy** *March 29, 2017 09:01*

photos 

![images/b7f95923ce57cbe35d2d64e1c8774840.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b7f95923ce57cbe35d2d64e1c8774840.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:01*

![images/451c06d6003a3769f695314666e18c47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/451c06d6003a3769f695314666e18c47.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:01*

![images/e0a9075f3df26de2e93a2f76d5633128.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0a9075f3df26de2e93a2f76d5633128.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:02*

one of the better 50 watt blue boxes has network which works yea

![images/04823a866c4b1f9d3f6fac9a2db33bfc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/04823a866c4b1f9d3f6fac9a2db33bfc.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:02*

![images/45c9befc2feb5f0e61e39fb293d43c76.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/45c9befc2feb5f0e61e39fb293d43c76.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:03*

![images/f022cd949dbcb92dd816c90cbcd2d75e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f022cd949dbcb92dd816c90cbcd2d75e.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:03*

![images/0f54b1c196d1508865992e0232de749f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f54b1c196d1508865992e0232de749f.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:03*

![images/2f3fe435bc00875a9c5f3e293954c505.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f3fe435bc00875a9c5f3e293954c505.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:04*

![images/1626fd0517df4734f5538ed5dbb05fa4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1626fd0517df4734f5538ed5dbb05fa4.jpeg)


---
**Phillip Conroy** *March 29, 2017 09:08*

all ready installed a amp meter as do not trust % ,turns out 100% is 24ma  so somebody has set power supply to this limit.Etches twice as fast as the k40 as etches with laser head moving both left and right,Down side is as laser cutter has been sitting for over 6 months tube is down to only 38 watts-i have read they where never 50 watts anyway......Dsp controller was scary for the first 2 days now i love it 

![images/0aaa91d674d77c18a90aa16ee369a8fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0aaa91d674d77c18a90aa16ee369a8fd.jpeg)


---
**Rick Tennyson** *March 30, 2017 20:38*

I want to upgrade too, but I just cant afford to be down if I cant figure it out. 


---
**Phillip Conroy** *March 31, 2017 09:00*

I have figured it out ,with no manuals,or software that came with machine ,was worried I made a mistake, now 2 days later will never look back.dsp controller so much better than k40 stock board,


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/EGAosB6dWUC) &mdash; content and formatting may not be reliable*
