---
layout: post
title: "I'm having a strange problem with what I think is a loss of steps that I'd love input on"
date: July 22, 2018 13:49
category: "Discussion"
author: "ab1244"
---
I'm having a strange problem with what I think is a loss of steps that I'd love input on.



I have a K40, with SBASE board running the newest Smoothieware.

I'm feeding the laser with Visicut, mostly with files from Inkscape.

I set this up a while ago, made a few projects, then left it alone until recently. I had a weird behavior back then that I thought might be due to a weird input file. 



I've recently had a need to do some more laser projects an am running into the same problem again.



From what I can tell, I'm losing X-axis steps, but in an inconsistent manner. Here are two photos that I think help. The first photo shows some successes to the left. The complicated text seems to cut out fine, wherever that file is in the laser bed. The squares were simply cutting the same file twice to be able to get through this plywood. To the right, you can see me cutting the same square a few times (ie, a new job from Visicut each time) and it is losing x-axis position. Trying to run a more complicated file (right photo) shows that it loses x-axis position even within the same file.



The laser head moves easily. The file with lots of curves and short bursts runs fine (as do other similar files, even when large ~6inches), anywhere in the bed.  Changing the speed in Visicut doesn't seem to effect much.



Any suggestions?

thanks

mike









![images/cf55282f05abdb4535a6c14a5ea2f2fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf55282f05abdb4535a6c14a5ea2f2fb.jpeg)
![images/0d9d813e9c361a514855d262b062ab06.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0d9d813e9c361a514855d262b062ab06.jpeg)

**"ab1244"**

---
---
**Stephane Buisson** *July 22, 2018 13:56*

possible mechanic issue on 1 axe only,

check if the screw holding the poulie doesn't come loose. at the the same time look the belts tension.


---
**ab1244** *July 22, 2018 15:16*

Thanks Stephane. I put a mark across the shaft and pulley, it doesn't move so that is not slipping. The belt tension seems okay to me. If I slow things way down, I don't lose steps, so your intuition might be correct. I'll keep poking at it.


---
*Imported from [Google+](https://plus.google.com/111075041458030923509/posts/CMySSdLWdeL) &mdash; content and formatting may not be reliable*
