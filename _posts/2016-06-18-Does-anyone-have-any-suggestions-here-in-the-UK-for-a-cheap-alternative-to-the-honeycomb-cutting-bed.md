---
layout: post
title: "Does anyone have any suggestions, here in the UK, for a cheap alternative to the honeycomb cutting bed?"
date: June 18, 2016 07:41
category: "Modification"
author: "Pete Sobye"
---
Does anyone have any suggestions, here in the UK, for a cheap alternative to the honeycomb cutting bed? Anyone tried mesh etc??





**"Pete Sobye"**

---
---
**Anthony Bolgar** *June 18, 2016 07:57*

I use expanded steel mesh, got it at my local home improvement stor for $10, was large enough to make two grills for the K40


---
**Stephane Buisson** *June 18, 2016 08:26*

that's arrive flat in a envelope, you just pull on it to unfold. make yurself a frame if you want.

[http://www.ebay.co.uk/itm/10mm-thick-cell-6-4mm-Honeycomb-aluminium-beds-Laser-machine-honey-comb-/141759884244?var=&hash=item21018c2bd4:m:mxp2yfJ1CTsxskQNXmPjXCQ](http://www.ebay.co.uk/itm/10mm-thick-cell-6-4mm-Honeycomb-aluminium-beds-Laser-machine-honey-comb-/141759884244?var=&hash=item21018c2bd4:m:mxp2yfJ1CTsxskQNXmPjXCQ)


---
**Jim Hatch** *June 18, 2016 13:48*

Or make yourself a nail bed - simple finishing nails driven into a piece of plywood or MDF. If you have an air or power nailer it's 10 minutes work.



BTW, nail beds make for great standoffs for when you're finishing a piece for staining or poly. I use pin nails (23ga) beds I've made from 1/4" ply and 1" pins. Pins are great for fastening wood laser material together - in the U.S. you can find a pin nailer for under $30 at the big box home centers or Amazon.


---
**Phillip Conroy** *June 18, 2016 21:09*

I use 10mm magnets and 65mm screws to hold up my 3mm mdfthat i mainly cut ,so far have only needed to make 10 of these to hold my [work.it](http://work.it) all depends on what you are cutting/engraving, if work is thin ie paper than a bed that surports it is needed.however as mdf is very dirty when cutting it a honeycomb bed would get very dirty/covered in the mdf cutting fumes /glue and would neeed cleaning often.i also have a tin removable tray incutting bed that needs cleaning inhot water weekly as sticy gunk builds up from cutting mdf.

The advantages of magnets/screw combos are that i can move them around to suitthe work and that i can quicky change height if i want by useing shorter screws.


---
**Jim Hatch** *June 18, 2016 23:16*

**+Phillip Conroy** The magnet/screw combination is a great idea. Thanks!


---
*Imported from [Google+](https://plus.google.com/101703286870399011779/posts/2BRoXk4tD9a) &mdash; content and formatting may not be reliable*
