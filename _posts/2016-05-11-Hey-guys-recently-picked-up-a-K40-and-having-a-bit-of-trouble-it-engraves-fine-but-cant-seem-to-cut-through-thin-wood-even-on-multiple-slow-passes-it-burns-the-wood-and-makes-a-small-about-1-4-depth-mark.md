---
layout: post
title: "Hey guys, recently picked up a K40 and having a bit of trouble - it engraves fine but can't seem to cut through thin wood, even on multiple slow passes - it burns the wood and makes a small (about 1/4 depth) mark"
date: May 11, 2016 22:01
category: "Hardware and Laser settings"
author: "Jamie Martin"
---
Hey guys, recently picked up a K40 and having a bit of trouble - it engraves fine but can't seem to cut through thin wood, even on multiple slow passes - it burns the wood and makes a small (about 1/4 depth) mark.



However, the main issue at the moment is the power of the laser at the end of the rails - I can be engraving something (in this case, a photo frame) and it will do one side perfectly whilst it barely marks the other side - is this an alignment issue? Alignment looks to be ok when I place masking tape over the hold on the laser head and fire, they line up relatively well in all 4 corners of the axis, however power is noticably down in the furthest corners - any advice?





**"Jamie Martin"**

---
---
**Greg Curtis (pSyONiDe)** *May 11, 2016 22:21*

Dust or damage on the mirrors would be my first guess. Any dust/debris on the mirrors would be likely to burn and damage them though, so clean them and check that they still look smooth.



Then double check your alignment and try again.



I check my mirrors before each use, but I'm not using it every day.


---
**Jean-Baptiste Passant** *May 12, 2016 06:46*

Alignment for sure ;)



Square the bed and align the mirror.



Check again, it will cut through it like cheese :)


---
**Jamie Martin** *May 14, 2016 19:11*

Just wanted to  update you all on this - it was indeed alignment, spent an hour going back over it all and it cut through the wood I was using for testing on the first pass - solid power across the whole bed now and cuts and engraves on a much lower power setting - thanks for all the help, I'm sure I'll have more questions as I slowly make changes and improve things!


---
*Imported from [Google+](https://plus.google.com/111997191311797045379/posts/bWmHZmNnDQb) &mdash; content and formatting may not be reliable*
