---
layout: post
title: "Im adding a crosshair laser and air assist to my K40 and was wondering what you guys do to mount the wire and air hose so that it moves freely with the head and not bind"
date: November 23, 2016 02:04
category: "Modification"
author: "Bob Damato"
---
Im adding a crosshair laser and air assist to my K40 and was wondering what you guys  do to mount the  wire and air hose so that it moves freely with the head and not bind. Any ideas or pictures?

Thank you!







**"Bob Damato"**

---
---
**Ashley M. Kirchner [Norym]** *November 23, 2016 02:39*

Cable chain, sometimes also called a drag chain.

![images/a379b76951b569cabb0ac1a1f3b7a88e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a379b76951b569cabb0ac1a1f3b7a88e.jpeg)


---
**Paul de Groot** *November 23, 2016 05:44*

Indeed just 3d printed a drag chain for my mini fan which is my version of air assist 😁

![images/bfd65254a35ec72b0285ec5a6396475c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bfd65254a35ec72b0285ec5a6396475c.jpeg)


---
**Bob Damato** *November 23, 2016 12:45*

Thank you everyone!




---
**Kelly S** *November 23, 2016 23:00*

I also use a cable chain that runs my air line and power for my red dots.  Works really well. 

![images/ae62970c0ab59b6d3d27462164223883.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae62970c0ab59b6d3d27462164223883.jpeg)


---
**Kelly S** *November 24, 2016 17:45*

With the pointers installed.

![images/121ab0e7d6062486a4e7b3e74ef8879f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/121ab0e7d6062486a4e7b3e74ef8879f.jpeg)


---
**Bob Damato** *November 24, 2016 19:34*

Thanks Kelly. Thats just what Im looking for.  I ordered a drag chain from fleabay, so when that comes in Ill give an update.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/AYZya6U3xZU) &mdash; content and formatting may not be reliable*
