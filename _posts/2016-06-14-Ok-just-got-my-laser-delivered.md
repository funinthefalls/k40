---
layout: post
title: "Ok just got my laser delivered. :)"
date: June 14, 2016 17:06
category: "Original software and hardware issues"
author: "Ned Hill"
---
Ok just got my laser delivered. :).  First issue that jumps out at me is that the x-axis appears to be out of square. :( Thoughts?

![images/09c611c39e25151d3d10c05e517cf421.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09c611c39e25151d3d10c05e517cf421.jpeg)



**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 14, 2016 17:19*

It could be that it has somehow slipped out of alignment during the shipping. Check all the screws on both ends of that rail. Check if you can move it to "square" position by hand & if there is something causing it to be in that odd position. Might be worth pulling it all out & seeing if you can square it up. It's actually a very simple process (only 4 bolts if I recall correct). Looked daunting to me the first time around but I was surprised at how easy it was.


---
**Ned Hill** *June 14, 2016 17:42*

**+Yuusuf Sallahuddin** Thanks, got it out and figured out it was the position of the right hand rail guide.  Slipped the belt down a few notches on the drive and it's good now. :)


---
**Pete Sobye** *June 14, 2016 19:03*

Got mine yesterday and it was like yours, askew. I disassembled the bed and frame. I had to open up the holes in the frame and chassis to give me enough movement to square it up. 


---
**Stephen Sedgwick** *June 14, 2016 21:03*

I haven't corrected mine yet, I have done like 2 burns/raster images on it... like that - but I am replacing all the controllers so haven't worried about it too much.  Can you please give more detail on the fix?  (I am sure I can figure it out however others I am sure will enjoy it also)


---
**Scott Marshall** *June 14, 2016 21:14*

There's 4 screws thru a plate the tie the rail into the Y carriage. They're in slots so you can adjust the position, Use a good square and get it squared up well, test  and then take the screws back out 1 at a time and put  blue loctite on them. Careful, they are cheap screws with phillips heads, you'd ahead of the curve if you bought 4 nice socket button head screws.I believe they are 3mm, but may be 2.5mm.  Don't tighten too much, the threads aren't very good either.



The Y axis is driven from the right side, there is a bracket there with the belt attached, and 2 guide wheels which run in a track down in the narrow crack between the right wall and frame..

It's screwed to the end of the X rail and the screws are inaccessable without removing the whole XY assembly. Not as bad as it sounds, But does require mirror realignment. You probably won't have to go there.


---
**Ned Hill** *June 15, 2016 00:54*

**+Stephen Sedgwick** I basically did what Scott said in his second paragraph.  After removing the carriage I loosened the drive belt using the screw on the back end and then square up the right side with the left and tightened the belt.


---
**Christina Marie** *June 22, 2016 01:38*

mine came in like this and I'm not understanding??


---
**Ned Hill** *June 22, 2016 01:54*

The on the right side the x-axis rail is attached to the timing (drive) belt which loops around the x-axis stepper motor at the front right.  The right side of the x-axis rail needs to slide the belt around the motor pulley without the pulley turning to line up with the left side which is attached to a slide rail. On the back right edge of the carriage frame there is a screw that needs to be turned to loosen the belt so it can slide over the teeth of the motor pulley. Unfortunately you need to remove the whole carriage frame to get to it.  This maybe problematic because of the protruding air vent.  See my other post here [https://plus.google.com/108257646900674223133/posts/TWpTcM9gQeY](https://plus.google.com/108257646900674223133/posts/TWpTcM9gQeY) on how I got it out and in one of my comments there is a link to a pic of the belt tensioner  you need to get to.


---
**Ned Hill** *June 22, 2016 01:57*

Here is the pic of the belt tensioner on my machine. [https://www.dropbox.com/s/5ydakcuro4xwdxi/20160615_133834.jpg?dl=0](https://www.dropbox.com/s/5ydakcuro4xwdxi/20160615_133834.jpg?dl=0)


---
**Ned Hill** *June 22, 2016 02:12*

If the right side is higher than the left, and there is a bit of slack in the belt, you maybe able to pull the rail all the way forward and then just pull on the right side to get the teeth of the belt to pop over the teeth of the motor to get it aligned.  Warning, you could potentially damage the belt doing this so proceed with caution.  I actually pulled mine one pop down before deciding to bite the bullet and remove the frame because I wanted to remove the air vent anyway.


---
**Christina Marie** *June 23, 2016 01:22*

yep i gave it a good tug and popped it in place!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/jJPKAqhV4JU) &mdash; content and formatting may not be reliable*
