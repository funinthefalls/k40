---
layout: post
title: "Chuck Comito I moved this project ...."
date: November 09, 2017 12:15
category: "Modification"
author: "Don Kleinschnitz Jr."
---
#K40AirAssist



**+Chuck Comito** I moved this project ....



[https://plus.google.com/117314700387764776689/posts/jE3WeGwaaEM](https://plus.google.com/117314700387764776689/posts/jE3WeGwaaEM)

 

.... to this forum as it seems more appropriate for this group.



.......................

Here is my first shot at the air assist solenoid driver design.

<b>WARNING! I have not done any testing or checking of this design yet!</b>



<b>Solenoid:</b>

I am in the blind on the solenoids power rating. Some solenoids are designed for intermittent operation and some for continuous duty. I have no way of telling what this ones rating is. Depending on how long [cutting time] its on it may get hot, it may eventually fail. We will just have to test it. Mounting it to a heat sync or metal surface can help.



<b>Schematic:</b>

The attached schematic has the guts of the KNACRO and the external connections laid out. This includes an inline fuse.



<b>Smoothie connection:</b>

We need to decide exactly how you will connect this to the smoothie.



There are two approaches:

1.) The input is supposed to handle >3V <20V. So you could connect to a 3.2v port on a header somewhere. I think this approach is marginal and I do not like driving 3.2v directly from the processor and off the board. I have seen this work however.



2.) IMO the best approach is to use an OPEN DRAIN [[http://www.microcontrollertips.com/what-is-an-open-drain/](http://www.microcontrollertips.com/what-is-an-open-drain/)]

 connection using one of the small MOSFETS. This setup will turn on the opto-coupler's LED to fire the solenoid. However the <i>IN</i> pin needs a DC voltage and 24vdc is to high for this setup without modifications. We can add a resistor in series with the <i>IN</i> pin to get the operating point correct. I do not know what current LED (L1) will handle so we may have to remove and short it out. Haven't done any calcs yet on the 817 input.



Let me know how you want to connect to the Smoothie and I will calculate the added resistor etc.



[https://www.digikey.com/schemeit/project/knacro-mosfet-trigger-module-K0OMDHG301RG/](https://www.digikey.com/schemeit/project/knacro-mosfet-trigger-module-K0OMDHG301RG/)









**"Don Kleinschnitz Jr."**

---
---
**Chuck Comito** *November 09, 2017 12:23*

Hey **+Don Kleinschnitz**​, I thought you weren't going to get to this for a few days! Lol. 


---
**Don Kleinschnitz Jr.** *November 09, 2017 12:26*

I was on a roll early this AM ....... to much caffeine!


---
**Chuck Comito** *November 09, 2017 17:54*

Well that works for me. My Amazon order will be here tomorrow so I will start testing asap. I'll have to figure out an open drain on the smoothie and how to configure the firmware but I have a pretty good idea on where to start with that. 


---
**Don Kleinschnitz Jr.** *November 09, 2017 21:56*

**+Chuck Comito** 

My suggestions for testing:



1.) Connect 24vdc across the solenoid and verify it works. If so connect solenoid to ST2.

2.) Connect the diode D1 properly across ST2

3.) Connect +24vdc and 24vdc gnd to the respective pins on the driver power terminal ST3, 

4.) Connect plus 12vdc to the "IN" (VSS). Connect both 12vdc and 24vdc power supply grounds together. 

5.) touch the "Gnd" on ST1 pin to ground..... solenoid should energize by mosfet.



BTW my calcs show that you should be able to use 24vdc on the "IN" pin.



6.) If everything above works then replace the plus 12V with plus 24V and rerun. 



Hope for no fire and smoke ....


---
**Chuck Comito** *November 11, 2017 19:09*

Good news, no smoke. Bad news no solenoid action. I will continue testing and see where I get. I'll keep you posted. 


---
**Chuck Comito** *November 11, 2017 19:43*

**+Don Kleinschnitz**​, works like a charm. Bad diode I guess. Now to test open drain from smoothie. Any suggestions?


---
**Chuck Comito** *November 11, 2017 19:49*

![images/fa6386d19c3417390c9a218a07411978.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa6386d19c3417390c9a218a07411978.jpeg)


---
**Don Kleinschnitz Jr.** *November 12, 2017 03:06*

Good! I will look at smoothie schems and provide suggestion. 


---
**Chuck Comito** *November 12, 2017 14:12*

I think pin 2.7 is a good candidate. 


---
**Don Kleinschnitz Jr.** *November 12, 2017 14:29*

**+Chuck Comito** when you say bad diode what do you mean?

Did you try the input with 24vdc?


---
**Don Kleinschnitz Jr.** *November 12, 2017 15:55*

**+Chuck Comito**  I would use P2.6 as we don't need high current drive and that will leave P2.7 for other high current stuff. 



You can still use P2.7, just substitute its location and name for P2.6 below.

.......

<i>LOL, while reading the schematics I realized that you could probably just drive the solenoid directly from P2.7 ...uggh. You would still need to add the 1N4001 across the output. Although the MOSFET has an internal snubber diode they are often not robust enough for high inductance devices like motors and solenoids.</i>



<i>The advantage of this design however is that the solenoid is isolated from the smoothie's power etc. via the opto-coupler and all high currents are off the board.</i>

<i>Probably overkill but I run all my externals opto-isolated and off the board.</i>

........

I updated the schematic and included an image [click link] of the P2.6's connector location and the circuit inside the smoothie.

[https://www.digikey.com/schemeit/project/knacro-mosfet-trigger-module-K0OMDHG301RG/](https://www.digikey.com/schemeit/project/knacro-mosfet-trigger-module-K0OMDHG301RG/)



To test, on the smoothie board, insure that the "+" of X8 is 24vdc ( I assume your VBB is 24vdc) by measuring it to smoothie ground. 



[https://photos.app.goo.gl/RbElwN5ZtoMFkzce2](https://photos.app.goo.gl/RbElwN5ZtoMFkzce2)



When in operation the "-" of X8 should be the drain of the smoothie MOSFET and provide the ground to ST1 to turn on your external driver.



<b>Smoothie Configuration</b>



In the configuration file you want to assign a M code to that switch.



Here are the "Switch" instructions for the smoothie.

[smoothieware.org - switch [Smoothieware]](http://smoothieware.org/switch)



I have not programmed a switch module yet but I think this is what you will need:



<b>switch.airassist_solenoid.enable         true</b>

<b>switch.airassist_solenoid.input_on_command		M106</b>

<b>switch.module_name.input_off_command		M107</b>



You can change the M command code to whatever you like as long as it does not conflict withe M commands you are using. The M106-107 are typically FAN controls so you should be good.


---
**Chuck Comito** *November 14, 2017 01:14*

Hi **+Don Kleinschnitz**, I'm wired up and ready to go with smoothie. Does the firmware switch function require a defined pin? Something like:

switch.airassist_solenoid.enable         true

switch.airassist_solenoid.output_pin    2.6

switch.airassist_solenoid.input_on_command		M106

switch.module_name.input_off_command		M107



Or is that not needed because we are using the fan and it will default to that pin? That would seem odd to me. Thoughts??


---
**Chuck Comito** *November 14, 2017 01:31*

Ok, everything is almost working. I used the code and schematic posted above: 



switch.airassist_solenoid.enable         true

switch.airassist_solenoid.output_pin    2.6

switch.airassist_solenoid.input_on_command		M106

switch.module_name.input_off_command		M107



Then in laserweb, set up a start gcode of M106 and a end gcode of M107. I then ran a test job and the solenoid fired and I have air pressure.



This is twice I've put the diode in backwards though! :-)



Once the switch function is varified, we could probably safely release this mod..



Looks like M106 turned the solenoid on but it didn't turn it off. Any ideas?


---
**Chuck Comito** *November 14, 2017 01:39*

Probably would help if I renamed the M107 line... "switch.module_name.input_off_command" should be "switch.airassist_solenoid.input_off_command".


---
**Don Kleinschnitz Jr.** *November 14, 2017 04:10*

Work now?


---
**Chuck Comito** *November 14, 2017 04:25*

Yes. I meant to post a status update but forgot to hit post. I turned the gcode slider on for the air assist in lw4 and set the gcode on to M106 and off to M107. After I renamed the switch module it worked great!


---
**Don Kleinschnitz Jr.** *November 14, 2017 15:03*

**+Chuck Comito** Yes! so we are ready for a write up?

Can you share a G Photos album with pictures of your setup :).

I have not done this with LW so some screen shots of setup would be useful. 



Good Job!


---
**Chuck Comito** *November 14, 2017 15:09*

I will do this tonight time permitting but I'll get it  


---
**Chuck Comito** *November 14, 2017 23:10*

I will do that for sure. I have to work tonight so I'll do it as soon as I can. Nice job!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Dt2878DRHuP) &mdash; content and formatting may not be reliable*
