---
layout: post
title: "would anyone care to share how they fitted a plastic drag chain to a k40 for carrying the air assist hose to the nozzle?"
date: June 27, 2016 06:36
category: "Modification"
author: "adrian miles"
---
would anyone care to share how they fitted a plastic drag chain to a k40 for carrying the air assist hose to the nozzle?





**"adrian miles"**

---
---
**Jean-Baptiste Passant** *June 27, 2016 07:00*

This one fit my tube. I screwed it to the top screw of the laser head and to the top screw of the X axis.



Fit in the K40 with no problem.



[http://www.aliexpress.com/item/Free-Shipping-1m-3-28ft-7-x-7-mm-Cable-drag-Chain-Radius-15mm-Wire-Carrier/32464684482.html?aff_platform=aaf&aff_trace_key=e2846ee2a56c49859337f62054a4e047-1467010680089-03338-bAmQVJa2R&sk=bAmQVJa2R%3A&cpt=1467010680089&cv&dp=0&af=cc](http://www.aliexpress.com/item/Free-Shipping-1m-3-28ft-7-x-7-mm-Cable-drag-Chain-Radius-15mm-Wire-Carrier/32464684482.html?aff_platform=aaf&aff_trace_key=e2846ee2a56c49859337f62054a4e047-1467010680089-03338-bAmQVJa2R&sk=bAmQVJa2R%3A&cpt=1467010680089&cv&dp=0&af=cc)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 07:51*

That's a decent sized one at 7mm x 7mm **+Jean-Baptiste Passant**. I've been wanting something nice and small to mount the same way you suggested.


---
**adrian miles** *June 27, 2016 09:24*

i bought the next size up (10 x 15mm) and fitted it using the screw you suggested and at the other end with a screw through the back wall - but at the furtherst point from the mounting position on the back wall it isnt flexible enough.....is that the way you mounted it?






---
**adrian miles** *June 27, 2016 09:25*

**+Jean-Baptiste Passant** is it possible to show a photo?




---
**Jean-Baptiste Passant** *June 27, 2016 09:35*

Here are some photos, sorry for the quality.



[https://1drv.ms/f/s!Akp3KOq2lNXJhNR69sV7gaTNlRPwrQ](https://1drv.ms/f/s!Akp3KOq2lNXJhNR69sV7gaTNlRPwrQ)


---
**adrian miles** *June 27, 2016 09:53*

i pretty much fitted mine like this



[https://www.flickr.com/gp/25376817@N02/TWM9o8](https://www.flickr.com/gp/25376817@N02/TWM9o8)



but it wont flex at to enable the head to move to the extreme right bottom corner




---
**Ned Hill** *June 27, 2016 13:59*

**+adrian miles** Don't attach it to the back wall.  Only attach it to the x rail.  Then you leave enough slack on the right beside the bed for the y-axis movement.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 20:06*

**+adrian miles** What if you moved the mounting point on the back wall more to the right? would that allow it to access top-left/bottom-left/bottom-right/top-right?


---
**adrian miles** *June 28, 2016 11:15*

got it running just had to modify a couple of the links on the plastic chain to enable a sharper bend than normal




---
*Imported from [Google+](https://plus.google.com/110236332044659495622/posts/GyfdEVzn7z2) &mdash; content and formatting may not be reliable*
