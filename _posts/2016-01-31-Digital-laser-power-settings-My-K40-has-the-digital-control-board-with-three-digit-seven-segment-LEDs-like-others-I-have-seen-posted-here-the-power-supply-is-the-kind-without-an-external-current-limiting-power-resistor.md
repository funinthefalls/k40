---
layout: post
title: "Digital laser power settings. My K40 has the digital control board with three digit seven segment LEDs like others I have seen posted here, the power supply is the kind without an external current limiting power resistor"
date: January 31, 2016 20:25
category: "Hardware and Laser settings"
author: "David Richards (djrm)"
---
Digital laser power settings.

My K40 has the digital control board with three digit seven segment LEDs like others I have seen posted here, the  power supply is the kind without an external current limiting power resistor.



I made a table of digital power setting and laser current measured by ammeter in the laser negative lead. Here are the results plotted as a graph. I do not now run the machine at greater then 75 on the digital setting, approx 18 mA. The laser strikes at just below 10, approx 3mA. I measured the current with both a digital multi-meter and moving coil ammeters. 



Different setups could well produce different currents, I don't know and I have nothing to compare against. I'm thinking of making an EHT voltage probe to see what voltage the tube is running at, I've seen an example on the net using a chain of high value resistors. hth David.

![images/4c236fb2fbf0745d241019f0537723e4.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4c236fb2fbf0745d241019f0537723e4.png)



**"David Richards (djrm)"**

---
---
**I Laser** *January 31, 2016 21:53*

I'll be setting my digital machine up this week, so will be interesting to see how close it is to your results.


---
**Stephane Buisson** *February 01, 2016 00:04*

70-75% seem to match the max current recommended by most usual laser tube supplier (18ma). those pot manual or digital (in%) does the same, limit the current max for pwm (as a ceiling value) to protect the life time of the tube.

thank you for sharing.


---
**Sebastian Szafran** *February 01, 2016 07:44*

Thanks a lot, very useful. 


---
**I Laser** *March 18, 2016 10:41*

**+david richards** 

Here's my mA results, it's way out from your's but seems pretty close to my other machine.



[http://s18.postimg.org/xqf8ty66x/image.jpg](http://s18.postimg.org/xqf8ty66x/image.jpg)



apologies for the delay...


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/5MZr6aRaiEt) &mdash; content and formatting may not be reliable*
