---
layout: post
title: "Anyone have a good source for blank engraving stuff?"
date: August 02, 2016 15:07
category: "Object produced with laser"
author: "Bob Damato"
---
Anyone have a good source for blank engraving stuff? Granite pieces, dishes, trophies etc?  I make a lot of stuff for the local race track and finding blanks would be awesome!

Thanks!





**"Bob Damato"**

---
---
**Brandon Smith** *August 02, 2016 15:22*

I am sure there are online resources for blank materials. I find most of my blank material at thrift shops in town, and it is all VERY cheap. There are always tons of glasses and dishes. You also find a lot of blank wood items. Of course, there is always tons of other items that may work as well. Just my $1.05


---
**Bob Damato** *August 02, 2016 15:26*

Thats what Im finding too, lots of good local stuff which is nice.  I kind of needed to step it up a notch with a little nicer things. I do see some online stuff just looking for peoples experiences and recommendations so I dont go with a crappy place. :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 02, 2016 15:45*

I tend to purchase anything I make either locally at cheap shops or actual hardware/material suppliers. I absolutely hate waiting for things to arrive by mail (a snail can almost move faster to Australia).


---
**HalfNormal** *August 02, 2016 19:43*

Local shops will sell or give scraps away. Cabinet and flooring. 


---
**Jim Hatch** *August 03, 2016 02:49*

Craft stores like Michael's in the US as lots of jars, boxes, vases, etc that can be engraved to bump up their uniqueness and express your creative  urges and are useful when completed.


---
**Robi Akerley-McKee** *August 08, 2016 06:01*

I get all my granite from the granite shop across the parking lot...  Free, any scrap in their trailer... Kitchen sinks, plywood, ...  Anything they don't have to haul to the dump saves them money.  I make pattern weights from 4x6" pieces, put the felt with the sticky back on it.  My wife and I love them, my mum loves them, and I make them as gimmes for when we do faires and microcons (we do a steam punk booth). Which is how I burned up a 40w tube...  DO NOT RUN a 40w tube at 15ma for 30 minutes at a time. 10ma seems fine.  I just have to run more passes.


---
**Bob Damato** *August 08, 2016 12:40*

**+Robi Akerley-McKee** Thanks for the tips! Theres a granite counter place not far from me, Ill see if I can take some scraps!  You have pictures posted anywhere on what you make? Id love to see them.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/H8Hh4qxyAkk) &mdash; content and formatting may not be reliable*
