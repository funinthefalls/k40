---
layout: post
title: "Another question, this time about the protective film on acrylics: do folks remove that, one side, both sides, leave it on, remove after cutting?"
date: September 29, 2015 03:01
category: "Materials and settings"
author: "Ashley M. Kirchner [Norym]"
---
Another question, this time about the protective film on acrylics: do folks remove that, one side, both sides, leave it on, remove after cutting? I assume for engraving, removing it is a must. Or at least on the engraving side.



The reason I ask is because the materials I buy from places like Inventables, come with a paper protective sheet, however the cast stuff I get from my office, some of it has a plastic film on. So far I've only been removing it from the top side while leaving the bottom side till it's done. Then I peel it off.





**"Ashley M. Kirchner [Norym]"**

---
---
**David Wakely** *September 29, 2015 07:02*

You are correct, most people will get the best results by removing the top layer but leaving the bottom layer intact. If you leave the top layer on you risk fire. Especially if it's paper.


---
**Joey Fitzpatrick** *September 29, 2015 13:37*

if it is plastic and its left on, the plastic melts into the engraving causing bubbles.  If you peel it off, the engraving will look better. there will be a lot of engraving particles(dust) left on the surface.  This will clean off easily with a little "Elbow Grease"


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/K73a1r598Bx) &mdash; content and formatting may not be reliable*
