---
layout: post
title: "Day of the dead rose Skull at about 300DPI"
date: October 20, 2016 18:21
category: "Object produced with laser"
author: "Nick Williams"
---
Day of the dead rose Skull at about 300DPI

![images/54d9ede4e31c39195e39cefa81dc891e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/54d9ede4e31c39195e39cefa81dc891e.jpeg)



**"Nick Williams"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 20, 2016 21:20*

That looks great. Love the border around it too.


---
**Jose A. S** *October 22, 2016 05:32*

Could you share the design with us?


---
**Nick Williams** *October 22, 2016 16:43*

**+Jose A. S**  Sorry, this image was downloaded from Shutter Stack so I can't share the photo directly, but I will post a link if to the original image.

[http://www.shutterstock.com/pic-394219159.html](http://www.shutterstock.com/pic-394219159.html)



Cheers, 



[shutterstock.com - Vector Black And White Tattoo Skull Roses Illustration - 394219159 : Shutterstock](http://www.shutterstock.com/pic-394219159.html)


---
**Juan Diaz** *November 15, 2016 03:32*

whats the DPI mean??


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/TxyLSrEjuj7) &mdash; content and formatting may not be reliable*
