---
layout: post
title: "MY MKS SBASE smoothie install Here is the wiring diagram I put together that shows how my setup is wired to the SBASE"
date: July 26, 2016 17:51
category: "Smoothieboard Modification"
author: "David Cook"
---
MY  MKS  SBASE smoothie install 



Here is the wiring diagram I put together that shows how my setup is wired to the SBASE.   also here is my smoothie config file.



Config file has the following statements:

1. stepper currents set to 0.6 for Y and 0.5 for X axis

2. total laser power limited to 0.8

3. Endstops setup to home to bottom left 0,0  ( I 3D printed a bracket to relocate my Y-Axis optical endstop.





WIRING DIAGRAM    [https://drive.google.com/open?id=0ByNecjb0RD-TOF9jYkNJbW5Xc3c](https://drive.google.com/open?id=0ByNecjb0RD-TOF9jYkNJbW5Xc3c)



CONFIG FILE   [https://drive.google.com/open?id=0ByNecjb0RD-TbGROSEVxSXMyajg](https://drive.google.com/open?id=0ByNecjb0RD-TbGROSEVxSXMyajg)



Disclaimer:

This is what I have done to my machine,  I have no idea if it will work on your machine and advise to use this information at your own risk :-)





**"David Cook"**

---
---
**Jon Bruno** *July 26, 2016 19:25*

Aside from your Y stops it's pretty straight forward. Thanks a lot for this! are you using the original endstop but just relocated it? What does your custom bracket look like? (.stl?) ;-)


---
**David Cook** *July 26, 2016 20:13*

**+Jon Bruno**  I am using the original optical end stop.  I suppose I could have gotten it to home to the bottom left by adjusting the config file,  but I'm a mechanical Engineer so I went with the mechanical re-location lol.  that way it worked like my 3D printers do.



I did not post the STL file for the bracket because even after printing it, I had a slight height issue,  so I just took my heat-gun and softened the printed part so I could bend it by hand, then I held it in place until the material hardened back up again lol.   I did not want to re-print the part 


---
**Jon Bruno** *July 27, 2016 12:53*

hah! heat gun adjustment.. love it!.

I've done similar.. especially if it's a long print.

I had printed several copies of a custom intake manifold model for a project I was working on ..14 hrs each print...little tweaks sometimes need to be made,


---
**Damian Trejtowicz** *August 24, 2016 21:23*

what firmware bin was used?

do you have it to share please?


---
**David Cook** *August 24, 2016 22:28*

**+Damian Trejtowicz** I used the latest firmware BIN from the MKS SBASE GIT hub. [https://github.com/makerbase-mks/MKS-SBASE](https://github.com/makerbase-mks/MKS-SBASE)﻿


---
**Damian Trejtowicz** *August 24, 2016 22:30*

Im asking because i think ,only this firmware works with this board.i try newest from smoothie github and its not working with this board


---
**David Cook** *August 25, 2016 00:20*

**+Damian Trejtowicz**  yes I have experienced the same,  I almost gave up on it because I could not figure out I needed the actual MKS Sbase BIN,,,    but this is the price we paid for saving a little cash  ;-)   hind sight..  I totally would have rather bought a legit Smoothie board for the wealth of open support I would have gotten.


---
**Damian Trejtowicz** *August 30, 2016 05:26*

All works now ,except on 100% power i get more like 28mA.i need set up max 60% of power ,how to lover this in config? Is pwm frequency(i have 200 ) need to be adjusted?


---
**Jon Bruno** *August 30, 2016 13:54*

afaik laser_module_pwm_period  should be set to 20.. thats what Ive seen in examples before..  I questioned the setting of 200 just the other day but haven't had a response yet. But yes I also see 25+mA on 100% (watching)


---
**David Cook** *August 30, 2016 15:53*

**+Damian Trejtowicz** in the config file you need to set max power there.  mine is set to 0.8   so basically 80% max achievable power.



laser_module_maximum_power                   .8             # this is the maximum duty cycle that will be applied to the laser


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/2MJJwuVHo9S) &mdash; content and formatting may not be reliable*
