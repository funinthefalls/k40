---
layout: post
title: "I made these mods to my K40 at the begining of the summer but never made a blog entry:"
date: September 14, 2018 16:36
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I made these mods to my K40 at the begining of the summer but never made a blog entry:









**"Don Kleinschnitz Jr."**

---
---
**James Rivera** *September 16, 2018 01:07*

**+Don Kleinschnitz Jr.** I'm looking at the mods you've done and I'm thinking to myself: I wonder if it is cheaper to just buy an expensive laser will all these already built in?  ;-)


---
**Don Kleinschnitz Jr.** *September 16, 2018 03:04*

**+James Rivera** First, to my knowledge most machines do not have all these meters. 2cnd I am way below the $4+K for a serious production laser will all the adds I have made :).


---
**crispin soFat!** *September 16, 2018 22:02*

It looks awesome and we're all richer for your improvements, thanks for sharing your work!


---
**James Rivera** *September 17, 2018 02:44*

**+crispin soFat!** Agreed!


---
**salvatore sasakingsoft** *October 03, 2018 20:08*

ciao mi sai dire che cinghia monta la laser k40 ?


---
**Don Kleinschnitz Jr.** *October 04, 2018 11:02*

**+salvatore sasakingsoft** what straps are you referring to?


---
**salvatore sasakingsoft** *October 04, 2018 11:17*

Yes 


---
**Etched NI Bespoke Handcrafted Gifts** *October 08, 2018 13:46*

Are these temp cut outs from a refrigeration unit?


---
**Don Kleinschnitz Jr.** *October 08, 2018 14:08*

**+Etched NI Bespoke Handcrafted Gifts** no they are controllers I bought on amazon.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/LbAz4U3c5nK) &mdash; content and formatting may not be reliable*
