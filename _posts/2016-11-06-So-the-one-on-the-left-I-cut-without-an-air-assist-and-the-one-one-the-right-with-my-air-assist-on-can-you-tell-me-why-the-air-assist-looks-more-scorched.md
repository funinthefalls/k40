---
layout: post
title: "So the one on the left I cut without an air assist and the one one the right with my air assist on can you tell me why the air assist looks more scorched?"
date: November 06, 2016 22:58
category: "Discussion"
author: "3D Laser"
---
So the one on the left I cut without an air assist and the one one the right with my air assist on can you tell me why the air assist looks more scorched?

![images/55a55ea2eca93b331e84527a04f7194d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/55a55ea2eca93b331e84527a04f7194d.jpeg)



**"3D Laser"**

---
---
**Cesar Tolentino** *November 07, 2016 00:42*

I think it's the smoke being forced down by the air assist. Can easily removed by hand cleaner?  Try to speed up a little when with air assist.   The problem without is that all smoke goes up to your lens and fogged it up fast.  


---
**Ashley M. Kirchner [Norym]** *November 07, 2016 01:51*

I have a very small hole in my air assist nozzle which blows straight down into the cut. It blows all the smoke straight down, as well as whatever burned wood sap. I don't get any smoke coming up above the work piece. 


---
**Ashley M. Kirchner [Norym]** *November 07, 2016 01:51*

Oh, I have a 3D printed nozzle. 


---
**Stephane Buisson** *November 07, 2016 09:20*

not sure from the photo, but is it the SAME wood ? if not maybe different glue in your plywood.


---
**Derrick Armfield** *November 07, 2016 15:15*

After cutting/etching I take the piece to the kitchen sink and spray it with kitchen cleaner I got from the Dollar store.  Wipe with a towel and all the crud turns the towel brown.   Wood looks bright and clean!   


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/HuakvdKon87) &mdash; content and formatting may not be reliable*
