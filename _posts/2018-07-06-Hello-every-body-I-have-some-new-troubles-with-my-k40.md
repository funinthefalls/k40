---
layout: post
title: "Hello every body, I have some new troubles with my k40..."
date: July 06, 2018 14:29
category: "Discussion"
author: "Manon Joliton"
---
Hello every body, I have some new troubles with my k40...



My laser doesn't cut the right shape.

Yesterday, i checked the tension of the left and right 'roll' (i don't know how it's called?), could you help me find out what is happening ?

I noticed the rotary pole in the front of the machine   didn't turn 'round' because of some screw that were missing on what i think is a motor ? (The little   black cube that make the rotary pole rotate)

so i fixed the screws but it still doesn't work...

I checked the alignment by 2 times but i also still notice that while cutting, a second lasercut happens, like if there was a reflection, i checked that everything is clean.

I can't figure out what's happening

Thank you in advance and sorry if what i say isn't really clear...





**"Manon Joliton"**

---
---
**Manon Joliton** *July 06, 2018 14:43*

Here's what happen (it should be circles)

also, you can notice that at the bottom of the circles there are some 'spot', because the laser make a little loop there![images/60d91d5aa73100c4ca1d58e5ed9e697d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60d91d5aa73100c4ca1d58e5ed9e697d.jpeg)


---
**Don Kleinschnitz Jr.** *July 06, 2018 15:06*

I would start with a complete optical alignment. Especially make sure that the beam goes through the head straight and perpendicular to the surface. If it is not it can hit the side and create reflections that show up on the surface.


---
*Imported from [Google+](https://plus.google.com/110371358543635278419/posts/TyqhDbUSGV7) &mdash; content and formatting may not be reliable*
