---
layout: post
title: "Hi guys, I got a K40 the other day and the power supply blew the fuse (I've replaced the fuse and bridge - they both blew a 2nd time :/), the seller is shipping me a replacement"
date: March 10, 2017 08:45
category: "Discussion"
author: "Craig \u201cInsane Cheese\u201d Antos"
---
Hi guys,



I got a K40 the other day and the power supply blew the fuse (I've replaced the fuse and bridge - they both blew a 2nd time :/), the seller is shipping me a replacement. I'd like to eliminate shoddy wiring and I'll probably upgrade the 240v wiring to something a little more robust. Casing is a little beat up, I'll fix that at some point, looks like someone dropped it on the controller end :( 



In the meantime, from reading here and other places (Facebook/Reddit) I gather the controller itself is rubbish - I have a ramps+full graphics display on hand (from a 3D printer I ended up putting a duet wifi in) are there any guides to hooking this up? And are there any other things I should do while I'm messing with the internals? I have a spare arudino mega so I was thinking of getting a flow meter and some temp probes and hooking that up to a display. I've also got an old radiator from PC water-cooling I might use to help keep the water cool.



Don't really want to go to overboard $$$wise yet.





**"Craig \u201cInsane Cheese\u201d Antos"**

---
---
**Don Kleinschnitz Jr.** *March 10, 2017 15:25*

I would suggest you avoid a lot of pain and just get a C3D **+Ray Kholodovsky**



Here is a blog that may help you think through all that has to be done: [donsthings.blogspot.com - Click Here for the Index To K40 Conversion](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)




---
**Ray Kholodovsky (Cohesion3D)** *March 10, 2017 15:29*

Cohesion3D Mini - drops right in to replace the stock board, gets you full grayscale control for engraving, and is an all around affordable board that runs smoothie. 



[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



Make sure to get the GLCD adapter so you can put that screen you have to work with this board. 


---
**HalfNormal** *March 11, 2017 00:45*

Standard arduino running GRBL 1.1 and servo shield is a cheap start and widely supported. The Ramps/Mega combo unfortunately has not been tuned well and has spotty updates. Unless you purchase one of the smoothie boards from cohesion3d (very highly recommended!) you will need a middle man board for the ribbon cable.


---
**Craig “Insane Cheese” Antos** *March 11, 2017 07:45*

I ended up going with a cohesion mini, seems everybody loves them. And drop in replacement is always nice. I'll probably rewire it anyway, I don't like the look of the quality of wire used. Is there a wiring diagram for these available? I'd like to double check that it's all correct, and poor wiring isn't going to blow a 2nd PSU.


---
**Don Kleinschnitz Jr.** *March 11, 2017 12:53*

**+Craig Antos** my blog has schematics for most everything. 



My conversion is a smoothie 5x but using a C3D is mostly the same. 

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)



Stock (mostly) schematic is here: 

[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Stock)




---
**Craig “Insane Cheese” Antos** *March 12, 2017 01:03*

Cheers mate, I'll have a read :) 


---
*Imported from [Google+](https://plus.google.com/114851766323577673740/posts/HfiDR4Fk5Yd) &mdash; content and formatting may not be reliable*
