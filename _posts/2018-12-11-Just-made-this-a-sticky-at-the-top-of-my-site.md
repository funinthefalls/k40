---
layout: post
title: "Just made this a sticky at the top of my site"
date: December 11, 2018 23:51
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Just made this a sticky at the top of my site.



[https://www.pjrc.com/how-to-get-tech-help-from-strangers-on-the-internet/](https://www.pjrc.com/how-to-get-tech-help-from-strangers-on-the-internet/)





**"HalfNormal"**

---
---
**Ned Hill** *December 12, 2018 00:53*

That's wonderfully written and I so much agree.  


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/YRRuDKDRrmV) &mdash; content and formatting may not be reliable*
