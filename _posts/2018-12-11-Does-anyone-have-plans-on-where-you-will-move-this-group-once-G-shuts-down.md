---
layout: post
title: "Does anyone have plans on where you will move this group once G+ shuts down?"
date: December 11, 2018 14:33
category: "Discussion"
author: "Martin Dillon"
---
Does anyone have plans on where you will move this group once G+ shuts down?  Or is there another group that anyone belongs to? 





**"Martin Dillon"**

---
---
**Martin Dillon** *December 11, 2018 14:36*

**+Ned Hill** I just get a blank screen at everythinglaser.  Update. All my browsers see the site as an ad???


---
**HalfNormal** *December 11, 2018 14:42*

Nothing official, I have a site it has no advertisements or tracking. 

[www.everythinglasers.com](http://www.everythinglasers.com)

[everythinglasers.com - Home - Everything Lasers](http://www.everythinglasers.com)


---
**Martin Dillon** *December 11, 2018 14:45*

That is the one that my ad blocker blocks.


---
**HalfNormal** *December 11, 2018 14:45*

**+Ned Hill** just checked in everything is working fine for me. Even checked using my cell phone and cell service.


---
**HalfNormal** *December 11, 2018 14:46*

You need to white list it. Unfortunately due to spam you have to make sure things are tight!


---
**HalfNormal** *December 11, 2018 14:46*

There are absolutely no ads or trackers there.


---
**HalfNormal** *December 11, 2018 14:50*

Looking into the problem. Seeing if maybe a setting or two might be incorrect.


---
**Ned Hill** *December 11, 2018 15:23*

Yeah I joined Halfnormal's site.  Going to start moving some of my old posts to the site and double post new stuff here and there.  We need to start encouraging people to start migrating there to build the base


---
**Jerry Hartmann** *December 11, 2018 15:26*

I set up a mewe group




---
**James Rivera** *December 11, 2018 16:57*

**+Jerry Hartmann** what is the name of your mewe group?


---
**James Rivera** *December 11, 2018 16:58*

**+HalfNormal** I’ll join it later today.


---
**James Rivera** *December 11, 2018 16:58*

I’m also on the FB K40 laser group, but I know FB gives some people here the willies. And for good reason.


---
**Don Kleinschnitz Jr.** *December 11, 2018 17:09*

**+James Rivera** I am not afraid of FB but the amount of useless content is overwhelming :(. 

We can start an exclusive FB group 

[G+ Laser Engraving] 

....I did that for a wood turning group I was on. 

No offense to anyone but I don't care for the forum motif and its lack of integration with other apps, my desktop and my mobile devices. Going back to download then upload to alternate locations drives me nuts.


---
**HalfNormal** *December 11, 2018 17:19*

**+Don Kleinschnitz Jr.** Give me some examples of what you are looking for. The newer forum has a lot of features and flexibility.


---
**Ned Hill** *December 11, 2018 17:21*

What I like about our current group here is that it’s dedicated to the K40 users. Some of the other FB groups for all lasers can be down right snobbish about k40s.  I also like the high technical level here and also the ability to cross reference to other maker groups.  It may be that a dedicated k40 Facebook group gives us the most similar aspect.  What helps us on G+ is that its a low usage platform so we tend to collect dedicated individuals without a lot of junk users.  Of course this is also the reason we are losing G+. 


---
**Ned Hill** *December 11, 2018 17:23*

That being said, the advantages of a traditional forum site is that it’s much easier to organize information. 


---
**HalfNormal** *December 11, 2018 17:24*

The real problem with group communities is you are at the mercy of the company that has created it. I looked into some group software but they're all pretty complicated and again at the mercy of vendors. The open-source ones we're okay but still a very steep learning curve.


---
**Jerry Hartmann** *December 11, 2018 18:54*

**+James Rivera** k40 lasercutting








---
**Jerry Hartmann** *December 11, 2018 18:55*

**+HalfNormal** You're still at the mercy of whoever is hosting.


---
**HalfNormal** *December 11, 2018 18:57*

**+Jerry Hartmann** if I ever decided not to host anymore, I would be more than happy to give the forum over to somebody else if they wish to take it on.


---
**Ned Hill** *December 11, 2018 19:01*

**+Jerry Hartmann** where is this "k40 lasercutting" group hosted? 

Edit: Nevermind, just figured out that Mewe wasn't a typo for New :D  [MeWe.com](http://MeWe.com)






---
**Ned Hill** *December 11, 2018 19:37*

**+Jerry Hartmann** I don't see a K40 Lasercutting group on MeWe.


---
**Ned Hill** *December 11, 2018 19:41*

MeWe looks good for social interaction, but it's even worse than G+ for organizing information.  Each group has a got a place to make posts and a seperate general chat section and that's it.


---
**James Rivera** *December 11, 2018 19:42*

**+Jerry Hartmann** I cannot find a "k40 lasercutting" group on MeWe. In fact, just "K40" came up empty. Growing pains, I guess.


---
**James Rivera** *December 11, 2018 20:03*

**+HalfNormal** Regsitered for EverythingLaser.com, got the success message, saying, "You have successfully created your account! To begin using this site you will need to activate your account via the email we have just sent to your address", but no email yet. Over 10 minutes. I sent myself an email just to make sure my email is working. It is.


---
**HalfNormal** *December 11, 2018 20:09*

**+James Rivera** there's a slight glitch in the spam bot controls that is causing an issue I'm working on it. I'm on the road and should be able to take care of you in a couple hours. 


---
**HalfNormal** *December 11, 2018 22:35*

**+James Rivera** You are now activated. Sorry for the delay.


---
**Jerry Hartmann** *December 11, 2018 23:26*

**+Ned Hill** **+James Rivera** fixed. Forgot to set to public


---
**Don Kleinschnitz Jr.** *December 12, 2018 00:44*

**+HalfNormal** to start ...I want to add pictures to a post directly from my phone. I want to access the forum from an app on my phone, not a web site. 


---
**Don Kleinschnitz Jr.** *December 12, 2018 00:48*

**+Ned Hill** that is exactly why I started the carefully controlled FB group. Is is only for Stubby lathe users which had that as a common interest. These are folks that payed a lot for a lathe shipped from Australia and are serious about wood turning with that specific equipment.

I think to some degree the difficulty in learning G+ is what got dedicated users here .....


---
**HalfNormal** *December 12, 2018 01:06*

**+Don Kleinschnitz Jr.** the phone web version of my forum is close to looking like an app. You can actually post a photo from your phone in the site. I was able to access all files and even my Google photos from my phone.


---
**Martin Dillon** *December 12, 2018 03:22*

I was caught in the glitch.  Never got the email to verify.  


---
**HalfNormal** *December 12, 2018 03:35*

**+Martin Dillon** I do not see you pending. Please check again and let me know.


---
**HalfNormal** *December 12, 2018 03:39*

**+Martin Dillon** I am showing you is activated.


---
**Don Kleinschnitz Jr.** *December 12, 2018 03:59*

**+HalfNormal** what mobile app are you referring to. 


---
**HalfNormal** *December 12, 2018 04:03*

The website viewed on Google chrome for Android makes the forum look like an app. It is optimized for mobile.


---
**Don Kleinschnitz Jr.** *December 12, 2018 04:25*

**+HalfNormal** generaly the mobile site is better than most. I hit the site from my Android and tried to post pictures. The screen is not sized correctly and only partially visible. To add a picture I have to give it a source filename? It doesn't open Google photos and show me thumbnails. 


---
**HalfNormal** *December 12, 2018 04:28*

**+Don Kleinschnitz Jr.** If you look to the right of that file name box there is a folder that will open up folders on your phone


---
**Don Kleinschnitz Jr.** *December 12, 2018 15:08*

**+HalfNormal** I tried again. In the portrait mode the camera button you mentioned to the right is off the screen to the right. Rotating to the landscape mode I can get to the button but when the keyboard slides up it covers the editing screen and blocks what I am trying to do. I tried multiple times to add an image and could not because the editing window was hidden behind other dialogs and the keyboard. For some reason while on the site it would not let me take a screen shot.

Can I attach a video?

**+HalfNormal** my intention is to not to criticize you or this site. I know that you are trying to help.

Building a modern mobile experience with web site foundation rather than a local app is hard ...I know. 

My thirst for a mobile experience is that I create most of my content from the shop. G+ and its integration with my Blog and all other Google apps dramatically reduced my blogging overhead. Of course that ship is sailing ...... 




---
**HalfNormal** *December 12, 2018 15:37*

**+Don Kleinschnitz Jr.** No offense taken. I appreciate the feedback. I do want to make the forum experience as painless as possible to everyone. That's why your feedback is essential. I will see what I can do to make the mobile experience better. Thanks again for the feedback.


---
**Don Kleinschnitz Jr.** *December 12, 2018 16:27*

Got on WeMe, it has a phone app. Cannot find the K40 Laser Cutter group.

So far is doesn't look to bad...

**+Ned Hill** why do you think it is harder to search?


---
**Don Kleinschnitz Jr.** *December 12, 2018 16:46*

I created a group here is the link: see if you can find it and request access: [mewe.com - MeWe: The best chat & group app with privacy you trust.](https://mewe.com/join/k40lasercuttingengravingmachine)

Seems a lot like FB.


---
**James Rivera** *December 12, 2018 21:27*

**+Don Kleinschnitz Jr.** Joined!


---
**Fernando Bueno** *December 13, 2018 09:49*

**+HalfNormal** It is not entirely true. The web tries to read the browser fingerprint to identify it in a unique way and that is a form of tracking. When you block that operation, the page appears blank. I use an advertising blocker and it is not who blocks access to me, but if I activate the canvas blocker, the page appears blank.



No matter how fashionable this tactic is, and no matter how WordPress is doing it, it still seems like another form of tracking to avoid.




---
**HalfNormal** *December 13, 2018 12:57*

**+Fernando Bueno** I use a widely respected app called Privacy Badger.

It does not detect any tracking, including cookies for my site. I do agree that there are nefarious tracking going on by the big companies but the EU regulations are making everyone aware if the site is tracking you. A lot of browser ad blockers go overboard and I too have had problems but at some point you have to believe that some people are not trying to do harm. Here is a screen shot of what privacy badger found on my site and another site.



![images/b62ad00c08ec193ac1dde85f84a3599d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b62ad00c08ec193ac1dde85f84a3599d.jpeg)


---
**Fernando Bueno** *December 13, 2018 13:24*

**+HalfNormal** Possibly, your Privacy badger settings ignore the fingerprint readings, either because you have a rule for it or because you have it incorrectly configured. When I access the web using Tor, I get all kinds of alarms, whereas when I access the Firefox with which I usually browse, I get 2 tracking alarms in Canvas Blocker and the blank page appears.

![images/1bba3ef97e1fb1b6067f1fa3fc07b52f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bba3ef97e1fb1b6067f1fa3fc07b52f.jpeg)


---
**HalfNormal** *December 13, 2018 13:35*

**+Fernando Bueno** can you please let me know what these trackers are so that I can find out how to rectify the blank page issue.


---
**Fernando Bueno** *December 13, 2018 17:55*

**+HalfNormal** You can see this page ([https://panopticlick.eff.org](https://panopticlick.eff.org)), from which you can analyze if your browser is vulnerable. It also explains what the fingerprint is and how to avoid tracking. One way is to use Privacy Badger and configure it well, as Tor does and another is to use Canvas Blocker, which allows you to block the fingerprint reading or trick the server, giving you one random each time.



WordPress uses this technique since a few versions and it does not seem like a very elegant way of working, because it does not report it and the average user can not protect itself, since browsers, by default, do not block this tracking although you configure them to avoid it (In fact, only a blank page will block the reading of the fingerprint). But the problem is that the owner of the web can know who visits the web and much more information, even if the visitor has configured his browser to not be tracked.


---
**James Rivera** *December 13, 2018 18:38*

I installed PrivacyBadger (from [eff.org](http://eff.org), yay!) and it looks like the only tracker is from Google.  I suspect you have the google analytics file installed. That's probably it. That being said, it worked fine for me in Chrome, with PB installed.


---
**James Rivera** *December 13, 2018 18:41*

Here is what it looks like for me:

![images/65076b11c72c38c43987c73d9477a955.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65076b11c72c38c43987c73d9477a955.jpeg)


---
**HalfNormal** *December 13, 2018 21:17*

Unfortunately the saying " no such thing as a free lunch " is true if you're using a WordPress product.


---
**BEN 3D** *December 15, 2018 15:14*

I will also join [everythinglasers.com - Home - Everything Lasers](http://www.everythinglasers.com)

And here my Twitter Account for any how is interested. [https://twitter.com/BEN3D1](https://twitter.com/BEN3D1)


---
**Don Kleinschnitz Jr.** *December 15, 2018 15:50*

Earlier in this thread I started a closed WEME group. So far I think it is a viable alternative as it has a modern UI and well integrated mobile 

 and web apps. 

I don't know if this alternative is better than a private FB group though. 

In fact WEME looks like a FB clone with no data sharing on their part. At least that is what they say.

I am skeptical of their business model as their operating revenue seems to come from apps purchased by users.

Best I can tell they are funded mostly by angels out of calif.



**+Ned Hill** has also been playing with WEME and I am interested in his summary of the platform.



My learning is that all the information that I create will be done in my blogs not on social networks. Social network posts will link to that content. Of course they could kill the blogs also :(!






---
**HalfNormal** *December 15, 2018 16:45*

**+Don Kleinschnitz Jr.** There are reports that postings on WEME are not accessible by web searches. As of now, the searches of WEME are not very useful either.


---
**Ned Hill** *December 15, 2018 17:39*

**+Don Kleinschnitz Jr.**  I find MeWe to be interesting.  It's like a facebook clone, but the UI is definitely less polished.   I'm sure it will improve with time.  The separate chat and posts are nice and the ability to do some picture editing is nice as well.  As well as having a file section.  I'm also somewhat skeptical on their revenue model.



So with MeWe you get a "Cloud" storage with a free 8GB for photos and files.  The big downside I just figured out is that everything you upload to a group remains tied to your cloud.  So if you delete a file or photo from your cloud it deletes it, post and all, everywhere you have shared it.  Also means that if someone deletes their account it deletes everything they ever shared.



**+HalfNormal**  yes MeWe is a closed network and isn't indexed by search engines as far as I can tell.  Also searching across MeWe is limited for groups you are not apart of.  Really depends on how open the settings are for a group.  


---
**James Rivera** *December 15, 2018 17:40*

**+HalfNormal** The price of privacy. If they exposed everything to Google’s crawlers, then it would.


---
**James Rivera** *December 15, 2018 17:42*

Also, GDPR “Right to be forgotten”. If you deleted your account, wouldn’t you want your posts and comments deleted?


---
**Ned Hill** *December 15, 2018 17:52*

**+James Rivera**  the downside to that is we potentially lose valuable knowledge/info for the group.  It’s a bit of a double edged sword. 


---
**Don Kleinschnitz Jr.** *December 16, 2018 13:07*

**+Ned Hill** FYI pretty sure that if you attach a photo from your google photos in G+ and then delete it they are deleted from posts.**+James Rivera** I think GDPR only applies in Europe?




---
**Don Kleinschnitz Jr.** *December 16, 2018 13:13*

The bottom line is that access to information and convenience decreases privacy. 

I love to hear folks bitch about privacy while putting their passwords on stickies on their computer. 

When you get on any social network you are going to give up privacy that is the point of being social. How do people think these companies make money to pay their software engineers??? With search and add revenue.



I still think the most comfortable way out is to create a private FB group. What info is this group giving up that is so private and that we haven't already given up just by being on the internet? 


---
**James Rivera** *December 16, 2018 20:58*

**+Don Kleinschnitz Jr.** Yes, GDPR is a European law, but lots of American companies are updating their privacy policies everywhere because it is easier than trying to figure out who is from Europe.


---
**'Akai' Coit** *December 22, 2018 06:46*

Scanned over most of the responses here, but didn't see what I'm about to suggest... What about Diaspora or Discord?


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/So6115FWT3V) &mdash; content and formatting may not be reliable*
