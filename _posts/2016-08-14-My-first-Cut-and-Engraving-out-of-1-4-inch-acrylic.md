---
layout: post
title: "My first Cut and Engraving out of 1/4 inch acrylic"
date: August 14, 2016 01:36
category: "Discussion"
author: "Robert Selvey"
---
My first Cut and Engraving out of 1/4 inch acrylic.

![images/b2b02cb395ae0546df5d226dbf7c6c36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2b02cb395ae0546df5d226dbf7c6c36.jpeg)



**"Robert Selvey"**

---
---
**Alex Krause** *August 14, 2016 01:41*

Cast or extruded acrylic?


---
**Alex Krause** *August 14, 2016 01:57*

If you are using cast acrylic try about 5 ma at 450mm/s it will turn your engrave a frosty white


---
**Robert Selvey** *August 14, 2016 02:47*

It is cast acrylic was the first time on it so I did it at 2ma at 20mm really to slow was not sure how to speed it up in the middle so I just let it finish.


---
**Alex Krause** *August 14, 2016 02:48*

I'm getting ready to run an engrave on acryllic I will post the results shortly


---
**Robert Selvey** *August 14, 2016 03:16*

looking forward to seeing it


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/R2NPQfEjMjG) &mdash; content and formatting may not be reliable*
