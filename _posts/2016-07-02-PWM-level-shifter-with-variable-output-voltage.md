---
layout: post
title: "PWM level shifter with variable output voltage"
date: July 02, 2016 21:20
category: "Discussion"
author: "David Richards (djrm)"
---
PWM level shifter with variable output voltage.



Here is a circuit idea to provide a variable voltage PWM drive from a 3v3 PWM source, this has not been tried yet. I have been thinking of a way to incorporate a front panel laser current control with the pwm from a controller board. The pot would be able to be used to set the maximum current and the PWM m/s ration would then give proportional control up to that maximum. The circuit would depend on the power supply integrating the pwm voltage to produce the laser driving current.



Any comments welcome, David.



![images/b2d147c86fe3a1dae870963f44a0aa93.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b2d147c86fe3a1dae870963f44a0aa93.png)



**"David Richards (djrm)"**

---
---
**Don Kleinschnitz Jr.** *July 02, 2016 21:47*

This is a clever way to limit the PWM voltage. 



FYI: I have been doing quite a bit of research on driving the laser power supply. I have concluded that a level shifter is not necessary. Depending on your supply  TL,TH, or -L on the supply can be used for the PWM input using an open drain configuration, while leaving the current control pot in the machine. I am in the process of testing and characterizing this approach all the way out to the lasers output. 


---
**David Richards (djrm)** *July 02, 2016 23:15*

Don, I look forward to hear your test results. I would be happier to have the open drain FET separate from the microprocessor output to offer some protection in case of a mis-configuration allowing 5v to be applied to an output pin. Otherwise sounds like a good idea. David.﻿ 



Update, perhaps I misunderstood and you are meaning to use one of the high current driver FETs and not an open drain microprocessor output. In this case it should be just fine.


---
**Stephane Buisson** *July 03, 2016 08:01*

injecting variable HV into level shifter via pot, clever !

(my old/actual version being reducing V after)

**+Peter van der Walt** 


---
**Mircea Russu** *July 03, 2016 08:37*

Still unpredictable if HV<LV and most level converters have a min HV over 1V and we get 5ma at under 1V


---
**Stephane Buisson** *July 03, 2016 09:13*

**+Mircea Russu** it's no point to be HV<LV, at 3.3V  you are under the max mA ceilling and your tube is safe. (main goal)

now I get your point if you move inadvertantly the pot, but it's true both way.


---
**Don Kleinschnitz Jr.** *July 03, 2016 13:53*

**+david richards** Yup meant to use the Mosfet as open draIn.


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/Rp6rwuWCbGR) &mdash; content and formatting may not be reliable*
