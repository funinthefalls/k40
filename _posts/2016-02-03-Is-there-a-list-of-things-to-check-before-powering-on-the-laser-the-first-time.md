---
layout: post
title: "Is there a list of things to check before powering on the laser the first time?"
date: February 03, 2016 22:39
category: "Discussion"
author: "HP Persson"
---
Is there a list of things to check before powering on the laser the first time?



Seen some tips here and there, cheking cables, power, mirrors and other hints on what to look out for on the K40 lasers before powering it on and the first test.



A step-by-step on this matter would be nice, for others to find too.

Sorry if i missed one already existing, didnt find anything here though :)





**"HP Persson"**

---
---
**Stephane Buisson** *February 03, 2016 23:00*

You said it all except to check water line (air buble) & flow.


---
**Joseph Midjette Sr** *February 04, 2016 01:51*

Yes as Stephanie said, water, make sure it is distilled and let it run for a good 5 or 10 minutes to get any air bubbles circulated out. You have everything else covered except ventilation


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/Xbs8MpmSAZK) &mdash; content and formatting may not be reliable*
