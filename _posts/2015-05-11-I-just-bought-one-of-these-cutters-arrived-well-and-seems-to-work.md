---
layout: post
title: "I just bought one of these cutters, arrived well and seems to work"
date: May 11, 2015 10:49
category: "Discussion"
author: "Troy Baverstock"
---
I just bought one of these cutters, arrived well and seems to work. can anyone point me to information regarding an upgrade to the amp meter and current control? 



I don't know enough about how the pot is controlling the current to tap into it. I was thinking the safest way might be a 1k 2w dual pot, create my own geared shaft and have it motor driven with an arduino reading the second output. That way I could have power presents and control it more accurately. My concern (also the concern for the amp meter is that the voltages would be very high?)



If there are other ways to control and read the current I'd like to hear them (short of replacing the control board)





**"Troy Baverstock"**

---
---
**Jon Bruno** *May 11, 2015 11:37*

Yeah you are looking at upwards of 20kv.

Many people use aftermarket digital displays to get a more accurate reading. An arduino monitor would be overkill. Especially since you will be tweaking it on a material and cut style basis.


---
**Stephane Buisson** *May 11, 2015 17:59*

Welcome here Troy.

K40 update is one big thing I like to see here, I personally wait a bit to update for a better hardware/software combination with smoothieboard/visicut. nothing  specific about current management which will be part of the mod.


---
**Troy Baverstock** *May 11, 2015 23:31*

The switches and Potentiometer look standard, main switch rated at 250v 15A, test switch 250v 1A, on/off laser 250v 2A, the pot is a carbon based one.



I don't see anything to indicate that they are suited to high voltage. The arduino would monitor a second winding on a dual pot, so not the current directly, it would just know where in a pots movement equates to what current. 



Arduinos are less than 4$, and a display for them are 3$, so they are not big expenses and allow for the option of monitoring the cooling system etc.



What I am having trouble with is finding a suitable '2W' rated dual pot, I'm wondering if it really needs to be rated so high (plenty of motorised pots at low wattages) or if it's just some sort of control signal. I'm not knowledgeable enough in such matters.



If I go this way I wont need to even touch the analogue gauge and I would be able to set my current before the laser is turned on (as the arduino would know where to shift the pot to)


---
*Imported from [Google+](https://plus.google.com/+TroyBaverstock/posts/QBx5VXD2nSC) &mdash; content and formatting may not be reliable*
