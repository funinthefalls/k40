---
layout: post
title: "Can someone recommend a good pair of laser safety glasses that don't cost as much as a k40 I find my self wanting to do stupid stuff when trying to align my laser like leave the lid open I have been look at some on eBay but I"
date: July 20, 2016 23:19
category: "Discussion"
author: "3D Laser"
---
Can someone recommend a good pair of laser safety glasses that don't cost as much as a k40 I find my self wanting to do stupid stuff when trying to align my laser like leave the lid open I have been look at some on eBay but I don't know what spectrum to get





**"3D Laser"**

---
---
**Jim Hatch** *July 20, 2016 23:29*

Laser Safety Eyewear - Co2/Excimer Filter In Black Plastic Fit-Over Frame Style. [https://www.amazon.com/dp/B000HJMS4A/ref=cm_sw_r_cp_apa_UKaKxb6KZ4YJC](https://www.amazon.com/dp/B000HJMS4A/ref=cm_sw_r_cp_apa_UKaKxb6KZ4YJC)


---
**Jim Hatch** *July 20, 2016 23:29*

Or New Protective Goggles for Co2 Laser 10600nm 10.6um Safty Glasses-absorption Type2 [https://www.amazon.com/dp/B00L2SU1TY/ref=cm_sw_r_cp_apa_uLaKxbZPSRZG0](https://www.amazon.com/dp/B00L2SU1TY/ref=cm_sw_r_cp_apa_uLaKxbZPSRZG0)


---
**Jim Hatch** *July 20, 2016 23:30*

I prefer wearing the first ones. They're more comfortable and closer fitting.


---
**3D Laser** *July 21, 2016 00:22*

Thanks Jim 


---
**Ben Walker** *July 25, 2016 13:01*

I would also like to thank Jim as well.  I need to order a gross of them as it seems that I tend to have an ever growing audience in my space.  And with the new x700 trucking its way across the country this week I am sure that will only increase the numbers.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/8CSRrwsLsck) &mdash; content and formatting may not be reliable*
