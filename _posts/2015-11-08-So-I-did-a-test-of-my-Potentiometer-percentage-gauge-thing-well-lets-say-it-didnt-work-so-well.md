---
layout: post
title: "So I did a test of my Potentiometer percentage gauge thing & well, let's say it didn't work so well"
date: November 08, 2015 13:43
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I did a test of my Potentiometer percentage gauge thing & well, let's say it didn't work so well.



First things first, the size was too large (only fractionally). I had it at 26mm on the inner circle, which was perfect. The outer edge of all the points was at 59.something mm. It was too large to fit around the potentiometer because it would push up against the mA meter & the Test Fire button.



So, basically I need to shrink the outer size by at least 1-2mm so it fits.



Next problem is that the original font I used was very illegible after cutting/engraving. The numbers 0-10-20-30-----100 were too small. So I revised my original idea & changed the numbers to 0-1-2-3-----10 in a more legible font.



This sorted that problem, however then there was the problem with it not cutting all the way through the leather. I was using about 6mA power, 10mm/s cutting speed. Went about 3/4 through the 3mm leather (which turned out to be closer to 5mm on the scrap I was using). Then I tried a thinner leather (about 2mm). Still didn't go all the way through (which was totally unexpected).



Also, I used painter's masking tape over the top of the entire working area of leather to minimise soot marks on the top of the leather. Someone suggested that to me here (can't remember who, but +1 to them for that, since it works perfectly).



Here's some pics of the results anyway. I will repost etc once I get it all worked out.



![images/6d2277c38b67c0809f72ba2db74dcd3e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d2277c38b67c0809f72ba2db74dcd3e.jpeg)
![images/9e07320fe342101838061909cb1b7a12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9e07320fe342101838061909cb1b7a12.jpeg)
![images/56315a8382dc999fa0ea24aae3f26a78.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56315a8382dc999fa0ea24aae3f26a78.jpeg)
![images/c5a319ffde427d97db07e84440d286a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5a319ffde427d97db07e84440d286a6.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Coherent** *November 08, 2015 14:36*

For gauge and switch markings (like on back lit panels in aircraft and machines) there are actually specific fonts that are commonly used so that they are easy to read even when small and are somewhat consistent. You can use whatever fonts and designs you want of course, but FWIW a good rule of thumb is,  basic easy to read fonts and simple designs/markings seem to work and look the best.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 08, 2015 15:27*

**+Marc G** I think part of my problem is that things look super-clear on my computer when I am designing the files. But I guess that is because I usually design @ 400% zoom haha. Also, as I use Adobe Illustrator, it is all vectors that antialias again even when I zoom in, so still super clear. Should maybe keep in mind that things will look a lot smaller. I will keep that in mind what you suggested though, as that seems to be a good rule of thumb indeed.


---
**Alessandro Milano** *November 10, 2015 11:32*

Sure your machine is using the right focus ?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 14, 2015 05:21*

**+Alessandro Milano** Turns out that the machine had the lens installed upside-down & my focus was slightly out. All sorted now.


---
**Alessandro Milano** *December 14, 2015 11:00*

Happy that you have solved !! Enjoy !!!


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UnwigkzbDrT) &mdash; content and formatting may not be reliable*
