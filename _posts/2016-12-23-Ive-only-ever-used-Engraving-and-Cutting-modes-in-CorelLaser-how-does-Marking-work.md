---
layout: post
title: "I've only ever used \"Engraving\" and \"Cutting\" modes in CorelLaser - how does \"Marking\" work?"
date: December 23, 2016 20:47
category: "Software"
author: "Tev Kaber"
---
I've only ever used "Engraving" and "Cutting" modes in CorelLaser - how does "Marking" work? I'm assuming it is scoring, and works like cut (vector) but arranges the line order differently?



I could just try it, but I figured I'd ask here before I fire up a mode I've never used...





**"Tev Kaber"**

---
---
**John-Paul Hopman** *December 23, 2016 21:23*

I would assume marking is using a special spray paint on the object to be lasered, which the machine then cures. Afterward you then clean off the non-lasered paint to reveal the marking.



Just a guess.


---
**greg greene** *December 23, 2016 22:06*

I tried it on 3mm ply, it appeared to be the same as engraving


---
**Tev Kaber** *December 23, 2016 22:14*

Good to know, guess I'll stick with a fast/low power "cutting" for vector marking.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/jHdDbFEwUEo) &mdash; content and formatting may not be reliable*
