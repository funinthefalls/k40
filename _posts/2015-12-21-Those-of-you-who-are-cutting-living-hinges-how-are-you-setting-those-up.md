---
layout: post
title: "Those of you who are cutting living hinges, how are you setting those up?"
date: December 21, 2015 17:06
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Those of you who are cutting living hinges, how are you setting those up? Knowing that a stock controller likes to cut a 2-point line twice, is there a way to overcome that, other than replacing the controller for a better one?





**"Ashley M. Kirchner [Norym]"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 21, 2015 20:11*

I haven't done it, but I would do it like this

[https://drive.google.com/file/d/0Bzi2h1k_udXwRUw4R2ZBemlXeE0/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwRUw4R2ZBemlXeE0/view?usp=sharing)



Obviously, that is an exaggerated version to show the setup, not actual dimensions. The punched out white areas in the black rectangle would need to be much smaller in size for the real design file.



This way it only cuts one 1 side of the line. You have to use shapes with no borders & solid black fill. I have tried super tiny borders & it still cuts twice with the stock controller. Only way that works for me for a single cut is to use solid black fill & no border.


---
**Ashley M. Kirchner [Norym]** *December 21, 2015 20:32*

Well yeah, I know that part, but I don't want to end up with holes in the hinges. I want just a single cut and done. I'll play with it and see what I can come up with.


---
**I Laser** *December 22, 2015 06:47*

Not sure of your setup, but if you set the line thickness to .01mm in CorelLaser it will only cut once.


---
**Ashley M. Kirchner [Norym]** *December 22, 2015 17:05*

I don't use CorelLaser, it won't run on my system. So I'm using Illustrator to create my files and importing them in LaserDRW, which sucks too as it only imports bitmaps, not vectors. I need to upgrade this thing pronto.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/JdV6VJeHYei) &mdash; content and formatting may not be reliable*
