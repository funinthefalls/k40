---
layout: post
title: "I'm having a strange problem...essentially the start and end of my cuts don't line up, which essentially means my cuts are distorted"
date: October 17, 2017 22:03
category: "Discussion"
author: "Deren Ash"
---
I'm having a strange problem...essentially the start and end of my cuts don't line up, which essentially means my cuts are distorted. In the attached pics are sample cuts of a circle. in 1, the y doesn't line up. In the other, the x doesn't line up. Most of the time it's mostly the y. Sometimes it's both. It seems to just start doing. The pic of the raster is supposed to be a square, and as you can see it's distorted.



The problem went away several times temporarily when I hooked up a C3D (was having a separate problem with that with the laser wire not hooked up to the right place so in those cases dunno if it would have been working with C3D) and then hooked the stock board back up. Of course that could be coincidence. 

But now it won't go away. I got the C3D firing the laser now, but it does the same thing with the lining up.



Yes, I aligned the beam, not that it would produce this particular symptom. You'd think it would be the belts right?  Nope, apparently not. I've checked the belts. I tried the belts to tighter than I think they should be and looser than I think they should be, and that didn't help either. I tried unhooking my air assist to make sure the tube wasn't causing drag. I tried changing the motor driver pots on the C3D to see if it was an under or overcurrent. It's basically the same issue with both CorelLaser/stock controller and with LaserWeb/C3D.



Anybody have any ideas?  Since it's clearly not the controller or software, and belts have been thoroughly fiddled with, what remains?  Steppers and power supply?



![images/1f94aad37c099411f2da00e68833a42f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f94aad37c099411f2da00e68833a42f.jpeg)
![images/a6e18174606e7ea04d2c25d9f439b6a5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a6e18174606e7ea04d2c25d9f439b6a5.jpeg)
![images/fae8f13df9d0f5046f865099a41dc64b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fae8f13df9d0f5046f865099a41dc64b.jpeg)

**"Deren Ash"**

---
---
**Adrian Godwin** *October 17, 2017 23:09*

A toothed wheel not tight on its shaft ?



Losing steps ? (maybe there's too much friction, or an area where it catches slighty)


---
**Deren Ash** *October 17, 2017 23:22*

There is one spot on the y where it feels like it catches slightly, however, it's about half way on the y, and it doesn't seem to make a difference whether I'm cutting or engraving above, below, or across this spot.


---
**Deren Ash** *October 17, 2017 23:54*

Also why would it seem to be sometimes the x and sometimes y?  If it was something mechanical or a stepper was failing, shouldn't it be just in that axis?  Is it possible that the steppers aren't getting the power they need?  Given that it's with both controllers and both axes, could it be that the power supply is intermittently dropping voltage just long enough to skip some steps?  Ever heard of a power supply going bad on that side of things?  Or that a bad stepper is shorting briefly causing a voltage drop that could affect the other one?



I did hook up a multimeter to it while it was going, and it doesn't vary more than 0.1 volts while it's going, however, my multimeter takes a second or so to respond so if it were dropping for fractions of a second then it wouldn't show that.


---
**Wolfmanjm** *October 19, 2017 09:41*

I'd say loose belts causing backlash. Also if the Y sticks (as mine did) I found spraying with WD40 to clean the rod and some 3in1 oil on the rod worked wonders and the Y is smooth now.


---
**Deren Ash** *October 19, 2017 10:01*

**+Wolfmanjm** Thanks, I have fiddled with the belt tensions.  Also I use machine oil on the rod.  It suddenly started working again after I fiddle with some more stuff (slightly loosening the wheels on the X carriage for less tension), but after about 45mins of laser time, now it's messing up again.


---
*Imported from [Google+](https://plus.google.com/109581708182277964430/posts/8YjM41sF62c) &mdash; content and formatting may not be reliable*
