---
layout: post
title: "Scorch has done it again. Whisperer 0.16 is released and he's fixed a really annoying issue with CorelDraw files that previously required me to export SVG files, then bring into Inkscape and ungroup, then save"
date: January 09, 2018 05:55
category: "Software"
author: "Timothy Rothman"
---
Scorch has done it again.  Whisperer 0.16 is released and he's fixed a really annoying issue with CorelDraw files that previously required me to export SVG files, then bring into Inkscape and ungroup, then save.  Native CorelDraw exported SVG files now work properly for vector cuts.



Thanks again **+Scorch Works**!



[http://www.scorchworks.com/K40whisperer/k40whisperer.html](http://www.scorchworks.com/K40whisperer/k40whisperer.html)







**"Timothy Rothman"**

---


---
*Imported from [Google+](https://plus.google.com/107673954565837994597/posts/gfPDF8kUgZx) &mdash; content and formatting may not be reliable*
