---
layout: post
title: "My old tube went pthht, so I decided to upgrade it with an 800mm, which they falsely advertise as 50 watts"
date: July 22, 2018 17:04
category: "Modification"
author: "Jamie Richards"
---
My old tube went pthht, so I decided to upgrade it with an 800mm, which they falsely advertise as 50 watts.  It's not working correctly and the beam isn't dark like the old one.  It takes near 5ma before it begins to affect paper and 10 passes on material the original 700mm tube could do in one.



![images/bb689054dc5ea38bb3fc68cbf54c5f9c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb689054dc5ea38bb3fc68cbf54c5f9c.jpeg)
![images/b0a435e5d05f5e2897649cdced56b899.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0a435e5d05f5e2897649cdced56b899.jpeg)
![images/a541afb557e603b95c16af97dea3d285.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a541afb557e603b95c16af97dea3d285.jpeg)
![images/83cf64e5a22c82b60d22285a151a06d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83cf64e5a22c82b60d22285a151a06d3.jpeg)
![images/f6ce455dd21d78fdcef5b595401adef1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f6ce455dd21d78fdcef5b595401adef1.jpeg)

**"Jamie Richards"**

---
---
**Anthony Bolgar** *July 22, 2018 18:05*

Sounds like you alignment is off. Redo all the alignment, and make sure your mirrors and lens are clean.


---
**Jamie Richards** *July 22, 2018 19:01*

Installed new Si mirrors and aligned at the same time, I can't even check the first mirror under 4ma, it's terrible.


---
**BEN 3D** *July 22, 2018 19:17*

Does it depends on the power of the supply? Did you Need a bigger supply? I doesn‘t now it, I ask just for my personal interrest 🤗


---
**Anthony Bolgar** *July 22, 2018 19:18*

Possible that you do not have a good connection at the HV side of the tube. I would re wrap the wire on the post after cleaning it with a bit of fine sandpaper, and strip the wire back to a new clean section.




---
**Jamie Richards** *July 22, 2018 19:19*

It's a metal head with a screw.


---
**Jamie Richards** *July 22, 2018 19:22*

Forgot to add, I did cut and strip it back a little, so the connections are solid.  This power supply is supposed to handle this size tube, if anything, the flyback could possibly be weak, but my old tube worked better even when it needed replaced.


---
**Anthony Bolgar** *July 22, 2018 19:47*

It does sound like it is a bad tube then. I would contact the distributor and request they send you a new tube. I personally only deal with Cloudray laser. They sell excellent quality tubes, and have great customer support 7 days a week.




---
**Jamie Richards** *July 22, 2018 19:58*

I was considering putting the old tube back in, but had an accident with it that caused the glass to break at the cathode terminal/post.


---
**Jamie Richards** *July 22, 2018 20:00*

I have CloudRay optics, but decided to go with this other place.  Then I found out they were basically just a drop shipper and have to contact the actual supplier in China.  Bleh.


---
**Don Kleinschnitz Jr.** *July 22, 2018 20:06*

These tubes do not start to ionize until about 4-5 ma and will not run properly that low. Try it with 10-20 ma. 


---
**Jamie Richards** *July 22, 2018 20:08*

Even at 20 it can't compete with old Betsy.


---
**Jamie Richards** *July 22, 2018 20:08*

Actually, I never ran old Betsy past 17mA.


---
**Jamie Richards** *July 22, 2018 20:11*

Don, the 800mm doesn't, or the 700mm as well?  I was able to lightly engrave at 3mA with the 700mm.


---
**Jamie Richards** *July 22, 2018 20:39*

It made a fizzing noise that low, but I actually liked the way it worked, the texture it gave on acrylic while fizzing.  These were done around 3-4mA withe the old tube while still fizzing.

![images/09e9978e2b0f2fb294fd14ab2e9a3683.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09e9978e2b0f2fb294fd14ab2e9a3683.jpeg)


---
**Don Kleinschnitz Jr.** *July 23, 2018 01:15*

The inonization point is pretty uncertain at low current I wouldn't be surprised if each tube is pretty different.


---
**Jamie Richards** *July 23, 2018 12:24*

Another thing I discovered is that you should go for a tube with a pinkish-purpleish center because it's some kind of gold catalyst to help with tube longevity.


---
**Kelly Burns** *July 24, 2018 23:33*

I replaced a tube to discover that the old powers supply was actually the problem


---
**Jamie Richards** *July 25, 2018 00:20*

Was it weak or not working at all?


---
**Kelly Burns** *July 25, 2018 01:45*

**+Jamie Richards** it was weak.  Then replaced with new one and t was only marginally better until I replaced PS


---
**Jamie Richards** *July 25, 2018 11:08*

I think it could be the supply.  I posted some interesting test results.  At 15mA, it would appear to only be driving the tube at around 19-20 watts at a voltage of 13kv. 


---
**Don Kleinschnitz Jr.** *July 25, 2018 12:06*

**+Jamie Richards** 

This LPS is the highest failure item in a K40.

The windings short (break down) or the output diodes go bad.



These tubes require substantially more than 13K to operate properly so without knowing your test setup its hard to draw any conclusions.



My guess is that your HVT is breaking down under load. 



Usually these are the symptoms:

Poor optical power

Erratic current seen on the laser current meter

Snapping or arching sounds coming from the supply

With the lights out arching or corona can be observed around the HVT.



If you suspect the HVT they are not that expensive I would just replace it, tinkering with that supply is dangerous. If that is not your problem I can assure you that sooner or later you will need to replace the HVT so having a spare is a good idea anyway.


---
**Jamie Richards** *July 25, 2018 14:56*

I have a 50 watt supply coming, but I'll definitely buy a spare transformer.  Thanks!


---
**Jamie Richards** *July 25, 2018 18:09*

Hmm, curiously wondering how well wiring in 2 piece 60 watt transformers would work...  


---
**Don Kleinschnitz Jr.** *July 25, 2018 19:55*

What do you hope to accomplish. You don't want to overdrive your lasers it will shorten the life without much benefit. 


---
**Jamie Richards** *July 25, 2018 20:01*

I'm just a very curious person.  I have an 800mm tube right now, but I want to see about eventually sticking in a 1200mm, and if I can convert the power supply rather than buy a new one, it would be more cost effective.  I've had to learn how to be frugal the past few years. 


---
**Jamie Richards** *July 28, 2018 22:23*

New power supply fixed the problem.  Going to order a spare HV transformer.  I can cut through Lowe's 1/4" primed utility ply at 6mm/s in one pass at 17ma.  Original tube could do it at 4mm/s in one pass, so there is a slight improvement


---
**Kelly Burns** *July 28, 2018 23:30*

**+Jamie Richards** good deal.  Figured that was problem


---
**Jamie Richards** *July 29, 2018 02:35*

Before changing the supply, it was taking several passes.  The tube is more vibrant looking now, too!


---
**Don Kleinschnitz Jr.** *July 29, 2018 02:57*

**+Jamie Richards** certainly, now the tube is fully ionizing. :).


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/6GknX5eSkNj) &mdash; content and formatting may not be reliable*
