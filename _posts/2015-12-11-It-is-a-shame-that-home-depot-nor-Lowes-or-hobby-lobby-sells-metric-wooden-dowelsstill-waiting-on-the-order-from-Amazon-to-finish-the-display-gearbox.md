---
layout: post
title: "It is a shame that home depot, nor Lowe's or hobby lobby sells metric wooden dowels....still waiting on the order from Amazon to finish the display gearbox!"
date: December 11, 2015 16:16
category: "Discussion"
author: "Scott Thorne"
---
It is a shame that home depot, nor Lowe's or hobby lobby sells metric wooden dowels....still waiting on the order from Amazon to finish the display gearbox!





**"Scott Thorne"**

---
---
**ChiRag Chaudhari** *December 11, 2015 16:50*

Yeah I know the frustration man. Anything metric is either impossible to find there or outrageous priced. I needed at least 16 pcs of 2mm flat washers, darn things $0.17 / pc that is $2.72 + tax. Purchased them online for $0.99/100 pcs. 


---
**Scott Thorne** *December 11, 2015 16:52*

Crazy right....that's why I love Amazon.


---
**Gary McKinnon** *December 11, 2015 17:48*

Online shopping rules. I buy all my clothes from China :)


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/MTvhgLEj8Gk) &mdash; content and formatting may not be reliable*
