---
layout: post
title: "HI Guys, SOS I have a problem with My Laser, I think the Tube is dead But I cant be 100% sure"
date: September 23, 2017 17:39
category: "Original software and hardware issues"
author: "Philip Travis"
---
HI Guys,



SOS



I have a problem with My Laser, I think the Tube is dead But I cant be 100% sure.



The Laser output is less the more power you put into it, over about 9 mA. This started happening a few weeks back but its got considerably worse in the last week. 



I am now cutting at 4mm/s and 8mA



Once I get to about 11 mA, there is no output at all.



Observations, 

The laser gets brighter at the back of the machine.

The output stops at the laser its self (I.e. the laser does not hit the first mirror)

The Laser seems to be arching internally to the Metal piece towards the front of the Laser, but I have seen reports that that is normal.



Things I have ruled out 

Dirty Water (although I am not using De-ionised water). I have completly flushed everything as much as possible

Mirror miss-alignmenet

Arcing from the red lead (I have removed the lead and reconnected it., no difference at all....)





On a side note, does anyone know any suppliers of lasertubes in the UK?????





**"Philip Travis"**

---
---
**Philip Travis** *September 23, 2017 18:28*

, This is not my video btw but this is the problem. He fixed his with 

deionised water, that didnt fix my problem...  


{% include youtubePlayer.html id="si7VySd86J0" %}
[https://youtu.be/si7VySd86J0](https://youtu.be/si7VySd86J0)


---
**Nate Caine** *September 23, 2017 18:34*

Your description is very confusing.  You refer to the "back of the machine" and "front of the laser".



Perhaps a picture of your laser in operation would help.



---------------



Some of what you describe sounds more like a Laser Power Supply problem rather than a Laser Tube problem.  Have you seen any arcing in the power supply or heard any snaps or sizzle there?



Sometimes with the room lights off, you can see arcs or corona discharge that is washed out when the room lights are on.



----------------



The end where the High Voltage lead is attached is the <b>Anode (+)</b>.  That's the end where your see a glass coil inside the tube. The internal mirror (that is part of the laser tube) is 100% reflective at this end, so no laser light exits here. The coolant enters at this end of the tube.



The end where the laser beam exits is the <b>Cathode (-)</b>.  The internal mirror at this end is only partially reflective, so the laser energy can exit here.  The coolant exits at this end of the tube.



In the normal operation of the tube you should see a faint pink or violet color plasma entending only <i>between</i> the metal electrodes at the Anode to the Cathode ends.  The plasma should be consistent along the span but will often "dance" around at the electrodes, especially at the Cathode.  This is completely normal.



<i>To be a little technical here:</i>  The pink color you see is a <i>side effect</i> of the plasma discharge.  The vast majority of the energy in the electrical discharge is transferred to the N2 (nitrogen) molecules in the laser tube gas.  And the N2 molecules transfer that energy to the CO2 (carbon dioxide) molecules.  In the "stimulated emission" part of the process the CO2 molecules give up their energy, in a very structured manner, but it's in the invisible infrared part of the spectrum.  



So within that pink plasma, unknown to you, is a bunch of infrared energy bouncing back and forth between the two end mirrors.  If you look, you will notice that the anode and cathode electrodes are actually hollow cylinders.  Hollow, to allow the infrared light to pass thru them.  And, as mentioned earlier, a portion of that infrared energy is allowed to escape thru the partial mirror at the cathode end, and that's the laser beam that you get to use in the machine for burning stuff.



<i>(Actually, the process is only about one-sixth efficient.  That means that to get 40-Watts of usable infrared laser energy out, that about 240-Watts of electrical energy enters the laser tube.  The difference, roughly 200-Watts, is waste heat, and must be removed using the liquid coolant, to keep the laser tube at a usable temperature range and keep the tube from cracking.)</i>




---
**Don Kleinschnitz Jr.** *September 23, 2017 19:13*

In addition to looking for arc as **+Nate Caine** suggests would not throw out conductive water a factor.



The symptoms and description sounds just like mine did when my water was conductive.



Ideally you could measure the waters conductivity but that requires a meter. 



Is this the same water source you have always used?



Are you adding anything to the water?



Also does the current meter jump when in operation especially at the currents that the power drops.



Can you measure a voltage in your water bucket?


---
**Nate Caine** *September 23, 2017 19:37*

Yeah, as **+Don Kleinschnitz**​ suggest, it's almost a "no brainer" to just change out the water with distilled water just to exclude that from the possiblities.  Cost you only $3.



A question would be, what happened to the water from the time it was working to now?


---
**Philip Travis** *September 24, 2017 12:33*

Ok, first things first, some video. 

This shows the ampage going up but the laser getting weaker. 


{% include youtubePlayer.html id="0KzYujLRyhI" %}
[youtube.com - K40 laser problem](https://youtu.be/0KzYujLRyhI)


---
**Philip Travis** *September 24, 2017 12:35*

This shows the internal arc I mentioned. 




{% include youtubePlayer.html id="68UqPpRUyPQ" %}
[youtube.com - K40 laser problem.](https://youtu.be/68UqPpRUyPQ)


---
**Philip Travis** *September 24, 2017 12:36*

This shows the laser tube getting brighter, beyond the point the output stops.




{% include youtubePlayer.html id="ZHS5TmNQyPg" %}
[youtube.com - K40 laser problem](https://youtu.be/ZHS5TmNQyPg)


---
**Philip Travis** *September 24, 2017 12:37*

This shows a whistle from the power supply  as the mA is turned uo. It's not there all the time. It also shows no arcing inside the psu. 




{% include youtubePlayer.html id="a1-17IFnrTg" %}
[youtube.com - K40 laser power supply whistle.](https://youtu.be/a1-17IFnrTg)


---
**Philip Travis** *September 24, 2017 12:46*

**+Nate Caine**, I have checked the psu. There is no arcing but there is a whistling sound. See my last video. 



I have closed the curtains in the videos showing the tube, but I will get some darker shots later on tonight. 



_<s>---------</s>



**+Don Kleinschnitz** I've been using tap water most of the time, (stupid me) so the source was the same. Because of this though, there is some scum in the rubber tube and in laser it's self.... Removing it is extremely difficult. I am using deionised water now but it changed nothing. 



There is nothing in the water other than water and the scuz in the tube... 



I will measure the water later on. 



The meter does not jump and seems to move like it should.



Thanks for your help so far guys. 


---
**Don Kleinschnitz Jr.** *September 24, 2017 14:56*

**+Philip Travis** it is hard to determine whether a power supply or tube is bad in a K40 that is acting up like this.



In cases like this its is useful to be systematic in thinking & troubleshooting and "trying stuff" can reveal hints as to what is really wrong.

A long rambling of "thinking out loud" below.

......................

<b>Symptoms</b>

In this case both LPS and tube seem to be capable of producing the correct power under certain conditions. 



As you have observed there seems to be a threshold where additional electrical power is not being converted to light but is being applied to the system properly. At this point the tube discharge is bright and stable except for the occasional jump at the anode (which may or may not indicate a problem).



<b>LPS</b> 

 At the threshold the LPS is under stress (the whistling noise). The meter reads increasing current as it is adjusted to higher than normal levels. That amount of current is at the LPS limit.  The key question is what happens to the light conversion at this threshold since:

....The power supply puts out more current

....The discharge glow is brighter



Unlikely culprits:

1a). Conductive water: Although your water exchange suggests the water is ok, I would get a meter and measure the water. Having a conductivity meter is a good thing anyway for laser cutter water management.  They can be gotten for reasonable $ on eBay and amazon. 



.....Its unlikely this is the problem but a measurement would check it off and insure we did not miss something ...



1b). HV breakdown: Perhaps somewhere there is a HV short circuit that occurs at higher power settings. You may be able to see this with the lights out. 

What discounts this possibility is that usually breakdowns can be seen as pulses in the meter and power drops. This machine is not behaving this way. 

In my experience these supplies are stable unless the HV transformer is damaged and in that case the output power and ionization would be lowered which it is not!

..or

the power supply is open-ended in which case it will arc violently and there will be lots of corona from  the HVT and the tube will not discharge !



Neither seem to be your case.....



I bet that the LPS is ok since it puts out current proportional to the meter setting. The noise from the supply is because it is stressed at more than normal current when the knob is adjusted all the way up...



<b>Tube</b>

To me the video shows the tubes ionization operating pretty normally. 



Since:

... the tube operates normally up to the threshold and 

... the discharge gets brighter when the LPS is turned up 

yet 

... laser output light gets dimmer 



<b>It seems the problem is inside the tube!</b>



<b>The tube is drawing more current as the LPS output is increased and the discharge suggests it should put out more light!</b>



<b>Potential culprits</b>

At the threshold something is happening with the internal optics or gas conversion?



Is the water at the proper temperature?



What does the ionization at the cathode end look like at the point of threshold?



<b>Test:</b> adjust the current to the point of threshold. [Don't go much above as all that will do is stress the supply.] At threshold take a video showing a closeup of the anode and more importantly the cathode end and mirror(s). Maybe that video will give us a hint?  


---
**Philip Travis** *September 24, 2017 16:30*

HI **+Don Kleinschnitz** Thanks for all that, I will do some further testing ASAP. 



However, 

I gather there should be no current in the water? if that's correct, then that is my problem. I just dipped a finger in and there is a current in there. 

I have a multi meter, set it to 200k on  Resistance (Omega) and I get a reading of 11.



Tap water gives me a 50 reading and a fresh container of De-Ionised water give me nothing.



So I will be flushing my system ASAP, Its in the middle of a job at the moment and wont be free for about 3 hours. 



Its just odd how its worked fine on tap water for a ling time and its just suddeny stopped. Fingers crossed a full flush will help.

<s>----</s>

Regarding cleaning the insides properly, what would you recommend?


---
**Philip Travis** *September 24, 2017 21:52*

**+Don Kleinschnitz** & **+Nate Caine**



I have now flushed the system with Deionised water and set it off again with Deionised water and it does seem quite a bit better but still not 100%.



I have done some Tests and it takes 3 passes to cut out now, at a wopping 6 mm/s, down from 4 passes at only 4 mm/s.... 



Furthermore, I can get an out put at 13mA (up from about 9mA, Although the output that hits the wood at any strength is still is not as strong as it was a week ago.



Also, I no longer get a shock from putting my hand in the water and my conductivity in the system is 0.



So, I have 4 more containers of water coming on Tuesday, I will use 1 to flush the system again (perhaps even 2) and see where we end up. I am going to upload a video of the tube in the dark as was suggested earlier.

<s>---</s>






---
**Nate Caine** *September 24, 2017 22:05*

Ain't that a hoot.  



3 passes @ 6mm/s  versus 4 passes @ 4mm/s is a Two-to-One improvement.



You need to go back and reconstruct the scene of the crime.  

i.e.  What changed from when it was working, to when it failed?  



In your opening post, you ruled out the water (even though using only tap water).  Regardless, your water didn't suddenly become conductive on it's own.  So there must be another piece of the puzzle here.



Either way:  Congrats! 



---------------

BTW, "deionized" water and "distilled" water are two different things.  Probably another thing the forum needs to investigate.


---
**Don Kleinschnitz Jr.** *September 25, 2017 03:15*

**+Philip Travis** go figure ..... that is exactly what happened to me without the the threshold problem???? In my case I was also adding RV sanitize which everyone said was OK and because I was to stubborn to believe conductive water was a factor.

I only tested <b>distilled water</b> [not deionized] with Clorox and also algaecide additives all other bets are off, best to get a conductivity meter.

I suspect lots of good tubes hit the trash due to this misunderstanding ....

The "shocking bucket" also seems to accompany this problem....



When you say the "conductivity of the system is 0" what do you mean. Be advised you cannot measure water conductivity accurately with a DVM.




---
*Imported from [Google+](https://plus.google.com/117996532194935420857/posts/TbF2YG2uMZV) &mdash; content and formatting may not be reliable*
