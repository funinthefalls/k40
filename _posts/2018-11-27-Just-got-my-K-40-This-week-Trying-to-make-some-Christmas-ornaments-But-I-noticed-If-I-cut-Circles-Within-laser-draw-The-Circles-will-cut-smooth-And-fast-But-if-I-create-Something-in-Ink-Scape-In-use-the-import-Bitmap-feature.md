---
layout: post
title: "Just got my K 40 This week Trying to make some Christmas ornaments But I noticed If I cut Circles Within laser draw The Circles will cut smooth And fast But if I create Something in Ink Scape In use the import Bitmap feature"
date: November 27, 2018 16:28
category: "Software"
author: "Timothy \u201cMike\u201d McGuire"
---
Just got my K 40 This week Trying to make some Christmas ornaments 

But I noticed If I cut  Circles Within laser draw The Circles will cut smooth And fast But if I create Something in Ink Scape In use the import Bitmap feature In Laserdrw The cut is sporadic Not smooth at all Any ideas  It's not just circles but anything I import

Thanks





**"Timothy \u201cMike\u201d McGuire"**

---
---
**HalfNormal** *November 27, 2018 17:51*

Engrave and cut are two different functions. A quick search on that should help.


---
**Scorch Works** *November 29, 2018 05:32*

The issue you are seeing might be  due to low bitmap resolution.  Make sure you make a high resolution image from Inkscape.  500 DPI or even 1000 DPI.  (Anything over 1000 DPI is a waste.)


---
*Imported from [Google+](https://plus.google.com/109629943464502534866/posts/ZZjdZK11VwK) &mdash; content and formatting may not be reliable*
