---
layout: post
title: "I would like to order machine tonight, but I would like some feedback on who to order from and who not to order from"
date: December 06, 2015 23:26
category: "Discussion"
author: "Scott Howard"
---
I would like to order machine  tonight,  but I would like some feedback on who to order from and who not to order from. ...also can I make the k40 run on any other kind of software other than what comes with it?  Thanks for all you help





**"Scott Howard"**

---
---
**Gary McKinnon** *December 06, 2015 23:38*

I got mine from ebay UK, if you're in the UK i'll post the link. Wherever you are, make sure you get the latest model that has the M2 Nano board.


---
**Scott Howard** *December 06, 2015 23:52*

I'm in u.s. what is the m2 nano board? 


---
**Gary McKinnon** *December 07, 2015 00:13*

The driver board in the right-side panel near the power supply. Here's the M2 Nano : [http://www.3wcad.com/](http://www.3wcad.com/)


---
**FNG Services** *December 07, 2015 00:46*

If your in the USA, I ordered mine from here [http://www.ebay.com/itm/200902245694?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/200902245694?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) It shipped from CA.. I ordered it on 11/9 and recived tracking that day. I recived the laser on 11/12. Via FedEx It came with the current M2 Nano board, CoarlLaser and LaserDraw software. It was packed well and has been working great. 



Now this was my experience with this seller, that's not to say they do things this way all the time.


---
**Scott Howard** *December 07, 2015 00:48*

Thank you


---
**Scott Howard** *December 07, 2015 00:48*

How big is the work area?


---
**Gary McKinnon** *December 07, 2015 00:56*

When you replace the cuting bed, 300x200mm.


---
**Scott Howard** *December 07, 2015 00:58*

Ok,thank you


---
**Gary McKinnon** *December 07, 2015 01:04*

There's a document here on the group somewhere that lists all of the things you can do to improve the machine.


---
**Scott Howard** *December 07, 2015 01:11*

I'm new to the group, where would I find that. .I'm ordering that machine now. ....thank you


---
**Brooke Hedrick** *December 07, 2015 01:40*

Avoid all things MoshiDraw.


---
**Scott Howard** *December 07, 2015 01:41*

Ok thanks


---
*Imported from [Google+](https://plus.google.com/113905238524886327481/posts/YppXa1xexJU) &mdash; content and formatting may not be reliable*
