---
layout: post
title: "Laser cutting as a business. Just leather, laser and some confidence"
date: May 21, 2017 03:31
category: "Object produced with laser"
author: "Paul de Groot"
---
Laser cutting as a business. Just leather, laser and some confidence. Maybe a good opportunity for a  k40 laser and a controller 😜



![images/dba80cb4054167f3ab7aaf99f0f18ed4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dba80cb4054167f3ab7aaf99f0f18ed4.jpeg)
![images/df40a6deb9c0305f7ce5aa41800cda23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df40a6deb9c0305f7ce5aa41800cda23.jpeg)
![images/8c2f54a5f47c02f2f71f4be13df090a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8c2f54a5f47c02f2f71f4be13df090a6.jpeg)
![images/5ee16c068c86111ecad8f77f797d9aa7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ee16c068c86111ecad8f77f797d9aa7.jpeg)

**"Paul de Groot"**

---
---
**Joe Kyle** *May 21, 2017 13:46*

Some nice work there. Where is this at?


---
**Paul de Groot** *May 21, 2017 13:47*

**+Joe Kyle** this at the San Mateo makerfaire. Would highly recommend to visit this once in your life😊


---
**Joe Kyle** *May 21, 2017 13:52*

Sure would like to be there, on my list for someday. Been to makerfaire in Detroit and New York, but not out west yet.


---
**Richard Vowles** *May 21, 2017 18:29*

Cool looking hats, are they yours? And if so do you have a URL?


---
**Paul de Groot** *May 21, 2017 23:12*

**+Richard Vowles** i wish they were. I just attended the makerfaire 


---
**Jeff Johnson** *May 23, 2017 14:16*

Are you cutting the leather? How do you deal with the smell? I've got a decent wallet making hobby but haven't had luck cutting leather with my laser. However, I have had lots of success making acrylic templates to help in hand cutting and stitching.


---
**Paul de Groot** *May 23, 2017 15:50*

**+Jeff Johnson** I just make the drop in controllers for the K40. I have cut some leather and indeed the smell is awful. These guys had a charcoal filter and we're outside on the Makerfaire.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/399TjZAy4MY) &mdash; content and formatting may not be reliable*
