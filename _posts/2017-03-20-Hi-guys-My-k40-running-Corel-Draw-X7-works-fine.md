---
layout: post
title: "Hi guys, My k40 running Corel Draw X7 works fine"
date: March 20, 2017 01:57
category: "Discussion"
author: "John Tran"
---
Hi guys, 



My k40 running Corel Draw X7 works fine. from time to time i use fonts instead of PNG files of TEXT. From to time. my font choice is completely cropped down when i am engraving and ill lose the product.



for example.. if it words are 4 inches long and 1 inch tall..

the K40 will only zap about 3.5inch x .5 in tall.



I will only a cropped portion of the text.

Ive converted to bit map and also retraced it once its an image. im out of ideas.

it will do this on SOME but not ALL fonts i use in CorelDrawX7. 

Any help would be amazing.



Thanks,

John







![images/650ecdd8ef2893af9d180b2f507e8c62.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/650ecdd8ef2893af9d180b2f507e8c62.jpeg)
![images/d41bbc055861a0a9250c5fbbe359b886.png](https://gitlab.com/funinthefalls/k40/raw/master/images/d41bbc055861a0a9250c5fbbe359b886.png)

**"John Tran"**

---
---
**Ned Hill** *March 20, 2017 13:03*

Is it cropped in the preview?  Also what is an example font this happening with?  What engrave output file type are you using?




---
**John Tran** *March 20, 2017 17:37*

Hi Ned, It's not cropped in the preview before engraving its acts completely normal. When it comes time to zap.. its cropped. 



one example of a font is  "OXIN"

[dafont.com - Oxin Army Font &#x7c;](http://www.dafont.com/oxin-army.font)



or another similar, EXOSTENCIL.

[http://www.dafont.com/exostencil.font](http://www.dafont.com/exostencil.font)



I've zapped it as TEXT and as a converted BMP/PNG file.



Still confused.






---
**Ned Hill** *March 21, 2017 03:03*

If you want, link me a file and I'll give it a try.




---
**John Tran** *March 21, 2017 07:08*

download OXIN ARMY font, and shoot a text onto something...if it doesnt work for you, its the font itself.


---
**Ned Hill** *March 21, 2017 13:20*

**+John Tran** I tried the font and it worked fine for me.  I used BMP as the output file type.


---
**Jeff Johnson** *April 06, 2017 21:34*

I've had several fonts do this to me and I always convert the font to curves and they engrave and cut properly after that.


---
**'Nstabooth Photobooth Services** *April 27, 2017 19:55*

Jeff and ned, now its doing it to fonts that are just standard and that ive used forever. what gives?


---
**Jeff Johnson** *April 27, 2017 21:03*

I never tried to troubleshoot my font problem after I discovered converting to curves fixed it. Have you tried converting the font to curves? At that point the text should be seen as lines just like everything else.




---
*Imported from [Google+](https://plus.google.com/113069068176332761588/posts/NcETT7hQzsF) &mdash; content and formatting may not be reliable*
