---
layout: post
title: "Hi I have a K40 and love it!"
date: February 13, 2017 12:06
category: "Discussion"
author: "Surrey Woodsmiths"
---
Hi



I have a K40 and love it!  



I would like to upgrade to something a little bigger and more powerful and was wondering if anybody has used either of these Chinese machines on Ebay? I believe one is an XB-1060 and the other is a VEVOR



I am looking at either 80w or 100w and around 100mm x 600mm cutting area and, ideally, a machine that will allow me to assign different colours to different laser settings (to engrave and cut in one job)



There is virtually no information on these so I thought it would be great to see if anybody has used them or could recommend which is best?



Many thanks







![images/97b1049af760ed9ee9cc541eaa490784.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/97b1049af760ed9ee9cc541eaa490784.jpeg)
![images/f56d77d6ca549a1947a223723c251c98.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f56d77d6ca549a1947a223723c251c98.jpeg)

**"Surrey Woodsmiths"**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 13, 2017 12:09*

"Laser engraving and cutting" group on Facebook. Lots of folks with machines like that. 


---
**Surrey Woodsmiths** *February 13, 2017 12:14*

Thanks Ray... I have just requested membership to that FB group


---
**Maker Cut** *February 13, 2017 14:57*

If you are in the UK, check out Thinklaser. Russ has one and talks about it here: 
{% include youtubePlayer.html id="MTUJUkT9cxo" %}
[youtube.com - RDWorks Learning Lab 84 My New Adventure](https://youtu.be/MTUJUkT9cxo)


---
**Surrey Woodsmiths** *February 13, 2017 16:24*

Interesting, funnily enough they are just down the road from me!  I have sent an enquiry


---
*Imported from [Google+](https://plus.google.com/105599569700790157394/posts/DAGfjyxG2Rc) &mdash; content and formatting may not be reliable*
