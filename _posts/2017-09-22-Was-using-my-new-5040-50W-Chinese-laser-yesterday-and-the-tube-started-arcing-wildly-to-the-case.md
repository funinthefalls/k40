---
layout: post
title: "Was using my new 5040 50W Chinese laser yesterday and the tube started arcing wildly to the case"
date: September 22, 2017 23:58
category: "Discussion"
author: "Anthony Bolgar"
---
Was using my new 5040 50W Chinese laser yesterday and the tube started arcing wildly to the case. Managed to hit the e-stop within about 20 seconds. Checked the HV connection, the red HV positive wire had burned through the insulation in a very small spot. I Cleaned the positive post off on the tube, wrapped the red wire again and insulated using clear silicon. Today it was dry, fired up the machine and the tube is dead. Emailed the seller and he is sending the tech rep out with a new tube on Monday. I will just keep the new tube as a spare as I have a 60W tube that I will install this weekend. I love buying local, much better service than an ebay purchase, luckily this sell had ebay pricing available for local pickup. I was sceptical at first about the 1 year on site warranty if it was within 60 miles (100km) of the seller, but they are honouring the warranty without any problems. The company is ASC365.com, they have the main warehouse in Toronto, Ontario, Canada, with another warehouse in California somewhere. Good bunch of people.





**"Anthony Bolgar"**

---
---
**Ned Hill** *September 23, 2017 01:22*

Make sure the seller's tech doesn't need to verify the tube is dead before you replace it with the one you have.


---
**Anthony Bolgar** *September 23, 2017 23:47*

Yup, thanks for reminding me. I'll wait till he gets here to find out the score. Thanks Ned.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/3WMurd42ypx) &mdash; content and formatting may not be reliable*
