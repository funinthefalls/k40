---
layout: post
title: "Well I have just ordered: 1 of: Improved 18mm ZnSe Focus lens (F50.8mm) 1 of: 18mm Laser head w/ air assisted"
date: January 19, 2016 10:17
category: "Modification"
author: "Tony Schelts"
---
Well I have just ordered:    

1 of: Improved 18mm ZnSe Focus lens (F50.8mm)

1 of: 18mm Laser head w/ air assisted. Ideal for K40 machine

 

Now the question is, is it possible for someone without any electronic training to upgrade the board, etc?????. Or without any programming training?





**"Tony Schelts"**

---
---
**Stephane Buisson** *January 19, 2016 10:53*

**+Tony Schelts**  Changing the board isn't a big deal. I agree without a proper tutorial, it look scary. but it's something repeatable ( so no need of to be electronic engineer like **+Peter van der Walt** & **+David Cook**  ), just the inconvenience of fitting connectors and follow few cables, screws & nuts.

I did write my experience for other to look at

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)

take it as a first attempt, hopefully more tutorial will come up.

And for the softwares it's already there and easy to look at.


---
**Tony Schelts** *January 19, 2016 11:13*

Thanks Stephane,  I guess i better start saving.  does the upgrade make he machine more versatile?


---
**Anthony Bolgar** *January 19, 2016 12:00*

The upgrade increases the ease of use and reliability of the software.


---
**Pete OConnell** *January 19, 2016 12:31*

Seems we had the same shopping list......apart from mine also included an air pump, tubing and a smoothie board plus some drag chain :)


---
**Stephane Buisson** *January 19, 2016 13:13*

**+Tony Schelts**  my best answer would be to look at my today Workflow post


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/gp2RbuNyREr) &mdash; content and formatting may not be reliable*
