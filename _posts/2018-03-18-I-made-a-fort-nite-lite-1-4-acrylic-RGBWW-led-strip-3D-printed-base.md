---
layout: post
title: "I made a fort-nite-lite :) 1/4 acrylic RGBWW led strip 3D printed base"
date: March 18, 2018 01:00
category: "Object produced with laser"
author: "Printin Addiction"
---
I made a fort-nite-lite :)



1/4” acrylic

RGBWW led strip

3D printed base







![images/41bd55e48aec50a666098a3ea0f325f7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41bd55e48aec50a666098a3ea0f325f7.jpeg)
![images/49423def6273cf46329758775ec716df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/49423def6273cf46329758775ec716df.jpeg)
![images/bb17ef1f64ae1c9b5136534d3795e056.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bb17ef1f64ae1c9b5136534d3795e056.jpeg)

**"Printin Addiction"**

---
---
**Don Kleinschnitz Jr.** *March 18, 2018 14:03*

Nice work, its fortunite that you have a laser cutter/engraver....


---
**Printin Addiction** *March 18, 2018 17:39*

**+Don Kleinschnitz** ah touche’. :)


---
**Abe Fouhy** *March 19, 2018 01:03*

Idea to completion = 2 weeks :p


---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/EVxkC1j4LZr) &mdash; content and formatting may not be reliable*
