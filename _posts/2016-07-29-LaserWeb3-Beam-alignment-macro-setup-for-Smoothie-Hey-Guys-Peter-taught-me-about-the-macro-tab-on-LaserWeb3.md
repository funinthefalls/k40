---
layout: post
title: "LaserWeb3 Beam alignment macro setup for Smoothie Hey Guys, Peter taught me about the macro tab on LaserWeb3"
date: July 29, 2016 20:20
category: "Hardware and Laser settings"
author: "David Cook"
---
LaserWeb3   Beam alignment macro setup for Smoothie



Hey Guys,  Peter taught me about the macro tab on LaserWeb3.

So I used it to setup some Macros.  I really liked the alignment procedure in this guys video 
{% include youtubePlayer.html id="wY5D27TQwZI#t=57.600075" %}
[https://www.youtube.com/watch?v=wY5D27TQwZI#t=57.600075](https://www.youtube.com/watch?v=wY5D27TQwZI#t=57.600075)



I also liked how his DSP controler has pre-set head positions to make this process easier.



So I created a similar deal in LaserWeb3.

created a button to test fire the laser at 20%

created 4 more buttons:

Left-Rear

Right-Rear

Left-Front

Right-Front



you may need to play with some of the gcode values to get the movement you want. but this gives a clear idea of how to do it.



The test-Fire command is clever as it uses a G1 Z move  to fire the laser as it moves the Z position. ( this way the head does not move at all when test firing)



macro setup [https://drive.google.com/open?id=0ByNecjb0RD-TbDRtQ2VVZFZ5aE0](https://drive.google.com/open?id=0ByNecjb0RD-TbDRtQ2VVZFZ5aE0)



control panel [https://drive.google.com/open?id=0ByNecjb0RD-TZU4xcEV1ZElsOTQ](https://drive.google.com/open?id=0ByNecjb0RD-TZU4xcEV1ZElsOTQ)



I want to thank  **+Peter van der Walt** and everyone else involved with the LaserWeb project.  It really has made tinkering with this K40 a lot of fun for me.









**"David Cook"**

---
---
**Jim Hatch** *July 29, 2016 21:56*

Cool. When Peter's got the Z axis stuff working though (for things like a rotary table attachment for glasses and bottles and such) we'll just have to remember not to fire the test with the rotary enabled.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/i7Ed8G4a1FA) &mdash; content and formatting may not be reliable*
