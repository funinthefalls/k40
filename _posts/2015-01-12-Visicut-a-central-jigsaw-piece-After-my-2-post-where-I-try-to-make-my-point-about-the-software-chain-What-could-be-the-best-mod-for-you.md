---
layout: post
title: "Visicut: a central jigsaw piece ! After my 2 post where I try to make my point about the software chain: What could be the best mod for you ?"
date: January 12, 2015 00:07
category: "Software"
author: "Stephane Buisson"
---
Visicut: a central jigsaw piece !



After my 2 post where I try to make my point about the software chain:

What could be the best mod for you ? (your expectations) 24/11/2014

What a good tool chain should look (Software tool chain thinking) 30/11/2014



I am coming back to underline and help understanding on why Visicut is a central piece of the jigsaw, and why it will become the standard (being under the LGPL licence will help a lot).



+Thomas Oster in is thesis did a analysis of big picture of the situation and find out a solution to solve the heart of the problem.

No laser cutting standard, different design softwares and operating systems, proprietary hardware and drivers issues to start with.



by developing Visicut for Linux/Mac/Windows, he solve the multi OS question for most of us. Visicut is not a design software, and yes the user can continue to use his favourite one, Visicut is compatible with most of design software using the most common file format, just respect the cutting/engraving setting while creating your 'print out file' to process.

the question of positioning (origin of file/origin of k40) is solved with the webcam option, allowing a previewing of the future result.

Creating a 'Material' option (eg: perspex 3mm), to define and store the cutting setting (speed, intensity), Visicut will save us a lot of mistakes/materials.

as well as a plugin approach (eg: to print directly from Inkscape)



The heavy work he is doing now is to develop the last bit of software to complete the tool chain, the drivers to match some hardware controller board. Thank you Thomas for your hard work.



Will arrive very soon a time for laser cutter brands to be or not to be compatible with the Visicut standard.





**"Stephane Buisson"**

---
---
**Stephane Buisson** *January 12, 2015 00:08*

[https://github.com/t-oster/VisiCut/wiki/VisiCut-manual](https://github.com/t-oster/VisiCut/wiki/VisiCut-manual)


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/FfnKvCCRb5G) &mdash; content and formatting may not be reliable*
