---
layout: post
title: "merry Christmas to everyone! After week of trying with no luck i want to ask for your help.."
date: December 25, 2016 23:16
category: "Modification"
author: "Kostas Filosofou"
---
merry Christmas to everyone!



After week of trying with no luck i want to ask for your help.. 

i have almost finish with the rebuild of my 40w laser and i have make some (not so good) test cuts.. But .. i have some issues with the mirror alignment as you can see in the photos my axes (X and Y) are perfect aligned and my tube also but i can't center the X axis as much as I try with screws of the mirrors , the beam drifts too match as you can see in the first picture . In the second picture i have move my tube far back from the first lens .. i don't know if this is the problem .. I would appreciate any help.



![images/cf84ae46b1b1f352c4fe79c2dfec7327.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf84ae46b1b1f352c4fe79c2dfec7327.jpeg)
![images/343d7dbcd5f90c2259c0b55e7b893b0d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/343d7dbcd5f90c2259c0b55e7b893b0d.jpeg)

**"Kostas Filosofou"**

---
---
**greg greene** *December 25, 2016 23:29*

When you say aligned - are they aligned as close as possible to the center of the mirrors?




---
**Kostas Filosofou** *December 25, 2016 23:50*

Yes


---
**greg greene** *December 26, 2016 00:03*

What I do is this, align tube to center of fixed mirror, align traveling mirror by adjusting fixed mirror, align center of opening in head to traveliing mirror by adjusting travelling mirror, align center of lens by shimming twist of head if needed.


---
**Kostas Filosofou** *December 26, 2016 00:15*

I have tried it. The tube is dead center with the mirror. The problem is to the second mirror and to the head i can't fix it no matter what. I think the problem begins from the tube ... But i am not sure


---
**greg greene** *December 26, 2016 00:22*

You could be right, but if the beam from the tube hits the centre of the fixed mirror then it should all be good, here's a hint - switch the first and second mirrors and see if the problem follows the mirrors, if so - you have a flaw in the mirror.  I had exactly the same pattern as you show on my machine - on the second mirror, but it turned out that the beam from the tube was not hitting exactly centre of the first mirror.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 26, 2016 01:02*

The question is, are your rails square? I'd imagine alignment problems like this could be caused by your rails not being square, so as the travelling mirror moves on the X-axis it is ending up a few mm out from the beam.


---
**Kostas Filosofou** *December 26, 2016 07:53*

**+greg greene**  ill try it .. ill try to bring the tube front and very close to the mirror.. 


---
**Kostas Filosofou** *December 26, 2016 08:00*

**+Yuusuf Sallahuddin** Yes as i mentioned to my first post is pretty aligned ... do not justify such a big drift of the beam


---
**Ian C** *December 26, 2016 08:20*

Looks to me like your first fixed mirror isn't inline with the second mirror, so as the end mirror move away in the Y axis the beam is too much offline to bring back. You've not said which way the beam is moving as the Y axis moves away from the the tube, so can't advise if the end mirror needs moving closer to or away from the front of the laser tube 


---
**Kostas Filosofou** *December 26, 2016 08:51*

**+Ian C** hmm maybe.. I have to check this out also.


---
**Kostas Filosofou** *December 28, 2016 12:24*

**+Ian C**​ you were right! The mirrors was not inline.. after relocate them the beam is dead center! 


---
**Kostas Filosofou** *December 28, 2016 12:26*

Any tips for finding the optimal high for the focus lens ?﻿ I have the stock lenses ..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2016 13:12*

**+Konstantinos Filosofou** The simplest test for finding the optimal point for the focus is to do a "Ramp Test". Place a piece of wood on an angle in the cutting bay. Cut a line along the wood. I find horizontally placing the wood & cutting the line easiest.



You will see the line along the wood goes from thick beam to thin beam to thick again. The optimal distance is in the range where the cut is thinnest. Measure the height from your laser head to the thinnest point on the wood (without moving the wood). That should give you the measurement (from where ever you chose to measure from on the head, e.g. base of head).


---
**Kostas Filosofou** *December 28, 2016 15:46*

**+Yuusuf Sallahuddin**​ good! I will do that.. seems pretty easy solution! Thanks again!


---
**Ian C** *December 28, 2016 15:54*

**+Konstantinos Filosofou** brilliant well done. I only know as I had it myself and sat there for an hour scratching my head


---
**Kostas Filosofou** *December 29, 2016 22:22*

And after that everything was running smoothly.... my power supply explodes! in my first cut project..i never catch a break.



The question yet is... I buy only a new power supply? or i upgrade the cutter with 50w Laser tube + power supply ? the 10w difference is so significant ?


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/h8VKwhGNYUc) &mdash; content and formatting may not be reliable*
