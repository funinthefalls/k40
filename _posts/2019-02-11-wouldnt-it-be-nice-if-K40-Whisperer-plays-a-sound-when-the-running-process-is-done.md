---
layout: post
title: "wouldn't it be nice if K40 Whisperer plays a sound when the running process is done ??"
date: February 11, 2019 00:58
category: "Software"
author: "William Marin"
---
wouldn't it be nice if K40 Whisperer plays a sound when the running process is done ?? 





**"William Marin"**

---
---
**Anthony Bolgar** *February 11, 2019 17:02*

Anything that helps with unattended operation of the laser is a bad idea, and could lead to liability issues for the developer.


---
**'Akai' Coit** *February 13, 2019 15:53*

I see where you are coming from, **+Anthony Bolgar** , but what about multitaskers who just want a distinct sound to let them know it's done vs looking directly at is or hoping to notice a change in background noise? I'm a multitasker that would benefit from a notification sound because I don't always notice when the machine has stopped while sitting right next to it.


---
**LightBurn Software** *February 13, 2019 19:10*

Fire is quiet until it really gets going. Ask me how I know.


---
*Imported from [Google+](https://plus.google.com/101246131202182508749/posts/EryixDKq7ff) &mdash; content and formatting may not be reliable*
