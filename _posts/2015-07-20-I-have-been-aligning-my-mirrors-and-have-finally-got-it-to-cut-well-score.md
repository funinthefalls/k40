---
layout: post
title: "I have been aligning my mirrors and have finally got it to cut, well score"
date: July 20, 2015 16:50
category: "Discussion"
author: "george ellison"
---
I have been aligning my mirrors and have finally got it to cut, well score. Here's a photo. I cannot get the point to hit the cutting head at the same place in all four corners, any suggestions?

![images/036a2dc3728200452e2bb95cb6eaf0be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/036a2dc3728200452e2bb95cb6eaf0be.jpeg)



**"george ellison"**

---
---
**Chris M** *July 20, 2015 19:44*

I'm assuming that the four burns are from the four corners? This shows that the beam is not aligned at the 2nd mirror, which means you must adjust the first mirror to get the point to hit the same spot (ideally the centre) on the 2nd mirror at the extremes of y-axis travel. When you've done that, the beam should only make 2 marks at the extremes of the x-axis travel, and you adjust the 2nd mirror to bring those two points together.


---
**george ellison** *July 21, 2015 07:34*

Hi, finally discovered that. Got the first mirror aligned, that was the problem. I need a spacer as the beam was hitting it too high. I then got it in the centre of the 2nd mirror, then the third. Hopefully that has worked now, thanks for your comments.


---
**Bee 3D Gifts** *July 24, 2015 00:15*

Took me awhile to understand how the best way to align. I personally found it easiest to make sure laser from tube is directly in the center of the first mirror. If not I unscrew that mirror and move it front or back to align in the center of the laser directly out of the tube. Then I move the left moving mirror and see where that hits and do the same once hitting center of 2nd mirror adjust 2nd mirror screws to hit center of the 3rd mirror. Messing with those screws are touchy. So overall, align the mirrors to the laser, not the laser to the mirrors. Also make visibly sure all screws are roughly even. Oh and 1 last tip, the first mirror is the only mirror that needs to be centered. The 2nd movable mirror does not need to be centered but the laser needs to hit the same spot front to back.



Hope this helps


---
**Bee 3D Gifts** *July 24, 2015 00:17*

With our k40 we can cut 3mm acrylic at 30 power only 2 rounds. If we turn up 60+ we can cut with a single round.


---
**george ellison** *July 24, 2015 05:41*

I heard you were never meant to go past 10 power so i kept it on that max and with 3mm acrylic can cut it around 6 or 7mm sec


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/SKVXnXQFj6Y) &mdash; content and formatting may not be reliable*
