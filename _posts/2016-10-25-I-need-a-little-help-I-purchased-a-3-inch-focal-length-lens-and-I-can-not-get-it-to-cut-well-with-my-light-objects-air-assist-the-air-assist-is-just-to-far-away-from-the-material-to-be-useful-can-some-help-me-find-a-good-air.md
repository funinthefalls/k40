---
layout: post
title: "I need a little help I purchased a 3 inch focal length lens and I can not get it to cut well with my light objects air assist the air assist is just to far away from the material to be useful can some help me find a good air"
date: October 25, 2016 23:01
category: "Discussion"
author: "3D Laser"
---
I need a little help I purchased a 3 inch focal length lens and I can not get it to cut well with my light objects air assist the air assist is just to far away from the material to be useful can some help me find a good air assist that would work with. 3 inch focal length lens 





**"3D Laser"**

---
---
**Gee Willikers** *October 25, 2016 23:06*

Laser some rings decreasing in size, glue to make a hollow cone.


---
**Don Kleinschnitz Jr.** *October 25, 2016 23:33*

Is there and advantage to cutting with a 3" FL lens?


---
**Gee Willikers** *October 26, 2016 01:06*

Less hourglass shape / angled cuts but you sacrifice power if I'm not mistaken.


---
**3D Laser** *October 26, 2016 01:07*

You can cut through thicker material as well


---
**Alex Krause** *October 26, 2016 01:24*

**+Don Kleinschnitz**​ does this help you visualize it?

![images/a4d72f249173b68f9e3164f7c608986a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4d72f249173b68f9e3164f7c608986a.jpeg)


---
**Don Kleinschnitz Jr.** *October 26, 2016 02:59*

That's what I thought. You loose power because the energy density is lower do to larger spot size but the DOF is longer.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/D3tnpLz8kxz) &mdash; content and formatting may not be reliable*
