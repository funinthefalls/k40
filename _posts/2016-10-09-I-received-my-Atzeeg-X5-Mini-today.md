---
layout: post
title: "I received my Atzeeg X5 Mini today"
date: October 09, 2016 01:37
category: "Smoothieboard Modification"
author: "K"
---
I received my Atzeeg X5 Mini today. I'm working on the config file, and I was wondering if the config changes listed on the Smoothk40 guide are the same for the X5 (aside from steps per mm).





**"K"**

---
---
**Ariel Yahni (UniKpty)** *October 09, 2016 01:52*

Alpha and beta current can be different. 


---
**K** *October 09, 2016 03:30*

It's looking like the pins are different as well. I changed them per the SmoothK40 config file and my X and Y are now flipped.


---
**Ariel Yahni (UniKpty)** *October 09, 2016 03:43*

Oh yes pins can vary from board to board


---
**Alex Krause** *October 09, 2016 04:18*

[http://smoothieware.org/azteeg-x5](http://smoothieware.org/azteeg-x5)


---
**Alex Krause** *October 09, 2016 04:19*

Check out the first link at the top of the page in the link... should give you a starting point


---
**K** *October 09, 2016 04:20*

My beta axis doesn't want to move. I swapped the x and y plugs to see if it was an issue with the motors, and it's not. I also installed a new stepper driver to no avail. 🙁


---
**K** *October 09, 2016 04:21*

Thank you! That's very helpful. 


---
**Alex Krause** *October 09, 2016 04:23*

Do you have the micro stepping jumpers set?


---
**Alex Krause** *October 09, 2016 04:27*

Depending on the driver's you have you may need to change your steps per mm... honestly I would use 1/8 if you can but if your drivers are 1/16 go with it... if you use a different microstep step setting you will need to use the rep rap calculator 


---
**Alex Krause** *October 09, 2016 04:28*

The link for that is [http://prusaprinters.org/calculator/#stepspermmbelt](http://prusaprinters.org/calculator/#stepspermmbelt)


---
**Alex Krause** *October 09, 2016 04:30*

Stock smoothie is 1/16 I believe


---
**K** *October 09, 2016 04:35*

That wiring diagram shows different jumper settings than the diagrams specifically for the drivers I have. 


---
**Alex Krause** *October 09, 2016 04:37*

**+Roy Cortes**​ can you help out with this?


---
**K** *October 09, 2016 05:08*

It looks like the diagram linked to on the Smoothieware page is for v1, whereas the X5 is at v3.


---
**K** *October 09, 2016 07:35*

Hmm.. could I just remap the Y axis pins to the Z axis since that's not in use?


---
**K** *October 10, 2016 00:08*

Alright, I got my Y axis sorted out, but I still can't get my Y Axis end stop to work. I did the usual (swapped plugs, etc) to verify it's not the stop itself. Can I get someone to look at my config for the endstops to see if I have anything wrong? When I hit "home all axis" the X homes, but the Y does nothing. 

![images/1f13e29821c04c2466b438e39c6eea2a.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1f13e29821c04c2466b438e39c6eea2a.png)


---
**Alex Krause** *October 10, 2016 02:39*

Can you take a picture of your board with the estop wiring?


---
**K** *October 10, 2016 02:52*

![images/65bb5b3ad4ba6887415fe0264c3ca0c5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/65bb5b3ad4ba6887415fe0264c3ca0c5.jpeg)


---
**K** *October 10, 2016 02:54*

![images/1de8ab8c911d3e81a079b08a38d7a048.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1de8ab8c911d3e81a079b08a38d7a048.jpeg)


---
**Alex Krause** *October 10, 2016 05:15*

I'm looking into this... azteeg boards seem to vary with homing... I have a min and max set of pins on my smoothie board to home... just to double check can you jog the Y axis using laserweb or pronterface?


---
**Alex Krause** *October 10, 2016 05:19*

**+Kim Stroman**​ Just looking again at your config place a # before beta_min_endstop and delete the # before beta_max_endstop. The # is used to comment out sections of config files for future referance... any thing on the line after the # is ignored


---
**K** *October 10, 2016 05:21*

Figured it out... I had home_to_max commented out. I removed the # and it works!


---
**Alex Krause** *October 10, 2016 05:22*

And on another note I hope you are aware of using the proper text editor to make edits to a config file Gedit is the recommended text editor for smoothie. NOTEPAD and WORDPAD can cause issues if used. 


---
**K** *October 10, 2016 05:23*

I was not. Thank you. I was just using TextEdit on Mac. I'll grab the program you've recommended.


---
**Alex Krause** *October 10, 2016 05:24*

**+Kim Stroman**​ I would like to thank you again for choosing the azteeg board over the azsms or mks sbase board you originally were looking at. 


---
**K** *October 10, 2016 05:24*

It was an easy choice once you told me that the other two don't contribute to the project.


---
**Alex Krause** *October 10, 2016 05:29*

**+Arthur Wolf**​ ^^^


---
**K** *October 10, 2016 05:36*

I just realized I totally missed your comment on the # before beta_max! Thanks for looking everything over and for all your help. Now on to wiring up the power supply and laser tube!


---
**Arthur Wolf** *October 10, 2016 06:32*

aaaah ... the infamous # ....


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/YMbvMnAPbAJ) &mdash; content and formatting may not be reliable*
