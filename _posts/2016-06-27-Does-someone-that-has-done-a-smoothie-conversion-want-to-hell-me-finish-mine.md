---
layout: post
title: "Does someone that has done a smoothie conversion want to hell me finish mine?"
date: June 27, 2016 06:09
category: "Smoothieboard Modification"
author: "Alex Krause"
---
Does someone that has done a smoothie conversion want to hell me finish mine?





**"Alex Krause"**

---
---
**Jean-Baptiste Passant** *June 27, 2016 07:01*

Sure, count me in, I'll do my best to help you.


---
**Don Kleinschnitz Jr.** *June 27, 2016 12:30*

I will help as much as I can but I am in the middle of mine also. What are you missing?


---
**Alex Krause** *June 27, 2016 16:26*

I'm stuck on which pin to activate on the PSU to control the laser


---
**Vince Lee** *June 27, 2016 17:34*

There are two choices I've seen done.  First, you can control the same digital enable line that the old controller used.  I did this, and it is simple to wire as one can use the original wiring harnesses.  It also allows the pot to stay connected for on-the-fly power tweaking.  It can be pwm controlled, but I had to find the right pwm period to get a smooth ramp in power levels.  Alternatively, you can tie the enable line low (on) and pwm control the analog power level line going to the pot instead.  This is not so sensitive to the frequency since it is an analog line and thus a natural for pwm, but requires a level shifter and some have reportedly added extra stuff to keep the unit from firing if the laser is powered up before the smoothieboard.


---
**Don Kleinschnitz Jr.** *June 28, 2016 01:28*

See the "IN" sections of this post.

[http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface_86.html](http://donsthings.blogspot.com/2016/06/k40-s-laser-power-control-interface_86.html)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/codZmd6Zyqk) &mdash; content and formatting may not be reliable*
