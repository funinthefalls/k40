---
layout: post
title: "Originally shared by Arthur Wolf Hey everybody !"
date: January 15, 2015 21:49
category: "Discussion"
author: "Arthur Wolf"
---
<b>Originally shared by Arthur Wolf</b>



Hey everybody ! PLEASE SHARE THE SMOOTHIE CONTEST with your circles !



The Smoothie project is organizing a contest  in which you have the opportunity to participate in a great Open-Source project, and to win a Smoothieboard.



What’s a Smoothie ?

The Smoothie project’s objective is to design a CNC controller board and associated firmware, with the following goals : all Open-Source, simple to use and configure, using powerful hardware, feature-rich, works for lasers/3D-printers/CNC-routers and contributor-friendly.

We want to showcase the fact that the Firmware is particularly easy to contribute to and add things to, that’s why we are doing this contest.



How does the contest work ?

You enter the contest by presenting an idea for a project. The project idea must involve the Smoothie firmware or board, and make the Smoothie project better for everybody in some way. Note, you don’t need to actually do the project to enter the contest, you just need your idea to be selected. Then when the contest deadline is over ( February 15th 2015 ), we will choose 30 project ideas, and send one Smoothieboard 5XC to each selected project. Then we expect everybody to actually use their Smoothieboard to implement their project idea.



Where can I learn more ?

There is a website at [http://smoothiecontest.org](http://smoothiecontest.org), with project ideas to help inspire you, all of the contests’ rules, and a list of the contest entries so far.



You can also email me directly at wolf.arthur@gmail.com if you have any question.



Cheers all, can’t wait to see what kind of stuff you’ll propose !





**"Arthur Wolf"**

---


---
*Imported from [Google+](https://plus.google.com/+ArthurWolf/posts/R38Gn37FvMP) &mdash; content and formatting may not be reliable*
