---
layout: post
title: "So I made simple punch tracker this morning so my wife and I can keep track of how more loan payments we have until we are done with my student loans"
date: May 29, 2017 13:48
category: "Object produced with laser"
author: "3D Laser"
---
So I made simple punch tracker this morning so my wife and I can keep track of how more loan payments we have until we are done with my student loans.  The numbers are only held by .5 mm on each side so they are easy to break off.  Nothing fancy but it gets the job done

![images/c29c2c1d8b80afc75a7aa2cd6df5efd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c29c2c1d8b80afc75a7aa2cd6df5efd7.jpeg)



**"3D Laser"**

---
---
**Steve Anken** *May 29, 2017 15:18*

A 440 hz 


---
**3D Laser** *May 29, 2017 15:37*

440 dollars 


---
**Phillip Conroy** *May 29, 2017 19:09*

Good idea


---
**Steve Anken** *May 29, 2017 21:16*

Bad pun, A 400 is standard pitch in music and and hz is Hurts. 




{% include youtubePlayer.html id="zODkjFvkoFI" %}
[youtube.com - 440Hz Sine Wave Test Tone](https://www.youtube.com/watch?v=zODkjFvkoFI)




---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/14yUqdj85YK) &mdash; content and formatting may not be reliable*
