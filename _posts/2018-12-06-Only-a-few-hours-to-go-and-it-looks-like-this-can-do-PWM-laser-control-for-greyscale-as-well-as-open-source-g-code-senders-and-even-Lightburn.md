---
layout: post
title: "Only a few hours to go and it looks like this can do PWM laser control for greyscale as well as open source g-code senders and even Lightburn"
date: December 06, 2018 05:51
category: "Modification"
author: "Timothy Rothman"
---
Only a few hours to go and it looks like this can do PWM laser control for greyscale as well as open source g-code senders and even Lightburn.  The Kickstarter is amost 100% funded!  Looks like an all or nothing kickstarter.



[https://www.kickstarter.com/projects/2118335444/automate-anything-with-super-gerbil-cnc-gcode-cont](https://www.kickstarter.com/projects/2118335444/automate-anything-with-super-gerbil-cnc-gcode-cont)









**"Timothy Rothman"**

---
---
**Timothy Rothman** *December 06, 2018 09:53*

Awesome.Paul!  Fully funded. Awesome.tech

It was cool to see it driving the k40 engraving.


---
**Stephane Buisson** *December 06, 2018 10:39*

bravo !


---
**HalfNormal** *December 06, 2018 12:35*

Congratulations!


---
**Don Kleinschnitz Jr.** *December 06, 2018 13:45*

Great! and best of luck!


---
**Don Kleinschnitz Jr.** *December 06, 2018 13:51*

**+Paul de Groot** is this project open source? Does the board have a spindle driver?


---
**Anthony Bolgar** *December 06, 2018 14:14*

Yes the board has a spindle driver, and the tool chain is open source.


---
**Don Kleinschnitz Jr.** *December 06, 2018 14:22*

**+Anthony Bolgar** by spindle driver I mean the motor is driven directly from the board? I noted on kickstarter that the spindle is driven from PPM output?


---
**Anthony Bolgar** *December 06, 2018 15:03*

Has a driver right on the board.




---
**Paul de Groot** *December 06, 2018 20:15*

Thank you all so much !  You all helped me by posting about it which is incredible. Anthony even stayed up till 4.30am, I owe you all. Thanks again, Paul


---
*Imported from [Google+](https://plus.google.com/107673954565837994597/posts/2bSXzCgedoP) &mdash; content and formatting may not be reliable*
