---
layout: post
title: "Shared on June 29, 2016 08:22...\n"
date: June 29, 2016 08:22
category: "Object produced with laser"
author: "Glyn Jones"
---




![images/fae1dccdd98e5f8a4f73ae99fde23c20.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fae1dccdd98e5f8a4f73ae99fde23c20.jpeg)
![images/0375f286ea8530fe066202f7f7d6addc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0375f286ea8530fe066202f7f7d6addc.jpeg)
![images/e0ba08f928e237c9c7b814807234b97f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e0ba08f928e237c9c7b814807234b97f.jpeg)

**"Glyn Jones"**

---
---
**Eoin Kirwan** *June 29, 2016 08:39*

Looks great


---
*Imported from [Google+](https://plus.google.com/+GlynJonesCarlos/posts/8g4VHNMh4eR) &mdash; content and formatting may not be reliable*
