---
layout: post
title: "Has anyone ever looked at building a new frame out of extruded aluminium to give more area to cut in?"
date: April 14, 2016 19:01
category: "Discussion"
author: "Pete OConnell"
---
Has anyone ever looked at building a new frame out of extruded aluminium to give more area to cut in? I've been thinking about it recently. A project I want to start needs some cutting but far too big for my cutter.





**"Pete OConnell"**

---
---
**Scott Marshall** *April 14, 2016 19:29*

It's as big as will fit in the case. to go any bigger would require a scratch start, you may as well build a big machine and save the k40 for the smaller projects.


---
**Pete OConnell** *April 14, 2016 19:41*

yeh I was thinking of taking everything out of the current box. Not just changing the bed.


---
**Scott Marshall** *April 14, 2016 19:48*

Yeah, I've debated it byself, but I finally cut away the bottom half of the cutting compartment, hung the frame from the front and rear with L brackets made from angle, and cantilivered the case by mounting the right side to a aquarium stand. With long focal length lenses, it allow you to laser larger objects like baseball bats and such. 



Next deal on a laser tube, probably more like 100w, I'm going to build a 48 x 36 or so, so I can cut sheet foam and such for model airplanes. I find new uses for it every day. I'm sure a bigger/more powerful one would open even more possibilities.


---
**Scott Marshall** *April 16, 2016 18:48*

I have. You may be hearing from me when I get there. Nice work.


---
**Brien Watson** *April 17, 2016 12:50*

I am about to go down that road...  I have a 400 X 600 frame kit on the way from lightobject.  Already have a new DSP X7. 50 watt power supply. 50 watt tube mirrors nozzle X Y gantry 24 volt power supply and mini controllers.  Just waiting for the frame. Should be here next week. 😜


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/g1pYwYg3V5Q) &mdash; content and formatting may not be reliable*
