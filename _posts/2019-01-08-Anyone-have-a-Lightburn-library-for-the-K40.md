---
layout: post
title: "Anyone have a Lightburn library for the K40"
date: January 08, 2019 06:51
category: "Materials and settings"
author: "Timothy Rothman"
---
Anyone have a Lightburn library for the K40.  It would be great to have some starting point.





**"Timothy Rothman"**

---
---
**Adrian Godwin** *January 08, 2019 12:44*

Lightburn don't recommend it as the K40 control board has no laser power control commands. It probably makes sense to upgrade it with a grbl or similar controller.


---
**James Rivera** *January 08, 2019 17:42*

What are you expecting from a LightBurn “library”?


---
**Timothy Rothman** *January 09, 2019 06:13*

Hi James.... Basic settings aligned to what a typical K40 would do.  I know there is a range of settings and each machine varies, but we should have a  baseline that we can refer to in order to assess whether we need to clean mirrors, perform alignment or change the tube.  I understand mileage may vary.


---
**James Rivera** *January 09, 2019 18:20*

**+Timothy Rothman** I don't think such a thing exists. That information is available in this group though (i.e. stock cutting size). Stuff like alignment or cutting settings are also available here, but are really more related to each machine as they are often different. Basically, what you want seems more like information about how to use it than anything software can just "do for you".


---
**Timothy Rothman** *January 09, 2019 18:26*

I just want a baseline.  I suspect your machine (**+James Rivera**) is a little different due to the lens setup. I also have a LightObject upgraded lens as do many others.  Yes there are many types of setups, but definitely a lot of commonality. 

 Like I said just want a baseline.  Even if it's a consolidated table... just asked for a library to facilitate sharing.


---
*Imported from [Google+](https://plus.google.com/107673954565837994597/posts/29p49gggmfc) &mdash; content and formatting may not be reliable*
