---
layout: post
title: "Hi all, I'm new here, just got my laser delivered today"
date: June 24, 2016 16:47
category: "Modification"
author: "Tev Kaber"
---
Hi all, I'm new here, just got my laser delivered today. I also bought a replacement blower + connector from Amazon, and honeycomb tray, goggles, air assist head and lens from LO. Still need to get an air compressor and better water pump.



I'm going to try and get everything assembled and calibrated this weekend. I look forward to cutting and engraving!



I wanted to get a z-axis, but LO seems to always be out of stock. Is there another seller that makes them? I see the LO z-axis on ebay but for an outrageous $600.





**"Tev Kaber"**

---
---
**Derek Schuetz** *June 24, 2016 17:18*

I still don't get why the LO z axis table is $200 it is so much for such a simple mechanism


---
**Tev Kaber** *June 24, 2016 17:24*

It looks well-built, I'd be willing to pay $200 - but not $600. 

In the meantime maybe I'll try the manual z-axis method **+Ned Hill** outlined.


---
**3D Laser** *June 24, 2016 21:02*

For the size of the k40 manual is best I think otherwise you loose so much cutting area


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/ivoQphC1RFH) &mdash; content and formatting may not be reliable*
