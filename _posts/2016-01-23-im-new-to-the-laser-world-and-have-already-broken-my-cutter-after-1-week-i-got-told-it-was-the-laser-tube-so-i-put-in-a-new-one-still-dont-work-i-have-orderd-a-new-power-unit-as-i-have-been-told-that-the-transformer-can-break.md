---
layout: post
title: "im new to the laser world and have already broken my cutter after 1 week i got told it was the laser tube so i put in a new one still dont work i have orderd a new power unit as i have been told that the transformer can break"
date: January 23, 2016 07:48
category: "Modification"
author: "beny fits"
---
im new to the laser world and have already broken my cutter after 1 week i got told it was the laser tube so i put in a new one still dont work 

i have orderd a new power unit as i have been told that the transformer can break down 

as the cutter is out of order at the moment im takeing the time to change the cutter bed for a mesh one 

i would like to know what other upgrades i may be able to do 

i was going to cut down the lenth of the air vent but in my unit its welded in not just screwed in like i have seen in other units 





**"beny fits"**

---
---
**Sunny Koh** *January 23, 2016 08:07*

Currently this ([http://www.amazon.com/gp/product/B003NE59HE?psc=1&redirect=true&ref_=ox_sc_act_title_2&smid=ATVPDKIKX0DER](http://www.amazon.com/gp/product/B003NE59HE?psc=1&redirect=true&ref_=ox_sc_act_title_2&smid=ATVPDKIKX0DER)) is on my shopping list for Amazon when I get to the US and I believe you might get something from the local burnings if you are down under. 



Main lesson I learn breaking my machine as well, double check the water as well as the heat of the water.


---
**beny fits** *January 23, 2016 08:23*

yeah i ran mine for about 1hr befor it stoped working and the water did get hot  so this time im puting a computer radiator in the line to help keep it all cool and maby a couple more fans around the power unit to help keep it cool also 


---
**I Laser** *January 23, 2016 09:35*

Not sure what part of Australia you're from. I'm in Melbourne and given the recent spell of 43c+ days a radiator is not going to help. Well actually I've got a Swiftech apogee PC water cooling setup I'll sell you lol!



Problem with radiators is they're designed to bring temps to ambient, so useless in our climate for our purpose... Apparently the water should be kept below 25c.



Cheapest solution is to freeze some bottles filled with water and add them to your tank as required.


---
**beny fits** *January 23, 2016 09:39*

**+I Laser** im near tamworth in nsw so it gets hot as hell I'll have to try the bottle trick when i get it working again 


---
**ThantiK** *January 23, 2016 15:05*

So, the best way to deal with laser heat is to get a 5-gallon bucket of water, and throw the water pump in it after cutting holes in the top.  You don't ever really want to chill the water, because it can cause condensation and heat gradients which would crack the tube.  You <i>want</i> the laser cooled to ambient.  Just use a large reservoir.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 23, 2016 16:07*

I've got a 20L bucket setup with the pump in the bottom, holes drilled in top to feed the pipe & lid put on (to keep dust & bugs out). I'm in Gold Coast, so weather hasn't been too hot here lately, but my setup is all in the garage under the house and the ambient temperature is usually about 5 degrees cooler than upstairs ambient temperature. Also I tend to do a lot of my cutting/engraving in the night or very early morning.


---
**I Laser** *January 23, 2016 23:08*

**+ThantiK** problem is ambient can be well into the 40's here (105f+) which is way too hot for the laser to run.


---
**Dustin Hartlyn** *January 30, 2016 18:56*

Ya that is way to hot, if the laser gets above 24c it will start to degrade. I have seen some clever projects where people hooked one of those cheap air conditioner units to an ice chest filled with water (remove fan and submerged the coils). They can then circulate cool water through the tube. Just keep in mind though, that if your tube is 24c, and the ambient temperature is 40c, the temperature difference might case the tube to crack. I will say though that you will get a lot more power out of your tube if it is running below 20C.


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/UYf36Mmt4Z9) &mdash; content and formatting may not be reliable*
