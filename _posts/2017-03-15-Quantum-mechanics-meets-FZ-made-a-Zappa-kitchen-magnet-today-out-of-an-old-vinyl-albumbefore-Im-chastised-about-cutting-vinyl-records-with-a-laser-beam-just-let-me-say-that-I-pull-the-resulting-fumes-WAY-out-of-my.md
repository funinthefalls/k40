---
layout: post
title: "Quantum mechanics meets FZ... made a Zappa kitchen magnet today out of an old vinyl album...before I'm chastised about cutting vinyl records with a laser beam, just let me say that I pull the resulting fumes WAY out of my"
date: March 15, 2017 22:31
category: "Object produced with laser"
author: "Mike Meyer"
---
Quantum mechanics meets FZ... made a Zappa kitchen magnet today out of an old vinyl album...before I'm chastised about cutting vinyl records with a laser beam, just let me say that I pull the resulting fumes WAY out of my workspace...

![images/4a2fe677dcc627a15600bd44435bc92a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a2fe677dcc627a15600bd44435bc92a.jpeg)



**"Mike Meyer"**

---
---
**Jim Hatch** *March 15, 2017 23:21*

It's not just you the fumes are bad for - they do serious damage to the electronics. One album isn't like to be too bad but if you make a habit of it plan on getting a new laser soon.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 15, 2017 23:46*

I've read that the fumes can cause rust on the metal parts of the machine. But, end result looks really cool :)


---
**Alex Krause** *March 16, 2017 00:25*

Hydrochloric Acid off gassing from PVC vinyl


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/8NeDEjPzqt4) &mdash; content and formatting may not be reliable*
