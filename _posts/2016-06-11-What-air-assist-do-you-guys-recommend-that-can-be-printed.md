---
layout: post
title: "What air assist do you guys recommend that can be printed?"
date: June 11, 2016 17:42
category: "Air Assist"
author: "Ray Kholodovsky (Cohesion3D)"
---
What air assist do you guys recommend that can be printed? Pla ok?





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Ariel Yahni (UniKpty)** *June 11, 2016 18:15*

**+Yuusuf Sallahuddin**​ has a great design that he posted somewhere.  


---
**Jim Hatch** *June 11, 2016 18:42*

I'd go with ABS if you have it. Higher temp & strength. I have a 3D printer (Makerbot 2X) but went with a LightObject head because an errant flame is gonna burn the nozzle. Not with the time & effort when the LO one was $15 I think.


---
**Corey Renner** *June 11, 2016 23:59*

ePC and ePA are both hi-temp and flame retardant, er flame differently-abled...  Second choice would be ABS or PETG for higher temps than PLA, but all 3 of those will burn.  The LO nozzle is $18.50 on Amazon delivered and it's very nicely made.


---
**Derek Schuetz** *June 12, 2016 00:45*

[http://www.thingiverse.com/thing:1189368](http://www.thingiverse.com/thing:1189368)



This air assist in my opinion is better then the light object it creates we s a jet of air right at the laser point. Compare that to the LO air assist head which just created a gentle breeze all around


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 01:37*

**+Ray Kholodovsky** This is the end result of my design I have been working on. It was 3D printed on a resin based printer by someone local at 3DHubs. If you are using 3DHubs at all, there are some refer codes on my page that will give you US$10 off the print (>$25 prints) [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iTbTG7u75DC](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/iTbTG7u75DC)



It is designed to focus a cone of air at the focal point of 50.8mm, with the intention that the cone of air creates a barrier that prevents any smoke getting to the lens region (requiring less cleaning). From my observations whilst using it, it seems to do just this, even with the horribly low pressure I am using for the air.



I plan to upgrade my air pressure/flow as $ permits & as I find a suitable compressor/airpump. 



Here's the STL file if you are interested in taking a look/printing one: [https://drive.google.com/file/d/0Bzi2h1k_udXwRHpZTWc1SHpSenc/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwRHpZTWc1SHpSenc/view?usp=sharing)


---
**Stephane Buisson** *June 12, 2016 11:17*

wrap and glue some foil on it, plastic (pla & abs) take fire


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/TKyiGcuJQoW) &mdash; content and formatting may not be reliable*
