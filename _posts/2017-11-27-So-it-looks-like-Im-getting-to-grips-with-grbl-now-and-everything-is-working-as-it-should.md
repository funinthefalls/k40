---
layout: post
title: "So it looks like I'm getting to grips with grbl now and everything is working as it should"
date: November 27, 2017 10:17
category: "Object produced with laser"
author: "Andy Shilling"
---
So it looks like I'm getting to grips with grbl now and everything is working as it should. Even learning to hand stain  and waxing wood has been fun.



![images/0ad3a7a47257bde425809fb4842d6284.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0ad3a7a47257bde425809fb4842d6284.jpeg)
![images/a79cedca7e63f2ae2d49bd3cd8a2a154.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a79cedca7e63f2ae2d49bd3cd8a2a154.jpeg)

**"Andy Shilling"**

---


---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/1ydoi5YCaZy) &mdash; content and formatting may not be reliable*
