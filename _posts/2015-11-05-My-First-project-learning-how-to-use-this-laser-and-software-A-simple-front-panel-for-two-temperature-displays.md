---
layout: post
title: "My First project learning how to use this laser and software: A simple front panel for two temperature displays"
date: November 05, 2015 20:13
category: "Object produced with laser"
author: "Todd Miller"
---
My First project learning how to use this laser

and software:  A simple front panel for two

temperature displays. 



![images/74d37d2b3edc88907635a9a37210615b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/74d37d2b3edc88907635a9a37210615b.jpeg)
![images/df4c6abfd7241c0cb93c740e7607fc6e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/df4c6abfd7241c0cb93c740e7607fc6e.jpeg)

**"Todd Miller"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2015 07:21*

Very nice work Todd. I like it & want one too :D



Where did you get the temp gauges?


---
**Todd Miller** *November 07, 2015 01:59*

[http://www.parts-express.com/sure-electronics-blue-led-temperature-display-external-sensor--320-506](http://www.parts-express.com/sure-electronics-blue-led-temperature-display-external-sensor--320-506)


---
**David Cook** *November 07, 2015 04:14*

Nice !


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 07, 2015 08:21*

I'm curious, where did you place the probes for the two temperature gauges? I see one is for laser head & one for water. I think this would be a nice, simple & very useful modification to build.


---
**Todd Miller** *November 20, 2015 21:23*

I used a metal break line, heat sink compound and shrink tube.

I share a pic but I don't know how without starting a new thread.


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/2gxRjY6LCPw) &mdash; content and formatting may not be reliable*
