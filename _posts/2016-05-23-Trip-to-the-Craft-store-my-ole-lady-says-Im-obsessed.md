---
layout: post
title: "Trip to the Craft store my ole lady says I'm obsessed..."
date: May 23, 2016 00:22
category: "Material suppliers"
author: "Alex Krause"
---
Trip to the Craft store  my ole lady says I'm obsessed... Must Laser Everything! Hobby Lobby my new best friend. The leather remnant pack was a cool find 3lbs of leather scraps for 6$ 

![images/edae88f7425d34d27f84004a53cef650.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/edae88f7425d34d27f84004a53cef650.jpeg)



**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *May 23, 2016 00:30*

Damm I don't have a place like that here :(


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 00:53*

Nice selection of goodies. We have similar places in Australia, but not Hobby Lobby themself. Things like Spotlight & Riot Art n Craft stock a lot of these things, although leather pieces I have yet to find a craft store stocking them (I get mine through a specific leather supplier). Heads up with the leather, cutting with low mA (I use 4mA) & multiple passes is better than trying to increase the power to get through in one go.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 02:06*

**+Susan Moroney** I've never heard of Reverse Art Truck. Will have to look into it. Thanks for that Susan.



edit: Awesome, found something similar in QLD, up in Woolloongabba (Brisbane). [http://www.reversegarbageqld.com.au/](http://www.reversegarbageqld.com.au/)


---
**Alex Krause** *May 23, 2016 02:15*

I'm going to have to have you Aussies pick me up some Hilltop Hoods merchandise it's hard to find here in the USA :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 23, 2016 02:22*

**+Alex Krause** Haha, haven't heard Hilltop for years. Not even sure where to look, but if you know of any that you want I'd be happy to look around for you.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/gweQu6TnRnf) &mdash; content and formatting may not be reliable*
