---
layout: post
title: "what PSI is the recommended for air assist pump?"
date: January 07, 2016 12:28
category: "Discussion"
author: "Pete OConnell"
---
what PSI is the recommended for air assist pump?





**"Pete OConnell"**

---
---
**Jim Hatch** *January 07, 2016 13:54*

I have used up to 15psi (with wood or acrylic) and although it clears the head area there's still a substantial amount of smoke swirl at lower speeds & higher power for cutting. I know some of these come thru with aquarium air pumps so I don't think higher pressures are needed as much as making sure the air is in line with the cutting line and that there is sufficient exhaust flow (cfm) to get the smoke out of the box. That's what I'm working on this weekend. The 60W laser I use at the local makerspace has intake fans mounted on the front panel and the exhaust out the back and we get barely visible smoke. I believe the K40 housing is constrained by the ability to refresh the air in the box and higher pressure air doesn't help much - in fact some kind of hvlp setup might be preferable especially with lighter materials like paper which might be moved around by higher pressure air movement. I'll post my results with the front panel air intake mod.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 07, 2016 15:39*

I personally am using an aquarium air-pump for my air-assist. It is very low airflow & pretty weak. I have found that by focusing the airflow through a ball-inflation needle, it has increased the pressure & also allows me to focus the airflow to right where the cutting was taking place (i.e. 50mm from the lens). This has caused less issues with soot/stains on my working pieces. However, I do believe my setup could be improved with better airflow (intake & exhaust fans like Jim suggests).


---
**ChiRag Chaudhari** *January 07, 2016 20:58*

I purchased two air compressors and returned both of them. Now I use the Aquarium Air Pump instead. With the compressed air you will also need the water filter and stuff. What we need here is more Air Volume not so much of the pressure.  110 LPM commercial air pump costs the same as 1/5 HP air brush compressor but puts out a lot more air. And its pretty quiet too, my steppers make more noise than that!!!


---
**Pete OConnell** *January 08, 2016 07:23*

cheers :)


---
**Arestotle Thapa** *January 08, 2016 14:09*

Have anyone used anything like this which has about 1100LPM? Is that too much air?

[http://www.amazon.com/Intex-Quick-Fill-Electric-110-120-38-9CFM/dp/B0007VU0RU/ref=pd_sbs_21_2?ie=UTF8&dpID=51PQUjf4ASL&dpSrc=sims&preST=_AC_UL160_SR160%2C160_&refRID=0C781KCR925S1KCAWY7K](http://www.amazon.com/Intex-Quick-Fill-Electric-110-120-38-9CFM/dp/B0007VU0RU/ref=pd_sbs_21_2?ie=UTF8&dpID=51PQUjf4ASL&dpSrc=sims&preST=_AC_UL160_SR160%2C160_&refRID=0C781KCR925S1KCAWY7K)


---
**Jim Hatch** *January 08, 2016 14:40*

This is the one I've got coming in to add to mine. It's not as high volume but should be higher pressure. [http://www.amazon.com/gp/product/B002JPRNOU?psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s00](http://www.amazon.com/gp/product/B002JPRNOU?psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s00) My current plan is to mount it inside the control side of the cabinet and wire it directly to the 120V main power so it automatically goes on when I turn on the cutter. But if I see that there are vibration issues that affect the laser head then I'll move it outboard but I'd like to keep it all in the physical footprint of the cutter. I'll post next week how it goes as I'll be hooking it up this weekend. 



**+Arestotle Thapa**  I actually have one of those Intex pumps for an air mattress so I'll try that too. I won't fit inside the cutter housing but the higher volume lower pressure (hvlp) may be a better approach (& cheaper). I'll do some tests of each.


---
**I Laser** *January 08, 2016 22:11*

**+Jim Hatch** Regarding front airflow, any chance of some pics? I've been thinking about doing something similar, say two to three holes on the front with PC case fans blowing in.


---
**ChiRag Chaudhari** *January 09, 2016 18:26*

**+Arestotle Thapa** WOW thats a lot of air flow. That is 1100 LPM . And its so cheap! I want to try it now!


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/VuwqHHsQBDP) &mdash; content and formatting may not be reliable*
