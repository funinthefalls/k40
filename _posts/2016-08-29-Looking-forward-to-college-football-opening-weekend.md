---
layout: post
title: "Looking forward to college football opening weekend!"
date: August 29, 2016 01:19
category: "Object produced with laser"
author: "Jeremy Hill"
---
Looking forward to college football opening weekend!  The base is from a crepe myrtle I trimmed and split the log in two.  Cut out the top and set the lights in a pocket underneath.  Polyurethane coating on the log.



![images/be14e79cacdf2cf02444d1f1bef5f9c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be14e79cacdf2cf02444d1f1bef5f9c6.jpeg)
![images/7d6928a3d7f5d67fec3129ecde58ecea.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d6928a3d7f5d67fec3129ecde58ecea.jpeg)

**"Jeremy Hill"**

---
---
**Bob Damato** *August 29, 2016 12:11*

This looks great, and I was going to attempt one of these this week. I bought the blank and the lighting but wanted to mess around first.  I assume you did a negative and did it from the back? What were your laser settings? Speed, power, out of focus etc? Very nice!




---
**Jeremy Hill** *August 29, 2016 12:57*

I placed the graphics and the shape in the Corel draw/laser program.  I put a small circle at upper left corner to make sure the cut would go where I wanted on the 12x12 sheet of ~.25" acrylic. I can only use about 9"x12" area for the laser. I used about 3/4 power on my stock laser power dial. I used 300 mm/sec. 


---
**Bob Damato** *August 29, 2016 13:12*

Thats great info, thank you. Ill give it a try this week and if Its successful Ill post back :)


---
**Jeremy Hill** *August 29, 2016 13:17*

**+Bob Damato** good luck!  Look forward to hearing/reading about the results. 


---
**Bob Damato** *September 05, 2016 21:12*

**+Jeremy Hill** How did you cut the shape? One pass with full power or a few passes at lower power? I need to cut mine but there is no protective cover on  it to keep the smoke from staining it




---
**Jeremy Hill** *September 05, 2016 21:36*

I had four passes at 3/4 power with 4 mm/sec and 6 mm/sec


---
**Bob Damato** *September 05, 2016 21:46*

Thank you, Ill give it a try tonight :)


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/fT1t2JS6Dop) &mdash; content and formatting may not be reliable*
