---
layout: post
title: "Is there anyway of doing a grey scale photo with laser draw or is just the amound of dots in the picture"
date: November 19, 2015 13:32
category: "Discussion"
author: "Tony Schelts"
---
Is there anyway of doing a grey scale photo with laser draw or is just the amound of dots in the picture.  I'm new to this stuff so sorry if I sound vague. I only have the Cheap chineese 40w





**"Tony Schelts"**

---
---
**Gary McKinnon** *November 19, 2015 14:07*

I have the same cutter but am not experienced with it yet. I read in this tutorial that JPG works best for greyscale : [http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml](http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml) 


---
**Gary McKinnon** *November 19, 2015 14:07*

I have the same cutter but am not experienced with it yet. I read in this tutorial that JPG works best for greyscale : [http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml](http://www.daycounter.com/Articles/LaserDrw/LaserDrw-Tutorial.phtml) 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 19, 2015 21:37*

Hey Tony. If you take the image, grayscale it, then dither @ 60% you can produce a decent result. You may need to tweak brightness/contrast (else dark areas may come out too dark). Check this one I did using method suggested by Marc G.

[https://plus.google.com/+YuusufSallahuddinYSCreations/posts/AJCXBuJffLt](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/AJCXBuJffLt)



(hoping this is what you are meaning)


---
**Tony Schelts** *November 19, 2015 22:07*

That looks good Thanks will give it a try over the weekend 


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/3ekwg9Zpiza) &mdash; content and formatting may not be reliable*
