---
layout: post
title: "Anyone familiar with the Lasersaur project? I'd like a large format (900x1200) 120-130W laser"
date: July 25, 2016 02:48
category: "Discussion"
author: "Jim Hatch"
---
Anyone familiar with the Lasersaur project? I'd like a large format (900x1200) 120-130W laser. I was thinking of using the Lasersaur project to build the frame & mechanicals and use a Smoothie board for the controller and LaserWeb for the software. Or is there an easier way to get a big bad laser with Smoothie control? I thought of buying another Chinese laser - a bigger boy and then doing the same Smoothie conversion I'm doing with the K40 but I figure that'll end up costing more and likely taking just as long. I will also add the 7" travel motorized Z-axis which with the 2" of nozzle adjustment the Lasersaur specs, would give me 9" of vertical adjustment - more if I drop the bottom and put that on some sort of manual lift.



With 130W I should be able to cut 1/2" or better ply. with a 900x1200 bed there's some serious real estate on the bed and if I alter the plans to add a pass-through slot I could probably easily accommodate 2inch x 4foot x infinite (well about 1/2 as long as my basement anyway) material.





**"Jim Hatch"**

---
---
**Alex Krause** *July 25, 2016 02:53*

**+Jim Hatch**​ check on the laserweb community I'm pretty sure someone on there is very familiar with the lasersaur project


---
**Anthony Bolgar** *July 25, 2016 03:35*

Check out the Freeburn laser build on Openbuilds.com **+Peter van der Walt** designed that one.


---
**Anthony Bolgar** *July 25, 2016 11:45*

You have LaserWeb on your mind **+Peter van der Walt** I think you meant the Lasersaur google group.


---
**Anthony Bolgar** *July 25, 2016 11:50*

That's more like it ;)


---
**Jim Hatch** *July 25, 2016 13:18*

Thanks. I'll check out the Freeburn project. How big is the bed?


---
**Anthony Bolgar** *July 25, 2016 13:21*

Project is here:[http://www.openbuilds.com/builds/openbuilds-freeburn-1-v-slot-co2-laser-60-100w.1001/](http://www.openbuilds.com/builds/openbuilds-freeburn-1-v-slot-co2-laser-60-100w.1001/)


---
**Arthur Wolf** *July 25, 2016 21:38*

My recommendation is : Get a chinese laser ( signsgtech is better quality that most chinese laeser, for not much more cost ), import it by sea, install a smoothieboard in it. I'm currently in a remote fablab doing this as a service, I'm taking pics and will document it.

Definitely the best cost/capabilities ratio, by far, of any option ( inscluding DIY ).

 You can email me at wolf.arthur at [gmail.com](http://gmail.com) if you want more info.


---
**Jim Hatch** *July 26, 2016 01:57*

**+Arthur Wolf** That seems to be the consensus so far. Get a 100 or so with the big bed size and then upgrade to the controller (Smoothie) I want and when the tube goes, bump that to 130W then. There are a couple of ebay dealers who ship from CA (already imported I guess). I'll take a look at those and see what your lead looks like too.


---
*Imported from [Google+](https://plus.google.com/114480049764906531874/posts/3CWZK5NQSV1) &mdash; content and formatting may not be reliable*
