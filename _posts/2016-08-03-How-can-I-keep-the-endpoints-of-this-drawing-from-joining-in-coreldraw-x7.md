---
layout: post
title: "How can I keep the endpoints of this drawing from joining in coreldraw x7?"
date: August 03, 2016 16:02
category: "Discussion"
author: "Tony Sobczak"
---
How can I keep the endpoints of this drawing from joining in coreldraw x7?  I just want to cut on the existing lines without the lines that connects the two ends in corel laser.



Thanks



Here's the file



[https://dl.dropboxusercontent.com/u/101980659/Test%201.cdr](https://dl.dropboxusercontent.com/u/101980659/Test%201.cdr)

![images/c3d118a525f0e3b9a97322e8a2d0393d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3d118a525f0e3b9a97322e8a2d0393d.jpeg)



**"Tony Sobczak"**

---
---
**Jim Hatch** *August 03, 2016 16:19*

Is this from that fractal puzzle guy? Make sure the line is thin .001mm or hairline so it's really a cut line and not being treated as an engrave.


---
**Scott Marshall** *August 03, 2016 16:35*

I'm not a Corel guy, but can you possibly close the shape with a line on a different layer and set the power for that layer to 0?



That would satisfy the software's desire to close the shape and just not cut the closure part.



I've done it in Draftsight like that.



Hope it helps a little.



Living hinge?



Scott


---
**Jim Hatch** *August 03, 2016 17:07*

**+Scott Marshall** It looks like this: [http://nerdist.com/this-fractal-puzzle-makes-large-pieces-disappear/?utm_source=facebook&utm_medium=social&utm_campaign=dailydot%E2%80%8B](http://nerdist.com/this-fractal-puzzle-makes-large-pieces-disappear/?utm_source=facebook&utm_medium=social&utm_campaign=dailydot%E2%80%8B)


---
**Scott Marshall** *August 03, 2016 18:55*

VERY COOL. Now I have to make one.



Very much worth the effort.



Hope you find a solution to your problem, but now that I know what's going on, I have to ask - why do you need to make the cuts openended?



It would seem the pieces and the socket would both be a closed polygon, albeit a complex one. Is is because you can't make the whole socket in one pass on the little K40? - or am I just missing an obvious fact? (something I do frequently)



Scott



PS, This one rates going in a Projects library, it's going in mine, right near the top.


---
**Tony Sobczak** *August 03, 2016 19:18*

**+Scott Marshall** It's like the one **+Jim Hatch** pointed out only a smaller scale.   The lines are already at .001.  The line between the two end points is not there until you load it up in CorelDraw. It really screws up the puzzle when it cuts..  I've tried using the freehand tool to make a loop back to itself, but can't seem to get that to work. The link shows the issue when you get it ready to cut.


---
**Jim Hatch** *August 03, 2016 20:49*

**+Tony Sobczak**​ I'm in airports for the next few hours but will check with Corel when I get to my hotel. Which version are you using?



How did you make it? If it's a line then you don't need to close the ends. But if it's a polygon then it's going to look to close it. If it is a polygon then you can convert it to a line and delete the connection between those two end nodes. I'll do it when I hit my hotel but you can try it yourself if you're in a hurry.


---
**Tony Sobczak** *August 03, 2016 22:24*

**+Jim Hatch**​ x7. Downloaded it from a website.  Lists as a curve in CorelDraw


---
**Jim Hatch** *August 04, 2016 16:59*

In X8, it's okay. Even throwing it to the laser driver it doesn't show the connection line you don't want. But, I notice the image above has an outline around the squiggly line. If that's in your file that's likely the issue - if it's also not a .001mm line and defined as a cut, then it's creating an engrave operation between the outside black outline box and the internal red squiggly which needs to be a closed object for the engrave operation.



Is that outline box in your file? It doesn't show when I grab it from Dropbox now.



Also do you have WMF as the file type in the setup (the first green icon in the add-in icon bar) for engraving and PLT for the cutting?


---
**Tony Sobczak** *August 04, 2016 20:41*

The other lines are the cutout for the board.  They are on a different layer and are not displayed when I cut the puzzle pieces.



I had both set to WMF. I swear I saw one post a while back that said they had to both be set to WMF.  Setting to WMF-PLT works like a champ.  Thanks so much Jim.


---
**Tony Sobczak** *August 04, 2016 20:43*

For anyone interested here is the link for 3 different fractal puzzles.





[http://community.glowforge.com/t/laser-fractal-puzzle/1152/6](http://community.glowforge.com/t/laser-fractal-puzzle/1152/6)


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/ax5BXh3hX2F) &mdash; content and formatting may not be reliable*
