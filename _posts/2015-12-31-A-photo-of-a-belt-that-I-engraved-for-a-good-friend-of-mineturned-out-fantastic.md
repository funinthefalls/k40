---
layout: post
title: "A photo of a belt that I engraved for a good friend of mine...turned out fantastic!"
date: December 31, 2015 20:40
category: "Object produced with laser"
author: "Scott Thorne"
---
A photo of a belt that I engraved for a good friend of mine...turned out fantastic!

![images/51f4b7de0c72723f0d249c6db4ec6f67.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51f4b7de0c72723f0d249c6db4ec6f67.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 01, 2016 02:33*

Looks great. Your new machine came with rotary attachment right? Is this done using that?


---
**Scott Thorne** *January 01, 2016 04:48*

It came with it....but I just laid the belt on the table and used the laser pointer that is built in on the head to make sure it was straight from left to right.


---
**mason cohen** *January 02, 2016 15:46*

how long did it take to engrave?


---
**Scott Thorne** *January 02, 2016 17:12*

around 4 minutes


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/GBBCL2NoZWw) &mdash; content and formatting may not be reliable*
