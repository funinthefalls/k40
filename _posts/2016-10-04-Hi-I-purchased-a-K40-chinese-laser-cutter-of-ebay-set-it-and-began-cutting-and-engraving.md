---
layout: post
title: "Hi I purchased a K40 chinese laser cutter of ebay, set it and began cutting and engraving"
date: October 04, 2016 20:46
category: "Modification"
author: "Laura Birkett"
---
Hi



I purchased a K40 chinese laser cutter of ebay, set it and began cutting and engraving. I managed to do a few cuts through 3mm mdf and after a few goes it stopped cutting. The water that the pump was in was really hot so I let it cool down over night. When I tried again the laser still wouldn't cut.



After reading several forums I tried a few different ways to get it cutting such as cleaning the mirrors, realigning the mirrors, turning the voltage down, turning the voltage up, slowing the speed, increasing the speed. Nothing has worked.



I have also realised how naive I have been in thinking that 40w actually meant 40w



Now I am left with a very weak beam, which, i think, means that the tube is broken/burn out.



I could be being completely thick, but can I buy a 60w tube to go in this machine so that the laser is strong enough to cut through 3mm mdf?



If not what do you lovely people suggest.





**"Laura Birkett"**

---
---
**Randy Randleson** *October 04, 2016 21:03*

thats not good, your pump shouldn't get hot. Are you sure its pumping water still?


---
**Laura Birkett** *October 04, 2016 21:32*

yeah the pump is definitely working. I think it was my fault for have the power on full and trying to cut on a slow speed. 


---
**Scott Marshall** *October 04, 2016 21:47*

The K40 can cut 3mm Plywood in 1 pass, quite easily. Mdf by all accounts is easier to cut than the plywood, so there's probably one or more things not set up correctly on your machine.



To REALLY put it right, a good quality set of mirrors, lens and air assist head are required.



The air assist head will shield your lens from smoke, debris, and allow you to fine tune the focus (a critical operation).



The mirrors are less important, but the stock ones don't last well, and a good set of moly (arguably the most durable) mirrors will take routine cleaning with no harm.



Your pump should fill a 1 gallon jug 1/2 full in 1 minute (about 2 liters per minute) more flow is better. Water should never be allowed to become warm to the touch, add ice to the tank if needed, but even at high power, it normally takes hours to get the water that warm. (Use 5 Gallons minimum as a tank size)



Even the meager 30 watts of the K40 produces over a quarter million watts per square inch in a properly focused K40.



The most consistant power robbers are poor focus and poor mirror adjustment. 



Start there. You should be cutting 3mm mdf at 1/2 power, maybe a little more if all is 'dialed in'



No, it's not practical to just install a 60w tube in a k40. It's possible, just not practical, or usually required.

You would need a larger power supply, better mirrors, lens and head, not to mention a larger cabinet.



If you spend some time adjusting the mirrors dead on, and get the focus set just right (Aim for the center of the material if you're trying for a 1 pass cut), I think you'll find your 30watt K40 wakes right up and cuts surprisingly well.



Take it to "level 2" with an air assist head and fresh optics (see link)

it's down right amazing.



Level 3 involves a longer focal length lens. I've found that a longer focal length lens makes for a great cutting combination on 3mm ply.





You'll want 20mm Moly Mirrors Set of 3, and 18mm x 50.8mmFl ZnSe lens (fits new head)

If you want to try the longer lens, get an 18mmx62.5. They cut well, but are a touch less sharp to engrave with.



Saite cutter is a good source for the optics and have a great adjustable focus head. 

[http://www.ebay.com/itm/HQ-CO2-K40-Shenhui-Engraving-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252563123459](http://www.ebay.com/itm/HQ-CO2-K40-Shenhui-Engraving-Head-Mirror-20mm-Focus-Lens-18mm-Integrative-Mount-/252563123459)



[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**Jim Hatch** *October 04, 2016 21:49*

"power on full"? As in over 20ma? If so likely you burned out your tube. You shouldn't run more than 20ma - 18ma is best to consider max power.


---
**Ashley M. Kirchner [Norym]** *October 04, 2016 21:56*

And the water should be cold ... 20C or less. I freeze small 20-oz drink bottles and use them to keep the water cold.


---
**Laura Birkett** *October 04, 2016 21:56*

Scott which mirrors etc would you recommend? Do you think it is worth me buying an upgrade kit and a new 40w tube?

This is one that I have been looking at [http://www.lightobject.com/AWC608-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P758.aspx](http://www.lightobject.com/AWC608-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P758.aspx)



Jim - I think I got a bit impatient when it wouldn't cut and assumed that more power would be better. I think at the max it ran at 25ma. i now realise what a mistake that was



[lightobject.com - AWC608 DSP Upgrade Kit for D/K40 Small CO2 Laser Machine](http://www.lightobject.com/AWC608-DSP-upgrade-kit-for-DK40-small-CO2-laser-machine-P758.aspx)


---
**Laura Birkett** *October 04, 2016 22:04*

Thanks for the tip Ashley  x


---
**Scott Marshall** *October 04, 2016 22:23*

I added the mirror specs to the 1st post. Lenses as well.



Something that occurred to me the other day. 



Even if you don't have or can't afford an air pump or compressor right now, go ahead and install the air assist head. 

Many of it's advantages are not air related. The Adjustable focus (#1 MAJOR GAIN) and lens protection don't require air.



Later, all you have to do is connect your air pump to get cleaner cutting, less smoke deposits, and fire supression.



and, in the meantime...

Even with a $10 aquarium pump, you still get a lot of 'assist'.



and down the road:

If you want a controller up grade, I may be able to help...





[ALL-TEKSYSTEMS.com](http://ALL-TEKSYSTEMS.com)



[all-teksystems.com - all-tek-systems](http://ALL-TEKSYSTEMS.com)


---
**Phillip Conroy** *October 04, 2016 23:46*

If not using air assist then could have burned the focal lens with resulting fames,clean with alochol and soft cloth ,check it is clean by shining a torch at diffrent angles to see better.you need to check also that tne pump is ok as it could be your source of heat if fautly-you need to stick a temp gauge in the water and run pump for awhile with no laser cutting-if temp increases a lot [more that room temp ]pump is faulty.i never laser cut with water temps over 20 deg 


---
**Laura Birkett** *October 05, 2016 06:32*

Thanks Philip, It came with a fan I am not sure that is the same thing. I have cleaned all of the mirrors and the focal lens but witll check the temp of the pump


---
**Phillip Conroy** *October 05, 2016 07:32*

cutting mdf or ply without air assist[compressed air blowing on spot the laser beam hits] will just make fire and smoke


---
**Laura Birkett** *October 05, 2016 07:37*

There has been a lot of that. 



I do have another question as I am just weighing up all of my options



Have you had any experience with an  80W LASER ENGRAVING MACHINE ROTARY AXIS 700X500MM CUTTER  - ebay



I basically need a machine to 



* to cut through 3mm mdf cleanly 

*to cut through paper and card stock



Additional benefit of this machine

* to cut shapes through 25mm mdf (router for this)



Would this do the job or would I be jumping out of the frying pan and in to the fire?


---
**Jim Hatch** *October 05, 2016 12:27*

Properly setup the K40 will do what you're planning (except for cutting 25mm MDF - or 25mm anything). You don't need an 80W (which won't cut 25mm either). 


---
**greg greene** *October 05, 2016 13:59*

Your problem is beam alignment - there are several methods mentioned here to do this properly - until you get this right - from the tube bedding on out - your chasing ghosts.

[theflyingwombat.com - The Aussie Flying Wombat](http://theflyingwombat.com)


---
**Jim Hatch** *October 05, 2016 14:38*

**+greg greene**​ generally I'd agree except she said it was working and then in the middle stopped working. Time to see if the laser tube is still firing - checked at the first mirror closest to the tube output to make sure it's still lasing.



I've not found my alignment going bad in the middle of a job. After I've mucked with things like upgrading hardware, sure. But mid-job suggests some other issue. We don't know if the tube still works after having been overdriven (full power).


---
**Laura Birkett** *October 05, 2016 15:00*

Thank you so much for all of the advice. I think I will go back to the start and look at everything you have mentioned again to see if I have missed a step during my frustration. I will let you all know how I get on x


---
**greg greene** *October 05, 2016 15:25*

Correct Jim - I think you may be correct - possibly a PSU problem then


---
*Imported from [Google+](https://plus.google.com/100251455340598785382/posts/DBE6AyUyi8Q) &mdash; content and formatting may not be reliable*
