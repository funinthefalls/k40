---
layout: post
title: "Hey everyone, I upgraded my laptop (Windows 10) and I managed to successfully install CorelDraw 12.0.0.485 and CorelLaser 2013.02.08.00289"
date: July 05, 2017 13:44
category: "Software"
author: "Linda Barbakos"
---
Hey everyone, I upgraded my laptop (Windows 10) and I managed to successfully install CorelDraw 12.0.0.485 and CorelLaser 2013.02.08.00289. The laser cutter is recognised and connected to the laptop - all seems fine. 



When I change the origin coordinates the laser head seems to move at a normal speed, however when I cut my work piece, no matter what speed setting (I set it to 7mm/sec), the laser seems to go crazy fast. 



Any thoughts on what settings I need to adjust or what may be causing this issue?



![images/6eb5deae8183cc5c7b214a78d551a4b1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6eb5deae8183cc5c7b214a78d551a4b1.png)
![images/7467b8f021a2e07bc936030bf1e4795d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7467b8f021a2e07bc936030bf1e4795d.png)

**"Linda Barbakos"**

---
---
**Linda Barbakos** *July 05, 2017 14:01*

Actually.. Hubby found the setting! I needed to configure the Mainboard to the same one found on the controller board.


---
**Ned Hill** *July 06, 2017 14:09*

Yes mainboard and device ID need to be set as noted on the controller board. #K40Stocksoftware

![images/0e11475fd8560783ecba54def0ac0aa8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/0e11475fd8560783ecba54def0ac0aa8.png)


---
**Ned Hill** *July 06, 2017 14:11*

Other recommended settings.

![images/2f1b4a3aba8ff122265dcfd7aa6162fc.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2f1b4a3aba8ff122265dcfd7aa6162fc.png)


---
**Frank Jones** *August 09, 2017 13:04*

I wonder if this would help me, when I try and engrave with corel laser the stepper motors move slow enough to cut not engrave. 


---
*Imported from [Google+](https://plus.google.com/107262725887494713165/posts/Jjs9iMAKBDJ) &mdash; content and formatting may not be reliable*
