---
layout: post
title: "Can I clean my focus lens with IPA (isopropyl alcohol) or would this be too aggressive?"
date: August 28, 2017 12:02
category: "Discussion"
author: "Bernd Peck"
---
Can I clean my focus lens with IPA (isopropyl alcohol) or would this be too aggressive?





**"Bernd Peck"**

---
---
**Phillip Conroy** *August 28, 2017 12:29*

That's what I have been using every day for 2 years


---
**Anthony Bolgar** *August 28, 2017 13:27*

Ditto to **+Phillip Conroy** 's comment. Just do not rub too hard, you can actually rub the coating off if you are too aggresive.


---
**Chuck Comito** *August 28, 2017 17:58*

I also use isopropyl. Get a cotton swap wet and gently rub then wipe with micro fiber. 


---
**Bernd Peck** *August 28, 2017 18:59*

Thanks. Will give it a try


---
**Phillip Conroy** *August 28, 2017 19:24*

And always have a spare,they do wear out,or shatter if dropped


---
**Chuck Comito** *August 28, 2017 19:59*

Or get crushed in crappy lens holders lol. 


---
**Nate Caine** *August 29, 2017 13:35*

This question comes up every month.

Here's a helpful link from a manufacturer of high end machines:



[support.epiloglaser.com - Cleaning Your Optics](http://support.epiloglaser.com/article/8205/11940/cleaning-epilog-optics)



They recommend Everclear and have a comment about rubbing alcohol and acetone substitutes.


---
**Bernd Peck** *September 10, 2017 06:59*

Thanks. It has worked perfect for me 


---
*Imported from [Google+](https://plus.google.com/100238441539373475210/posts/RcXRzH95FCf) &mdash; content and formatting may not be reliable*
