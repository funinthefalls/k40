---
layout: post
title: "Does anyone has engrave on rocks or stones?"
date: April 05, 2016 15:50
category: "Discussion"
author: "Jose A. S"
---
Does anyone has engrave on rocks or stones? This may sound a little bit crazy, but, Is it possible? What type of laser would I need?

![images/aa5b17b0967e952af2baaeadffb47ca7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa5b17b0967e952af2baaeadffb47ca7.jpeg)



**"Jose A. S"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 16:02*

I've tried on glazed & unglazed tiles. Worked okay, but not very deep. Although I had the power at about 10mA when I tested. Not sure if we could do it on rocks, but would be interesting to test & see. I think some people here have engraved on slate if I remember right.


---
**Ben Walker** *April 05, 2016 16:54*

I just posted a photo of my test runs doing rocks.   Same as glass.   Wet paper or dish detergent prevents chipping.   During my tests I had a rock explode when I tried to drill a hole


---
**Jim Hatch** *April 05, 2016 16:56*

It does work. Higher power is better. Similar to glass in fact. Different rocks yield different results so practice. Also you have to be careful with head clearance and alignment so a projection doesn't whack the laser head as it travels. Flat rocks are better :-)



You can also try one of the Theramark/Cermark products. That's a solution you paint on the surface and then etch (really it just "marks"). The paint then fuses to the surface and you can wash off the rest. Works kind of like toner in a laser printer.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 05, 2016 16:59*

**+Jim Hatch** Thanks for those suggestions/advice Jim.


---
**HalfNormal** *April 05, 2016 20:39*

I find slate great to work with. Cheap and looks great.


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/FDqDzERMrMZ) &mdash; content and formatting may not be reliable*
