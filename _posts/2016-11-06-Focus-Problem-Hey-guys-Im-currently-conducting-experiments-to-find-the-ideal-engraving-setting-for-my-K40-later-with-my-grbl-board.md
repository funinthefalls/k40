---
layout: post
title: "Focus Problem? Hey guys, I'm currently conducting experiments to find the ideal engraving setting for my K40 later with my grbl board"
date: November 06, 2016 07:43
category: "Discussion"
author: "Timo Birnschein"
---
Focus Problem?



Hey guys, I'm currently conducting experiments to find the ideal engraving setting for my K40 later with my grbl board.



The results looked pretty promising and then degraded drastically for no apparent reason.

In the picture below you can see two identical objects. The left looks great, nice linear gradient and good grey at the bottom left tip.

The right one uses exactly identical setting but was cut about 150mm to the right!



0,0 is at the bottom left with my machine.



Could this be a focus problem? Why is it just burned? Why all the black? Fuzzy beam?





![images/3b780c29728fbe358f3b582bbd6a0ed6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b780c29728fbe358f3b582bbd6a0ed6.jpeg)



**"Timo Birnschein"**

---
---
**Timo Birnschein** *November 06, 2016 07:49*

This test pattern at almost the same location looks perfectly fine!![images/ae8be6a4e06404474e62cb3e5654df27.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae8be6a4e06404474e62cb3e5654df27.jpeg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 06, 2016 08:02*

To me it looks like your scan lines on the rightmost triangle are closer together. Aside from that, maybe the wood composition is slightly different. I think more tests are in order to verify whether that 150mm position is odd.



edit: on closer inspection, the "scan lines" I saw look actually to be the wood grain.


---
**Ariel Yahni (UniKpty)** *November 06, 2016 10:49*

I would say yes, focus. You migth have a different distance at that spot. 


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/LJHWrSKDv7X) &mdash; content and formatting may not be reliable*
