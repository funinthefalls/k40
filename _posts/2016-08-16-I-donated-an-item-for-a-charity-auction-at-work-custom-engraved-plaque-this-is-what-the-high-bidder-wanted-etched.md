---
layout: post
title: "I donated an item for a charity auction at work: \"custom engraved plaque\" - this is what the high bidder wanted etched"
date: August 16, 2016 02:42
category: "Object produced with laser"
author: "Tev Kaber"
---
I donated an item for a charity auction at work: "custom engraved plaque" - this is what the high bidder wanted etched.

![images/07d13d8b64cc2772a791ce0f7d831cf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/07d13d8b64cc2772a791ce0f7d831cf9.jpeg)



**"Tev Kaber"**

---
---
**Jonathan Davis (Leo Lion)** *August 16, 2016 02:52*

Very nice 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 16, 2016 06:11*

That came out very nice. :)


---
**Tev Kaber** *August 16, 2016 21:26*

Thanks! They were very happy with it.


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/9usbF3AprRh) &mdash; content and formatting may not be reliable*
