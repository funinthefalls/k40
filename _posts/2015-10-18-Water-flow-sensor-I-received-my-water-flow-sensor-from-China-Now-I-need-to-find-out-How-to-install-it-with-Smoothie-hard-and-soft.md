---
layout: post
title: "Water flow sensor I received my water flow sensor from China ;-)) Now I need to find out How to install it with Smoothie (hard and soft)"
date: October 18, 2015 18:07
category: "Smoothieboard Modification"
author: "Stephane Buisson"
---
Water flow sensor



I received my water flow sensor from China ;-))

Now I need to find out How to install it with Smoothie (hard and soft).

The ref of sensor is in the following post:

[https://github.com/Smoothieware/Smoothieware/issues/750](https://github.com/Smoothieware/Smoothieware/issues/750)



the goal being to detect if water is on and pump running, or switch laser off.

Any help would be appreciated. **+Arthur Wolf** 



![images/102aa366fd22cfe005fe4e3d570c0dce.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/102aa366fd22cfe005fe4e3d570c0dce.jpeg)
![images/ff2715e86b5d39941d02b311452d95f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff2715e86b5d39941d02b311452d95f3.jpeg)

**"Stephane Buisson"**

---


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/1ZV6htwq1b3) &mdash; content and formatting may not be reliable*
