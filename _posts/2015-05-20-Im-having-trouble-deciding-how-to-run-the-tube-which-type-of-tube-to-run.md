---
layout: post
title: "I'm having trouble deciding how to run the tube/ which type of tube to run"
date: May 20, 2015 08:17
category: "Modification"
author: "Troy Baverstock"
---
I'm having trouble deciding  how to run the tube/ which type of tube to run. I have to come from the top back, back or right hand side in my setup. 



Curly cord, airbrush size, compressor size, straight tube, tube in guide. I was about to get the curly airbrush tube, as i can see its being used but I havent seen what happens when the head gets to the side of the lead (ie. how it bunches or not).



I have the MPQ-903 12v air pump [http://en.resun-china.com.cn/products_detail/&productId=415.html](http://en.resun-china.com.cn/products_detail/&productId=415.html)





**"Troy Baverstock"**

---
---
**Eric Parker** *May 20, 2015 17:04*

I vote for silicone tube in drag chains.  it seems to be the neatest and cleanest way of doing it.


---
*Imported from [Google+](https://plus.google.com/+TroyBaverstock/posts/QXuXXwcbQu8) &mdash; content and formatting may not be reliable*
