---
layout: post
title: "YouTube video of my K40 upgrade progress, increasing cutting size to 600 x 400"
date: May 02, 2018 10:50
category: "Modification"
author: "Duncan Caine"
---
YouTube video of my K40 upgrade progress,  increasing cutting size to 600 x 400.





**"Duncan Caine"**

---
---
**Adrian Godwin** *May 02, 2018 11:18*

Interesting, thanks.

How will you house it ?

You're british and apparently in the UK (UK plugs) - wherabouts ?


---
**Don Kleinschnitz Jr.** *May 02, 2018 12:06*

Nice.... looks awesome



Just a few notes on the LPS.

I doubt that the insulation you provided on the LPS will really keep the LPS from arching to something including your body :(. Its output is 20,000v and I would hate to see you get shocked. My guess is that if you enable the laser the LPS will arc from that red wire in spite of the insulation.



Be careful ....



On the end stops you can put a fixed stop so that the carriage will never get fully driven into the end stop sensor. Its a common failure for the end stop to crash into the sensor and damage it.



I like the laser mount. I plan to replace the current laser mounting in my K40 with a modular sub-unit when my laser dies and I upgrade to more power. Do you plan on sharing that design?






---
**Duncan Caine** *May 02, 2018 12:36*

**+Adrian Godwin** Hi Adrian, well spotted. I'm just outside the M25 near Leatherhead.  On the housing I've not yet decided other than the housing will fix directly to the blue frame


---
**Duncan Caine** *May 02, 2018 13:00*

**+Don Kleinschnitz**  Don, many thanks.  I'm competent with mechanical engineering but not with electronics.  Your point about the LPS is taken, the thought of arcing never even occurred to me I just insulated it so I wouldn't touch it.   Good idea about the end stops, that's now on my list to do.



I have a DXF drawing of the clamps, but do check the tube diameter as it may not be the same as mine.  You're welcome to a copy, just let me know how to get it to you.  



BTW, thank you for your Blogspot on the replacement 10 turn pot.  I bought mine straight after reading your blog, it hasn't arrived yet but it looks like it has an inset window to show the turn you are on ... 

[ebay.co.uk - Details about 1PCS 3590S-2-502L 5K Ohm Rotary Wirewound Precision Potentiometer Pot 10 Turn](https://www.ebay.co.uk/itm/1PCS-3590S-2-502L-5K-Ohm-Rotary-Wirewound-Precision-Potentiometer-Pot-10-Turn/302310734162?ssPageName=STRK:MEBIDX:IT&_trksid=p2057872.m2749.l2649)


---
**Don Kleinschnitz Jr.** *May 02, 2018 15:50*

You can email me the file at don_kleinschnitz@hotmail.com. Alternatively you can save it to your Google Drive and share it.


---
**Frederik Deryckere** *May 03, 2018 15:35*

great project! Not too dissimilar from my build. There'll be a huge update later today or tomorrow at the latest on here. 



One thing to keep in mind: motors are also generators. When you move an axis that is connected to a motor, you generate current. Do that too fast and the current generated may fry your drivers.



At least that is how I have always been told. If this is incorrect, someone chip in, but I'd rather be safe than sorry.


---
**Don Kleinschnitz Jr.** *May 03, 2018 17:38*

**+Frederik Deryckere** good question to discuss. I thought the drivers are protected but never checked. It bothers me cause it actually powers the board and turns on leds etc. Never caused failure though. Both my CNC and K40 do it.


---
**Adrian Godwin** *May 03, 2018 22:44*

Some stepper drivers have internal protection diodes (perhaps a part of the mosfet structure) which shunts unwanted braking power to the supplies. In some cases this can exceed the permitted supply voltage and break things - even by sharply braking when moving under power. If there aren't adequate internal diodes, or additional diodes mounted on the pcb, these currents can flow through inadequate routes.



So in general - yes, turning the stepper motors will generate power. In some cases it's dealt with adequately. In better systems it will be safely dumped even when switched off. Lights coming up on the board suggest it doesn't have a completely safe dump, and may or may not have adequate diodes to carry the current into the unpowered supply rails.



I do tend to turn steppers by hand, but if I saw it powering the board up I'd be uncomfortable about it.






---
**Frederik Deryckere** *May 03, 2018 22:47*

I do it on both my cncs, but always slowly. Thanks for your input btw.


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/fvRCceEQqsC) &mdash; content and formatting may not be reliable*
