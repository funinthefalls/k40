---
layout: post
title: "Hey I need some help with this laser"
date: November 11, 2015 09:23
category: "Discussion"
author: "Bharat Katyal"
---
Hey I need some help with this laser. Ive had it for 4 months (Bought preowned and seller used under 1hour)



Im cutting 3mm and 2mm acrylic, after a few months I finally got mirrors aligned and laser cut perfectly for 2 week. I basically had 6mm lifted from the regular cutting area. Eventually it started during in a certain corner and I started messing with it. At this point its just burning the acrylic and not even cutting through.



Im trying to figure out how to resolve.

1 Issue I noticed is I would use regular tap water, I looked inside the lens area and it looks like it might have gotten some contamination due to FL water?

Issue 2 is leveling the acrylic I'm cutting closer to the laser head.





Any help would be appreciated 

Thanks





**"Bharat Katyal"**

---
---
**Bharat Katyal** *November 11, 2015 09:25*

now I feel its burning more when it goes straight direction.

I just know certain directions it burn like right now its left to right direction


---
**Bharat Katyal** *November 11, 2015 09:26*

Power is 50-90 % always


---
**Anthony Bolgar** *November 11, 2015 20:39*

You are dramatically shortening the life of the laser tube running that high power. You should never be mor than 18mA, and the lower the power setting the longer the tube lasts.


---
**Scott Thorne** *November 11, 2015 21:35*

I only use distilled water.


---
**Scott Thorne** *November 11, 2015 21:36*

Rather than increasing power you should slow down the cutting head, mine works best at a speed of 4 or 5 when cutting.


---
**Scott Thorne** *November 11, 2015 21:39*

Another thing is never leave the door to the cutting area open while cutting, the soot from the acrylic will rise straight to the lens and cloud it up...instantly.


---
*Imported from [Google+](https://plus.google.com/109809891684909604662/posts/T6qPk53nsxq) &mdash; content and formatting may not be reliable*
