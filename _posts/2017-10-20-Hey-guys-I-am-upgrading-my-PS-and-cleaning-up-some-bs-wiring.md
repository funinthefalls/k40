---
layout: post
title: "Hey guys, I am upgrading my PS and cleaning up some bs wiring"
date: October 20, 2017 16:03
category: "Discussion"
author: "Abe Fouhy"
---
Hey guys,



I am upgrading my PS and cleaning up some bs wiring. I just created my wiring diagram and had to clean up some rats nests in the tube area. Now that it is out I noticed that the wires are stripped and there was arching when I stopped it several months ago. I need to replace this the correct way.



Question:

1.  I saw two ways to install the tube, wrap the wire  and silicon OR wrap the wire, solder it (quickly) and then silicon it. 

2.  The plug is cracked/ripped too, can I just use a piece of tubing and silicon into it?

3.  Which way does the tube point up? Is it the incoming or outgoing water nipple up?





**"Abe Fouhy"**

---
---
**Don Kleinschnitz Jr.** *October 20, 2017 16:29*

The link below may help, also check other areas of my  blog.



1. The link below has a few approaches. I think the safest and most popular is to wrap the wire. Soldering must not overheat the pin and needs to be HV compatible. I never saw an advantage of soldering. Opinions vary.



2. You can use a piece of silicon tubing and either the silicon (white) that came with the machine or the "blue" silicon that is referenced in my blog. I think there is references to replacement in the link below.



3. Having the water outlet UP can aid in keeping bubbles from forming but it also can allow arcs at the anode end if it gets to close to the case. Note: rotating the tube will likely cause realignment. My plan is next time an alignment is needed, I will rotate the tube but insulate the anode area of the chamber with 1/4" acrylic using double back tape.

 



[donsthings.blogspot.com - K40 Laser Tube Specifications, Maintenance, Failure & Replacement](https://donsthings.blogspot.com/2017/05/k40-laser-tube-specifications.html)


---
**Abe Fouhy** *October 20, 2017 16:38*

Don you are the man! Thanks for the quick response!


---
**joe mckee** *October 20, 2017 17:18*

don't solder ..the glass or the connection inside the tube will fail ...i use a 5a connector and silicon over it ..point HV connector to the furthest away point to metal .also looking at getting your water feed at a high point too ..






---
**Nate Caine** *October 20, 2017 20:57*

Don't attempt to solder.  



The tube manufacturers warn you that the pin material is chosen to create a robust glass to metal bond, but the pin itself won't bond to solder.  (Kind of like soldering to aluminum--ain't gonna happen).  



I use a terminal pin from a spare connector, and solder the lead to the terminal connector, and then attache the connector to the tube pin with a screw--a solid mechanical connection.


---
**Abe Fouhy** *October 21, 2017 00:11*

Awesome guy! Just to check I should use BLUE RTV gasket sealer from the auto parts store?


---
**Steve Clark** *October 21, 2017 01:21*

I believe you want something like this:



[amazon.com - Robot Check](https://www.amazon.com/dp/B0195UWAHG/ref=asc_df_B0195UWAHG5225520/?tag=hyprod-20&creative=395033&creativeASIN=B0195UWAHG&linkCode=df0&hvadid=198095726143&hvpos=1o2&hvnetw=g&hvrand=17109591417523026753&hvpone&hvptwo&hvqmt&hvdev=c&hvdvcmdl&hvlocint&hvlocphy=1014257&hvtargid=pla-318872280169)


---
**Don Kleinschnitz Jr.** *October 21, 2017 01:54*

The blue rtv works fine. Referenced on my site and it's only a few $$$. Can get at Auto and big box stores. 


---
**Abe Fouhy** *October 21, 2017 02:00*

Cool, that is what I got as per your blog


---
*Imported from [Google+](https://plus.google.com/107771854008553950610/posts/ePN4wEV1pJG) &mdash; content and formatting may not be reliable*
