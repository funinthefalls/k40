---
layout: post
title: "Hey guys! Any chance of some help!!???"
date: November 27, 2015 14:26
category: "Hardware and Laser settings"
author: "Martin Byford"
---
Hey guys!

Any chance of some help!!???

The machine will run as if it is printing but the laser will not fire!!!

Any ideas??





**"Martin Byford"**

---
---
**Gary McKinnon** *November 27, 2015 15:19*

Some people have this problem if the machine isn't grounded properly. Hopefully someone here can explain how to check for that ?


---
**Scott Thorne** *November 27, 2015 17:46*

Does the tube fires when you press the test button? Is the laser switch depressed?


---
**DIY3DTECH.com** *November 27, 2015 19:53*

Martin, after watching the video I think your saying the unit fires when you press the test fire button but not via LaserDrw?  Had similar issue with mine as it ran for a while but then stopped working.  So I uninstalled the software (windows uninstall) and reinstalled and it started working again.  In addition, I never (either time) told it the board ID it seem to read and update itself.  Hope this helps...


---
**DIY3DTECH.com** *November 27, 2015 20:09*

Gary to your question about ground, if there is no "ground" then you have no current loop.  The HV power supply works like a high frq multi/vib circuit to create a high voltage  arch in the flash tube.  My unit has (US 110) three a wire Hot/Neutral and Ground connection.  The reason for the chassis ground is safety as even if you have a GND connector on your outlet, you can not be assured it is real or sufficient.  I can't tell you the times I have seen electricians tie to water pipes for ground and plumbers come it in splice in polypropylene  meaning the home owner is at risk as their ground is gone.  As for good measure a home owner should have an 8 foot earthen stake tied to their main panel for ground.  As keep in mind inside that metal box is a thin wire with 25,000 volts and if shorts that chassis will be energized with with voltage and with no ground it is likely to another path (i.e. the operator).  


---
**Martin Byford** *November 27, 2015 23:28*

I FIGURED IT OUT !!! 

The 4 small white wires coming out of the laser power box they were in reverse. The k+ and k- plug had to be swapped with the other one there. Thanks for all the help ! 


---
**DIY3DTECH.com** *November 28, 2015 03:05*

Cool!  However from your video it was unclear, so it never worked with the software correct?


---
**Gary McKinnon** *November 28, 2015 12:52*

**+Martin Byford** Good job :)


---
*Imported from [Google+](https://plus.google.com/109380924123528435976/posts/VmSyXFBTNbj) &mdash; content and formatting may not be reliable*
