---
layout: post
title: "I am trying to get everything dialed in and I am having trouble in the bottom right-hand corner of my engraving area"
date: August 13, 2016 14:20
category: "Hardware and Laser settings"
author: "Bradley Blodgett"
---
I am trying to get everything dialed in and I am having trouble in the bottom right-hand corner of my engraving area. I can't seem to figure out if it's a mirror alignment issue or if my platform is out of whack. Pictured is the back side of a cut job that had two passes. You can see that it didn't quite make it all the way through in the bottom right corner (left in this picture). I am still using the stock platform with the useless clamp in the middle. I have done a little shim work on it with the level, but I am afraid to over-rotate on one line of troubleshooting if I'm barking up the wrong tree. Any guidance would be appreciated!



![images/a4a29fc0f80e13e51b88a316e949c3a8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a4a29fc0f80e13e51b88a316e949c3a8.jpeg)
![images/56c811fb99a12930245995303e2beb68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56c811fb99a12930245995303e2beb68.jpeg)

**"Bradley Blodgett"**

---
---
**Ariel Yahni (UniKpty)** *August 13, 2016 15:05*

I would love to tell you there is a simpler solution but their isn't. Its all about level


---
**HalfNormal** *August 13, 2016 15:18*

First thing I did was remove the clamp plate and replace it with a bakers cooling rack bought at the local mega store (Walmart). Quick, easy and cost only a few dollars. 


---
**Jim Hatch** *August 13, 2016 15:23*

Bet it's not square and level (parallel). My right front corner was off square by 3/8" if I recall.


---
**Eric Flynn** *August 13, 2016 15:43*

Its actually not about being level!  A level is absolutely the wrong tool to use here.  What matters is that your head is travelling square to your bed , or your bed is square to your head travel.  A level doesnt really help you there.  You need to measure the relative distance from a fixed point on the head, to the bed, and adjust accordingly.



A dial indicator mounted to the head is a ideal tool to use. A caliper would be the second option.  Id neither of these is available , a good steel scale will get you close.


---
**Jim Hatch** *August 13, 2016 16:06*

**+Eric Flynn** That's right. Level was the wrong word - it's got to be on the same planar surface across the whole thing. The other thing to check is if the tube is parallel to the X-axis gantry. That way the beam doesn't go askew.


---
**Eric Flynn** *August 13, 2016 16:40*

Yes. The tube needs to be square to the motion system as well.  Good mentioning that.



Actually, the squareness of the optical path, with relation to the motion system and bed, is the most important factor for good beam delivery next to mirror/lens alignment.  They are acually both equally important and play on each other.  They play on each other as well.



Getting your machine square first, will make alignment much easier, and allow your cuts to be the best they can possibly be.


---
**I Laser** *August 13, 2016 21:20*

Those pics look like incorrect focal length. The lines shouldn't be that thick.



Though could be charring from multiple passes and no air assist.


---
**Jim Hatch** *August 13, 2016 21:30*

**+I Laser**​ They could also be bad lines - not hairline (.001mm) so it draws them instead of cutting.


---
**Tev Kaber** *August 16, 2016 16:49*

I have the same issue, haven't had time to check my leveling and alignment yet.


---
**J.R. Sharp** *September 08, 2016 13:51*

Yup, this is what I am experiencing too.




---
*Imported from [Google+](https://plus.google.com/+BradleyBlodgett/posts/eipcBhyUy7i) &mdash; content and formatting may not be reliable*
