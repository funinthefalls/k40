---
layout: post
title: "Anyone used this software with the K40?"
date: September 21, 2016 20:05
category: "Software"
author: "greg greene"
---
Anyone used this software with the K40?



[http://rdworks.software.informer.com/8.0/](http://rdworks.software.informer.com/8.0/)





**"greg greene"**

---
---
**Cloudbase Engineering** *September 21, 2016 20:38*

I used this software with a Morn Laser and it worked well.  I only ever used it to import DXF files and than cut.  One problem I had was sometimes my DXF would be wrong.  It had issues sometimes with a dxf made from solidworks that had any fillets.  It would do crazy things near the fillet.  To fix it I had to set Solidworks to make the DXF as if it were a very old version of autocad and then it would work about 75% of the time.  The other 25% I would have to jump through hoops.  The machine was DSP controlled if that makes any difference.


---
*Imported from [Google+](https://plus.google.com/110995417849175821233/posts/6ApFVaBVJqr) &mdash; content and formatting may not be reliable*
