---
layout: post
title: "Good morning, Last night I noticed condensation on both ends of the laser tube"
date: May 18, 2017 13:33
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Good morning,



Last night I noticed condensation on both ends of the laser tube. I woke up this morning to the laser not firing. It doesn't fire at all on low power (14). When I turn it to 24 I see a spark and hear a crackle from the right side of the tube (the end with the red wire connected). Laser head and everything else acts normal,  just no laser beam coming out.



I'd appreciate any guidance on what's the issue or at least where to start...







**"Nathan Thomas"**

---
---
**Phillip Conroy** *May 18, 2017 23:54*

Dry everything out and try firing laser 


---
**Phillip Conroy** *May 18, 2017 23:55*

What temp are you running the coolest at,room temp


---
**Nathan Thomas** *May 19, 2017 00:40*

Yeah I did and still giving me the same results.

It was a hot day yesterday but the coldest the tube got was 19C which I didn't think was too bad.

But I'm guessing I have bigger problems now....just don't know what it is?




---
**Steve Clark** *May 19, 2017 01:23*

What is you humidity? It’s really important given what I saw in you pictures. Also look at your first post the day it happened. I suggested getting a meter to test that.


---
**Nathan Thomas** *May 19, 2017 01:26*

Yeah the problem now though is that it's not firing...any idea why? Or does it just need to dry out for a few days...?


---
**Nathan Thomas** *May 19, 2017 01:27*

I'm in the south so yeah there's a ton of humidity this time of year. 


---
**Steve Clark** *May 19, 2017 02:48*

Yes, that's going to be a big problem for you. Take a look at this webpage and Table 1 that will give you an idea of what your fighting. AC would help but the meter at the least would let you know when there is going to be a problem.



The only thing I can suggest is to warm it up and hope you can dry things out. I know it sounds counter to what seems logical but in this case you want to evaporate the moisture on the tube. 



Warming it up with a heater then cooling it down to room temp may give the best chance because you need to get your air to the point that it will take on more water as a gas. So using Gay-Lussac's Law the temperature is all you got to work with.  Also, in this case having your water coolant at room temp is of benefit too and necessary.



The table will help but to really avoid the issue for you it’s monitoring the dew point so maybe you need to get a meter.




---
**Nathan Thomas** *May 19, 2017 02:57*

Thanks a lot 



So the issue is the tube?

If it's not firing does it mean a new tube? Or is it something in the wiring?


---
**Steve Clark** *May 19, 2017 03:09*

I don’t know for sure. I can just say that given your conditions the only way you will know is if you can get the tube dried out. But IMO you do need to solve the humidity problems too...even with a new tube.


---
**Nathan Thomas** *May 19, 2017 03:22*

Gotcha...i see



By dried out, do you mean including the water inside the tube? Or just warming up the tube with the water inside?


---
**Steve Clark** *May 19, 2017 03:34*

Just warming the tube with water inside. I might add that during the cooling don’t rush it and make sure your coolant is at the same or high temp than the tube. 



What I believe you need is to know if your tube is still good and the connections are without a easier electrical path. That means getting it dry.



Disclaimer: I am real neo-type and on a steep learning curve about circuitries. But I know professionally a lot about air gases and humidity.  


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/18aQWtwjw7s) &mdash; content and formatting may not be reliable*
