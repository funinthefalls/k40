---
layout: post
title: "So i need advice from someone out there :) i recently purchased the VEVOR Laser 40W CO2 (China) Laser Machine, and was thinking of upgrading to the parts as below : 1"
date: March 21, 2017 16:29
category: "Modification"
author: "Ian Busuttil Naudi"
---
So i need advice from someone out there :) i recently purchased the VEVOR Laser 40W CO2 (China) Laser Machine, and was thinking of upgrading to the parts as below :



1. 18mm Laser Head w/Air Assist. Ideal for K40 Machine



2. 20mm Gold Plated Reflection Mirror



3. High quality 18mm ZnSe Focus lens (F50.8mm)



4. 135W Air compressor/ pump for CO2 laser AC110V



(As in attached images)



Has anyone done all or any of the above upgrades and if so did you see i big improvement in cutting power and decrease in flame marks on the wood.



I look forward to hearing from you all as i am new to laser cutting :)







![images/76a5ac7e0cf4d5276ba21aacfff43386.png](https://gitlab.com/funinthefalls/k40/raw/master/images/76a5ac7e0cf4d5276ba21aacfff43386.png)
![images/83583ec9771464a07f61370345239777.png](https://gitlab.com/funinthefalls/k40/raw/master/images/83583ec9771464a07f61370345239777.png)
![images/6c7b62ef4f6243b5e7c22a11fbe3290e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/6c7b62ef4f6243b5e7c22a11fbe3290e.png)
![images/823ac54f38de1c9cf0672b3630e2cff3.png](https://gitlab.com/funinthefalls/k40/raw/master/images/823ac54f38de1c9cf0672b3630e2cff3.png)

**"Ian Busuttil Naudi"**

---
---
**Chuck Comito** *March 21, 2017 16:43*

I have this exact item and feel that it's a must have. Air assist made all the difference. I'd like to have a look at the pump you mention. Do you have a link?


---
**Ian Busuttil Naudi** *March 21, 2017 17:13*

Thanks for your reply yes sure this is the link :



[lightobject.com - 135W Air compressor/ pump for CO2 laser AC110V](http://www.lightobject.com/135W-Air-compressor-pump-for-CO2-laser-AC110V-P832.aspx?ItemId=1287170)



I also found it here at a similar price : 



[https://www.sign-in-china.com/products/15966/135w_electromagnetic_air_pump_for_laser_cutter_laser_engraving_machine_220v.html](https://www.sign-in-china.com/products/15966/135w_electromagnetic_air_pump_for_laser_cutter_laser_engraving_machine_220v.html)



Do yo use the same lens i mentioned ?


---
**Ariel Yahni (UniKpty)** *March 21, 2017 17:22*

If I had to choose I would go for something that will blow air directly into the cut


---
**Ian Busuttil Naudi** *March 21, 2017 17:30*

I am going to use a pipe to connect to the head of the laser directly to the pump exit as the head i am going to buy has place for an air intake as that good?


---
**Ariel Yahni (UniKpty)** *March 21, 2017 17:38*

I'm not sure what that means but if it is air directly into the cut without going into the head sounds good


---
**Nate Caine** *March 21, 2017 17:53*

I'd be interested to know the construction of that pump.  On a similar one we received, several of the "metal" pieces were molded plastic painted silver.  So not nearly as rugged as it appears.



The brass metal barb on the pump had not been dirlled completely thru.  So initially no air at all.  Great Qualtiy Control!


---
**Lance Ward** *March 21, 2017 18:05*

You don't really need the new 20mm mirror for the new head... The mirror from the original head will fit and work fine.  My LO air assist head came with an adapter ring so I could use the factory lens from the original head.  I wasn't aware of this when purchasing however so I bought and am using the same lens you listed.  I'm also using a similar air pump (80W version) and so far everything has been working well.


---
**Chuck Comito** *March 21, 2017 18:30*

For the head I used the stock mirror but I am using that lense. I don't have a good comparison to the original because my original lense sort of crumbled away. As far as air through the head in regards to what **+Ariel Yahni**​ mentioned about it blowing on the cut, I don't know if it matters where the air comes from. But there may be some science as to why you want it from the outside vs the inside. I like it inside because I feel it helps from contaminants entering up onto the lense. I might be wrong though. I'll also mention that my air assist did not come with a lense adapter and initially made my own to reuse the stock 12mm one. I also use a large air compressor for my source and blow it rather hard through the nozzle. At least much harder than those little pumps are capable of.  


---
**Joe Alexander** *March 21, 2017 18:43*

From my research I found that having it enter the head is ideal as it creates a positive pressure, keeping debris and smoke from fouling the focus lens as quickly. It also still directs the air directly into the cut. I am using the LO head and it works great.


---
**Ian Busuttil Naudi** *March 21, 2017 19:01*

That's great Thank You all for your replies it was SUPER helpful i will ask if the new head comes with an adapter ring if not i will try creating some form of washer. The head i will be getting sends the air directly onto the cut so it should be good.



On another note just another quick question is it normal that the laser hits the head i have towards the top of the hole like in the screen shot i have attached or should i try lower it more towards the center of the hole?



Thanks again!





![images/903d423a98e34b2de5672fa12eb13cff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/903d423a98e34b2de5672fa12eb13cff.jpeg)


---
**Joe Alexander** *March 21, 2017 19:36*

Where it hits in that hole only matters if it is "clipping" your beam upon exit from the nozzle end. Put a piece of tape on the end and fire, you should see a circle with a dot in the middle. If its crescent then your beam is hitting the nozzle and needs adjusting.


---
**Mark Brown** *March 21, 2017 20:15*

In addition to what Joe said.  If it's hitting that spot no matter where the head is you're fine.  If the spot moves up as you move the head to the right lower it.


---
**Ian Busuttil Naudi** *March 22, 2017 19:15*

Hi I did the tests it's not crescenting so I guess it's good just did my first official cut have a look design from [http://www.thingiverse.com](http://www.thingiverse.com)





![images/aa08facbd82770abe787ec90688558fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aa08facbd82770abe787ec90688558fc.jpeg)


---
*Imported from [Google+](https://plus.google.com/113176208318796732189/posts/85rSGbH891J) &mdash; content and formatting may not be reliable*
