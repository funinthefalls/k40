---
layout: post
title: "K40 upgrades: Lid interlock switch Air assist pump Fume extractor Coolant flow interlock"
date: July 16, 2015 22:44
category: "Modification"
author: "David Richards (djrm)"
---
K40 upgrades:



Lid interlock switch

Air assist pump

Fume extractor

Coolant flow interlock.



I have arranged the air assist pump to turn on when the laser is enabled, this uses a ss relay driven with a transistor. I did this mainly to reduce the annoying noise.



The lid and water interlocks are wired in series. 

I totally removed the flat extractor duct inside the machine but left the fan in place.

![images/2ccb70185ddf4a8af20993a4a6f87fa5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2ccb70185ddf4a8af20993a4a6f87fa5.jpeg)



**"David Richards (djrm)"**

---
---
**Jon Bruno** *July 16, 2015 22:54*

Did you install a large capacitor on the relay? 

The switching of the laser state causes the pump to turn on and off at a pretty high frequency.  It may not like that very much.


---
**Jon Bruno** *July 16, 2015 22:54*

Did you mean Enabled or Firing?﻿


---
**David Richards (djrm)** *July 16, 2015 22:55*

Enabled


---
**Jon Bruno** *July 16, 2015 22:56*

Ahhh ok


---
**I Laser** *February 15, 2016 00:06*

**+david richards** trying to connect a flow switch to my machine. Making an assumption here and assuming you have the same machine considering your graph plotting power % and mA and the above pic with the internal LED light. So could you please advise what you hooked your flow switch to? Thanks!


---
**David Richards (djrm)** *February 15, 2016 09:00*

**+I Laser** My Laser power supply had a small white two pin socket, plugged into it was a two pin plug, the two wires in this plug had a small stub of wire, the two ends were connected together shorting the two terminals together. 



I think these are the K- connections - I'll have to check. When these connections are open the laser does not fire.



Both the flow sensor and door switch have change over contacts, I used the contacts which are closed when everything is normal, and wird them in series to the two plug terminals.

When both water is flowing and the door is shut the laser can fire.



I chose a mores expensive type of flow switch which has the switch output instead of the kind which generates pulses, these would require additional circuitry to work in the way I have wired it, something like a NE555 could be used as a pulse streather / relay driver if this was needed. I just wanted to keep it simple.

I did forget to turn on the water pump once and it took a while for me to reaslise what the problem was - there is no indication that the interlock is active, but it worked as desired otherwise.



hth David.


---
**I Laser** *February 16, 2016 08:34*

Thanks David, is this the plug you are referring to?



[http://s24.postimg.org/eic96f0r9/k40d.jpg](http://s24.postimg.org/eic96f0r9/k40d.jpg)



Highly professionally circled lol!



They've just dumped a bunch of solder in there.



By the way, would much prefer to not know the interlock had kicked in, than blow a tube! :)


---
**David Richards (djrm)** *February 16, 2016 09:47*

**+I Laser** Yes, that is the connection I am using.


---
**I Laser** *February 16, 2016 10:42*

Thanks again David. Now time to work out how to remove the solder (not a huge fan of soldering PCB's, let alone trying to remove solder from a PCB..)



When you talk about your switch, I thought most switches were either circuit connected or not connected. What do you mean by pulses?


---
**David Richards (djrm)** *February 16, 2016 21:08*

**+I Laser** Hi, there is a kind of flow sensor which uses a spinning magnet next to a hall effect probe used to generate a series of signals whose repetition rate depends on the rate of liquid flow. This one is very inexpensive and looks as if it could connect directly to the coolant pipes. 

URL: [http://pages.ebay.com/link/?nav=item.view&id=360707100647&alt=web](http://pages.ebay.com/link/?nav=item.view&id=360707100647&alt=web) my flow sensor cost a bit more and the pipe connectors much more again. The downside would be that a circuit would need to be used to interface to the power supply interlock input. If I were designing a microprocessor controlled protection system then I would use it as it could measure the flow rate and allow a user settable low limit. Hth David


---
**I Laser** *February 16, 2016 21:25*

Not really versed on the electrical side of things so apologise if my questions are redundant!



I actually have one of those coffee flow sensors, if I understand due to the design it provides a pulse, which is great if you have suitable circuitry as it could be used to determine flow rates, set minimum flow levels, etc. Not much use otherwise!



I also have a flow switch that I believe is like yours with a reed switch. Water flows, circuit is closed, otherwise circuit is open. I assume this one would be suitable to connect to the aforementioned plug?



Thanks again ;)


---
**David Richards (djrm)** *February 16, 2016 21:30*

**+I Laser** Yes the one I have has a simple reed switch which connects directly to the power supply interlock inputs.


---
**I Laser** *February 16, 2016 21:49*

Great, thanks for all your help! :)


---
**David Richards (djrm)** *February 16, 2016 21:53*

**+I Laser** In case you have never seen it, then look up desoldering braid, it could help to clean up the blob of solder in the connector without having to dismantle the power supply. Good luck, David.


---
**I Laser** *February 17, 2016 05:41*

**+david richards** I just managed to find my solder wick. I'll have a play with the machine without the switch in to make sure it behaves itself. I have to sort my garage out before I can do that though (orders from the missus lol). ;)



Where'd you get those end caps on your flow switch? I'm going to need to sort that out too before I can proceed.



Interestingly I was under the impression that the switch had to be vertically mounted to function properly?


---
**mina saad** *July 21, 2017 09:41*

Hi,  I need to buy an air assist head and air compressor for my k40 to use it for cutting MDF. Can you please recommend the right type.

Thanks


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/4BERzcor7jQ) &mdash; content and formatting may not be reliable*
