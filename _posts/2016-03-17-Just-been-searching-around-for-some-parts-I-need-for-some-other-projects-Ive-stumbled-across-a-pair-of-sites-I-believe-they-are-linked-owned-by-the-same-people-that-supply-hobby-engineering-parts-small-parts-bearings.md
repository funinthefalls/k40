---
layout: post
title: "Just been searching around for some parts I need for some other projects & I've stumbled across a pair of sites (I believe they are linked/owned by the same people) that supply hobby/engineering parts & small parts/bearings"
date: March 17, 2016 20:41
category: "External links&#x3a; Blog, forum, etc"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Just been searching around for some parts I need for some other projects & I've stumbled across a pair of sites (I believe they are linked/owned by the same people) that supply hobby/engineering parts & small parts/bearings. Thought maybe someone may be looking for some parts & find this site useful to purchase.



They are located in Australia (Queensland), however it seems they are linked with some US dispatch service, for customers in the USA.



Anyway, here are the links:



[http://www.smallparts.com.au/](http://www.smallparts.com.au/)

[http://www.hobbyparts.com.au/](http://www.hobbyparts.com.au/)





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/39HF7HiWPkC) &mdash; content and formatting may not be reliable*
