---
layout: post
title: "I am finally back home and again working on my smoothies conversion"
date: September 09, 2016 18:08
category: "Modification"
author: "Don Kleinschnitz Jr."
---
I am finally back home and again working on my smoothies conversion.



I completed V1.0 model of the electrical design: 



[http://www.digikey.com/schemeit/project/k40-s-20-HRJ3GD82020G/](http://www.digikey.com/schemeit/project/k40-s-20-HRJ3GD82020G/)



This includes:

-All AC & DC  power and fuses

-Relay module to control accessories from panel and smoothie

-Smoothie hook up

-Interlocks, temp and flow control

-Middleman board & interconnects

-End-stops & interconnect

-GLDC



I also competed a model for a new approach to packaging:

Its a modular front mounted enclosure that modular-izes everything that is not K40 basic. This way I can use this packaging for other CNC projects.



There will be a DC power module that holds the DC supplies that is mounted inside the cabinet. I am using a separate 24VDC and 12VDC supply.



I will leave the current K40 panel mostly as it is except for adding the temp controller and current control pot meter.



Includes:

-Relay controls

-Accessory buttons that can override Smoothie control

-GLDC mounting

-Smoothie

-Strain reliefs

-AC sockets for accessories



[https://goo.gl/photos/FTGRJmE8eAWFQTqS7](https://goo.gl/photos/FTGRJmE8eAWFQTqS7)

[https://goo.gl/photos/qZx8oBq7eVSiJXEc7](https://goo.gl/photos/qZx8oBq7eVSiJXEc7)



Now I have to build all this stuff :).



Gosh posting pictures is ugly in G+, let me know if can get to these photos they may be private??

This is also posted on spaces....







**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *September 09, 2016 18:26*

Cool idea, photos are fine. I have to do something like that but include the PSU


---
**Anthony Bolgar** *September 09, 2016 18:28*

Nicely done.


---
**Don Kleinschnitz Jr.** *September 09, 2016 18:51*

**+Peter van der Walt** Nice that worked, thanks.

![images/e005a9a4a68996984badfa13eead1613.png](https://gitlab.com/funinthefalls/k40/raw/master/images/e005a9a4a68996984badfa13eead1613.png)


---
**Don Kleinschnitz Jr.** *September 13, 2016 04:39*

Adding the DC power supply packaging. 

This gives me 24 and 12 V at 5A each with jacks and fuses for each output.

![images/525170d35fbfff18ad1f39034aa77bb2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/525170d35fbfff18ad1f39034aa77bb2.png)


---
**Skull Duggary John E Clark** *September 17, 2016 19:57*

I have a question.  First off, Thank you very much for this K40-S-2.0  Man I understand how to work with that.  ok Couple questions.  Why didn't you use the level shifter or am I just not seeing it?  Why use a 24 and 12 Volt PSU separate from the laser PSU?


---
**Don Kleinschnitz Jr.** *September 17, 2016 21:40*

**+Skull Duggary John E Clark** I have concluded that a level shifter is not necessary and in fact I think creates an error in the programmed PWM value vs what the laser outputs, due to a DC offset. 

My plan is to use the L signal (the same signal used by the M2) that is on the LPS and an open drain on the smoothie. I will prove this and write it up as soon as I am running. Others have had success with it.

I wanted 12V because there are many cheap devices from amazon that use that voltage. My temp monitor is an example. The LED cabinet light and the Laser finder light are other examples.

I added a separate 24PS because I am not confident in the output capacity and quality of the LPS and did not want my smoothie on the same supply as a HV supply. Many others have used it successfully however. 

I am using the LPS 24V to drive the lift table electronics so it is not wasted and I care less about its connection to the HVPS. I also wanted plenty of capacity for the Smoothie and expansion.



Lastly, I wanted to make the smoothie conversion design such that I can use it for other CNC projects like a x-y mill and conversion of my 3D printer. The intention is that all my fab toys will use smoothie and Gcode tool chains. You will notice from other of my posts that the control panel and PS are self contained modules that can be used for any CNC project. So I do the design once and have a controller/control panel module and a PS module that is universal. 

The power design could have been more efficient by stretching and using the 24 supply for everything and using regulators for 12v if needed. Turns out these supplies were pretty cheap.


---
**Skull Duggary John E Clark** *September 18, 2016 03:16*

You know, I am by no mean an engineer, but I actually understood that. :)  It make since.  My only thing is the 12V.  Most everything is going by the way side on 12V.  Why hold on to it?  I understand completely about everything else.  Once you get it up and running, I would really like to hear how it is going.  My plan was to learn as much as I can on the conversion of the K40 and then start to build the CNC and 3D Printer from that.


---
**Don Kleinschnitz Jr.** *September 18, 2016 11:19*

I have the following things that run on 12V: laser finder, temp controller, cabinet lights. It is very common for things like timers, and other embedded controllers (arduino) to use 12v also.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/RQnK7N7VZey) &mdash; content and formatting may not be reliable*
