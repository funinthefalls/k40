---
layout: post
title: "Hi all. I finished my new Laser exhaust ducting setup, hidden within the wall and inside faux ceiling beams (only the middle beam is a duct)"
date: September 15, 2016 05:48
category: "Modification"
author: "Vince Lee"
---
Hi all.  I finished my new Laser exhaust ducting setup, hidden within the wall and inside faux ceiling beams (only the middle beam is a duct).  I'm really happy with how it came out.  No more tripping over hoses and goodbye fumes!



Full write-up here: [http://makermonkey.blogspot.com/2016/09/diy-faux-ceiling-beams-and-laser-cutter.html](http://makermonkey.blogspot.com/2016/09/diy-faux-ceiling-beams-and-laser-cutter.html)



![images/31b130bc7eb45e80a34b0ca2d09a17ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/31b130bc7eb45e80a34b0ca2d09a17ac.jpeg)
![images/fbc8c19fd44cea582586e15bbde99fee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fbc8c19fd44cea582586e15bbde99fee.jpeg)
![images/8b3a37da0e0c45f75d7355a7f6a47774.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8b3a37da0e0c45f75d7355a7f6a47774.jpeg)
![images/c3343226a352dff18187943c3ab8a42f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c3343226a352dff18187943c3ab8a42f.jpeg)
![images/a0e18b2a5513b42be0ce3fe6fc176358.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0e18b2a5513b42be0ce3fe6fc176358.jpeg)
![images/6afd7dac891a19c8804c56c20097d2fb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6afd7dac891a19c8804c56c20097d2fb.jpeg)
![images/6269d75955783364077c6e85b70c5805.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6269d75955783364077c6e85b70c5805.jpeg)

**"Vince Lee"**

---
---
**Scott Marshall** *September 15, 2016 06:55*

Pretty fancy!


---
**Don Kleinschnitz Jr.** *September 15, 2016 13:33*

Wow, nice work. What CFM blower is driving that duct?


---
**Vince Lee** *September 15, 2016 14:18*

**+Don Kleinschnitz** it's a 270 cfm 4" fan, which is the highest flow fan I could find that could fit entirely inside the duct (after sawing off its mounting tabs).  Seems pretty good so far.  Having the fan at the very end of the run seems to make a world of difference compared to before.


---
**Stephen Sedgwick** *September 15, 2016 20:08*

I am looking for someone to come do mine now ;-)  looks great!! I don't know that I would ever do something that fancy with mine as it isn't in a finished room to start with.... but WOW...




---
**Anthony Bolgar** *September 16, 2016 21:13*

You really knocked this one out of the park! Kudos!


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/UQZYr56khzd) &mdash; content and formatting may not be reliable*
