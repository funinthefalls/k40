---
layout: post
title: "My First attempt at engraving on slate, Im quite happy"
date: April 21, 2016 22:22
category: "Object produced with laser"
author: "Tony Schelts"
---
My First attempt at engraving on slate, Im quite happy. This was engraved at 150mm/sec at 7MA. 

I have a problem (many in fact) but this one is.  If I decide to put a circle around an item and then cut it,  it offsets it for no reason that I can think of.  If you look at the second picture you can just make out a cut line but it isnt in the correct place even though it is on screen..



![images/4536fbbcb5a095c6fc55eb8d80571c43.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4536fbbcb5a095c6fc55eb8d80571c43.jpeg)
![images/de297e60bd8e40bd98c3e1061d434593.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de297e60bd8e40bd98c3e1061d434593.jpeg)

**"Tony Schelts"**

---


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/7ZJUtn61sgR) &mdash; content and formatting may not be reliable*
