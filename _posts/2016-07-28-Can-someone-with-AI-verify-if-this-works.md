---
layout: post
title: "Can someone with AI verify if this works"
date: July 28, 2016 05:27
category: "Software"
author: "Ariel Yahni (UniKpty)"
---
Can someone with AI verify if this works [https://github.com/antishow/save-layers-to-svg](https://github.com/antishow/save-layers-to-svg)





**"Ariel Yahni (UniKpty)"**

---
---
**Ariel Yahni (UniKpty)** *July 28, 2016 05:28*

This one also [https://github.com/mtreik/export-svg](https://github.com/mtreik/export-svg)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 28, 2016 09:35*

I will give it a check & report back.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 28, 2016 09:44*

**+Ariel Yahni** Yep, the first script linked works. It breaks the AI file by layers into separate SVG files.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 28, 2016 09:48*

**+Ariel Yahni** Second script is a bit buggy, doesn't work with the same file I used for the previous script. It crashed giving some error & didn't create the Layer for Layer 1.


---
**Ariel Yahni (UniKpty)** *July 28, 2016 13:43*

It's up to you **+Peter van der Walt**​, I refuse to think that having to save multiple files from the same project and the openning them to United them again is the best workflow


---
**Ariel Yahni (UniKpty)** *July 28, 2016 13:43*

Mayne this also can help [https://hofmannsven.com/2013/laboratory/svg-stacking/](https://hofmannsven.com/2013/laboratory/svg-stacking/)


---
**David Cook** *July 28, 2016 19:25*

I just tested the first script link,  it worked fine.  exported a 3 layer AI file into 3 .SVG files.  simple enough,   But I do agree this workflow to re-align after export is extra work.  But that's what we have today and it works.


---
**David Cook** *July 28, 2016 19:46*

**+Peter van der Walt**  Makes sense.   This is the first time I've ever messed with a .svg file.  


---
**Ariel Yahni (UniKpty)** *July 29, 2016 03:39*

So going back to the Wiki workflow, instead of exporting 3 files we could do it in only one with colors vectors and embedded raster ( already supported ). Only thing to do is relocate the raster


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/RPrjJRX5HnJ) &mdash; content and formatting may not be reliable*
