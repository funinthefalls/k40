---
layout: post
title: "I've been following this community for a while and with the little extra cash I received for Christmas I think I'm finally ready to pull the trigger on a k40"
date: December 25, 2016 07:11
category: "Discussion"
author: "David Gray"
---
I've been following this community for a while and with the little extra cash I received for Christmas I think I'm finally ready to pull the trigger on a k40. Can anyone recommend a trustworthy seller that will ship to Australia for a reasonable price? Cheers.





**"David Gray"**

---
---
**Phillip Conroy** *December 25, 2016 07:56*

Where in Auare you?i am in central vic 


---
**David Gray** *December 25, 2016 07:59*

West Melbourne


---
**Phillip Conroy** *December 25, 2016 08:00*

Do you ever come to Bendigo


---
**David Gray** *December 25, 2016 08:01*

Occasionally, why?


---
**Phillip Conroy** *December 25, 2016 08:04*

You are welcome to come and see my laser setup ,iIhave had my laser  cutter for almost 2 years and know a few tips and tricks, so far blown a tube at 50 hours and worn a laser tube out at 600 hours use .what are you going to use laser cutter/engraver  for? 


---
**David Gray** *December 25, 2016 08:13*

I already have a jtech 2.8w laser attached to my shapoko running smoothieware. Just looking at an upgrade, mostly for engraving, but being able to cut stuff other than paper would be great 😝


---
**Phillip Conroy** *December 25, 2016 08:14*

The laser cutter is only the start of a laser system, you need air assist if cutting  other wise things just catch fire, Iuse a shop air ccompressor  for the air assist and that has its own problems,also a water flow switch will help save the laser tube if  a water pump fails ,water temperature  meters, so that you don't use llaser above 25 ddegree s ,be warned the stock exhauste fan is ccrappie, and if the laser machine uses adaptors for the power plugs that the supplyed one are not very good 


---
**Phillip Conroy** *December 25, 2016 08:19*

Then you are in for a treat the first time you do a laser mirror alignment, for the first one allow 2 days and a roll of masking tape,after doing a few Ihave got it down to 30 mminutes, tip buy spare mirrors and a  spare focal lens my mindphone number is 0434 550 678 give me a txt if you ever want to get together and look at my setup  


---
**David Gray** *December 25, 2016 08:20*

I have a compressor and a suitable air duct/fan setup that I currently use with my laser module to extract fumes. I know what I'm getting myself in to. I do however need to find a reliable supplier as reviews on AliExpress are pretty daunting. I haven't seen 1 seller with a good rating that also has good reviews.


---
**Phillip Conroy** *December 25, 2016 08:56*

I brought my machine on ebay 2 yearsago for $500 ,however Ihave noticed that they are closer to $1000 now. 


---
**Phillip Conroy** *December 25, 2016 10:47*

[https://www.aliexpress.com/item/40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-High-Precise/32722536840.html?spm=2114.01010208.3.294.mQfToc&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10000009_10084_10083_10080_10082_10081_10060_10061_10062_10056_10055_10054_10059_10099_10078_10079_427_10073_10103_10102_10096_10052_10050_10051,searchweb201603_3,afswitch_5&btsid=71c7f383-38e8-45b0-b7b9-6d77b8460fed](https://www.aliexpress.com/item/40W-CO2-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB-Port-High-Precise/32722536840.html?spm=2114.01010208.3.294.mQfToc&ws_ab_test=searchweb0_0,searchweb201602_3_10065_10068_10000009_10084_10083_10080_10082_10081_10060_10061_10062_10056_10055_10054_10059_10099_10078_10079_427_10073_10103_10102_10096_10052_10050_10051,searchweb201603_3,afswitch_5&btsid=71c7f383-38e8-45b0-b7b9-6d77b8460fed)


---
**Phillip Conroy** *December 25, 2016 10:47*

Link Isent has good rreviews even a couple of aAubuyers 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 25, 2016 14:07*

I bought mine on ebay (around Sept 2015) for about $550 & it shipped from Sydney. From recent discussions here, prices should drop after the whole Chinese New Year ends & should be back to around that price. Maybe spend a few weeks sussing it out & keep an eye on the price before you hit the Buy button.


---
*Imported from [Google+](https://plus.google.com/117535437951142576212/posts/TU5DxkXmXa3) &mdash; content and formatting may not be reliable*
