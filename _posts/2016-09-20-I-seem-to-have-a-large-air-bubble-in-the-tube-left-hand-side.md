---
layout: post
title: "I seem to have a large air bubble in the tube, left hand side"
date: September 20, 2016 17:21
category: "Discussion"
author: "Adam J"
---
I seem to have a large air bubble in the tube, left hand side. I'm not sure if this okay? If not, how do I get rid of it? Also a bit of frothing/foaming? I am using RO water with 3% alcohol added and a tiny spot of bleach . There's also a smaller bubble on the right hand side...



![images/c69a1d56f1078513201afbe69917ad6c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c69a1d56f1078513201afbe69917ad6c.jpeg)
![images/b37d40ece21ad36a1637fb981774a238.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b37d40ece21ad36a1637fb981774a238.jpeg)
![images/10f4175edd27956f333262870ed1f9c4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/10f4175edd27956f333262870ed1f9c4.jpeg)
![images/1e063af20ec8190ad9a7743dba0f1d05.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e063af20ec8190ad9a7743dba0f1d05.jpeg)

**"Adam J"**

---
---
**Michael Audette** *September 20, 2016 17:27*

I was able to clear mine by tipping the machine while running the pump.  Air bubbles always rise in water.  It also helps if the inlet and outlet are at the very top of the tube when it's leveled.


---
**Don Kleinschnitz Jr.** *September 20, 2016 17:35*

Bubbles are bad ....


---
**greg greene** *September 20, 2016 18:03*

It will work itself out - lots of reports on here of them


---
**Adam J** *September 20, 2016 18:12*

Ahhhh thanks for the tip. I spun my tube so the inlet & outlet pipes are at the top as suggested and watched the bubbles totally disappear. Thanks!


---
**greg greene** *September 20, 2016 18:15*

Awesome ! - I hadn't thought of that - I guess I get the Homer Simpson award for the day :)


---
**Don Kleinschnitz Jr.** *September 20, 2016 19:00*

Need realign after rotating tube.


---
**Michael Audette** *September 20, 2016 20:49*

What Don said...that why I rotated the whole machine.  Once the air is out - assuming you don't pump air back in it'll stay that way.




---
**Adam J** *September 20, 2016 21:25*

Indeed, just had the pleasure of 1 hours worth of realignment, firing almost spot on in all four corners again. No bubbles back too! Happy, for now :-)


---
*Imported from [Google+](https://plus.google.com/107819143063583878933/posts/HkX2hRJAHu4) &mdash; content and formatting may not be reliable*
