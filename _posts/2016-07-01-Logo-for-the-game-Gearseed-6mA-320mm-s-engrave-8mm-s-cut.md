---
layout: post
title: "Logo for the game Gearseed. 6mA, 320mm/s engrave, 8mm/s cut"
date: July 01, 2016 20:56
category: "Object produced with laser"
author: "Tev Kaber"
---
Logo for the game Gearseed. 6mA, 320mm/s engrave, 8mm/s cut. Cleaned up with some sanding. Piece is 50mm x 50mm.

![images/ff93b0365f190698f9bfd7578e1a3bd0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff93b0365f190698f9bfd7578e1a3bd0.jpeg)



**"Tev Kaber"**

---
---
**Tony Schelts** *July 01, 2016 22:46*

That's really nice


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/XCVCHuLbAcY) &mdash; content and formatting may not be reliable*
