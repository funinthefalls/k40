---
layout: post
title: "Still learning, if cutting material that is 4mm etc, do you want the focal length to be hitting the top centre or bottom??"
date: January 22, 2016 06:51
category: "Discussion"
author: "Tony Schelts"
---
Still learning,  if cutting material that is 4mm etc,  do you want the focal length to be hitting the top centre or bottom??





**"Tony Schelts"**

---
---
**Anthony Bolgar** *January 22, 2016 07:12*

I usually set the focal length to the middle of the material.


---
**Daniel Wood** *January 22, 2016 07:17*

Centre focus for cutting and out of focus for engraving generally gives best result. If you focus on the top or bottom surface you sometimes get an angled cut. Engraving out of focus leaves a softer finish which is usually nice but can cause problems on very fine text. 


---
**Tony Schelts** *January 22, 2016 09:02*

thanks for your help


---
**Norman Kirby** *January 23, 2016 09:22*

how do you set the focal lenght????


---
**Daniel Wood** *January 23, 2016 09:32*

The focal length of these lenses are fixed. You can find your focal length by engraving a piece of wood on an angle. Start at the high end and engrave to the low end. You will notice the thickness of the engraved line changes with the height between the lens and the work piece. Where the engraved line is thinest is the focal length of the lens. 


---
**Anthony Bolgar** *January 23, 2016 09:34*

K40 laser usually comes with a 50.8mm (2") focal length lens.


---
**Joseph Midjette Sr** *January 23, 2016 12:01*

**+Daniel Wood** So, what you are saying is after the focal length is determined, one would adjust the head up or down to set the height for the job at hand, be it cutting or engraving?


---
**Anthony Bolgar** *January 23, 2016 12:05*

Head cannot be adjusted, you have to adjust the work table that your work piece is sitting on. Most people remove the fixed bed, and install some type of adjustable bed, or just place blocking of various heights to get the work piece to the proper height under the head.


---
**Joseph Midjette Sr** *January 25, 2016 11:05*

Here is something I found on ebay. Maybe it will work along with a bed change may be beneficial? [http://www.ebay.com/itm/262150928817?euid=9ea9d4acf48f42d59e087b5cd9c2b1fd&cp=1](http://www.ebay.com/itm/262150928817?euid=9ea9d4acf48f42d59e087b5cd9c2b1fd&cp=1)


---
**Anthony Bolgar** *January 25, 2016 13:13*

That head is too big and heavy for a K40 laser machine. Light Objects has an adjustable Z table that is designed to work with the K40 as well as a replacement air assist laser head with an upgraded focus lens. [http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx](http://www.lightobject.com/Power-table-bed-kit-for-K40-small-laser-machine-P722.aspx)  and [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/9tFuycz6Yuf) &mdash; content and formatting may not be reliable*
