---
layout: post
title: "If you are going to sell a few products for K40 laser upgrades what is the policy talking about them here?"
date: February 02, 2018 19:54
category: "Discussion"
author: "Steve Clark"
---
If you are going to sell a few products for K40 laser upgrades what is the policy talking about them here? I could see it being a problem for the site owner if not handled correctly. I do not want to abuse this site. 



Are there any guidelines? I will have a general web site and for details, prices and ect. I'm more interested in just letting those on this site know about the availability, answer general questions and get feedback.  





**"Steve Clark"**

---
---
**Stephane Buisson** *February 02, 2018 21:19*

**+Steve Clark** thanks for asking, well I am quite open mind ...

depending on what you sell and the interest for the community, as an informative way, I would say I will let a first post. but please don't spam. you can alway post me in private if your are not sure.


---
**Steve Clark** *February 02, 2018 21:46*

Thanks’ for the reply Stephane.  I'll try a test private post to you.


---
**Joe Alexander** *February 03, 2018 05:56*

"terms and restriction apply, see store for details" :)


---
**Ned Hill** *February 03, 2018 15:57*

I personally don't have a problem with it especially from people like yourself that are engaged with the community.


---
**Steve Clark** *February 03, 2018 17:39*

The concern I have is how to let people know without encouraging spammers or becoming one myself.


---
**Ned Hill** *February 03, 2018 18:01*

If nothing else, you just get the word out initially and if you have good products then regular contributors like myself are usually happy to point other people to you.  **+Ray Kholodovsky** is a prime example of this with his C3D product.  If you engage with the community to make it better then we benefit, our knowledge base increases and nobody should begrude you making some money in the process.  Don't worry, if you become spammy somebody will definitely let you know ;)


---
**Don Kleinschnitz Jr.** *February 04, 2018 12:02*

I think referring to your product with a link is no different than someone providing a link to LightObject or such!

This assumes that your product is something that is relevant to a K40 :).


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/bNJPxG1ZUfd) &mdash; content and formatting may not be reliable*
