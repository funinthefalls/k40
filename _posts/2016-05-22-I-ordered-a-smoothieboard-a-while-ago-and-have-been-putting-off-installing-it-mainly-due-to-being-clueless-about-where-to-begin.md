---
layout: post
title: "I ordered a smoothieboard a while ago and have been putting off installing it, mainly due to being clueless about where to begin"
date: May 22, 2016 13:12
category: "Smoothieboard Modification"
author: "Darren Steele"
---
I ordered a smoothieboard a while ago and have been putting off installing it, mainly due to being clueless about where to begin.  I finally got started today and I have flashing lights and am able to make a telnet connection to the board.  Onwards and upwards!!

![images/e5e5e8587e70a808e38bfdb7a5ecc43c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e5e5e8587e70a808e38bfdb7a5ecc43c.jpeg)



**"Darren Steele"**

---
---
**Jim Hatch** *May 22, 2016 13:38*

😃 excellent. I'm in a similar position. I'm assembling the other parts & pieces so I can do it in a couple/three weeks. Super busy now with work & other hobbies that are time sensitive. I figure I'm going to burn a couple of weekends to get it done.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 22, 2016 16:30*

I guess starting is the first step. I have yet to order my smoothie, but I'm interested to see the experience others have during the install process as I think I might struggle through it a bit too.


---
**Dennis Fuente** *May 22, 2016 16:58*

you might also look at the lasersaur board


---
**Arthur Wolf** *May 23, 2016 07:27*

Hey if you need any help with the Smoothieboard don't hesitate to contact me directly.

Cheers.


---
**Stephane Buisson** *May 24, 2016 06:51*

please share with community, and don't hesitate to edit/add (depending on your power supply) on Smoothie website the following link:

[http://smoothieware.org/blue-box-guide](http://smoothieware.org/blue-box-guide)


---
*Imported from [Google+](https://plus.google.com/+DarrenSteele/posts/dJbEyKZRSX4) &mdash; content and formatting may not be reliable*
