---
layout: post
title: "Made my first box tonight! Laserdrw is a pain but with a bit of time and effort I can get it to do what I need it to do"
date: January 03, 2017 04:04
category: "Discussion"
author: "3D Laser"
---
Made my first box tonight! Laserdrw is a pain but with a bit of time and effort I can get it to do what I need it to do

![images/33abb957c95dd1a56b1bd1884962f274.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/33abb957c95dd1a56b1bd1884962f274.jpeg)



**"3D Laser"**

---
---
**Alex Krause** *January 03, 2017 04:17*

You should look into a grbl/smoothie upgrade... laserweb makes things like this a piece of cake 


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 05:02*

And, hoping you don't mind me jumping in, the Cohesion3D Mini's are going to be in stock this week so if there was ever a time to jump on the easy smoothie upgrade for k40 wagon, it would be now. Cheers, Ray


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 05:09*

Yes, but **+Claudio Prezzi**'s ReMix is almost arrived and we are hoping to change that.


---
**3D Laser** *January 03, 2017 11:03*

What ever one comes out with a plug and play option will be my hero I am a idiot when it comes to wiring and circuit boards.  Either that or a very awesome how to YouTube video


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 16:51*

The Cohesion3D Mini is drop in, the mounting holes are the same as the stock board and all known wiring connections are broken out.  It should be a fairly quick swap in. 

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Ashley M. Kirchner [Norym]** *January 03, 2017 21:37*

But the C3D doesn't make my coffee in the morning ... oh wait, it can drive a relay that turns the pot on. NEVER MIND! :)


---
**3D Laser** *January 03, 2017 22:33*

**+Ray Kholodovsky**  my power supply is different than the one shown also what software does it use 


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 22:37*

**+Corey Budwine** we also support the ribbon cable (shown) and if your power supply has all green terminals that's fine too. We support all the options we have seen so far. ![images/f603339ffa9d667c3b65d12cb1f5a66c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f603339ffa9d667c3b65d12cb1f5a66c.jpeg)


---
**3D Laser** *January 03, 2017 22:41*

**+Ray Kholodovsky** mine had all green ports also what software does it use is there a way to demo the software.  I noticed it was preorder when will they be In stock 


---
**Ray Kholodovsky (Cohesion3D)** *January 03, 2017 22:44*

LaserWeb is the software we recommend. You can see a lot of screenshots on its Google + community. 



The boards will be in stock in a few days and we will start shipping after the weekend. 



You can also see and get support at our community: [Cohesion3D](https://plus.google.com/communities/116261877707124667493?iem=1)


---
**Jérémie Tarot** *January 05, 2017 22:34*

**+Peter van der Walt** you just slammed my champion :'( and with all the time I spend reading everything in related G+ communities, this nevertheless comes out of the dark ! Where should I go to be that informed about GRBL ? Or maybe it's just that I don't grasp all I read... 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/GUCXkVk2DBD) &mdash; content and formatting may not be reliable*
