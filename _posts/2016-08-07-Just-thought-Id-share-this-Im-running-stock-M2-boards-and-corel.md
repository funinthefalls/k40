---
layout: post
title: "Just thought I'd share this. I'm running stock M2 boards and corel"
date: August 07, 2016 23:58
category: "Original software and hardware issues"
author: "I Laser"
---
Just thought I'd share this.



I'm running stock M2 boards and corel. All jobs are set out in corel with the occasional import of SVG's. So the issue doesn't seem related to weird import issues.



I've noticed with certain jobs, usually complex engraving, that the machine will glitch. Basically it's like it jams for a split second and the head doesn't advance down, so two objects that should have say 15mm between them end up butted together. It happens on both machines, so it's not hardware related. It's been frustrating and for some time I've resorted to engraving elements separately when processing known problematic files.



I had an idea yesterday, what if I place transparent boxes between elements where the head skips. It actually seemed to work, a job that constantly skipped engraved in one run without a hitch. Not necessarily saying it's a definite fix but it seems to be, more testing required I guess.



Anyone else experienced similar issues?





**"I Laser"**

---
---
**Scott Marshall** *August 08, 2016 00:46*

USB cable noise could do it.

USB cables are ground loop making machines, and they have the noise sensitivity to go with the giant noise pickup coil they generate. Use only the cables you have to (unhook the chargers etc when you're using the Laser cutter, try to get the cable count as low and short as possible..



Use good quality cables with the little ferrite beads  on each end. Gold plating doesn't mean a cable is good. Look for cables with foil and braided shields.



Keep the power hungry and noisy appliances off when cutting. This is noisy things like microwave ovens, big motors like washing machines that produce transients when they turn on and off, or shift gears.



Even after all the cutting data has been sent to the controller (The M2nano doesn't have much memory, so it "draws" from the PC frequently, noise can follow the cable in and interuppt the processor.



A bad connection in your laser can do it too. The wiring in a K40 is not very secure, and is prone tyo intermittent connections. a drop out while cutting will produce missed steps, a processor reset, or almost anything you could think of.



Your transparent box solution would suggest a software issue, but they are rarely intermittent or random in occurance. not that's it's impossible, it's just rare.



Scott


---
**Ariel Yahni (UniKpty)** *August 08, 2016 02:54*

Noise / controller buffer limits as stated above 


---
**I Laser** *August 08, 2016 05:32*

Thanks for the replies but I don't think it's interference. The same files glitch at the exact same points on both machines regardless of whether they're run in a vm or the host.



It also only glitches where there's white space. :\


---
**Scott Marshall** *August 08, 2016 05:35*

Well, NOW you tell us.



Never mind....


---
**I Laser** *August 08, 2016 06:12*

Uh Scott, re-read the OP I did tell you it's not hardware as it does the same thing on both machines.



If it did it randomly why would I think that adding transparent boxes would make any difference? Also engraving the file in parts wouldn't have been an interim solution either...



Feel like I've pointed out a possible solution to a problem I assume others have encountered and have been flamed because you misinterpreted the post.


---
**Phillip Conroy** *August 08, 2016 06:26*

What typr of computer are you running the coral draw on?


---
**Scott Marshall** *August 08, 2016 06:55*

**+I Laser**

I was just giving you a hard time. I'm not flaming you. I'm sorry if it came off that way. "Never mind" is a joke from 1980's Saturday night live, I guess I was reaching with the reference, it kind of dates me.



I thought you knew me well enough you would know I wasn't serious., I was wrong.



I apologize  for coming off that way, It's not how I roll. I don't flame or insult. It's not how I am. I didn't mean to insult you, it was a poor attempt at humor after working too long.



On your glitch, I'm thinking that it's probably a command or instruction in Corel that isn't working correctly with laser draw. Knowing how Laser draw is put together, it seems likely. I've had several unexplained weird things happen with it at a particular point (usually in a Draftsight drawing). My solution (a lot like yours) was to change the drawing features a bit  at a time until it stopped doing it. (sometimes locking up, crashing into rails, or just plain not starting).

I was using the Corel that came with the software "package"



Scott


---
**I Laser** *August 08, 2016 07:08*

I recently deleted my account from a forum I was a member of for 14 years, so I'm probably feeling a bit sensitive so my apologies.



It definitely seems an issue with corel, well corels output being gargled.



**+Phillip Conroy** 

I'm running Windows 7 x64 Home edition on both machines.



As a side. I updated my laptop to Windows 10 and corel asks to be registered everytime Windows starts! Decided to leave my laser machines on 7!



Oh the computer is a Core 2 Duo 8400 with 8GB ram and a GTS250 (that's about to die!).


---
**Eric Flynn** *August 08, 2016 07:19*

I have had the same issue, and it does seem to be related to white space.  My solution is to run each separate engraving feature as a separate task. Much easier than messing with white space etc. Only thing is, I have no idea what the task limit is, so if there were many, I'm not sure how it would pan out. I have only done 5 tasks at once so far.


---
**Eric Flynn** *August 08, 2016 07:22*

Also, I have seen something similar when you have it set to home after every task. Make sure "do not back" is selected so it doesn't return after a task.  The offsets get off for some reason.


---
**I Laser** *August 08, 2016 08:47*

Figured it couldn't just be me! My 'do not back' checkbox is selected.



I repeat the same designs a fair bit and up until now I was running each bit by itself because on certain designs it would glitch every time. Just adding a transparent box where it glitches seems to have done the trick and I can run the whole job at once again.



Fingers crossed it's a fix as I'm yet to test all my problematic designs.


---
**HalfNormal** *August 08, 2016 12:57*

**+I Laser** You need to set up a firewall policy that blocks Corel from phoning home. That will take care of the registration bugging you.


---
**Eric Flynn** *August 08, 2016 21:46*

**+I Laser**   I think I discovered the real issue here.  Make sure that both Cutting Data, and Engraving data are bot set to WMF in the Corel Setting dialog.  I can repeat the issue every time when cutting is set to PLT(plot).  When setting them both to WMF, the issue goes away, with no silly stuff required to make it work.   However, set your cut lines to .001mm , or you will get a double cut. Hairline still generates double cut.


---
**I Laser** *August 08, 2016 22:27*

Thanks Eric but mine has been set to WMF since not long after I bought the machine.I had major issues and couldn't work out why. I stumbled on a post that recommend WMF for both cutting and engraving, though it made no difference for me at the time. Turns out my issue was the board was set wrong.



I've wondered for me if it's a case of a file getting corrupted after numerous iterations. I use the same designs over and over, with slight changes (personalisations) and duplicate content a lot. 



The files originally worked fine, but over time have started to glitch in the white space. I tried copying and pasting the design out to a fresh file but that didn't work either. Seems transparent boxes between elements is the solution for me.


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/8kRqCb71fJf) &mdash; content and formatting may not be reliable*
