---
layout: post
title: "I was experimenting today with cutting painted mdf"
date: November 28, 2017 02:01
category: "Materials and settings"
author: "Anthony Bolgar"
---
I was experimenting today with cutting painted mdf. What I found out is that it takes much more power to cut through 6mm painted mdf than plain 6 mm mdf. My guess as to why would be the reflective nature of the hammered copper color pait I used. On plain mdf my 60W laser can cut through in 1 pass at 10mm/s  On the painted it took 2 passes at 10mm/s.  So the next piece I ran the first pass at 30mm/s which cut through the paint layer, then I ran the 2nd pass at 10mm/s and it cut through. All cuts were at 90% power.





**"Anthony Bolgar"**

---
---
**Claudio Prezzi** *November 28, 2017 10:18*

If your paint is only on one side, you could also try to turn the painted side downwards (to prohibit reflection).


---
**Anthony Bolgar** *November 28, 2017 14:11*

Thanks Claudio, I will try that today.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/QTUgrW9Zyn1) &mdash; content and formatting may not be reliable*
