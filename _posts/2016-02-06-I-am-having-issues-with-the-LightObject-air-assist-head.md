---
layout: post
title: "I am having issues with the LightObject air assist head"
date: February 06, 2016 00:30
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
I am having issues with the LightObject air assist head. I have all my mirrors perfectly aligned. Without the air assist nozzle, I get great cuts and engraving, but when I put on the air assist nozzle, the beam is not going straight down, most of it is hitt the inside of the air assist cone. How do I figure out :

a)What area of the cone is being hit?

b)How do I compensate for the misalignment?





**"Anthony Bolgar"**

---
---
**Jim Hatch** *February 06, 2016 00:44*

Is the mounting plate level (or on plane with the Y axis mirror rail)?


---
**Anthony Bolgar** *February 06, 2016 00:51*

Hmmmm.....did not check that. Will have a look and let you know.


---
**varun s** *February 06, 2016 04:27*

Check if the air is moving the lens inside the head! 


---
**I Laser** *February 06, 2016 07:26*

I noticed one of the air assists I bought the nozzle was screwed on off center. Maybe you could do something similar?


---
**Scott Thorne** *February 06, 2016 13:20*

The only way I could correct that on my k40 was to turn the head sideways to give the beam an angle going down.....the best thing to do is remove the lens and turn it a bit until you get a full circle burn with the nozzle on...if it's hitting the cone it will look like a half or quarter moon.


---
**Phil Willis** *February 13, 2016 19:52*

I have exactly the same issue. I was running without the nozzle until my new lens arrived. 



The lens is a big improvement over the original, I will re-fit the nozzle and see if the beam path has changed with the larger lens.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/MejmmCwHXd7) &mdash; content and formatting may not be reliable*
