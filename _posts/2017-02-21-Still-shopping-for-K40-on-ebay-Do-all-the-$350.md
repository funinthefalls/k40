---
layout: post
title: "Still shopping for K40 on ebay... Do all the $350"
date: February 21, 2017 17:23
category: "Discussion"
author: "Lance Ward"
---
Still shopping for K40 on ebay...  Do all the $350. systems come with the same software?  I noted that some specifically say that Corel Draw is included, some say MoshiDraw, some say LaserDraw.  Some don't mention software at all.  What is the real story here?  



I plan to upgrade to Cohesion3D soon but just trying to get the most bang for my buck here to get started.  Thanks.



FYI, I just got a notice from ebay that for the next 2 days, I'll get 10% ebay bucks back on any purchase.  I'm in!





**"Lance Ward"**

---
---
**Ned Hill** *February 21, 2017 17:35*

Mine came with both Laser Draw and Corel Draw.  I know some people get just one or the other or even Moshidraw.  Keep in mind the corel draw will be a very old pirated version.   It just depends.  If you are going to upgrade the controller you will be moving to another more capable software like laserweb (design in inkscape or AI or autodesk) anyway so it doesn't matter.  IMHO


---
**greg greene** *February 21, 2017 17:47*

You never know till you open the box !


---
**Stephane Buisson** *February 21, 2017 19:47*

2 controller boards

a) Moshi with Moshidraw

b) Nano with the other softwares.



But You will want quickly upgrade your software to the freedom side.


---
**Lance Ward** *February 21, 2017 20:10*

Thanks everyone.  I just pre-ordered a Cohesion3D conversion and am going to order this system from ebay:  [http://www.ebay.com/itm/131917873988](http://www.ebay.com/itm/131917873988). Am I making any obvious errors here?  Thanks again!



[ebay.com - Details about  High Precise 40W CO2 USB Laser Engraving Cutting Machine Engraver Wood Cutter](http://www.ebay.com/itm/131917873988)


---
**Cesar Tolentino** *February 21, 2017 22:12*

If you can get laser draw, get that one. Mohidraw is a waste of time.  Besides the was a new inkscape plugin that works wonderful for laser draw.  


---
**Lance Ward** *February 22, 2017 20:45*

Ordered the above unit.  Seller replied that it comes with LaserDraw.  With the 10% ebay bucks bonus, total cost was  $313.74.  Not too bad.  Should be here in a week.  Probably won't mess with it too much until the Cohesion3D stuff gets here.


---
**Cesar Tolentino** *February 22, 2017 20:48*

**+Lance Ward** lucky you have laser draw. Just install inkscape and the plugin for laserdraw.  And you are good to go.


---
**Lance Ward** *February 23, 2017 17:20*

Wow.  Ordered the engraver from Ebay yesterday and I just got an email from UPS telling me they are delivering it tomorrow. I better get on the stick and find a place for it. lol 


---
*Imported from [Google+](https://plus.google.com/114422684423642232545/posts/gGZsbhCjhTG) &mdash; content and formatting may not be reliable*
