---
layout: post
title: "Is it possible to upgrade this kind of laser to do gradients?"
date: August 23, 2015 02:36
category: "Hardware and Laser settings"
author: "Niclas Jansson"
---
Is it possible to upgrade this kind of laser to do gradients? Like if I want a photo engraved I can convert it to a million dots and get a pretty decent result, but is there any upgrade kits out there that will adjust the actual power of the laser in real time?





**"Niclas Jansson"**

---
---
**William Steele** *August 23, 2015 09:56*

The out of the box software, as bad as it is, already does it.


---
**James McMurray** *August 23, 2015 16:08*

William,  You mean the K40 with the laser power controlled by a potentiometer knob will do the gradients?


---
**William Steele** *August 23, 2015 16:44*

Yes... The laser pulse is actually fired by a PWM signal... The knob just adjusts the current for the laser when firing continuously.  (Basically the maximum current it will draw... But you can still pulse it for shorter durations to reduce the average laser power.)


---
**James McMurray** *August 23, 2015 16:57*

Okay,  senior moment there.   Is there any rule of thumb for setting that max draw current for engraving images or for more 3D style engraving?


---
**William Steele** *August 23, 2015 17:30*

Yes... I always set it to about 80% of the maximum current rating of the tube.  That way, if a 100% duty cycle is used on the engraver, then it will still be well below the rating of the tube which will prevent an early failure.  The laser will last significantly longer if you stay well below the rating.  I have glass tube lasers that have well over 1500 hours on them... And I have RECI tubes with over 5000 hours using this practice.


---
**Jim McPherson** *August 30, 2015 04:07*

I installed a smoothieboard into my K40 which allowed me to use programs like picengrave pro to do vector photo engraving on a K40. Your laser needs to have a PWM power supply. My recently purchased from eBay K40 had it from the factory.


---
*Imported from [Google+](https://plus.google.com/105892779133697678832/posts/fk69ctSuamH) &mdash; content and formatting may not be reliable*
