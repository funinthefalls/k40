---
layout: post
title: "Quick question, for those of you that are adding the air assist and spot lasers for alignment, where are you picking the 5V needed for the laser pointers?"
date: July 10, 2016 22:47
category: "Modification"
author: "Terry Taylor"
---
Quick question, for those of you that are adding the air assist and spot lasers for alignment, where are you picking the 5V needed for the laser pointers? I metered everything that I could find on the front of the power supply and did find a 5V source.





**"Terry Taylor"**

---
---
**Ariel Yahni (UniKpty)** *July 10, 2016 23:01*

Really? There is supposed to be a 5V coming from the psu to the stock board


---
**Jean-Baptiste Passant** *July 10, 2016 23:09*

That, and at least one for the potentiometer.


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2016 23:11*

Yep. Also 5v going to the nano board over the 4 pin cable. If you have the screw terminal psu your life is easy. :)


---
**greg greene** *July 10, 2016 23:19*

what do you use for pointers and how are they mounted?


---
**Ariel Yahni (UniKpty)** *July 10, 2016 23:23*

**+greg greene**​ you would use them to point where the laser would actually point. You need to make a mount or 3d print it


---
**Ned Hill** *July 10, 2016 23:32*

The blue cable in this 4 wire group going from the psu to the M2 Nano board is labeled 5V on mine [https://www.dropbox.com/s/1n2nswvspzbbwja/2016-07-10%2019.24.43.png?dl=0](https://www.dropbox.com/s/1n2nswvspzbbwja/2016-07-10%2019.24.43.png?dl=0)  Was planning to tap my spot laser to the psu terminals where the blue and green wire connect.


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2016 23:36*

I second that, assuming of course that the psu creates enough juice.  I don't use its 5v pin at all, I regulate my own logic level voltages from the raw 24v.


---
**greg greene** *July 10, 2016 23:44*

well then looks like I need a 3d printer


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2016 23:48*

Or just ask any of us to print it for you... or get it done on 3DHubs...  Many cheap options.


---
**greg greene** *July 10, 2016 23:50*

Well if some one can do that for me - plse let me know ! Although it is a good excuse to get a 3d printer :)


---
**Ray Kholodovsky (Cohesion3D)** *July 10, 2016 23:52*

Where are you located? And find an STL file on thingiverse that you want.  Send me a link.


---
**greg greene** *July 10, 2016 23:57*

I live in Creston BC Canada - but I get mail at Bonner's Ferry Idaho I'll look for that file



Thanks


---
**Ray Kholodovsky (Cohesion3D)** *July 11, 2016 00:00*

Yeah, shipping to Canada isn't going well right now. But anywhere in the U.S. is no problem. 


---
**Anthony Bolgar** *July 11, 2016 04:48*

If you ship them to my Niagara Falls, NY address, I can forward them on from Niagara Falls, Canada where I live. Probably cheaper to do that than to pay for International shipping. If interested, just send me a private message and we can work out the details.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/DT6EtfUYMqr) &mdash; content and formatting may not be reliable*
