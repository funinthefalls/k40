---
layout: post
title: "I'm not sure what I'm doing wrong here"
date: February 06, 2016 14:40
category: "Materials and settings"
author: "Ben Walker"
---
I'm not sure what I'm doing wrong here.   I select the part I want to engrave,  choose 'selected only' and it engraves perfectly.   Then I choose the offset stroke to cut,  go through all the procedures and it offsets by this much every single time.   

![images/2b5707739b259ce6053428facd218429.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b5707739b259ce6053428facd218429.jpeg)



**"Ben Walker"**

---
---
**Ben Walker** *February 06, 2016 15:23*

Ah hah.  I figured it out.   I needed to check the advanced parameters.  Method: Dialog.  Now I'm cutting like a champ. 


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/BsjL42kyPCe) &mdash; content and formatting may not be reliable*
