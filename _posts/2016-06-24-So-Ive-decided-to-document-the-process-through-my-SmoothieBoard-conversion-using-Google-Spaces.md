---
layout: post
title: "So I've decided to document the process through my SmoothieBoard conversion using Google Spaces"
date: June 24, 2016 18:20
category: "Smoothieboard Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I've decided to document the process through my SmoothieBoard conversion using Google Spaces. I have yet to start the process as I am awaiting my SparkFun Voltage-Level Translator Breakout - TXB0104. Hopefully it will arrive soon.



I am not 100% sure how Spaces works, but I think you can follow or join the space by following this link (which I think allows you to post/comment/etc into the space).





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Tony Sobczak** *June 24, 2016 18:53*

Will follow. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 18:58*

**+Tony Sobczak** You may want to click & join somehow. I see Peter & Rob joined somehow. That way you can keep track of the progress better.


---
**Steve Anken** *June 26, 2016 22:32*

I have a smoothie arriving Monday to put in a K40 so I'll be following and trying to get mine going next week.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 26, 2016 22:40*

**+Steve Anken** Join the Space & post any questions/images/etc in there too. That way we can try combine it all into one place/resource.


---
**Steve Anken** *June 26, 2016 23:04*

Will do.


---
**Don Kleinschnitz Jr.** *June 27, 2016 13:23*

Joined but not sure how this works and is different than our communities?

Will this end up being redundant with G+, wiki, blogs etc?

 I didn't even know spaces existed :)? 

You can follow my conversion at: 

[http://donsthings.blogspot.com/](http://donsthings.blogspot.com/)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 27, 2016 20:03*

**+Don Kleinschnitz** Spaces is a new google thing. Just recently been made available I believe. From what I read, the goal of spaces was to bridge the gap between Hangouts (small group chat) & G+ Communities (large groups) with something for medium sized groups.



The reason I am posting to Spaces is that if I ask a question regarding anything to do with the Smoothie conversion & someone else wants to post a picture as a reply, they can do so with ease.



Feel free to have a play around in my other "Test" Spaces space here to see how it works/what you can do: [https://goo.gl/spaces/L3YpPNTV9q6oKa9H7](https://goo.gl/spaces/L3YpPNTV9q6oKa9H7)



oh, also post your blog link into the Spaces space for the K40 stuff, that way others who may want to use it as a reference can find it too.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/YvEQeACnmCz) &mdash; content and formatting may not be reliable*
