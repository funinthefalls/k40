---
layout: post
title: "A coworker of my wife emailed her a picture of an ornament she wanted with the names"
date: January 04, 2017 15:13
category: "Object produced with laser"
author: "Jeff Johnson"
---
A coworker of my wife emailed her a picture of an ornament she wanted with the names. This is what I came up with.

![images/e106efc21ce2bb239784a4e5ddf6d3f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e106efc21ce2bb239784a4e5ddf6d3f5.jpeg)



**"Jeff Johnson"**

---
---
**David Cook** *January 07, 2017 02:47*

Awesome.  nice work




---
**Jeff Johnson** *January 07, 2017 02:56*

Thank you


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/AsxuUj6WZ6n) &mdash; content and formatting may not be reliable*
