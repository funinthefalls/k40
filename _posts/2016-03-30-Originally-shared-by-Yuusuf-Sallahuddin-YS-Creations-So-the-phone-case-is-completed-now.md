---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) So the phone case is completed now"
date: March 30, 2016 08:10
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



So the phone case is completed now. There are actually more issues with it than I originally thought. Aside from the right hand side of the interior (that holds phone) having issues with the button holes, there is also issues with the width of the phone compartment being slightly to thin. Another 1-2mm width would have been better I think, as this issue causes a bit of buckling on the leather & misaligns the camera/speaker/mic/etc holes slightly. Also, I've noticed that due to the overall width of the case, it is awkward to open the screen cover flap to the left (when I hold the phone in my left hand). So some rethinking of that aspect is in order.



So all in all, I am happy with it. Gives me something to protect my phone at least, but I will be modifying the design again & making sure to fix errors & rethink it all to increase strength of areas, minimise warping, maximise comfort & usability, etc. I'm thinking the entire case needs to be a double layer of kangaroo-hide, or thicker leather. Something in the range of 1.5-2mm thick would give it extra stiffness. Or it would need some kind of reinforcing (maybe a layer of stiff fabric as an interior liner, e.g. denim backed with interfacing).



![images/554991f92079cdeef75255fa5511bdfc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/554991f92079cdeef75255fa5511bdfc.jpeg)
![images/7a5db8cde54e2bbbe21c0f326f51f0f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a5db8cde54e2bbbe21c0f326f51f0f3.jpeg)
![images/c72b3501c8bcb65db04919df8187a3d6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c72b3501c8bcb65db04919df8187a3d6.jpeg)
![images/b45d15a286494d3c395a05a4e79f6839.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b45d15a286494d3c395a05a4e79f6839.jpeg)
![images/432f5e4b46b4f19a949debcd5019b212.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/432f5e4b46b4f19a949debcd5019b212.jpeg)
![images/33ed1b600958a498bc77bbaf49af32a4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/33ed1b600958a498bc77bbaf49af32a4.jpeg)
![images/20b92ab3f5fbde65fca0e9b24c95ebc2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/20b92ab3f5fbde65fca0e9b24c95ebc2.jpeg)
![images/ff5b30594be27f859a4a8b81fa0c2e44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ff5b30594be27f859a4a8b81fa0c2e44.jpeg)
![images/c84c0183c4258d65456e9085e19256a5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c84c0183c4258d65456e9085e19256a5.jpeg)
![images/7951f3886b3f9447a58a81f79aaacae5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7951f3886b3f9447a58a81f79aaacae5.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Anthony Bolgar** *March 30, 2016 08:25*

What about using clear acetate or something similar as a stiffening agent?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 11:42*

**+Anthony Bolgar** I'm not familiar with clear acetate, however a quick google search shows me it is some kind of clear plastic? I tend to avoid using plastic with leather, as leather is porous & needs an ability to dry else it will go mouldy. Normally I lacquer my products, but this one had issues already so I didn't bother. Usually the dye/lacquer process stiffens the leather (sometimes way too much).



But I will see about getting some clear acetate for some test purposes though. It may be suitable for stiffening if I provide it with some laser cut breathe holes (or dots) at regular intervals. Thanks for the suggestion.


---
**Dennis Fuente** *March 31, 2016 17:00*

nice work 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 01, 2016 13:00*

**+Allready Gone** Thanks. A new job will be going up soon to show my improvements on it.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/UyFAcroMXid) &mdash; content and formatting may not be reliable*
