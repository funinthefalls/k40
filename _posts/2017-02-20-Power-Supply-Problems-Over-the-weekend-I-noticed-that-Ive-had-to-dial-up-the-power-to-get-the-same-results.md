---
layout: post
title: "Power Supply Problems? Over the weekend I noticed that I've had to dial up the power to get the same results"
date: February 20, 2017 16:21
category: "Hardware and Laser settings"
author: "Jeff Johnson"
---
Power Supply Problems?

Over the weekend I noticed that I've had to dial up the power to get the same results. It doesn't appear that the laser is less powerful, just that I have to give it more power to do the same job. I've also noticed once that the power faded in and out and I had to adjust the dial back and forth a little before it became consistant. Maybe it's the dial?

When marking lines for detail on MDF I would normally do a very light cut at 3ma and 25mm/s. Now I am having to use 3.5ma. For cutting I have been using 12ma and 8mm/s. This gives me a clean cut where the parts easily fall free. Now I'm having to use 15ma. 

Any ideas?





**"Jeff Johnson"**

---
---
**Phillip Conroy** *February 20, 2017 16:54*

How oldis the ttube? 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 20, 2017 17:09*

I'm surprised to hear that you get any marking at 3ma. Mine seems to not want to do anything below 4ma.



Like Phillip mentions, tube age can degrade the performance. Over time (based on usage & also sometimes just residual loss) it can lose power.



In regards to the power fading in & out, does this happen at low ma range or even at higher ma? No real ideas there either way, but maybe someone else will have an idea.


---
**Jeff Johnson** *February 20, 2017 17:29*

**+Phillip Conroy** I've had the laser cutter for 6 months and the tube has hundreds of hours on it. I don't know how long it sat in a warehouse before I bought it.


---
**Alex Krause** *February 20, 2017 17:31*

Have you cleaned all the mirrors and the lens?


---
**Jeff Johnson** *February 20, 2017 17:36*

**+Alex Krause** Yes, I clean every day that I use it. I'm also using a mirror that only has about 20 hours on it. The last one dropped while cleaning and shattered.


---
**Alex Krause** *February 20, 2017 17:39*

Are you cleaning the lens at the exit of the laser tube... Not the first mirror but the very end of the tube 


---
**Jeff Johnson** *February 20, 2017 17:45*

I cleaned it in December after a really busy Christmas rush of orders and it had nothing on it. I visually inspect it and it still has nothing on it that I can see. I'm always worried that I'll ruin it if I touch it too much. I'll clean it when I get home just to make sure it isn't the issue.


---
**Jeff Johnson** *February 20, 2017 17:47*

By the way, I have really good ventilation after adding an inline exhaust booster and the insides stay pretty clean now.


---
**Alex Krause** *February 20, 2017 17:48*

Yeah I only clean mine when it gets really bad... Tubes are a consumable they gradually degrade over their life span 


---
**Maker Cut** *February 20, 2017 21:12*

By dial I am assuming that it is a rheostat (analog), and yes that could be part of the problem. You will need to see if you can get cleaner (electronic contact cleaner) into the rheostat to try to clean the contacts. If you have a multi-meter, you can check the resistance on the rheostat for stable changes in resistance, it should increase or decrease smoothly, not jump. If it jumps you will most likely need to replace the rheostat. If it is smooth, then you will need to look at your power supply and tube.


---
**Jeff Johnson** *February 20, 2017 21:13*

**+Maker Cut** Yes that's what I'm talking about. Thank you for the advice, I'll do that this evening.


---
**Jeff Johnson** *February 20, 2017 21:26*

**+Maker Cut** Do you happen to know the proper values I should expect from the rheostat? I've seen someone mention 2 - 1.1k ohm but that's all I've found.


---
**Maker Cut** *February 20, 2017 21:34*

**+Jeff Johnson** I do not. If I had the part number I could find out though. [digikey.com - DigiKey Electronics - Electronic Components Distributor](http://Digikey.com) should have the specs. if you can get the part number.


---
**Jeff Johnson** *February 20, 2017 21:38*

**+Maker Cut** Thanks, I'll see what I can find.


---
**Jeff Johnson** *February 21, 2017 01:27*

It's a pot and says 1k and it's close. It's about 960ohm and has a little inconsistentcy at the bottom end, starting low, going a little higher and then lower. It also stops and holds for maybe a degree or two of rotation right at about 3ma, which is where I first noticed some change. I guess this pot needs to be replaced. I'm going to replace it and see what happens.



The mirror on the tube is clean, by the way.


---
**Maker Cut** *February 21, 2017 01:34*

**+Jeff Johnson**​ that would be the culprit. Let us know if that doesn't do the trick.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/JnvUNV5H6Jf) &mdash; content and formatting may not be reliable*
