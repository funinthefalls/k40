---
layout: post
title: "The mirror came out fairly well"
date: August 26, 2016 19:51
category: "Object produced with laser"
author: "David Spencer"
---
The mirror came out fairly well

![images/9553d8ab99a935011659e56ea0ae48d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9553d8ab99a935011659e56ea0ae48d2.jpeg)



**"David Spencer"**

---
---
**greg greene** *August 26, 2016 20:28*

Engraved on the rear?


---
**David Spencer** *August 26, 2016 20:29*

Yes


---
**greg greene** *August 26, 2016 20:29*

Well Done !


---
**Jim Hatch** *August 26, 2016 23:47*

Did you respray the back after doing the engrave and removing the silvering?


---
**David Spencer** *August 26, 2016 23:52*

No, I didnt have a chance. My wife saw it and I did not see it again until she sent a picture of it hung in her office today.


---
**Jim Hatch** *August 26, 2016 23:58*

😀


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/JpLS9TveK2S) &mdash; content and formatting may not be reliable*
