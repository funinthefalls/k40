---
layout: post
title: "Here's a great gift for someone who has everything: I think it might look really nice if cut and engraved out of coloured acrylic!"
date: December 09, 2016 17:25
category: "Object produced with laser"
author: "Ulf Stahmer"
---
Here's a great gift for someone who has everything: [https://tinkerings.org/2016/11/05/buckminster-fullers-map-a-lasercut-dymaxion-globe/](https://tinkerings.org/2016/11/05/buckminster-fullers-map-a-lasercut-dymaxion-globe/)

I think it might look really nice if cut and engraved out of coloured acrylic!





**"Ulf Stahmer"**

---
---
**Stephane Buisson** *December 09, 2016 18:09*

That make me have a think to what happen at the present moment to some amazing world race sailors.

World tour solo without stop or any assistance. half way with an average speed over 20 knots.

[vendeeglobe.org - Home - Vendée Globe 2016-2017](http://www.vendeeglobe.org/en/)



In same time a french guy attempt to beat the world tour speed records (solo), he have a huge advance on it and only the Atlantic to go up to the finish line. his average on 31 days, a stunning 25knots.



[https://www.sodebo.com/fr/voile/actualites/article/31-jours-seul-au-bout-horn-nouveau-record](https://www.sodebo.com/fr/voile/actualites/article/31-jours-seul-au-bout-horn-nouveau-record)




---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 09, 2016 20:20*

That's pretty nifty. Would look cooler with recessed/flush brass screws & maybe even some nice brass edging along the angled edges. Or even some nice leather strips glued/tacked/riveted along the angled edges.


---
*Imported from [Google+](https://plus.google.com/116877257180815912154/posts/MUn5VeEPsiW) &mdash; content and formatting may not be reliable*
