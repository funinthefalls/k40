---
layout: post
title: "Originally shared by Ray Kholodovsky (Cohesion3D) First real cut with the new COR3D Mini board I put in my laser"
date: July 10, 2016 00:41
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



First real cut with the new COR3D Mini board I put in my laser. We're on rev2 now and it works like a champ. Finally got pwm control working too, with on board level shifter and everything! 

This is 1/4" MDF, pwm at 100% federate 300. 



![images/d0a02e24f9853937ce3fdecce9cf5d9e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d0a02e24f9853937ce3fdecce9cf5d9e.jpeg)
![images/2b73fa8539abe15fc980d1bda34d3d5a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b73fa8539abe15fc980d1bda34d3d5a.jpeg)

**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/59uuiN9eGjf) &mdash; content and formatting may not be reliable*
