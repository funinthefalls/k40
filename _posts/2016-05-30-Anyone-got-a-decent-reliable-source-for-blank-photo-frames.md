---
layout: post
title: "Anyone got a decent reliable source for blank photo frames?"
date: May 30, 2016 20:18
category: "Material suppliers"
author: "Jamie Martin"
---
Anyone got a decent reliable source for blank photo frames? Wooden ones - I see hundreds of sellers on ebay, all seem to use the same base frame for engraving or painting or customising in some way, so there must be a decent supplier, I just can't seem to find them?! Any help would be great





**"Jamie Martin"**

---
---
**Jeff Kwait** *May 30, 2016 22:06*

I have used this many times for our picture framing business 

[http://www.frameusa.com/wood-frames/decorate-it-picture-frames](http://www.frameusa.com/wood-frames/decorate-it-picture-frames)


---
**Jeff Kwait** *May 30, 2016 22:15*

They also have a wholesale division  if you need larger quantities


---
**Arjen Jonkhart** *October 21, 2017 03:50*

Check the dollar store - I found several, but supply is not consistent.


---
*Imported from [Google+](https://plus.google.com/111997191311797045379/posts/RfXm7RK96m1) &mdash; content and formatting may not be reliable*
