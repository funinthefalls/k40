---
layout: post
title: "Adjustable platform I made for my K40"
date: February 26, 2017 19:35
category: "Modification"
author: "Scorch Works"
---
Adjustable platform I made for my K40.  (Freshly cleaned for the video.)




{% include youtubePlayer.html id="7cl0OkYHS5I" %}
[https://www.youtube.com/watch?v=7cl0OkYHS5I](https://www.youtube.com/watch?v=7cl0OkYHS5I)





**"Scorch Works"**

---
---
**Ariel Yahni (UniKpty)** *February 26, 2017 19:39*

Nicely done


---
**Don Kleinschnitz Jr.** *February 26, 2017 21:10*

Awesome idea, any drawings to share?


---
**Scorch Works** *February 26, 2017 21:25*

I don't have any drawings but I put a parts list in a blog post.

[scorchworks.com - Spring Loaded K40 Laser Platform](http://www.scorchworks.com/Blog/spring-loaded-k40-laser-platform/)


---
**Scorch Works** *February 26, 2017 21:31*

**+Don Kleinschnitz** I was hoping the video and list on the blog would be enough detail for someone to recreate the design.  I can make some drawings if someone is committed to making one and needs drawings.


---
**Don Kleinschnitz Jr.** *February 26, 2017 21:36*

**+Scorch Works** your video is great and gives me the basics, I was just being lazy:). 

I am going to implement this idea but I do not want to remove my gantry. I also want to make it front to back adjustable and removable.

I'll do some drawings for that and post when that is implemented.



Thanks for sharing the idea.


---
**Scorch Works** *February 26, 2017 21:47*

**+Don Kleinschnitz** I didn't have a plan when I started so I drilled all of the holes individually.  If I were to do it again I would stack all three parts (the two pieces of angle and the flat bar) and match drill two small holes all the way through all three of the parts.  Then you don't need to worry about measuring where the holes are in each part.  You just need to enlarge the holes in each hole as needed to make it work.


---
**Cesar Tolentino** *February 26, 2017 21:51*

This is so crazy simple.... hahaha... thank you for a great idea... im going to copy this.... 




---
**Jim Hatch** *February 26, 2017 23:42*

Brilliant! 


---
**HalfNormal** *February 26, 2017 23:45*

I have to say that this is a wonderfully simple idea!

The only negative I can see is that with thicker material, it would be a little more difficult to re-adjust the focal distance. For everything else, this is just the greatest!


---
**Don Kleinschnitz Jr.** *February 27, 2017 00:19*

**+HalfNormal** I was thinking mostly engraving. For cutting acrylic you need to be 1/2T so this would not work easily for that unless there was another clever way to adjust it.


---
**Jim Hatch** *February 27, 2017 01:25*

**+Don Kleinschnitz**​ seems simple enough to put 1/4" slices of acrylic under the lips of the angle bracket. That would leave the surface of your half inch piece proud of the focal point - but the focus would then be 1/2 way thru the thickness of your acrylic.


---
**Don Kleinschnitz Jr.** *February 27, 2017 03:50*

**+Jim Hatch** yup!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 27, 2017 05:00*

That is a really cool idea :) Nice & simple solution too. Great work.


---
**Jorge Robles** *February 27, 2017 07:43*

Dead Drop Simple! Excellent!


---
**Don Kleinschnitz Jr.** *February 28, 2017 19:06*

**+Scorch Works** I through together a design with some additional features. Take a look and let me know what you think, what I missed and what will/won't work. 

[donsthings.blogspot.com - K40 Clamping Table](http://donsthings.blogspot.com/2017/02/k40-clamping-table.html)


---
**Scorch Works** *March 01, 2017 03:29*

**+Don Kleinschnitz** Thank you. You were very kind to me in your blog post.  Here are my guesses at potential issues. I might be wrong...



The span between the springs looks long.  You might need a piece of angle where I had the 1/8" flat bar.



I need to remove the top piece of angle to get the expanded metal out.  If you also need to do that it would mean adjusting two screws every time you remove the mesh.



 I look forward to seeing   a post of your build.


---
**Don Kleinschnitz Jr.** *March 01, 2017 04:13*

**+Scorch Works** in this design did you notice that the back clamp slides forward and back. Won't this eliminate removing the angle to get the expanded metal out?


---
**Scorch Works** *March 01, 2017 04:31*

**+Don Kleinschnitz** No, I did not notice that, but if a platform (i.e. expanded metal) is in place it might be difficult/impossible to reach the  locking knob.


---
**Don Kleinschnitz Jr.** *March 01, 2017 04:34*

**+Scorch Works** Yup doh..that't one I missed :).

That's why we review these things :)


---
**Jorge Robles** *March 01, 2017 07:12*

Just drill the box and make the shaft and knob go outside. 


---
**Don Kleinschnitz Jr.** *March 01, 2017 13:40*

**+Jorge Robles** Heh that is a good idea I will add that to the options. I was going to cut a slot in the front of my machine anyway.


---
**Bob Buechler** *March 23, 2017 19:37*

I'm not well equipped to do much metal working. The best I have is a dremel and a miter box saw fitted with a wood cutting blade, unfortunately. 



I wonder if anyone would be up to making me a kit from this design for a reasonable labor rate (plus materials of course)? 


---
**Don Kleinschnitz Jr.** *March 24, 2017 10:20*

**+Bob Buechler** my goal is to be able to do this with shop tools; drill, hacksaw, screwdriver etc. There is some simple machining of 4 screws (e-clips)  that we can get a batch made or provide drawings to your machine shop.

I am sure that when the design is tested and ready we can figure out a way to hook you up :). I have a lot of stuff backed up so I do not know when.



BTW here is newest design simplified. I am trying to work out a tension mechanism for the forward clamp: [plus.google.com - 1). Clamping table 2.0 design progress. Working on a tension mechanism and th...](https://plus.google.com/113684285877323403487/posts/4rZosYeYd14)


---
**Bob Buechler** *March 24, 2017 18:11*

COOL. :D In the meantime I yanked the stock plate w/ the stamp clamp in it, and replaced it with a simple expanded steel grate and some washers. Good news is, for my most common need, after measuring, it appears as though the bed is at the optimal cutting height, so it looks like I'll be able to actually get results from it as-is. 


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/UkrvPtra7UK) &mdash; content and formatting may not be reliable*
