---
layout: post
title: "Well I have been playing a lot of settlers recently, and am always the banker, I decided to make myself a card holder, what do you think"
date: March 23, 2016 09:30
category: "Object produced with laser"
author: "Tony Schelts"
---
Well I have been playing a  lot of settlers recently, and am always the banker, I decided to make myself a card holder, what do you think



![images/15ba2f2a7b31283ff088f5c2c1040a8a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15ba2f2a7b31283ff088f5c2c1040a8a.jpeg)
![images/96f7b08a49ab8848643541bc908f45c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96f7b08a49ab8848643541bc908f45c7.jpeg)
![images/414b221f47a6fee3007b4591bab4b81f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/414b221f47a6fee3007b4591bab4b81f.jpeg)

**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 23, 2016 10:32*

Don't really know the game myself, but anything that helps organise games & related pieces is a win in my book. Looks pretty decent.


---
**Joe Keneally** *March 23, 2016 13:20*

 Hey that looks really nice. What materials did you use?


---
**Tony Schelts** *March 23, 2016 13:46*

3mm birch ply


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/FZRR16wukrV) &mdash; content and formatting may not be reliable*
