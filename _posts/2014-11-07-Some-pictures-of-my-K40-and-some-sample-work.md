---
layout: post
title: "Some pictures of my K40 and some sample work"
date: November 07, 2014 22:53
category: "Discussion"
author: "Anton Fosselius"
---
Some pictures of my K40 and some sample work



![images/19bb7b7f9e8924ed64276963947f2bcb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19bb7b7f9e8924ed64276963947f2bcb.jpeg)
![images/1c9d6d337effa787d4a183fa564b5654.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1c9d6d337effa787d4a183fa564b5654.jpeg)
![images/94e09d204d7111b355426e2fd2eeb2c7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94e09d204d7111b355426e2fd2eeb2c7.jpeg)
![images/cdbc7644b0af51ede0b118ec81e636ac.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdbc7644b0af51ede0b118ec81e636ac.jpeg)
![images/629dc615392eadd6adeaa2a852bccaad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/629dc615392eadd6adeaa2a852bccaad.jpeg)
![images/551dcb075e63f17968ff067eb6af1ade.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/551dcb075e63f17968ff067eb6af1ade.jpeg)
![images/dc2b6bac42200d11e3d062d0ce816a47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc2b6bac42200d11e3d062d0ce816a47.jpeg)
![images/5dba06abf72b3905c3bf395794477757.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5dba06abf72b3905c3bf395794477757.jpeg)
![images/bd8caf8c67a737f09d2a85daf3d9e222.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bd8caf8c67a737f09d2a85daf3d9e222.jpeg)
![images/5cf805291ab8a444a748959c36d5d302.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5cf805291ab8a444a748959c36d5d302.jpeg)
![images/44c2babab06165a45cafc28b52c27b73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/44c2babab06165a45cafc28b52c27b73.jpeg)
![images/fdbcb380fb5ce8a38d1854e035eeaec8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fdbcb380fb5ce8a38d1854e035eeaec8.jpeg)
![images/15a029b73d375d2b1378519d86a0f6cd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/15a029b73d375d2b1378519d86a0f6cd.jpeg)
![images/8ee5923dedac9de2c02da6e4f065942f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ee5923dedac9de2c02da6e4f065942f.jpeg)
![images/9c71f7fbf2c67936677c0bb828f334f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c71f7fbf2c67936677c0bb828f334f4.jpeg)

**"Anton Fosselius"**

---
---
**Stephane Buisson** *November 09, 2014 18:20*

Welcome Anton !

Nice thank you for sharing those photo, that help me with the tube fitting.


---
**Anton Fosselius** *November 09, 2014 18:21*

I had that in thought when i shared it. My laser came fully assembled


---
*Imported from [Google+](https://plus.google.com/115118068134567436838/posts/DYZSWUi6RDF) &mdash; content and formatting may not be reliable*
