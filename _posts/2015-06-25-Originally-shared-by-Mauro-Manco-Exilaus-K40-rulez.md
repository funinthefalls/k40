---
layout: post
title: "Originally shared by Mauro Manco (Exilaus) K40 rulez"
date: June 25, 2015 21:32
category: "Materials and settings"
author: "Mauro Manco (Exilaus)"
---
<b>Originally shared by Mauro Manco (Exilaus)</b>



K40 rulez

![images/7a6a1435cda70a881d2af051119db38b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a6a1435cda70a881d2af051119db38b.jpeg)



**"Mauro Manco (Exilaus)"**

---
---
**Imko Beckhoven van** *June 28, 2015 18:49*

Try engraving ice ;-) 


---
**Mauro Manco (Exilaus)** *June 28, 2015 18:54*

Imko. ..not ice but soon engrave acrylic mirror....same effect of ice :)


---
**Imko Beckhoven van** *June 28, 2015 19:03*

a flat stone works great as well! I did a lot of engraving on mirror's make sure you engrave the backside so the laser removes the reflection (ooh and mirror youre image) 


---
*Imported from [Google+](https://plus.google.com/114285476102201750422/posts/GqRrqhZzGQS) &mdash; content and formatting may not be reliable*
