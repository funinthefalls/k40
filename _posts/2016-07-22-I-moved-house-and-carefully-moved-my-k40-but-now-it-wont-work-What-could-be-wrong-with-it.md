---
layout: post
title: "I moved house and carefully moved my k40 but now it won't work What could be wrong with it"
date: July 22, 2016 20:23
category: "Discussion"
author: "beny fits"
---
I moved house and carefully moved my k40 but now it won't work 

What could be wrong with it 





**"beny fits"**

---
---
**greg greene** *July 22, 2016 20:32*

Check all connections - especially the power cord as some were not soldered - only twisted together.


---
**Custom Creations** *July 22, 2016 20:55*

Did you unplug it from the old house and plug it in new house? :D


---
**greg greene** *July 22, 2016 20:57*

If you scan the pics in the forum you will see all sorts of 'inventive' ways to connect wires the manufacturer used - the only thing missing was bubble gum (possibly)


---
**beny fits** *July 23, 2016 04:21*

Yeah im getting power to the unit just the lazer is not working im thinking its the power unit 


---
**greg greene** *July 23, 2016 13:05*

Ok. power into the power cord

What then - does it reach the power in terminals on the power supply unit?

If so then do the PSU outputs have any life.  If the power actually reaches the PSU and you get nothing out - it is the PSU - Light Objects sells replacement units.


---
**beny fits** *July 23, 2016 14:09*

**+greg greene** could you send me the link please 


---
**greg greene** *July 23, 2016 14:10*

[http://www.lightobject.com/](http://www.lightobject.com/)


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/g6uCQh9FbaE) &mdash; content and formatting may not be reliable*
