---
layout: post
title: "I am receiving an error message when I attempt to send my bmp from laserworks software to the k40"
date: March 04, 2016 18:50
category: "Hardware and Laser settings"
author: "Glennis Merrifield"
---
 I am receiving an error message when I attempt to send my bmp from  laserworks software to the k40.  Says no communication.  I have changed data transfer cable hoping that would fix the problem but to no avail.  There is a red light flashing on one of the  pins on the motherboard that the usb cable attaches to and since I've not looked inside before I don't know whether that is an indication that my motherboard is at fault or not.  I can still run the programs on all existing files no prob but can't get a new one on the machine.  Any thoughts on what to check or how to fix.  I don't see anything in the software which addresses communication to the machine itself





**"Glennis Merrifield"**

---


---
*Imported from [Google+](https://plus.google.com/117667985561710527106/posts/WqyQCyCgFav) &mdash; content and formatting may not be reliable*
