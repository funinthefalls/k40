---
layout: post
title: "Very nice piece of scrollwork I found on the internet...I did this on a scrap piece..but it turned out great"
date: March 30, 2016 21:52
category: "Object produced with laser"
author: "Scott Thorne"
---
Very nice piece of scrollwork I found on the internet...I did this on a scrap piece..but it turned out great.

![images/984ff75f1b7f436c52bdf51f313c936c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/984ff75f1b7f436c52bdf51f313c936c.jpeg)



**"Scott Thorne"**

---
---
**ED Carty** *March 30, 2016 22:02*

sweet


---
**Jean-Baptiste Passant** *March 30, 2016 22:18*

Wow **+Scott Thorne**, you keep amazing me with your laser work. Really good job you did here ! Keep it coming, I love it !


---
**Scott Thorne** *March 30, 2016 22:38*

**+Jean-Baptiste Passant**....thanks brother.....I'm still in the learning phase! 


---
**Scott Thorne** *March 30, 2016 22:39*

**+ED Carty**...thanks man...it's always a pleasure to get a compliment from you my friend. 


---
**ED Carty** *March 31, 2016 01:00*

Thank you. You do you great work. It is very inspiring to see the great work you are putting out. I truly appreciate your willingness to share your experiences. 


---
**Phillip Conroy** *March 31, 2016 08:22*

Nice ,bit burned on the very fine stuff,what is the other side like ,i have found that anything less than 1mm burns on the front but is fine on the back,check focal lens is clean,as with my set up  have to clean every 1hours worth of cutting time,you need to shine a light at the right angle to see if it is clean,i will start a new post on focal lens and cleaning when i figure how to get photos off my phone


---
**Scott Thorne** *March 31, 2016 09:21*

**+Phillip Conroy**...it was a piece of scrap that had a few engravings on it....that's why it looks burnt in a few places.


---
**Scott Thorne** *March 31, 2016 11:44*

**+Phillip Conroy**...doesn't help much that this 1/8 plywood had a major bow in the middle....that's why I used it for the practice run.


---
**Dennis Fuente** *March 31, 2016 17:01*

looks good to me a little sanding and it's good to go


---
**Scott Thorne** *April 01, 2016 22:23*

**+Allready Gone**...thanks bro. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/35DYXouGBhG) &mdash; content and formatting may not be reliable*
