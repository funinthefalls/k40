---
layout: post
title: "Hi there - new to the group"
date: May 07, 2016 00:43
category: "Hardware and Laser settings"
author: "Ryan Matthew"
---
Hi there - new to the group. I'm glad I stumbled upon this. I've been happily running my K40 for 6-months or so with near daily use and I've just had the X-axis belt break. 



This could be a totally stupid question - but the guys over at Lightobjects.com sell a 5ft length of the belt - - - but how in the world do I create a Loop out of it? 



I'm relatively handy - and have kept this thing running this long - - but all the belts I've replaced in my life are pre-sized. 



Hopefully its a 'duh' moment for me - but I wanted to know if any of you have figured this out. 



Thanks!





**"Ryan Matthew"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 00:57*

You don't need to create a loop. If you pull out your rails setup (4 bolts if I recall correct) & take a close look, you will see that the loop is created by a screw that attaches both ends of the belt to the laser head carriage.



edit: Welcome to the group by the way.



edit2: Or you could maybe use a mirror to see under the laser head carriage to find what I'm talking about.


---
**Ryan Matthew** *May 07, 2016 04:10*

Yuusuf 



Thank you so much for the reply and for the welcome. When I got home from work I took apart the assembly and you are absolutely correct. The belt is held together by a screw and clamp plate under the assembly. New belt has been ordered so hopefully we will be back up and running soon. 



Thanks again



Ryan


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 07:02*

**+Ryan Matthew** Awesome. Hope it all goes well for you.


---
*Imported from [Google+](https://plus.google.com/111128653352445186567/posts/XVcCr4iwBMD) &mdash; content and formatting may not be reliable*
