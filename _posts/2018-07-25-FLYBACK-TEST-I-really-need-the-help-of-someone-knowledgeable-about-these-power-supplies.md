---
layout: post
title: "FLYBACK TEST: I really need the help of someone knowledgeable about these power supplies"
date: July 25, 2018 10:53
category: "Original software and hardware issues"
author: "Jamie Richards"
---
FLYBACK TEST: I really need the help of someone knowledgeable about these power supplies.  Stock K40 supply with flyback mounted to board.  This is a 100 to 1 high voltage interface I made awhile back for testing microwave transformers and I cautiously decided to connect it to my new tube. For every 10 volts on this scale, the actual should be 1000.  



RESULTS: Sitting idle without pushing the test button, it is reading 2,000 volts.  At 5ma, it shows around 10,000 volts, at 10ma, it was only doing 15,000 volts and finally at 15ma, it dropped to about 13,000 volts.  Experts?  There was a downward spiral the more I increased the current.  Is this normal behavior for these?  At 15mA, it would seem to be only driving the tube at 19 watts?  What's interesting is that just past the 4ma marker when it quits fizzing, the voltage jumps up and I was able to back it down close to 3mA to achieve 20,000 volts.  Since I know very little about these supplies and how they behave - HELP!!!!





**"Jamie Richards"**

---
---
**Don Kleinschnitz Jr.** *July 25, 2018 11:56*

<b>Warning: These power supplies output LETHAL voltages & current. Do not attempt to troubleshoot them unless you are skilled in the repair of Very High Voltage Circuits</b>



We would need to know more about the test setup that you speak of. How you are driving the HVT is important. Was there a photo intended to be part of the post?



Note that the transformer is a high voltage transformer and not a flyback. It is driven by an H switch. It has a voltage doubler potted into the transformer on the output side.

These HVT's typically develop shorted winding's or the High Voltage diode fails. For this reason it is pretty hard to test them. Usually if they are suspect they are replaced as they are not that expensive.




---
**Phillip Conroy** *July 25, 2018 12:10*

Spare laser power supplies are cheap and having a spare makes fault finding so much easier. 


---
**Jamie Richards** *July 25, 2018 13:20*

I'll add more data as I do further testing, but my interface is connected directly to the tube and I'm driving it just as you would set it for cutting, potentiometer and test button.  I have photos, but it just shows voltages on the meter, nothing spectacular. I may be able to position things to show the current and volts, I'm just keeping the meter a few feet away in case of flashover.  I'm probably going to make a graph from 0-20mA.  Been working with high voltage since the 80's (showing my age) when I started TV and VCR repair.  I used to have a probe, but lost it in a house fire and simply made my own interface for occasional use.   I have a 50 watt LPS coming, so I'm curious to see what results it gives.  



Thanks for the explanation on the transformer, that's an interesting design.  These are new to me.  Are there schematics for these?  I'd love to take a look.  Will do a search.


---
**Don Kleinschnitz Jr.** *July 25, 2018 14:32*

You can find information on these supplies on my blog:

[donsthings.blogspot.com - Don's Things](https://donsthings.blogspot.com/)



One more caution: these supplies are substantially more dangerous than TV HV power. 


---
**Jamie Richards** *July 25, 2018 14:57*

Thanks Don, you're awesome! :)


---
**Paul de Groot** *July 26, 2018 09:00*

Also the stock power supply characteristics are not linear. So when it draws more current it is expected that it will drop voltage beyond its optimum working spot. 


---
**Jamie Richards** *July 26, 2018 10:11*

13,000v at 15mA seems like a big drop.


---
**Paul de Groot** *July 26, 2018 10:21*

**+Jamie Richards** you can download the schematics at awesome.tech/downloads

[awesome.tech - (no title)](http://awesome.tech)


---
**Don Kleinschnitz Jr.** *July 26, 2018 10:28*

**+Paul de Groot** 

Did you know that a newer version is available here? This is a cumulative version of the work you, I and Nate Cain did.

Probably should move this version to Eagle some day.



[digikey.com - K40 LPS-2](https://www.digikey.com/schemeit/project/k40-lps-2-EFKO7C8303M0/)




---
**Don Kleinschnitz Jr.** *July 26, 2018 10:38*

**+Paul de Groot** **+Jamie Richards**

This discussion is complex since the tube is a gas discharge device. Therefore the tube as a load has the negative resistance characteristics of a gas discharge device.

To make sense of this test we would also need to see the circuit that is driving the HVT and in turn the tube including its drive voltage, frequency and feedback control schema.

[donsthings.blogspot.com - Laser Response Charcteristics](https://donsthings.blogspot.com/2017/03/laser-response-charcteristics.html)


---
**Jamie Richards** *July 26, 2018 11:03*

Thank you both for this.  I'll have to wait and see how the new power supply performs..  From what I've been reading, the JG-50 is pretty much the same thing as the 40.   New one has a fan, the old one has round perforations on top, no fan.

![images/d03438646f6c1b1dd14d41d657502201.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d03438646f6c1b1dd14d41d657502201.jpeg)


---
**Paul de Groot** *July 27, 2018 00:38*

**+Don Kleinschnitz** Thanks Don, I will add the link to my site. Well done.


---
**Paul de Groot** *July 27, 2018 00:40*

**+Don Kleinschnitz** also the HT circuit in the K40 stock PSU is very simplistic. Just a H bridge made up of 4 transistors without any snubber diodes. I managed to blow it up (don't know why and how but it did) and replaced the H bridge transistors to fix it.


---
**Jamie Richards** *July 27, 2018 01:10*

Has anyone thought of testing individual components to see if they used counterfeit or substandard parts?  I know there were some fake mosfets and other semiconductors floating around that didn't perform anywhere near what they should.  Testing has to be compared to a genuine and stress tests performed.  There were people making linear amplifies who ran into the mosfet problem.  If it seems too cheap to be true, it probably performs cheap. ;)


---
**Don Kleinschnitz Jr.** *July 27, 2018 03:02*

**+Paul de Groot** 

Yes I knew that you added snubbers to your unit. Interestingly although I have diagnosed/repaired many of these I have never seen the Hswitch blown. Always its the HVT, sometimes the bridge rectifier and once the LV flyback regulator.


---
**Don Kleinschnitz Jr.** *July 27, 2018 03:12*

**+Jamie Richards** 

That would be an immense amount of work and what would you do if you had that information :). Would you replace the fake parts? My schematic lists the correct parts so if there is a failure then you can get a good replacement part.



I have diagnosed/repaired many of these and the overwhelming failure is the HVT. Although our dissections could never prove it I would bet the culprit is the HV diode in the voltage doubler (no way to test). @ 20,000 v it does not take much to fry a diode. 



Additionally these supplies are very hard to troubleshoot. Even when the HVT is removed the HVT drive is floating @ >600vdc so its easy to hurt yourself if not fry expensive test equipment.



I considered a redesign with better parts and regulation but there is no way one could build these for the ebay/amazon price. You can get better supplies for 2x the price.



After lots of hrs investigating I decided that the best thing we could do for the community is to well document the design, create troubleshooting and repair procedures and better characterize the power control transform (on my list of to do's).


---
**Jamie Richards** *July 27, 2018 10:53*

Your advice is solid and I will listen. My curiosity drive is just so darn high. 



I'm so accustomed to correcting deficiencies in other devices, especially RF, being a HAM operator, but I've put that hobby on hold for awhile since the house fire.  No $$ for equipment.


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/8PvmTxRBCs9) &mdash; content and formatting may not be reliable*
