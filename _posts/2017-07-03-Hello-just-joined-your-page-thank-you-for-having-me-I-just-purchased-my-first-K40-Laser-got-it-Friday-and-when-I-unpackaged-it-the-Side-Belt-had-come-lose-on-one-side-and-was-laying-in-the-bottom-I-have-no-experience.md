---
layout: post
title: "Hello, just joined your page, thank you for having me, I just purchased my first K40 Laser, got it Friday and when I unpackaged it, the Side Belt, had come lose on one side and was laying in the bottom, I have no experience"
date: July 03, 2017 05:10
category: "Discussion"
author: "Lawrence Kelley"
---
Hello, just joined your page, thank you for having me,  I just purchased my first K40 Laser, got it Friday and when I unpackaged it, the Side Belt, had come lose on one side and was laying in the bottom, I have no experience or knowledge about this machine, is this a hard belt to repair ??  if needed replace ??  for someone with no knowledge of this and it's operation ??  I am a small Mom and Pop Woodshop, who wanted to try something new and hopefully expand my small business in a new direction, I pray this wasn't a mistake, any suggestions or advise is greatly appreciated.





**"Lawrence Kelley"**

---
---
**Nathan Thomas** *July 03, 2017 15:31*

Can you post a couple pics of it so we can see?


---
**Steve Clark** *July 03, 2017 16:07*

From what you described I'm thinking the head mounting screw.  The belts are not a loop but a section and the sections are held together with a screw that pulls the head around or bar being the sides...Y axis belt... there are two on that axis.



 Should be a fairly easy fix but may take some effort to get to. As Nathan said, pictures would help us to see for sure.


---
**Lawrence Kelley** *July 03, 2017 21:47*

I believe your right, because the end that is loose, looks like it may have been screwed down, has a Horseshoe type end on it, being from the side, that moves the machine forward and backwards, I just need to figure out how to get to it  :-)



![images/32952f5595be2270333092e6dd0fa087.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/32952f5595be2270333092e6dd0fa087.jpeg)


---
**Lawrence Kelley** *July 03, 2017 21:47*

![images/8df55584547b6a9551f773def7120e12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8df55584547b6a9551f773def7120e12.jpeg)


---
**Lawrence Kelley** *July 03, 2017 21:49*

![images/842e665ed3ab08f23e8ca983a3615365.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/842e665ed3ab08f23e8ca983a3615365.jpeg)


---
**Lawrence Kelley** *July 03, 2017 21:50*

**+Nathan Thomas**   here are 3 different photos of my belt and where it seems broken ??  or come loose




---
**Steve Clark** *July 04, 2017 00:38*

If the remaining belt section is now not long enough because it has torn loose the simplest thing would be to order this from Laserobject



[lightobject.com - Y axis belt for K40](http://www.lightobject.com/Y-axis-belt-for-K40-P760.aspx)






---
**Lawrence Kelley** *July 04, 2017 04:37*

Thank you so very much, very New to all of this, so when I try to repair, I will make you a Video for Dummies  :-) Y axis belt for K40 repair  :-)    wish me Luck




---
**Steve Clark** *July 04, 2017 16:09*

Your welcome, lots of help here.

 

Note:

 

On your machine there are four holes, two in the front and two in the back of the case. I have not had to do it yet but I believe they are there to access the belts and adjust the tension on them. 



I would think you have to move or loosen them in a manner (before belt replacement) that allows you to adjust that tension afterwards. 



Maybe someone who has done so will speak up. 


---
**Lawrence Kelley** *July 04, 2017 23:40*

Thank you so much  :-)  I decided tomorrow I would get into it, wish me Luck  :-)  will let you know how it comes out




---
**Steve Clark** *July 10, 2017 19:40*

Great!


---
**Lawrence Kelley** *July 11, 2017 00:36*

thank you for the advise, I will let you know how I do, every time I am about to get started, something more urgent comes up  :-)  eventually I will get some me time and get this task started and hopefully successfully completed  :-)




---
*Imported from [Google+](https://plus.google.com/108241240546580115383/posts/67mCnEiYMtc) &mdash; content and formatting may not be reliable*
