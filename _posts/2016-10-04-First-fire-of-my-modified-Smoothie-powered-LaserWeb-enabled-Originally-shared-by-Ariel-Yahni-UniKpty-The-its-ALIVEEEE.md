---
layout: post
title: "First fire of my modified Smoothie powered, LaserWeb enabled Originally shared by Ariel Yahni (UniKpty) The its ALIVEEEE!!!!!!"
date: October 04, 2016 01:45
category: "Modification"
author: "Ariel Yahni (UniKpty)"
---
First fire of  my modified #UK40OB Smoothie powered, LaserWeb enabled



<b>Originally shared by Ariel Yahni (UniKpty)</b>



The #UK40OB its ALIVEEEE!!!!!!




{% include youtubePlayer.html id="r9lixZon0og" %}
[https://youtu.be/r9lixZon0og](https://youtu.be/r9lixZon0og)







**"Ariel Yahni (UniKpty)"**

---
---
**Don Kleinschnitz Jr.** *October 04, 2016 01:59*

Very impressive.....


---
**Alex Krause** *October 04, 2016 02:01*

Holy Smokes Batman that's smooth!!!!


---
**Don Kleinschnitz Jr.** *October 04, 2016 02:02*

**+Alex Krause** its a "SMOOTHIE" ?


---
**Joe Alexander** *October 04, 2016 03:39*

wouldn't that make it a #UK40SOB? sunova...heh Very impressive and clean cabinet.


---
**J.R. Sharp** *October 04, 2016 04:02*

Wow. What are the upgrades?




---
**Alex Krause** *October 04, 2016 04:02*

**+J.R. Sharp**​ magic :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 04, 2016 06:46*

Very nice. Looks like that goes on the list of "what to spend my $ on".


---
**Randy Randleson** *October 04, 2016 12:14*

Great job Ariel!!!! I agree with Yuusuf, lol. 




---
**Ric Miller** *October 04, 2016 20:05*

That is a huge work area. Cool!


---
**Timo Birnschein** *October 05, 2016 08:25*

Great job!


---
**Ashley M. Kirchner [Norym]** *October 05, 2016 22:22*

This makes me want to get a second K40 just to do stuff like this to it. I can't do that to the one I am currently using because, well, it's a (semi) production machine. I can't have it dead while playing. :)


---
**Ariel Yahni (UniKpty)** *October 05, 2016 22:28*

**+Ashley M. Kirchner**​ I know what you mean. I had everything planned and then changed almost everything 


---
**Ashley M. Kirchner [Norym]** *October 05, 2016 22:32*

Such is the nature of DIY projects though. Rarely do I end up with something that's exactly as planned. But I've been wanting a larger cutting surface so this may warrant a second cheapo K40 just for fun.


---
**Ashley M. Kirchner [Norym]** *October 05, 2016 22:34*

I wonder, in your build, you attach the side rails directly to the chassis. Considering how thin that metal is, are you concerned with it twisting out of true level if you move the machine?


---
**Ariel Yahni (UniKpty)** *October 05, 2016 22:41*

That can be an issue but Is a gamble between that and the bottom of the machine that was actually allready out of level and wobbly. It was a judgment call


---
**Ashley M. Kirchner [Norym]** *October 05, 2016 22:57*

True, but I think if you had build the gantry on a solid plate and laid that in the machine, regardless of the bottom of the machine warping, that plate would always be flat and the gantry would always be level in comparison to that plate. If the plate is at an angle, so would the gantry sides and the laser distance will always remain the same regardless. As long as it's lined up with the rear mirror, you should be okay ... Then again, you can always just build your own enclosure and chuck the cheapie Chinese one. :)


---
**Russ “Rsty3914” None** *August 30, 2017 17:32*

Where did you get the rails ?  That is exactly what I want to do with mine now.  Want to engrave Rifle stocks...




---
**Ariel Yahni (UniKpty)** *August 30, 2017 17:35*

**+Russ None**​ look here

[plus.google.com - Guys, for those interested i have started a Build for the #UK40OB <https://p...](https://plus.google.com/+ArielYahni/posts/PvR3pMWMVwe)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/5XZzvRcm87D) &mdash; content and formatting may not be reliable*
