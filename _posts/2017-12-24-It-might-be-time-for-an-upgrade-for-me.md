---
layout: post
title: "It might be time for an upgrade for me"
date: December 24, 2017 13:14
category: "Modification"
author: "Bob Damato"
---
It might be time for an upgrade for me. I blame all of you guys because I see what great stuff you make with your modded systems :)  I currently have  bone stock K40 using corel laser. nuff said.  



But now the question is, where do I go from here?  I want a bigger system so ill get a new one and leave this one alone so I can keep using it.  Im thinking a 50-60W system. whats the most flexible mod for it that will allow me to do better photo engraving etc? But also fairly easy to use being Im not that bright :)   



Thank you for any advice!





**"Bob Damato"**

---
---
**Chris Hurley** *December 24, 2017 19:03*

Bone stock K40....well air assist is where I would start. It's the best ratio of money in vs effect. It's supper easy to install, cuts cutting time, and lowers maintenance effort. The best upgrade tho costs nothing. That's allinement. Getting it zeroed is tough as balls and can change due to different reasons (cleaning and wear on the machine being normal ones). 


---
**Chris Hurley** *December 24, 2017 19:04*

Oh and venting .....get that stock fan replaced with something which you think is over kill. The stuff your cutting is not nice chemicals to your lungs. 


---
**Bob Damato** *December 24, 2017 21:26*

Hi Chris, thanks for the input. I did do a full alignment when I got it, and youre right, tough as balls! But when you get it, magic happens. So I do have that!  


---
**Tech Bravo (Tech BravoTN)** *December 25, 2017 10:25*

most of the 50 watt + lasers i have seen already have air assist and dsp controller. if you want a free better solution for the k40 (wont get you pwm control of the laser but...) check out K40 whisperer. way better software than laserdraw. if you want a cost effective solution to get a dsp type controller for the k40 (with pwm laser control) check out the cohesion3d mini k40 upgrade kit! 1/3rd the cost of a cheap dsp and laserweb (and soon LIGHTBURN) work with it.




---
**Tech Bravo (Tech BravoTN)** *December 25, 2017 10:36*

and check out my cheat sheet: [techbravo.net - Beginners Guide To The K40 Laser Engraver - Knowledgebase - Bravo Technologies](https://techbravo.net/hosting/knowledgebase/4189/Beginners-Guide-To-The-K40-Laser-Engraver.html)


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/fqGtuBtPZ18) &mdash; content and formatting may not be reliable*
