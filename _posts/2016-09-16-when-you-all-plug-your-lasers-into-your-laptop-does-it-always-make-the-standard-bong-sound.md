---
layout: post
title: "when you all plug your lasers into your laptop , does it always make the standard \"bong\" sound ?"
date: September 16, 2016 11:59
category: "Discussion"
author: "Martin McCarthy (NGB Discos)"
---
when you all plug your lasers into your laptop , does it always make the standard "bong" sound ? 





**"Martin McCarthy (NGB Discos)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 12:22*

I don't hear any sounds, although I have Windows sound theme disabled.


---
**Randy Randleson** *September 16, 2016 12:55*

It should make the bong, if not try going on under System | Device Manager  and see if anything has a caution sign or and exclamation mark




---
**Martin McCarthy (NGB Discos)** *September 16, 2016 12:56*

yeah have done that, nothing appears


---
**Randy Randleson** *September 16, 2016 12:57*

I saw that you tried a different cable, did you try a different USB port?


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 12:58*

yep, tried multiple ports on different laptops, no sounds or any signs that its been detected


---
**Randy Randleson** *September 16, 2016 12:59*

and you have replaced the motherboard too I think you said in another post... Thats REALLY strange


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 13:01*

yep new motherboard as well


---
**Randy Randleson** *September 16, 2016 14:58*

I wonder if there could possibly be an issue with the power supply?


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 15:00*

Well that's the only thing left really although the laser fires OK,  although the psu makes a hell of a racket, a lot of vibration,  don't know if that last normal 


---
*Imported from [Google+](https://plus.google.com/108373254461031274579/posts/UEPEn7wDRdt) &mdash; content and formatting may not be reliable*
