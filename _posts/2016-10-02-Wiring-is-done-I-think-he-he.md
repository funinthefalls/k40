---
layout: post
title: "Wiring is done.... I think he he..."
date: October 02, 2016 02:50
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Wiring is done.... I think he he...



Tomorrow I power up and do a static test.



Ok. I am getting close to running this K40-S beast!



Configuration: Is their a collection of configuration files somewhere?



Anyone using this config specifically for K40 Smoothie:

-RepRapDiscount GLCD, 

-Optical  end stops

- "L" PWM control?



Firmware: I assume that I have to update the Smoothie firmware (been watching that discussion).



How would you recommend initial testing. Want to be cautious until I confirm motors are wired right.

-Go right to LaserWeb (can I do simple moves from there)?

-Ponterface as the guide suggests?









**"Don Kleinschnitz Jr."**

---
---
**Ariel Yahni (UniKpty)** *October 02, 2016 03:26*

You can go directly to LW if you want. Jog there to test your wiring, also dry tub if you like


---
**Don Kleinschnitz Jr.** *October 02, 2016 03:50*

Thanks guys very helpful.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/KvrXcH87cHE) &mdash; content and formatting may not be reliable*
