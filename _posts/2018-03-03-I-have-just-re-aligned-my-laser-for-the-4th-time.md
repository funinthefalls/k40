---
layout: post
title: "I have just re-aligned my laser for the 4th time"
date: March 03, 2018 04:55
category: "Hardware and Laser settings"
author: "Seon Rozenblum"
---
I have just re-aligned my laser for the 4th time. I had altered my air-assist and possible knocked something so start again, and now after doing multi-hour long re-alighments of all mirrors and head in my K40, I have 100% concluded that my new 18mm F50.8 lens I got from light object is NO WAY an F50.8 length. My best cuts are when my black 2mm acrylic is as close as 22mm where the lense is <b>roughly</b> sitting.



The box the lense came in says 18mm F50.8 - but I have no real way of checking that do I?



Is it possible something else is going wrong? My laser alignment is getting as close to the middle of each mirror as I can get it, and at each extreme it's within 1mm when using tape covering the mirrors/head. I know 1mm is still far, but that is the worst case at the far right of the bed, and I am not cutting there. it's within 0.5mm at the left.



After 4 attempts I can't get it any more accurate than that as the tape burns a whole bigger than that anyway, so impossible to tell for sure.



Any thoughts? Ideas? 



Thanks!







**"Seon Rozenblum"**

---
---
**Joe Alexander** *March 03, 2018 06:46*

did you ensure that the lens is installed "bump" up?


---
**Seon Rozenblum** *March 03, 2018 06:47*

**+Joe Alexander** Yup, def bump up. 


---
**Anthony Bolgar** *March 03, 2018 08:14*

Do a ramp test to find the exact focal length.


---
**Nate Caine** *March 06, 2018 20:21*

We had an (otherwise reliable) vendor ship us mis-labeled lens.  When we complained, they had us measure the focal distance and photograph the result and email it to them.  They then sent us the correct lens.  



I don't have my notes handy (this was 5 years ago).  But I recall the difference was about 90%.  What that means is that a lens designed to focus 10.6u IR light at 2" (50.8mm) will similarly focus visible light at about 90% of that distance ~45mm.  This is because the index of refraction of the ZnSe lens material is very different at 10.6u than from 500nm (visible).



The approximate 22mm distance you describe sounds like it might be a 25.4mm (1") focal length lens.  So try using a point source in a darkened room (led flashlight 10ft away) and then with bump side towards the source, focus onto a white card.  If you have a mis-identified lens, you should get something down near 22mm as you describe, but completely out of focus at 50.8mm.  If you have a known good lens, you can do a comparison.



Don't take my word for the details, the part about 90% (I'll try to dig up my notes), but you get the idea.  Use something you can see (visible light) as a check against whats happening in the invisible IR.  


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/USPUUf7XxxJ) &mdash; content and formatting may not be reliable*
