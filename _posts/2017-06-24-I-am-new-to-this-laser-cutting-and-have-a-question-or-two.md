---
layout: post
title: "I am new to this laser cutting and have a question or two"
date: June 24, 2017 22:33
category: "Discussion"
author: "John Weichert"
---
I am new to this laser cutting and have a question or two. I am using inkscape and laserdrw Inkscape has the extension to create cut lines in red vector cut in blue and engraving in black. when i create an object everthing looks ok in laserdrw. the raster engrave file is black,but when i engrave it the object is left white(blank) and the background is engraved(black). can anyonee give me an idea as to what i am doing wrong?

LikeShow more reactionsComment





**"John Weichert"**

---
---
**Joe Alexander** *June 24, 2017 23:12*

sounds like you have invert on somewhere, look for an option to uncheck.


---
**John Weichert** *June 25, 2017 00:29*

Thanks Joe but I haven't been able to find such a thing


---
**Scorch Works** *June 25, 2017 01:43*

Make sure that the "Sunken" box is checked.  I think un-selecting "Sunken" is equivalent to inverting the image.

![images/9971ad0a8f5a45c402331bd4fba63348.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9971ad0a8f5a45c402331bd4fba63348.png)


---
**John Weichert** *June 25, 2017 13:37*

Thanks scorch works that was my problem.




---
**Marty Battles** *June 27, 2017 09:20*

I have had the same problem if I don't check the sunken box it engraves the background how do you engrave without the sunken box checked I want to do photos but can't like this 


---
**Scorch Works** *June 27, 2017 16:51*

**+Marty Battles**​ in order to do a picture with the stock K40 controller you will need to convert the image to a halftone image.  You need to do this because the stock K40 controller does not control the laser power it only controls laser on/off.


---
*Imported from [Google+](https://plus.google.com/+JohnWeichert/posts/1CQibSvSsju) &mdash; content and formatting may not be reliable*
