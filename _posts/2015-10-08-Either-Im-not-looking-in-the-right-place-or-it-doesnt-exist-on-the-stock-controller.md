---
layout: post
title: "Either I'm not looking in the right place, or it doesn't exist on the stock controller"
date: October 08, 2015 02:56
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Either I'm not looking in the right place, or it doesn't exist on the stock controller. The cutter always starts closest to the upper rail (near the exhaust intake) and moves away from it as it's working. When engraving a piece, this means that all the hot gases are flowing right over what you have just engraved. I'm trying to reverse that. Meaning, I want it to start at the lower rail and move up towards the exhaust.



In terms of x,y coordinates, by default it's always starting at y=0 and increments up from there. I want it to start at y=imagemax_size and increment down to y=0. Does anyone know if the stock controller using LaserDRW has the ability to do this? Can a DSP controller do this?





**"Ashley M. Kirchner [Norym]"**

---
---
**Brooke Hedrick** *October 08, 2015 12:31*

I don't know enough about the software you are using, but MoshiDraw has a setting called TopBottom or BottomTop.  If you choose BottomTop it will start farthest from the exhaust and work toward it.  It doesn't resolve the issue of starting at the front of the machine, though.  



For that, you would need to set the Y position to 200 or so before you start the job.


---
**Ashley M. Kirchner [Norym]** *October 08, 2015 15:03*

Ok, so then the software mine came with does not have that. <grrrr>


---
**Brooke Hedrick** *October 08, 2015 16:06*

Don't get too upset!  Maybe it is called something else.  Plus, believe me, MoshiDraw has some unpleasant issues.  I have had my k40 for just less than 2 weeks and I have already started a 'help' ticket with the eBay seller.


---
**Ashley M. Kirchner [Norym]** *October 08, 2015 16:33*

Nah, I looked and tried everything. Every checkbox, every option, nothing. Mine came with something different, not MoshiDraw. No big deal, I plan on replacing the controller anyway. 


---
**Brooke Hedrick** *October 08, 2015 16:36*

That's where I am headed.  Trying to put the upgrade off for awhile is possible.  Already have the new controller in my light"[object.com](http://object.com) cart.


---
**Ashley M. Kirchner [Norym]** *October 08, 2015 16:59*

Yup, same here. Between that and a z-bed, that's a big chunk that I will be doing.


---
**Kirk Yarina** *October 08, 2015 22:47*

LaserDRW has an equivalent option, but when I tested it it still scans from the side closest to the vent.  It will move where it starts drawing the image, but will then move outside the working are and hit the stops.  Rotating the image 180 will place it in the lower right corner and still scan the same direction.



With a DSP that uses standard gcode you'll still need to find software that will generate the gcode to go from Ymax to Ymin, which might be a bit tricky.  Scanning like this is not in the controller, DSP or otherwise, but rather in the software on your PC that generates the instructions to the laser on how to move.



Have you tried putting masking tape (or one of the laser mask papers) on your workpiece to keep the smoke off of it?  Works for me, although it can be a bit of a pain to remove the little pieces from inside cut areas.


---
**Ashley M. Kirchner [Norym]** *October 08, 2015 22:52*

Adding anything to the piece being engraved causes said material to get burned into the engraving. I guess I'll have to write my own code to have the thing go bottom-top instead of top-bottom.


---
**Brooke Hedrick** *October 08, 2015 23:01*

If you are going the lightobjects route, I believe the software is called LaserCAD.  You may want to check what its capabilities are.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/XppLUTtMNXk) &mdash; content and formatting may not be reliable*
