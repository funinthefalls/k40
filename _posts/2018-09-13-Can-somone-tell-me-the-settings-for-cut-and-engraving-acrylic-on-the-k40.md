---
layout: post
title: "Can somone tell me the settings for cut and engraving acrylic on the k40?"
date: September 13, 2018 18:39
category: "Materials and settings"
author: "Markus Dangl"
---
Can somone tell me the settings for cut and engraving acrylic on the k40?

For cutting i set to 80% and speed of 10 mm/s.

For engraving is set 80% and speed of 200 mm/s

The results are not as good as expected.









**"Markus Dangl"**

---
---
**Anthony Bolgar** *September 13, 2018 19:57*

Need more info: What thickness of acrylic are you trying to cut? Is it cast acrylic or extruded acrylic you are trying to engrave? Every machine will behave differently, depends on how well the mirrors are adjusted, age of the tube, etc. Best thing to do is create a test template and try it at different speeds and power levels until you find what works for your machine.




---
**Markus Dangl** *September 15, 2018 12:10*

I use extruded acrylic. 3mm thick.

For now i set the power to 13% and speed to 150 mm/s for engraving.

Settings for cutting are at the moment 15% power and speed 6 mm/s



The results are quite good. But if i engrave images like jpg it looks not so good.

I'm new at this and i don't know whats better. Setting down the speed or setting more power?



Anyone have experience and can share the settings?



I have to say, that i use K40-Whisperer.


---
**Anthony Bolgar** *September 15, 2018 13:40*

For engraving images you will get a much better result if you use cast acrylic.


---
**Markus Dangl** *September 16, 2018 11:40*

Okay,

I set up the laser with 13% and a speed of 150 mm/s to engrave the acrylic i have.

so, if a would like to speed up the machine. mabe to 400 mm/s. how much to i increase the power.

i cant find the right settings for this. 


---
*Imported from [Google+](https://plus.google.com/107749210380302338093/posts/JY15BviaGs5) &mdash; content and formatting may not be reliable*
