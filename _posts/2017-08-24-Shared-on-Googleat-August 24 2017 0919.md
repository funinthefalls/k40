---
layout: post
title: "Shared on August 24, 2017 09:19...\n"
date: August 24, 2017 09:19
category: "Modification"
author: "Claudio Prezzi"
---
[https://plus.google.com/u/0/+ClaudioPrezzi/posts/MaXX96kxDuH](https://plus.google.com/u/0/+ClaudioPrezzi/posts/MaXX96kxDuH)





**"Claudio Prezzi"**

---
---
**Don Kleinschnitz Jr.** *August 24, 2017 10:27*

#K40Lift

Looks great and a lot like the LO one. What would you say is better about this design?

What did your final cost end up being?

Is this a model or a real unit?

I have never used it so I wonder if Resopal (looked it up though) will provide the dimensional stability/machinability for the screw shafts.


---
**Jeff Bovee** *August 24, 2017 18:06*

I like it but would be concerned that the belt travels through the lasing area and could be clipped...


---
**Claudio Prezzi** *August 24, 2017 22:09*

**+Don Kleinschnitz** Resopal is usually used for building faces. Absolutely immune for humidity and water, very strong, inherently stable and easy to machine. I use it often for machine cases.

The picture is just a rendering yet. I will publish more pictures when I have built it. The calculated cost is about 60-70$.


---
**Claudio Prezzi** *August 24, 2017 22:54*

**+Jeff Bovee** The belt is about 55mm bellow the focal point, which means the beam should not have enough power to damage the belt. 


---
*Imported from [Google+](https://plus.google.com/+ClaudioPrezzi/posts/RBjrU4uwYhR) &mdash; content and formatting may not be reliable*
