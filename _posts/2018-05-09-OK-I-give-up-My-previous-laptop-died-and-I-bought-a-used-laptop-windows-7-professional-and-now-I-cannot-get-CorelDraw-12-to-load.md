---
layout: post
title: "OK, I give up. My previous laptop died and I bought a used laptop (windows 7 professional) and now I cannot get CorelDraw 12 to load"
date: May 09, 2018 14:34
category: "Original software and hardware issues"
author: "Mike Meyer"
---
OK, I give up. My previous laptop died and I bought a used laptop (windows 7 professional) and now I cannot get CorelDraw 12 to load. I use it exclusively for my laser projects...I keep on getting an error message "graphics suite 12.msi not found" when I run setup.exe. Anyone have any ideas? Very frustrating...





**"Mike Meyer"**

---
---
**James Rivera** *May 09, 2018 15:39*

It might be trying to install unsigned drivers, which Windows 7 blocks by default. You can bypass it, but that invites malware (which is the point of the feature). Personally, I do not trust the hacked Chinese version of this software, so I never even tried to install it. My plan was to change the board to a #Cohesion3D, but K40 Whisperer works so well with the stock hardware that I’ve put that on hold...for now.


---
**Stephane Buisson** *May 10, 2018 14:21*

**+Mike Meyer**, try the alternative road, using open source softwares, 

k40 wishsperer wihout hardware modification, 

Or with drop in replacement board, Laserweb 4, Visicut.

also with the same C3D board you can try Lightburn.



this community was crated with the idea to make the K40 an enjoyable/efficient device, we are happy to say we succeed.

dig into the categories ...


---
**James Rivera** *May 10, 2018 15:45*

Yep. Highly recommended.[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
*Imported from [Google+](https://plus.google.com/100105914015640201301/posts/7K5hAJmnAiU) &mdash; content and formatting may not be reliable*
