---
layout: post
title: "What happens when you mix a Raspberry Pi and K40 Whisperer?"
date: March 04, 2018 15:16
category: "Modification"
author: "HalfNormal"
---
What happens when you mix a Raspberry Pi and K40 Whisperer? This is what!



[http://raspi.tv/2018/run-a-k40-laser-cutter-from-your-raspberry-pi-with-k40-whisperer](http://raspi.tv/2018/run-a-k40-laser-cutter-from-your-raspberry-pi-with-k40-whisperer)





**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *March 04, 2018 15:48*

Nice write up...


---
**Timothy Rothman** *March 17, 2018 04:35*

Great job. I'd love to figure out how to use Whisperer with Awesome.tech controller.  I really like Whisperer as I do editing in a lot of other SW and just need something to engrave/cut once my editing is done.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/EZ7Ms7Viy9x) &mdash; content and formatting may not be reliable*
