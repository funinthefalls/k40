---
layout: post
title: "Where do you get your aluminum egg crate louver bed from?"
date: December 12, 2017 03:26
category: "Material suppliers"
author: "Alex Hayden"
---
Where do you get your aluminum egg crate louver bed from? Who has the cheapest deal with shipping. The best I have found is $82 total. For just one 2ft by 4ft sheet. Can anyone offer me a better deal?





**"Alex Hayden"**

---
---
**Ned Hill** *December 12, 2017 05:22*

Are you talking about aluminum honeycomb?


---
**Alex Hayden** *December 12, 2017 05:23*

**+Ned Hill** yes anything like that.


---
**Ned Hill** *December 12, 2017 05:29*

I bought mine over a year ago from this guy on Ebay. [https://www.ebay.com/sch/hcr561/m.html](https://www.ebay.com/sch/hcr561/m.html)?  Depends on what size you need.


---
**Madyn3D CNC, LLC** *December 12, 2017 19:50*

I used a metal paint strainer for a 5-gallon paint bucket for $2.00. I'm not sure you can find one as large as you need though. 



![images/e6101a4a3a845cadbd818abbfcbc427d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e6101a4a3a845cadbd818abbfcbc427d.jpeg)


---
**Cesar Tolentino** *December 23, 2017 15:32*

I use cooling rack for baking, the one found inside ovens. Went to thrift store for 2$


---
*Imported from [Google+](https://plus.google.com/+AlexHayden/posts/gzHD82S4Eva) &mdash; content and formatting may not be reliable*
