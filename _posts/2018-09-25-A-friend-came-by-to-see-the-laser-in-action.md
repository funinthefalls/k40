---
layout: post
title: "A friend came by to see the laser in action"
date: September 25, 2018 18:13
category: "Object produced with laser"
author: "HalfNormal"
---
A friend came by to see the laser in action. I made this fridge magnet to help convince his wife that a laser is a good investment.







![images/e26c861d22e8a4af28b1a5224c79c6d2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e26c861d22e8a4af28b1a5224c79c6d2.jpeg)



**"HalfNormal"**

---
---
**Don Kleinschnitz Jr.** *September 25, 2018 19:14*

😀


---
**Rene Munsch** *September 27, 2018 09:01*

Nice Idea!




---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/CNRhQYxdwTR) &mdash; content and formatting may not be reliable*
