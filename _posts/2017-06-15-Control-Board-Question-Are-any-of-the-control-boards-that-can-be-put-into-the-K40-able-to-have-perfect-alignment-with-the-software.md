---
layout: post
title: "Control Board Question: Are any of the control boards that can be put into the K40 able to have perfect alignment with the software?"
date: June 15, 2017 19:53
category: "Discussion"
author: "Nathan Thomas"
---
Control Board Question:



Are any of the control boards that can be put into the K40 able to have perfect alignment with the software?



The primary function of the red dot laser we install is so we can see where the engrave/cut will begin etc. Is there a control board out there that will tell you that from the software?



For example, I have an old epilog with corel draw and the engraving table is directly set to the software...so  when I set my design (from the software) to start engraving at coordinates 5" from the corner, the laser head moves there on it's own and starts engraving. The red dot is nice, but not necessary. 



Is there a control board that allows something similar that can go on the K40?



I see people referencing different controllers in here all the time, so I'm curious if that's a function of any of them.





**"Nathan Thomas"**

---
---
**Ashley M. Kirchner [Norym]** *June 16, 2017 00:31*

That's all done in software for the simple fact that 1 control board can run different sizes of machines. So you set the machine settings in the software, not the hardware. 


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 00:35*

Regardless of what software I'm using, I always create a canvas size that matches the machine, 300x200mm. Then I create my design on that, even if it's a tiny piece. By having the full canvas, I control where I put it and where the machine puts it on the material. Both the stock controller, as well as any other will support this, it's all done in software. 


---
**Nathan Thomas** *June 16, 2017 09:54*

**+Ashley M. Kirchner** I understand what you're saying. I know that you can move the design around the software and it will move the laser head. And that's the function of the diode to tell you where it will engrave. So I can set the coordinates at (10, 10) and the head will move there and start to engrave. 



I simplified this on one of the K40s with 2 rulers, one at x the other at y, once I found true zero. But I was curious if there was a software I could use instead on the 2nd K40. That way it'll be easier to measure accurately when I want to use the middle of the table and still get a precise cut/engrave...without the extra steps of test firing or converting inches to mm 


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 14:00*

Those machines are designed in mm, all the software for them will be in mm. Perhaps if you try to explain what you want to achieve, we might be able to help you how to do it better, assuming there is a better way. 


---
**Nathan Thomas** *June 16, 2017 14:04*

Mentioned it earlier, I'd like to be able to do precise cuts and engraving without needing the red dot or test firing...knowing exactly where it will engrave from the software. But from the convo I believe I'll just have to improvise like I did before. Once I have it setup I'll post a video of what I did and what I was talking about 😊


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 14:19*

I guess I'm not understanding what you mean with "precise cuts and engrave" then. I don't have a red dot on my machine, I don't need it. I cut and engrave all day long. Where I set the piece in the software is where it will be on the material piece. I'm able to engrave exactly where I want it then cut exactly where I want it. I'm also able to do double sided pieces with no problem. That's what jigs are for. 


---
**Nathan Thomas** *June 16, 2017 14:23*

If you have a piece that is 6" x 6"....but you're only trying to cut a shape 1" x 1" precisely at 4" from the top, 2" from the bottom. How would you handle that?


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 14:35*

I have a 3D printed jig in my machine that allows me to put any piece at the exact 0,0 every time. I will draw a 6x6 outline in the software starting at that 0,0 corner, put the artwork where I need it to be within that outline (the required 4" offset that you want), put the material in and of I go. Easy and repeatable, over and over again. 


---
**Nathan Thomas** *June 16, 2017 14:43*

Exactly. So my original question was basically...."is there a software or controller that allows you to do those easily repeatable and accurate steps without all the extras of making jigs, diodes or other things we diy to accomplish that." 



But maybe not, and that's cool I was just asking before I did my own thing again. I like your design though, I do something similar to find 0 but the jig sounds easier 


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 14:57*

No software nor controller will do that for you, simply because every machine is different and every operator will use it differently. Controllers are used for multiple diffent machines as are softwares.  I never bother with converting inches to mm. If I want to draw in inches, then I'll draw in inches. It makes zero difference as far as the machine goes. Learn the limits (300x200) and draw within that. For me that means always creating a canvas in my application that matches that size, then draw within that. With the corner jig, what I draw is what I'll get on the machine. I don't have to figure out where it is on the bed. And if I'm doing a double sided, I simply flip the piece within the hole I just cut it out of. Sounds to me like you're overcomplicating things in your head when you don't need to. But I could be wrong. 


---
**Nathan Thomas** *June 16, 2017 15:06*

Actually my epilog does which is where the question comes from. But that's the difference between a $10k machine and a diy machine...which I'm loving anyways because I can customize it.


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 15:18*

You said it yourself earlier, it's set in the software. It is not done in the hardware. It's all software based. You don't seem to understand that. You can set the table size in any software that you use to draw in, ANY. With the original stock board, create a layout that matches the table, 300x200. With LaserDRW, do the same thing. With LaserWeb, configure it to that. I draw in Illustrator and Photoshop, my canvasses are always set to the size of the bed. The same hardware is used for a variety of diffent sized laser beds, it's how the software gets set up. You can misconfigure the software and the machine will happily do what you've asked if to do and the head will go slamming into the end stop. The hardware does not know what the physical size is, is only doing what the software tells it and it checks its end stops. Please understand that the controller has no idea about the physical size of the bed. 


---
**Nathan Thomas** *June 16, 2017 15:25*

You don't seem to understand the basic question. How many times did I start off asking specifically about the SOFTWARE. I mentioned Corel draw etc...because the hardware reads the software.  So I said software AND controller because they are linked. Why change one without the other? I am not asking only about a control board. If you don't have the understanding or patience don't reply to questions.  


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 16:19*

In this specific instance, Epilog simply choose to use Coreldraw with their custom controllers. In that aspect they are "linked", however that is because of them. Epilog choose to write a plugin for Coreldraw which talks to their custom board and to make it easier for the user, they added a custom layout in Coreldraw so the user always has the right size for their machine. However, with a Smoothie based controller, you can use several diffent softwares for it, that's the beauty of it. There's nothing about it that has to be "linked" with anything specific. So long as you are using a software that can talk the language that the controller understands. So long as you know how to configure it correctly for your machine. The same applies for those crappy Chinese controllers that the k40 comes with, you're locked into using their plugin, which only works with Coreldraw - I never bothered with Coreldraw, I simply used the layout interface to import my Illustrator or Photoshop files and send them to the laser. I've used mine like that for a year, engraving, cutting, multiple passes, double sided, never had a problem aligning things. Eventually, due to lack of full laser PWM control and proper vector support, I changed it for a Smoothie based board, however my software workflow has not changed, I still design in Illustrator and create vector cut files from there. I still import into the layout program. Now if you want to do that, then look at the Cohesion3d Mini laser upgrade bundle. Otherwise my answer will stands, there is no one specific software nor hardware combination. Not even hardware and layout program as you can use it without the layout program as well. 


---
**Ashley M. Kirchner [Norym]** *June 16, 2017 16:26*

To clarify, 

old, stock Chinese board:

(Illustrator/Photoshop) -> LaserDRW layout -> laser



new, Cohesion3D Mini board:

(Illustrator/Photoshop) -> LaserWeb layout -> laser 



My design software don't change. The workflow didn't change either. Only the layout software since it now had much better control of the laser. 


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/KAe8sgpML5N) &mdash; content and formatting may not be reliable*
