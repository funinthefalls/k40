---
layout: post
title: "Spacer to fit 12mm lens in 18mm holder"
date: January 10, 2018 17:31
category: "Modification"
author: "Ray Rivera"
---
Spacer to fit 12mm lens in 18mm holder. 



Purchased the Lightobjects 18mm Laser Head w/Air Assist; created this spacer to be able to reuse the 12mm lens I already have. It took about 4 go's to get the inner and outer diameters snug for my particular lens and laser head. I took a smaller 60mmx60mm piece of 1.58mm (1/16") acrylic and sanded it down to 1.15mm. This made it thinner than the lens thickness (1.25mm) so that when the head is assembled the friction/pressure from head holds the lens in place especially if the acrylic warps/changes from heat.



[https://drive.google.com/open?id=1NuIjTb5NPC81ZXC__NIxioeAIO6rTkSe](https://drive.google.com/open?id=1NuIjTb5NPC81ZXC__NIxioeAIO6rTkSe)





**"Ray Rivera"**

---
---
**Ned Hill** *January 11, 2018 01:56*

Good solution. It’s still better to upgrade to the larger lens when you can because it’s less sensitive to misalignment. 


---
**Ray Rivera** *January 11, 2018 16:48*

Will keep that in mind for the future. 


---
*Imported from [Google+](https://plus.google.com/117639914879018784950/posts/Y7EG4jtX7jy) &mdash; content and formatting may not be reliable*
