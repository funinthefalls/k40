---
layout: post
title: "My Trio 8\" Windows tablet. Going to use it as a touchscreen interface for my K40 and LE400 lasers"
date: April 29, 2016 01:58
category: "Modification"
author: "Anthony Bolgar"
---
My Trio 8" Windows tablet. Going to use it as a touchscreen interface for my K40 and LE400 lasers. Also posted a pic of a neat little Z axis that was 3d printed and only cost $20 including the stepper motor and 8mm rails.



![images/3c6f77ae0cc4093dfe1be0322be7bad0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3c6f77ae0cc4093dfe1be0322be7bad0.jpeg)
![images/f34e97304956aa0d013150d80e1e4243.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f34e97304956aa0d013150d80e1e4243.jpeg)

**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 02:39*

I like the Z-axis. Great idea to run everything from a tablet also.


---
**Josh Rhodes** *April 29, 2016 02:54*

Is there a model online for the z-axis?


---
**Ariel Yahni (UniKpty)** *April 29, 2016 02:56*

How is that Z axis suppose to work? 


---
**Rob Thompson** *April 29, 2016 03:09*

Good job! I'm on this K40 - I hope - like white on rice :-) 



From my few days worth of being possibly the 1000th member of the group, I'm looking forward to learning more and contributing. 



My smoothie board is in the mail - which I am guessing is how you control the Z?


---
**Anthony Bolgar** *April 29, 2016 05:12*

My tablet is running Windows 8.1


---
**Anthony Bolgar** *April 29, 2016 05:14*

**+Josh Rhodes** [http://www.thingiverse.com/thing:962400](http://www.thingiverse.com/thing:962400) for the Z axis slider. It says to use 1/4" threaded rod but I used 5/16 and epoxied the nut in place instead of putting it in the notch designed for a 1/4" nut. And it is just a basic Nema17 stepper motor.


---
**Anthony Bolgar** *April 29, 2016 05:19*

**+Peter van der Walt** Mine is running an Intel atom processor.


---
**Anthony Bolgar** *April 29, 2016 07:13*

Just found a guy locally selling the exact same unit for $140 Here is his link [http://www.kijiji.ca/v-printers-scanners-fax/mississauga-peel-region/slider-z-axis-cnc-nema-17-paste-extruder-arduino-projects-diy-3d/1129848878?enableSearchNavigationFlag=true](http://www.kijiji.ca/v-printers-scanners-fax/mississauga-peel-region/slider-z-axis-cnc-nema-17-paste-extruder-arduino-projects-diy-3d/1129848878?enableSearchNavigationFlag=true)  Are there really dumb enouigh peopple out there that would pay that much for a cheap $20 item?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 29, 2016 07:38*

**+Anthony Bolgar** I'd for certain say that is the case with everything. So many things you can make yourself are way overpriced (e.g. a wallet, costs like $20-30 to make, is customisable & people sell for somewhere in the $80+ range for a decent 100% leather one).


---
**TinkerMake** *May 04, 2016 03:42*

**+Anthony Bolgar** How did you implement the z-axis on the laser? Very curious to see how this was achieved...as are others. Thanks.


---
**Anthony Bolgar** *May 05, 2016 05:21*

The Z axix was just a toy to make,not for the laser


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/eqJtsgUe7qg) &mdash; content and formatting may not be reliable*
