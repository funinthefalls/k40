---
layout: post
title: "Created a set of custom engraved slate coasters with a custom 3mm ply holder"
date: November 29, 2018 20:34
category: "Object produced with laser"
author: "Ned Hill"
---
Created a set of custom engraved slate coasters with a custom 3mm ply holder. Made it for a silent auction at my up coming college fraternity chapter’s 50th anniversary get together. 



![images/52438a796d007f0b66b8f8520c880d12.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52438a796d007f0b66b8f8520c880d12.jpeg)
![images/6ffb3f9e32992caa908cdaf86ea027c9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ffb3f9e32992caa908cdaf86ea027c9.jpeg)
![images/e11c51a97225b7809ee1b961699bc178.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e11c51a97225b7809ee1b961699bc178.jpeg)
![images/1674fc0dafee918b6317b4a453d71579.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1674fc0dafee918b6317b4a453d71579.jpeg)
![images/c14040f2fb0c2b10da8ed44e5010e281.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c14040f2fb0c2b10da8ed44e5010e281.jpeg)

**"Ned Hill"**

---
---
**Don Kleinschnitz Jr.** *December 02, 2018 13:11*

Again very nice. Where did you get the slate from?


---
**Don Kleinschnitz Jr.** *December 02, 2018 13:16*

**+Ned Hill** what thickness material, speed, current,  and lens did you use to make these cuts please. My engraving seems to be ok but not cutting that well and I need a reference.


---
**Ned Hill** *December 02, 2018 18:22*

**+Don Kleinschnitz Jr.** The slate came from Michaels craft store of all places.  They are selling a set of 4 4x4" coasters, with thin bumper pads on the bottom, for $3.29.  They had a 40% off storewide over thanksgiving weekend if you ordered online.  So I ordered 30 sets, got free shipping, and cleaned them out online :).  They seem to be back in stock online and local stores may still have some if there is one in your area.  On Alibaba you can find them even cheaper if you are will to buy at least 100 sets and wait for the slow boat from china shipping.  



[https://www.michaels.com/slate-coasters-by-artminds/10212298.html](https://www.michaels.com/slate-coasters-by-artminds/10212298.html)



The thickness of the birch ply for the holder was 3.1mm.  I cut it at 10mm/s and 5ma. (Keep in mind I've got a brand new tube) My lens is a 50.8mm focus that I got from light objects.


---
**Ned Hill** *December 02, 2018 18:38*

This was actually my first time doing slate.  It was interesting that the actual engraving didn't really depend much on the power and speed.  That is, there wasn't much difference between high power and slow speed and lower power/fast speeds.  This is probably true to a certain extent as I didn't try and find the bottom range of speed/power.  It appears that the laser is just thermally altering the mineralogy of the slate and it's just a surface effect.  



Also noticed that I was sometimes seeing small black specks in the engraved area.  When I looked them under the microscope it was clear that the specks were mineral inclusions in the slate that were resistant to lasering.


---
**Don Kleinschnitz Jr.** *December 03, 2018 12:40*

**+Ned Hill** Thanks....


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/GQHmZMBpkpq) &mdash; content and formatting may not be reliable*
