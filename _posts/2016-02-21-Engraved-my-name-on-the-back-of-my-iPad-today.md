---
layout: post
title: "Engraved my name on the back of my iPad today"
date: February 21, 2016 21:27
category: "Discussion"
author: "Scott Thorne"
---
Engraved my name on the back of my iPad today. ...it did great on this aluminium. 

![images/9a1f3939422e9f71e91edf9d475d65a7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a1f3939422e9f71e91edf9d475d65a7.jpeg)



**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 21, 2016 23:53*

That's very nice. I like how it lightens the aluminium.


---
**Jim Hatch** *April 04, 2016 20:54*

@scott thorne - Scott, what settings did you use for that? I want to do something similar for some work iPads but don't want to trash them with an errant laser etch :-)


---
**Scott Thorne** *April 04, 2016 21:05*

300 mm/s at 25% power.


---
**Jim Hatch** *April 04, 2016 21:07*

Great. Thanks! Figured I'd have to trial & error it otherwise start low & fast. This helps cut the process time :-)


---
**Scott Thorne** *April 05, 2016 01:38*

Maske sure you out it on a towel or something...it will slide around if you don't...I always run a practice test with my laser off...it slid all over the place.


---
**Jim Hatch** *April 05, 2016 01:42*

Thanks. I would have missed that.


---
**Scott Thorne** *April 05, 2016 01:44*

Oops...that was supposed to read...sit it on a towel...lol...sorry Jim.


---
**Jim Hatch** *April 05, 2016 01:46*

LOL I figured it out. I didn't even read the "out it" - read it as "put it". Did see the "Maske sure" but I'm always making typos on the phone.


---
**Scott Thorne** *April 05, 2016 02:13*

Lol...missed that one...I hate this new phone...hard to type on.


---
**Jim Hatch** *April 06, 2016 15:58*

Did 2 iPads & an iPad mini. Your settings were spot on. I did try speeding one up (and bumped up the power a bit) but it didn't turn out as well. I think I needed more power - I'm going to create something that gives me power/mm/s so I can take something like 25% @ 300mm/s and turn it into a new % for 500mm/s for instance (I used 30 but I think I needed to go closer to 35-40% since I was jumping the speed by 67% I should have jumped the power by that too).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2016 16:02*

**+Jim Hatch** I'd imagine that going 2/3rds faster would require 2/3rds more power to give the same result. Based off your values, 2/3 of 25% = ~16.6%, .: you would require ~41.6% power @ 500mm/s. But it is a good idea to create some sort of calculation chart. Also, you may find that things don't scale linearly (although I'd imagine that they would).


---
**Scott Thorne** *April 06, 2016 16:09*

**+Jim Hatch**...Yuusuf is right...there is a speed to power ratio that needs to be applied when marking aluminum....using different types of wood as well...for example...I can engrave birch plywood very deep at 500mm/s at 25% power but when I engrave cherry or walnut I have to slow down to 300mm/s @40% power....glad I could help you with the project.


---
**Jim Hatch** *April 06, 2016 16:11*

My daughter wants a customized cartoon on the back of hers. And my wife asked if I'd be willing to do 250 of them for the parochial school she works at :-) I haven't volunteered for that. LOL


---
**Scott Thorne** *April 06, 2016 16:12*

I would be hesitant as well. 


---
**Ariel Yahni (UniKpty)** *May 31, 2016 01:50*

**+Scott Thorne**​ I'm trying this on an iPad but for some reason almost no register on the ammeter even wen dial at 15, speed was at 320


---
**Scott Thorne** *May 31, 2016 09:25*

**+Ariel Yahni**....that's odd


---
**Jim Hatch** *May 31, 2016 12:07*

**+Ariel Yahni**​ that doesn't sound like it's related to the iPad. The laser doesn't know what you're etching. Does the same file work on any other material like cardboard or plywood?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 31, 2016 12:32*

**+Ariel Yahni** Does your machine have the pot instead of digital % buttons? My pot is touchy. Sometimes when I set it to a value, when lasering it fluctuates strangely. I have to hold down the test fire button for like 5 seconds to do a proper test to get it at a stable amount on the ammeter, whilst I adjust to the value that I wish to receive (e.g 10mA). I think it's probably one upgrade that is worth doing as a few other's here have replaced the pot with a better one & seem to not have those issues.


---
**Ariel Yahni (UniKpty)** *May 31, 2016 13:52*

**+Jim Hatch**​ I'm testing new auto recognize features of LW lol. 


---
**Scott Thorne** *May 31, 2016 14:04*

Lol


---
**Ariel Yahni (UniKpty)** *May 31, 2016 14:29*

Don't discard the idea :) 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/CuzXDCoBSqo) &mdash; content and formatting may not be reliable*
