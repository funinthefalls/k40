---
layout: post
title: "Having issue with a new k40 will not emit any power on test unless it is cranked up and will not even burn a dot on a piece of paper unless it us up to about 90 percent"
date: March 14, 2017 01:13
category: "Original software and hardware issues"
author: "camille sugrue"
---
Having issue with a new k40  will not emit any power on test unless it is cranked up and will not even burn a dot on a piece of paper unless it us up to about 90 percent.  I did notice that the purple light stops at the end of the tube and jumps across to mirror.  Suggestions???





**"camille sugrue"**

---
---
**Paul de Groot** *March 14, 2017 05:51*

Can you see whether the red led on the power supply is on? Just trying to see if it's the power supply or tube.


---
**camille sugrue** *March 14, 2017 07:39*

No just the green light, I have read in other posts that people have heard a pop if the power supply is bad will the laser tube still fire?


---
**Paul de Groot** *March 14, 2017 07:43*

This is what I try to ask. It's a little red led at the powersupply. If that doesn't light then the power supply is bad.

![images/140490ab43d1c4ecc36197cf1514c83c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/140490ab43d1c4ecc36197cf1514c83c.jpeg)


---
**Don Kleinschnitz Jr.** *March 14, 2017 11:28*

**+Paul de Groot** some power supplies have a green light instead of a red one :).

**+camille sugrue** some things to check and answer:

1. is there any arching in or around the tube. (video it pls)?

2. what coolant are you using?

3. what is the background and state of machine prior to this problem. New machine, worked fine then this started etc, etc?

4. Photo of laser power supply & panel

5. Upgrades to this machine?


---
**Nate Caine** *March 14, 2017 18:45*

Don't overlook the possibility that the HVPS is <b>doing</b> what it's told to do, but is being <b>told</b> to do the wrong thing.  I don't know your HVPS, but in some cases if the front panel "Current Regulation" pot is bad, or a bad connection, might cause it to put out low power because it thinks that's what you are asking for.  



When you adjust the pot, while it's running, what does the "Current Indication" panel meter show?  Does it vary between 0mA and 20mA as you adjust it and the laser fires? 



Are you certain that the "Laser Switch" (enable) is on (so not inhibiting the laser).  You need to supply more information about exactly what you are observing.


---
**camille sugrue** *March 14, 2017 19:25*

I thought I had video of the tube and power supply in action along with the lack of power when it came to the final output, I  upgraded both the mirror and the focus lens, the machine is brand new, no coolant in the distilled water,  the video came out black I think I was two close to the laser tube or some other issue, I  will have to take more pictures and videos this weekend 


---
**camille sugrue** *March 14, 2017 19:51*

Mine is the newer model with the digital read out and temperature display, it will not fire unless it is about 17-20 percent it will not make a mark table material unless it is higher for  testing I needed to have the power cranked up 90 percent just to get something out of the last mirror and had to hold the test button down for almost 20 to thirty seconds....not a good thing , I am sure that trying to run it at 90 percent even if I could have gotten a continuous burn is bad for the machine


---
**Chris Hurley** *March 15, 2017 02:45*

do a step by step run down. what do you get when you put paper right in front of the tube? (and for the love of puppies you have this thing grounded right?)


---
**camille sugrue** *March 15, 2017 05:14*

I like puppies so yes it is grounded.  The break down is this, first no measurable burn comes out of the laser unless it is set at 17-20 percent per the digital read out, paper burns in a round dot on first lens in front of the tube ...moving on to 2nd mirror, paper in front of in the forward (home ) position I get burn, moving on to next mirror, still in home position I get burn within the circle but I have to increase the power... now we are at about 40 percent, but still in the home position I  get a burn, no I want to get a mark out of the focus lens  I get next to nothing until I crank it up to about 80 to 90 percent and I do not really get a mark just a scratch and I am holding doing the test button constant just to get that.  Now I know that some of these things have the focus lens with the wrong orientation so I changed that so I could at least get some output from the focus lens, mind you this is all in the home position. Move the focus lens away and I get nothing.  Not sure where the power drops, but it does, This is all with good quality mirrors (not the cheap stuff it came with).  My other machine worked great out of the box with a little tuning.  I purchased this one from a different supplier because I thought it would be just a better to have the digital readouts, power supply is noisier.  Still not sure about the pop I heard tube does not look cracked I have not opened up the power supply.  


---
**Don Kleinschnitz Jr.** *March 15, 2017 12:47*

**+camille sugrue** the "Move the focus lens away and I get nothing" statement leads me to believe that there is some kind of optical alignment problem (among other issues). Even if the laser was not putting out full power this should not be the case. 



All I can suggest is try and remove one symptom at a time. 



Optical troubleshooting:

Move the head to where it drops power and then trace backward (with burned targets) until you find where you loose it or it goes out of allignment. 



You probably know all this but:

The beam, as it traverses the mirrors, must be in the same vertical plane as the laser. 

The beam coming from mirror 1 to mirror 2 must be at a 90 to the tubes output and the beam from mirror 2 to mirror 3 must be 90 to that path. Then the beam down through the objective lens must be at 90 (downward) to that beam. 



I have seen lack of parallelism and angular position to cause the beam to shift position on the last mirror as the head moves left to right. This causes the beam to be off center in the lens and deflect off of the lens mount or the air assist walls.   


---
**camille sugrue** *March 15, 2017 17:16*

Thanks


---
*Imported from [Google+](https://plus.google.com/101932514551985897452/posts/8bruHs7LWcx) &mdash; content and formatting may not be reliable*
