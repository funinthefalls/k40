---
layout: post
title: "Anyone point me in the direction of plans for a new extruded aluminum frame for the K40?"
date: August 16, 2017 15:18
category: "Discussion"
author: "Bill Keeter"
---
Anyone point me in the direction of plans for a new extruded aluminum frame for the K40?



I would love to build a new larger case and then gut my K40 for parts. Vslot for the X and Y rails. I've been brainstorming this for a while but figure someone has already leap frogged this idea.





**"Bill Keeter"**

---
---
**Don Kleinschnitz Jr.** *August 16, 2017 15:40*

#K40Extension



Search works great! :).



[https://plus.google.com/u/0/+ArielYahni/posts/KgT8mBrPm85](https://plus.google.com/u/0/+ArielYahni/posts/KgT8mBrPm85)



[https://plus.google.com/u/0/104261497719116989180/posts/jSb4MiFyGy4](https://plus.google.com/u/0/104261497719116989180/posts/jSb4MiFyGy4)



[plus.google.com - Hello all and Happy New Year to everyone! Finally the Project is finished! I...](https://plus.google.com/+KonstantinosFilosofou/posts/5dtgUA61SBL)


---
**Jim Fong** *August 16, 2017 15:55*

[http://openbuilds.org](http://openbuilds.org) has several laser build logs using their extrusion.  While maybe not K40 specific, it wouldn't really matter since you are really after a reliable x/y Cartesian frame. Using their components make it easy.   Makerslide is another extrusion that is popular with the 3d crowd.  



I am doing the same but using more industrial stuff. 80/20 extrusion, THK linear rails and Servo motors.  This will be CoreXY instead of the more popular standard Cartesian XY.  


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/eVXuJZ41scG) &mdash; content and formatting may not be reliable*
