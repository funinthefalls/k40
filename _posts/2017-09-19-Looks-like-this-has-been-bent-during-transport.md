---
layout: post
title: "Looks like this has been bent during transport"
date: September 19, 2017 04:07
category: "Original software and hardware issues"
author: "BEN 3D"
---


Looks like this has been bent during transport. Or is it with you also so crooked?

![images/83bda0deb05a3fe7b43aafbc29b20cc1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/83bda0deb05a3fe7b43aafbc29b20cc1.jpeg)



**"BEN 3D"**

---
---
**Ned Hill** *September 19, 2017 16:57*

Does appear to be bent somewhat.  I would recommend removing the mounting plate and place it on a flat surface to see how much it's really bent.  Then probably straighten if you can.


---
**BEN 3D** *September 19, 2017 17:22*

Ok thanks, i think i get it. Just want to be safe that is not the correct form.


---
**Michael Audette** *September 19, 2017 18:04*

It really depends on how well the laser/mirrors are aligned there...although that looks pretty extreme.




---
**BEN 3D** *September 19, 2017 18:36*

oh jeah it is BENted ;-) I uploaded a Video for you. I will screw it between two heavy and plane parts I found.




{% include youtubePlayer.html id="-hQ39-tRuc4" %}
[youtube.com - blent aluminium](https://youtu.be/-hQ39-tRuc4)


---
**BEN 3D** *September 19, 2017 18:51*

Now that is my planed fix


{% include youtubePlayer.html id="h9mVBPFNuz8" %}
[youtube.com - BENted Fix](https://youtu.be/h9mVBPFNuz8)


---
**BEN 3D** *September 19, 2017 20:22*

Its better then before.

But not really plane. I searched on thingiverse but did not find a replacement sketch for my 3d printer. Whatever, I will give it a try.![images/6d32c1bf461460c0f42b2dceb0e787bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6d32c1bf461460c0f42b2dceb0e787bc.jpeg)


---
**Ned Hill** *September 19, 2017 20:59*

I found this on thingiverse that looks like it could work. [thingiverse.com - K40 Laser Cutter New Lens mount by Retroplayer](https://www.thingiverse.com/thing:621265)


---
**BEN 3D** *September 19, 2017 21:04*

Cool, thanks.


---
**BEN 3D** *September 20, 2017 06:19*

And last but not least  a BENted fixed photo![images/3da2724515ceb612d029a725ad3d5012.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3da2724515ceb612d029a725ad3d5012.jpeg)


---
**Don Kleinschnitz Jr.** *September 20, 2017 13:07*

**+BEN 3D**

I rebuilt my objective stage using .22" acrylic and have been happy with it.

[donsthings.blogspot.com - K40 Optical Path Improvements](http://donsthings.blogspot.com/2017/01/k40-optical-path-improvements.html)


---
**BEN 3D** *September 20, 2017 13:41*

Hi **+Don Kleinschnitz**, Super thank you very much. 

Cheers BEN 3D


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/ip3vbSPkMQz) &mdash; content and formatting may not be reliable*
