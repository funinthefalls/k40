---
layout: post
title: "What are your favorite sources for materials?"
date: July 06, 2016 20:27
category: "Material suppliers"
author: "Tev Kaber"
---
What are your favorite sources for materials? 



Near my house (MA, USA), Lowes has plenty of 1/4" wood but not much 3mm, AC Moore has pricy 3mm plywood, but cheap boxes and plaques. 



I ordered some acrylic from a seller on Etsy but it seems pricy.

[https://www.etsy.com/listing/230952815/18-3mm-thick-opaque-black-cast-acrylic?ref=shop_home_active_65](https://www.etsy.com/listing/230952815/18-3mm-thick-opaque-black-cast-acrylic?ref=shop_home_active_65)



I also ordered some craft 3mm plywood from Midwest Products (cutting out the AC Moore markup).

[http://midwestproducts.com/products/3-mm-x-12-x-12-ply-wood](http://midwestproducts.com/products/3-mm-x-12-x-12-ply-wood)



I don't know where to get MDF.



What are people's favorite material sources?





**"Tev Kaber"**

---
---
**Jeff Kwait** *July 06, 2016 20:35*

just bought this off of ebay so far so good



[http://www.ebay.com/itm/1-8-3mm-x-8-x-10-Baltic-Birch-Plywood-for-CNC-Laser-Scoll-Saw-40-pieces/272196329852?_trksid=p2047675.c100012.m1985&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D35389%26meid%3Dc69ab52da55b4b5bb705277a26c8018c%26pid%3D100012%26rk%3D1%26rkt%3D10%26mehot%3Dlo%26sd%3D351662631467](http://www.ebay.com/itm/1-8-3mm-x-8-x-10-Baltic-Birch-Plywood-for-CNC-Laser-Scoll-Saw-40-pieces/272196329852?_trksid=p2047675.c100012.m1985&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D35389%26meid%3Dc69ab52da55b4b5bb705277a26c8018c%26pid%3D100012%26rk%3D1%26rkt%3D10%26mehot%3Dlo%26sd%3D351662631467)


---
**Jeff Kwait** *July 06, 2016 20:35*

I actually bought the 12 x 12 from the same seller


---
**Evan Fosmark** *July 06, 2016 20:56*

I go to my local TAP Plastics shop. They have a "scrap" section. Yesterday I picked up 10lbs of acrylic for $10.


---
**Josh Rhodes** *July 06, 2016 21:06*

**+Tev Kaber**​ lowes in the south always has some MDF too.



I like to goto ace if it's something they have.



My town is a "small town" when it comes to this sorta stuff. Shipping things sucks though.


---
**Jim Hatch** *July 06, 2016 23:12*

Michaels has felt & small boxes. Dollar General has cutting boards, baking utensils, etc. Woodcraft has 3mm Baltic birch & lots of other woods as well as finishing materials. Amazon for 12"x12" wood (scrollsaw wood) and cast acrylic in 1/8" & 1/4" and lots of colors. Also Tandy Leather for leather. I don't do much MDF because the stuff at Lowes and Home Depot contain formaldehyde resins which are nasty & it makes a lot of fine dust that gets sucked into the exhaust. There are laser safe MDF supplies out there but I haven't found any local.


---
**Ashley M. Kirchner [Norym]** *July 06, 2016 23:52*

[http://ocoochhardwoods.com](http://ocoochhardwoods.com)


---
**Michael Audette** *July 07, 2016 11:21*

MA is a big enough state.  I have gone to highland hardwoods in NH (It's a haul) and to The woodery which is much closer to me in Lunenburg....but that was for nice hardwoods.  ([http://www.wooderylumber.com](http://www.wooderylumber.com))...They both however have nice ply (and exotic ply).  You can call and see what they have.  Both have huge selections of lumber and excellent customer service (Compared to the big box droids).



If you actually want volume:  [https://www.amazon.com/12-Baltic-Birch-Plywood-Woodpeckers](https://www.amazon.com/12-Baltic-Birch-Plywood-Woodpeckers)®/dp/B013NT3OAC/ref=sr_1_2?ie=UTF8&qid=1467890296&sr=8-2&keywords=3mm+plywood



This works out to $1.20/sheet for 3mm baltic birch.



As much as I like ordering wood online (I don't) I prefer to go look at the wood before I buy it and/or pick out a piece with the grain I prefer.


---
**Stephane Buisson** *July 07, 2016 17:26*

[https://plus.google.com/117750252531506832328/posts/57ozsyxMQK9](https://plus.google.com/117750252531506832328/posts/57ozsyxMQK9) is mostly made of 3mm ply (in EU)


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/D5nkArSYKtV) &mdash; content and formatting may not be reliable*
