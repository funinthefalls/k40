---
layout: post
title: "I'm just designing a breakout board for my M2Nano"
date: May 25, 2018 08:07
category: "Hardware and Laser settings"
author: "Scruff Meister"
---
I'm just designing a breakout board for my M2Nano. Can anyone tell me what the part number (or name) of the power connector and the Y motor connector are please - I've circled in the picture?



I thought the Y-stepper might be a JST-XH but it doesn't quite seem to fit in some reference connectors I have for that type. The power type I am not at all familiar with...



![images/6ad805b0f1178aae21b7b4adf0671acb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6ad805b0f1178aae21b7b4adf0671acb.jpeg)



**"Scruff Meister"**

---
---
**Don Kleinschnitz Jr.** *May 25, 2018 11:50*

Pretty sure that **+Ray Kholodovsky** has these part #.


---
**Ray Kholodovsky (Cohesion3D)** *May 25, 2018 15:18*

Motor connector is XH for sure. 



Power is a VH3.96. 


---
*Imported from [Google+](https://plus.google.com/116126524987588221083/posts/fxAjjTgKEHa) &mdash; content and formatting may not be reliable*
