---
layout: post
title: "Should I be concerned about this noise coming from my PSU when the laser is firing?"
date: October 14, 2016 00:08
category: "Discussion"
author: "K"
---
Should I be concerned about this noise coming from my PSU when the laser is firing?

![images/8aee6f16f6f52ad1ef839f1026ccfc3e.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/8aee6f16f6f52ad1ef839f1026ccfc3e.gif)



**"K"**

---
---
**K** *October 14, 2016 06:11*

I've been told that the noise is from the PWM, so no big deal. Thanks to Alex Krause for clearing that up for me. 


---
*Imported from [Google+](https://plus.google.com/115567306264374614046/posts/eitqXyXT3Eb) &mdash; content and formatting may not be reliable*
