---
layout: post
title: "Anyone using a cheap mini frig to keep their water chilled?"
date: July 31, 2017 02:09
category: "Discussion"
author: "Bill Keeter"
---
Anyone using a cheap mini frig to keep their water chilled? This weekend I was randomly wondering if I could ran tubes through a hole in the side and keep 4 or 5 gallons chilled in a reservoir. Maybe just seal up the hole with hot glue or epoxy.  ...Would this work or is a mini frig just too inefficient?





**"Bill Keeter"**

---
---
**Martin Dillon** *July 31, 2017 15:16*

A mini fridge would be fine you just need to watch out for condensation.  I think typically a fridge is colder than you want your water and I find the thermostats on mini fridges don't work very well.


---
**Don Kleinschnitz Jr.** *August 01, 2017 14:18*

I have also wondered about a roll of copper tubing in a small frig?


---
**Eric S** *August 01, 2017 18:23*

I'm using a kitchen water cooler (with a thermometer to monitor) and never had any issues, I turn it on at the same time as I power on the K40, so there's no thermal stress and by the time I have my stuff set up the temperature drops slowly to compensate for the heat eventually generated by the laser.   I Guess using copper tubing or anything with "heat sink" vs just putting a bucket of water in a mini fridge should work fine.  Bucket of water, less sure... use a thermometer to always monitor, that's your best approach.  What I did here is put sillicone on a cheap temp sensor bought in china and dip it in a few key spots... i.e reservoir temperature, exaust temperature, test various scenarios (cutting, engraving, short, long run) to make sure there's no over-cooling or over-heating.


---
**Don Kleinschnitz Jr.** *August 01, 2017 18:37*

**+Eric S** what was your worse case ambient and what temp could you get the coolant down to?


---
**Adrian Godwin** *August 01, 2017 21:28*

I got a fish-tank-cooler (for coldwater fish) for almost nothing on ebay. It didn't work. Minor electrical fault and no refrigerant.



I've just got it going, by refilling with a car refill kit and a can of pipe freezer spray (the car refrigerant bottles are hard to buy here at a reasonable price due to CFC laws .. but pipe freezer is the same stuff and is intended to be discharged into the atmosphere ..).



It got 5l of water down to 0c in about half an hour. No, I won't let it get that cold with the laser tube in circuit.



I did consider a mini-fridge, but I lost that auction and won this one. Although it would get the water plenty cold enough, I'm not sure if the thermal transfer from water jug to fridge via the air would be fast enough to get the laser's heat out. You might have to run it for a while with the laser off to get it back in range. It might be better to trickle the water over the fridge heat exchanger.






---
**Eric S** *August 01, 2017 23:38*

**+Don Kleinschnitz** actually, I never tried to see how low I could make it go but room temperature was between 20-25 degree  (winter/summer  -  Basement) and I've set the cooling to get the water to 13-15 degree so when cutting it would vary between 15 max 20, could have lowered the delta by adding more water or by dialing the cooling of the water unit further down, still had some room left there .  I'm using only about 1/2 of the tank.  And worst case, I could have used a water tank above it to add more damping but it wasn't needed for my setup.   I am currently moving everything in a hotter area (garage) so I'll see if everything still holds :)



One good thing about this water cooler setup is you don't have to pay for an expensive chiller, AND there's plenty of "junk" cooler on Kijiji (junk as in working extremely well but since not in stainless or whatever, they are trowing it away).  Mine was cheaper than a case of beer.  Easy to maintain and purge too.

![images/d5578fd9c44d13da674d994ff557e179.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5578fd9c44d13da674d994ff557e179.jpeg)


---
**Bill Keeter** *August 02, 2017 00:23*

**+Eric S** let me know how that setup works in the garage. That's where I run my K40. The water this summer has been sitting at 30c until I drop two or three frozen water bottles in there. 


---
**Don Kleinschnitz Jr.** *August 02, 2017 14:33*

I wish my shop was that cool a temp mine is running close to 40C  :(.


---
**Adrian Godwin** *August 03, 2017 19:57*

Move to england. Mine is at 18C without cooling.


---
**bbowley2** *August 05, 2017 16:42*

I stopped by a hydroponics store and asked about water chillers.  The guy showed me a 1/10 HPp chiller for $385.00.  A little high but he said he had a chill he wasn't using. It was a 1/4 HP chiller and he would take $200 for.  I checked the new price for this unit and it was over $600.  I'm plumbing it right now.  Used chillers are out there.  Ya just have to look for them. 


---
**Bill Keeter** *August 08, 2017 01:39*

**+Don Kleinschnitz** I had another idea. Close loop the tank of water but add a coil of copper to the line sitting in a second open tank. This one you can dump ice into. It'll chill the tank through the copper but not contaminate the laser water. Plus you can dump and refill as much as you want. 


---
**Jeff Bovee** *August 22, 2017 19:58*

I use a mini fridge.  I run the water through a CPU heat sink in the fridge and out to a 5 gallon bucket.  My setup is in the basement but I never go over 20c.



[amazon.com - Amazon.com: BXQINLENX 18 Pipe Aluminum Heat Exchanger Radiator for PC CPU CO2 Laser Water Cool System Computer 120mm(A): Computers & Accessories](https://www.amazon.com/gp/product/B01D81UPHA/ref=oh_aui_search_detailpage?ie=UTF8&psc=1) 




---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/hN284jVS8vs) &mdash; content and formatting may not be reliable*
