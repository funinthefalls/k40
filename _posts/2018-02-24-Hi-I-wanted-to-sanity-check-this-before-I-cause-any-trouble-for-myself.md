---
layout: post
title: "Hi. I wanted to sanity check this before I cause any trouble for myself"
date: February 24, 2018 19:02
category: "Hardware and Laser settings"
author: "Jake B"
---
Hi.  I wanted to sanity check this before I cause any trouble for myself.  Is there any reason that I couldn't use this display with the Cohesion3D mini:



[https://www.aliexpress.com/item/AZSMZ-12864-LCD-with-Big-SD-or-TF-Card-3D-printer-smart-controller-control-panel-For/32837222770.html?spm=a2g0s.9042311.0.0.O6HcOR](https://www.aliexpress.com/item/AZSMZ-12864-LCD-with-Big-SD-or-TF-Card-3D-printer-smart-controller-control-panel-For/32837222770.html?spm=a2g0s.9042311.0.0.O6HcOR)



Silkscreen seems to indicate AUX-2 and AUX-3 connectors rather than EXP-1 and EXP-2.  Not sure if they're equivalent.



Corollary question: is there a pinout for the GLCD adapter?  The one I found in gut hub seems to be different.









**"Jake B"**

---
---
**James Rivera** *February 24, 2018 19:56*

I can't answer your question, but I found it interesting that they included thingiverse links to cases people made for it. Very helpful, IMHO.


---
**Cris Hawkins** *February 24, 2018 20:21*

It looks like it will work. There will be some minor changes to the config file.


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2018 23:10*

Get me one and we'll figure it out :) 


---
**Jake B** *February 25, 2018 01:55*

I do have one here that I ordered to play with.  Not going to get to play with it for a few weeks though.  I think you're a few towns over, if you want to give it a try?



Looking at it, the pinout is different than the reprap GLCD, unfortunately.  It isn't a direct plug in.  Probably achievable with manually mapping the pins.



I do like it for its size, and it seems like it would have a better viewing angle than the RepRap GLCD.



Oh, and its got a place to solder an ESP-01 on the back.  Not sure what that's for, but sounds cool.


---
**Claudio Prezzi** *February 25, 2018 11:05*

I have one of those on my big 3d-printer. It was not working until I got a special version of repetier from the supplyer. I think they need to be driven a bit different than the normal GLCDs.


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/eSYjyKUn6Ee) &mdash; content and formatting may not be reliable*
