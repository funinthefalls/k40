---
layout: post
title: "If you are looking for a way to make a cheap and easy air assist coil hose, you can make your own"
date: August 31, 2015 03:13
category: "Modification"
author: "Joey Fitzpatrick"
---
If you are looking for a way to make a cheap and easy air assist coil hose,  you can make your own.  Buy some 4mm I.D / 6mm O.D. pneumatic hose off of ebay(or somewhere else)  Wrap the hose tightly around a small pipe and heat it with a heat gun.  After heating, immediately cool the hose with cold water.  The hose will hold it's shape indefinitely.  I also bought some 90 deg pneumatic fittings and a straight through hole fitting to make a clean looking install.  all parts cost less than $10 US

![images/f304578fa9a98ba8ce02d4ebbf58236b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f304578fa9a98ba8ce02d4ebbf58236b.jpeg)



**"Joey Fitzpatrick"**

---
---
**Flash Laser** *August 31, 2015 03:20*

thanks for your share!!


---
**Jim Root** *August 31, 2015 21:55*

Very nice!


---
*Imported from [Google+](https://plus.google.com/113216564983855026330/posts/eoVruaXwv52) &mdash; content and formatting may not be reliable*
