---
layout: post
title: "Well turns out the MKS Base 1.3 I bought ( Smoothie capable) is jacked.."
date: January 08, 2016 20:43
category: "Modification"
author: "David Cook"
---
Well turns out the MKS Base 1.3 I bought  ( Smoothie capable)  is jacked..   sO i just ordered a RAMPS 1.4 setup lol   cheap on ebay  [http://www.ebay.com/itm/252181241032](http://www.ebay.com/itm/252181241032)





**"David Cook"**

---
---
**David Cook** *January 08, 2016 21:25*

it's acting like it lost a Stepper driver somehow.  I'll send it to you if you want,  you can keep it if you fix it.  it's the V1.3 board




---
**David Cook** *January 08, 2016 21:48*

not a problem at all Peter,  I'll ship it out on Monday


---
**Anthony Bolgar** *January 08, 2016 21:48*

Peter, do you need a ramps setup as well? 


---
**Anthony Bolgar** *January 08, 2016 21:50*

David, if you need help with the upgrade, let me know, I have done mine and figured out all the little issues.


---
**David Cook** *January 08, 2016 21:53*

**+Anthony Bolgar** Thank you ,  I appreciate the help if needed


---
**Jim Hatch** *January 08, 2016 22:08*

**+David Cook**​ - is the new one you're getting Smoothie capable as well?


---
**Sebastian Szafran** *January 09, 2016 00:05*

**+Anthony Bolgar**​ I am in the middle of upgrading my K40 with Ramps upgrade. Your help would be greatly appreciated. 


---
**Anthony Bolgar** *January 09, 2016 00:10*

Sebastian, what are you having trouble with?


---
**Anthony Bolgar** *January 09, 2016 00:20*

Will smoothieware run on a ramps board?


---
**ChiRag Chaudhari** *January 09, 2016 09:18*

Ramps1.4 and #LaserWeb by **+Peter van der Walt** is the best thing happened to my K40. 



While waiting for the Ramps to arrive, get all the components for the middleman board. I actually soldered jumper cables to the ribbon cable, but say if I have to do it again, I will get the ribbon socket. Makes things easy if you ever decide to go back to the stock. (I dont thing anybody would do that!)



Also get some 22 AWG silicon wires for Laser Fire, PWM, and 5V input. They are soft and easy to route. I used regular wires for those three and end up changing it to all silicon wires.



Here are pics from older post. [https://plus.google.com/+ChiragChaudhari/posts/gW49XQXTqNi](https://plus.google.com/+ChiragChaudhari/posts/gW49XQXTqNi)


---
**Arestotle Thapa** *January 09, 2016 10:48*

**+David Cook** what are the benefits of switching to Ramps 1.4 (instead of smoothie)?


---
**Phil Willis** *January 09, 2016 13:51*

Does anyone make a kit or have a list of parts to convert the connectors used on the K40(LaserDRW) to ones suitable for the RAMPS boards. I would like to try the upgrade but not burn my bridges until I was happy with the conversion.


---
**Anthony Bolgar** *January 09, 2016 15:06*

Someone created a middleman board to plug the ribbon cable into so you don't have to modify the laser permanetly. I have a couple made up if you want one, just pay for the shipping to wherever you are located, board is free.


---
**David Cook** *January 09, 2016 16:56*

**+Arestotle Thapa** that I don't have to throw another hundred bucks at this thing :-)  that's the only advantage I can think of.  I was using the Azteeg X5 with smoothie ware until I shorted it out with the scope probe.. Then tried this MKS base 1.3 board from eBay.  It stopped working after it crashed a stepper motor. So less than $40 got me that Ramps kit from eBay.  Then I found the modified firmware for it that fires the laser with a low signal.  Looks very simple. I'm familiar with Ramps as that is what I used when I built my first custom 3d printer .  on my first laser power supply I unsoldered the the ribbon connector and use it as an adapter to wires back into the control board.   Everything else is ready to plug in.  I also have a Viki LCD I want to try and use from Panucat as well as an X3 board to try


---
**Arestotle Thapa** *January 09, 2016 17:50*

Thanks **+David Cook** . How is raster/engraving support in Ramps kit compared to smoothie compatible boards?


---
**Anthony Bolgar** *January 09, 2016 17:51*

Using Turnkey Tyranny's plugin for inkscape to generate the Gcode, ramps is excellent at raster engraving


---
**David Cook** *January 09, 2016 17:58*

yeah   looks like Ramps does Raster   [https://github.com/lansing-makers-network/buildlog-lasercutter-marlin](https://github.com/lansing-makers-network/buildlog-lasercutter-marlin)




---
**David Cook** *January 09, 2016 17:58*

**+Peter van der Walt**  once I make some cash with this laser  I'll be looking to get back into Smoothie




---
**Phil Willis** *January 09, 2016 20:52*

**+Anthony Bolgar** I am in the UK and I would be interested, if you can let me have a quote for the post and packing.


---
**Anthony Bolgar** *January 09, 2016 21:35*

I will get a quote to you on Sunday Phil.


---
**Anthony Bolgar** *January 09, 2016 21:38*

Just did a quick check for you online Phil. I could send it to you for $10 USD. 


---
**Phil Willis** *January 10, 2016 11:54*

PM me a paypal email address and I will send back my postal address. **+Anthony Bolgar** 


---
**Anthony Bolgar** *January 11, 2016 11:29*

Phil, my email is nfallsclockguy@hotmail.com


---
**Anthony Bolgar** *January 11, 2016 17:37*

Phil, can you email me your address please.


---
**Sebastian Szafran** *January 11, 2016 21:39*

**+Anthony Bolgar**​ Will you have one more middleman board available? I am also interested, shipping to Poland.

And regarding help, I will post my questions to you by mail. Thx a lot.


---
**Anthony Bolgar** *January 11, 2016 23:22*

I don't have any more boards made up right now. Sorry


---
**Sebastian Szafran** *January 11, 2016 23:24*

So I will have to make one ;)


---
**David Cook** *May 15, 2016 20:38*

**+Peter van der Walt**  Hey,  not to leave you hanging,  I have not shipped the board.  It turned out to be something wrong with the config file.  board works,  now ill swap out the ramps with it and start playing with Laser Web yay.   Thanks for your continuous development it looks great.


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/VeKtSZRSSWB) &mdash; content and formatting may not be reliable*
