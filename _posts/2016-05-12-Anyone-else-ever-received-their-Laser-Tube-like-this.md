---
layout: post
title: "Anyone else ever received their Laser Tube like this?"
date: May 12, 2016 07:51
category: "Hardware and Laser settings"
author: "Gunnar Stefansson"
---
Anyone else ever received their Laser Tube like this? 



Can I glue it myself? if 'Yes' what Glue?



Edited: Alright thanks people, I cleaned the old glue with Acetone and glued it back together with some 2comp epoxy. Fired it up and it works flawlessly. Didn't need to re-align anything and it cuts perfectly... 



![images/3e056d6dcce6c73acf95c6233cb67bee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e056d6dcce6c73acf95c6233cb67bee.jpeg)
![images/5c4acea17ab61786405c40dae6e285b4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5c4acea17ab61786405c40dae6e285b4.jpeg)

**"Gunnar Stefansson"**

---
---
**Daniel Wood** *May 12, 2016 08:10*

Mine broke off. I cleaned up the laser with acetone and used a small amount of epoxy to glue it back on. Its lasted 2 years.


---
**Gunnar Stefansson** *May 12, 2016 08:27*

**+Daniel Wood** Nice... thanks for the fast reply, it also looks like it's very do-able... I'll see if seller gives me a discount for doing it myself or replacement. before I start doing it.


---
**I Laser** *May 12, 2016 10:08*

Ouch! Just got my replacement, thank goodness it was in one piece.



Many moons ago there were numerous posts on this forum where people had the same issue. Back then everyone said the tube was stuffed and you'd have to replace it. Interesting to see it is salvageable.


---
**Gunnar Stefansson** *May 12, 2016 10:13*

**+I Laser** Well good for you :D

Seller gives me 10€ discount and says to use AB Glue to reconnect... AB glue is apparently just 2component epoxy... so **+Daniel Wood** was correct... Thx


---
**Mike Mauter** *May 13, 2016 03:10*

When my replacement tube came in I accidentally broke the water inlet off. I repaired it with epoxy and silicon tape. i was wondering if there was any way to get that end off the old tube and replace the broken one but couldnt figure a way to get it off.


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/brAoPC1faH1) &mdash; content and formatting may not be reliable*
