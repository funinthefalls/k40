---
layout: post
title: "Has anyone experimented on engraving cork?"
date: May 06, 2016 06:28
category: "Materials and settings"
author: "Alex Krause"
---
Has anyone experimented on engraving cork?





**"Alex Krause"**

---
---
**Anthony Bolgar** *May 06, 2016 06:38*

Cork engraves well, just make sure you are running an air assist nozzle, cork catches on fire very easily, and the air assist keeps the flames down. Also, you need very little power and can run at fairly fast feed rates.


---
**Jean-Baptiste Passant** *May 06, 2016 06:43*

Be careful with cork, as **+Anthony Bolgar** said, it catches fire easily. High speed and low power will do the trick. We should probably do a list of recommendations / Experiences with some materials.


---
**Alex Krause** *May 06, 2016 06:44*

Link for the best air assist head out there? I need to set up my k40 tomorrow finish a wedding present accept a package for my new CNC  mill frame and order accessories for the K40 (flow switch, middle man board ,air assist, look at lenses and mirrors) it's a fun filled day


---
**Alex Krause** *May 06, 2016 06:46*

Could cork be soaked in water before hand to prevent flair ups along with an air assist


---
**Anthony Bolgar** *May 06, 2016 06:50*

Not sure, I just have a feeling that since the cork will expand when wet,once dry the engraving will be off when it shrinks, and also not sure if steam buildup will affect the cork, might be possible that it cause small pieces to blow off. But that being said, I can't see any problems with trying a test of it to see how it reacts when wet.


---
**Jean-Baptiste Passant** *May 06, 2016 07:01*

**+Alex Krause** Send me your adress, I'll get you a MiddleMan board free ;), got 3 and used one so ...


---
**Alex Krause** *May 06, 2016 07:02*

**+Jean-Baptiste Passant**​ I'm based in the USA will that be a problem?


---
**Jean-Baptiste Passant** *May 06, 2016 07:05*

**+Alex Krause** Not a problem ;)


---
**Anthony Bolgar** *May 06, 2016 07:07*

LightObjects has a great air assist head for $18.50. You just need to buy an 18mm lens to go with it, otherwise it is a drop in replacement for the K40 head. It is all metal, will not melt like 3D printed heads.


---
**Mishko Mishko** *May 06, 2016 07:50*

You can't really soak cork in any liquid, it doesn't absorb liquids, so you'll be left with just a film of water on the surface at best... Youtube is full of cork engraving videos,  it seems to em it engraves quite well, with some soot an the edges, but it seems that can be washed off with water, too, at least some of it...


---
**Jean-Baptiste Passant** *May 06, 2016 09:41*

**+Mishko Mishko** Well, cork can absorb but not at the same rate as another material.



If the cork is old or has been dryed, heated or anything, small cracks may appear.



If you open an old wine bottle that have been stocked horizontally, the cork will be wine colored, and if you open it you can see the color is not only on the surface but also inside (never seen more than a mm, and even that seems extreme). Cork also leave some particules in the drink (and give a cork taste to it) and that's why wine bottles now use artificial cork or plastic.



So trying to put some water on it will probably do nothing, but leaving it to soak for X days can get the water in.



Hard to do but not impossible. Also, a pita to get it to dry after that.


---
**Jim Hatch** *May 06, 2016 19:30*

**+Jean-Baptiste Passant** Can I have a board too? If so, I'll have some bits & pieces (connectors) I was going to use to build one that I'll pass on to anyone here who needs them. If you won't take $ for the board, would you at least take it for postage? Or allow a Paypal to be sent to cover a glass of wine or a pint?


---
**Jean-Baptiste Passant** *May 06, 2016 20:58*

**+Jim Hatch** And you get a free board too :)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/9J26BfaQGrZ) &mdash; content and formatting may not be reliable*
