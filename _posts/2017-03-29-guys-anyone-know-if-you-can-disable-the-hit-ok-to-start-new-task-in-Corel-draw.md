---
layout: post
title: "guys anyone know if you can disable the \"hit ok to start new task\" in Corel draw?"
date: March 29, 2017 02:56
category: "Software"
author: "Chris Hurley"
---
guys anyone know if you can disable the "hit ok to start new task" in Corel draw? I want to be able to set it up to do 5 or 6 cuts without having to babysit it.





**"Chris Hurley"**

---
---
**Ned Hill** *March 29, 2017 03:16*

I think Method needs to be "All layers"

![images/253e1ba22d143f2f21daaeab1d3b90a1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/253e1ba22d143f2f21daaeab1d3b90a1.png)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 29, 2017 07:44*

Pretty much the only way  I managed (when I was on stock setup) was to set it up by clicking "Add Task" with the "Starting" checkbox disabled. Add all the required tasks to the queue. Then you click the "Play" button on the plugin system-tray right click menu. This allowed me to add the engrave to the queue, inside cuts to the queue, then outside cuts to the queue as 3 operations & then they would run in the order they were added to the queue.


---
**Phillip Conroy** *March 29, 2017 08:35*

Not a good idea to leave any laser machine as can catch fire 


---
**Mark Brown** *March 29, 2017 11:50*

Not sure if I'd leave it completely unattended.  But if you want you can "add task"s one at a time, then on the last one check "starting" and hit add task.


---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/2Ad44wfFT17) &mdash; content and formatting may not be reliable*
