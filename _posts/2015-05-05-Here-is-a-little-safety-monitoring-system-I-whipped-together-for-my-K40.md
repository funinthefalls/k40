---
layout: post
title: "Here is a little safety monitoring system I whipped together for my K40"
date: May 05, 2015 17:41
category: "External links&#x3a; Blog, forum, etc"
author: "Jon Bruno"
---
Here is a little safety monitoring system I whipped together for my K40. with a few tweaks to the panel David Wakely put together this might work out nicely.



[https://www.dropbox.com/s/6389gfotk4aa6kt/LaserSafetySystem.zip?dl=0](https://www.dropbox.com/s/6389gfotk4aa6kt/LaserSafetySystem.zip?dl=0)





**"Jon Bruno"**

---
---
**Mauro Manco (Exilaus)** *May 05, 2015 18:58*

i think this expensive .... currenly i order lcd temperature([http://it.aliexpress.com/item/1PC-New-Mini-Digital-LCD-Thermometer-Temperature-Sensor-Fridge-Freezer-Thermometer-free-shipping/32328735040.html](http://it.aliexpress.com/item/1PC-New-Mini-Digital-LCD-Thermometer-Temperature-Sensor-Fridge-Freezer-Thermometer-free-shipping/32328735040.html)) and try to hack for add it in loop for stop laser(button and door switch)... soon my result. have a sense add uno or mega on laser only want change all brain and automate all.


---
**Jon Bruno** *May 05, 2015 19:00*

That is a good deal on a thermometer for sure.

This setup is more of a "monitoring system" than just a thermometer.


---
**Stuart Middleton** *May 05, 2015 19:04*

That's amazing! I've literally just designed this exact system today! That saved me a lot of time and effort! Thanks! :)



What sensor are you using for flow? I'm just about to order one. I've got the switches, LCD and temp probe on order.


---
**Jon Bruno** *May 05, 2015 19:07*

HAH! Glad to be be able to help!

...and I fixed the spelling of "IMMEDIATE"



Any of the small 1/2" port pinwheel style meters will do.

 I ordered this one from Amazon: [http://www.amazon.com/gp/product/B00HG7G2GG/](http://www.amazon.com/gp/product/B00HG7G2GG/)



As long as there is an event < 3 seconds apart it shows as flowing. this of course is adjustable. 


---
**Stuart Middleton** *May 05, 2015 22:34*

Ah, excellent, the one I'm looking at is very similar specs. Should do just fine.

I'm upgrading to a RAMPS 1.4 board in the next month and was thinking of adding the functionality to the firmware, although it'd probably be easier to keep it as a seperate module. Especially since I don't need to write any code any more :)


---
**Jon Bruno** *May 05, 2015 22:58*

Cool. I'll be anxiously watching for your build.


---
**Stuart Middleton** *May 06, 2015 13:36*

I do plan to write up the process as I know it's a mod a lot of people are considering. I'll share the link when I've got it all documented.


---
**Mauro Manco (Exilaus)** *May 06, 2015 13:51*

Confirm , we have different approch but safetely before all.


---
**David Wakely** *May 06, 2015 17:15*

This looks awesome. Puts my design to shame!


---
**Jon Bruno** *May 06, 2015 18:57*

Not really David.. Yours is a good solid setup for sure.

Mine just takes it a few steps further to shut things down if you open the door or if something goes wrong.



It's just a little safety net for the tube if for some reason you don't notice the light. .. 

I may actually change the temp sensor to a DS18S20 instead of the TMP36 I originally wrote it for. they seem easier to get and are available in waterproof setups.

I also like that they can be used with parasitic power which eliminates one more wire from the mess.


---
**David Wakely** *May 06, 2015 22:37*

How have you made it kill the power to the tube, i'm guessing you are tapping into the laser switch line on the front panel and attaching it to an arduino pin to make/ break connection?


---
**Jon Bruno** *May 06, 2015 22:40*

The safety switch circuit runs through a relay. Or....you could try to gnd the enable directly with the arduino but I  wouldn't do it that way.


---
**Jon Bruno** *May 08, 2015 19:31*

if anyone downloaded this project I made a small change.

to fix the animation of the flow meter I had to change a few things in the FlowSensor section.

it's a real simple thing, I needed to slow down the advancement of the arrow so it ticks over once every 4 rotations of the pinwheel.

You can just copy and paste over the section with this:





void FlowSensor() //coolant flow animation

{

    if (displayalarm == 0)

    {

        lcd.setCursor(4, 0);

        lcd.print("Coolant Flow");

        lcd.setCursor(0,2);

        lcd.print(" ");

        lcd.setCursor(2,1);

        lcd.print("[");

        lcd.setCursor(17,1);

        lcd.print("]");

        if (pulses >48)

        {

            pulses=0;

            lcd.setCursor(3,1);

            lcd.print("  ");

        }

        else

        {

            lcd.setCursor((pulses)/4 +3 ,1);

            lcd.print("->");

        }

    }

}


---
**Jon Bruno** *May 08, 2015 19:33*

I still may need to include a debounce because the one i have here is glitching the animation. But hey, as long as there are events we know the water is flowing...


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/1iHXXZ5yKyk) &mdash; content and formatting may not be reliable*
