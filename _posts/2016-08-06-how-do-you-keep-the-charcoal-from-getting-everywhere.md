---
layout: post
title: "how do you keep the charcoal from getting everywhere?"
date: August 06, 2016 16:31
category: "Discussion"
author: "David Spencer"
---
how do you keep the charcoal from getting everywhere?





**"David Spencer"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 06, 2016 16:38*

Air-assist minimises it, as does masking the area off with painter's tape.


---
**Jim Hatch** *August 06, 2016 16:42*

Also lower power or faster speed so you're cutting through but not over cutting which lets it burn more. You should try using the lowest power& highest speed whenever you're cutting.


---
**David Spencer** *August 06, 2016 16:52*

I meant after you cut and are handling parts


---
**David Cook** *August 06, 2016 16:58*

Need a small portable vacuum or dust buster. 


---
**Jim Hatch** *August 06, 2016 17:08*

**+David Spencer**​ you mean off your hands? If you cut low & fast you'll minimize the charcoal. Also a wipe with isopropyl alcohol (rubbing alcohol) will help. 



Different woods have different amounts. Birch ply seems to have more than solid woods too.


---
**greg greene** *August 06, 2016 18:34*

alcohol swabs


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/8aY4r8NYQ2c) &mdash; content and formatting may not be reliable*
