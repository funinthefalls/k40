---
layout: post
title: "Hi peopz, really glad I joined this group lot's of cool stuff here, so I just wanted to make my first share"
date: April 24, 2016 20:54
category: "Object produced with laser"
author: "Gunnar Stefansson"
---
Hi peopz, really glad I joined this group lot's of cool stuff here, so I just wanted to make my first share. 



I took this idea from someone else online, don't know who as it was a just a google image search, used Inkscape to trace bitmap, exported as .DXF and opened with Cambam and after designing created the gcode. 



I changed the font and added some Carbon Fibre Vinyl to really make it Pop, it turned out ok, I would have wanted some black nuts but didn't have any and used 3mm mdf plates.



![images/dd38caf0c45a5907f0e93f05eb6bb584.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dd38caf0c45a5907f0e93f05eb6bb584.jpeg)
![images/4ee9e8e960a4e519c4df9294f2bcee3b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4ee9e8e960a4e519c4df9294f2bcee3b.jpeg)

**"Gunnar Stefansson"**

---
---
**3D Laser** *April 25, 2016 01:07*

Where did you get the carbon fiber 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 25, 2016 01:31*

It looks really cool. I think gold/brass nuts would be nicer than black nuts, as they would complement the gold colour of the Vader cutout. I like the idea to use vinyl on top for your colouring. Really nice work.


---
**Brien Watson** *April 25, 2016 01:36*

You used carbon fibre in your laser?  Very, very brave man...


---
**3D Laser** *April 25, 2016 02:08*

**+Brien Watson** whys that brien


---
**Gunnar Stefansson** *April 25, 2016 10:58*

**+Corey Budwine** I bought the Carbon Fibre Vinyl on Ebay.


---
**Gunnar Stefansson** *April 25, 2016 10:59*

**+Yuusuf Sallahuddin** Thanks, and yes brass nuts would definetly be the thing todo especially with the golden carbon vinyl... I'll look into that :)


---
**Gunnar Stefansson** *April 25, 2016 11:00*

**+Brien Watson** I used some Cabon Fibre Vinyl ontop of the mdf plate before cutting. It's not actual carbon fibre it's just plastic texture ;)


---
**Bill Parker** *April 25, 2016 18:58*

Does carbon fibre not cause particles of carbon that could damage electronics? 


---
**Brien Watson** *April 26, 2016 02:05*

Not only that, but I thought carbon fibre gave off toxic fumes like PVC does.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 26, 2016 02:27*

There's not actually a great deal of electronics in the actual cutting/engraving area. Except a small circuit board on the Y-rail near the stepper. If your exhaust system was decent enough, wouldn't most of the carbon particles by extracted?



Also, if carbon fibre gives off carbon particles, wouldn't it be the same for wood, as wood is high in carbon content?


---
*Imported from [Google+](https://plus.google.com/100294204120049700616/posts/aBKnTH8vbXG) &mdash; content and formatting may not be reliable*
