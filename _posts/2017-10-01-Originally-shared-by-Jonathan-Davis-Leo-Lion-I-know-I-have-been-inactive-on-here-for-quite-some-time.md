---
layout: post
title: "Originally shared by Jonathan Davis (Leo Lion) I know I have been inactive on here for quite some time"
date: October 01, 2017 15:40
category: "External links&#x3a; Blog, forum, etc"
author: "Jonathan Davis (Leo Lion)"
---
<b>Originally shared by Jonathan Davis (Leo Lion)</b>



I know I have been inactive on here for quite some time. But I am considering the prospect of building a custom 3D printer with a large print area to produce large objects such as costume head bases. With that said I do not know where to begin and nor do I know what parts I will need? So a bit of help from those who have worked and designed such machines would be nice and slection of tools are quite limited and i am wanting to keep it affordable. 





**"Jonathan Davis (Leo Lion)"**

---
---
**HalfNormal** *October 02, 2017 01:53*

Lots of information out there for doing large scale 3D printing. Unfortunately it does not come cheap and you need to be fairly good at  both mechanical and electronics to make everything work together. The problem is that the everything gets more complex as it scales up.


---
*Imported from [Google+](https://plus.google.com/+JonathanDavisLeo-Lion/posts/F8UFCPshPm2) &mdash; content and formatting may not be reliable*
