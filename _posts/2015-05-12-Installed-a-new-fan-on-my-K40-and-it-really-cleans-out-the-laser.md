---
layout: post
title: "Installed a new fan on my K40 and it really cleans out the laser"
date: May 12, 2015 21:15
category: "Modification"
author: "Jim Coogan"
---
Installed a new fan on my K40 and it really cleans out the laser.  Quiet and came with a speed controller.

![images/31cd137afbf0e5930a362e2516d74d65.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/31cd137afbf0e5930a362e2516d74d65.jpeg)



**"Jim Coogan"**

---
---
**K Thrash** *May 13, 2015 15:01*

is that this one - [http://www.amazon.com/VenTech-Inline-Exhaust-Variable-Controller/dp/B005GJ7TFE](http://www.amazon.com/VenTech-Inline-Exhaust-Variable-Controller/dp/B005GJ7TFE)



I am looking to do the same thing. I am going to try to use a single unit for 2 machines and thinking of going with the 6" one. Are you running carbon?


---
*Imported from [Google+](https://plus.google.com/+JimCoogan_CoogansWorkshop/posts/guHJ5tnhRC2) &mdash; content and formatting may not be reliable*
