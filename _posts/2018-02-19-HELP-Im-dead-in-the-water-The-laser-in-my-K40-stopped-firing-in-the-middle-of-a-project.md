---
layout: post
title: "HELP! I'm \"dead in the water.\" The laser in my K40 stopped firing in the middle of a project"
date: February 19, 2018 13:43
category: "Discussion"
author: "Joel Brondos"
---
HELP! I'm "dead in the water."



The laser in my K40 stopped firing in the middle of a project. I had noticed that it wasn't cutting very efficiently and had turned the current up -- and the temps showing on my CW-3000 industrial chiller was up about 4 degrees.



Now, after letting it sit overnight, I turn the main power off and on, click the laser switch off and on, and try the Test Switch, but the laser does not fire -- and the analog amp meter does not register ANY current.



What does this mean? Did I fry the laser tube . . . or blow something else?



If I need a replacement laser tube, does anyone have any suggestions or warnings?





**"Joel Brondos"**

---
---
**Don Kleinschnitz Jr.** *February 19, 2018 16:19*

Does the laser fire if you press the "Test" switch down on the LPS?




---
**Joel Brondos** *February 19, 2018 16:23*

**+Don Kleinschnitz** The laser does not fire and the analog mA current meter does not register ANY current. I was just looking at your blog post recommended by TechBravoTN. I am not an electrician but feel comfortable swapping out computer boards, etc. I really appreciate your help.



The power supply seems to be a "green" one - all green connectors with the pattern of 4 - 6 - 4 connectors.



I think it might be safest if I just swap out the entire power supply - can you recommend a unit and supplier?




---
**Don Kleinschnitz Jr.** *February 19, 2018 16:38*

**+Joel Brondos** does it <b>not</b> fire using the red button down on the supply? I only ask because that button bypasses all the circuits in your machine and helps eliminate other than LPS or tube problems.



I don't have a particular vendor to recommend. These supplies have a high failure rate (literally consumable) so I recommend finding the cheapest supply on amazon or eBay that is a replacement so you do not have to rewire. 



<b>Other questions</b>

Where do you live? 

How old is the LPS?

How much do you use the machine?

What kind of coolant are you using?

![images/deb751201b9e6a2f0f8bcdb9c294294e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/deb751201b9e6a2f0f8bcdb9c294294e.jpeg)


---
**Chuck Comito** *February 19, 2018 17:13*

You would not see current if the tube stopped conducting as the ma meter passes through it or the power supply is dead.. I'd check your wiring first. Make sure all connections are good especially those to the laser test fire and laser enable switch. I doubt your tube died suddenly though. They tend to weaken first but they will still pass current and register on the meter. Also make sure You have a solid connection on the anode and cathode of the laser tube.  Be very careful as this is extremely high voltage. 


---
**Don Kleinschnitz Jr.** *February 19, 2018 20:41*

**+Chuck Comito** FYI: the test button down on the LPS bypasses all the external wiring. If that fires the laser then the external wiring etc may be suspect if not then likely the supply is dead.


---
**Joel Brondos** *February 19, 2018 22:58*

So, I live in the Chicago area,  my LPS is about 4 months old, I use it several times a week. I'm using distilled water through my CW-3000 industrial chiller (and just learned from BrovoTechTN to include several drops of Dawn detergent and some algae treatment . . . I did notice inside of tubes getting a little gray.)



I am thinking about replacing the power supply with this from Amazon:



Wisamic 40W CO2 Laser Power Supply for Laser Engraving Cutting Machine 

 

Link: [http://a.co/a6q60fS](http://a.co/a6q60fS)



Or this from LightObject



JLD40WPS 40W CO2 Power Supply - 45Watt (LSR-JN40W-1V)



[lightobject.com - 20W~45W PWM CO2 Laser Power Supply (AC110V)](http://www.lightobject.com/20W45W-PWM-CO2-Laser-Power-Supply-AC110V-P71.aspx)



But the LightObject does not have the same pin-out connections . . . no terminals to power the Cohesion3D Mini board (and we CAN'T have THAT!).






---
**Joel Brondos** *February 19, 2018 23:17*

**+Don Kleinschnitz** Ohhhh, you mean the TEST fire button on the CIRCUIT board. I thought you meant the test fire button on the external case panel. And of course, that IS what you actually said: "down on the supply."



I had started to remove the power supply from the case, but it is still all connected. Is it safe to power it up if it is inside the case and resting on the bottom? Then, I will try the TEST button on the power supply circuit board.






---
**Don Kleinschnitz Jr.** *February 20, 2018 02:17*

**+Joel Brondos** install at least one screw so we insure it's grounded. Then you should be ok.


---
**Joel Brondos** *February 20, 2018 04:19*

**+Don Kleinschnitz** After reinstalling one screw (my wife thanks you that she didn't become a laser widow), I depressed the TEST button on the power supply circuit board. The laser did not fire and the meter did not indicate any current. There is a green LED on the power supply circuit board which is lit though.


---
**Don Kleinschnitz Jr.** *February 20, 2018 04:30*

**+Joel Brondos** My guess is that at least you have a bad supply (not uncommon). I will look at your choices above in the AM and respond.


---
**Joe Alexander** *February 20, 2018 08:37*

I have had this issue a couple times now and so far the culprit has been the LPS going out. Considering it usually only costs ~$70 for one its a cheaper fix than getting a new tube first.


---
**Don Kleinschnitz Jr.** *February 20, 2018 11:15*

**+Joel Brondos** 

Power supply: the Wisamic model is the supply I would go with as that will not require any rewiring and the addition of a DC supply. It looks unavailable?



Here is another: [amazon.com - Cloudray 40W PSU Laser Power Supply 110V/220V for CO2 Laser Engraving Cutting Machine MYJG 40W - - Amazon.com](https://www.amazon.com/Cloudray-Supply-Engraving-Cutting-Machine/dp/B079MHKQXF/ref=sr_1_6?s=hi&ie=UTF8&qid=1519124482&sr=1-6&keywords=40+watt+laser+power+supply)



You can do other searches for better deals, found this one on ebay:

[https://www.ebay.com/itm/40W-Power-Supply-Mini-CO2-Laser-Rubber-Stamp-Engraver-Cutter-Engraving-110-220V/202163129374?epid=5012180290&hash=item2f11dc8c1e:g:wYcAAOSwAaJaRG6n](https://www.ebay.com/itm/40W-Power-Supply-Mini-CO2-Laser-Rubber-Stamp-Engraver-Cutter-Engraving-110-220V/202163129374?epid=5012180290&hash=item2f11dc8c1e:g:wYcAAOSwAaJaRG6n)



Make sure the labels of the signals on yours and the replacement are the same. Some supplies have the same color plugs but different labels.



...

As far as water treatment I have not found the detergent in the water to be beneficial (bubble elimination) and I do not want to mess with the coolant chemistry so I use straight distilled and a couple drops of aquarium algecide. There is a post on this on my blog about this.



...

Not to be an ambulance chaser ... but <b>I do accept donations of dead laser supplies for testing and failure analysis</b>. So if you want to donate it to the bone pile, the community would be grateful.




---
**Joel Brondos** *February 21, 2018 03:45*

**+Don Kleinschnitz** 1) Do you think all of these power supplies are made by the same company and different vendors just put their name on them?



2) Is there any way to tell whether a power supply is made from quality parts?



I <i>could</i> donate the "dead" power supply to the bone pile, but I'm also contemplating seeing if I can just swap out the flyback transformer . . . and then I'd have a spare power supply on hand. Have you ever tried that? 



Flyback transformer for 40W Laser power supply:

[amazon.com - Amazon.com: New High Voltage Flyback Transformer For 40W Co2 Laser Power Supply: Computers & Accessories](http://a.co/96AAKCe)


---
**Don Kleinschnitz Jr.** *February 21, 2018 12:52*

**+Joel Brondos** 



1.) I think they are all made by a few companies but mostly the same design.



2.) I have dissected many of these supplies with the intention of finding out how and why they fail. In most cases the HVT fails. My theory is that the HV diodes that are potted in the transformer assy open up. Why, is unknown at this point. There is no way to know this for sure and no good way to test HVT's that I have found. A good and bad HVT diode will both measure as open since the required forward voltage is very high.



Yes you can replace the HVT my blog has lots of information including parts and procedures. Make sure you get the right one as there are a few different types of connections. You can only tell by removing the one your have.



<b>REPAIRING A LPS CAN BE LETHAL, so you do so at your own risk</b>



Let me know if you replace the HVT and if that was the problem.



Here is a index search of my blog.





[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Laser%20Power%20Supply)








---
**Joel Brondos** *February 22, 2018 01:04*

Well, I guess I'd rather be safe than sorry. And since a little knowledge is a dangerous thing, I probably won't try replacing the flyback on my own. I don't want to be a candidate for The Darwin Awards ([http://www.darwinawards.com/](http://www.darwinawards.com/)).



Now, I just got my replacement unit this evening. See the picture for the way the "factory" connected the power lead to the laser tube.



I'm not sure what I'd find under all that silicon or whatever it is. Would it be inadvisable to cut the wire a couple of inches away from that connector and use a wire nut to attach the new power lead to the laser?





![images/514dccad5f4acdd902a4cb8accae599a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/514dccad5f4acdd902a4cb8accae599a.jpeg)


---
**Joe Alexander** *February 22, 2018 02:02*

its best to keep the wire intact, or buy a ceramic connector that is rated for this amount of power. search "co2 laser tube high voltage connector" and they will pop up. and under that silicon i would bet that the wire is just wrapped around the post a couple times. easy to redo and re-dope with silicon. dont forget to slide a piece of tubing on the wire before you wrap it :P


---
**Joel Brondos** *February 22, 2018 02:12*

**+Joe Alexander** Yes, there was a piece of tubing over the silicon glob.



The ceramic connector is a better idea, though: a worthwhile upgrade. I really appreciate the advice. Thanks.



The silicon way . . . 
{% include youtubePlayer.html id="t2jjYRTHU-I" %}
[https://youtu.be/t2jjYRTHU-I?t=7m32s](https://youtu.be/t2jjYRTHU-I?t=7m32s)



This guy used heat shrink tubes: 
{% include youtubePlayer.html id="jodMkeNP0S0" %}
[https://youtu.be/jodMkeNP0S0](https://youtu.be/jodMkeNP0S0)



I don't have the guts to try what this guy did: 
{% include youtubePlayer.html id="nnF9X42lnaM" %}
[https://youtu.be/nnF9X42lnaM](https://youtu.be/nnF9X42lnaM)






---
**Don Kleinschnitz Jr.** *February 22, 2018 04:04*

There is LPS installation information on my blog.



It seems counter-intuitive that wrapping a wire is sufficient but at these voltages it will work fine. The key thing is not to leave sharp or pointy ends or edges as they will create corona effects. Use the right silicon (also on blog) and reuse the factory sleeve. Re-attach it more or less like you took it off.



Let us know if the new supply fixes your problem?


---
**Don Kleinschnitz Jr.** *February 24, 2018 15:00*

**+Don Kleinschnitz** **+Joel Brondos** do you still need some help? Looking for the post you mentioned in the PM?


---
**Joel Brondos** *February 26, 2018 03:54*

**+Don Kleinschnitz** I think I've got all my questions answered as far as the hardware is concerned, thanks. But it always seems like there's one thing more to fix. Still, I'm learning a LOT.




---
**Adam J** *May 23, 2018 00:50*

**+Joel Brondos**, was it the Power Supply in the end and not the tube?


---
*Imported from [Google+](https://plus.google.com/112547372368821461862/posts/2z9E3ZnE22C) &mdash; content and formatting may not be reliable*
