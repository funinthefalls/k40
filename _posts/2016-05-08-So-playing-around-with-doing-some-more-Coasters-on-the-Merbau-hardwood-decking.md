---
layout: post
title: "So, playing around with doing some more Coasters on the Merbau hardwood (decking)"
date: May 08, 2016 00:14
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, playing around with doing some more Coasters on the Merbau hardwood (decking).



As it's Mother's Day today, I decided I would give Mum something Australian & sturdy for when she has guests over for drinks. So, here is a set of 6 coasters each engraved with 1 of the 6 Australian Coins imagery.



Unfortunately on the 5c piece (bottom right) there is a line running down the centre from the top. This shouldn't have been there but actually should have been at the far right of that coaster (as a guide line for cutting to size with the drop-saw). What caused it is that my air-assist hose must have got tangled on something & wouldn't allow the laser head carriage to move further to the right. Luckily I noticed before it did too much more & hit the Laser Power off button.



All-in-all, I'm pretty satisfied with the results. Was 2 separate layers cut for each image. 1st layer @ 4mA @ 20mm/s. 2nd layer @ 10mA @ 20mm/s. This was to provide a little bit of gradient into the imagery.



Took about 1 hour 45 minutes all up to complete. In the photo, it might look a little dark around the cuts, but that's water that is still drying from when I cleaned up all the soot with nail brush.



 #mothersday2016   #mothersdaygift  

![images/28b1142ffce750f418aa28720f4a78e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/28b1142ffce750f418aa28720f4a78e0.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**I Laser** *May 08, 2016 00:16*

Nice work! Very cool, where'd you get the artwork for the coins?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 00:28*

**+I Laser** Royal Australian Mint, took their zoom images from their coins on this page:

[https://eshop.ramint.gov.au/2016-Uncirculated-Year-Set/310668.aspx](https://eshop.ramint.gov.au/2016-Uncirculated-Year-Set/310668.aspx) & then I put into Adobe Illustrator & Live Traced them (3 colours). Removed the lightest colour, changed the other two colours to black (as separate groups) then I used my vertical rectangle vectoring method to create the engrave files.


---
**I Laser** *May 08, 2016 00:34*

Ha very nice...



Has that decking got the ridges on the bottom or did you sand them off?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 00:38*

**+I Laser** Yeah got the rounded ridges on the bottom. Got the decking at Bunnings. ~$5/linear metre. Worked out $8.25 for a 1.8m length. Used about 600mm worth doing this set (so only $2.75 worth). But many hours of design/lasering (mostly due to the having to wait 10+ mins for the dialog windows to appear because my design was too complex).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 00:58*

**+I Laser** Here's svgs of the coins if you want them (after I traced/etc, but without the vector rectangling). [https://drive.google.com/folderview?id=0Bzi2h1k_udXwajBvWTkzT2hsNzg&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwajBvWTkzT2hsNzg&usp=sharing)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 01:29*

**+Susan Moroney** Thanks Susan. Mum seemed quite happy with them.


---
**Scott Marshall** *May 08, 2016 01:55*

Wow, Nice. You put some time in those, I'd imagine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 02:39*

**+Scott Marshall** Nearly 2 hours vector engraving the 2 layers per piece. If I had used standard engrave, I'd probably see you all next week lol.


---
**Scott Marshall** *May 08, 2016 03:27*

Good job getting that to work thru the Corel platform, I tried quite a few converters to get .svg files to work, but never succeded. Now I'm going Smoothie, so I have to start over learning that. My modern software handicap is a pain, I still don't get exactly how Github works, it's file sharing system seems similar to Googles, but I don't get it (Yet). Just need to spend time working with both.



Halfnormal is re-posting my M2Nano information in a more readable format, want to make sure yo don't too. 

Thanks for the help. I've always been a hardware guy, but that's not even an option these days. I will catch up, and appreciate the help in the meantime. 



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 04:14*

**+Scott Marshall** I couldnt get SVG to work with CorelDraw 12 that came with the k40. But with x5 it works fine. No issues with it.



I haven't got around to looking at anything yet for the website (or emails) but will do so in the next few days. I will keep in mind that HalfNormal is posting your info & won't redo it. Thanks for mentioning.



I've never been really an anything guy. More of a bit of this bit of that sort of fella. Although of late, my skills seems to be more in design than anything else (graphics, artwork, patterns for sewing, etc).


---
**Scott Marshall** *May 08, 2016 06:49*

**+Yuusuf Sallahuddin**



You sure do show a talent for the design, and sure work hard at it, You pump out so many projects, one would think you  have a couple of people working for you.



I'd think you would be selling a ton of the phone cases and similar stuff by now. I have done some coasters, but used birch ply and did a deco-pour (epoxy) finish. The problem is it hides the depth. Yours look great, never heard of that wood before, must be an exclusively Australian variety? It looks like Oak grain with a Teak coloring, and oiliness. Merbau must be common there?  I'll be looking for some, but I'm sure I won't find it at OUR lumberyard.



I often visualize what I want to happen, but can't even draw a decent stick figure. Give me Autocad and I can render a device down to the last thread. I have a hard time stopping. I've just learned to adopt it as my style. I do custom T shirts (thinking of doing one for here) and just draw in the line art style. Sometimes it works,  sometimes not.



I find you're usually your own worst critic. What looks iffy to me often is very popular.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 07:14*

**+Susan Moroney** Just random things for myself (e.g. pants/shorts/bags/cases/etc.). Although lately I've been having a go at eco-fabric planters (I have some listed on my etsy for sale). Never done any quilting as I rarely use a quilt (doesn't get cold enough) & I have no idea how. One of my friend's mother does a lot of quilting. That's a great idea to make your own quilter's templates. A guy I know suggested I make those sort of things when I first purchased my laser, but like a lot of things, I never got around to it.



**+Scott Marshall** It's funny you mention that, but I don't sell anything mostly, although I would like to. I use to sell a lot of handmade stuff (tobacco pouches & wallets) but have had enough of spending hours on a piece to make $10 profit. Not worth all the effort I put into it. So I've just been playing around with different ideas to see what is cheap (materials), easy (effort) & efficient (time) to try find something that will work as a viable income product.



I didn't think Merbau was an Australian wood, however a quick google gives me the origins. ([http://www.woodsolutions.com.au/Wood-Species/merbau](http://www.woodsolutions.com.au/Wood-Species/merbau)). According to that, it is a South-East Asian hardwood. You may have to look for it by different name too (Other Names: Kwila, Ipil, Vesi, Johnstone River Teak or Scrub Mahogany). Either way, it is a nice wood in my opinion, but I'd hazard a guess any nice hardwood would give you similar effect as these did.



I have all these amazing ideas for artwork/things I want to do & 90% of the time I am held up by my lack of skill. Give me a pen & paper, it will look horrible. Give me a mouse or touchpad & I can probably get it pretty spot-on lol. I used to do custom t-shirts, but my only customer was myself (& family). Made a heap for a fishing competition with our team logo on it & sponsor (family member owned a local fishing shop in the town of the competition). What method do you use for the t-shirts? I used vinyl heatpress, but always wanted to get a sublimation setup. Just lack of $ as always & a million other things I spend my spare $ on.



Unfortunately, I can create a million great things that look like people would buy it, but I have no idea how to sell it or get people to buy it hahaha. That's a learning curve I'm working on at the moment.


---
**I Laser** *May 08, 2016 07:46*

Thanks for the link, to be honest was more interested in the process. ;) 



I've had a play with a few pieces of software but not found anything that works reasonably well for converting raster to vector. My background is prepress, so I can usually get something usable, or if that fails I end up redrawing it in corel (which is a PITA), the depth of detail you have there is impressive.



Agreed, it's easy to find things the laser can do but making a dollar on them is a completely different story! So many things I've tried but the hourly rate is not worth it. The machines have paid for themselves though, so that's a good start. ;)



As for SVG's I've found only SVG v1.0 works when importing into corel, everything else either errors or loses the plot when imported.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 08:24*

**+I Laser** I've been saving in SVG 1.1 since I started using Corel x5. To be honest, I didn't even pay attention when I was saving. No problems importing it at least.



I'm fairly sure I've seen an option in Corel Draw for Trace, but not certain if it does the same thing as Adobe Illustrator's Live Trace. The AI Live Trace is a marvel to use. You can take any image & do a trace, usually with good results. Sometimes you get odd things happening, others you lose detail, but there are 11 different presets that you can use to trace the image, from High Fidelity Photo (which retains the most detail) through to Technical Drawing (which I have no idea what to use it on, but maybe on some kind of plans). I usually use either the 3, 6 or 16 colour choices for my laser tracings, then I remove or combine the colours until I'm satisfied with the amount of layers used (don't want to be doing 16 layers of separate engrave powers/speeds most of the time). I think there are some limitations to what we can do, but I'm always interested in finding "loopholes" or "work-arounds" to do what I require.


---
**I Laser** *May 08, 2016 08:45*

That's very cool, I'll check it out as I'm always looking for easier ways of getting stuff done. :D


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 08:58*

**+I Laser** here is an example of the different trace modes:



AI version ([https://drive.google.com/file/d/0Bzi2h1k_udXwb0NCYVQ0NlU4LUU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwb0NCYVQ0NlU4LUU/view?usp=sharing))

PNG version ([https://drive.google.com/file/d/0Bzi2h1k_udXwaTJNZG96dEUtbVE/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwaTJNZG96dEUtbVE/view?usp=sharing))


---
**I Laser** *May 08, 2016 10:08*

**+Yuusuf Sallahuddin** so great to find another option. I had no idea that illustrator had that functionality. Cheers :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 10:42*

**+I Laser** Not sure what versions support it, but CC does at least.


---
**I Laser** *May 08, 2016 10:43*

I've got a copy of CS4 and it's in there ;)


---
**Scott Marshall** *May 08, 2016 11:29*

**+Yuusuf Sallahuddin**

T shirts. I'm using a Silhouette to do the vinyl work. I sprung for a $600 heatpress with sliding platen etc, as I tried a few cheap ones and wasn't impressed. I have a thing about my stuff NEVER coming back, 100% satisfaction, etc, and I test washed shirts done with the cheap presses, they started to peel @ 8-12 washes. Never had a failure with the new press.

I looked seriously into dye sublim, by the time you get a printer special ink tanks, special shiirts - Dye won't work on cotton, must be a poly blend or 100% poly (Yuck)or (the big deal breaker) print white. Or print on dark colors. Plus you still need good press. $3000 to get set up right, and then you have to tell customers your shirts are $2 more because you can't print on cotton, which most people prefer anyway. Or white lettering on dark green, blue, red which many sports teams use. You probably know all that, but I didn't until i started seriously looking into it. The big plus is you can do photos well.



I did a couple dozen shirts for Family and friends, got my system worked out, I've sold a few dozen on Ebay, mostly my original designs. And some for RC clubs. I am hoping to get into the schools around here, (figures, my youngest graduates in 2 weeks) If you can get in good with the coaches, there's a lot of business there for the Soccer, Softball and football teams.. hard to make much selling 1's and 2's on ebay, everybody takes a cut, especially the shipper. It costs about 5 bucks to ship a shirt, and 5.50 to ship 2. 20 will go for $10.



Now I'm committing to selling Laser stuff, laying out a lot of cash for parts. My big product is going to be a Plug in kit for the smoothie.

It includes a custom power supply,(2 options) a plug in wiring harness, and all hardware, instructions and support. Buy a smoothie, my kit, and plug it in. In 2 hours you can be running. The parts are expensive and you have to buy large quantities to get a decent price. Just sent off $100 for connectors. I don't mind doing the engineering or building, but the marketing and Shipping (paperwork) is a drag.



Been up all night designing, planning etc. Gotta sell some stuff I've got to cover expenses. Do website. Not my thing, the web design.



Sorry to ramble on.



You should pound out those coasters, and run them on ebay around Christmas for $50/set plus shipping.- More with holder. Run them off now and put them away till November. You'll do great.

Save a set for me.

Scott


---
**I Laser** *May 08, 2016 12:08*

Wow **+Scott Marshall** very interesting read as usual. Shame about your shits being more expensive though. :P



Are you committing to the laser because the shirts aren't profitable enough, takes too long to produce?


---
**Scott Marshall** *May 08, 2016 12:33*

I'll still do the shirts, but I am an engineer, and always enjoyed the work. I was building FPV screens that mount to your transmitter and a little camera/transmitter about the size of a Matchbox for a while, then the Chinese started making the same thing, cheaper than I could buy the parts. I also do custom groundstations in 2 sizes, one with a 19" screen and another with a 8" (mini case). I also have a line of  model plane speed controls, I modify and improve chinese ones with heatsinks and better power supplies. I really neeed to get a website going, and put this stuff up for sale. I'm like Yuusuf like that I guess, don't sell much because it's not my thing. Never WAS a salesman.

I jump around, doing what I can when I can. My health is pretty spotty, I can't tell from one day to the next how I'll be doing. That's why I spend so much time here, when I don't feel well enough to get up and around, I can help people (or at least confuse them) from here.

I've had 86 surgeries since 1998 and things aren't real good right now, been all over the US looking for a Dr who can help. For the time being I'm trapped, and figure I'll make the best of it building parts for people etc.



Good catch, I get to typing so fast sometimes I hit the backspace key before I screw up: P

Anyways that's part of what going on with all my "experimental" stuff. I think I could make any of several fly if i ever get my  health back, in the meantime I keep trying the "mini-enterprise" approach. Usually COSTS me money, but it keeps me from going crazy (er).



Scott


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 18:48*

**+Scott Marshall** Yeah, the mini-enterprise stuff is the way I try & go (usually due to startup funds required) but as yet, no success (probably due to the lack of customer service/advertising/marketing skills). Websites these days can be done for super cheap (if you pay people from india or wherever to set it all up). There is a website I came across a while ago called "AirTasker" where you put up your task & people send a message to try accept the task. I saw a lot of people wanting website, or social media campaigns, etc & paying not much at all, yet people still jumped on happy to freelance it.



That's quite interesting what you're planning to do with the smoothie plug in kit. Would be great for people like me with minimal electronics background. What sort of pricing are you looking at for the finished product? I'd be interested as it will save a lot of time/effort for me (rather than trying to figure out all sorts of electronics stuff I have no idea about).



Maybe an idea for all of us is to combine together & create a shop-style website where we can all sell our laser stuff. Although, I have to do some work on the Everything Laser before I start any extra projects lol.



I've in the meantime placed up my coasters on my Etsy shop ([https://www.etsy.com/au/shop/YSCreationsAU](https://www.etsy.com/au/shop/YSCreationsAU)) for AU$55 (cover material costs, some of the time involved although not all, machine maintenance, etc). I'm thinking though, to improve the coasters I need to do something like route the edges with a nice curved bevel. Although, never used a router haha. First time for everything I suppose. Would love a CNC router setup though, then I could just control it via computer.



Actually, you are engineering background right? So is there a specific field of engineering that you have experience in? I've got an idea I am working on (for garden related products, something I think might be able to turn into a real business rather than a micro-business). I'm in need of some advice from someone with engineering knowledge however, for mechanical related aspects (e.g. how to determine the size & type of bearings to use for something that is going to have 100-200kg weight spinning on an axis). I tried read some data-sheets for different types but had no clue what it was on about lol.


---
**I Laser** *May 08, 2016 20:29*

Standard Chinese practice, they find someone buying a lot of their hardware wholesale, find out what they're doing with it and then produce it with local labour for a tenth of the price that it costs to do it in the first world...



Not much of a salesman myself either, though I do have an IT background and have setup/run a few ecommerce magento sites with varying success. Nothing to quit your full time job for though. Anyway hope you find a doctor and get your health issues sorted!



I'd be extremely careful about offshoring a web build, plenty of horror stories around. Usually the language barrier leads to misunderstood requirements / scope creep. If you're looking for a simple static site check out one of the do it yourself mobs (ie wix), though it can add up if you want ecommerce functionality. 



I do have a background in software/web dev but a bit rusty now, not much good at UX (just incase Anthony sees this post!). To be honest, building a site from scratch to sell laser goods on would be a fair task, you're basically building eBay/gumtree functionality and likely there's not enough targeted interest to make it worthwhile. Happy to be proven wrong though! Anyway good luck with the coasters. :D


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 21:09*

**+I Laser** I've never contracted someone offshore for any web work, just saw it & thought it was an interesting idea. Maybe not worth it to contract them for a full site, however for specific features or components could be useful (if you explained well what you wanted/didn't have language barrier issues). I was meaning something more simple, just for this group, not full-scale like eBay etc. But yeah, there are plenty of already existing platform for selling things online (ebay, etsy, etc).


---
**Scott Marshall** *May 09, 2016 07:48*

**+I Laser** You're probably right about the website. I've been looking at the WIX site, they claim you can build your own world class site in no time. It's not looking that simple to me. The fees aren't bad, but now I'm looking at learning another software package. 



My credentials are Electrical, but I've done a fair amount of mechanical work. Industrial machinery is pretty much a broad field. When I was in business I networked with my customers, for example I did a lot of work for a large hydraulic firm, and when I needed Hydraulic power for a job, I went t their fluidpower engineers for help. It was a great relationship.

Sorry I didn't get to call yesterday, was feeling bad, I'll try tonight.

Scott


---
**I Laser** *May 09, 2016 08:29*

**+Scott Marshall** yeah most of the 'do it yourself' website builders  are drag and drop. Which whilst easier than coding, presents it's own learning curve.



 I'm not completely dismissing your idea **+Yuusuf Sallahuddin**  it's just a fair amount of work. It's not the size of gumtree/ebay you need to accommodate it's the functionality. ;)



My (limited) experience has been helping those that have been burnt by cheap OS devs. Usually you'll be dealing with a intermediary that has little coding/project management experience but can speak, read, write English reasonably. They'll palm your project off to a team of local devs whose experience will be usually limited too (hence why it's so cheap). 



At best you end up with some usable code, at worse you lose your investment and end up having to hire a local dev to start the project again!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 09, 2016 17:05*

**+Scott Marshall** Personally I wouldn't consider Wix websites as "world class" but rather "just functional".



There are a few places out there that allow you to create a site/store for free (such as [storenvy.com](http://storenvy.com) or [bigcartel.com](http://bigcartel.com)) with reasonable ease (e.g. just input your product details, put pictures, price, etc.). They all have default theme/layouts which are quite decent, but you can modify these (if you want to/know how). The only issues with these kind of places as per se are some limitations that they put on the functionality (e.g. bigcartel allows max of 5 products before u have to pay a fee; storenvy limits product photos to a max of 5 per item). However, that being said, Storenvy has it's own built-in marketplace, kind of like etsy, where your products can show up for people to search (amongst other peoples products too). This is good to some degree as it can increase awareness of your products.



Take a look at [eternur.storenvy.com](http://eternur.storenvy.com), that's one that I worked on for my partner & I to sell random craft related things. So you can get pretty decent functionality out of it.



**+I Laser** You're probably right that the work involved will be quite high & the functionality is almost similar to ebay/gumtree. Although gumtree is more like a forum (just a slightly different layout) in the way it functions. I'm wondering if there are any pre-existing solutions that would be kind of like plug & play website components for setting up a shop that allows multiple sellers. I've got too many projects as is though, so I should probably stick to finishing them first haha.



From what you say about the offshore coders, sounds like it is indeed best to not waste your time or money on it.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/jNqABdnfGuD) &mdash; content and formatting may not be reliable*
