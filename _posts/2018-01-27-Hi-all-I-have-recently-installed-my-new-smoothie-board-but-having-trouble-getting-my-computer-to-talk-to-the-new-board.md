---
layout: post
title: "Hi all. I have recently installed my new smoothie board but having trouble getting my computer to talk to the new board"
date: January 27, 2018 04:28
category: "Discussion"
author: "Jason Smith"
---
Hi all. I have recently installed my new smoothie board but having trouble getting my computer to talk to the new board. Am I missing something?



Please help 





**"Jason Smith"**

---
---
**Joe Alexander** *January 27, 2018 04:35*

a few more details will help this community assist you. What OS are you using? board details?(many types of "smoothie" boards) is the sd card inserted, and does it contain the firmware.bin file and the config file? are you using a good quality usb cable with ferrite on the ends or a cheap chinese blue cable?


---
**Jason Smith** *January 27, 2018 04:55*

So. This is my board. I am using a good cable, as card is connected. But computer says I have to format it. I have inscape, 2rx7 works and lightburn (paid for verson). 

[https://lh3.googleusercontent.com/apcWYPxIP9uXkKGJiAkgQ04Kq3RYMsK_U_E-wn9xj75enPEqlciesfMjgBCJrjm5Q4yzseZKPA](https://profiles.google.com/photos/108083115075731642273/albums/6515589612635201457/6515589614566346706)


---
**Jason Smith** *January 27, 2018 04:56*

Do I need to format this as card?


---
**Jason Smith** *January 27, 2018 05:06*

I have [config.txt](http://config.txt) and [FORMWARE.CUR](http://FORMWARE.CUR) on the sd. 


---
**Jason Smith** *January 27, 2018 05:06*

[FIRMWARE.CUR](http://FIRMWARE.CUR)..


---
**Jason Smith** *January 27, 2018 05:23*

 I have now put the latest [firmware.bin](http://firmware.bin) file onto the sd card


---
**Joe Alexander** *January 27, 2018 05:24*

thats fine firmware.bin becomes .cur after being read by the board.




---
**Joe Alexander** *January 27, 2018 05:25*

i see a crappy blue cable, you have a better one? this seems to be a common issue


---
**Joe Alexander** *January 27, 2018 05:27*

**+Ray Kholodovsky**




---
**Jason Smith** *January 27, 2018 05:29*

Ok. I will find another


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2018 05:32*

Some combination of these things will solve this common concern:



1. Good quality USB cable like Joe said, not the bad blue one like what came with the K40.

2. If the issue persists, format the card FAT32 and put new files on it: [dropbox.com - C3D Mini Laser v2.3 Release](https://www.dropbox.com/sh/7v9sh56vzz7inwk/AAAfpPRqu63gSsFk3NE4oQwXa?dl=0)

3. If still an issue, try another card. 



Also, what OS?  Windows 7 or 8 will need Smoothie Drivers.  



Joe is spot on with the debugging process.  Feel free to post to the C3D group on G+ here.


---
**Jason Smith** *January 27, 2018 05:33*

Ok. New cable in. Pop up on computer says

[https://lh3.googleusercontent.com/P5xUE77odMXKdpNNe-AtQlhXDoI-MsUglN0UTOJVk2C83zKPmMNEOuaB6-vliVQ5hJJwHlpO1w](https://profiles.google.com/photos/108083115075731642273/albums/6515599385553861649/6515599382823441970)


---
**Jason Smith** *January 27, 2018 05:35*

Thankyou. I am using win 7 x64


---
**Jason Smith** *January 27, 2018 05:47*

So I have got a new cord in place, reformatted the Sd card and placed the files back on from the dropbox link


---
**Joe Alexander** *January 27, 2018 05:48*

check out [www.smoothieware.org](http://www.smoothieware.org) for the windows 7 drivers. if that doesnt work, pull a fresh copy of the firmware from [cohesion3d.com](http://cohesion3d.com) to put on the card after you format it.


---
**Jason Smith** *January 27, 2018 05:53*

Fails to download the [smoothieware-usb-driver-v1.1.exe](http://smoothieware-usb-driver-v1.1.exe)

[https://lh3.googleusercontent.com/p5gTglBlUwzrMeU3_myNG4C_Ee7jJ8OS31ySLA0vrsQhzl9Z314nGBsckvfJl5MfvGHsG3XFmw](https://profiles.google.com/photos/108083115075731642273/albums/6515604649328361105/6515604652180823106)


---
**Ray Kholodovsky (Cohesion3D)** *January 27, 2018 05:55*

Do a manual install. It's explained on the Smoothie page


---
**Jason Smith** *January 27, 2018 06:07*

Successfully updated smoothie board. now shows in device managers, 

 


---
**Jason Smith** *January 27, 2018 06:17*

Great, It can now talk to the computer. Thankyou so much. 

, how do I set up  speed, power for optimal performance




---
**Jason Smith** *January 27, 2018 06:30*

[https://lh3.googleusercontent.com/LFuZ-BZYW8Tca9iiSK7Tkcob4Fn855qX5VCRSefd3TKDiVyxsILDhyCpeDJWzefvVWHovXLwAQ](https://profiles.google.com/photos/108083115075731642273/albums/6515614143650945313/6515614142033978434)


---
**Jason Smith** *January 27, 2018 06:30*

ok. so my next question is I have additional power source for extras like LED lights ect but not sure where to tap into the power supply. Is on the back of the power cable good? The additional power unit has  L (Brown wire) , N (Blue wire ) and Ground symbol (Green/yellow wire)




---
**Jason Smith** *January 27, 2018 06:41*

Thankyou Ray and Joe. I really appriciate the help. 


---
**Joe Alexander** *January 27, 2018 06:44*

depends on the voltage required. I have some led lights tapped into a separate small power supply for 12v that branches off the legs for the laser PSU. just remember to mind your polarities and tie all grounds together. be safe. also check out [donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com) for thorough experiences and guides.


---
*Imported from [Google+](https://plus.google.com/108083115075731642273/posts/EfXKhkHCe88) &mdash; content and formatting may not be reliable*
