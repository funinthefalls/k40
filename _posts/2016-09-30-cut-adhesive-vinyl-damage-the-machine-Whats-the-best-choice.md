---
layout: post
title: "cut adhesive vinyl damage the machine? Whats the best choice?"
date: September 30, 2016 22:01
category: "Materials and settings"
author: "Anderson Firmino"
---
cut adhesive vinyl damage the machine? Whats the best choice?





**"Anderson Firmino"**

---
---
**Anthony Bolgar** *September 30, 2016 22:25*

Vinyl tiles will give off chlorine gas....dangerous to breathe and corrodes the metal in the laser. Do not use!


---
**Scott Marshall** *October 01, 2016 03:48*

If you need peel and stick, mylar material is available.



I use a Silhouette drag knife cutter for Vinyl.


---
**Anthony Bolgar** *October 01, 2016 03:50*

i USE A 42" us cUTTER VINYL PLOTTER FOR MY VINYL NEEDS.


---
**Scott Marshall** *October 01, 2016 04:12*

**+Anthony Bolgar** Must be nice!


---
**Anthony Bolgar** *October 01, 2016 05:38*

I bought it originally to do the lettering on my business van. It was cheaper to buy the plotter than vinyl than it was to pay someone to make and install the lettering. I have slowly accumulated different colors and types of vinyl, I have about 75 different rolls of varying colors, designs etc. They are actually quite inexpensive, I paid $450 canadian for mine.


---
**Scott Marshall** *October 01, 2016 09:08*

**+Anthony Bolgar** I've looked into them very seriously, they're the K40 of the vinyl cutter world, compared to a name brand like a Roland.

Much the same issues too. I'm considering doing a smoothie to cutter kit, IF I ever get caught up with what I'm already into.

I spent my big bucks on a real good heat press and decided to upgrade to the larger cutter when/if my t shirt endeavor ever took off. It hasn't, but the cutter is on my list of goodies to purchase when my lottery ticket wins.


---
**Anthony Bolgar** *October 01, 2016 09:54*

I have not had any issues with my plotter, but I want to upgrade to one that can do contour cutting. But for now, it does a great job cutting out things like boat and snowmobile registration numbers at $20 a pop. I make a few thousand a year with it doing that.


---
*Imported from [Google+](https://plus.google.com/+AndersonFirmino/posts/167CnqzaEqg) &mdash; content and formatting may not be reliable*
