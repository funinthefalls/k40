---
layout: post
title: "I know there is one somewhere cause I have found it in the past"
date: August 18, 2018 16:31
category: "Discussion"
author: "timb12957"
---
I know there is one somewhere cause I have found it in the past. But for some reason I cannot find a tutorial on engraving a photograph with my K40. Seems it involved using Whisperer? Someone point me in the right direction please.





**"timb12957"**

---
---
**James Rivera** *August 18, 2018 17:11*

Scroll down to, "Halftone (grayscale) Images with K40 Whisperer".

[scorchworks.com - K40 Whisperer](http://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
**timb12957** *August 18, 2018 17:29*

Thanks so much, I knew it was somewhere!


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/QBTME2Nxkbd) &mdash; content and formatting may not be reliable*
