---
layout: post
title: "Just a curiosity. Does anyone know the reasoning behind the spring loaded clamp shipped in the K-40 laser bed?"
date: March 19, 2017 21:06
category: "Original software and hardware issues"
author: "timb12957"
---
Just a curiosity.  Does anyone know the reasoning behind the spring loaded clamp shipped in the K-40 laser bed? If  used, it severely limits the  area you could cut or engrave. I am pretty sure we have all removed it. 





**"timb12957"**

---
---
**Fred Padovan** *March 19, 2017 21:25*

I'm pretty sure these lasers are meant to engrave rubber stamp

The clamp is there to hold the rubber to be engraved 




---
**Ned Hill** *March 19, 2017 22:48*

Yep see this post for background [plus.google.com - This guys been modding K40's since they first started coming out of China (20...](https://plus.google.com/+vapetech/posts/YbMT1acR2Ja)


---
**Chris Hurley** *March 19, 2017 23:53*

I think it's actually made for making a certain type of signature stamp which is common in Japan. 


---
**timb12957** *March 20, 2017 01:56*

ok thanks just wondering


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/g4ttvZoLxVx) &mdash; content and formatting may not be reliable*
