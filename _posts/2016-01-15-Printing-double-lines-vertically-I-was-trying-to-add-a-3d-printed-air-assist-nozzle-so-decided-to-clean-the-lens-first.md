---
layout: post
title: "Printing double lines vertically.... I was trying to add a 3d printed air-assist nozzle so decided to clean the lens first"
date: January 15, 2016 14:38
category: "Discussion"
author: "Arestotle Thapa"
---
Printing double lines vertically....   I was trying to add a 3d printed air-assist nozzle so decided to clean the lens first. Once I clean the lens I wanted to make sure the alignment is still OK so I tested with a sample. I tried flipping the lens to make sure it is not the cure side up/down problem and same result.  



Have you seen this behavior? Did I knocked my mirrors out of place while I clean my lens or twisted something else? I don't remember doing anything like that!

![images/b47f9029aaea13b9f403b2c484808862.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b47f9029aaea13b9f403b2c484808862.jpeg)



**"Arestotle Thapa"**

---
---
**Anthony Bolgar** *January 15, 2016 21:27*

It looks like you knocked the laserhead out of being perfectly vertical when you cleaned the lens. There is some slop in the carriage, unscrewing and rescrewing the lens holder could knock it out of being straight up and down.


---
**Brooke Hedrick** *January 16, 2016 03:25*

I saw this when I had my alignment out so far that the beam was barely making in into the lens area and was "reflecting" in such a way the beam appeared to split on the work piece.



First stop for stuff like this, for me, now is check alignment.  4 or 5 pieces of stacked blue painters' tape tells me the story pretty quickly.



It definitely could be that the lens tube isn't square and perpendicular to the beam too.  In my case the lens tube doesn't really move that way when taking it apart to work with the lens - at least on my k40.


---
**Arestotle Thapa** *January 16, 2016 04:13*

Thanks **+Anthony Bolgar** I'll try to take the head off and on again.



Thanks **+Brooke Hedrick** I did check the other mirrors and the laser is centered. Do you mean to put the tape on the laser head or target object being cut?


---
**Brooke Hedrick** *January 16, 2016 04:29*

Laser head where the beam enters.  If you put tape at the bottom of the laser head, right where the beam exists, do you only get 1 mark or 2?  It may not be easy to see.



If your alignment is good, and the head that holds the lens can tilt, that head probably is the issue.  Mine doesn't really tilt.  There is a piece on the top of the x carriage plate and below.  They screw together through the plate.  For me, the carriage and/or plate would have to tilt somehow.


---
**Arestotle Thapa** *January 25, 2016 04:56*

Finally got some time to look at this. It looks like the problem could be caused by water bubble in the tube! I see the beam bending right before it laser exits the tube where I see a big water bubble which doesn't want to come out.. Hopefully once I get the bubble out it may work.


---
*Imported from [Google+](https://plus.google.com/104400841682788049551/posts/8d662pDR4yM) &mdash; content and formatting may not be reliable*
