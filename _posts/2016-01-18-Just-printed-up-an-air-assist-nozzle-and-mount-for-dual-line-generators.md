---
layout: post
title: "Just printed up an air assist nozzle and mount for dual line generators :-)"
date: January 18, 2016 23:35
category: "Modification"
author: "David Cook"
---
Just printed up an air assist nozzle and mount for dual line generators :-)



![images/be93dfa63243d9dd94116713ed0917bf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be93dfa63243d9dd94116713ed0917bf.jpeg)
![images/ed168bbda1b443e059b29ac7e150208a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ed168bbda1b443e059b29ac7e150208a.jpeg)
![images/a7b816c6b81ec1da3d1c791d14092c73.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7b816c6b81ec1da3d1c791d14092c73.jpeg)
![images/1e7e4f60731f00242834a22170e907f5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e7e4f60731f00242834a22170e907f5.jpeg)
![images/aaa6e8831d6c152cbfc558652094e009.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/aaa6e8831d6c152cbfc558652094e009.jpeg)
![images/9b12007bb4e0b10324832e5a9e739e9f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9b12007bb4e0b10324832e5a9e739e9f.jpeg)
![images/c0080ecc5a85325a47ffce5ebcb07f3f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c0080ecc5a85325a47ffce5ebcb07f3f.jpeg)

**"David Cook"**

---
---
**Shane Nunn (HeyThatGuy R16)** *January 18, 2016 23:51*

looks pretty good


---
**ChiRag Chaudhari** *January 19, 2016 00:05*

Oh wow looks nice man. Which printer you have? I just  purchased Monoprice Maker Architect 3D. 


---
**David Cook** *January 19, 2016 00:38*

I used an Ultimaker 2 here at work.   I did not model this one up , I found it here  [http://www.thingiverse.com/thing:1063067](http://www.thingiverse.com/thing:1063067)



3mm silver Ultimaker  PLA  printed at 210°C,  .35 layer height, 60mm/s speed.  30% infill,  60°C bed temp


---
**Anthony Bolgar** *January 19, 2016 01:49*

Yhat is the nozzle I have been using, works great.


---
**Jim Hatch** *January 19, 2016 01:54*

very nice. I figured I'd spend a ton of time diddling around getting it right so I used it as an excuse to get an upgraded lens from Lightobjects :-)




---
**Anthony Bolgar** *January 19, 2016 01:57*

The good thing about this nozzle is that the air only flows out of the 4 outside holes (the nozzle is double walled) so that it encircles the laser beam from all sides.


---
**Shane Nunn (HeyThatGuy R16)** *January 19, 2016 02:06*

that's cool. would allow for a more dispersed air flow


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/bQ8e8ByTMKB) &mdash; content and formatting may not be reliable*
