---
layout: post
title: "Can someone let me know ideal speed and power settings for the standard 40W laser to cut 2mm acrylic sheets ?"
date: February 06, 2018 02:17
category: "Object produced with laser"
author: "Ian Hayden"
---
Can someone let me know ideal speed and power settings for the standard 40W laser to cut 2mm acrylic sheets ?   I normally use my machine for engraving, but have been asked to do some cutting.  Have just replaced the mirrors so dont think i am losing efficeincy.   At 5mm/sec and 40amperes it cuts about 1.x mm  but at anything higher power or slower speed, it cuts, but the upper surface melts

Software is laserDRW.   Laser is properly aligned.   I cut some acrylic a couple years ago, and didn't record the settings.  





**"Ian Hayden"**

---
---
**Joe Alexander** *February 06, 2018 07:13*

well I personally can cut through 3mm acrylic at 15ma@8-10mm/sec and the pieces usually either drop out or can be popped out with little effort. 40amperes seems excessively high and will shorten the life span of your tube.


---
**timne0** *February 06, 2018 10:37*

40ma is crazy high... My 40w laser wouldn't go above 21ma (may be due to other issues) however that was more than enough to cut 6mm acrylic at 16ma @ 2.5mm/sec


---
**Anthony Bolgar** *February 06, 2018 12:31*

I have a sneaky feeling that you are trying to cut polycarbonate and not acrylic.




---
**Steve Clark** *February 06, 2018 17:25*

**+Anthony Bolgar**  I'm with you Anthony. My first thought also.


---
**timne0** *February 06, 2018 17:28*

If you're sure it's acrylic, what's the focal length and have you adjusted the z axis?my K40 was 50.8.  


---
**Ian Hayden** *February 07, 2018 01:56*

thanks.  Now you mention, I'm not sure acrylic or not, it was lying around the workshop.  I have just been out and picked up a piece of acrylic so will let you know.   Documentation says 50.8  Will experiment that 50.8 should be to the top surface or bottom surface.




---
**Joe Alexander** *February 07, 2018 02:47*

ideally 50.8 would land in the middle of the thickness of your material to keep a relatively straight edge over a directional taper.


---
**Ian Hayden** *February 07, 2018 03:34*

**+Joe Alexander**  Yes, i am worried about the life of the tube.  even for this one off project.  i even tried one letter at 80,, speed 4mm and it didnt cut thru the 2mm so there must be something fundamentally wrong. The mirrors are brand new and aligned. The test firing produces a fine bright dot, and the cut on the acrylic was sharp. No customizations to the board or machine.


---
**Joe Alexander** *February 07, 2018 08:25*

ive abused my tube for several months so i wouldnt fret too much about a small project. btw when you say "80" it that the number on the digital panel? if you have an ammeter that shows the actual output from the tube what is that? that should never exceed 21ma(manufacturer max spec) and is best ran in the 10-15ma range to reduce the shock to your tube. also if you haven't do a ramp test to ensure you are at the right focal depth.


---
**Ian Hayden** *February 07, 2018 12:22*

**+Joe Alexander** sorry - not electrical minded or trained.   I'm a woodturner doing a bit of engraving on pieces.  80 was the reading on the panel.   Can you explain the ramp test ?




---
**Joe Alexander** *February 08, 2018 04:45*

sure can, and I am a handyman also not electrically trained but know just enough to get myself in trouble :P a ramp test is where you prop up a board on one side(like a ramp, ironic right?) and engrave a line across it. the spot where the line is the finest will be your ideal focal depth.


---
**Ian Hayden** *February 08, 2018 10:22*

i see. now you mention it, very obvious to the initiated.  thanks.  I got a little better cutting action today (cf faster/less power) by fine tuning the mirrors, but not enought ovut 2mm   I think i can work the mirrors a bit more, they have been fine for engraving pictures.  


---
*Imported from [Google+](https://plus.google.com/111645426566096956650/posts/bAC2DLu6NNM) &mdash; content and formatting may not be reliable*
