---
layout: post
title: "Hi, got a very worrying problem. My k40 was working fine yesterday and i went to set up a job today, pressed the test fire button and a huge spark and loud noises came from the right hand side of the tube"
date: October 11, 2015 10:29
category: "Hardware and Laser settings"
author: "george ellison"
---
Hi, got a very worrying problem. My k40 was working fine yesterday and i went to set up a job today, pressed the test fire button and a huge spark and loud noises came from the right hand side of the tube. Scared to set the job going in case i do any more damage, any one have any idea on what i can do? Thanks





**"george ellison"**

---
---
**Phillip Conroy** *October 11, 2015 10:32*

Check the wires going to the tube----when every ting is offf---wire may have come off or arking to case


---
**george ellison** *October 11, 2015 10:32*

How do i access these? many thanks


---
**Phillip Conroy** *October 11, 2015 10:37*

undo the screw in the center bottom of the pannel at the back top-there the laser tube is-the tube has 2 wires going to it 1 each end-they must be well covered by solastic to stop arking-22,000 volts- check for black marks near the right hand side wire


---
**Phillip Conroy** *October 11, 2015 10:38*

you may have blown a hv transformer as well with the wire shorting out or arking


---
**Phillip Conroy** *October 11, 2015 10:43*

Just upgraded my laser cooling to us a undersink water chiller,temps for water where 25 plus now steady at 16deg,these laser cutters need a cutout switch for when lid is opened,water flow cutout switch,all fitted now,however blown a tube and hv power supply because forgot to turn on water pump,will never happen again with $5  flow switch from ebay and a bit of rewiring


---
**george ellison** *October 11, 2015 10:53*

Wires seem all right, no shorting or arcing. Can't understand why it's done it all of a sudden. Bit of a pain as i have orders to complete, do i need to change the tube?


---
**george ellison** *October 11, 2015 10:57*

Its fizzing so this must be arcing somewhere, need to have a decent look in the back at the wires to make sure theyre not touching each other of any metal, where can i find these hv transformers? Thanks


---
**Phillip Conroy** *October 11, 2015 12:14*

Inside the power supply at the back


---
**george ellison** *October 11, 2015 12:14*

I have seen a red cable from the tube going into there and another black i think but cant really see. Just need to know how i can sort this and if it can be done in 5 mins or do i need a new part? Thanks for help


---
**Phillip Conroy** *October 11, 2015 12:25*

Try and find any arcing-may have to do this in a dark room or at night


---
**george ellison** *October 11, 2015 12:42*

What causes arcing? I have look at all the cables i can find and they are covered up and if they are touching they have plastic over them, no exposed wires that i can see. 


---
**Stephane Buisson** *October 11, 2015 13:17*

Pay special attention to your ground, including your main plug, sometime the ground pin on the main plug (partly covered with plastic) don't contact with house earth.


---
**george ellison** *October 11, 2015 13:33*

In the uk so not using the ground wire port on the rear. Will check the mains plug anyway, many thanks


---
**george ellison** *October 11, 2015 13:46*

The mains cable has a uk plug. I did ask for a uk version when ordering, should this be ok? Have used it now for around 6 months with no problems. 


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/Az2dDsp7zUd) &mdash; content and formatting may not be reliable*
