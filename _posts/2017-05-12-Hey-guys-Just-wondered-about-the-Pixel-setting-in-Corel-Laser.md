---
layout: post
title: "Hey guys, Just wondered about the Pixel setting in Corel Laser"
date: May 12, 2017 12:08
category: "Software"
author: "Pigeon FX"
---


Hey guys, Just wondered about the Pixel setting in Corel Laser.



I tend to cut/engrave everything from a vector Ai file. Is it worth using vector for just the cuts, and use Raster for the images? 



I assume Pixel setting only apply to Raster? and if so, what Pixel range should playing with?



![images/bb95479a94a1abb806e55e239a1c7849.png](https://gitlab.com/funinthefalls/k40/raw/master/images/bb95479a94a1abb806e55e239a1c7849.png)



**"Pigeon FX"**

---
---
**Scorch Works** *May 12, 2017 21:17*

That setting determines the distance between scan lines when using the Engraving option.  It will affect everything that is engraved (raster and vector).



The number value is in thousandths of inches. 1=0.001in (.0254 mm)



If you change this to 2 (0.002 in) the engraving should take about half the time because the laser will cross the design half as many times.


---
**Pigeon FX** *May 13, 2017 14:56*

**+Scorch Works** Thanks mate, is there a large quality drop from 1 to 2 steps, is 1 the most common setting for people or is it common to bump it up to a higher setting?




---
**Scorch Works** *May 13, 2017 15:18*

The best setting it a personal choice.  I suggest you experiment with the setting to see how it affects the results for your particular application.  Also check out the discussion under the cow tags for additional information.  

[plus.google.com - I am engraving cattle tags that have a black center sandwiched between white ...](https://plus.google.com/101479367502614716645/posts/Sr5gnG5Pggk)




---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/G1mLGRwS8Yg) &mdash; content and formatting may not be reliable*
