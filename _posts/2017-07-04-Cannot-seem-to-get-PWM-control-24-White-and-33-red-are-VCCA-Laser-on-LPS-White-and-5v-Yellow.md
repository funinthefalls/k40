---
layout: post
title: "Cannot seem to get PWM control. 2.4 (White) and 3.3 (red) are VCCA, Laser on LPS (White) and 5v (Yellow)"
date: July 04, 2017 00:10
category: "Smoothieboard Modification"
author: "Jerry Hartmann"
---
Cannot seem to get PWM control. 2.4 (White) and 3.3 (red) are VCCA, Laser on LPS (White) and 5v (Yellow). I always get 100%.

![images/d7811aa667adfdf97d757d1f99cadf42.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d7811aa667adfdf97d757d1f99cadf42.jpeg)



**"Jerry Hartmann"**

---
---
**Don Kleinschnitz Jr.** *July 04, 2017 00:20*

Need to know some more information to help.

Controller, wiring etc,

This form may help you organize the information: 

[https://docs.google.com/forms/d/e/1FAIpQLSdWWE5FAwsg4-c9MWKRGFQEPBBvZtNpfv0z7RrofYrOBEw4UA/viewform?usp=sf_link](https://docs.google.com/forms/d/e/1FAIpQLSdWWE5FAwsg4-c9MWKRGFQEPBBvZtNpfv0z7RrofYrOBEw4UA/viewform?usp=sf_link)



This is a place to start.Look for sections on PWM control and smoothie wiring. [http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html)


---
**Jerry Hartmann** *July 04, 2017 02:12*

Im using a smoothie 3x. Ive read through your guides. 


---
**Don Kleinschnitz Jr.** *July 04, 2017 04:08*

Thanks for using the form.

I need to better understand exactly how you connected your PWM. It looks like in the picture you are using a level shifter? A level shifter is not needed. 

Did you review this: [donsthings.blogspot.com - K40-S Laser Power Supply Control "Take 2"](http://donsthings.blogspot.com/2016/11/k40-laser-power-supply-control-take-2.html)


---
**Joe Alexander** *July 04, 2017 04:49*

the picture that is worth 1000 words :) credit to **+Don Kleinschnitz** as my original source.

![images/92fe81987a1eda4e25d802b3c7fd463e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92fe81987a1eda4e25d802b3c7fd463e.jpeg)


---
**Don Kleinschnitz Jr.** *July 04, 2017 11:47*

**+Joe Alexander** working?


---
**Joe Alexander** *July 04, 2017 12:02*

oh im not the orginal poster, just another helping hand and giving you your due credit


---
**Don Kleinschnitz Jr.** *July 04, 2017 12:09*

**+Joe Alexander** whoops, need my coffee :).


---
**Jerry Hartmann** *July 04, 2017 13:39*

**+Don Kleinschnitz** The laser wouldn't fire until **+Scott Marshall** helped me set up the leveller.

**+Joe Alexander** That schematic is for the 5x. I'm using a 3x. My PS looks different as well.


---
**Jerry Hartmann** *July 04, 2017 13:45*

Let me make sure I understand. I have a GGGG. I disconnect the leveller. I don't use 2.4 or 3.3 and simply connect PWM0 to L?


---
**Don Kleinschnitz Jr.** *July 05, 2017 13:50*

**+Jerry Hartmann** 



...............



I assume:

1. that "2.4" refers to the PWM pin that is on the header on the board?

2. "3.3" refers to the 3.3V on the header that feeds the level shifter.



................



Its unclear to me if this was ever working, not filed in on your  problem profile?



3. Was this working? If yes what did you change that may have caused this problem?

...............

We have a choice:



4.a. to try an fix what you have i.e. level shifter config.

4.b. rewire to use the "L" input.



**+Arthur Wolf** I searched and could not find the schematic for a 3X, did find the 5X.






---
**Jerry Hartmann** *July 05, 2017 14:08*

I belie i put in the profile that it always fires at 100%.



I can tru removing the shifter but i need to know which pin to solder for ground as thr 3x lacks all the connectors the 5x has


---
**Don Kleinschnitz Jr.** *July 05, 2017 16:38*

**+Jerry Hartmann** as a help can you answer the other questions above?


---
**Jerry Hartmann** *July 05, 2017 17:06*

1. Yes 2. Yes 3. See above 4. Either


---
**Don Kleinschnitz Jr.** *July 06, 2017 01:35*

**+Jerry Hartmann** ok it was working and now it isn't, what changed?


---
**Jerry Hartmann** *July 06, 2017 03:51*

I never had pwm. I could fire the laser after the leveller was installed but i can control the power output with software


---
**Don Kleinschnitz Jr.** *July 06, 2017 16:10*

**+Jerry Hartmann** did you mean " i <s>can</s> cannot control the power output with software".

I am having trouble finding info on the smoothie 3X can you post a picture with all connectors showing?




---
**Joe Alexander** *July 06, 2017 16:32*

the smoothie 3x and 5x are identical boards minus the extra smt drivers and required components. board layout and pin location remains the same **+Don Kleinschnitz** for reference. i know if you get a 3x vs 4x it doesnt have all the screw terminals or the lan jack included.(4x and 5x its standard)


---
**Don Kleinschnitz Jr.** *July 06, 2017 17:50*

**+Joe Alexander** **+Jerry Hartmann** thanks I just found that out :). That's why there isn't a separate schematic ..... I will take a look and come back with recommendations.


---
**Don Kleinschnitz Jr.** *July 06, 2017 18:40*

**+Jerry Hartmann** this should be straightforward. The blog post sited above applies to your 3x board. 



The P2.4 (PWM) connector is populated just like the 5X picture (white wire). 

The connector that is used for ground (black wire) in that scheme is not populated in the 5x but you can pick up the ground right off of X13 on board where the connector would have been installed.



The attached layout graphic shows more detail. 



Verify your ground with a DVM to be sure you have the right pad.



If you wire it up as this suggests and post a picture before trying I will check it for you.



Note: you have to change the configuration file to match this setup (P2.4). 



See that in the blog post.



![images/597102570f4c41c673c2d54f7f2e8a19.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/597102570f4c41c673c2d54f7f2e8a19.jpeg)


---
**Jerry Hartmann** *July 07, 2017 21:27*

**+Don Kleinschnitz**

![images/11d44b3eaf9f1a58092529da5ef98d80.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/11d44b3eaf9f1a58092529da5ef98d80.jpeg)


---
**Jerry Hartmann** *July 07, 2017 21:28*

Im still running ground with the 24v in on VBB, correct?


---
**Don Kleinschnitz Jr.** *July 07, 2017 22:22*

**+Jerry Hartmann** yes the 24V and grnd does not change, this is just the PWM and its signal ground. Did you verify with a DVM that the black wire is grnd ?



You now have to insure that your config file is right and on the SD card.


---
**Jerry Hartmann** *July 07, 2017 22:35*

**+Don Kleinschnitz** laser module pin was already 2.4 while using the leveler. Do you mean switch.laserfire.output_pin ?




---
**Jerry Hartmann** *July 07, 2017 23:30*

Ok, so neat, it fires without the leveller. I copied your config onto the sd and it will attempt to engrave but it still does so at max power


---
**Don Kleinschnitz Jr.** *July 08, 2017 12:08*

**+Jerry Hartmann** you only need one port P2.4, I I have laser-fire disabled. 

Can you provide a picture of what you are engraving. Best to use a grey shade test pattern.


---
**Jerry Hartmann** *July 08, 2017 12:55*

![images/f9b2552e47b2ee4c66ae49f2ba9d3e88.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f9b2552e47b2ee4c66ae49f2ba9d3e88.png)


---
**Jerry Hartmann** *July 08, 2017 12:55*

**+Don Kleinschnitz** That should be 50% grey




---
**Don Kleinschnitz Jr.** *July 08, 2017 13:14*

**+Jerry Hartmann** Pictur above barely visible.  Suggest trying a grey scale pattern as its hard to see changes with a single power level.

You still have the current pot installed right?

Turn it down until the engraving is just visible and make adjustments to it and lower speed to tweak it in.


---
**Jerry Hartmann** *July 08, 2017 13:28*

**+Don Kleinschnitz** I cant turn it lower than 5 or it wont fire. Even at 5 its still digging in deep.


---
**Don Kleinschnitz Jr.** *July 08, 2017 13:41*

**+Jerry Hartmann** lets try it with a grey shade pattern.....then post picture and we will start from there.

1. What CNC software (didn't see in your profile) are you driving this with. I.e what is creating Gcode?

2. Have you tried changing the speed and what are you running?




---
**Jerry Hartmann** *July 08, 2017 13:46*

1. Yes

2. Laserweb4




---
**Jerry Hartmann** *July 08, 2017 15:02*

**+Don Kleinschnitz**​ greyscale produces no change


---
**Jerry Hartmann** *July 19, 2017 04:14*

**+Don Kleinschnitz** Any other suggestions?


---
**Don Kleinschnitz Jr.** *July 19, 2017 13:40*

**+Jerry Hartmann** did a miss a picture of the grey shade pattern?

1. Can you cut?

2. Can you see engraving information but it all one shade?



What was the answer to #2 above?

A picture of the grey shade pattern would help.




---
**Jerry Hartmann** *July 19, 2017 13:42*

**+Don Kleinschnitz**​ #2 was answered above. I can cut just fine.



I wont have pictures to upload until tonight


---
**Don Kleinschnitz Jr.** *July 19, 2017 13:49*

**+Jerry Hartmann** also insure that your PWM period is set to 200 not 20? What is it set to now?



NP I will check back in tonight on the pictures.



Its a common problem to have engraving issues when first setting up. These things have to be correct:

A. Pot tweaked to the minimum power to get darkest area.

B. PWM period tweaked to 200-400

C. Speed tweaked, may need to speed up to engrave properly.


---
**Jerry Hartmann** *July 19, 2017 13:54*

**+Don Kleinschnitz**​ I used the config file you provided


---
**Don Kleinschnitz Jr.** *July 19, 2017 14:07*

**+Jerry Hartmann** yes I know.

Assuming that PWM is working correctly, these machines need tweaking to get the best results. My machine is currently optimized for cutting.



Change the PWM period from 20 to 200, then 400 and try engraving both configs.

Note: some machines cut better with 20 but engrave better with 400. I can explain why but its a long story :)!.

Your best engraving is a combination of A-C above.


---
**Jerry Hartmann** *July 20, 2017 00:21*

**+Don Kleinschnitz** That did it! It engraves at both 200 and 400. A weird quirk though is that if I set the dial to 15mA and tell it to use 40% power, the result is still rather dark but i can control that with the dial.



I'm super stoked!

Thanks so much!


---
**Don Kleinschnitz Jr.** *July 20, 2017 00:47*

**+Jerry Hartmann** OH YA! Another happy K40 user engraving without "Level Shifting" .... engineering is a wonderful thing!


---
*Imported from [Google+](https://plus.google.com/105524720582279612505/posts/HEBPvN955Xo) &mdash; content and formatting may not be reliable*
