---
layout: post
title: "Another DSP question: For those with a LightObject X7 DSP: what software does it use?"
date: January 17, 2016 15:18
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
<b>Another DSP question:</b>



For those with a LightObject X7 DSP: what software does it use? Ideally I'd like to not be using any of the Chinese software. I'd rather stick with something open source (Visicut?)



I also wouldn't mind something I could hack/expand on and add things like an electronic air valve that I can control in software, as opposed to a manual mechanical switch, and some sensors.





**"Ashley M. Kirchner [Norym]"**

---
---
**Gee Willikers** *January 17, 2016 16:30*

It's a proprietary software called LaserCAD. It will control an air valve on one of the outputs but will need a transistor and relay or similar.


---
**Ashley M. Kirchner [Norym]** *January 17, 2016 17:38*

But is it another Chinese flavor?


---
**Gee Willikers** *January 17, 2016 20:31*

As I understand it, its written for Lightobject by a Chinaman yes. It's not a terrible software and is far superior than anything like Moshidraw. There is no alternate or upgrade path available for the DSP. 



I'd actually like to contact Lightobject about the software and suggest some modifications and improvements. 


---
**Brooke Hedrick** *January 17, 2016 21:22*

There is an ok manual for the software.  Unlike MoshiDraw, the software and manual is all in English and fairly well worded.



I have used both and LaserCAD is much easier to use.  All of the odd behavior I was seeing is gone.


---
**Ashley M. Kirchner [Norym]** *January 17, 2016 21:57*

Right, but after thinking about it, I don't think the DSP would offer me the same kind of expandability options. I may have to go to something like an Azteek maybe.


---
**Todd Miller** *January 17, 2016 23:15*

I'm curious about LaserCad also, I won't invest anymore in my K40, but I do want to build a 900 x 600 using parts from LightObject and this DSP.



I'd like to download the software and play with it first.


---
**Brooke Hedrick** *January 17, 2016 23:18*

Here you go...  Info about the 708c lite and you can download LaserCAD.  No dongle required.  [https://www.sinjoe.com/index.php?route=product/product&product_id=96](https://www.sinjoe.com/index.php?route=product/product&product_id=96) 


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/KznqZL8jMyE) &mdash; content and formatting may not be reliable*
