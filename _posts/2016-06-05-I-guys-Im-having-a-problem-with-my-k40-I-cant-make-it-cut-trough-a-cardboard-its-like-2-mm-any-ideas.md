---
layout: post
title: "I guys I'm having a problem with my k40 I can't make it cut trough a cardboard it's like 2 mm any ideas?"
date: June 05, 2016 21:38
category: "Original software and hardware issues"
author: "Cesar Padilla"
---
I guys I'm having a problem with my k40 I can't make it cut trough a cardboard it's like 2 mm any ideas?


**Video content missing for image https://lh3.googleusercontent.com/-cCYn4ZRPVtQ/V1Sbur2468I/AAAAAAAATOA/k6RC08XD3DMJTBHCgrVpsthaLutGVI1BA/s0/20160605_140527.mp4.gif**
![images/909d9d3fd201b2032fe846de2c74a1b6.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/909d9d3fd201b2032fe846de2c74a1b6.gif)



**"Cesar Padilla"**

---
---
**Cesar Padilla** *June 05, 2016 21:40*

wrong video :P this is couche paper 130grs


---
**Anthony Bolgar** *June 05, 2016 22:29*

Your mirrors are most likely out of adjustment if you can't cut paper or cardboard. This is a great article explaining how to adjust them



[www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 05, 2016 22:35*

Alternatively to **+Anthony Bolgar**'s suggestion, your lens could be upside down. A lot of us had it ship upside down & it would mark like yours is, but very difficult to cut. When swapped over it cut like hot knife through butter for me.


---
**Cesar Padilla** *June 06, 2016 03:35*

when you say lens it's located where it's the Last miror?


---
**Anthony Bolgar** *June 06, 2016 03:38*

It is in the laser head (the part that moves back and forth with the laser beam exiting it to the work piece. It unscrews, exposing the lens.Carefully clean it with a soft cloth an isopropyl alcohol, the flip it 180 degrees and put it back in the head and screw it back closed.


---
**Shannon Haworth** *June 06, 2016 04:03*

I wear nitrile gloves to keep the lenses oil free, and clean them with MG 810-15 Chamois Swabs.  Amazed at how much gunk those swabs lift off the lenses.


---
**Christoph E.** *June 06, 2016 10:02*

How can you tell if the lense is upside down? I put the curved part down pointing to the workpiece?


---
**Pippins McGee** *June 06, 2016 10:39*

**+Chris E** according to most peoples opinions (and i say opinion because it does just seem to be peoples opinions..) curve side suppose to point towards laser source.



i tried several line tests tonight with the lens flipped either way, and achieved exact same focus distance, sharpness of the point, seems like same cut depth too, no matter which way it was flipped.. although some people report better results with it flipped one way, and others report better results with it flipped the other way... so go figure



so im personally going with curve side facing up (away from table) because most people seem to reiterate this.


---
**Christoph E.** *June 06, 2016 10:52*

Thanks for your Answer. Maybe i'll try to switch it too.



Imho that's stuff that really should be written in a Manual. The missing manual yet is the biggest Problem i got with that machine/software. ^^



What settings do you use for trying to cut that  Cardboard Cesar?


---
**Ned Hill** *June 06, 2016 12:55*

A lens with one convex side and one flat side is called a plano-convex lens.  You will get focusing in either orientation but it is much sharper with the convex (curved side) up (toward the laser).  See the bottom diagram here.  [http://folk.uio.no/walmann/Publications/Master/node8.html](http://folk.uio.no/walmann/Publications/Master/node8.html)

For a properly made lens having the flat side toward the laser will result in longitudinal spherical aberration (bigger spot). 




---
**Pippins McGee** *June 06, 2016 14:15*

**+Ned Hill**



[http://i.imgur.com/Y3WHfUf.jpg](http://i.imgur.com/Y3WHfUf.jpg)

i cut 2 lines across material on a slope, with lens facing each way.

I can't tell any difference.

at least, with my eyes anyway, maybe there is the slightest of slight differences but nothing major like everyone says there should be?


---
**Ned Hill** *June 06, 2016 14:25*

It looks to me like the line is sharper and more burn with the curved side up.  All this is really dependent on the a lens being properly made and that your mirrors are all properly aligned.   If the beam isn't hitting the center of the lens it's not going to focus well either way.


---
**Pippins McGee** *June 06, 2016 14:44*

**+Ned Hill** thanks, good point on the alignment. something I never got 100%, just 'close enough' and gave up.

so what you're saying could make sense as to why there isn't as much a difference as there should be.


---
**Cesar Padilla** *June 06, 2016 15:16*

hi guys I already flipped the lens and looks like it have a deeper cut I'm going to made  another alignment, thanks for the advice ☺


---
**HalfNormal** *June 06, 2016 15:34*

Make sure you are the correct distance away for your lens. Stock is approximately 50 mm.


---
*Imported from [Google+](https://plus.google.com/109463094459516755907/posts/fyFAPjpmVyR) &mdash; content and formatting may not be reliable*
