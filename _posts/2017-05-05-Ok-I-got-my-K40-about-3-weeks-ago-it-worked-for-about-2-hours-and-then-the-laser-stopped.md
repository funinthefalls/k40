---
layout: post
title: "Ok I got my K40 about 3 weeks ago, it worked for about 2 hours and then the laser stopped"
date: May 05, 2017 01:54
category: "Discussion"
author: "Maker Fixes"
---
  Ok I got my K40 about 3 weeks ago, it worked for about 2 hours and then the laser stopped. Well I graduated from a high school  vo tech electronics class back in 1980 and have not used the knowledge  it since. The problem  the mechanics worked but the laser did not fire, but it would fire from the power supply test button. (my machine has 2 test buttons).  So here I sit with no meters tools and a pile is junk in the room. I figured out the laser was good as well as the power supply since the lower test button worked. I decided it must be the upper control board bad since that test button would not fire the laser. So I bought one for $20 (which the sell reimbursed me for - great seller).   

Today the new board get here I am happy to get this junk working so I install the new board.... same issue.  Well I decided to buy a meter and it took me 10 minutes to work out the problem, the water sensor unit was not closing the circuit. there is no adjustment on it so I just ordered a new one. Until then I just bypassed   and watch the water line carefully. After talking to a friend that has one of these machine he said his lasted 2 weeks (the water sensor) It appears they are putting a very low end sensor so I suggest new users order one upfront. ( I got the cheapie from light object like $7.50) it could save you hours or headaches)  





**"Maker Fixes"**

---
---
**Mark Brown** *May 05, 2017 02:50*

Mine didn't even come with a water sensor.  I bought a super cheapey, but it sounds like it might be a hair better than that, Ican adjust the spring pressure somewhat.


---
**Steve Clark** *May 05, 2017 03:18*

What water sensor the pressure type? I have the same one but decided to add the impeller (Flow) type to the return line. Mine did not come with the sensor either.



A few people here have made comments to the effect that relying on just a pressure may be a problem if you get a pinched line. you still may have pressure but no flow. 



Made sense, so I ordered the flow one to replace the pressure one when I rebuild the cooling system with a Peltier refrigerant setup inline. The price is right if your willing to wait a few weeks to get it.



[ebay.com - Details about  Clear Water Flow Sensor Flowmeter Hall Flow Sensor Water control 1-30L/min](http://www.ebay.com/itm/262704181896?_trksid=p2057872.m2749.l2648&ssPageName=STRK:MEBIDX:IT)

 


---
**Kirk Yarina** *May 05, 2017 14:07*

That sensor outputs pulses rather than being an off/on switch.  How are you planning to adapt it?


---
**Mark Brown** *May 05, 2017 16:02*

I got one of these.  There's a reed switch encased in the side, and a plug with a magnet that the flow pushes.  So it is flow, not pressure.  And simple open or closed circuit.  There is a chance of it sticking though. ~shrug~



[ebay.com - Details about  Hot Sale White Plastic Shell Magnetic Water Flow Switch w Inner Outer Thread HP](http://www.ebay.com/itm/Hot-Sale-White-Plastic-Shell-Magnetic-Water-Flow-Switch-w-Inner-Outer-Thread-HP-/262595970526?hash=item3d23f085de:g:dj4AAOSw5cNYaF2X)


---
**Maker Fixes** *May 05, 2017 16:29*

I was a bit unsure which one to get I went with [lightobject.com - Water flow/ pressure sensor. Ideal for CO2 laser water protection](http://www.lightobject.com/Water-flow-pressure-sensor-Ideal-for-CO2-laser-water-protection-P815.aspx) for for 12 (with shipping) I wont be out a ton it it dont work




---
**Steve Clark** *May 05, 2017 16:45*

Kirk, My understanding is you could ignore the yellow pulse output wire and wire only the red and black. I could be wrong and there is no reed switch in this one. I'll test it when I get time. If doesn't work... I'm only out a few bucks and will have to order another type.


---
**Steve Clark** *May 05, 2017 17:08*

It will work Calvin, it's the one I will be replacing eventually. It's sensitivity to pressure is adjustable by a side knob on it (not the screw). The problem with them is the tubing connector. It is way to small and has no barbs. I was able to get it to work for the size water tubing I have by carefully melting a groove in it with a soldering iron, sliding a short piece of smaller tubing over it I had, then sliding on my water line over that and tie wrapping the whole thing tight.  BTW, I would toss the "T" they send with it as the ID is so small that it would restrict water flow in your cooling system. Here is a pic. how  I set it up.

![images/a43b5e044a5fc5c525b15379a807f6ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a43b5e044a5fc5c525b15379a807f6ff.jpeg)


---
**Adrian Godwin** *May 05, 2017 22:55*

**+Steve Clark** I don't believe that will work. I think they have a magnet on the impeller and the red/black wire supply power to a hall effect sensor (which senses magnetic field). So it needs 5V power all the time and produces pulses on the third wire.



This isn't difficult to use but needs a little electronics. Crudely, it can be done with a circuit called a retriggerable monostable, but these days it would usually be done with a small processor like an arduino. 



I will have such a circuit set up shortly and will be happy to share details. I think someone else here also mentioned doing one.  


---
**Steve Clark** *May 06, 2017 00:18*

So it sounds like I got the wrong one. Your explanation follows what I little understand about Hall Effects. I ordered on a whim (ordering other stuff at the time) but should have thought about that item.



 Yes,  I would be interested in the circuit. I've trying to understand arduino's and even just ordered a uno broad to learn what they are about.


---
**Adrian Godwin** *May 06, 2017 19:04*

**+Steve Clark** A bare minimum test could be as little as :



void setup() {

  

  // put your setup code here, to run once:



 // sensor input pin usually needs a pullup  pinMode(10,INPUT_PULLUP);  



  // drive a relay, initially off

  pinMode(8,OUTPUT);          

  digitalWrite(8,0);

}



void loop() {



  // put your main code here, to run repeatedly:



  // turn relay on when pulse is shorter than 50ms  digitalWrite(8, pulseIn(10,LOW,50000) > 0);

}



This assumes that the sensor generates a pulse faster than 50ms when OK. Mine runs at about 20ms with the standard pump, at about 0.7l/min. 



A more thorough implementation would calculate the speed more correctly (strictly, you want the period or frequency of the pulses, not the time it's at 0v), demand a higher flow rate (I initially set 30ms but it jittered on and off, presumably due to the pulsing of the pump) and probably check temperatures etc. too and show them on a display.  Featuritis is a terrible thing.


---
*Imported from [Google+](https://plus.google.com/113242558392610291710/posts/FTmzBadrU3T) &mdash; content and formatting may not be reliable*
