---
layout: post
title: "Just mine in as well. Initial inspection looks good"
date: June 14, 2016 21:16
category: "Discussion"
author: "Ben Marshall"
---
Just mine in as well. Initial inspection looks good. Packaging was really good(double boxed, foam b/w boxes and foam around k40. Parental duties have halted the full inspection. "Man that extraction fan fits like a glove!!!!!!" Said no k40 owner...ever

![images/7f632ba4cadcde6891a9aeee08071e63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7f632ba4cadcde6891a9aeee08071e63.jpeg)



**"Ben Marshall"**

---
---
**Jim Hatch** *June 14, 2016 21:31*

I'm wondering if there's a packaging pattern. Do ones shipped from China direct have better or worse packaging than those coming from a domestic distributor for instance. Mine was from China and like yours in basically bulletproof packaging.


---
**Ben Marshall** *June 14, 2016 21:35*

**+Jim Hatch** I'm not sure. I  could always reach out to the seller and ask. But nonetheless, there were no loose items. I picked up the box, walked around my yard to my basement door, placed it down on my saw bench and never heard a clink or clatter


---
**Ariel Yahni (UniKpty)** *June 14, 2016 21:39*

I believe mine was double boxed also from a domestic seller. Seen out there wooden boxes


---
**Ray Kholodovsky (Cohesion3D)** *June 14, 2016 21:41*

Got mine in as well today. Double boxed, all cardboard and eps foam. 


---
**3D Laser** *June 14, 2016 21:41*

Let that be one of your first upgrades unless you are a big fan of cancer 


---
**Ashley M. Kirchner [Norym]** *June 15, 2016 01:23*

Oh mine fit like a glove as well ... an XXL glove on an XXS hand ..


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 06:28*

Mine was double boxed from local Australia seller. Foam everywhere. Only real issue was the packaging of all the random parts. I found the 2 power adaptors only recently when I pulled the entire machine apart, having originally thought they must have forgot to give me some. I operated with the two power adaptors jammed up the back behind the rail since October last year haha.


---
**Ben Marshall** *June 15, 2016 07:17*

Got everything zeroed in. About to start installing software.


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/cY2MMsr2ZYX) &mdash; content and formatting may not be reliable*
