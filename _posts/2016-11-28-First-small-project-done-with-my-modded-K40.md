---
layout: post
title: "First small project done with my modded K40"
date: November 28, 2016 02:42
category: "Object produced with laser"
author: "ViciousViper79"
---
First small project done with my modded K40. Works pretty awesome.





**"ViciousViper79"**

---
---
**Jonathan Davis (Leo Lion)** *November 28, 2016 03:04*

Cool project


---
**ViciousViper79** *November 28, 2016 03:14*

**+Jonathan Davis** thanks. will be a gift for a workmate


---
**Jonathan Davis (Leo Lion)** *November 28, 2016 03:16*

**+ViciousViper79** I bet they will enjoy it.


---
**Don Kleinschnitz Jr.** *November 28, 2016 14:44*

Sweet, btw how did you end up connecting your pwm?


---
**David Cook** *November 28, 2016 16:27*

That's cool !


---
*Imported from [Google+](https://plus.google.com/+ViciousViper79/posts/4rHyAMhTxrx) &mdash; content and formatting may not be reliable*
