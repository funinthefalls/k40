---
layout: post
title: "So I've got my machine and all appears to be ok"
date: May 08, 2016 05:52
category: "Hardware and Laser settings"
author: "Ian Ferguson"
---
So I've got my machine and all appears to be ok. Is it possible to run the machine without firing the laser ie just to check out xy movement and comms from software. My machine has the nano board 



![images/73c8e0147875252caf1fff1bc837dd8c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73c8e0147875252caf1fff1bc837dd8c.jpeg)
![images/738a6f1637706212ceaea41ce632666d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/738a6f1637706212ceaea41ce632666d.jpeg)

**"Ian Ferguson"**

---
---
**Ian Ferguson** *May 08, 2016 06:17*

I mainly want to cut balsa and thin ply. Is there a good instructional anywhere on how to steps of getting a DXF file cut. I have coreldraw 12 and laser draw that shipped with the machine.


---
**Anthony Bolgar** *May 08, 2016 06:24*

Yes you can run without firing, just turn off the laser enable button on the main control panel. To get a DXF to cut, import it into CorelDraw, tweak any errors or changes you want to make, then send it to the laser using the CorelLaser toolbar.


---
**Anthony Bolgar** *May 08, 2016 07:25*

I would suggest getting an air assist nozzle and air source as your first modification to the laser, it makes an amazing difference in the performance of the machine.


---
**Ian Ferguson** *May 08, 2016 07:54*

Anthony thanks so much for your help. Yes going to get air assist nozzle made up. Are u using little compressor? Does it need big airflow ?


---
**Anthony Bolgar** *May 08, 2016 09:27*

I use an airbrush compressor, but a large aquarium air pump will also do the job.


---
*Imported from [Google+](https://plus.google.com/100713211036434462043/posts/BEP6STmhDx9) &mdash; content and formatting may not be reliable*
