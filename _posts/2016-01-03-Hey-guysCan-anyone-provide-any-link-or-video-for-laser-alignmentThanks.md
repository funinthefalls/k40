---
layout: post
title: "Hey guys.Can anyone provide any link or video for laser alignment.Thanks"
date: January 03, 2016 11:36
category: "Discussion"
author: "Dheeraj Arora"
---
Hey guys.Can anyone provide any link or video for laser alignment.Thanks





**"Dheeraj Arora"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 03, 2016 14:19*

[http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.co.uk/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)



These got me through when I was attempting (for a few days) to get my alignment spot-on. Turned out there were other issues causing misalignment (e.g. my Mirror #1, closest to the laser tube, was actually angled down because of a screw in the bracket of it. After some packing up it levelled it out & I've had no issues since).



There is a post here by Stephane, which points out a lot of the common things to check/look at.



[https://plus.google.com/117750252531506832328/posts/HXacpbV4ZF9](https://plus.google.com/117750252531506832328/posts/HXacpbV4ZF9)


---
**Juan Gianni** *January 18, 2016 18:28*

hi Dheeraj. I from Uruguay. I have a problem and maybe you can help me. 

buy KC40 3020 laser machine but the USB key does not work. you have the files on the USB key? Thanks very much !!


---
**Dheeraj Arora** *January 22, 2016 05:00*

**+Yuusuf Sallahuddin** Thanks for your help


---
**Dheeraj Arora** *January 22, 2016 05:02*

**+Juan Gianni** USB key does not have anything in it.It act as a security key.Install moshidraw software and insert the usb..It  should wok.If it does not work contact the person from which you have purchased the machine...


---
*Imported from [Google+](https://plus.google.com/118105035571850216408/posts/NjZbK2Ci9Bx) &mdash; content and formatting may not be reliable*
