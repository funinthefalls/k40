---
layout: post
title: "I've got a new K40, I added the air-assist lens"
date: June 10, 2016 01:12
category: "Hardware and Laser settings"
author: "Corey Renner"
---
I've got a new K40, I added the air-assist lens.  Can anyone recommend some power/speed settings for 3mm (1/8") plywood?  I have the digital controls.  Thanks, c





**"Corey Renner"**

---
---
**Derek Schuetz** *June 10, 2016 01:25*

I do 25% power at 150mm/min for 3mm MDF so it should be about the same 



Edit: forgot settings are mm/min


---
**Tony Schelts** *June 10, 2016 07:56*

I cut loads of 3mm ply @ about 5-7 MA and 7-8mm a second. I havent cleaned my lens for 2 months because I have the ari assist on all the time when I'm cutting. 


---
*Imported from [Google+](https://plus.google.com/116200173058623450852/posts/Lng3d7NTjEj) &mdash; content and formatting may not be reliable*
