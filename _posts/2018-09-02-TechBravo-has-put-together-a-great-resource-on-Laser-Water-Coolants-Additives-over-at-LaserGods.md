---
layout: post
title: "TechBravo has put together a great resource on Laser Water Coolants & Additives over at LaserGods"
date: September 02, 2018 20:18
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
TechBravo has put together a great resource on Laser Water Coolants & Additives over at LaserGods.



[https://www.lasergods.com/laser-water-coolants-additives/](https://www.lasergods.com/laser-water-coolants-additives/)





**"HalfNormal"**

---
---
**Timothy Rothman** *September 02, 2018 22:24*

Very good work TechBravo!


---
**Don Kleinschnitz Jr.** *September 02, 2018 23:22*

I think it's traditional practice in the open source world to link to the source when referencing others work. 



Note: RV antifreeze has been proven to  result in a conductivity not suitable for laser use. 

To my knowledge we have not completed adequate tests to know what the fluid conductivity threshold is for proper operation. We recommend only distilled water and algecide.

 


---
**HalfNormal** *September 02, 2018 23:43*

**+Don Kleinschnitz** If you look at the end of the article, his references are posted and your blog is #1!


---
**Don Kleinschnitz Jr.** *September 03, 2018 00:31*

**+HalfNormal** My bad .... I am used to information being linked as it is used, especially quotes.


---
**Tech Bravo (Tech BravoTN)** *September 03, 2018 07:01*

**+Don Kleinschnitz** i didn't see the above quote in my article but i did make sure to cite you multiple times where i used your material. its great work and i point people to your site often :) sorry it wasn't clear. 

![images/401af5e4b04b339c30013ad0aa336a36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/401af5e4b04b339c30013ad0aa336a36.jpeg)


---
**Tech Bravo (Tech BravoTN)** *September 03, 2018 07:01*

![images/6fd829e5fbc4319e656ee19c8af4c2d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6fd829e5fbc4319e656ee19c8af4c2d5.jpeg)


---
**Tech Bravo (Tech BravoTN)** *September 03, 2018 07:04*

and, for transparency, i pulled the rss feed of your blog and made a category just for you: [https://www.lasergods.com/category/blogs/dons-things-blog/](https://www.lasergods.com/category/blogs/dons-things-blog/). if this is something you had rather me not do by all means let me know and i will correct it.

[lasergods.com - Don's Things Blog Archives - LaserGods Laser Engraver Info Portal](https://www.lasergods.com/category/blogs/dons-things-blog/)


---
**Tech Bravo (Tech BravoTN)** *September 03, 2018 07:07*

ah i see, the rv antifreeze info above was not the quote you were referring to. if its okay i'd like to append the rv antifreeze section with the above info, with credit of course.




---
**Tech Bravo (Tech BravoTN)** *September 03, 2018 07:08*

**+Timothy Rothman** thanks, i didn't reinvent the wheel here, just took what i learned from others and aggregated it :)




---
**Don Kleinschnitz Jr.** *September 03, 2018 13:16*

**+Tech Bravo** I have no problem with the use of any of my original information for personal use. Commercial use requires an approval. 

One challenge is when you just use the RSS feed it sometimes references but does not provide a "Donate" button. I have the button on the side bar but not in each post. 

Also this approach copies the info to your site but never takes the user to my blog reducing traffic and  "Add" revenue, comments etc. 

The only link I find to the actual blog is the "Original Article" link present at the bottom after you click the "read more" on a post.



I fund my research from donations and add revenue :). None of this is a lot of money but enough to buy parts, tools etc. :).



That said I am happy to share all this information but also want to keep doing advanced research and engineering work for the Laser and CNC communities.


---
**Tech Bravo (Tech BravoTN)** *September 03, 2018 20:17*

I am revamping the way my site works with other blogs like yours and hakan's. I will pull just the headline and about 25 words of the 1st paragraph (just enough for the ajax fuzzy search to work). That way it is just a "teaser". They will have to click read more and be taken to the original article to get any useful information. That should address all of your concerns. If not, please let me know and i will find a different solution. Thanks again for all of your work and content.


---
**Brent Dowell** *September 13, 2018 16:15*

I'm running distilled water + Dawn + Tetra biocide right now, so I'm assuming that's about as good as it gets, conductivity wise.  



With winter coming, I'm a little worried about the temps in my garage dropping too much.  Other than an aquarium heater and running the pump 24/7, is there anything I can add to the coolant to keep it freezing that wont break the bank?  I've got some straight propolene glycol I could add, but don't want to if it will cause the conductivity of the coolant to rise.



Also wondering what you are using to measure the conductivity of the water and if there's something reasonably priced I could get to do the same measurements.



Thanks!


---
**Tech Bravo (Tech BravoTN)** *September 13, 2018 16:23*

**+Brent Dowell** people use that i think with some success. much of the data i used for the article was compiled from other sources, much from **+Don Kleinschnitz**. Those meters are fairly cheap on Amazon. I have a $15 one. Don mentions the exact equipment used in his measurements. Take a look at his article for more detailed tech specs: [donsthings.blogspot.com - K40 Coolant](http://donsthings.blogspot.com/2016/07/k40-k40-s-tips.html)




---
**Brent Dowell** *September 13, 2018 16:42*

Ahh, Thanks!  I really need to Read Don's site much more thoroughly.  So much good information there.




---
**Brent Dowell** *September 13, 2018 17:09*

Picked up one of the HM Digital COM-80 meters.  Looks like I've got another toy to play with. I do have an aquarium heater coming that I figure I'll use just for good measure when it gets real cold.




---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/8pFMQVd5bRH) &mdash; content and formatting may not be reliable*
