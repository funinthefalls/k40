---
layout: post
title: "Hello all, trying to figure out why my cooling water seems to be at the same voltage as a laser"
date: February 19, 2018 02:41
category: "Discussion"
author: "Cody Fitzgerald"
---
Hello all, trying to figure out why my cooling water seems to be at the same voltage as a laser. Any thoughts?





**"Cody Fitzgerald"**

---
---
**Joe Alexander** *February 19, 2018 03:14*

use distilled water only, tap water can be conductive




---
**Don Kleinschnitz Jr.** *February 19, 2018 04:17*

Usually means that your coolant is to conductive. What voltage?


---
**Ned Hill** *February 19, 2018 06:40*

Yeah it's a strange phenomena that happens when your cooling water becomes conductive.  We aren't completely sure why it happens. It can cause premature power supply failure as well.  Change your cooling water to distilled water and don't use any additives other than some algaecide if you need to. 


---
**Steve Clark** *February 19, 2018 17:50*

**+Don Kleinschnitz**  **+Ned Hill** I'm thinking this phenomena may have a use to let you know when it's time to change your water.  Say an approperate digital voltage display with a probe in the water flow and to earth ground maybe??


---
**Don Kleinschnitz Jr.** *February 19, 2018 21:02*

**+Steve Clark** 

I think a water conductivity tester is cheaper :). 



I think that **+HalfNormal** has an arduino based one that could be a direct measurement approach. 



......

The design  problem may be that even at low current the voltage is high enough to fry an IC....  I suspect the voltage is 100's of volts.



Also we would have to find out empirically what are good and bad values. 



A simple approach would be to try a voltage divider.... with one of those cheap DVMs [which may need a battery]. Then again, you can get a DVM from HF for $10, or free on some holidays, stick it across the bucket to ground.



[https://www.amazon.com/gp/product/B00C58JGE6/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00C58JGE6/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)





Here is a calculator: [http://www.ohmslawcalculator.com/voltage-divider-calculator](http://www.ohmslawcalculator.com/voltage-divider-calculator)





![images/ea4e23b23561885d34242773bf2b2b06.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ea4e23b23561885d34242773bf2b2b06.jpeg)


---
**HalfNormal** *February 20, 2018 01:23*

Bad water pumps can also cause "hot" water.


---
**Don Kleinschnitz Jr.** *February 20, 2018 02:14*

In still think a closed system may be better. 


---
**Cody Fitzgerald** *February 20, 2018 03:31*

Thanks all for the help I'll try distilled water next weekend. 


---
**Don Kleinschnitz Jr.** *February 20, 2018 03:36*

What are you currently using? 


---
**Cody Fitzgerald** *February 20, 2018 15:52*

Just tap water. Through the bucket was quite dirt, may have had something in it to add conductivity. 


---
**Don Kleinschnitz Jr.** *February 20, 2018 15:57*

**+Cody Fitzgerald** I have experienced tap water getting conductive as it ages. 



If you have water conditioning that can also alter your waters conductivity. I have a NUVO system and that increases my conductivity. 



Just say NO to all but distilled :)!


---
**Steve Clark** *February 20, 2018 16:27*

I agree with Don, distilled water only. I have a closed peltier system of only about two and a half liters and planned on changing it often. But it's now at three months with no problems. However time to change anyway.


---
*Imported from [Google+](https://plus.google.com/106730167694664517564/posts/P4DP3w2txbR) &mdash; content and formatting may not be reliable*
