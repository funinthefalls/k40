---
layout: post
title: "I'm about to pull the trigger on a ebay K40 and have a few quick questions for the proud owners"
date: February 09, 2018 21:38
category: "Discussion"
author: "Joe Gerten"
---
I'm about to pull the trigger on a ebay K40 and have a few quick questions for the proud owners. 



Can anybody recommend a seller they that shipped without the broken lens or tube?



I'll be breaking it down and rebuilding the works, so shoddy wiring is not a problem as long as the major components are not shithouse, but

 I  understand that some of them won't fit the Lightobject air assist nozzle without replacing components. Is it possible to 3d print an adapter?



How about heat absorption at the mirrors? Does any of the "Light-train" get hotter than expected? 



Aside from the obvious safety issues, has anybody had any major snags when it comes to mounting the guts onto a bigger xy table and completely abandoning the blue on white enclosure? I have a bunch of extra openbuilds extrusion left over from a previous project that might just work perfectly for that.



Thanks guys! This community has been a great resource during my research!





**"Joe Gerten"**

---
---
**HalfNormal** *February 09, 2018 22:14*

Where you are located makes a lot of difference in the seller preferences. Also the systems change frequently so quality is never very consistent. As long as you know what you are getting into, you will have fun no matter what!


---
**Joe Gerten** *February 26, 2018 19:59*

I received my laser! Double cardboard packaging. Opened it up, everything seems to be in order, none of the horror stories I've been reading about, though I did have to remove the plastic spacer from the ground screw. My model has a digital power meter with momentary buttons for  laser on/off, laser test and laser power. A rocker switch for for laser on-off. It's the blue power-supply version, and has a LED strip already wired into the supply, run along the back pf the build area.



Tube works, mirrors had been terribly aligned, but it cuts 1/4 inch lowes ply in 2 passes. 



For anybody looking for a quality supplier, I got mine from wowfreeshipping on ebay, and it took less than a week to arrive, and I couldn't be happier!


---
*Imported from [Google+](https://plus.google.com/103261447984370625545/posts/3dFyPzBPu2n) &mdash; content and formatting may not be reliable*
