---
layout: post
title: "Franco Lanza I got a compile error with the version I got from your gitlab.."
date: May 27, 2016 00:12
category: "Modification"
author: "Jon Bruno"
---
**+Franco Lanza** I got a compile error with the version I got from your gitlab.. I copied over all of the no cooler/no flowmeter files and get this:error: 'print_job_timer' was not declared in this scope



    print_job_timer.stop();





**"Jon Bruno"**

---
---
**Jon Bruno** *May 27, 2016 00:22*

I just commented the call in temperature.cpp as per the comments it there to shut the unit down if the cooler goes down.. lol Unfinished feature.. I guess I grabbed it before it was done.. all good now, I got it to compile.


---
**Franco “nextime” Lanza** *May 27, 2016 03:03*

Thanks, it was a bug, an erroneous call to print_job_timer instead of print_job_counter, now fixed.


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/A91eXiZVmYz) &mdash; content and formatting may not be reliable*
