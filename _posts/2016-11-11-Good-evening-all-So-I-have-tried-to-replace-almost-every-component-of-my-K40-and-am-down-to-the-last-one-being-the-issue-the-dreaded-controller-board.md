---
layout: post
title: "Good evening all- So I have tried to replace almost every component of my K40, and am down to the last one being the issue, the dreaded controller board"
date: November 11, 2016 22:37
category: "Smoothieboard Modification"
author: "Bradley Blodgett"
---
Good evening all- So I have tried to replace almost every component of my K40, and am down to the last one being the issue, the dreaded controller board. I just got my Smoothieboard yesterday and am ready to get started. I have one big question, I see a lot of people using the middleman to use the optical end-stops that are built into the system. Is there anything stopping me from bypassing those electronics, connecting the stepper directly to the smoothie, and just using mechanical end-stops?





**"Bradley Blodgett"**

---
---
**Ariel Yahni (UniKpty)** *November 11, 2016 22:47*

Nothing


---
**Don Kleinschnitz Jr.** *November 11, 2016 23:39*

As  **+Ariel Yahni** says nothing stops you and it will work. 

You will need to fabricate a way to manage moving cables. 

I worry about running wire vs flat cable in movable harnesses as wire has a tendency to "work harden" and break over many cycles. Intermittent wiring in the gantry components would be a devil to find. Certainly failure would take some time to occur.



My rules of thumb when I have to move cable:

.. Move the least amount of wire

.. Use large radius's and wrap a loop if you can 

.. Do not use solid wire

.. Use a drag chain to guide & protect the cable




---
**Bradley Blodgett** *November 13, 2016 14:14*

Thanks **+Don Kleinschnitz**​! I definitely have this practice down from operating 3d printers with the same type of movement issues with wiring. I have another question though. In the guide, there is an unused 5v connection on the PSU, is there a reason not to use this for the 5v power needed for the smoothie board rather than using some sort of external 5v? Also what is the amperage requirement for this power?


---
**Don Kleinschnitz Jr.** *November 13, 2016 17:57*

**+Bradley Blodgett** sorry for the long answer but your question suggests a "it depends" answer.



The LPS capacity is:

+24VDC @1amp

+5VDC @ 1amp

Internally these are created from separate sections of the LPS so they have a total capacity of 29VA.



I do not know the power requirements for K40+smoothie when under full dynamic load.



Editorial:

I am skeptical that 24VDC@1 amp can run everything in this box at full power. I have not measured the dynamic load so at this point it is a suspicion. Power budgeting & verification is on my list of to-do's.



....................

How you wire the DC power depends on what you want.

........................

You can configure DC power to the Smoothie in three basic ways:



Way #1

+24VDC from the LPS 

+5VDC from regulator installed on-board smoothie

Capacity: 24VA

Pro's: simple two wire connection

Potential Con's: marginal power for anything else, local display, finder led, Z table, pump etc. 



Way #2 (this answers your question from above)

+24VDC from LPS

+5V from LPS

Capacity: 29VA

Pro's: more capacity 5VDC + 12VDC

Potential Con's: marginal power for anything else, local display, finder led, Z table, pump etc. 



Way #3

+24VDC from external supply with selected capacity

+5VDC from regulator installed on-board smoothie

Capacity: based on external supply choice

Pro's: full DC capacity for K40 + expansion. Simple 2 wire DC wiring

Potential Con's: more complex wiring of AC mains (to add on supplies) and purchase cost of external supplies.



As you can logic there are other combinations I did not cover.

.........................

Many of the users on this forum use Way#1 and 2, so it seems to work.



I chose Way#3 because:

1. I want as much isolation as possible from the HV laser supply. In my configuration only the grounds are common.

2. I wanted to make sure that the motors had full capacity under dynamic loads. I do not know the actual loads so I took the safe route :).

3. I wanted extra capacity for future accessories like the local GLCD and driving the stepper for the lift table. 



BTW: I currently use the LPS 24VDC & 5VDC  for other accessories such as external lift table and the finder LED.



You can employ Way#1 or 2 and then measure the PS voltage to insure that it is not drooping.


---
**Bradley Blodgett** *November 13, 2016 20:50*

Thanks for the great info **+Don Kleinschnitz**​. I managed to scrounge up another power supply for the 5v, and anything else extra I may want to add in the future. I have been working on it the better part of a day now, and have to walk away from troubleshooting x-axis movement issues. Y is working fine, but X grinds and is jerky. I boggles my mind that I can keep multiple 3d printers up and running, but this change has me in a tailspin.


---
*Imported from [Google+](https://plus.google.com/+BradleyBlodgett/posts/QDDxZw7fjDU) &mdash; content and formatting may not be reliable*
