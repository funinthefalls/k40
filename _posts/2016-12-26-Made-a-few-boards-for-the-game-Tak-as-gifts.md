---
layout: post
title: "Made a few boards for the game \"Tak\" as gifts"
date: December 26, 2016 07:10
category: "Object produced with laser"
author: "Tev Kaber"
---
Made a few boards for the game "Tak" as gifts. 



Bought a plank of decent wood at Lowes, cut it into square sections. 



I made a plywood jig for alignment and engraved the diamond shapes at 5mA/400mm, then did a 5mA/40mm cut of the lines (including outline of the diamonds). 



A quick sanding to clean up the surface, then a coat of stain/sealer and I set them aside to dry. 



Tomorrow I'll give them a quick buff with superfine steel wool and maybe glue felt to the bottom and they'll be good to go. 



Pretty fast turnaround, did 4 of them in about an hour. 

![images/9156dde74d445f10c00ea1e3cc20469c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9156dde74d445f10c00ea1e3cc20469c.jpeg)



**"Tev Kaber"**

---


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/J3R2CTCZwvb) &mdash; content and formatting may not be reliable*
