---
layout: post
title: "I am doing leather craft, but I don't know what is a good size(thickness) for leather for cutting and engraving?"
date: March 17, 2016 18:59
category: "Discussion"
author: "Trinh Ta"
---
I am doing leather craft, but I don't know what is a good size(thickness) for leather for cutting and engraving? could you share your experience about leather cutting and engraving on K40?





**"Trinh Ta"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 17, 2016 20:37*

<2mm thick is difficult to engrave (it wants to burn straight through it, even at lowest power). So for engraving, 2mm thickness at least. Cutting is difficult on 3-4mm thickness. So for cutting, it works best on <3mm.



So ideally, for cutting & engraving, somewhere around 2-2.5mm thickness is probably pretty good.


---
*Imported from [Google+](https://plus.google.com/114886757826264619236/posts/QnCEEGQy3EN) &mdash; content and formatting may not be reliable*
