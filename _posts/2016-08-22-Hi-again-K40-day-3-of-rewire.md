---
layout: post
title: "Hi again... K40 day 3 of rewire..."
date: August 22, 2016 18:43
category: "Smoothieboard Modification"
author: "Jez M-L"
---
Hi again... K40 day 3 of rewire... The two grounds that are on the ribbon cable controller end pins 6&7, where am I connecting these to a smoothieboard? or doesn't it really matter?





**"Jez M-L"**

---
---
**Scott Marshall** *August 22, 2016 18:50*

Any of the "gnd" pins will do, as will either power supply "-"



If you're running from a common power supply (as in the stock K40), that connection should already be made. (just throw an ohmeter on it to check)



If you hook up all the grounds individually, you'll create a bunch of ground loops and this can lead to noise issues. Best way is 1 ground for power straight to the powersupply and 1 for signal. That way any voltage drop in the power supply wiring doesn't get introduced into the signal circuits



If you do connect multiple grounds, connect them to a common point, using what is known as "star" grounding (as opposed to daisy chaining)



The FFP cable pins 6 and 7 are tied together at the ends, so just pick 1.



It can be a complicated subject, but it boils down to using a  minimum of ground connections, star connected.



Scott


---
**Jez M-L** *August 22, 2016 18:55*

Cheers Scott... I think that I'll put them on the endstop gnd's.  Any objections?


---
**Scott Marshall** *August 22, 2016 19:02*

Nope. It's there, the power supply or the ribbon cable. The Endstop connector isn't used, so it'a a good place to tie in.


---
*Imported from [Google+](https://plus.google.com/103448790120483541642/posts/Estw5WxFYFK) &mdash; content and formatting may not be reliable*
