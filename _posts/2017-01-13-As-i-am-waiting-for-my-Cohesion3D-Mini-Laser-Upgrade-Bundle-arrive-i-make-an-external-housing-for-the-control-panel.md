---
layout: post
title: "As i am waiting for my Cohesion3D Mini Laser Upgrade Bundle arrive i make an external housing for the control panel ..."
date: January 13, 2017 10:53
category: "Modification"
author: "Kostas Filosofou"
---
As i am waiting for my Cohesion3D Mini Laser Upgrade Bundle arrive i make an external housing for the control panel ... Did i forget anything?





I am thinking to put also the PSU in the external housing without the Flyback Transformer ( i Leave it in the laser housing) and make an extension cord (the three cords) from the external housing to the Transformer.

![images/6190c6d6a215fd3caddaab9a3649ffa3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6190c6d6a215fd3caddaab9a3649ffa3.jpeg)



**"Kostas Filosofou"**

---
---
**Don Kleinschnitz Jr.** *January 13, 2017 14:54*

You might consider switches for vacuum and air assist.

A digital voltmeter to tell you the pots setting is very useful.

I hard wired the light cause it makes it obvious the machine is on. I Also hard wired the pointer as I found I never need to turn it off and it reminds me where the beam is.



If I understand it correctly you are planning to remove just the flyback from the supply and remote it?

I would recommend against that?



Two reasons:

1. that connection is quite electrically noisy and running it outside the case can create noise problems and potentially make the output unreliable.

2. the drive circuit may not like the additional capacitance and inductance.



What advantage do you hope to gain by this change to the LPS.



![images/41bdb75436a9bbd63543c395d9c0c077.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41bdb75436a9bbd63543c395d9c0c077.jpeg)


---
**Kostas Filosofou** *January 13, 2017 16:33*

**+Don Kleinschnitz**​ as for the vacuum you are right maybe I make another hole... For the air assist I use airbrush compressor and is for general porpuse and it has his own on off switch.. as for the LPS is already separate from the flyback (is not attached to the board)from my previous setup and I want to gain better cooling to the board... I thing that is the reason that burned my PSU in the first time.



Can you tell me a little bit more about the voltage meter is really necessary?

![images/05c328cb1db5705ffe549cd09b81c74b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/05c328cb1db5705ffe549cd09b81c74b.jpeg)


---
**Don Kleinschnitz Jr.** *January 13, 2017 16:52*

I assumed you have a K40 so this may not apply to your situation.

The power control pot has no graticule making it hard to repeat a setting. Putting a digital voltmeter across the pot gives you a value that you can easy record and repeat.

[https://goo.gl/photos/WggMF1iacjrRwBtPA](https://goo.gl/photos/WggMF1iacjrRwBtPA)


---
**Kostas Filosofou** *January 13, 2017 17:05*

**+Don Kleinschnitz** ok I understand.. and where and how I connect the display?


---
**Andy Shilling** *January 13, 2017 17:08*

**+Don Kleinschnitz** could you let me know were you got that from please.


---
**Don Kleinschnitz Jr.** *January 13, 2017 17:23*

**+Andy Shilling** [https://www.amazon.com/gp/product/B00C58QIOM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00C58QIOM/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Andy Shilling** *January 13, 2017 17:30*

Thank you, I looked on eBay but I only saw a couple with two wire rather than three so want sure they would do the job.


---
**Kostas Filosofou** *January 13, 2017 17:46*

**+Don Kleinschnitz**​ never mind I get it.. I connect it parallel with my potentiometer right?


---
**Don Kleinschnitz Jr.** *January 13, 2017 18:40*

**+Konstantinos Filosofou** you must use the one I linked you to. A three wire is necessary to get full voltage.



[https://goo.gl/photos/6mCVw5N6hNsRwVU57](https://goo.gl/photos/6mCVw5N6hNsRwVU57)


---
**Abe Fouhy** *January 13, 2017 18:42*

**+Konstantinos Filosofou** where'd you get the psu? $?


---
**Kostas Filosofou** *January 13, 2017 18:56*

**+Don Kleinschnitz** ok thanks for the tip!


---
**Kostas Filosofou** *January 13, 2017 18:58*

**+Abe Fouhy**​ from here [ebay.de - Hohe Qualität 50W Netzgerät für CO2 Lasergravur Schneidemaschine 110V-220V   &#x7c; eBay](http://www.ebay.de/itm/112034954722)


---
**Don Kleinschnitz Jr.** *January 13, 2017 19:10*

**+Konstantinos Filosofou** BTW you might consider upgrading to this pot. The stock ones are low resolution and fail. 

[amazon.com - Uxcell 10-Turn Rotary Wire Wound Potentiometer, 5K Ohm 6 mm Round Shaft, Blue: Amazon.com: Industrial & Scientific](https://www.amazon.com/gp/product/B00VG93UD8/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)


---
**Abe Fouhy** *January 13, 2017 19:18*

**+Konstantinos Filosofou** ldid you get a bigger tube too?


---
**Kostas Filosofou** *January 13, 2017 19:47*

**+Don Kleinschnitz** I put that on the list also :)


---
**Kostas Filosofou** *January 13, 2017 19:47*

**+Abe Fouhy** is on the way..


---
**Abe Fouhy** *January 13, 2017 19:54*

Cool man. Did you have to modify the rear of the case to fit it?


---
**Kostas Filosofou** *January 13, 2017 20:20*

**+Abe Fouhy** I am hoping to do that this weekend


---
**Abe Fouhy** *January 13, 2017 20:21*

Oh wow. That's amazing. Btw your new inside case is awesome! Great work bud


---
**Kostas Filosofou** *January 13, 2017 20:27*

**+Abe Fouhy** thanks a lot :)


---
**Joe Spanier** *January 14, 2017 03:06*

How did you get the chiller control up there


---
**Kostas Filosofou** *January 14, 2017 12:01*

**+Joe Spanier** i don't have any chiller... This a normal 12v temperature controller that I bought from eBay


---
**Abe Fouhy** *February 19, 2017 15:55*

**+Konstantinos Filosofou** Any news on the larger tube case modification?


---
**Kostas Filosofou** *February 21, 2017 02:32*

**+Abe Fouhy** have to wait for a while.. :) I try my luck with the 40w for now ..


---
**Abe Fouhy** *February 21, 2017 03:28*

How are you liking your huge build area


---
**Kostas Filosofou** *February 24, 2017 21:17*

**+Abe Fouhy** Is huge like you say :) 

![images/1660ee4ce101069fd3f256ed44818480.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1660ee4ce101069fd3f256ed44818480.jpeg)


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/GQXMU6rv6MZ) &mdash; content and formatting may not be reliable*
