---
layout: post
title: "Hi laser geeks. Can anyone tell me the focal distance for the standard k40 setup please, I have removed the table and am struggling to get the perfect distance :) thanks"
date: June 27, 2015 09:28
category: "Hardware and Laser settings"
author: "grahame dale"
---
Hi laser geeks. Can anyone tell me the focal distance for the standard k40 setup please, I have removed the table and am struggling to get the perfect distance :) thanks





**"grahame dale"**

---
---
**David Wakely** *June 27, 2015 10:30*

Hi there,



The focus distance varies from machine to machine. The quickest way to get the perfect distance is to get a block of wood and place it in the machine at an angle so one end is lower and the other higher then cut a line on low power. The part where the line is at it thinest and sharpest is your ideal focus height. Measure it and then cut yourself a piece of acrylic that height then you have a focus tool :).



Good luck!


---
**Jim Coogan** *June 27, 2015 15:32*

For the K40 the standard lens is 50.8mm.  Place your parts that distance from the surface when engraving and half way through the material when you are cutting.


---
**quillford** *June 27, 2015 20:36*

It may be in your manual. The manual with the one I have says 5cm.


---
*Imported from [Google+](https://plus.google.com/110693493789999414301/posts/5hJXfoU2ref) &mdash; content and formatting may not be reliable*
