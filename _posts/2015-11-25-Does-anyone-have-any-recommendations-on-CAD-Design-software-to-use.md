---
layout: post
title: "Does anyone have any recommendations on CAD/Design software to use?"
date: November 25, 2015 14:20
category: "Software"
author: "Nathaniel Swartz"
---
Does anyone have any recommendations on CAD/Design software to use?  In the past for 3d model for 3d printing I've used blender, tinkercad, and very often sketchup.  For the cutting I've used mostly inkscape and a bit of sketchup but anything else I should try?  Any good recommendations on setting up sketchup for doing 2d work?





**"Nathaniel Swartz"**

---
---
**Coherent** *November 25, 2015 18:07*

If you have or can get Autocad, (even an older version) I personally think it's the best and fairly simple to use once you've worked with it a bit. I have Aspire, ArtCam for cnc router work and Inventor for 3d, but Autocad is what I go to for fast accurate 2d drawings. Tons of support and tutorials out there for it also. I've played with Sketchup and others. It & others work but to me have limitations and user interface/tool issues when compared to Autocad or Inventor. Part of that may be I'm proficient and comfortable with them, which always makes things easier. For simple stuff and text, Corel seems to work good and with the Laser plugin, keeps things simple and avoids exporting & importing files.


---
**Stephane Buisson** *November 25, 2015 19:58*

3D print: Sketchup, Meshmixer (boolean and finishing touch), Sculptris (with wacom tablette)

Blender (as swiss army knife of files format conversion)

Lasercut: Inkscape, Visicut



My next challenge will be a OX CNC, looking into software for that now.


---
**charlie wallace** *November 28, 2015 16:45*

check out Fusion360


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/B2dFDXkxC1v) &mdash; content and formatting may not be reliable*
