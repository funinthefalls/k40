---
layout: post
title: "Might help someone in a pinch Originally shared by Alex Krause Needed a quick and dirty led test base while I wait on my led strip to come from Amazon..."
date: July 17, 2016 06:10
category: "Repository and designs"
author: "Alex Krause"
---
Might help someone in a pinch 



<b>Originally shared by Alex Krause</b>



Needed a quick and dirty led test base while I wait on my led strip to come from Amazon... In the mean time I took apart a 1$ super bright 9 led flash light from Walmart and made my own keeping the button for the circuit of course :) 

P.S. Batteries were included :P



![images/ac5c7f93d1077780a5a33479ed4296e7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ac5c7f93d1077780a5a33479ed4296e7.jpeg)
![images/b0ec7920430a7182594471d9d0e43335.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0ec7920430a7182594471d9d0e43335.jpeg)

**"Alex Krause"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 17, 2016 14:40*

For $1, that's a decent setup. Nice work.


---
**Ned Hill** *July 17, 2016 15:44*

Great idea.


---
**Jim Hatch** *July 17, 2016 16:29*

Dollar Stores and Dollar General also usually carry a lot of these led lights you can turn into light sources. Same for Harbor Freight where they often have coupons for free 27 light LED flashlight.


---
**HalfNormal** *July 17, 2016 19:06*

**+Jim Hatch** 



[http://hackaday.com/2016/03/08/turn-a-free-flashlight-into-led-strips/](http://hackaday.com/2016/03/08/turn-a-free-flashlight-into-led-strips/) 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/VrsQhbDGZoZ) &mdash; content and formatting may not be reliable*
