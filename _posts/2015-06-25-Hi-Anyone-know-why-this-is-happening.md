---
layout: post
title: "Hi, Anyone know why this is happening"
date: June 25, 2015 15:41
category: "Discussion"
author: "David Wakely"
---
Hi,



Anyone know why this is happening. When i mirror a textbox upside down it engraves the correct way up?!? I am using CorelLASER x7. Anyone any idea?

![images/11099710a758a6453a818795c1626542.png](https://gitlab.com/funinthefalls/k40/raw/master/images/11099710a758a6453a818795c1626542.png)



**"David Wakely"**

---
---
**Jon Bruno** *June 25, 2015 20:02*

Looks like the special formatting doesn't transfer... thats interestingly odd. your text is technically vertically inverted.. what if you were to rotate the normal text 180 degrees to be horizontally inverted will it then carry over?


---
**Jon Bruno** *June 25, 2015 20:03*

Unless of course if you're looking to do a reflecting pool type text layout, then that may be an issue...


---
**David Wakely** *June 25, 2015 21:33*

I'll give it a go with rotation. Just strange that this happened!! I thought I was going mad when it was engraving!


---
**Jon Bruno** *June 25, 2015 21:34*

Lol I can understand that


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/R6RDf8pFvUp) &mdash; content and formatting may not be reliable*
