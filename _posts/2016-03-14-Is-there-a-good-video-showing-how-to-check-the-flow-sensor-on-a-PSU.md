---
layout: post
title: "Is there a good video showing how to check the flow sensor on a PSU"
date: March 14, 2016 03:15
category: "Discussion"
author: "3D Laser"
---
Is there a good video showing how to check the flow sensor on a PSU





**"3D Laser"**

---
---
**Anthony Bolgar** *March 14, 2016 07:46*

I am confused, a PSU doesn't have a flow sensor. but To check a flow sensor, a simple multimeter set to check continuity should be all you need.


---
**Jonathan Tzeng** *March 14, 2016 20:01*

[https://plus.google.com/105850439698187626520/posts/1iHXXZ5yKyk](https://plus.google.com/105850439698187626520/posts/1iHXXZ5yKyk)


---
**Anthony Bolgar** *March 15, 2016 01:19*

I sole this project from Jon( lol, with his blessing) and am maintaining it at [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/cPcJi3qtYK1) &mdash; content and formatting may not be reliable*
