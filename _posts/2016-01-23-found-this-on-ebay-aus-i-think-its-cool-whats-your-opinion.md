---
layout: post
title: "found this on ebay aus i think its cool whats your opinion"
date: January 23, 2016 07:52
category: "Modification"
author: "beny fits"
---
found this on ebay aus 

i think its cool whats your opinion 

[http://www.ebay.com.au/itm/281911665310?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/281911665310?_trksid=p2055119.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)





**"beny fits"**

---
---
**I Laser** *January 23, 2016 09:41*

Looks neat, not sure what the mods are actually worth and how well it would run. The blown tube is a bit of an unknown too.



Tubes are pretty fragile and getting one delivered intact here is apparently a lucky dip!



For 1k I'd also want to see it in action, which is kind of hard given the state it's in.


---
**Sean Cherven** *January 23, 2016 12:25*

I'd say there's too much going on inside there... To the point I wouldn't trust it...


---
**Joseph Midjette Sr** *January 24, 2016 01:44*

Too much. You can upgrade a $350 model and have a better machine that you built yourself and know what's in it. 


---
*Imported from [Google+](https://plus.google.com/116814442468846937788/posts/1mFZ6KTikVA) &mdash; content and formatting may not be reliable*
