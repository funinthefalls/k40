---
layout: post
title: "Can anyone please help me? I removed the mechanism from my laser in order to take out and modify the exhaust vent"
date: September 24, 2016 18:51
category: "Hardware and Laser settings"
author: "timb12957"
---
Can anyone please help me?

I removed the mechanism from my laser in order to take out and modify the exhaust vent.  After installation, when I powered up the machine, the head tried to find itself as usual. When it reached the rearmost position it did not stop trying to move and continued bouncing against the back of the machine.  While trying to figure out what was going on, I used a piece of paper to insert into the U shaped sensor which stops the head from moving backward. It then stopped tying to move backward. I now realized that the small metal plate meant to block the sensor when it reached its far back position was bent and had not been triggering the sensor. I straightened it and checked manually that the stop plate was entering the sensor correctly. However, now every time I turn the machine on, instead of moving to the rear to search for the back most position, the heads first movement is to the front of the machine.  It stops as if it had reached the rear of the machine, pauses, then moves slightly forward and to the right as if it has set this as home position. I have tried moving the head to the rear most position with the plate engaged in the sensor before powering up but I get the same result. Can anyone tell me how to set this back correctly? 





**"timb12957"**

---
---
**Ariel Yahni (UniKpty)** *September 24, 2016 18:54*

**+timb12957**​ I had the exact same problem and was resolved when one of the cables for that endstops was out correctly back into place. So check every cable is doing solid contact 


---
**Jim Hatch** *September 24, 2016 21:39*

**+Ariel Yahni**​ me too. Had one of the cables in backwards.


---
**timb12957** *September 25, 2016 01:42*

I have checked the cables. They are all in as they should be. 


---
**Ariel Yahni (UniKpty)** *September 25, 2016 01:46*

Just to note they could be where they need to be but a bad connection could still be present.


---
**timb12957** *September 25, 2016 01:56*

I tried to make sure they are in tight. At first the head did move to the rear as it always had, but failed to stop(because the stop plate was not entering the sensor) No cables were moved, yet after I used a scrap of paper to insert into the sensor and stop the rearward movement, that is when it began to always move forward upon power up.


---
**Ariel Yahni (UniKpty)** *September 25, 2016 01:57*

Are this mechanical or optical? 


---
**timb12957** *September 25, 2016 02:00*

The stop is optical


---
**Ariel Yahni (UniKpty)** *September 25, 2016 02:02*

Ok someone recently had the same issue so if cabling it's OK then the other suggestion would be cleaning them


---
**timb12957** *September 25, 2016 02:05*

Ok I can try that. But to point out, the carriage is finding a home position and stopping. Just not in the correct place


---
**Don Kleinschnitz Jr.** *September 25, 2016 02:19*

Does sound like a cable problem although weird it changed when you tested with paper, what about the stepper cable?

Here is a link to my K40-S schematic which has details of the end stops in case that helps. Ignore all but the end stop intfc tab. Just so happens that I am in the process of wiring my end stops so I am checking these drawings as we post.

[digikey.com - SchemeIt &#x7c; Free Online Schematic Drawing Tool &#x7c; DigiKey Electronics &#x7c; K40-S 2.1](http://www.digikey.com/schemeit/project/k40-s-21-8SM4SO8200E0/)


---
**Alex Krause** *September 25, 2016 02:20*

I just helped a guy yesterday troubleshoot this problem his was the result of bad solder joints on the pcb the optical endstop is on


---
**Alex Krause** *September 25, 2016 02:20*

Can you take a picture for us of the endstop


---
**timb12957** *September 25, 2016 02:23*

I will need to remove the carriage again I think, but I will try to get a pic


---
**timb12957** *September 25, 2016 02:33*

I remembered I have just recently acquired an inspection camera. Does this pic help? 

![images/96ee6d9a939ac668eb26451b282b148e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96ee6d9a939ac668eb26451b282b148e.jpeg)


---
**timb12957** *September 25, 2016 02:53*

Better Pics of sensor PC board. Please remember, the issue is not that the carriage is not stopping, It is stopping at the wrong location. (about midway from front to back)

![images/0e9f4478ac418af302b951679f7de487.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0e9f4478ac418af302b951679f7de487.jpeg)


---
**timb12957** *September 25, 2016 02:54*

Sensor pic two

![images/57fe6de98e04d1a8eaed6325982bf65a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57fe6de98e04d1a8eaed6325982bf65a.jpeg)


---
**Don Kleinschnitz Jr.** *September 25, 2016 03:09*

I don't see any thing wrong except the sensor looks like it is crooked. Could it have been impacted by the interposer and now has an intermittent. If the collector of the receiver side opened the pull-up could make the signal go high indicating that it is at home.

I would try reheating the sensor leads.


---
**timb12957** *September 26, 2016 02:04*

Thank you all for the suggestions but so far nothing has helped😩 I can only hope someone has more insight.


---
**Alex Krause** *September 26, 2016 02:06*

There is always replacing them with mechanical endstops... they are cheap and from my experience often more reliable


---
**timb12957** *September 26, 2016 02:14*

I do not think the issue is with the end stop or sensor. As a matter of fact, the sensor does not ever come into play. As stated before, the gantry use to move to the rear till the sensor was triggered then stop. Now it NEVER moves backward. Instead it moves FORWARD to about the middle of the machine, stops there (no sensor to trigger it to stop) and sets a home position in the middle of the machine. Seems to me it is a programming issue in the main control board




---
**Don Kleinschnitz Jr.** *September 26, 2016 02:50*

If I understand: after reinstalling, the gantry moved to the rear but did not stop.  

Now it moves forward to the middle and stops.

I don't see how it can be a programming issue as it worked before you took the gantry out and now it doesn't yet the controller code has not changed?

It doesn't make sense that it moves backward but it also doesn't make sense it stops?

Is the USB cable disconnected while testing and does this behavior occur after power up?




---
**timb12957** *September 26, 2016 03:12*

Currently no matter where the gantry is positioned (even fully forward)  it never moves toward the rear at power up. I have tried with and without the USB connected to my computer


---
**Don Kleinschnitz Jr.** *September 26, 2016 12:57*

**+timb12957** it used to work before you took the gantry out right?


---
**timb12957** *September 26, 2016 13:18*

Yes It worked correct before gantry was removed. If you read previous posts you can see how the failure began due to the interrupter plate being bent and not entering the sensor, to stop it in the rear most position. It tried to continue backward and what sounded like the belt jumping on the pulley could be heard. Several times I turned the power off, manually pulled the gantry forward, then powered back on.  Each time it tried to move to the rear,  never stopping until the power was switched off. After this sequence occured about 5 times I discovered and corrected the bent interrupter plate. However without going to the rear, it then began to ONLY move toward the front of the machine during power up. 


---
**Don Kleinschnitz Jr.** *September 26, 2016 13:53*

Overall from the scenario it seems that the sensor or breakout board must be damaged. I have no idea why damaging it would cause the motor to go the wrong direction and stop. 



Not sure what to try next but if you have a meter that would allow you to make measurements we can try an measure the sensor while interrupting it. You would have to remove it from its mount?


---
**Ariel Yahni (UniKpty)** *September 26, 2016 14:02*

**+Don Kleinschnitz**​ I did have this symptom from the get go with my unit. A bad solder on one of the cables from the mechanical endstop made the motor go and try to home in the other direction. Not sure exactly was the relationship but I have seen other users lately with the same issue


---
**Don Kleinschnitz Jr.** *September 26, 2016 14:08*

**+timb12957** based on **+Ariel Yahni** post I would try and ring out or test the signals from the end stop board and verify it was working correctly?


---
**timb12957** *September 26, 2016 14:36*

I do have a multi meter and a very basic knowledge of testing circuits. However I do not understand the term "ring out" nor do I know what to look for to test the signals. As well unless I disconnect the stepper motor for the Y axis,  even if removed from it's mount, the end stop would be connected by the cables to the gantry which is in motion making it difficult to test the circuit. Again let me say thanks to all of you guys for not giving up on trying to help me resolve this.


---
**Ariel Yahni (UniKpty)** *September 26, 2016 14:41*

Search for how to test continuity with a multimeter this does not need to be done with the endstop connected. What we want is to make absolutely sure the issue is not the endstop or cabling. BTW I will say that if this machine is under warranty start to include the supplier on the issue just in case you need a replacement part


---
**timb12957** *September 26, 2016 15:28*

When I get home this evening I will test for continuity on the end stop sensor. The machine is only 2 mos. old so is still under warranty. I do wonder how much success anyone has had with warranty service with these machines since they are sold by so many different eBay vendors.


---
**Ariel Yahni (UniKpty)** *September 26, 2016 15:33*

If you provide good info and a serious claim they will warrant it


---
**Don Kleinschnitz Jr.** *September 26, 2016 15:38*

**+timb12957** Ok, see if we can do this remotely :). 



Check "continuity" on the Y end stop as the first item.



Put your meter on the lowest ohms scale or if you have a mode that lets you get an audible, use it. Verify you are set up correctly by shorting the probes together to get zero ohms and/or a beep.



Use the schematic on this blog post: [donsthings.blogspot.com - K40 Optical Endstops](http://donsthings.blogspot.com/2016/06/k40-optical-endstops.html)



Check these traces from connector C10 to their endpoint, per the schematic:

...5VDC to one end of R3

...The other end of R3 to the ANODE of the sensor

...-YHome to the Collector of the sensor

...GND to the Emitter and Cathode of the sensor



This will test for opens but still may not show an intermittent. 


---
**timb12957** *September 26, 2016 17:18*

I will do these steps when I get home later this evening, thanks guys.


---
**Don Kleinschnitz Jr.** *September 26, 2016 17:28*

Coincidence: Turns out I am reinstalling the end stop back in my machine today after removing them so that I could trace the circuits and iverify the interface to my smoothie. 



Re-assembling the daughter card and installing it on the gantry positioned the sensor so that it would hit the interposer (Its a 90 degree bracket),  that is mounted in the back left on the gantry.  

The interposer is not adjustable and mounts from the back of the gantry so you can't get to it anyway. 



The sensor daughter board has no adjustment either. I could get it to barely clear the interposer if I forced the board to the left on its mounts. 



Not good, looks like I will have to file some slots in the sensor mounting plate so I can properly center the sensor. I wonder if any of the problems I have hard about optical sensors is this marginal alignment setup.



If I had not carefully checked this it certainly would have slammed into the sensor and messed something up.





![images/238b9e21d0094483d71c259278c5ce41](https://gitlab.com/funinthefalls/k40/raw/master/images/238b9e21d0094483d71c259278c5ce41)


---
**timb12957** *September 26, 2016 17:34*

I believe that is where all my problems began. The interposer plate was slightly out of place, thus the sensor was never "made" but rather impacted by the plate. 


---
**Don Kleinschnitz Jr.** *September 26, 2016 21:01*

So my endstop is again installed. I slotted the holes in the mounting plate and now the interposer is in the center of the sensor. It was a lot of trial and error to get it aligned.

Perhaps I should have put a small led on the board to show its working?

![images/712cd5b35d675acaeb906add16c39e68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/712cd5b35d675acaeb906add16c39e68.jpeg)


---
**timb12957** *September 26, 2016 21:06*

At least maybe discussing my mishap prevented you from the same fate


---
**Don Kleinschnitz Jr.** *September 26, 2016 21:28*

**+timb12957** my guess is your optical sensor if loose or damaged by the impact. We will get it figured out. BTW do you have a soldering iron?


---
**timb12957** *September 27, 2016 02:25*

I have used my meter to test for continuity on the traces in question. All tested fine. Yes I do have a soldering iron.


---
**Don Kleinschnitz Jr.** *September 27, 2016 02:32*

Uggh ..... a Hail Mary, use the soldering iron to re-flow (heat till solder flows) the connections on the sensor.

In fact lets reflow all the connections and look very closely to see if any traces are broke or cracked.

Noodling what we can try next ..... were do you live?


---
**timb12957** *September 27, 2016 02:46*

I am in Lyman South Carolina.  I just made an observation. On the side of the sensor with the anode and cathode there is what appears to be a small clear hemisphere. Like looking at the end of a led. On the side of the sensor with the emitter and collector there is not. Should not be sides have this lens structure? Maybe the plate damaged the lens on that side?


---
**Don Kleinschnitz Jr.** *September 27, 2016 02:59*

this pict shows two hemispheres.

[https://plus.google.com/u/0/photos/113684285877323403487/albums/6334748949069611569/6334748951563130690](https://plus.google.com/u/0/photos/113684285877323403487/albums/6334748949069611569/6334748951563130690)

You may be right the only way to know is to power the board and look for a signal when you interrupt the sensor.


---
**Don Kleinschnitz Jr.** *September 27, 2016 03:02*

you can get a new one here: [digikey.com - TCST1030 Vishay Semiconductor Opto Division &#x7c; Sensors, Transducers &#x7c; DigiKey](http://www.digikey.com/product-detail/en/vishay-semiconductor-opto-division/TCST1030/TCST1030-ND/2354846?WT.srch=1&gclid=CjwKEAjwjqO_BRDribyJpc_mzHgSJABdnsFWxtL51Xs-gQTcy02na9Bb_jUemKen7F1KwS0BHcrk_xoC-ebw_wcB)


---
**timb12957** *September 30, 2016 02:56*

I have ordered a new sensor from a local electronics supplier. It was not in stock so I have to wait a few days. In the mean time,  I have been in contact with the eBay seller. They instructed me to have it repaired by a local technician and they would pay the repair bill. How do you find someone local to work on a Chinese laser??


---
**timb12957** *September 30, 2016 03:11*

[drive.google.com - 20160927.mp4 - Google Drive](https://drive.google.com/file/d/0BwSEkdbV5i35OUE5UTdxZGEtQzQ/view?usp=sharing)



Video of the malfunction


---
**Don Kleinschnitz Jr.** *September 30, 2016 14:29*

If I was closer, I'd do it for ya :(


---
**timb12957** *October 07, 2016 03:01*

I want to  look for a local person to help me with this'

What technical training or occupation should I look for?


---
**Don Kleinschnitz Jr.** *October 07, 2016 03:15*

electronics technician or electrical engineer. I am willing to help advise anyone you select.


---
**timb12957** *October 07, 2016 04:03*

Just wondering Don, If you have viewed the video I posted showing the machines malfunction? After viewing it, what would be your first course of action to troubleshoot the problem?


---
**Don Kleinschnitz Jr.** *October 07, 2016 10:55*

I would measure the output of the sensor at the Nano end of the white cable while power is on and putting a piece of paper in-out of the Y endstop sensor. It should go from 0v to 5v.


---
**Vince Lee** *October 07, 2016 20:54*

It seems to me that it would be natural for the carriage to move in the wrong direction if the endstop sensor were stuck on.  The carriage thinks it is already in the home position, so it moves away from home waiting for the switch to disengage  until it reaches some internal maximum distance determined by the software and gives up.  If so, this should be easy to test.  Disconnect the sensor and wire up a switch with a pullup or pulldown resistor for testing.  I don't know offhand if the endstop is supposed to be active high or low, so try both ways.  


---
**Don Kleinschnitz Jr.** *October 07, 2016 22:32*

**+Vince Lee** the endstop is high when interrupted.


---
**timb12957** *October 08, 2016 14:43*

Don, referencing your daughter card schematic of the sensor, am I correct that the test points for this would be the Y home lead and ground?






---
**Don Kleinschnitz Jr.** *October 08, 2016 15:01*

**+timb12957** I have to do an errand I will give you a picture when I return in about an hr. You really want to check it at the Nano end of the cable to insure that it is getting all the way through ....


---
**timb12957** *October 08, 2016 15:14*

If those are the correct points to test,  under power I get 2.5v with sensor open, and when interrupted it goes to 4v 


---
**timb12957** *October 08, 2016 15:38*

Very good, and yes I was testing on the Nano connector


---
**Don Kleinschnitz Jr.** *October 08, 2016 17:15*

**+timb12957** the fact that it changes suggests that you are on the right pin. 



 4V might be ok but seems low, however when not interrupted Y should be closer to gnd as the photo transistor in the sensor should be turned on. 

2.5V is marginal for digital logic to see the sensor as NOT interrupted. 

My guess is that the Nano sees this as an interruption or like the sensor is always at home. 



Thinking of what could cause this:

What does the power supply read on the controller board? 

-Improper current in the transmitting LED side of the sensor. 

Look for: 

Bad 5V on the daughter card.

Bad 5V on the resistor that goes to the LED side of the sensor. 

Bad resistor on LED side...

-----------------------

For reference: Here is a portion of my machines schematic (only the connector and its signals apply to you) the dot is the signal you are testing. Also measure the 5V on that connector that goes back to the daughter.



[https://goo.gl/photos/KEg6KSRTqujysznC6](https://goo.gl/photos/KEg6KSRTqujysznC6)


---
**timb12957** *October 08, 2016 17:57*

Resistor checks good. Input voltage to daughter card and at resistor is 3.87v


---
**Don Kleinschnitz Jr.** *October 08, 2016 18:04*

**+timb12957** that seems to low I expected it to be 5V or whatever the LPS puts out. What is the 5V on the nano and the LPS connector measure?


---
**timb12957** *October 08, 2016 18:15*

Lps v5 output nano connector 5


---
**timb12957** *October 08, 2016 18:16*

Retest of daughter car 4.4.3v


---
**timb12957** *October 08, 2016 18:16*

4.3v


---
**Don Kleinschnitz Jr.** *October 08, 2016 19:18*

Its 5v at the LPS and 4.3 at the daughter card? What is the 5v at the nano end of the ribbon cable.


---
**timb12957** *October 08, 2016 20:03*

4.75v  at the Nano end of ribbon, AND after a power off and back on, it is now 4.75 at the daughter card


---
**Don Kleinschnitz Jr.** *October 08, 2016 23:58*

We need to figure out why the sensor does not sink to gnd when it is not blocked. Can you take some pictures of the daughter board at different angles. What did the resistor on the diode side of the sensor read when you checked it?


---
**timb12957** *October 09, 2016 00:24*

Pic a

![images/3df2d722887dbdbfb5e711ca7d2c6b79.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3df2d722887dbdbfb5e711ca7d2c6b79.jpeg)


---
**timb12957** *October 09, 2016 00:25*

Pic b

![images/b524ede872387afd37499538f1f86e37.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b524ede872387afd37499538f1f86e37.jpeg)


---
**timb12957** *October 09, 2016 00:25*

Pic c

![images/b0e8183093381e2040f6535fd560a805.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b0e8183093381e2040f6535fd560a805.jpeg)


---
**timb12957** *October 09, 2016 00:27*

Pic d

![images/4c5897800998cce1fe9cca994e484cf8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4c5897800998cce1fe9cca994e484cf8.jpeg)


---
**timb12957** *October 09, 2016 00:29*

Pic e

The resistor checked 1.004 ohms

![images/d1ce6ae344a6d563c1ea4d5b64dfaf82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1ce6ae344a6d563c1ea4d5b64dfaf82.jpeg)


---
**Don Kleinschnitz Jr.** *October 09, 2016 01:00*

Uggh! Nothing obvious. As soon as I get my machine back together I will verify that the uninterrupted signal should be less than 2.5v.  Currentlyy machine is disassembled. 


---
**Don Kleinschnitz Jr.** *October 09, 2016 13:30*

This AM I set up a TCST1030 sensor on a breadboard partly for my own sanity to see if it works like I expected. Luckily I bought one as a spare last week :).



The sensor is mostly hooked up like the K40 although the collector resistor may be larger than is used on the K40's Nano (shouldn't matter). 



See the schematic photo:

[https://goo.gl/photos/z3wGkUGqVa3L8rSU9](https://goo.gl/photos/z3wGkUGqVa3L8rSU9)



As the attached video shows when the sensor is NOT blocked the output voltage is close to zero (.13V) as the receiver is full on.  When it IS blocked it is close to the supply voltage or 4.76 because the receiver is almost off. 

Note: the voltage when blocked does vary with the transitivity of the blocking material but in the K40 application that is a metal tab so its irrelevant. 

![images/125795999169ad70cca936753e814b10](https://gitlab.com/funinthefalls/k40/raw/master/images/125795999169ad70cca936753e814b10)


---
**Don Kleinschnitz Jr.** *October 09, 2016 14:14*

My belief is that the sensor is NOT sinking to ground and as a result the sensor always looks like it is at home. 



As **+Vince Lee** suggests I think it might then try and pull back the carriage until it sees a change in the sensor which it does not.



The picture below outlines what the signals should be at the daughter card. 

I suggest you test and document the points on the daughter card that the picture shows the values for,  while your machine is powered on. 



Careful not to short anything, you may need extra hands.



Turn power off and remove the daughter card.



Then reheat the connections in the RED box, they look suspicious to me. The should not be ball-like but flow into the joint like the other connections on the ribbon cable connectors.



Then measure the values again.



I do not know why the sensor if not turning full on but am convinced that it the problem.



![images/63af1a4720ec040250e4520208efede3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/63af1a4720ec040250e4520208efede3.jpeg)


---
**timb12957** *October 10, 2016 02:11*

Red box?


---
**timb12957** *October 10, 2016 02:26*

sorry I could not see the red box until I zoomed in


---
**timb12957** *October 11, 2016 12:33*

After re flowing the points, no change in the problem. The seller has agreed to send me a new Nano M2 board, Y step motor, ribbon cable, and Y end stop board. I don't see how replacing all those can not resolve the problem. Mean time, I have found that after the machine powers on and locates the head in the middle of the machine, I can manually move the head to the correct approximate start location and make cuts from there. I just do not have a reliable repeatable zero location once the machine is turned off. Thank you all for the help as I struggle through this!


---
**Don Kleinschnitz Jr.** *October 11, 2016 12:48*

After you get the new parts and are working again, if you send the end stop daughter card and cable to me I will see if I can find the problem fix and return it to you. Dying to know what is wrong .....I am still betting its not the controller.


---
**Don Kleinschnitz Jr.** *October 11, 2016 12:50*

**+timb12957** it seems clear that the stepper is working so I would replace that last. You may want to replace the cable, end stop and controller  all together so that if there is a controller problem it doesn't blow up new end stops. Good luck....


---
**Ariel Yahni (UniKpty)** *October 11, 2016 13:05*

Maybe I'm late but through all this have we swaped the X for the Y parts to confirm? 


---
**timb12957** *October 11, 2016 13:27*

No, there was not a swap of X and Y end stop, however I did replace the actual optical sensor on the daughter card with a new one.


---
**Don Kleinschnitz Jr.** *October 11, 2016 13:42*

**+Ariel Yahni** I don't think you can swap the x&y daughter cards, can you?

Someone needs to make a replacement for these cards. When they go bad there is no reliable source.


---
**Vince Lee** *October 13, 2016 22:31*

This i not my area of expertise, but 1 ohm for the resistor seems wrong.  The test circuit in the datasheet for this sensor has a 50 ohm resistor and in the picture it looks like a 10 ohm?  If the resistor were bad it could damage the led in the sensor.  Can you shine or reflect a light into the receiving end of the sensor to get an output voltage lower than 2.5v?


---
**Don Kleinschnitz Jr.** *October 14, 2016 02:21*

Ok I missed that. I missed the decimal point and thought it said 1004 ohms. It should be 1000 ohms. So does it really read 1.004 ohms or is the meter on 1k scale. 

A 1ohm resister will burn out the sensors transmitter and it would have never worked? 


---
**Don Kleinschnitz Jr.** *October 14, 2016 03:20*

the resistor should be brown-black-red.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/6dmjyZy2A3F) &mdash; content and formatting may not be reliable*
