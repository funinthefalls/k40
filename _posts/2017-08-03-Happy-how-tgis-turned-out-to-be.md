---
layout: post
title: "Happy how tgis turned out to be"
date: August 03, 2017 21:33
category: "Object produced with laser"
author: "Paul de Groot"
---
Happy how tgis turned out to be. Setting the gamma levels would work even better but didn't had time to experiment with that. So just took tge pic as is and engraved it.

![images/9364d5ab5b119a829ce14b82fa5f96dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9364d5ab5b119a829ce14b82fa5f96dd.jpeg)



**"Paul de Groot"**

---
---
**Claudio Prezzi** *August 04, 2017 00:04*

Was this made with your new controller?



The result of +JohnMillekerJr looks much better to me. [https://plus.google.com/u/0/+JohnMillekerJr/posts/bMMAFxpVX78](https://plus.google.com/u/0/+JohnMillekerJr/posts/bMMAFxpVX78))

[plus.google.com - John Milleker Jr. - Google+](https://plus.google.com/u/0/+JohnMillekerJr/posts/bMMAFxpVX78)


---
**Paul de Groot** *August 04, 2017 00:15*

**+Claudio Prezzi** can you do the same engraving via your smoothie board? I just want to confirm that the picture was not manipulated (compare apples with apples instead of oranges)


---
**Paul de Groot** *August 04, 2017 00:17*

BTW I have done only one pass instead of two passes which was done on the original post. I should try that as well.


---
**Paul de Groot** *August 04, 2017 03:22*

**+Claudio Prezzi** the author said on the original post that he used a filter Floyd-Steinberg to enhance the picture, so will try that. So are you getting annoyed with me? Just kidding :)


---
**Alex Krause** *August 04, 2017 05:24*

How fast did you engrave?


---
**Claudio Prezzi** *August 04, 2017 07:15*

**+Paul de Groot** I would like to try the same engraving with my machine. Could you give me the file or a link?



Doing the engraving with multiple passes basically is a type of oversampling wihich matematical increases the gray resolution. This probably has the same effect as your higher PWM resolution (but takes longer).


---
**Claudio Prezzi** *August 04, 2017 07:18*

I'm not annoyed at all. Sorry if it sounded like that!

I just wanted to have some more infos to make a fair comparison.


---
**Claudio Prezzi** *August 04, 2017 07:21*

There are so many factors influencing the engraving quality and I would like to break that down to an easy to follow guide.


---
**Printin Addiction** *August 04, 2017 09:53*

**+Paul de Groot** Lets cross our fingers and hope the campaign gets funded!


---
**Glenn West** *August 04, 2017 10:27*

One thing I found in doing a laser full with lw4 and smoothie em2 was sometimes they drip glue on the top of wood so the darkness is different in those areas ie it's alot lighter - I can feel the difference even where the laser didn't cut it 


---
**Paul de Groot** *August 05, 2017 08:20*

**+Claudio Prezzi**​ see [pixabay.com - Free illustration: Steampunk, Steampunk Woman, Female - Free Image on Pixabay - 1980804](https://pixabay.com/en/steampunk-steampunk-woman-female-1980804)/ so try as is and with Floyd Steinberg conversion. You probably need gimp or something like photoshop to apply that filter. Haven't tried it myself yet


---
**Paul de Groot** *August 05, 2017 08:21*

**+Alex Krause** not fast just at 740mm/min but could do faster.


---
**Paul de Groot** *August 05, 2017 08:22*

**+Printin Addiction** it got funded!


---
**Stephane Buisson** *August 05, 2017 17:04*

**+Paul de Groot** Bravo for your KS campain. well done !


---
**Printin Addiction** *August 05, 2017 20:36*

**+Paul de Groot**  Looks like we are going to make it, I just ordered my K40 today.....


---
**Paul de Groot** *August 06, 2017 08:24*

**+Stephane Buisson** thanks! Much appreciated.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/X76bRNWwcGJ) &mdash; content and formatting may not be reliable*
