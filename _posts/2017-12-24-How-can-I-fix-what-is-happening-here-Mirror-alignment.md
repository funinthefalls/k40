---
layout: post
title: "How can I fix, what is happening here? Mirror alignment?"
date: December 24, 2017 11:53
category: "Hardware and Laser settings"
author: "BEN 3D"
---
How can I fix, what is happening here? 

Mirror alignment?

![images/99374e10f284ba3d618053e5dac1fd0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/99374e10f284ba3d618053e5dac1fd0f.jpeg)



**"BEN 3D"**

---
---
**Claudio Prezzi** *December 24, 2017 12:01*

I think the laser beam is reflecting on the side of the nozzle output. Try to adjust the last mirror to bring the beam to the center of the output.


---
**BEN 3D** *December 24, 2017 12:14*

YES!! **+Claudio Prezzi**, you fixed it, thanks for this christmas present  :-D


---
**Claudio Prezzi** *December 24, 2017 12:18*

Very welcome :)


---
**BEN 3D** *December 24, 2017 12:26*

much better![images/1f6c041252944f347f01b395fb88c17b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f6c041252944f347f01b395fb88c17b.jpeg)


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/5frtHRu3dck) &mdash; content and formatting may not be reliable*
