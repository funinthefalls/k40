---
layout: post
title: "Anyone know of a material that is like vinyl but doesn't produce chlorine gas when cut?"
date: April 13, 2016 11:23
category: "Materials and settings"
author: "I Laser"
---
Anyone know of a material that is like vinyl but doesn't produce chlorine gas when cut? :D



I've been doing some intricate cutting of 3mm acrylic and it's just a little too finicky. It has absolutely no give in it and therefore snaps at the drop of a hat.





**"I Laser"**

---
---
**HP Persson** *April 13, 2016 11:45*

Ask the seller or store for PVC-free vinyl, there is alot out there ;)


---
**I Laser** *April 13, 2016 12:18*

Thanks, but a search for PVC-free vinyl doesn't return much. Seems we don't have too many people selling it in Australia :(



Just to clarify, I'm looking for something like a vinyl record. I bought a few of them for the job prior to discovering the 'health benefits'. I've tried acrylic but not having much joy. I could go to thinner sheets but not so sure it would work much better.


---
**HP Persson** *April 13, 2016 12:25*

Oh, i thought you were looking for decal vinyls :)



Go for cast acrylic, it doesnt crack as easy as extruded acrylic does. (or is it the other way around? hmm).



Many (low budget) stores are selling crappy SAN as acrylic (styrene acrylic nitrile) and that pos shatters if you fart to close to it.



Look for real acrylic (pmma) and you will probably get better results.

I only use 2 and 3mm acrylic sheets in my cutting and stuff i do.


---
**Thor Johnson** *April 13, 2016 12:35*

Delrin and Nylon are much tougher than polycarbonate.  Delrin is hard to glue...   you could also look at hdpe (milk jugs).


---
**Alex Krause** *April 13, 2016 18:42*

Hdpe? You could test out on some of those cheapo cutting boards first to see if it will work for you 


---
**Alex Krause** *April 13, 2016 18:43*

Oops looks like **+Thor Johnson**​ beat me to suggesting hdpe.


---
**I Laser** *April 13, 2016 23:56*

Thanks all for the suggestions. I've currently got 3mm cast acrylic. Apparently extruded is prone to cracking.



Nylon and Delrin/Acetal, only seem available in 10+/6+mm sheets respectively.



HDPE is available in 2mm, though it's nearly double the price of acrylic. Might be able to get away with that if it's suitable though.



I was thinking maybe my design is a little too complex, with lines coming too close to each other. I'd prefer not to edit the design, would moving to a thinner medium help?


---
**Alex Krause** *April 14, 2016 01:39*

I would go look at your local supermarket I was looking on [Kmart.com.au](http://Kmart.com.au) and they have cutting boards for 2$ they are made of either HDPE or PP depending on the brand the ones I bought in the USA are about 3.5mm thick for under 2$  215mm X 280mm 


---
**I Laser** *April 14, 2016 04:09*

I'll go and have a look thanks :) I'm assuming I should be testing these for PVC prior to putting them in the machine?


---
**Alex Krause** *April 14, 2016 04:16*

Check the label on the cutting board if it is unmarked do a really small test cut in a well ventilated area pvc has a signature smell. I work in a PVC pipe manufacturing plant 


---
**I Laser** *April 14, 2016 09:36*

Nup no labels, bought a single board. To be honest I'm not sure what PVC smells like. Will have to get some copper wire to test. Will report back.



BTW **+HP Persson** "pos shatters if you fart to close to it" LMFAO!!! Must have missed that on the first read...


---
**I Laser** *April 25, 2016 09:47*

So meant to update, the cutting board I bought seemed a little inconclusive.



I don't have a blow torch, but with a 'jet lighter' and a bit of copper it was hard to tell, there was a some green in the flame. Anyway, I wasn't comfortable it didn't contain PVC.



I've trialed some 2mm acrylic which is a little more resistant to breaking. Still not ideal but better than the 3mm... If only they did >2mm!


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/64K4LEvBfGq) &mdash; content and formatting may not be reliable*
