---
layout: post
title: "K40 Whisperer 0.04 released with better grey-scale image control (and default settings)"
date: August 10, 2017 03:05
category: "Software"
author: "Scorch Works"
---
K40 Whisperer 0.04 released with better grey-scale image control (and default settings).  DXF support is also much better. Now you can cut and engrave with a single DXF file. (using blue color or layer naming)



I didn't plan on doing photos with my K40 (stock M2Nano controller) so any photo that is recognizable is a bonus for me.

 

- Better settings (and default settings) for raster engraved halftone (gray-scale) images

- Stop button pauses the current job with the option to terminate the job.

- Detects units in DXF files

- Asks for units for DXF files if they are not in the file

- Red/blue detection in DXF files for cut/engrave

- Items on DXF layers with "engrave" in the name are also engraved

- More status updates during preparation for engraving and during engraving.

- Can now stop a job during the preparation for engraving.



K40 Whisperer is available here:

[http://www.scorchworks.com/K40whisperer/k40whisperer.html#download](http://www.scorchworks.com/K40whisperer/k40whisperer.html#download)



The truck photo came from here:

[https://commons.wikimedia.org/wiki/File:Madrid_-_Fargo_Power-Wagon_WM300_-_130120_101212.jpg](https://commons.wikimedia.org/wiki/File:Madrid_-_Fargo_Power-Wagon_WM300_-_130120_101212.jpg)

![images/22e81293c069b29b510579def2951045.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22e81293c069b29b510579def2951045.jpeg)



**"Scorch Works"**

---
---
**Scorch Works** *August 10, 2017 03:08*

The photo was engraved at 150 mm/s, 4ma (default halftone settings in K40 Whisperer v0.04)


---
**Mark Brown** *August 10, 2017 11:12*

Oooh, that's purty!


---
**Anthony Bolgar** *August 10, 2017 12:19*

Keep up the good work! Thanks for continuing  to develop this.


---
**Tim Anderson** *September 24, 2017 11:31*

What wood are you using at those settings, I'm trying here and at 80mm/s I'm getting kinda passable darks....



I'm using 3mm Birch Ply.



Never had any luck with my Cohesion either but doing normal engraves is working great on my Chromebook running XUbuntu :)


---
**Scorch Works** *September 24, 2017 12:08*

I used 1/4 plywood from Homedepot.  I don't know what it is made from.  The core feels like balsa.  When I jave tried birch it definitely came out lighter.



[homedepot.com - Sande Plywood (Common: 1/4 in. x 4 ft. x 8 ft.; Actual: 0.205 in. x 48 in. x 96 in.)-479023 - The Home Depot](http://www.homedepot.com/p/Sande-Plywood-Common-1-4-in-x-4-ft-x-8-ft-Actual-0-205-in-x-48-in-x-96-in-479023/100073744)


---
**Tim Anderson** *September 24, 2017 13:01*

Thank you :) I'm trying another one and have slowed to 60mm/s, it is crawling along but seems to have more detail.



I've got to order some more wood soon anyway so will order a few different types this time :)


---
**Scorch Works** *September 24, 2017 19:51*

I have seen people report using speeds as low as  13.2 mm/s (790 mm/min) which I originally thought was a typo.  The results were great but that is slow.  (the speed was mentioned in the comments)

[plus.google.com - Engraving of a 1940s photo with my Gerbil k40 controller. http://awesome.tec...](https://plus.google.com/b/115250973662326777706/102073383537723054608/posts/DPzQeCqpguT)


---
**Tim Anderson** *September 24, 2017 21:56*

Wow! I backed the Gerbil, I did read that one of the engraves took around 8 hours, now I know why!



Got to say I really enjoyed working with K40 Whisperer today, it has a really nice workflow pretty much hides the limitations of the Chinese board :)



Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 04:01*

**+Tim Anderson** something not working with the Cohesion board that I can help you with?  I thought we got you sorted out a while back. 


---
*Imported from [Google+](https://plus.google.com/+ScorchWorks/posts/LTYZaa99wjh) &mdash; content and formatting may not be reliable*
