---
layout: post
title: "Shop Tip - When you are cutting out pieces with details that have narrow connection points you generally try to have at least 2 connection points, like the ball on the right in the picture, to give better stability"
date: January 31, 2018 14:10
category: "Discussion"
author: "Ned Hill"
---
Shop Tip - When you are cutting out pieces with details that have narrow connection points you generally try to have at least 2 connection points, like the ball on the right in the picture, to give better stability.  This is especially true for small details in thin stock.  Where this isn't reasonably possible you can add or expand an engraved edge, like the ball on the left, while keeping the unengraved area the same.  This adds width to the connection point while having the illusion of a narrow one. #K40ShopTips

![images/2a2858da303ce35193924d772adfb2bc.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2a2858da303ce35193924d772adfb2bc.png)



**"Ned Hill"**

---


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/SyswWuH1o8T) &mdash; content and formatting may not be reliable*
