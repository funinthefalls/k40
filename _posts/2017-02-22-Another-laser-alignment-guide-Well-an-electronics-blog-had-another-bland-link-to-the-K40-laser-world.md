---
layout: post
title: "Another laser alignment guide! Well an electronics blog had another bland link to the K40 laser world"
date: February 22, 2017 23:05
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
Another laser alignment guide!



Well an electronics blog had another bland link to the K40 laser world. I checked out the blog and did not  find any new info on the K40 but did discover a great link to a laser alignment guide that has a lot of pictures and explanations on how to optimize the mirrors. This is a great compliment to the floatingwombat link.



[http://justaddsharks.co.uk/support/laser-beam-alignment-guide](http://justaddsharks.co.uk/support/laser-beam-alignment-guide)





**"HalfNormal"**

---
---
**Richard Vowles** *February 23, 2017 03:25*

Thanks for sharing!


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/9MCyxd8CSh7) &mdash; content and formatting may not be reliable*
