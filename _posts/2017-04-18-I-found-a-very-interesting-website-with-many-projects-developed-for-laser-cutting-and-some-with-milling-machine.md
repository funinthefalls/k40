---
layout: post
title: "I found a very interesting website with many projects developed for laser cutting and some with milling machine"
date: April 18, 2017 18:14
category: "Repository and designs"
author: "Fernando Bueno"
---
I found a very interesting website with many projects developed for laser cutting and some with milling machine. All projects are hosted at Thingiverse and are for free download. I hope you like them.





**"Fernando Bueno"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 18, 2017 19:11*

Seen some of their stuff before on that site & also as Instructables. Some pretty good projects they share.


---
**Kelly Burns** *April 19, 2017 18:33*

Nice site.  I like how they just index the other sources and don't steal the credit like many other "collectors" do.  


---
*Imported from [Google+](https://plus.google.com/+FernandoBueno/posts/Yc9gnrAw9U7) &mdash; content and formatting may not be reliable*
