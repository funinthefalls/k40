---
layout: post
title: "Does anyone know were to get the small spade terminals that are typically used with the Green screw terminals?"
date: February 14, 2017 21:52
category: "Material suppliers"
author: "Don Kleinschnitz Jr."
---
Does anyone know were to get the small spade terminals that are typically used with the Green screw terminals?



I might be using the wrong terms.





**"Don Kleinschnitz Jr."**

---
---
**Kostas Filosofou** *February 14, 2017 22:15*

**+Don Kleinschnitz**​ you mean this? 

 [ebay.de - 800pcs Sortiment Aderendhülsen Terminal Aderendhülse Isoliert 0.5mm² - 6mm²  &#x7c; eBay](http://www.ebay.de/itm/391428246636)


---
**Ned Hill** *February 14, 2017 22:18*

Wire end sleeves?


---
**Don Kleinschnitz Jr.** *February 14, 2017 22:30*

**+Konstantinos Filosofou** yup thats it now I can find them since I know the name "Wire end sleeves :).

Thanks


---
**Don Kleinschnitz Jr.** *February 14, 2017 22:32*

Also called "Pin ends". 

[amazon.com - Amazon.com: Hilitchi 800pcs 10-24 A.W.G Wire Copper Crimp Connector Insulated Cord Pin End Terminal Kit: Home Improvement](https://www.amazon.com/gp/product/B01CXPE4BK/ref=crt_ewc_img_dp_1?ie=UTF8&psc=1&smid=AYI27JSX7SHMW)




---
**Ray Kholodovsky (Cohesion3D)** *February 14, 2017 23:17*

Insulated ferrules you say? 


---
**Don Kleinschnitz Jr.** *February 17, 2017 21:27*

Thanks for the suggestion on the pin ends. 

I got this kit and of course another crimp-er was needed :(. 

I considered not getting the crimp-er and just solder them in but the crimp-er makes a big difference. I expect this to be a much more reliable way to use screw terminals. Up to now I have been putting a bulb of solder on the wire to improve the grip.



[https://www.amazon.com/gp/product/B01K160CC8/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B01K160CC8/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)



![images/ad407566008f7192c189b14842213e02.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad407566008f7192c189b14842213e02.jpeg)


---
**Don Kleinschnitz Jr.** *February 17, 2017 21:28*

The kit and my first assembly. 

![images/d4802a77ccdd0182caf1373ffa9b4e7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d4802a77ccdd0182caf1373ffa9b4e7d.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/4jNCz32mi3i) &mdash; content and formatting may not be reliable*
