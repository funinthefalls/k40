---
layout: post
title: "Been a while since I posted. Moved into a new house with a larger garage and have room to work"
date: August 24, 2017 22:22
category: "Smoothieboard Modification"
author: "David Cook"
---
Been a while since I posted.

Moved into a new house with a larger garage and have room to work.  just about finished transplanting the K40 tube and power supply and a Smoothie board into a Large machine.  The motion control mechanics on this Universal Laser systems machine is SLICK.   can move hellla fast smoothly.   Once i get my liquid cooling up and running I will convert the smoothie board to GRBL 1.1



I am now driving the Laser with the - fan output  P2.5,  removed level shifter etc.   very simple.  Added POT back in for max limit,   verified PWM power control still works.



Had to purchase these molded tube mounts,  the PLA mounts I printed heavily warped in the heat of the garage  LOL that was fun..



Close !









**"David Cook"**

---
---
**Phillip Conroy** *August 25, 2017 08:39*

Is this the machine I surgested one one buy cheap


---
**Stephane Buisson** *August 25, 2017 10:38*

Welcome back to one of our Smoothie pioneer ;-))


---
**David Cook** *August 25, 2017 19:33*

**+Phillip Conroy**  This is a Universal Laser Systems M300  It had a broken 30 watt RF driven aircooled CO2 laser.  I picked up this machine for only $400 and transferred my K40 stuff into it.  basically doubled the work area and has excellent motion control mechanics.


---
**David Cook** *August 25, 2017 19:33*

Had to buy a new water pump, arrives this saturday.   almost complete


---
**Bill Keeter** *September 19, 2017 15:23*

I learned on a Universal. Man, if I could find a broken one like this I would be in heaven. Off to check craigslist again. 


---
**David Cook** *September 19, 2017 16:57*

**+Bill Keeter**  I admit I giggled like a little school girl when I came accross the opportunity to buy it for $400.  Initially I did ponder getting the RF source repaired...  Bust installing the K40 parts came next and the build is almost finished.  the motion gantry is sooo smooth.  Also aligning the beam   was insanely simple since the gantry is high quality and straight.


---
**David Cook** *September 19, 2017 16:57*

![images/c6ab9d2eea84df6f0fbd00a8851695b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c6ab9d2eea84df6f0fbd00a8851695b3.jpeg)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/EsS8VccDAC1) &mdash; content and formatting may not be reliable*
