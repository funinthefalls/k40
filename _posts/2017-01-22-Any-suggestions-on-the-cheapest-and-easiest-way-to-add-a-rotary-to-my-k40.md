---
layout: post
title: "Any suggestions on the cheapest and easiest way to add a rotary to my k40?"
date: January 22, 2017 08:17
category: "Modification"
author: "Nick Hale"
---
Any suggestions on the cheapest and easiest way to add a rotary to my k40? I'm not a very technical person. 





**"Nick Hale"**

---
---
**Ned Hill** *January 23, 2017 02:40*

**+Nick Hale**  I'm not sure if there is an easy/cheap way.  Light Object sells one for the K40 [lightobject.com - Mini 2 Axis Rotary. Ideal for K40 Engraving Laser Machine](http://www.lightobject.com/Mini-2-Axis-Rotary-Ideal-for-K40-Engraving-Laser-Machine-P941.aspx) but, as I understand it, it will require the use of an external step driver when using the stock board.  If you use an aftermarket board, like the minicohesion3, you wouldn't have to have an external step driver.  There are plans floating around for building DIY rotary units that may be cheaper.



The other consideration is that the distance between the floor of the machine and the laser focus point is only about 3.5".  So the rotary unit is going to take up some of that distance and you would probably be only left with only being able to do 2" diameter objects.  To get around this people have been building frames to raise the laser and then cutting a section of the bottom of the machine out big enough to fit the rotary unit.  You would also have to have a mech to raise and lower the unit to get the height right.  If I'm not mistaken **+Anthony Bolgar** has a rotary unit in his.  Hope this info is useful.


---
**Anthony Bolgar** *January 23, 2017 05:06*

Yup, I cut the floor out of my K40 and raised the entire unit up off the desk by 6" to gain some cutting depth, especially with the rotary.


---
**Nick Hale** *January 23, 2017 06:23*

Thanks for the info, I should have went into a little more detail. I plan on replacing the board. I also plan to cut out the bottom of the case and make a manual Z axis that will raise up into it. My concern was looking at the smoothieboard, I might be getting in above my head with electronics. 


---
*Imported from [Google+](https://plus.google.com/101739286835462605895/posts/PJ8JSFNdqz7) &mdash; content and formatting may not be reliable*
