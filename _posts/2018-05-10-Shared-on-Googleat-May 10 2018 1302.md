---
layout: post
title: "Shared on May 10, 2018 13:02...\n"
date: May 10, 2018 13:02
category: "Discussion"
author: "Stephane Buisson"
---


![images/705840cdd0820d4505f72ef62c641226.png](https://gitlab.com/funinthefalls/k40/raw/master/images/705840cdd0820d4505f72ef62c641226.png)



**"Stephane Buisson"**

---
---
**Stephane Buisson** *May 10, 2018 13:04*

comment field for stat purpose:

creation date, birthday 7/11/2014

community reach 100 members on 12/2/2015

community reach 200 members on 11/5/2015

community reach 300 members on 16/7/2015

community reach 400 members on 9/9/2015

community reach 500 members on 16/10/2015

community reach 800 members on 5/1/2016

community reach 900 members on 25/1/2016

community reach 1000 members on 7/2/2016

community reach 1250 members on 25/3/2016

community reach 1500 members on 6/5/2016

community reach 1750 members on 17/6/2016

community reach 2000 members on 3/8/2016

community reach 2250 members on 15/9/2016

community reach 2500 members on 11/10/2016

community reach 3000 members on 2/1/2017

community reach 4000 members on 11/08/2017

community reach 5000 members on 10/05/2018




---
**Anthony Bolgar** *May 10, 2018 13:13*

Just think of all the people who have been helped, Thanks for starting this group!




---
**HalfNormal** *May 11, 2018 00:48*

Congrats! It seems that the group is growing exponentially!


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/TS8xhzxzo9C) &mdash; content and formatting may not be reliable*
