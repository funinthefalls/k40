---
layout: post
title: "Anyones else tube covered in mdf gunk?the green in center is car radator anti freeze"
date: August 14, 2016 05:32
category: "Discussion"
author: "Phillip Conroy"
---
Anyones else tube covered in mdf gunk?the green in center is car radator anti freeze



![images/6312ed13930f106ef9d6b3ac73365d49.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6312ed13930f106ef9d6b3ac73365d49.jpeg)
![images/781cdae486055d4b069285f69a8f11a3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/781cdae486055d4b069285f69a8f11a3.jpeg)

**"Phillip Conroy"**

---
---
**greg greene** *August 14, 2016 13:05*

most folks use distilled water


---
**Gunnar Stefansson** *August 14, 2016 16:36*

Yes mine aswell, but not as bad as that... mine is just slighty less see through. I clean it from time to time. I also have a different laser cutter..


---
**greg greene** *August 14, 2016 17:18*

What is the preferred cleaning method


---
**Scott Marshall** *August 14, 2016 19:00*

hope you're not breathing that 'stuff'


---
**Gunnar Stefansson** *August 14, 2016 19:18*

I just use normal window cleaner to remove the worst, and then use microfiber cloth with some pure alcohol.


---
**greg greene** *August 14, 2016 19:23*

Ok - it's a sealed tube - how do you get the microfiber inside and and get it out again?


---
**Gunnar Stefansson** *August 14, 2016 21:32*

**+greg greene** Holy moly... so what you're saying is, the gunk is on the inside of the tube? Wow... if it's on the inside I would start by putting something to clean, chlorin and then let the water run, change the water and re-rinse, until clean!


---
**greg greene** *August 14, 2016 22:02*

Has to be - that's where the water flows for cooling


---
**Scott Marshall** *August 14, 2016 22:50*

Well if that crap is INSIDE the tube and it's still running - I'd say that puts an end to the "do you really need distilled water" debate once and for all.



The way we clean & disinfect ultrapure (de-ionized) water systems (industrial loops) is to run Hydrogen Peroxide (we use much higher concentrations than you can buy at the drugstore - by a lot) through, let it set 4 hours, rinse, repeat. (we shoot for 8-10% in the system, but 3% will work, just let it set overnight)



I'd flush out as much as you can with tap water 1st, then out of need for economy use chlorine bleach at about 20% for the 1st couple cycles. Rinse several times before going to peroxide (they do react). Gloves and glasses required. The pump is going to need a field strip and clean/inspect. If in doubt, a Little Giant PE-170 is a great upgrade and will last forever.



Going forward  I would stick with IPA as an antifreeze/disinfectant. I've been running 10% with no growth, but may go up in the winter for freeze protection.  You're going to have to change all soft parts (rubber or PVC/vinyl tubing etc), because they are damaged already.



Silicone tubing might be OK if that's what your main lines are – that's usually what a K40 comes with from the factory, and would be a good choice going ahead, as would Teflon if it's in the budget.



I'm guessing you were using an open tank for cooling, and the dust settled in it, and, along with the ethylene glycol created a bacterial smorgasbord. The tank will need a top, or mounted away from the dust source.



On the project list,  I have an old outdoor dog dish from which I plan on removing the automatic element. If wired to defrost and operate the pump, it ought to make a good freeze protection device in addition to the alcohol. My laser just may get a promotion to a warm environment since it's now working for a living.



Scott



I'd take a speargun with me to clean out the tank, who knows what's in there?


---
**greg greene** *August 14, 2016 22:55*

I don't see the need for anti-freeze in a home environment.


---
**Phillip Conroy** *August 15, 2016 08:58*

The crap is on the outside,do not see thebpoint in cleaning,i am using anti freeze as it has other stuff in it to keek water from going slimie ,and green color is cool


---
**Phillip Conroy** *August 15, 2016 09:01*

Most of the mdf gunk was tbere before j upgraded fan from 25watt stock to 250 watt 8inch job,now have not needed to clean first mirror in 3 months


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/99byFMkncfo) &mdash; content and formatting may not be reliable*
