---
layout: post
title: "Doing some alignment and was wondering if I raise the right side of the tube to hit the first mirror in the center do I have to raise the left side to keep the tube level ?"
date: September 08, 2016 01:23
category: "Discussion"
author: "Robert Selvey"
---
Doing some alignment and was wondering if I raise the right side of the tube to hit the first mirror in the center do I have to raise the left side to keep the tube level ?





**"Robert Selvey"**

---
---
**Ariel Yahni (UniKpty)** *September 08, 2016 01:28*

If you don't then the beam will not be on the same plane as the gantry, and that's very important


---
**greg greene** *September 08, 2016 01:36*

yes it must be parallel


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 03:15*

Yeah, if it's not level you risk missing the 2nd mirror & hitting yourself. It hurts by the way.


---
**Jez M-L** *September 08, 2016 08:40*

**+Yuusuf Sallahuddin** 😆😆


---
**J.R. Sharp** *September 08, 2016 14:05*

So what is the best way to check the tube for alignment? Measuring the brackets is nearly pointless since these cheap ass plastic things aren't consistent. I was considering making sure the chassis was level and then using a standard level on the tube.




---
**Ariel Yahni (UniKpty)** *September 08, 2016 14:11*

**+J.R. Sharp**​ is not only the the tube is aligned, you need to square everything and on the same plane


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/7hZ3H2giKAc) &mdash; content and formatting may not be reliable*
