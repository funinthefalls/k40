---
layout: post
title: "Well first to mods done, cut the air duct and swapped out the bed"
date: July 25, 2016 16:35
category: "Modification"
author: "David Spencer"
---
Well first to mods done, cut the air duct and swapped out the bed. Now what? 

air assist

some other board because the corel thing is quirky to me.

 what is the benefit of adding coolant to the water and how much



![images/9d579ddd3af2ca26a46b7e7f6e6bd1d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d579ddd3af2ca26a46b7e7f6e6bd1d7.jpeg)
![images/00ca8816a136c1cd50c1a68c0496f4b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00ca8816a136c1cd50c1a68c0496f4b8.jpeg)

**"David Spencer"**

---
---
**Jon Bruno** *July 25, 2016 16:51*

Well, actually RV/Marine antifreeze is suggested..

It's not the same as the toxic ethylene glycol like in your car's radiator.

You want the stuff that's used to protect potable water systems.

The primary components are either ethanol or propylene glycol.

Anyway, it's used to help keep the algae and other fuzzies from growing in your laser cooling system. You can run all the way up to 100% if you like. I'm not sure what the minimum percentage is to still have the proper results.


---
**Evan Fosmark** *July 25, 2016 17:03*

I use that same bed (from Home Depot), and I really don't like it. When cutting, it has a tendency to heat up and burn the bottom of the piece being cut. You'll notice scorch marks around the cutting areas, even when you dial back the power to the minimum needed to make the cut. 



One way to get around this is to raise the piece off of the bed by a mm or two, but you still have to deal with reflections.


---
**David Spencer** *July 25, 2016 17:05*

**+Jon Bruno** Like this: [http://www.quakekare.com/water-purification-tablets-p-23.html?c1=GAW_SE_NW&source=PLA&cr6=pla&kw=Water_Purification_Tablets&cr6=pla&gclid=CjwKEAjw8da8BRDssvyH8uPEgnoSJABJmwYom8b0V4n6SWqXibFF_Yg8kkE7v0hbJ5r0dCHeON-7qBoCCI_w_wcB](http://www.quakekare.com/water-purification-tablets-p-23.html?c1=GAW_SE_NW&source=PLA&cr6=pla&kw=Water_Purification_Tablets&cr6=pla&gclid=CjwKEAjw8da8BRDssvyH8uPEgnoSJABJmwYom8b0V4n6SWqXibFF_Yg8kkE7v0hbJ5r0dCHeON-7qBoCCI_w_wcB)


---
**Jon Bruno** *July 25, 2016 17:08*

Nah..Those are purification tablets for cleaning water..

Just head down to Wallymart and grab some of this..

[http://www.walmart.com/ip/Prestone-RV-Marine-Antifreeze/16213488](http://www.walmart.com/ip/Prestone-RV-Marine-Antifreeze/16213488)


---
**Jim Hatch** *July 25, 2016 17:14*

Try 10% DowFrost antifreeze with distilled water. That will take care of algae. The other thing about regular antifreeze is that it contains additives that don't necessarily play nice with lasers. The MSDS data on them is sketchy so it's hard to tell if it's shortening the life of the tube. You want distilled water because it won't have the minerals or chemicals that tap water has in it which can also precipitate or react with the tube components.


---
**Jon Bruno** *July 25, 2016 17:15*

Even better advice. Thanks Jim.


---
**David Spencer** *July 25, 2016 17:23*

dowfrost is 125.00 dollars for 5 gals. I dont think I need 5 gals.


---
**Jon Bruno** *July 25, 2016 17:24*

Lol Wal-Mart then.

Sheesh. I had no idea that stuff was so expensive.


---
**Jim Hatch** *July 25, 2016 17:26*

But you can find it by the quart at local RV or solar panel folks. Amazon sells it by the half-gallon (or gallon) as well. Or check your local school or makerspace to see if they're using it in their machines.


---
**greg greene** *July 25, 2016 20:14*

how easy is it to remove the stock bed and clamp??


---
**David Spencer** *July 25, 2016 20:19*

5 screws hold the bed down, it was easy took about ten minutes including cutting the new bed.


---
**Alex Krause** *July 25, 2016 20:28*

What did you use to cut your duct?


---
**David Spencer** *July 25, 2016 20:45*

I took the 4 bolts out that hold the X & Y frame pulled the duct out and cut it with a sawsall


---
**I Laser** *July 26, 2016 01:47*

Good idea, I cut the vent out of my analog k40 from behind as it was welded in. It works a treat.



The digital k40 I got had 4 screws holding the vent in. Given how successful the first machine was, I just removed them and ditched the entire vent. 



I've now discovered there are massive gaps between the walls and the platform that holds the tube which causes smoke to be drawn through the laser tube cavity too.



So I'll now need to fashion some sort of shroud to redirect things. Wish I had kept the vent and just cut it down...


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/MF7ZwHNHLva) &mdash; content and formatting may not be reliable*
