---
layout: post
title: "Tired of fighting some power issues. Can't get a consensus on source of the issue being Power Supply or Tube"
date: May 15, 2017 17:16
category: "Hardware and Laser settings"
author: "Kelly Burns"
---
Tired of fighting some power issues.  Can't get a consensus on source of the issue being Power Supply or Tube.   Light Object had the strange size 35 Watt tube for $99 so I went ahead and purchased it.  Now, i want to go ahead and get a power supply?   I would like something similar to what's in the K40, but with a little better quality.   I would rather not have separate supplies for Laser and Electronics.



Any input is appreciated.  





**"Kelly Burns"**

---
---
**E Caswell** *May 15, 2017 17:42*

Have a look at Dons blog. **+Don Kleinschnitz** he has spent endless hours looking at the power supplies and their reliability. 

Are you running stock board? 

As for not wanting to run electronic on a separate supply.! I would install one even if you find a better unit than a stock LPSU



A separate supply makes such a lot of difference in stepper control and a much smoother run.  That's what I have found. 


---
**Ariel Yahni (UniKpty)** *May 15, 2017 18:09*

Did you get the Tube yet?


---
**Kelly Burns** *May 15, 2017 18:17*

**+PopsE Cas** I'm pretty knowledgeable on CNC in general and I find the stock power supply to do pretty well.  I'm running a Smoothieware.  I'm pretty familiar with Dons blogs, but don't recall specific recommendations on a vendor.   I'll take another look.   I have plenty of quality power supplies to try for electronics so I may look to sure.  Problem is o don't have room.  I have removed all electronics from inside to a separate enclosure on side.  At this point I'm looking for quick and simple.  



Thanks for input.  


---
**Kelly Burns** *May 15, 2017 18:29*

**+Ariel Yahni** its on its way.  (Wed or Thurs) I'll have to cut a hole and make an enclosure to cover the extra length.   


---
**Don Kleinschnitz Jr.** *May 15, 2017 21:09*

**+Kelly Burns**​ I did not make a recommendation on stand alone supplies cause there are lots of choices. I think I blogged what I use. 

A standard K40 supply lps should work for 35w, my concern would be if it's overvoltage shortens the tube life. A tube is typically powered proportional to length. 



The stock supply is capable of 24 v @1 amp, 5v at 1amp.

You can try it and check the current draw(s) under peak load to see if it's marginal. If it is I would add a supply. 


---
**Steve Clark** *May 16, 2017 02:40*

From what I've read on how the laser tubes work. The power supply IS the limiting factor for the K40. So going to a larger wattage PS will burn the tube out quicker.  They pull whatever watts are available.



 Example…you have a 100w laser and a 200w PS setting the power knob at 75% will give you 150w available …not good if you’re not paying attention..



I was down at LightObect last month and they were talking about a 100 watt so laser that kept burning tubes out. 



Turned out the factory put in the wrong higher wattage power supply and that was determined to be the problem. Replace it with a 100w and that fixed the problem.

 


---
**Don Kleinschnitz Jr.** *May 16, 2017 11:56*

**+Steve Clark** my concern would be the voltage not the power, which is what initiates the ionization. 

Of course if the power is higher likely so is the voltage E*I.



The voltage level is matched to the tube length since more gas requires more voltage to ionize, after ionization the tube enters a negative resistance mode.

 

My concern is whether the higher than needed voltage will have an impact on the Co2 recombination characteristics of the lasing process.



The design information on voltage vs reliablity is pretty sketchy. I am sure that LO knows how to size this stuff.


---
**Steve Clark** *May 16, 2017 16:18*

One comment I did not put in yesterday that you probably know already is the tube you have ordered is  830mm long and the original K40’s are 700 to 720mm.



As for the power supply I think I would ask LO if you have not already. They can tell you if your old one needs replacement or not.


---
**Cristian Cimpeanu** *May 16, 2017 18:21*

I have a lot of experience with chinese  lasers (7 years..).

K40 have the simplest power supply possible.  Basically is a pc power supply  with a HV transformer. Current correction is in front of the power supply (current transformer is the small one).

On start is a small time when current measurement is blinded. This is call "SuperPulse" and it is for start circulation of gases inside tube. In general "SuperPulse" is 10 times power of laser tube.  If you have a glass leak (a short to ground) power supply start again and again and you hear a buzz. In this time your tube have a current minium 10 times max. admissible and the co2  don't have time to  regenerate and your power is low. In short time (max. 10 hour) your laser tube is dead and his electrodes  are burnt. A laser with a good power supply after 1000 hours have no burnt spots on electrodes.

My advice. Find a power supply with  strike voltage and  continuous voltage that match with your laser tube.  Current is adjustable on every source.  


---
**Kelly Burns** *May 16, 2017 20:03*

**+Steve Clark** Yes, I'm aware that its a bit longer.  I was actually going to go with the even longer  true 45W,  (1000mm) but then came across this one.   Actually went ahead and cut the hole last night.  I ended up just sectioning off the wooden enclosure I had added to the side for the electronics.   I'll will post a picture of it when I get it cleaned up.



LO does not sell a replacement that is a Combination unit like the one in the K40s.   I can certainly buy the 40W Laser Supply they have, but was trying to avoid having two supplies.  It does look like this will be the best option.





**+Don Kleinschnitz**  thanks for the information.  I guess my question wasn't very clear.  I am simply looking for a 35-40 Watt Power Supply to replace the existing one.  I'm not trying to up the Wattage, I will stick with the one that matches the wattage of the Tube.  I was looking to keep it to a single Power Supply for simplicity.   If it turns out I need a new power supply, I will likely just buy the one from Laser Objects.  So far, I have been pretty happy with everything from them.   I was simply being frugal and was hoping to find a less expensive option from a re-seller that had been verified by others.    At this point I should know better.  


---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/5LXVt77cKok) &mdash; content and formatting may not be reliable*
