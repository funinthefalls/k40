---
layout: post
title: "I have been going my reading and would like to know from someone how the steps per mm where calculated for the smoothie configs out there"
date: May 11, 2017 02:07
category: "Smoothieboard Modification"
author: "John Revill"
---


I have been going my reading and would like to know from someone how the steps per mm where calculated for the smoothie configs out there. All the example config files I have found have steps per mm at 157.575 which seems like an odd value.



My Marlin was set to 80, and it was calculated because the belt is a GT2 2mm pitch belt, the pulley is a 20T, the stepper has 200 steps per revolution and the stepper driver is set to 1/16 micro steps. So we get (200*16) / (20*2) = 80. My cuts always seems acurate. On a smoothie board using a 1/32 micro stepping driver, I calculate the steps per mm at 160 and based on my accuracy with my old setup, I will be sticking with 160. But how did 157.575 come about?







**"John Revill"**

---
---
**Stephane Buisson** *May 11, 2017 09:00*

Well, it was some time ago, but if I remember well, I calculated it by lasering different lines sizes, then mesure the results. the difference gave me a ratio to apply to the default value in the config. the result was 157.575.

I redo the job, and I was bang on.

the little difference with your calculation could be due to the kerf. (acceptable within 20x30cm K40 area).


---
**Wolfmanjm** *May 12, 2017 05:23*

Because many K40 do not use GT2 they use MXL which is 2.03mm hence the difference. In fact I have not heard of a K40 that uses GT2.


---
**Stephane Buisson** *May 12, 2017 08:25*

thank you **+Wolfmanjm** for this precision


---
**John Revill** *May 12, 2017 10:20*

Mine must be unique. I've lost my original tests, but i will redo it over the weekend. My marlin runs 16 micro steps, so a value of 80 is perfect. I will laser etch 80 and 78.7875 steps per mm and compare it to my ruler. I know 80 is perfect on mine but i want to see how far out 78.7875 is on mine. Stay tuned. Ill have a picture tomorrow.


---
**ALFAHOBBIES** *May 12, 2017 15:56*

I wondered too where 157.575 came from. Used to GT2 on printers. Interesting post. Now I know the belts are MXL. 


---
**John Revill** *May 12, 2017 18:07*

OK, for the MXL belt/pulley setup, for the sake of accuracy, 157.575 is close but not 100%. MXL has a pitch of 0.08" which is 2.032mm. so the steps per mm should be 157.480315. It doesn't sound like much, but it would be 0.2mm difference over a 300mm length. 


---
**Wolfmanjm** *May 12, 2017 18:23*

FWIW I set my steps/mm to 315.26999 (I have  1/32) so that would be  157.634995 for 1/16


---
**John Revill** *May 12, 2017 20:43*

+Wolfmanjm Thats really high. Are you running different steppers, like 400 steps per rev? or smaller pulleys? 


---
**Stephane Buisson** *May 12, 2017 21:20*

**+John Revill**, do you have a trick to reduce the kerf ? (0.2 over 300mm for a precision K40 machine is more than acceptable by my standard)

just joking ....


---
**Wolfmanjm** *May 12, 2017 21:57*

I think the steppers are 0.9° they were standard K40 ones, I think it may also be 1/16 not 1/32 it is a K40 adapter board for a re arm from Panucatt.

I forget what I counted the pulleys at, but basically I calculated the steps/mm from an MXL belt with 0.9° steppers.


---
**John Revill** *May 12, 2017 22:55*

Typical chineese swaping and changing things. My K40 (Without amp meter and has digital power controller has 1.8° steppers and GT2 belts and pulleys. It gives me 80 Steps per mm on a 1/16 stepper controller.

![images/041cf7465016920b2c73e5c498232e71.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/041cf7465016920b2c73e5c498232e71.jpeg)


---
**ALFAHOBBIES** *May 12, 2017 23:07*

The way it sounds the .080" mxl belt is not such a good thing. I would think that it makes it hard on  the software to work with odd numbers instead of nice even numbers that the gt2 belts and pulleys would give. 


---
**John Revill** *May 12, 2017 23:41*

Most CNC CAM software will take the Kerf into account when generating the tool path. And put simply, trying to compensate for it in the steps per mm is not as accurate and doing it in CAM. in you want a 10x10cm box and the laser is 0.1mm width, the tool path with actually be 10.01x10.01 and the resulting box is 10x10cm perfectly. So when you do to 50x50cm box the tool path is 50.01x50.01. Obviously with a 0.1mm laser , it's almost insignificant. but it plays i big roll in CNC machining when the tool might be 6mm wide. So for me, accuracy is the key. I design my part in whatever I feel like at the time, Inkscape or OpenSCAD. Then I open it up in Estlcam and tell it the tool is 0.1mm wide. It generates a tool path compensating for the laser width. Some of the acrylic pieces I have cut have been accurate to 0.00mm.

![images/8c99dd59553e03ccdec685a2c3371e4f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8c99dd59553e03ccdec685a2c3371e4f.jpeg)


---
**John Revill** *May 12, 2017 23:42*

As you can see, the original design was 109mm wide. Doesn't get more accurate than that.

![images/f55dbd6f1c13c31cd5e4509bafd96d8e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f55dbd6f1c13c31cd5e4509bafd96d8e.png)


---
**John Revill** *May 15, 2017 12:57*

I've labeled this 160, but mine is actually 80.

![images/5b06fb040f5dd13f416c51f7b612d7ae.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5b06fb040f5dd13f416c51f7b612d7ae.jpeg)


---
**John Revill** *May 15, 2017 12:59*

Again, i've labeled this 157.575, but i've set mine to 78.788.

![images/63cc3e18d54b3f9517a49e6d416b8a89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/63cc3e18d54b3f9517a49e6d416b8a89.jpeg)


---
*Imported from [Google+](https://plus.google.com/114782552977074073979/posts/NYWBtrj6w1J) &mdash; content and formatting may not be reliable*
