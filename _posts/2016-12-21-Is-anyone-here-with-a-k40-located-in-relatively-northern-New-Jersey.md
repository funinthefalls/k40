---
layout: post
title: "Is anyone here with a k40 located in relatively northern New Jersey?"
date: December 21, 2016 17:17
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
Is anyone here with a k40 located in relatively northern New Jersey?  





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Randy Randleson** *December 21, 2016 18:04*

I am in Levittown, PA




---
**Randy Randleson** *December 21, 2016 18:04*

not exactly northern NJ, but somewhat close




---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2016 18:05*

**+Wayne Moore** does your k40 have the ribbon cable by chance? 


---
**Randy Randleson** *December 21, 2016 18:06*

Ill be back at its location in a bit and let you know


---
**Randy Randleson** *December 21, 2016 21:57*

This one?

![images/0f3f6613478777e874908d5cc16f5e85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f3f6613478777e874908d5cc16f5e85.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 21, 2016 22:03*

Yeah thats what I need. Messaged you on hangouts. 


---
**Ray Kholodovsky (Cohesion3D)** *December 22, 2016 05:59*

This worked out well. Just went to Pennsylvania and back - Wayne graciously allowed me into his shop and we verified that the ribbon cable on the production mini boards works great. 

With that developmental milestone taken care of, the assembler has been notified and will begin producing the mini batch now. 

Cool dude - thanks Wayne! 


---
**Randy Randleson** *December 22, 2016 14:33*

Thanks Ray, it was great to meet you and I can't wait to get a couple of your boards as soon as they become available!


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/BuvX9mmk2Xe) &mdash; content and formatting may not be reliable*
