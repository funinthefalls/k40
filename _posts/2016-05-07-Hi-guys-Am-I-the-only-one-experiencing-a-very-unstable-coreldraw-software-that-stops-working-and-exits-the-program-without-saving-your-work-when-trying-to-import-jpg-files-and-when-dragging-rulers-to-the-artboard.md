---
layout: post
title: "Hi guys. Am I the only one experiencing a very unstable coreldraw software that stops working, and exits the program without saving your work when trying to import jpg files, and when dragging rulers to the artboard?"
date: May 07, 2016 21:14
category: "Software"
author: "Martin Larsen"
---
Hi guys.

Am I the only one experiencing a very unstable coreldraw software that stops working, and exits the program without saving your work when trying to import jpg files, and when dragging rulers to the artboard?



Are there better software that works with the original K40 hardware?

Best regards,

Martin 





**"Martin Larsen"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 07, 2016 22:19*

I had those sort of issues when I was using the CorelDraw 12 that was provided. I'm running Win 10, maybe you are too. I would have it crash any time I tried to open the object viewer panel or any other panel for that matter.



So I've changed recently to CorelDraw x5 & I don't have these issues anymore, although I have one issue with x5 in that the File Edit etc. etc. menus are basically invisible for some reason. If I click & hover my mouse over the area where it should be, then I can see which one I want (e.g. the Arrange so I can align things).


---
**Martin Larsen** *May 08, 2016 07:53*

Okay. Thanks for your feedback Yuusuf. I just found your post with the X5 software, and i think im gonna try this out. Im using win7 64 bit, but the software crashes if i try to import files, leaving it quite useless.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 08, 2016 08:26*

**+Martin Larsen** Yeah, the CD12 was a bit buggy for some reason. Not sure what causes that. It may be something to do with the CorelLaser plugin, but for me it seemed to happen even when not trying to use the plugin (i.e. just running CD12 on it's own). It was bothersome, so I just never tried to change any options. I would just open CorelLaser, import stuff, then hit cut/engrave. That's the only way I could get it to function.


---
*Imported from [Google+](https://plus.google.com/105195340721594982075/posts/SS9njgFb7C6) &mdash; content and formatting may not be reliable*
