---
layout: post
title: "One of those times where you start designing something and realize you can make something else out of the design as well"
date: November 11, 2017 22:19
category: "Object produced with laser"
author: "Ned Hill"
---
One of those times where you start designing something and realize you can make something else out of the design as well.  Was designing a wood button/ pin and then realized I could make some cool name tags with it as well.  



![images/a6dd219f01a5fbed34d829397bd13517.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a6dd219f01a5fbed34d829397bd13517.jpeg)
![images/94dc44ea98b4dc5b6c12e7c6a32edb43.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/94dc44ea98b4dc5b6c12e7c6a32edb43.jpeg)
![images/a0a621a5795159ad5ce11d88aba2c685.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0a621a5795159ad5ce11d88aba2c685.jpeg)
![images/dab026841f5cd3fbbd7869ac987f5aa7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dab026841f5cd3fbbd7869ac987f5aa7.jpeg)

**"Ned Hill"**

---
---
**Travis Sawyer** *November 13, 2017 12:33*

Looking good, brother!


---
**Jamie Richards** *March 31, 2018 09:12*

I was asked to make a Masonic clock for someone.  Progress so-far.  Hope where the shaft comes through is acceptable.  My uncle is a Mason and I may surprize him with one, too! 

![images/948ddb7ce4ed735b204b0b20f2d8e0d2.png](https://gitlab.com/funinthefalls/k40/raw/master/images/948ddb7ce4ed735b204b0b20f2d8e0d2.png)


---
**Travis Sawyer** *March 31, 2018 10:15*

Good placement Jamie. Looks great.  I approve, if it means anything... (Master of Berkshire Lodge, Adams MA)


---
**Jamie Richards** *March 31, 2018 10:19*

Awesome, thank you! 


---
**Ned Hill** *March 31, 2018 13:23*

**+Jamie Richards** Looks awesome. Making a Masonic clock has actually been on my to make list for awhile. 😀


---
**Jamie Richards** *March 31, 2018 14:43*

Thanks!  I'm making a backing plate for it with a hanger opening to help it sit flush on the wall, so I'll see how that goes.  There are so many possibilities with these machines! 


---
**Jamie Richards** *April 03, 2018 10:54*

It's finished.  Wonder if the hands should be darker?  Hope he likes it!

![images/ce050a65cdfd93a6852186e6b8ab124e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/ce050a65cdfd93a6852186e6b8ab124e.png)


---
**Jamie Richards** *April 03, 2018 10:56*

Here's how I did the back.

![images/3fe7d044a72b1e8263dffce787f2890e.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3fe7d044a72b1e8263dffce787f2890e.png)


---
**Ned Hill** *April 03, 2018 17:55*

Very cool.  Personally I probably would have opted for darker hands but it still looks awesome.  Nice work. :)


---
**Jamie Richards** *April 03, 2018 21:21*

Thank you!  I thought darker hands might look better, so I sent him a pic in case I needed to paint them, but he likes it this way and I'll be dropping it off tomorrow!  I want to experiment with inlay stuff, too!  Thanks for your help, guys, I really appreciate it!


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/idXUfn7UYFP) &mdash; content and formatting may not be reliable*
