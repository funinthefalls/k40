---
layout: post
title: "We're celebrating one amazing year since the launch of our webstore, and many hundreds of 3D Printers, CNCs, and especially Laser Cutters upgraded, with 15% off sitewide with coupon code ONEYEAR (ends Monday 11/27 at 11:59PM"
date: November 24, 2017 19:42
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
We're celebrating one amazing year since the launch of our [cohesion3d.com](http://cohesion3d.com) webstore, and many hundreds of 3D Printers, CNCs, and especially Laser Cutters upgraded, with 15% off sitewide with coupon code ONEYEAR (ends Monday 11/27 at 11:59PM EST)



Now is a great time to pick up the Cohesion3D Mini Laser Upgrade Bundle for your K40 so you can engrave in full grayscale, use open software such as LaserWeb and Lightburn when it comes out shortly, and untether your machine (add the GLCD Adapter and GLCD Screen to run jobs from the board's memory card, without needing a computer connected) - all at a substantial discount. 



See the original post below for more details and to hear about the journey.  



Cheers!

-Ray



<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



One year ago, on Thanksgiving evening, I opened the [cohesion3d.com](http://cohesion3d.com) webstore to the public.  



The 6 months leading up to that, I had been making my 32-Bit Control boards the ReMix and the Mini by hand and sending them off to willing testers.  Finally, the first 100 unit production run of the Cohesion3D ReMix board which I had funded on a credit card was set to arrive on Friday, and the Mini board testing was going well in both 3D Printers and K40 Laser Cutters.



I launched the storefront with the ReMix board in stock and pre-orders for the Mini board and the Laser Upgrade Bundle.  Lots of K40 Laser Cutter Upgraders pre-ordered the Mini to get away from the frustrating and limiting software it comes with, to be able to do true grayscale engravings, and use open source and powerful software such as LaserWeb to control their machines. 



We sold out, we shipped, and we sold out again, all while working hard to navigate the challenges of hardware manufacturing, software compatibility, and make for the simplest and best possible user experience to upgrade their machines to the powerful benefits of a Cohesion3D board... And the user feedback has been phenomenal - we've fundamentally allowed people to breathe new life into machines that many found unusable beforehand. 



With all that said, I want to thank everyone that has been along for the ride, including my beta testers, community members, software developers, and customers, for being understanding of all the growing pains of a new small business and helping us get to where we are today. 



In celebration of the web store's 1 year anniversary, and as a further thank you, I am offering 15% off everything on [cohesion3d.com](http://cohesion3d.com) for today, the entire weekend, and Cyber Monday with coupon code ONEYEAR - this will expire Monday Nov 27 at 11:59PM EST.  If you have been thinking about upgrading your machine, a 32 bit controller is a great way to improve performance, and now is a great time to pull the trigger on that. 



As always, you heard it here first.  Please help spread the word, and happy holidays to all!



-Ray







**"Ray Kholodovsky (Cohesion3D)"**

---
---
**greg greene** *November 24, 2017 20:09*

well done !  I'm happy with mine !


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/cnzac9f15Fa) &mdash; content and formatting may not be reliable*
