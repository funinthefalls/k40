---
layout: post
title: "once more can I ask the group would this one be any good for air assist, it has the capacity of 23L per min starting at 30psi"
date: January 29, 2016 08:48
category: "Discussion"
author: "Norman Kirby"
---
once more can I ask the group would this one be any good for air assist, it has the capacity of 23L per min starting at 30psi

[http://www.ebay.co.uk/itm/Mini-Airbrush-Compressor-AS18-2-Oil-free-/131015709681?hash=item1e812513f1:g:BmoAAOSwQPlV89k3](http://www.ebay.co.uk/itm/Mini-Airbrush-Compressor-AS18-2-Oil-free-/131015709681?hash=item1e812513f1:g:BmoAAOSwQPlV89k3)





**"Norman Kirby"**

---
---
**Stephane Buisson** *January 29, 2016 08:53*

I do have a compressor of that size, no problem with the air output, but the noise make me develop another solution, will post soon, at the end of the weekend.


---
**Phillip Conroy** *January 29, 2016 09:14*

I use full size air compressor with  pressure regulator set to 6psi with water trap,


---
**Tony Schelts** *January 29, 2016 09:14*

I pretty much have the same, different name but AS18m and it works fine.  I dont mind the noise.  Its not too loud 


---
**Josh Rhodes** *January 29, 2016 15:12*

That's basically what we have at **+Mobile Makerspace**​ but I wish it was located somewhere far away because of the noise. 



You want this kind because you want a certain amount of flow and no moisture. 



So this will work, but I would put it in a closet or attic or enclosure or something. 


---
**Phillip Conroy** *January 29, 2016 19:58*

How about using rechcled fridge compressos 


---
**Steve Gray** *January 30, 2016 16:11*

i've done some tests with compressed air, and higher pressure/flow seemed to cut slower. so any compressor will work, it just needs to be strong enough to blow out any flames and smoke


---
**ChiRag Chaudhari** *January 30, 2016 16:20*

I had this compressor and it used to get hot and stop momentarily. So I switched to this one:

[http://www.amazon.com/Hydrofarm-AAPA110L-112-Watt-110-LPM-Commercial/dp/B002JPPFJ0/ref=sr_1_4?ie=UTF8&qid=1454170513&sr=8-4&keywords=commercial+air+pump](http://www.amazon.com/Hydrofarm-AAPA110L-112-Watt-110-LPM-Commercial/dp/B002JPPFJ0/ref=sr_1_4?ie=UTF8&qid=1454170513&sr=8-4&keywords=commercial+air+pump)

About the same price, same noise level (which is not too lound IMO), runs for longer time without getting hot or stop, no need to use filter to remove moisture.  


---
*Imported from [Google+](https://plus.google.com/103798744920995011573/posts/7CQWHmDEZ5N) &mdash; content and formatting may not be reliable*
