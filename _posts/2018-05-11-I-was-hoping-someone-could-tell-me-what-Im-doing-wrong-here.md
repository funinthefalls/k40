---
layout: post
title: "I was hoping someone could tell me what Im doing wrong here"
date: May 11, 2018 13:03
category: "Original software and hardware issues"
author: "Bob Damato"
---
I was hoping someone could tell me what Im doing wrong here. I went to make an edge lit acrylic sign, on my 100% stock, unmodified K40 using corellaser. (I know I know....!) 

I create the image in photoshop, convert to bmp, import into corellaser.



Once in CL, I make a new layer and draw cut lines to cut the acrylic at the right places.  What happens is, I can burn the image just fine (engrave) but when it comes to the cut, the cut is nowhere near the engraving! Here are a couple screen shots of my engrave and cut settings. In one shot you can kind of see where my cut lines are.  Any tips are much appreciated! 



Bob



![images/180464e5bffe8db5a885e78bf835bcff.png](https://gitlab.com/funinthefalls/k40/raw/master/images/180464e5bffe8db5a885e78bf835bcff.png)
![images/595c4a5ee8ae2f5e6c4efe8185c3e910.png](https://gitlab.com/funinthefalls/k40/raw/master/images/595c4a5ee8ae2f5e6c4efe8185c3e910.png)
![images/cc7060f0291d60a3581b2a11dfe5d938.png](https://gitlab.com/funinthefalls/k40/raw/master/images/cc7060f0291d60a3581b2a11dfe5d938.png)

**"Bob Damato"**

---
---
**HalfNormal** *May 12, 2018 05:34*

What some people have done is incorporate a 1x1 pixel dot in the top left corner of all drawings surrounded by a white box that is the same size as the cutting area so that cut and engrave match up. 


---
**Kelly Burns** *May 12, 2018 16:01*

I have to leave, but I did this with Moshi,  if no one answers I will provide a step-by-step later today




---
**Kelly Burns** *May 12, 2018 16:03*

Sorry, I missed HalfNormal's comment.  That is the basic idea.  Have a single Pixel   (I used a short line) in the upper left corner that is active for both Engraving and Cutting functions.  


---
**Bob Damato** *May 12, 2018 16:06*

Thank you!  I <b>think</b> I know what youre trying to say but it didnt work as planned still :(  Would the box be on the same layer?




---
**Kelly Burns** *May 12, 2018 16:11*

Yes. I didn’t use layers.  I selected items  and the chose cut or engrave from menu.  In your case, since you are using layers, You would have the line/dot in the same spot in both layers.    



Hint: Make a small design to work out the concept so you don’t waste material  and you find out if it works much more quickly. 


---
**Kelly Burns** *May 12, 2018 16:37*

by the way, the Pixel/Line should be where your origin is.  My K40 was Upper Left.   If yours is in the lower left, then you should place it there.  



I was waiting for my ride and did a little searching.  I found this article.  Its using Moshi, but explains the concept pretty well.   [instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](http://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)


---
**Bob Damato** *May 13, 2018 12:56*

Thank you for the link Kelly,  I will definitely read this!




---
**Bob Damato** *May 15, 2018 13:36*

That tutorial worked great for me, thank you! 


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/bXzUuMyfhpW) &mdash; content and formatting may not be reliable*
