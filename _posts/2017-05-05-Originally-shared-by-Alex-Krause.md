---
layout: post
title: "Originally shared by Alex Krause"
date: May 05, 2017 03:27
category: "Materials and settings"
author: "Alex Krause"
---
<b>Originally shared by Alex Krause</b>





**"Alex Krause"**

---
---
**Mark Brown** *May 05, 2017 17:10*

Wow, crazy difference between the engraves.


---
**Mark Brown** *May 08, 2017 01:56*

A little more info I just found:



"Cast sheet has the following benefits over extruded sheet:

- less low angle distortion

- low minimum run/order quantities

- thousands of standard and custom colors available

- greater range of available thicknesses (available in thicknesses greater than 1.00")

- slightly better chemical resistance

- less tendency to melt or chip during machining

- slightly higher service temperature

- more suitable for use involving continuous contact with water

- better capability to handle long-term mechanical loads



Extruded sheet has the following benefits over cast sheet:

- lower cost

- better thickness tolerance

- less dirt,lint or particulate contamination in the sheet

- cements and thermoforms faster"

Source: [http://www.usplastic.com/knowledgebase/article.aspx?contentkey=887](http://www.usplastic.com/knowledgebase/article.aspx?contentkey=887)


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/PNURfm6htwq) &mdash; content and formatting may not be reliable*
