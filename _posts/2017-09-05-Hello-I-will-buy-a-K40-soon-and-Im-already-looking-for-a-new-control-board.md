---
layout: post
title: "Hello, I will buy a K40 soon and I'm already looking for a new control board"
date: September 05, 2017 14:10
category: "Modification"
author: "Daniele Salvagni"
---
Hello, I will buy a K40 soon and I'm already looking for a new control board. Cohesion3d looks like the best option but too expensive for me, with shipping and taxes it would probably cost me arount 140-150$, I'm looking for something far less expensive.



Would something like this work? I plan to use GRBL:

- Arduino Nano (inexpensive/already have)

- CNC Shield 4.0 [https://goo.gl/KckzA1](https://goo.gl/KckzA1) (7$)

- DRV8825 Driver [https://goo.gl/jFtrxh](https://goo.gl/jFtrxh) (~1$ each)



Which limitations would I have with this setup? Do I need a fan with the 8825?

Is there some benefit in using Smoothieware instead of GRBL? (GRBL at the moment has better support for Laserweb)





**"Daniele Salvagni"**

---
---
**Stephane Buisson** *September 05, 2017 21:03*

**+Daniele Salvagni**

Look like you did some search before (Smoothie, GRBL & LW).

but maybe you miss the following point that arduino nano isn't powerful enough for best engraving. again you can't compare fast 32bits chips vs slow 8 bits.



So if you prefer quality over little saving (board), you will save yourself a lot on material (for testing), without counting your time. no more than 80 $ difference and is well worth it.



not trying to convince you, just saving you the troubles.


---
**Daniele Salvagni** *September 05, 2017 22:04*

**+Stephane Buisson** Thanks, I will consider it but sadly for me the difference is bigger as I live in the EU. With shipping and taxes I extimated about 150$ for a Cohesion3D Mini which is almost half the cost of the k40.


---
*Imported from [Google+](https://plus.google.com/117822352975366881011/posts/BHVxtrhvWqx) &mdash; content and formatting may not be reliable*
