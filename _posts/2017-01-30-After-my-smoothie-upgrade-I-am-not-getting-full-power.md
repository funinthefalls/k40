---
layout: post
title: "After my smoothie upgrade I am not getting full power"
date: January 30, 2017 04:39
category: "Smoothieboard Modification"
author: "Kris Backenstose"
---
After my smoothie upgrade I am not getting full power. My understanding its caused by 3.3v pwm signal from the smoothie.



What ways can I increase this voltage to 5v?





**"Kris Backenstose"**

---
---
**K** *January 30, 2017 06:56*

Are you using a level shifter?


---
**Kris Backenstose** *January 30, 2017 07:02*

I ended up connecting 5v to IN. Now I am getting more mA.


---
**Anthony Bolgar** *January 30, 2017 15:01*

That is the way I did it as well, PWM to the L terminal and 5V to the IN (However I have a pot on one of the machines so I can set it as a hard maximum limit, around 4.5V)


---
**K** *January 30, 2017 15:23*

By 5V do you mean the 3.3v from the board or 5V from a shifter?


---
*Imported from [Google+](https://plus.google.com/113059362277072411228/posts/ZLnFFi5jASa) &mdash; content and formatting may not be reliable*
