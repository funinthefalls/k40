---
layout: post
title: "I am trying to get LaserWeb working with my machine.."
date: May 06, 2016 12:23
category: "Discussion"
author: "Custom Creations"
---
I am trying to get LaserWeb working with my machine.. I have Ramps/Marlin installed and I can get the LaserWeb to open in browser and it says server connected but it will not connect to Ramps.. I followed the instructions for windows install.. Please, what am I doing wrong or not doing properly? **+Anthony Bolgar** 





**"Custom Creations"**

---
---
**Ariel Yahni (UniKpty)** *May 06, 2016 13:29*

**+Custom Creations**​ this is LW1 or LW2? 


---
**Custom Creations** *May 06, 2016 13:54*

**+Ariel Yahni** LW1


---
**Ariel Yahni (UniKpty)** *May 06, 2016 13:57*

Are you on Windows? Does you serial port appear on the dropdown on the top left? Post a picture of the cmd/terminal after executing node server to run LW


---
**Roberto Fernandez** *May 06, 2016 14:06*

LASER WEB or LASERWEB2?


---
**Anthony Bolgar** *May 06, 2016 14:08*

As **+Ariel Yahni** posted, a picture of the terminal window would really help.


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/ZWbVDz6xguy) &mdash; content and formatting may not be reliable*
