---
layout: post
title: "Gone quiet here a bit, hope everyone is busy making :) Here's a project my daughter and I came up with as a birthday gift for one of her best friends"
date: August 05, 2017 17:25
category: "Object produced with laser"
author: "Ned Hill"
---
Gone quiet here a bit, hope everyone is busy making :)  Here's a project my daughter and I came up with as a birthday gift for one of her best friends.  Her friend is off to college at NC State University this fall semester.  Made from a 1.75" (45mm) cube of black walnut and etched on all sides.  Drilled a hole in the top and inserted a small air plant.  Perfect for a college dorm room.



![images/418e72e6bf615247d5481ef8ba0fe8ba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/418e72e6bf615247d5481ef8ba0fe8ba.jpeg)
![images/875fb82b7d482b90c3f6f24e0074da67.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/875fb82b7d482b90c3f6f24e0074da67.jpeg)

**"Ned Hill"**

---
---
**Joshua Guthrie** *August 13, 2017 01:27*

Go Pack!




---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/LBGx2n3bg1m) &mdash; content and formatting may not be reliable*
