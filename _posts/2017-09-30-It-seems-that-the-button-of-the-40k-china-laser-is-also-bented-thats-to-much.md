---
layout: post
title: "It seems that the button of the 40k china laser is also bented, thats to much"
date: September 30, 2017 16:18
category: "Original software and hardware issues"
author: "BEN 3D"
---
It seems that the button of the 40k china laser is also bented, thats to much. I ask my reseller on ebay called mosttool to send me a replacement. I hope he will replace it....otherwise I have to complete rebuild it ....



![images/cf2481d397771d69113155d92b2572d5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf2481d397771d69113155d92b2572d5.jpeg)
![images/79f439003bab9fcaf21809a88696bb54.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/79f439003bab9fcaf21809a88696bb54.jpeg)
![images/98415c5e193ad6b2f2d9b8546a96bfc0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/98415c5e193ad6b2f2d9b8546a96bfc0.jpeg)
![images/c1249990f0e3501416715794266c8f2e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1249990f0e3501416715794266c8f2e.jpeg)
![images/5d887203f541ec8c684e1d8ad0fc7218.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d887203f541ec8c684e1d8ad0fc7218.jpeg)

**"BEN 3D"**

---
---
**Adrian Godwin** *September 30, 2017 16:59*

Amazing ! 



The sheet-metal case always seems the most carefully-made part. The rusty and mangled brackets screwed to it, less so.




---
**Nate Caine** *September 30, 2017 17:02*

It looks like you got one of the <i>better</i> machines!  lol


---
**Gee Willikers** *September 30, 2017 17:06*

Screw the whole machine down to a piece of three-quarter inch plywood or in my case MDF. Provides more rigidity to the whole machine and more mass.


---
**BEN 3D** *October 01, 2017 09:48*

I will wait to the answear of the ebay seller first ... 


---
**Steve Clark** *October 01, 2017 16:18*

**+BEN 3D**  I agree, give them a chance to make good.


---
**BEN 3D** *October 04, 2017 04:39*

The Ebay seller does not answeared yet. So whatever a friend order a new button plate for me. I did not know what he ordered yet..... surprise . ...:-)


---
**BEN 3D** *October 04, 2017 20:08*

Today I had have a great Idea. I used a tube with water (german "water tube" -> "Schlauchwaage") instead of my "vernier caliper" -> "Schieblehre"  to mesure the  level of the axis is correctly. And it is unbeleavable, the button plate is dented and crooked, but the x and y carriage travel are working very well. So I am really happy today and it seems that the device is looking realy ugly, but it could works beatefule, after the mirrow alighned is finished ;-D



I feel like MacGyver ... "Solved with a tube and water!"


---
**Gee Willikers** *October 04, 2017 23:06*

We call that a water level in the states. I vaguely remember using one once.


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/VA9eNDuQfFJ) &mdash; content and formatting may not be reliable*
