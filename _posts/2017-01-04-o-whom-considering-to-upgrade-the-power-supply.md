---
layout: post
title: "o whom considering to upgrade the power supply.."
date: January 04, 2017 18:04
category: "Modification"
author: "Kostas Filosofou"
---
Τo whom considering to upgrade the power supply..



In left side it's 50w PSU and in the right side is 40w PSU ... can you spot the difference?? there is no difference in the board even have the same part number ..the only difference is in the High Voltage Flyback Transformer.

So if you want to upgrade from 40 to 50watt you can only change the Transformer with the bigger one (cost 20-40EUR) than change the whole PSU (cost 90-120EUR).







![images/0f4b38a7930623b894f942aed9aa3d63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0f4b38a7930623b894f942aed9aa3d63.jpeg)
![images/91a80b3743f201c11961fffdf1cf7958.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/91a80b3743f201c11961fffdf1cf7958.jpeg)

**"Kostas Filosofou"**

---
---
**Alex Krause** *January 04, 2017 18:22*

**+Don Kleinschnitz**​


---
**Don Kleinschnitz Jr.** *January 04, 2017 18:44*

Thats believable but I have not found a source of the flybacks have you **+Konstantinos Filosofou** 


---
**Kostas Filosofou** *January 04, 2017 18:58*

**+Don Kleinschnitz** Here for example [stores.ebay.de - Items in CNCOLETECH store on eBay !](http://stores.ebay.de/CNCOLETECH/CO2-Laser-Power-Supply-/_i.html?_nkw=High+Voltage&submit=Finden&_fsub=12352596012&_sid=489770982)


---
**Kostas Filosofou** *January 04, 2017 19:01*

or here 



[aliexpress.com - Online Buy Wholesale laser transformer from China laser transformer Wholesalers &#x7c; Aliexpress.com](https://www.aliexpress.com/w/wholesale-laser-transformer.html)



[https://www.aliexpress.com/item/50W-high-voltage-flyback-transformer-for-50W-CO2-laser-power-supply/32725581921.html?spm=2114.40010208.4.128.l3LP3v](https://www.aliexpress.com/item/50W-high-voltage-flyback-transformer-for-50W-CO2-laser-power-supply/32725581921.html?spm=2114.40010208.4.128.l3LP3v)


---
**Don Kleinschnitz Jr.** *January 04, 2017 19:19*

**+Konstantinos Filosofou** great, I never thought to call it a laser transformer. 

What is even more interesting is this:

[https://www.aliexpress.com/item/50W-high-voltage-flyback-transformer-for-50W-CO2-laser-power-supply/32725581921.html?spm=2114.40010208.4.126.QAdekb](https://www.aliexpress.com/item/50W-high-voltage-flyback-transformer-for-50W-CO2-laser-power-supply/32725581921.html?spm=2114.40010208.4.126.QAdekb)

..... transformer which is a replacement for 50W is the exact part number of a LPS I have on my bench that came from a K40????


---
**Kostas Filosofou** *January 04, 2017 19:40*

**+Don Kleinschnitz** maybe the only difference between the 40 and 50 watt is in the label :)


---
**Don Kleinschnitz Jr.** *January 04, 2017 19:48*

**+Konstantinos Filosofou** that would be sad!


---
**Ian C** *January 05, 2017 09:31*

Visually the two larger capacitors are bigger on the 50 watt psu, what differences are there in the ratings written on each? You may find the larger caps are resonsible for helping with the initial firing of the laser, which takes more current than keeping it running once it is firing. Hence the larger size on the 50 watt psu. Just to say it, for those not electronically minded I would advise caution modding your 40 watt psu, at these voltages it's best to be safe than sorry.


---
**Kostas Filosofou** *January 05, 2017 16:00*

**+Ian C** The difference between the capacitors is 90μf (50w is 560μf 220v and the 40w is 470μf 220v) i think the difference is too small. As for the size of the psu is exactly the same. Another part that i notice is different is the rectifier module in 40w the part no. is KBP310 and to the 50w psu is KBP206G..


---
**Don Kleinschnitz Jr.** *January 05, 2017 16:17*

Good catch guys. Those caps look like filter caps on the offline switcher not the HV flyback switch. These differences may simply be changes to improve the supply's reliability. Both these parts are common failures.  NOT ...... the KBP310 is 3Amp and the KBP206G is 2A??? Also the 220V rating on these caps seems marginal especially in non-US use cases.


---
**Don Kleinschnitz Jr.** *January 05, 2017 16:18*

ECHO  **+Ian C**  ....... unless you have experience with HV power...

 DO NOT MESS WITH THIS SUPPLY!

....... IT WILL KILL YOU!


---
**Ian C** *January 05, 2017 16:57*

Better to be safe eh. I use high power PSU's in my day job and I tread carefully when opening them up and I don't even go down to component level. Although mine only output 12 to 50 volts not 15,000+

![images/d326ad24d2e2f8f361e1b3c80ae3960d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d326ad24d2e2f8f361e1b3c80ae3960d.jpeg)


---
**Don Kleinschnitz Jr.** *January 05, 2017 17:05*

**+Ian C** yah there is a big difference between 20,000v @ 20ma and most utility voltages. However the input side of the supplies you work with can hurt bad ....


---
*Imported from [Google+](https://plus.google.com/+KonstantinosFilosofou/posts/h9uY13fun7o) &mdash; content and formatting may not be reliable*
