---
layout: post
title: "Does anyone know if the Cohesion3D or smoothieboard 5x v1.1 will make it where the K40 will change power levels in the software vs using the knob on the machine?"
date: May 27, 2018 22:45
category: "Modification"
author: "C and K Cards Games & More"
---
Does anyone know if the Cohesion3D or smoothieboard 5x v1.1 will make it where the K40 will change power levels in the software vs using the knob on the machine? Or what do I need to make the machine do that and better controller from the stock.





**"C and K Cards Games & More"**

---
---
**Martin Dillon** *May 27, 2018 23:04*

Cohesion 3D will.  I think it is a percentage of the knob setting.


---
**C and K Cards Games & More** *May 28, 2018 00:48*

I seen some videos of people saying that you set the knob to max and the software will do % of that, but didnt say what was used to have software control the laser.




---
**Don Kleinschnitz Jr.** *May 28, 2018 00:59*

It depends on how your power control is connected. The most popular is the smoothies pwm on L and a pot on the IN pin.

The net power = software df% * the pot df%. Think of it like the L pin is the programmable power and the pot is the overall intensity, adjustable to account for practical elements like material differences and tube decay. 


---
**Stephane Buisson** *May 28, 2018 12:35*

yes the pot define the ceilling, the software a % of that range. notice as well you need a minimum intensity to trigger the laser(around 4mA).


---
*Imported from [Google+](https://plus.google.com/103419089723576845802/posts/PLtV63MwzpE) &mdash; content and formatting may not be reliable*
