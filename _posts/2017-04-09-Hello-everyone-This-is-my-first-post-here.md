---
layout: post
title: "Hello everyone, This is my first post here"
date: April 09, 2017 18:22
category: "Smoothieboard Modification"
author: "Cris Hawkins"
---
Hello everyone,



This is my first post here. I recently bought a K40 laser cutter and am upgrading to Smoothieware (using a Re-ARM board from Panucutt Devices) right away, without even turning the machine on. So far I have connected the Re-ARM board to RAMPS (without stepper drivers installed) and have gotten the 12864 LCD working. The SD card reader on the display board is intermittent, but the card reader on Re-ARM works fine.



The Re-ARM board seems ideal for the K40 since it is so basic and doesn't contain a bunch of circuitry that isn't used. My plan is to use the Re-ARM board with external stepper drivers (at least for now since I have a bunch of them). I will make custom cabling to connect to the LCD and stepper drivers so I won't need the RAMPS board.



Some questions I have are:

1. Since the Re-ARM board operates on 3.3v, do I need to do level shifting to interface with the LCD, SD reader, and stepper drivers?

2. I have a CNC Bridgeport that is retrofitted with MACH3 and it has what it calls "Soft Limits" to make sure the gcode program doesn't exceed the machine's working envelope. Does Smoothieware have this capability?

3. What is the maximum frequency that Re-ARM can provide to the stepper drivers? (This affects the max speed and/or  max resolution of the steppers)



Thanks in advance!





**"Cris Hawkins"**

---
---
**Stephane Buisson** *April 10, 2017 08:57*

Welcome here Cris.

I would not advice to do that, in cost and time it's not worth it. 

You need more hardware than just the 45usd, so the budget is more (not including postage fee for each element), wiring is your to do.

it would take time to debug. 

Panucatt  is just hacking into Smoothieware and not participating, doesn't benefit Smoothie assistance.


---
**Cris Hawkins** *April 10, 2017 15:32*

Stephane,



I have no idea why you say what you say. A Laser Cutter has an X axis, Y axis, and Laser PWM - THAT'S IT. How could this be so difficult?



I posted here because I found many referrals in my google searching. The referrals mention how active and helpful this group is. I request you live up to these referrals. Your negative attitude is easy to find, I am looking for something positive. A good place to start is to actually attempt to answer any of the three questions I posted above.



Also, in regard to Panucatt Devices, I found this post by Arthur Wolf himself over on the Smoothieware Forum:



"Panucatt does Open-Hardware boards, which is a very good start. They also send free hardware to contributors from time to time. They plan on helping a bit with the new web interfaces projects we have too, which is very appreciated. They definitely don't contribute as much as Robotseed or Uberclock which are very integrated with the Smoothie project's development, but in a world where we have things like MKS I'm definitely not going to complain about a company like Panucatt ( except maybe I'd like it if we heard less complaints about their customer support/documentation )."


---
**Stephane Buisson** *April 10, 2017 17:18*

**+Cris Hawkins**



Please don't give me any intention I don't have. You are not oblige to take my advice. but you could understand why you will not find a lot of info from this community on that matter.

I had read all the posts since the community creation, and I can tell you some have attempt other Panucatt smoothie compatible conversion with various grade of success, but their conclusion was it was not worth the trouble for the small price difference and time invested.



Now if you look the positive side, using the experience of the community with Smoothie 1 or 1.1 or C3D, you will find happy users. this is what is important to me. verified mod & hassle free.



I am trying to save you the effort, again, you are not obliged to lesson to me, and still be a happy member of this community. as I said before you are welcome.



I leave **+Arthur Wolf** or **+Ray Kholodovsky** with their own explaination.






---
**Cris Hawkins** *April 10, 2017 19:05*

Stephane,



NO, I DON'T understand why you say I won't find info from this community!



You say don't do it but you don't say why. You give no examples of the kind of issues, you just say NO!



You treat me as if I'm freeloader just because you don't like the brand of hardware I use. You say Panucatt does not contribute but as I have already shown, you are wrong! Maybe they don't contribute as much as others, but they DO contribute.



Besides, you have no idea how much I have contributed via PayPal to the Smoothie project.



Why does it have to be so difficult to get answers to three questions!



Some questions I have are:

1. Since the Re-ARM board operates on 3.3v, do I need to do level shifting to interface with the LCD, SD reader, and stepper drivers?

2. I have a CNC Bridgeport that is retrofitted with MACH3 and it has what it calls "Soft Limits" to make sure the gcode program doesn't exceed the machine's working envelope. Does Smoothieware have this capability?

3. What is the maximum frequency that Re-ARM can provide to the stepper drivers? (This affects the max speed and/or max resolution of the steppers)






---
**Stephane Buisson** *April 10, 2017 21:50*

**+Cris Hawkins**, I am quiet happy to let you dig in all posts, for you to find your answers, if you think it's in. please don't trust me.



On a other side, let's see if the tone of your previous comments is appealing to others members to answer you.


---
**Arthur Wolf** *April 10, 2017 22:09*

1. 3.3v is the voltage of the logic outputs ( it still provides 5V power to peripherals ). 3.3v logic should be fine for LCD and SD, but might not be enough for steppers depending on model ( very rare, mostly it's fine ). Give exact stepper driver model and we can look at specs.

2. Smoothie just got soft limits a week or two ago, it's not documented yet but should be soon.

3. 100khz.


---
**Cris Hawkins** *April 10, 2017 22:31*

Stephane,



In my first post, I asked three specific questions. For some reason that is unknown to me, you did not address any of them, but proceeded to tell me that what I am doing is not worth doing. You gave me no explanation nor details.



Then you tell me "Panucatt is just hacking into Smoothieware and not participating, doesn't benefit Smoothie assistance."



What tone are you taking? It sounds to me like you do not have any desire to provide assistance.



All I want is the answer to three questions.



It just so happens that I was concurrently in an email conversation with Arthur Wolf. He suggested that I am being oversensitive.



Stephane, I apologize for being too sensitive to your comments and therefore misunderstanding them. I get that English is not your first language, so I now see there is a possibility that this can lead to more misunderstanding. I apologize for not taking this into account earlier.



I do not understand what your original point is. Can you please explain in more detail?



Also, is there a problem with me using a Re-ARM from Panucatt Devices? From your previous comment, it sounds like there is.



Stephane, I apologize for the misunderstanding. Can we start over?



Cris


---
**Cris Hawkins** *April 10, 2017 22:40*

Arthur,



Thanks for the answers. I have the specs for the stepper drivers, so I'll check.



I look forward to docs on soft limits!



Cris


---
*Imported from [Google+](https://plus.google.com/112522955867663026366/posts/e5efUKgdKVm) &mdash; content and formatting may not be reliable*
