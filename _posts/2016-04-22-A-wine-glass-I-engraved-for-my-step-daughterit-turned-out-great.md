---
layout: post
title: "A wine glass I engraved for my step daughter...it turned out great"
date: April 22, 2016 10:03
category: "Object produced with laser"
author: "Scott Thorne"
---
A wine glass I engraved for my step daughter...it turned out great. 

![images/5885196675f1eef312e6bcd33d80019b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5885196675f1eef312e6bcd33d80019b.jpeg)



**"Scott Thorne"**

---
---
**ED Carty** *April 22, 2016 12:41*

Nice


---
**Scott Thorne** *April 22, 2016 12:55*

**+ED Carty**...thanks brother...lol...the one I made for my wife broke while I was engraving it...I was to close to the rim.


---
**ED Carty** *April 22, 2016 13:40*

See we all learn. I acid etch glass myself...


---
**ED Carty** *April 22, 2016 13:40*

**+Scott Thorne** Much more time consuming tho i believe to acid etch


---
**Scott Thorne** *April 22, 2016 13:48*

**+ED Carty**...the one in the photo took about 15 seconds.


---
**ED Carty** *April 22, 2016 16:20*

yea way faster.. ha ha


---
**Chris Parks** *April 24, 2016 03:04*

mind if i ask what setting were you using


---
**Scott Thorne** *April 24, 2016 03:49*

Cutting...50mm/s at 8 mA.


---
**Chris Parks** *April 24, 2016 03:52*

Thank you 


---
**Lance Robaldo** *May 10, 2016 13:02*

Did you use a rotary table?  Or the "rice bowl" method?  I've seen nice things done without the rotary, but this one seems to wrap the curve a bit so I'm just curious.


---
**Scott Thorne** *May 10, 2016 15:46*

I've got a rotary attachment. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/CGDaXmvWffB) &mdash; content and formatting may not be reliable*
