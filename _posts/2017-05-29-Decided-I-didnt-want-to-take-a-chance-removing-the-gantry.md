---
layout: post
title: "Decided I didn't want to take a chance removing the gantry"
date: May 29, 2017 20:14
category: "Discussion"
author: "bbowley2"
---
Decided I didn't want to take a chance removing the gantry.

Now I'm trying to figure LaserWeb out.  When I try to add a document I can see the preview but it doesn't open.  If I drag the same file to the spot indicated the image opens but there are no controls visible





**"bbowley2"**

---
---
**Ariel Yahni (UniKpty)** *May 29, 2017 20:29*

**+bbowley2**​ Can you post screenshots and more details please


---
**Don Kleinschnitz Jr.** *May 29, 2017 21:43*

FYI I cut the flue back without removing the gantry.

[donsthings.blogspot.com - K40-S Cabinet Modifications](http://donsthings.blogspot.com/2016/12/k40-s-cabinet-modifications.html)


---
**Joe Alexander** *May 29, 2017 22:52*

don't get intimidated by removing the gantry. After the first few calibrations you'll end up a pro. Gantry comes out and back in under 15min now when it was taking me like 2 hours the first time. Really is only 4 bolts which on mine self-aligned and weren't oval.


---
**bbowley2** *May 29, 2017 23:19*

here are some screen shots

![images/a15987e5758cf8233099b072e9ac215e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a15987e5758cf8233099b072e9ac215e.jpeg)


---
**bbowley2** *May 29, 2017 23:20*

![images/dc71475cf64c3bd3f5110a723069da1c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dc71475cf64c3bd3f5110a723069da1c.jpeg)


---
**bbowley2** *May 29, 2017 23:20*

![images/223ab0edb2e9e24e2c20305247393005.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/223ab0edb2e9e24e2c20305247393005.jpeg)


---
**Joe Alexander** *May 29, 2017 23:33*

by the add documents higher up click open and open the file there. you drop the items in the files(actual paths) in the lower area


---
**Ariel Yahni (UniKpty)** *May 29, 2017 23:51*

**+bbowley2** droping the files in the wrong place

![images/b16448a88061fef8bc8809445594d635.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b16448a88061fef8bc8809445594d635.jpeg)


---
**bbowley2** *May 31, 2017 17:05*

OK I dragged an image to the place just above the red circle.  I have an image on a big black field with no tools visible.  What's the magic word to show the tool bar?


---
**Ariel Yahni (UniKpty)** *May 31, 2017 17:10*

**+bbowley2** you lost me there. Post image


---
**bbowley2** *May 31, 2017 17:39*

Image is from an SVG.  White BG.  This is the entire screen.

![images/6e23dad046cfc33627cb8edfbc6f15ef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6e23dad046cfc33627cb8edfbc6f15ef.jpeg)


---
**Ariel Yahni (UniKpty)** *May 31, 2017 17:46*

That means you are not dropping the file in the correct place. Try use the other option that is to just select the vector you want to create an operation for and click where it says " create single operation "


---
**Joe Alexander** *May 31, 2017 20:58*

next to documents theres this little red flag that says" click here to begin". I'd try that one!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2017 13:27*

Yeah, what seems to be the issue is that you are dragging the file from in windows onto that "drop document(s) here" bit right? & it's doing nothing?



If I'm understanding correct, it's what **+Joe Alexander** points out. You need to start by clicking the "Add Document" button at the top, which will bring up a file open dialog box where you can select the file. Then it will load all the elements of the file into the bar on the left in a tree view. You can then select those elements & drag to the "drop document(s) here" part.



Then further down you will find the Operations panel appears below that "drop document(s) here" allowing you to specify what you want to do (e.g. Laser Cut, then set the specific settings).



Sorry if you've already figured this out. Just saw & no resolution in comments so I figured maybe not yet.


---
*Imported from [Google+](https://plus.google.com/110862182290620222414/posts/QaLSwKkuKMd) &mdash; content and formatting may not be reliable*
