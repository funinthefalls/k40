---
layout: post
title: "Anyone ever cut Delrin (Acetal) sheets? I have an enquiry to do some special gaskets .01 thick but my laser is in the middle of me installing a Peltier cooling system so I cant even do a test cut"
date: June 29, 2017 17:11
category: "Materials and settings"
author: "Steve Clark"
---
Anyone ever cut Delrin (Acetal) sheets? I have an enquiry to do some special gaskets .01” thick but my laser is in the middle of me installing a Peltier cooling system so I can’t even do a test cut. 





**"Steve Clark"**

---
---
**Steve Clark** *June 30, 2017 00:43*

'Inquiry' not 'enquiry' am I the only one who cannot edit on this forum?


---
**Jeff Johnson** *June 30, 2017 14:30*

It's possible. Before I got my own laser I had ordered a couple leather stamps that were laser engraved onto delrin pieces. The guy that did it was using an older co2 laser.


---
**Bill Keeter** *July 26, 2017 02:09*

I've cut 1/16" delrin for a few projects. Cuts pretty nice. Just leaves a little residue on the bed. 


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/7gUPd8AUrxb) &mdash; content and formatting may not be reliable*
