---
layout: post
title: "Im trying to do an etching on wood"
date: August 23, 2016 02:36
category: "Original software and hardware issues"
author: "Bob Damato"
---
Im trying to do an etching on wood. While I seem to have fantastic results making it a 3D relief, its not always what I want. Im looking to make a photo on wood that is not relief. I followed the instructions on youtube someone  posted here how to do it in corel draw but it still came out as a relief. Id like results something like this:


{% include youtubePlayer.html id="gMS7IZZ6oVI" %}
[https://www.youtube.com/watch?v=gMS7IZZ6oVI](https://www.youtube.com/watch?v=gMS7IZZ6oVI)





**"Bob Damato"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 23, 2016 03:01*

I have done various etchings on wood (usually merbau hardwood). When using to mark the wood I noticed that with my power set as low as it can possibly go & still fire it wouldn't create a bas-relief. So that is 4mA on my machine, with speed of 350mm/s @ 5pixel steps (with the stock software). Also, my material was 15mm out of focus (closer to the lens). It created a very distinct burn (i.e. black) on the wood, but if you run your hand over it it would feel as smooth as any other area of the wood (no relief at all).



edit: I guess these things also depend on the wood that you are using. Softer woods are probably likely to bas-relief no matter what you do.


---
**Alex Krause** *August 23, 2016 03:19*

**+Bob Damato**​ what kind of wood are you working with?


---
**Jim Hatch** *August 23, 2016 03:35*

Also check which kind of engrave option you're choosing - raised or sunken.


---
**Eric Flynn** *August 23, 2016 05:06*

Yuusuf may have the key there to get the results you want. Try moving your material out of focus to create more of a scorch than an ablation.


---
**Bob Damato** *August 23, 2016 11:23*

Yes I hadnt thought of that. Maybe speed up the laser and keep it a bit out of focus.  **+Alex Krause** Im using birch. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 23, 2016 11:43*

**+Bob Damato** Low power too. The higher it is the deeper it will burn into the wood.


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/Gj3jiBzbZ1A) &mdash; content and formatting may not be reliable*
