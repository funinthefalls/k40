---
layout: post
title: "Decided to order a couple USA ZnSe lenses for my K40 and 60W 5040 lasers"
date: February 18, 2018 20:35
category: "Modification"
author: "Anthony Bolgar"
---
Decided to order a couple USA ZnSe lenses for my K40 and 60W 5040 lasers. Hopefully they will live up to the claims of a smaller dot size as well as increased cutting ability. **+Scott Thorne** and I had a discussion about them last year and he was impressed with them. I'll let you all know how they work once I get them delivered and installed.





**"Anthony Bolgar"**

---
---
**Andy Shilling** *February 18, 2018 21:12*

I've gone over to a GaAs lense and Mo mirrors, Im yet to do a serious test but I can already tell I'm getting more power down to the job than the stock mirror/lense.  


---
**HalfNormal** *February 18, 2018 21:17*

Thanks for making me want to spend more money!


---
**Ray Kholodovsky (Cohesion3D)** *February 18, 2018 21:36*

Where did you order from? 


---
**Scott Thorne** *February 18, 2018 22:26*

I'm still using them my friend, they work great. 


---
**Scott Thorne** *February 18, 2018 22:28*

**+Andy Shilling** I use GaAs  most of the time now,  more cutting power and they last so much longer. 


---
**Anthony Bolgar** *February 18, 2018 22:33*

I ordered off of ebay from Cloudray.

Now I need to buy some GaAs's because of you now **+Scott Thorne** ;)


---
**Scott Thorne** *February 18, 2018 23:12*

**+Anthony Bolgar** You know you love the upgrades, ive spent all the money that I can spend upgrading mine, I just installed a new gantry from light objects, this thing purrs like a kitten....500 x 300 mm I need to talk to Ray Kholodovsky and see if he can get my fiber laser to fire using my Ruida controller....he would be the man to talk to.


---
**HalfNormal** *February 19, 2018 00:10*

**+Scott Thorne** actually Oz, the author of LightBurn, is a real Rudia controller whisperer.


---
**Don Kleinschnitz Jr.** *February 19, 2018 00:26*

#K40Optics


---
**Scott Thorne** *February 19, 2018 01:36*

**+HalfNormal** then i need to talk to both of them, I need Ray for his electronic experience...lol






---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/ekvMLVcUR5P) &mdash; content and formatting may not be reliable*
