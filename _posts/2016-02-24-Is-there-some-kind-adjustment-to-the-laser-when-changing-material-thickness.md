---
layout: post
title: "Is there some kind adjustment to the laser when changing material thickness?"
date: February 24, 2016 09:51
category: "Discussion"
author: "Tony Sobczak"
---
Is there some kind adjustment to the laser when changing material thickness? E. G. A half inch piece of material vs 1/8"?





**"Tony Sobczak"**

---
---
**Phillip Conroy** *February 24, 2016 10:24*

Slow the cutting speed down untill it cuts through in 1 or 2 passes,lower wotk so that the centre of the work is around50mm -which is the focal lenght of the laser lens-the point which the laser beam is narrowest


---
**Brooke Hedrick** *February 24, 2016 13:27*

I cut out a little 'feeler gauge' from 1/8" acrylic that lets me put it just under the lens holder and then I can move my table up or down until the other side of the gauge just touches the top of the work piece.


---
**Joe Keneally** *February 24, 2016 16:17*

I have a stock K40 and the bed is not adjustable (yet).  Any advice about setting focus? 


---
**Brooke Hedrick** *February 24, 2016 16:23*

Adjust the height of the material on the bed instead of the bed.  You may need to remove the 'stamp holder' platform.  I have used qtips, cut off pieces, nails.



It is obvious to me now, but when I first got the cutter it didn't occur to me that the beam was focused at a specific height for a small region.  This is why a motorized table becomes important if you are switching between material thicknesses often.


---
**Brooke Hedrick** *February 24, 2016 16:24*

At least easily adjustable, not necessarily motorized.


---
**Joe Keneally** *February 24, 2016 17:08*

You all have my gears turning - Thanks!


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/UWDkDS5LTzt) &mdash; content and formatting may not be reliable*
