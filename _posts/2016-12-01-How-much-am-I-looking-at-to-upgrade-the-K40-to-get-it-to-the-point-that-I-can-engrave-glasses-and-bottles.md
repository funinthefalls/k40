---
layout: post
title: "How much am I looking at to upgrade the K40 to get it to the point that I can engrave glasses and bottles?"
date: December 01, 2016 01:01
category: "Discussion"
author: "ben ball"
---
How much am I looking at to upgrade the K40 to get it to the point that I can engrave glasses and bottles? The information on here is great, it's just SO much for a newbie!





**"ben ball"**

---
---
**Alex Krause** *December 01, 2016 01:15*

[lightobject.com - Mini 2 Axis Rotary. Ideal for K40 Engraving Laser Machine](http://www.lightobject.com/mobile/Mini-2-Axis-Rotary-Ideal-for-K40-Engraving-Laser-Machine-P941.aspx)


---
**Alex Krause** *December 01, 2016 01:17*

That is an off the shelf unit. You can build one for cheaper... or find one on ebay possibly 


---
**ben ball** *December 01, 2016 01:41*

So just the rotary tool? No immediate need for air assist or changing the board?


---
**Alex Krause** *December 01, 2016 02:07*

Air assist is recommended 


---
**Jason Johnson** *December 01, 2016 21:49*

here are the upgrades I started with on my K40. 
{% include youtubePlayer.html id="jnpnJ1FvcQU" %}
[youtube.com - China Laser Upgrades](https://www.youtube.com/watch?v=jnpnJ1FvcQU&t=3s)


---
**ben ball** *December 01, 2016 23:42*

Thanks for the advice. Jason, how much do you figure you have into that set up?




---
**Kelly S** *December 02, 2016 00:32*

I did air assist with a new focal lens at the same time, then I added dual laser pointers for finding optimal focal point easy on my projects, made a cable chain for wiring and air line for the first two upgrades.   To end I made an adjustable bed and upgraded to a c3d board running smoothie for better laser control vs corel.  All together I figure I have about 150 total invested, but a lot of the items I made myself and didn't count my time or material. 


---
*Imported from [Google+](https://plus.google.com/117207220356642950492/posts/f3uyPdctwEh) &mdash; content and formatting may not be reliable*
