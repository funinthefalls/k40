---
layout: post
title: "Busy weekend for the laser, I still need to figure out engraving on slate"
date: April 23, 2018 10:59
category: "Object produced with laser"
author: "Printin Addiction"
---
Busy weekend for the laser, I still need to figure out engraving on slate.



![images/418954e5bfae9232bd4f451b1cc0f7e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/418954e5bfae9232bd4f451b1cc0f7e4.jpeg)



**"Printin Addiction"**

---
---
**Don Kleinschnitz Jr.** *April 23, 2018 12:39*

Nice work!


---
**Printin Addiction** *April 23, 2018 18:58*

**+Don Kleinschnitz** - Thanks Don.

  


---
**Printin Addiction** *April 24, 2018 01:27*

**+Don Kleinschnitz** - Thank you for all your help, your website help me out a lot!


---
**Don Kleinschnitz Jr.** *April 24, 2018 12:18*

Your welcome!


---
**Nitro Zeus** *May 16, 2018 16:04*

I'm also really interested in trying slate!


---
*Imported from [Google+](https://plus.google.com/+PrintinAddiction/posts/it48k1oiA4f) &mdash; content and formatting may not be reliable*
