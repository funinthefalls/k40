---
layout: post
title: "My frustration with the K40 and retro fitting Ramps"
date: June 07, 2017 09:47
category: "Modification"
author: "Ty Web"
---
My frustration with the K40 and retro fitting Ramps.



Hi Does any one have a working copy of the marlin software, for the Mega, and the correct version of the IDE loader, and the tools they use to convert from DXF to Gcode.

I have wasted 6 days trying to get this thing running reliably and the answer is ... about to chuck it in the bin....



Thus far.. Installed Ramps and Mega, cut over all the wiring. My PSU is the same as Dan's original picture. However I tried three different Marlin Kimbra variants and all have varying issues. I have one I dont know were I got it from that close to works. No PWM control  but it fires the laser ok and seems to follow Gcode mostly.  It wont connect to My Octo Pi. but the LCD functions fine. It truncates Gcode...dont know why. but the Gcode output from the laser looks nothing like the original drawing. I can check the Gcode in Grbl controller and it looks fine.

I cant get Grbl controler to talk to this version of marlinkimbra.

Down loaded one from Downunder35's ible, seems to work, with octo print for the most part, but the laser once fires never turns off so all the moves are lasered and the laser stayes on at the end.

So figured it was the Gcode, so went away from inkscape and tried DXF2Gcode, seems to work but as I say the output file is truncated by the laser and it compresses aprox 1/3rd of the cut in one corner....very strange. Tried Simplylaser, does all sorts of weird stuff.. half cuts circles, incomplete lines. 



basically I have tried 3 different Firmwares and multiple dxf to gcode programs, the limiting thing is the laser is being fed by Pronterface as this seems to be the only program I can get to talk to the firmeware that works the most...



HELP please, all I want is a working Marlin and either uses DXF to g code or inkscape and Octo print or if I must a PC with interface of some sort.



Ty





**"Ty Web"**

---
---
**HalfNormal** *June 07, 2017 12:40*

It has been awhile since I have played with a laser version of Marlin but if memory serves me correctly there is a change to the config.h for memory buffering. Try and search for increasing buffering on Marlin. 


---
**Joe Alexander** *June 07, 2017 22:45*

im not really familiar with marlin but it sounds like its compatible with grbl gcode.... shouldn't you ba able then toload grbl onto the board? still compatible with pretty much any gcode streaming program, just not sure about lcd support as there are so many kinds.


---
**Anthony Bolgar** *June 08, 2017 00:58*

A simple and cheap option would be to get an Arduino Uno and Grbl shield (about $20.00) and use that as a replacement controller. LaserWeb V4 runs great on grbl boards. If you are willing to spend more (around $100) you could get one of **+Ray Kholodovsky** 's boards , they are a smoothie based drop in replacement.


---
**HalfNormal** *June 08, 2017 03:48*

Here is one solution with an explanation of what it is and does;

[hobbytronics.co.uk - Arduino Serial Port Buffer Size Mod](http://www.hobbytronics.co.uk/arduino-serial-buffer-size)

Here is the version of LaserMalin I used without issues. 

[https://github.com/timschmidt/buildlog-lasercutter-marlin](https://github.com/timschmidt/buildlog-lasercutter-marlin)




---
**Stephane Buisson** *June 08, 2017 06:33*

**+Ty Web**, Sorry not an answer but a question :

What motivated your original hardware choice ? (think about other readers)



just hope it's not to save few $, to have a 20 years old processor with limitation issue, and many days to try to make it work, compare a modern solution at 100 $.



Could you enlight us ? (what point do I miss ?)


---
**Ty Web** *June 08, 2017 06:50*

Thanks All for your feedback, anyone using a later version of marlin in there K40 than the 4yr old bulldog version?? If not can anyone tell me which version of IDE they used to compile it, as I am on 1.6.8 and I am getting errors.

Thanks

Ty


---
**timne0** *June 22, 2017 12:36*

Hey **+Ty Web** I'm using bleeding edge MK4Duo.  Think we've got the problems ironed out of it now. Can't test till Tuesday, but try that.


---
**Ty Web** *June 23, 2017 04:50*

Hi Stephane, My Ramps hard ware choice if this is the hardware you are referring to is based on simplicity and known resource. I already have this hardware in my two 3d printers, Porting it to the laser was a no brainer, only one set of spares is all that I require. I don't Engrave images etc, Just text on the odd occasion, my use is to do cutting. I draw in draftsight, pass of to SimplyLaser and then cut. The original K40 couldn't handle the various formats so you had to do multiple steps through third parties and then the sizes were always out. I am aware there are plenty of other platforms like a smoothie etc, but for me its what I have, what I know and my needs are simple not complex.




---
**Ty Web** *June 23, 2017 04:52*

timne0

Hi thanks for the update, let me know how you go, as I am building another 3D printer at the moment and am considering upgrading the version of Marlin, and then also porting it across to the laser.


---
**timne0** *June 23, 2017 12:10*

MK4Duo was updated last night with the fixes - I'll be testing it on tuesday.


---
*Imported from [Google+](https://plus.google.com/101325553520553385988/posts/Lo3rGfG48FS) &mdash; content and formatting may not be reliable*
