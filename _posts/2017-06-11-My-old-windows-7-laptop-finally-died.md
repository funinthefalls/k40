---
layout: post
title: "My old windows 7 laptop finally died"
date: June 11, 2017 02:02
category: "Modification"
author: "Nathan Thomas"
---
My old windows 7 laptop finally died. 

I don't want to get it fixed. Is there a control board that would allow me to use one of my existing computers running win 10 and Corel x8?





**"Nathan Thomas"**

---
---
**greg greene** *June 11, 2017 02:24*

cohesion 3d


---
**Ned Hill** *June 11, 2017 02:37*

You can use the corel plugin with x8 and win 10 with the stock controller.  Otherwise you'll need to upgrade to a CD3 controller, design in x8 and export to laserweb.


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2017 02:42*

[cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)

And it just got back in stock :)


---
**Fernando Bueno** *June 11, 2017 09:46*

If you do not want to change more than computer, and therefore operating system, you can use Inkscape and the extension "Laser Draw Inkscape Extension", which allows you to forget about CorelLaser and Core Draw. This extension generates files for LaserDRW just by creating your design with the right colors: Red for cut lines, black for raster format recording and blue for engraving in vector format.



And also adds a very important value: Inkscape, and therefore the extension "Laser Draw Inkscape Extension", work perfectly in Linux, in addition to being free software.



You can download it from [scorchworks.com - Laser Draw (LaserDRW) - Inkscape Extension](http://www.scorchworks.com/LaserDRW_extension/laserdrw_extension.html)


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/4hYAnYKJXxS) &mdash; content and formatting may not be reliable*
