---
layout: post
title: "So Gary challenged me to nerd out a little and I did by designing this laser aligning jig"
date: November 30, 2015 02:07
category: "Hardware and Laser settings"
author: "DIY3DTECH.com"
---
So Gary challenged me to nerd out a little and I did by designing this laser aligning jig.  So far the results are interesting and if I get time tomorrow will start the adjustment as thinking the "fiddling" will take a couple hours...





**"DIY3DTECH.com"**

---
---
**Stephane Buisson** *November 30, 2015 08:44*

**+DIY3DTECH.com** We do know the link for your Youtube video channel, please don't repeat it any more.

I am sure with the large number of repeats everyone interested would be able to find your videos from your channel. Honestly it's start to be a bit invasive.


---
**Gary McKinnon** *November 30, 2015 10:01*

But he's just linking to the helpful video he's made.


---
**Stephane Buisson** *November 30, 2015 10:49*

Why not just add a new comment on previous post linked to the same channel ?

I just try to keep easy access to all subjects. too many posts afraid new readers because information is over diluted. 


---
**Gary McKinnon** *November 30, 2015 11:14*

Yes that's a good point, Google Communities doesn't lend itself well to organised info, oddly!


---
*Imported from [Google+](https://plus.google.com/+DIY3DTECHcom/posts/13Zn1nv5ZKs) &mdash; content and formatting may not be reliable*
