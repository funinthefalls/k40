---
layout: post
title: "It's not much, but my thickest cut to date with 1/8\" birch ply"
date: August 24, 2016 01:00
category: "Object produced with laser"
author: "Carl Fisher"
---
It's not much, but my thickest cut to date with 1/8" birch ply. Up to this point I've been doing mostly engraving and acrylic cutting. Took 4 passes at around 15ma and 12mm/sec. LO head with air assist and 18mm lens.



![images/8e8f1f712b3051809ed0185da7f62c1a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e8f1f712b3051809ed0185da7f62c1a.jpeg)
![images/5701a27ff56c9a811558bfd140a94e5f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5701a27ff56c9a811558bfd140a94e5f.jpeg)
![images/f3a7596068b56693cb89dfdfff707f6e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f3a7596068b56693cb89dfdfff707f6e.jpeg)
![images/ad96f7b0b0f191e6a8651b367775db85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ad96f7b0b0f191e6a8651b367775db85.jpeg)

**"Carl Fisher"**

---
---
**greg greene** *August 24, 2016 01:09*

LOOKS NICE !


---
**Eric Flynn** *August 24, 2016 01:21*

Really?  4 passes at 15mA @ 12mm/s?  You need to seriously consider revisiting your alignment.  You should be able to get through 1/8" birch ply in one pass at that speed a tube current, especially with air.


---
**Jim Hatch** *August 24, 2016 01:36*

+1 Eric


---
**greg greene** *August 24, 2016 01:38*

Well, there's Birch - and there's Baltic birch


---
**Eric Flynn** *August 24, 2016 02:01*

Greg, shouldnt matter when we are talking 1/8" ply


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 24, 2016 03:41*

+1 Eric's comment. Also maybe consider your focal point. If it's out, then it will affect the ability to cut in 1 pass. Also, lens orientation. Bump side up. It honestly made a massive difference to me cutting through leather when I figured out mine was upside down.


---
**Paul de Groot** *August 24, 2016 04:14*

Same for me. Lens orientation and lens focus made all the difference 


---
**Phillip Conroy** *August 24, 2016 04:47*

Check that the focal lens is clean-shine a torch on it from diffrent directions to check,check centre of cutting peace is 50mm from bottom of lens ,i cut 3mm mdf at 8mm/sec and 10ma power in one pass.if everything checks out change focal lens -they do wear out


---
**Phillip Conroy** *August 24, 2016 04:48*

A good indcation something is wrong is how wide the cut looks from the top .if wider than 1mm something is wrong with focal lens


---
**Carl Fisher** *August 24, 2016 15:18*

The ply had some twist in it so it wasn't sitting at an ideal focal point at all corners. I did the best with what I had. 



The circle in the middle and the bottom were free after 2 passes. The additional 2 passes were to get the top of the castle free. There is almost no taper on the cut in the areas that freed up first which theoretically put me at about halfway into the board with my focal point. 



I'll try again on a small piece that does not have a twist, but I still don't have high expectations of making it through 1/8" in a single pass. Perhaps I don't have high enough expectations on this machine?




---
**Eric Flynn** *August 24, 2016 15:23*

Even with the twist, and that power, 2 passes should have done it.  It can do 1/8" in one pass. Your expectations are OK. You just need to get it set up properly.  The K40 when set up right is far more capable than the info on most of the web will have you believe.


---
**Carl Fisher** *August 24, 2016 15:26*

I admit I never did run a slant board to find my ideal focal point. With the acrylic I've been cutting I never had the need. This was my first go with ply and anything thicker than 2mm. 



Maybe I'll break down and run a slant board in there to find the best focal point. I also want to play with the focus lens orientation just in case. There has been so much debate about the orientation I honestly don't recall which way I put it in there back when I added the LO head.




---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/Z7zvXpxsoEJ) &mdash; content and formatting may not be reliable*
