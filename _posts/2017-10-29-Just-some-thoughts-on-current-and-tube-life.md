---
layout: post
title: "Just some thoughts on current and tube life"
date: October 29, 2017 07:24
category: "Discussion"
author: "Anthony Bolgar"
---
Just some thoughts on current and tube life.



Everyone seems to think that running the tube at lower mA settings will make it last longer. That is true. However, it does not mean that you can make more items doing this, because of the slower speed and multiple passes you need to make.



For example:

 

On my 60W laser I can cut 1/4" MDF in 1 pass at 8mm/s with a current of 20mA

At 10 mA it takes 2 passes at 5mm/s.



So in the same amount of time I can cut 3 times as much at the higher mA setting. 



So if the tube, for example, is good for 1000 hours at 10mA current and only 500 hours at 20mA, I can actually cut 1.5 times the amount of product before the tube dies vs the amount able to cut in 1000 hours at 10mA.



This also allows me to get product out to customers quicker, and my laser is making me more $/hour at the higher mA setting.



One final thought is that if you are selling products made on your laser and you want to make a $20/hour wage, your pricing should have the laser producing $40/hour worth of product so as to cover overhead, maintenance, and advertising costs. That way you can cover the cost of a replacement tube when it is needed.



Bottom line: If you are using the laser for commercial purposes, treat the tube as a consumable and build the tube replacement cost into your pricing structure.



#tubelife







**"Anthony Bolgar"**

---
---
**Phillip Conroy** *October 29, 2017 09:42*

All true,my new spt tube is rated at as 50 watt tube ,you can push it to 60watts by using 25ma power ,the good tubes will always have a built in time correction power level ,so that it will still output 50w at its end of life pwr level,the company alowsig 10w for pwr drop during its life,.

Tube is a 10,000 hour tube at 20ma not sure what this means tho ,  is it for 


---
**Jim Hatch** *October 29, 2017 13:27*

The only issue with your calculation is that tube life is not linear. The affect of power vs life is pretty flat from 1%-90%. It starts to take off from there with the curve going exponential. The 95-98% curve goes almost vertical in terms of shortening tube life. Over 100% and the curve goes negative as you overpower the tube. 



The way to look at it is under 95% power a minute of burn takes a minute of tube life. Between 95 to 98% a minute of burn might eat 2 minutes of tube life. Over 98% and that minute is costing you 10 minutes of tube life. Over 100% and that minute cost you 100 minutes or more.



So low power (under 95-98%) doesn't "save" you tube life so it's a good idea (generally) to use 95-98% (depending on your level of conservatism) so you can maximize speed and thus reduce time the tube is burning.



This does depend on tube quality and true rating. You can overpower a tube by as much as 20% so the Cheap Chinese Laser will spec that as the tube's rating - my 40W machine actually has a 32W tube. If I push 40W I'll kill the tube in short order. Good tubes will have two ratings - like 100W/115W and be sold as the lower rating. That is much safer to run at 98% than a K40. 



For lasers I manage that have the ability to set throttles, I set the max power to 98% so the user can spec 100% in his job but he'll only get 98%. 


---
**Anthony Bolgar** *October 29, 2017 13:50*

**+Jim Hatch** I agree with you totally, the 10mA and 20mA values I used as well as the 1000/500 hour values were just for illustrating the point with easy round numbers. I limit my tube to 95% power, usually most cuts are at 90-95% power.



I just enjoy being able to slice through 1/4" plywood like it was butter :)


---
**Jim Hatch** *October 29, 2017 14:11*

😀 I always start with full (95%) power and adjust the speed to get cuts as clean as possible. No point in wasting time with lower power/lower speed cuts as long as they're coming out cleanly. I have a calibration design I use to figure out the best power/speed for any material. 


---
**HalfNormal** *October 29, 2017 15:26*

Any product being used for commercial use should always be part of the equation of cost for production. Like all things if you cannot afford the upkeep then you should not be using it!


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/jGp6g8QidQS) &mdash; content and formatting may not be reliable*
