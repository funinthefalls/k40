---
layout: post
title: "Which type of power supply is your K40 equipped with?"
date: August 25, 2016 01:38
category: "Discussion"
author: "Scott Marshall"
---
Which type of power supply is your K40 equipped with?

The "GREEN"? The "WHITE"? - Something different??



Take a quick look and let us know.

I've been selling the conversion kits which require the user tie in here, and up until recently, I was under the impression the Green type were relatively rare. It seems that's not the case. I'm updating my kits to include the Green and White connectors as standard.



One trait of the "Green" Power supplies is the fan grille is centered on the PSU cover, and often doesn't have a fan. (This might be a cheap upgrade for those with the 'unpopulated' fan grille)

The "White" Power supplies seem to have the fan located in the front right corner of the cover and include a fan which runs whenever the PSU is powered (as opposed to coming on based on heat level)



Please weigh in on the power supply question, maybe we'll find the difference is where your K40 was made, maybe it will be when -  possibly newer models have the Green supply.



I'm not completely sure, but I believe both variations are made by the same manufacturer "JNMYDY" - the White ones are for sure. (website is JNMYDYen.com for the English version, and includes specs and pinouts for some variations. This one is known as the "Rubber Stamp" PSU. Maybe the K40 was originally used for engraving rubberstamps? - You never know what you'll find when you follow the K40 scavenger hunt...



Mines a White, and the 4 I just got in are also white, So plug-in adapters are now in production here at ALL-TEK.





**"Scott Marshall"**

---
---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2016 01:39*

Yep, white here as well, or as I call it plugs vs screw terminal variant. 



The green terminal psu tends to be the machine that has a ribbon cable, while my white connector machine had no ribbon. 


---
**Scott Marshall** *August 25, 2016 01:43*

Mine is White WITH ribbon cable. It seems like there's a LOT of variations. Wonder which one I'll trip on next...?


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2016 01:45*

Which is why I said "tends to" and not a definitive statement. 


---
**Scott Marshall** *August 25, 2016 01:48*

I "tends to"  is the way they do it, They use what they have available or cheapest at the time. There seems to be no parts network in place by any of the manufacturer/sellers.

The Green one in the picture uses orange wire as ground. I'd bet the wire color code used is determined by stock levels.


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2016 01:49*

Or the mood of the "technician" that day...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 01:50*

Green for me, as you know :) Mine has a bunch of writing on a sticker (in Chinese I assume) & says KH-40W. I have the ribbon connector. Mine was purchased in September of 2015.


---
**Eric Flynn** *August 25, 2016 01:53*

MIne is a blue psu, with mixed phoenix(green) and jst/molex (white) connectors, and it is non-ribbon with JST plugs on the motors.  I like my version best I think, compared to the others ive seen.


---
**Alex Krause** *August 25, 2016 02:00*

White connector but all the wires are Blue


---
**Scott Marshall** *August 25, 2016 02:01*

Sale on blue that day..


---
**Ariel Yahni (UniKpty)** *August 25, 2016 02:03*

Ive being working on getting the schematics for the PSU. Found a good source this week, hope it pays


---
**Eric Flynn** *August 25, 2016 02:07*

This is the exact PSU model I have, with thermally controlled fan  [http://www.rubylasertech.com/cn/cpzs/cojg/2015/1027/2.html](http://www.rubylasertech.com/cn/cpzs/cojg/2015/1027/2.html)


---
**Scott Marshall** *August 25, 2016 02:33*

**+Ariel Yahni** I've got 2 brand new ones here, 2 more on the way, I was going to de construct, but if you can do it the easy way, all the better. 



Time's become tight lately, I'm having trouble keeping all the balls in the air, the kits are selling steady, but my health hasn't been so steady. 



I'm being forced to slow up and do the best I can. Not happy about it, guess I was lucky to have a long run there. 



I've got a power supply project in the works, and a bunch of other things.



I wanted something to keep me busy, well it is.



Scott


---
**Ray Kholodovsky (Cohesion3D)** *August 25, 2016 02:40*

Scott are you hand assembling boards? I mean... you've seen my controllers and I've hand assembled all the tiny parts on there for all the units I've sold to date... but it's really worth looking into having a factory do it.


---
**Ariel Yahni (UniKpty)** *August 25, 2016 02:42*

**+Scott Marshall**​ amazed by your spirit really. I hope i get there with the same energy. I will get back you you on the PSU


---
**HalfNormal** *August 25, 2016 03:30*

**+Scott Marshall** the blue on the PS is the protective plastic on the aluminum. 


---
**I Laser** *August 25, 2016 03:37*

One green and one white... Could only vote once, I was sure both were green so voted green. Sorry for skewing the results :\


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 04:10*

**+HalfNormal** I pulled that stuff off thinking it might cause fires lol.


---
**Michael Knox** *August 25, 2016 08:31*

Green on my psu.


---
**Scott Fisher** *August 25, 2016 10:41*

Blue one here.


---
**Scott Marshall** *August 25, 2016 13:27*

**+Scott Fisher** Blue connector? Or power Supply Case?



If you have a blue connector, you may have a new variety yet un-reported. 

Which wouldn't surprise me much and I would very much like to know more.



Thanks, Scott


---
**Piotr Kwidzinski** *August 25, 2016 19:11*

Mine has "green" connector but wire colors are not friendly as 3 of them are yellow and one black on the other hand PSU itself has visible markings of signals like the "white" one on the picture. PSU itself has always on fan on right corner so I guess there are all sorts of combinations with those machines. **+Scott Marshall** you are doing great job making this adapters - wish you best with your health as well.


---
**Robi Akerley-McKee** *August 26, 2016 18:48*

Green with no ribbon and with ballast resistor and no outlets on back of machine.


---
**Scott Marshall** *August 29, 2016 10:32*

I'm surprised the Greens are winning by a large margin. 



I designed a adapter (plug-in no tools required) over the weekend and manufactered a dozen, they will be sent with detailed instructions to anyone who purchased a kit for the white connector and has a Green PSU.

They will also come with every kit shipped in the future,



Thanks, Mr Kiwidzinski, for your patience. Your unit went out Sat (I'll send you an email shortly)



Scott


---
**Scott Marshall** *August 29, 2016 10:34*

**+Robi Akerley-McKee**

How old is your machine? I was under the impression the ballast resistor/Moshi machines went out of production quite a while ago.


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/4USSDhpni4n) &mdash; content and formatting may not be reliable*
