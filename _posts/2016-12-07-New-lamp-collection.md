---
layout: post
title: "New lamp collection"
date: December 07, 2016 18:52
category: "Repository and designs"
author: "Amer Al fk"
---
New lamp collection 



![images/dfc987cef9d744332e40b6f621a2b33d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfc987cef9d744332e40b6f621a2b33d.jpeg)
![images/ae5afa9ee72031237b1e007382cf5764.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae5afa9ee72031237b1e007382cf5764.jpeg)
![images/ae3433ed4e9667d32d0f75cd12f88af4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae3433ed4e9667d32d0f75cd12f88af4.jpeg)
![images/09c5c07e535ed9f0e6cde8147901cbfb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/09c5c07e535ed9f0e6cde8147901cbfb.jpeg)
![images/2342315f4457ca600c83753a4c29ca18.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2342315f4457ca600c83753a4c29ca18.jpeg)
![images/499379062d4e50c48b7a40c27419e732.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/499379062d4e50c48b7a40c27419e732.jpeg)
![images/f37473bf2acc4b92b79a707984b55c4b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f37473bf2acc4b92b79a707984b55c4b.jpeg)

**"Amer Al fk"**

---
---
**Bill Keeter** *December 08, 2016 14:13*

Very cool! I'm about to make some of these for some friends. How are you changing the color of the LED? Colored sheet between the LED and the Acrylic?


---
**Carle Bounds** *December 08, 2016 17:07*

Where can I find more information on making these acrylic lamps? Are there plans out there anywhere to get me started?


---
**HalfNormal** *December 08, 2016 17:31*

**+Carle Bounds** This is from a CNC site but you can learn a lot here [discuss.inventables.com - Inventables Community Forum](https://discuss.inventables.com/search?q=edge-lit%20acrylic)


---
**Amer Al fk** *December 08, 2016 18:36*

[www.model1center.com](http://www.model1center.com) there is plan 


---
**Amer Al fk** *December 08, 2016 18:44*

Bill keeter this os just RGB LED LIGHT on bottom  


---
**Carle Bounds** *December 08, 2016 19:06*

Thanks everyone!


---
**Carle Bounds** *December 08, 2016 19:13*

[Model1center.com](http://Model1center.com) seems to not work for me right now.


---
**Amer Al fk** *December 08, 2016 19:17*

[www.model1center.co.uk](http://www.model1center.co.uk)


---
**Carle Bounds** *December 08, 2016 19:23*

**+Amer Al fk**  Thanks! I see the site has the acrylic 3D models to purchase which is great. What about the wooden base? Are the plans for the wooden base included when you purchase the acrylic file?


---
**Amer Al fk** *December 08, 2016 19:25*

The wooden plan will be updated next Cople of days 


---
**Todd Miller** *December 09, 2016 02:41*

A co worker was just showing me some of these lamps tonight (Death Star, X Wing, Vader, etc..) on the internet.  I thought about buying one and scanning the image  so I could make them myself.  If there is a source for the line drawings, that would bet better !


---
**Amer Al fk** *December 10, 2016 04:29*

[www.model1center.co.uk](http://www.model1center.co.uk)

also you can ask for customer design


---
**bobmo1969** *December 14, 2016 22:41*

iv been making these and making the base with my cnc 

![images/f79fb7369bbc339fc705c4927d6e84f3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f79fb7369bbc339fc705c4927d6e84f3.jpeg)


---
**bobmo1969** *December 14, 2016 22:41*

![images/13fad6745291df800bbf29045676b073.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/13fad6745291df800bbf29045676b073.jpeg)


---
**Amer Al fk** *December 14, 2016 22:49*

![images/c29415e3d1a9cb3af187aef5c8ce81b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c29415e3d1a9cb3af187aef5c8ce81b2.jpeg)


---
**Amer Al fk** *December 14, 2016 22:49*

![images/be2fdd80ead77bab4c718a165560b3d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/be2fdd80ead77bab4c718a165560b3d8.jpeg)


---
**Bob Damato** *December 27, 2016 16:31*

These are great. Id love to know how to make those drawings, what a great look






---
**Amer Al fk** *December 27, 2016 16:42*

[Www.model1center.co.uk](http://Www.model1center.co.uk)


---
**Bob Damato** *December 27, 2016 19:23*

**+Amer Al fk** I went to the site, but you have to admit it isnt great. I see the have predone ones, but what Im interested in is making my own, and not have someone make it for me (which the site doesnt even indicate you can do that from what I saw)


---
*Imported from [Google+](https://plus.google.com/101804487986287829578/posts/XPV3ZUaNDrR) &mdash; content and formatting may not be reliable*
