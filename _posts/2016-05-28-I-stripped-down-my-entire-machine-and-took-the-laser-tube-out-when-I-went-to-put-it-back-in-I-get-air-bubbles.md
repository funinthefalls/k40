---
layout: post
title: "I stripped down my entire machine and took the laser tube out, when I went to put it back in I get air bubbles"
date: May 28, 2016 01:07
category: "Original software and hardware issues"
author: "Eric Rihm"
---
I stripped down my entire machine and took the laser tube out, when I went to put it back in I get air bubbles. I rotated it so I get almost zero air bubbles but one tiny one but one of the hoses still leaks. I checked and there isn't any holes it justs coming where the hose connects. I tried sealing it with liquid tape and it worked pretty well but it sprang a tiny leak, gonna patch and see if it holds. Any suggestions, I think I have my water inlet and outlets oriented wrong and need to turn the tube somehow but am not sure.





**"Eric Rihm"**

---


---
*Imported from [Google+](https://plus.google.com/+EricRihm/posts/Uucn4LV6USq) &mdash; content and formatting may not be reliable*
