---
layout: post
title: "Much longer process this time. Drawing took a lot longer, and machine time also took longer because of the design"
date: April 16, 2017 02:52
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
Much longer process this time. Drawing took a lot longer, and machine time also took longer because of the design. 4+ hours crunched into 26 minutes.



<b>Originally shared by KA4 Designs</b>



Making a decorative lamp, from design to assembly.





**"Ashley M. Kirchner [Norym]"**

---
---
**Ariel Yahni (UniKpty)** *April 16, 2017 03:15*

Very nice, lots of LaserWeb4 operations there!!!


---
**Ashley M. Kirchner [Norym]** *April 16, 2017 03:21*

Yeah, I do that on purpose, to control how it cuts. Usually it doesn't matter much, but occasionally if I am cutting something that's too warped, doing a premature cutout will cause problems, so I leave that till the very end.


---
**Ned Hill** *April 16, 2017 13:59*

Wow very nice work.  I really wish I know how to use AI that well.




---
**Ashley M. Kirchner [Norym]** *April 16, 2017 14:19*

**+Ned Hill**, practice, practice, practice. That's the best advice I can give.


---
**Ned Hill** *April 16, 2017 14:21*

**+Ashley M. Kirchner** Lol, yeah I know.  You only learn by doing. :)


---
**Ashley M. Kirchner [Norym]** *April 16, 2017 14:37*

And by making mistakes. :) If you aren't making mistakes, you aren't trying hard enough, or you've gotten complacent and aren't pushing yourself. At least, that's how I approach things.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/Ct8z5ThYCAh) &mdash; content and formatting may not be reliable*
