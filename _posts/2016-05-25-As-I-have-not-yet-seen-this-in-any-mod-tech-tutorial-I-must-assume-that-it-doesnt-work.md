---
layout: post
title: "As I have not yet seen this in any mod/tech tutorial I must assume that it doesn't work"
date: May 25, 2016 15:36
category: "Discussion"
author: "Purple Orange"
---
As I have not yet seen this in any mod/tech tutorial I must assume that it doesn't work.  However please endorse me and explain why this should not work as I don't have the knowledge to understand the principles.  



What about mounting a red dot pointer close to the first or second mirror and align the light parallell to the laser to get red spot through the nozzle?  



Why would this not work? 





**"Purple Orange"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 15:53*

I've considered this & to be honest am still thinking of a solution to do just this.



The problem with it is that in order for the red-dot-pointer to not get hit by the laser beam it would have to be aligned/angled slightly away from the laser beam. This in turn would cause the angle of reflection of the red-dot beam to then be different to the angle of the laser beam. All in all, it would not follow the same path & more than likely not even end up coming out of the lens.



I have been thinking of methods to do pretty much what you have suggested & the only one that I can come up with that might work is something that raises the red-dot-pointer perfectly in line with where the laser beam exits the tube. So you would have to raise/lower it when you want to use red-dot or actual laser tube.


---
**Dennis Fuente** *May 25, 2016 16:40*

try reversing the laser send the beam up through the lens with a target on the tube


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 25, 2016 16:49*

**+Dennis Fuente** That's an interesting idea, but I think Purple's point is to have a dot show on the workpiece where it is going to hit.



I'd wonder too whether sending the beam through the lens in the reverse orientation would cause the beam to disperse/defocus & when it reaches the tube to show position it would maybe be a large circle rather than a dot?



Another option that may work is some kind of beam refraction through a prism. It may be possible (although I'm not sure of the physics behind it) to have main laser beam enter like so:

<s>----</s>> PRISM <s>---</s>>

and exit in a straight line.



Then you could have the laser pointer beam entering perpendicular like so:

           |

           |

       PRISM-<s>----</s>>

and exit at 90 degrees.



If it was possible to do this with the same prism, which I can't recall from physics class years ago, then maybe it can be done very simply. Also, the prism would have to absorb minimal of the laser energy as the beam passes through.


---
**Jim Hatch** *May 25, 2016 17:09*

Are you trying to make a d-i-y of a beam combiner (& lens) so you can run the LED pointer along the same light path of the CO2 laser beam? You can get those from LightObject likely cheaper than making it yourself.


---
**Thor Johnson** *May 25, 2016 17:32*

So, if you want the beams to both be active at the same time, you need a beam-combiner (it's a mirror to visible light, but it lets IR pass through -- [http://www.lightobject.com/Pro-25mm-beam-combiner-mount-with-laser-pointer-P872.aspx](http://www.lightobject.com/Pro-25mm-beam-combiner-mount-with-laser-pointer-P872.aspx) -- including housing and red laser).



OTOH, if you're cheap and don't mind the red dot going away when you lower the cover, you can print my thing on Thingiverse: [http://www.thingiverse.com/thing:1002341](http://www.thingiverse.com/thing:1002341)



When you shut the door, it lifts out of the way of the beam.  I put a swatch of aluminum on the backside because I periodically forget and press the "test fire" button when the red laser was down (smokes the wires).


---
**Purple Orange** *May 25, 2016 17:42*

**+Yuusuf Sallahuddin** Yeps,  that's what I was wondering about. 


---
**Purple Orange** *May 25, 2016 17:43*

**+Jim Hatch** Cheers for the feedback 


---
**Purple Orange** *May 25, 2016 17:46*

**+Thor Johnson** sweet adaption of the key koncept of my question.  Does it work well?  how do you align it between lid openings? Easy adjustable that stays put? 


---
**Carl Fisher** *May 26, 2016 01:46*

Ok, that print from Thor looks like a great idea. Anyone have a 3d printer that would be willing to make me one :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 05:48*

**+Carl Fisher** Either that or look for a local hub on 3D Hubs or Shapeways. Can get small prints done reasonably cheap & pickup.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 05:50*

**+Thor Johnson** I had previously seen that design on thingiverse & couldn't figure out what it does or how it works. Now I realise given the context of this conversation. It is actually perfect for the job.



Never new about the mirror that lets visible light reflect & IR pass through it.


---
**Carl Fisher** *May 26, 2016 12:08*

**+Yuusuf Sallahuddin** Well what do you know, I found a 3d hub right in my home town. I never knew about that service. Thanks for the heads up :)




---
**Carl Fisher** *May 26, 2016 12:16*

**+Thor Johnson** What is the case version of the STL on Thingiverse? Looking at it I can't see what those cases provide.




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 13:49*

**+Carl Fisher** You're welcome. Someone else here put me onto 3D Hubs, so it's only fair to pass it along. Also, if you look on my profile, there is a refer code for 3D Hubs that will give you US$10 off if your order is US$25 or more.


---
**Carl Fisher** *May 26, 2016 13:52*

Good to know. I did place an order with a local hub bit it only comes out to about $11 for that part.


---
**Thor Johnson** *May 26, 2016 15:41*

**+Carl Fisher** Case version?

Mine was just a little wobbly when I snapped it into place, so I put a dab of gorilla glue on each end, and it's rock-solid now (you have to bend the sheetmetal and slide it back and forth for it to align perfectly, but it is possible).  Be aware that nobody puts effort into aligning the laser pointers, so if you have to replace it (or rotate it), you have to realign it (as I found out when I smoked the wires).  I'm just powering the pointer from the 5V supply on the laser so I'm glad I didn't accidentally short it out :0, so do put a bit of aluminum on the backside (you can see mine held by the electrical tape that holds the wires), so you don't accidentally smoke and have to replace it.



I have a spring from a ballpoint pen in mine, and it's rock-solid as far as coming back down and making a dot anywhere.  Because the beam is so small, you can't see the focus (so I made an L that I balance on the head and use that).  But it's a real treat for aligning jigs -- I do the initial setup, close the lid and burn 2 dots at a known X,Y of 100+mm apart using the test fire.  Now to reposition the jig, move to the first position, and twiddle the jig until the red beam "falls in the hole", move to the 2nd, and repeat until it falls in both holes, and now the jig is aligned!


---
**Thor Johnson** *May 26, 2016 15:43*

Yep, and if you don't have a local hub, I'd be glad to print something for anyone and drop it in the post (at least in the US... international shipping would make the price insane)- [http://www.3dhubs.com/atlanta/hubs/thor](http://www.3dhubs.com/atlanta/hubs/thor)


---
**Carl Fisher** *May 26, 2016 16:03*

**+Thor Johnson** meaning when I pulled the files from Thingiverse that you provided above there was a LaserTarget.STL and a LaserTarget-NoCase.STL. The first version has some weird case/blocks around both ends of the arm while the other does not.


---
**Thor Johnson** *May 26, 2016 16:53*

Oh ok.  Now I remember.  When I was modeling it, I put blocks up for "Laser-Target Plate " -- the thin rectangular plae, "Keep Out Area" (where the mirrors are - the big block), and where the lid hit (block that is intersecting with the D shape at the end).  I originally did this in Solidworks, but I figured I'd put the one with the blocks in it up on Thingiverse so you can see how it worked... but I suppose that is confusing since there wasn't any context (par for me).


---
**Carl Fisher** *May 26, 2016 16:55*

Rats, this thing just went up to a $30 quote from the local hub for "design fee to export solidworks files to STL". Said the all in one STL would create a part that doesn't move. 



Not being familiar with 3d hub before today, am I now committed to paying him for doing the modeling work?


---
**Thor Johnson** *May 26, 2016 17:06*

**+Carl Fisher** I wouldn't think so; I'd ask if I could change the model and then send him this the "Separated" version that I just put up (then again, to make this all I did was tell Repetier-Host to "split the model" and then rotate the arm until it was flat.  Sheesh.).


---
**Carl Fisher** *May 26, 2016 18:02*

**+Thor Johnson** I gave the other guy a couple of bucks for his time but didn't use him to print the part. I'll see if I can reach out to you through your hub. Thanks


---
**Dennis Fuente** *May 26, 2016 18:09*

So when you hit the fire the laser test button that indicates where the laser will first fire but it will not tell you where the laser will start to cut or engrave this to me is a big one so what i do is do a simulation with out the laser on test firing the laser at different points then i postion my material accordinly, as to the mirror alignment it dose work as i have used this methiod to aligning my mirrors in this way you can tell exactly where the beam hit's leaving the head to the first mirror thats how i found out that my head mirror was way out


---
**Thor Johnson** *May 26, 2016 19:14*

**+Dennis Fuente** Yep.  So what I do is I turn the laser off, and tell the laser to run the pattern - you have to look at the indicator on the laser to see if it wanted the laser to be on (usually at a lower speed), and I position the part where I want it.  I wait, then tell the laser to go to 2 dots and fire, and then I can position the fixture using the red dots.  I'll make an instructable on that...


---
**Thor Johnson** *June 08, 2016 15:51*

**+Carl Fisher** Did it work on your laser?


---
**Carl Fisher** *June 08, 2016 16:40*

**+Thor Johnson** It looks like it will but I haven't had any time to test it yet. I still need to wire it up. What is the preferred method to clamp it in place? It's on there now but slides around freely. Wondering if the material will hold up to a small grub screw tapped into it to fix the position.




---
**Thor Johnson** *June 08, 2016 16:49*

It can handle that.  I secured mine with a dot of gorilla glue (figuring I can break it with a screwdriver if necessary).  You'll slide it / bend the sheetmetal to get it aligned (similar to aligning the mirrors (min/max X&Y), but since the mirrors are aligned to the laser, you end up bending the sheetmetal lip [not much] to get this to align instead of messing with the mirrors -- luckily you can see the dot, and the red laser doesn't have to be as aligned as the CO2 laser since you're "looking for a dot" instead of "trying to get max power").



I think I'll make another version that's a little looser with a springiness to it and add 2 grub screws so you don't have to bend the sheetmetal as much (though I had to tweak mine anyway because it wasn't vertically aligned).

Something wrong with vice-grips being your friend...


---
**Carl Fisher** *June 08, 2016 17:05*

LOL, fair enough. I also need to put the spring in there to push it down when I lift the lid. I think I'll be glad for that little addition.






---
**Carl Fisher** *June 10, 2016 22:58*

So I ended up not using the arm. There was too much play and I couldn't get it lined up and keep it lined up so that it was repeatable. It was a good 1/2" lower than the mirror line which tells me that the arm is likely too long for my machine.


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/NUKSeTjkLjS) &mdash; content and formatting may not be reliable*
