---
layout: post
title: "Here is a question which may sound silly"
date: May 11, 2016 08:08
category: "Modification"
author: "Sunny Koh"
---
Here is a question which may sound silly. Has anyone tried to fit a "40w" China Laser Tube into one of these machines? [http://www.instructables.com/id/How-to-Setup-and-Use-the-NEJE-DK-5-Pro-5-Laser-Eng/](http://www.instructables.com/id/How-to-Setup-and-Use-the-NEJE-DK-5-Pro-5-Laser-Eng/) Seriously considering as a "portable" setup for flea market.





**"Sunny Koh"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 09:35*

Honestly, I don't know how big that machine you've posted is, but it looks like you're never going to fit a 40w tube into it. The k40 tube is 700mm long. If you could mount the tube under a table & reflect the beam up through the table, then reflect again to the x/y axis, could possibly utilise this sort of machine.



edit: portable setup for flea market is a cool idea, but I'd suggest for safety/liability sake you look into the laser blocking curtains or build some large metal case that will block it all. Especially if you are considering using a 40w tube.


---
**HP Persson** *May 11, 2016 10:03*

No, just no! ;)

The axis are on the wrong alignment to fit a big laser tube into it. You need to have a portable one with flat axis, like the K40 is.



You should rather check out the Prusa i3 3D printer design, where you can mount the tube and lens standing up, and just the bed moving around.

(dont use a i3, just a example of the construction)


---
**Sunny Koh** *May 11, 2016 10:12*

Was thinking of doing a metal enclosure or acrylic lined with foil or wood. I do know the size of the tube as I have to carry 2 back from China earlier in the year.  Nice thing of this design seems that the X is moving so you can place the flying mirror on it. (3D Print the part) 



Need only a small work space of 6 inch by 1 Inch (Actually The Laserable Area is only 1/4 of that).



The idea is to build a case in which the tube is off to the side of the frame to house the tube and Laser PSU which aligns to the flying mirror instead of the Mini 1 Watt Laser.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 10:19*

Is there a reason for putting the 40w? Wouldn't that be a bit overkill for such a small area of cutting/engraving? I wonder if a smaller diode laser (but > 1 watt) would suffice for your needs? I just feel I wouldn't want to be carting around a glass tube regularly in my car & pump/water/exhaust kit/etc. Just seems a little hectic.


---
**Jean-Baptiste Passant** *May 11, 2016 10:21*

I got one, will answer all your question one I have more time ;)


---
**Sunny Koh** *May 11, 2016 10:24*

Engraving on pens which are either Acrylic or Coated Metal. Trying to make it small as I will be leaving it at a friend's place for most of the year in the USA. Back here in Singapore I have my full size K40 for proper work.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 10:28*

**+Sunny Koh** Well if you are going to be using a 40w tube, that kind of defeats the "small" size right? Unless you are planning on taking the tube with you when you go to USA & use it?


---
**Sunny Koh** *May 11, 2016 10:37*

Small is a relative term. And yes, I was planing to take the tube to the US. (Check in the machine while hand carry the tube in a "small" housing.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 10:52*

**+Sunny Koh** Because the Y-axis is fixed on this machine you show, you could just set the tube to the left (or right) directly in line with the X-axis, requiring just 1 mirror to reflect it down onto the work piece. Can't see why it wouldn't work since the Y-movement is controlled by the board at the bottom.


---
**Jim Hatch** *May 11, 2016 12:06*

**+Yuusuf Sallahuddin**​ I'd go with a 5W diode laser. It will do exactly what you're trying (engraving pens and such) without the complications & size of a water cooled CO2 tube. It will even cut some thin materials (3mm).


---
**Jean-Baptiste Passant** *May 11, 2016 12:29*

Okay, I read the question and comments really fast, but here is my thought :

This machine is small, you can barely put a smartphone in it (phablet are a no no).

Nope, adding a 40W tube is not a great idea.

You say "Small is a relative term", not with this machine, it's small and it's probably smaller than you think : it use 2 CD ROM drive to move, it's slow and can do 5.5cm ? (a CD is 12cm, it turns so only 6cm and if you remove the center it should be 5-5.5cm).

There is no clamping system, and making one for a small surface will not be easy (elastics are not a good clamping system).

I bought one for christmas, used it twice, it was not that expensive, but what can I do with a machine who take 1+hour to make an engraving on a 6x6cm surface ?

I tried engraving the back of my phone and it melted it, you can see the design but it's not burned. I tried it on another phone and it worked better.

It can barely engrave plastic and some cardboard, don't expect it to cut through anything, and even though you find something it may cut, what are the probabilities for it to be a usable material ?

No, this won't engrave metal or coated metal.

It can engrave wood, at least burn the surface, it's pretty cool on some items but seriously, who use a wood pen or engraved chopstick ?

Burning wood and using it for food is not recommended too (safety).

So yes this machine is shitty, and no, adding a 40w laser is not a good idea.



Simple rule is : if you want to make money out of it, you will have to pay for it.



As you are browsing Instructable, search for DIY Laser machine or anything similar, you will probably find something that : is transportable, not as expensive as a K40, easily repairable (you builded it so repairing is easier), with a bigger surface (size is up to you) and a diode that will fit your need : decide if you need to cut anything or just engrave.



A laser plotter with a ~3-5W laser diode should suit your need more than any sort of Frankenlaser you are talking about.



Imagine the power of a god (40w) in a machine the size of a table (1m), with the speed of a snail and able to show his power on 6x6cm...

This is what you are talking about :P, probably not what you want to end up with.


---
**Sunny Koh** *May 11, 2016 14:35*

Doubling checking the specs, the 2 DVD Drives can only do 38mm of movement, which for my case is well, enough for the concept I am considering. (The actual target area is about 12 mm by 30 mm as most pens only can fit a name on it without using a rotary attachment



The initial plan is to 3D print a mount to attach the flying mirror and lens of the K40 on it for the X axis and mount the 40W Tube to point directing into the flying mirror. The next part will be replace the plastic bed with something metal or wood. And to Enclose the whole setup in some sort of protective cover. 



It seems that the question on my mind and Jim said is a 5w laser will do the job? I though we need near the max range to do laser marking especially with items such as coated metals and Cermark. Off hand I believe I had to use 60% to 70% power to engrave one of the coated metal pens. Hence why I am thinking of the power requirements. Portability (For flying) would be the goal here as I don't want to trouble my friends to carry a K40 from San Francisco to LA for a 4 day event and back again.


---
**Vince Lee** *May 11, 2016 14:42*

I'd maybe try to find a unit with a bed that moves in both axes.  Then you could avoid mirrors altogether and fix the lens at the end of the tube.  That might make moving it easier.


---
**Eric Parker** *May 14, 2016 00:32*

never gonna happen.  Let me show you why.


---
**Eric Parker** *May 14, 2016 00:35*

[https://drive.google.com/open?id=0ByeYY2NDsUuJRDlwVkE4b29kb3M](https://drive.google.com/open?id=0ByeYY2NDsUuJRDlwVkE4b29kb3M)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 01:45*

**+Eric Parker** Lol. Thanks for showing us why. Hahaha.


---
**Sunny Koh** *May 14, 2016 08:16*

I thinking about it, though I have a budget to stick to if I do it. **+Vince Lee**  Idea is good but it would mean finding a way to mount a 700mm tube on top of a 100mm to 200mm system without falling over.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 08:47*

**+Sunny Koh** It would give it a nice small footprint at least (100mm x 200mm) but be nice & tall (let's say 1000mm just for simplicity sake). If the bed is moving, eliminates any issues with the weight of the tube being a problem. Would be an interesting project & I'm looking forward to seeing any progress you make on it.


---
**Sunny Koh** *May 14, 2016 09:52*

**+Yuusuf Sallahuddin** They actually sell a version of the 40W Laser which does exactly that. The link is here [https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-3001843930.26.3vK80E&id=522555892191](https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-3001843930.26.3vK80E&id=522555892191)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 14, 2016 10:07*

**+Sunny Koh**  That's very interesting. A machine like that would have possibly been a slightly better as it's already set up for a moving cutting table. Would be very simple to scale that one up in size in comparison to converting my entire machine to a moving table/fixed head. The price of that machine you show is pretty decent in my opinion. If it's within your budget, might be worthwhile going with that. Could always change controller to smoothie/etc in future.


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/dbvrMhMQdoT) &mdash; content and formatting may not be reliable*
