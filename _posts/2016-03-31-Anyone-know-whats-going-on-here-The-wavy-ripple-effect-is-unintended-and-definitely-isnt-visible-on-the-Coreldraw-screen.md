---
layout: post
title: "Anyone know what's going on here? The wavy ripple effect is unintended and definitely isn't visible on the Coreldraw screen!"
date: March 31, 2016 23:00
category: "Discussion"
author: "Bob Steinbeiser"
---
Anyone know what's going on here? The wavy ripple effect is unintended and definitely isn't visible on the Coreldraw screen!  This is an engraving of a vector drawing at 10ma and 250 mm/sec. on maple wood. The letters are about 20mm tall, and the wood was clamped down solid. The wood is about 1 mm beyond the focal length.



I've successfully done engravings before, this is the first one after installing the air assist, but I can't imagine that is the problem.

![images/a82970aa99c3aa2a9f06c14b0bc5fe33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a82970aa99c3aa2a9f06c14b0bc5fe33.jpeg)



**"Bob Steinbeiser"**

---
---
**Coherent** *March 31, 2016 23:17*

Maybe someone will chime in with specifics because I'm not home to look, but there is a setting on the driver/plugin that you can change for the engraving that I think effects what your issue it. Especially if there are small "peaks" and "valleys" between your engraved lines. Can't remember what it's named but remember seeing it and playing with it. If I remember correctly it adjusts the distance between "scan"  lines as it engraves. So in your case reduce the distance. At that point it looks like you could reduce the power a bit.


---
**ThantiK** *April 01, 2016 00:48*

It looks like the Y axis motor on the bottom may be binding.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 01, 2016 01:05*

I think what **+Coherent** is mentioning is the Pixel Steps option?


---
**Phillip Conroy** *April 01, 2016 01:09*

check focal lens is clean,do a single etch line ,if it is more than 1mm thick Focal lens is your problem


---
**Phillip Conroy** *April 01, 2016 01:10*

see my other posts 


---
**Phillip Conroy** *April 01, 2016 05:05*

your laser beam is hitting the side of the air assist,remove air assist and conform tjis,when i installed mt air assist it completly blocked the beam until, i tilted the laser head to align the beam to the center of the air assist hole


---
**Coherent** *April 02, 2016 14:59*

Did you determine the cause and fix?


---
**Bob Steinbeiser** *April 04, 2016 14:42*

Looks like **+Phillip Conroy** was exactly correct.  I took off the air assist nozzle and the problem went away.  I'm not sure I understand how this works, you'd think that the laser power to the wood would be diminished, not distorted!  Anyone have a process for centering the beam through the nozzle?


---
**Phillip Conroy** *April 04, 2016 21:55*

You could try sticking a peace of stickit note to the bottom of the air assist head and test firing laser at tbe lowest setting it will lass at[mine will fire tube at 2ma] and using flat washers under the flat plate on one of the screws- beam will move to side u have put the washers on...........ps turn off air assist-clean mirror and lens when finished.


---
*Imported from [Google+](https://plus.google.com/109944262024094898042/posts/7VACsV9Rn6n) &mdash; content and formatting may not be reliable*
