---
layout: post
title: "Labor day weekend project: new laser exhaust"
date: September 04, 2016 20:17
category: "Modification"
author: "Vince Lee"
---
Labor day weekend project: new laser exhaust.  I plan to sheetrock the vertical duct into the wall and faux finish PVC fence posts to act both as a horizontal duct and false ceiling beams.



![images/309d5d33960ece0980d1af97e269226a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/309d5d33960ece0980d1af97e269226a.jpeg)
![images/6759a5280d52c0bbae66172ac396ac23.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6759a5280d52c0bbae66172ac396ac23.jpeg)
![images/8a6f4ca08ab8c6e0fc02bedb6fe64f1f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8a6f4ca08ab8c6e0fc02bedb6fe64f1f.jpeg)

**"Vince Lee"**

---
---
**Tom Nardi** *September 04, 2016 20:29*

That's absolutely brilliant.



It's a fairly convoluted vent setup (I'm assuming there's a good reason you have to take this particular path to get outside), but definitely the most stylish I've seen.  


---
**Vince Lee** *September 04, 2016 22:46*

**+Tom Nardi** yeah my current setup has a temporary hose running across a walkway to the outside wall, so I want to run it across the ceiling instead so I can leave it hooked up permanently.


---
**Vince Lee** *September 05, 2016 14:47*

Little further along.  Now just need to add other dummy beams and seal wall and vent.

![images/002b801015380d10f907ec16c3a0c484.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/002b801015380d10f907ec16c3a0c484.jpeg)


---
*Imported from [Google+](https://plus.google.com/107471533530265402525/posts/2EjirRG1Vty) &mdash; content and formatting may not be reliable*
