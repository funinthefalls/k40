---
layout: post
title: "Let me first off say how fantastic this community is"
date: February 08, 2016 01:08
category: "Software"
author: "Silverado Studio"
---
Let me first off say how fantastic this community is.  I joined several days ago and have been diligently going through previous posts for information and "tried/true" methods everyone has used for these capable little machines.  I hope to soon be able to contribute to this wealth of information and projects seen here.  Now to the fun stuff!  I have a problem I occurred in Laser Draw.  I noticed that William Klinger had a similar problem with Corel Laser.  If I try and cut or engrave a specific sized object, the final engraved/cut piece is reduce in size from what the dimensions are supposed to be.  Roughly 20-25%.  Has anyone had this problem in Laser Draw?  Any fixes/ ideas to resolve this problem.  Thank you in advance!





**"Silverado Studio"**

---
---
**Silverado Studio** *February 08, 2016 01:31*

I did check that yesterday, everything is copacetic.


---
**I Laser** *February 08, 2016 03:08*

What board do you have selected?


---
**Silverado Studio** *February 08, 2016 05:41*

**+I Laser** M2


---
**Silverado Studio** *February 08, 2016 05:43*

**+Carl Duncan** Not silly at all.  Sometimes the easiest fixes are the most obvioussolutions.  Can I change from mm to in?  I never saw an option anywhere to do so...


---
**William Klinger** *February 08, 2016 14:55*

I believe when I changed "drawing" to "all layers" everything worked correctly. I found too that the serial number of the board was entered in the CorelLaser properties but was not in the LaserDRW till I entered it there too. WK


---
**Silverado Studio** *February 08, 2016 20:39*

**+William Klinger** Thank you for your input.  Where is the drawing/ all layers found at in LaserDRW?  I would try to use Corel Laser but unfortunately I can't seem to get it running on Windows 7.  Are they compatible?  Any suggestions would be appreciated.


---
**William Klinger** *February 08, 2016 23:33*

When you go to the engrave or cut window, about 1/2 way down on the right side is a box that can have All Layers or Drawing in it.  I tried three computers before finding one that would run the Corel Laser correctly. All three loaded the Corel Draw 12 but one would not open CorelLaser or LaserDRW, the second one seemed to load up OK but gave distorted actions. All three are Windows 7 but each one has a different version of the OS. The Home Premium seems to be working on an older Lenovo laptop. Good luck.


---
**Silverado Studio** *February 09, 2016 00:19*

**+William Klinger** Thanks for the help.  I tried to switch from "dialogue to all drawings" in LaserDRW with no such luck.  Guess I will just have to keep playing around with the settings to see if anything changes. 


---
**William Klinger** *February 09, 2016 01:03*

be sure you have your board serial number entered in both Corel and LaserDRW. 


---
**Bob Steinbeiser** *February 15, 2016 17:19*

Any solution here yet?  I have the same problem, Corel image is always about half scale but LaserDRW works just fine!


---
**Tony Sobczak** *February 19, 2016 08:25*

**+Carl Duncan** how do you know what's the correct board?  I just got mine yesterday. How can I tell what it should be set to? TIA. 


---
**Tony Sobczak** *February 19, 2016 08:27*

**+William Klinger** where do you set that up? 


---
**I Laser** *February 19, 2016 09:09*

Look in the electronics compartment, there will be a green pcb with the model written on it. eg mine is 6C6879-LASER-M2. I think most people are getting M2 boards.



This needs to be set in corel, just click the cut or engrave button whilst in corel and on the following dialog choose properties (top left), mainboard = model, just select your board from the drop down menu.


---
*Imported from [Google+](https://plus.google.com/112653136109716584038/posts/N6dNj9SYSLZ) &mdash; content and formatting may not be reliable*
