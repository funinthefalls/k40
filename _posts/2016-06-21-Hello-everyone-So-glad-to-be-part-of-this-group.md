---
layout: post
title: "Hello everyone.. So glad to be part of this group.."
date: June 21, 2016 19:10
category: "Original software and hardware issues"
author: "Chris Boggs"
---
Hello everyone..  So glad to be part of this group..  I have a couple of (hopefully) small issues..  Just got my new cutter today.  

1st Laser draw app seems to work and load fine,  corellaser tells me coreldraw is not installed and won't load. 

2nd using laser draw I was at to engrave a sample,  then the second attempt it just engraver a straight line..  Not sure if I've checked a box somewhere.

Any assistance would be greatly appreciated.. Also please let me know if you need more info from me

![images/3ec14fa4e0d9f020eda3b0255cd2551d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3ec14fa4e0d9f020eda3b0255cd2551d.jpeg)



**"Chris Boggs"**

---
---
**Jim Hatch** *June 21, 2016 19:25*

Your first issue is easy - CorelLaser is an add-on driver for Corel that gets the drawing from Corel to the laser. Think of it like a print driver. It's essentially the laser control component of LaserDRW with Corel providing the drawing capabilities that are in LaserDRW (but aren't as robust as Corel). That means you need to install Corel first. I think there's a Corel 12 install on the CD. If not, Yusuff here has it stashed out on Dropbox somewhere.



Your second issues needs more info. What's your machine - Nano or Moshi (I think it's Nano based but don't want to make assumptions), what are your settings in the machine config for LaserDrw? Do you have it set right for model, size, etc. Easiest way is for you to take a screenshot of the machine settings and posting it so we can take a look.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 21, 2016 19:33*

[https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0](https://www.dropbox.com/sh/lcpw8qlad8tsw5p/AAB1EFmRZTgvn9_huu6UsrK_a?dl=0)



Dropbox link to CorelDraw x5 & CorelLaser & a brief install instructions by me.



I can't comment on LaserDRW as I never bothered using it as it looked to be horrible software in comparison to the Corel/CorelLaser combo.


---
**Jim Hatch** *June 21, 2016 19:41*

**+Yuusuf Sallahuddin**​ yep, it's pretty awful. Only good for testing the machine without adding Corel & the driver to the list of things that might be wrong.


---
**Chris Boggs** *June 22, 2016 19:19*

Thanks so much for your fast replies..  Im sorry it tool so long for me to get back with you.. 1st  Yes it is a Nano..  The core draw is now working.. Thanks Yuusuf.. However I am still having the single line engraving thing..  Any idea what my hardware ID should be..  Machine properties gives me several choices..  A, B, M, B1, M1, M2, B2.. Thanks again for all your assistance 


---
**Chris Boggs** *June 22, 2016 19:42*

Here is a link to some photos of my current config.. If that helps.. 

[https://www.dropbox.com/sh/i6smb67f4tl5kt6/AAAHSR7kYSKiKnnrOQz0W8Rha?dl=0](https://www.dropbox.com/sh/i6smb67f4tl5kt6/AAAHSR7kYSKiKnnrOQz0W8Rha?dl=0)


---
**Jim Hatch** *June 22, 2016 19:57*

**+Chris Boggs**​ Pick the machine type in the dialog that has M2 in it. Should only be one entry with that in the drop down choices.


---
**Chris Boggs** *June 22, 2016 20:22*

Tried that..  No luck..  Thanks for your input. I have just added a video to the Dropbox link above.. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 22, 2016 21:18*

Just had a look at your video **+Chris Boggs** & your device ID is set incorrectly. Make sure you use the Device ID that is marked on the right hand side of your M2Nano board. I would say with almost 100% certainty that is what is causing your issue.


---
**Jim Hatch** *June 22, 2016 21:25*

**+Chris Boggs** couple of things. One note - you said "there's no way to cancel it" in the video you shot & you  have to power off. Actually, the laser controls are now in the system tray (lower right corner where things like the clock, battery, speaker status, active printer, etc. show up) - at least in Windows.



Now for the engraving problem, you've got some settings you need to fix. You've got the Mainboard right in the video you shot (-M2) but the DeviceID is wrong. If you look at the Nano board you can see a sticker on the side that has this on it: 093C4243 13BAD65F. That's your device ID. In the video you still have BFF6DB9A567P90FB. That's not your device.



Also, I noticed you had your page size as 200x200 (was 400x400 in your still shots). That should be okay but it's not the full bed size. I'm not at mine but I think I'm at 320x280 if I recall correctly. Also, you  have Frame as "Rectangle" which means it will try to draw a box around everything. You also have Refer X/Y set for non-0 and do not home checked so it's going to start a couple inches in from the upper left corner.



Try fixing the Device ID.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 22, 2016 22:10*

Full bed size by standard is 300x200mm. Like **+Jim Hatch**, mine has been modified somewhat & I get 320x220mm without too much issue.


---
**Chris Boggs** *June 22, 2016 22:15*

Issue resolved..  I'm kind of ashamed to say what it was...  I had the "sunken"  image checked incorrectly..  It was engraving the entire background.. Not just the image..  That is what was causing it to engrave that way..  I just saw it doing it and would turn the machine off before I could tell it was making progress down the image..  "lesson Learned"  Thanks again everyone for you assistance 


---
**Jim Hatch** *June 22, 2016 22:17*

Sunken wasn't checked in the video you posted.


---
**Chris Boggs** *June 22, 2016 23:13*

I know..  I checked it and all works now..  I thought it worked the opposite way..  Oh well..  As long as I know how it works iron this machine.. 


---
*Imported from [Google+](https://plus.google.com/101316203748949165087/posts/CxzZSgiB9qA) &mdash; content and formatting may not be reliable*
