---
layout: post
title: "Hi everyone im in the uk just purchased a k40 laser it worked fine for a day then blew a small fuse on the power supply when I turned it on, the seller is saying it's my fault as here in the uk we run 230v and the machine"
date: September 13, 2016 22:50
category: "Discussion"
author: "Craig Goddard"
---
Hi everyone im in the uk just purchased a k40 laser it worked fine for a day then blew a small fuse on the power supply when I turned it on, the seller is saying it's my fault as here in the uk we run 230v and the machine runs at 220v and wants me to purchase another power supply or return it to China for a full refund yet it's gonna cost 200+ to send it can anyone give me any advice?





**"Craig Goddard"**

---
---
**Anthony Bolgar** *September 13, 2016 23:15*

If the seller shipped it with a UK plug, or plug adapter, that is proof enough that he fully expected it run and work on UK 230V power, as well as the fact that the tolerance of the PSU is probably +/- 10 % allowing for a =/- on the voltage of 22V. I would tell him to rethink his position, and send you a new PSU. While you wait, it is a simple matter to replace the fuse, I would replace it and see if it works or not, it may just have been a faulty fuse.


---
**Jim Hatch** *September 13, 2016 23:29*

+1 That's a B.S. story from the seller. The 230V is a "nominal" voltage (like 120V in the US). Stuff is supposed to run from 220-240V (as in the US they can run from 110-130V) due to line variations, spikes, sags, etc. If they can't they're not allowed to be attached to the mains and thus aren't legal for sale. Tell him you'll be reporting them for importing illegal equipment into the UK if they don't rethink their position.


---
**Anthony Bolgar** *September 13, 2016 23:31*

I agree Jim, that is what I meant about the =/-10% tolerance. BTW, who was the seller?


---
**Jim Hatch** *September 13, 2016 23:34*

Yep. The power supply probably even has the range printed on it (well it would from a legit dealer anyway) :-)



Most power supplies are universal now too - just the cord is different. Beats the old days when I used to have to bring different power supplies when I visited the UK/Europe.


---
**Anthony Bolgar** *September 13, 2016 23:53*

**+Craig Goddard** Did it come with a UK style plug or plug adapter?


---
**Scott Marshall** *September 14, 2016 01:18*

The power supplies seem to all be the same. I have 5 in stock and they all say"230" right on the voltage select switch.



Any seller that would try to push that story on you should be ashamed.



Usually what blows the fuse is the full wave bridge (a 4 diode rectifier package) and it's located directly behind (to the right from a top view) of the mentioned voltage switch.





If you're electrically inclined, check the bridge with an ohmeter and you'll probably find a shorted section (each section should conduct only 1 direction). 



Many of these supplies just need a new rectifier and fuse and are good to go. A clue that's the problem is a "Hard Blow" on the fuse, producing a black coating in the glass shell.



Black fuse > Bad rectifier 9 of 10 times.



Scott



PS then make the crooked dealer cough up a new PSU!!


---
**Damian Trejtowicz** *September 14, 2016 06:36*

I had the same situation,my psu just blow off one day,i was trying to get new psu from seller but never get,the seller was industrysky.

They all say ,laser have 3 years warrenty but its bullsh**t,because they know ,its to expensive to send back item to china




---
**Craig Goddard** *September 14, 2016 06:43*

Hi guys thanks for all ur replies everyone has said that this is a b.s. Story, yes it has a uk plug on it, it also has 250v 10amp fuses in it the first time it blew it blew the glass out so I've now soldered a new fuse in and it's blown both the power input on the back of the machine and the power supply both are black glass I am all new to electrics as I was a gearbox engineer for 15 yrs but recently lost my job and found some1 who needs parts etching so I bought 1 of these with my last £300 and still getting the same from the seller and Paypal want me to send it to China it did come from uk as it only took 3 days to arrive also the machine does say 220-240v on the top of the machine I have taken pics and sent seller and still the same response


---
**Craig Goddard** *September 14, 2016 06:44*

How much are you're power supplies Scott and are you in uk?


---
**Damian Trejtowicz** *September 14, 2016 07:02*

I paid £75 for new PSU.its always risk when You buy things from china,they  register accounts in UK but they send from china and when you want refund its to expensive to send back

You can try deal with ebay or paypal


---
**Craig Goddard** *September 14, 2016 07:12*

Do you have any pics of the rectifier that shows what to test thanks


---
**Scott Marshall** *September 14, 2016 07:14*

Mine are about $100shipped in the US, but the Shipping to the UK is brutal. I just Shipped an ACR module to a gentleman in Broughshane and it cost about $65usd.



Hate to say it, but you'd be better to buy on that side of the big pond...


---
**Jon Bruno** *September 14, 2016 13:32*

Typically the component that blows the fuses out is the bridge rectifier in the LPSU. You can replace the rectifier and fuse for a fraction of the cost of a new LPSU. I would recommend that you consult a local electronics repair person and price a repair instead of obtaining a headache trying to get support from china.


---
**Craig Goddard** *September 14, 2016 13:43*

Thanks for all you're guys help I've taken the power supply unit to a electronics repair guy I have told them what you guys have suggested and there going to take a look hopefully I will know something by the end of the day I will keep you updated when I know more thanks again


---
**Jim Hatch** *September 14, 2016 13:46*

You should still pursue the seller. Tell them it's going to generate a negative review if they don't take care of it. They will likely pay (via refund) for the repair.


---
**Jon Bruno** *September 14, 2016 14:21*

**+Scott Marshall** +1 to you sir. Hah.. I didn't see that you already suggested the rectifier..


---
**Craig Goddard** *September 14, 2016 14:55*

Well my tech guy has got back to me it's gonna cost me £20 to fix it and it will be ready Friday not sure what has gone wrong with it but they found something I will find out more on Friday when I speak to the lad who checked it over massive thanks


---
**Craig Goddard** *September 19, 2016 12:23*

Hi guys got my part back and it was the rectifier, it's all back together now but just tried to engrave with it and it's just going from right to left not doing the work on the PC any ideas I'm using laserdraw couldn't get the coral draw to work it this worked fine before


---
**Jon Bruno** *September 19, 2016 14:24*

Glad it was an easy fix..

Quick question... Does the laser fire when you press the test fire button?


---
**Craig Goddard** *September 19, 2016 14:42*

It's ok got it working again uninstalled the software and reinstalled it working good again another question tho do you guys know of any way to etch alluminium with these I'm trying dry moly at the mo


---
*Imported from [Google+](https://plus.google.com/105144545744699864291/posts/72RQAqUrwTb) &mdash; content and formatting may not be reliable*
