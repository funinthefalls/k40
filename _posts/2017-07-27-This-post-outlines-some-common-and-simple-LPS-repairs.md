---
layout: post
title: "This post outlines some common and simple LPS repairs"
date: July 27, 2017 19:53
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40LPS



This post outlines some common and simple LPS repairs.



As the post suggests I don't recommend repairing your LPS because:

... Cascading failures can make it more expensive than a new one by the time its done.

....Although this repair is done on a "powered down" supply I worry it will tempt someone to power it up outside the machine. 



However, many of you have asked for simple repair procedures so spare supplies could be created. 

......

Let me know if you see any problems with this post.

.....



<b>Originally shared by Don Kleinschnitz Jr.</b>



<b>Repairing the K40 LPS #1</b>

Background It is quite common for the Laser Power Supply (LPS) in your K40 to fail. Although our knowledge of the LPS design has been dramatically increased it is still unclear why certain parts of the supply fail. I continue to collect failed LPS's dissect...





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/KeaXPN7FNgz) &mdash; content and formatting may not be reliable*
