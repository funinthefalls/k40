---
layout: post
title: "So all I've done with the monster today was spend two hours straightening the adjustable bed;"
date: October 31, 2015 22:52
category: "Discussion"
author: "Peter"
---
So all I've done with the monster today was spend two hours straightening the adjustable bed; one of the four legs was either to short or the bracket holding it on at the top is totally in  the wrong place. possibly a bit of both.

Didn't focus it, didn't do anything else except do a test pulse to see if the tube worked. It did work and it was perfectly focused so I left it alone!

I've had some OK results so far...




{% include youtubePlayer.html id="f_llIa-kn3U" %}
[https://youtu.be/f_llIa-kn3U](https://youtu.be/f_llIa-kn3U)

![images/e1f87845cc5a7ccda944efa56f18d07b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e1f87845cc5a7ccda944efa56f18d07b.jpeg)



**"Peter"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 01, 2015 02:38*

That's a cool design there. Very fine details too. Is that done using Cut or Engrave?


---
**Peter** *November 01, 2015 22:44*

That was engrave. I think i'd still be watching it work if it was a cut :)

Got a few more pieces off today. Some Christmas hanging decorations, some snowflakes cut from 2mm acrylic (which stinks! What do people do about the smell where they work?), and I did a couple of small boxes.

Ran out of wood now so just ordered more but laser ply this time, not cheap junk that I bought to test it out.


---
**Peter** *November 03, 2015 12:32*

I cant work out how the air flows inside this thing. I know its not airtight and that air has to come in to the machine to replace the smoke etc that is extracted. The smoke from the burns seems to swirl around inside towards the back of the machine instead of being sucked straight out. Im thinking a small PC fan at the front to push air in the front through and out the back. or blocking off as many openings/gaps as I can and let the extractor do its job...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *November 03, 2015 12:41*

**+Peter Jones** I've actually noticed a similar thing in my machine where the smoke swirls around a bit instead of just sucking straight out. I agree that it is because there is no real intake. However, I think even without an actual fan pulling air in, as long as there was a vent at the front it would allow the air to pull in (be better with a fan like you suggested though).


---
*Imported from [Google+](https://plus.google.com/+PeterJones79/posts/3i1Tecd2ZEE) &mdash; content and formatting may not be reliable*
