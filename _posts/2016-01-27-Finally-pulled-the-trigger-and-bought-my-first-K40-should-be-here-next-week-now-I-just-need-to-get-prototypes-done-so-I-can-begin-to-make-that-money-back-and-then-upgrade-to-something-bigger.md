---
layout: post
title: "Finally pulled the trigger and bought my first K40 should be here next week now I just need to get prototypes done so I can begin to make that money back and then upgrade to something bigger"
date: January 27, 2016 03:34
category: "Discussion"
author: "3D Laser"
---
Finally pulled the trigger and bought my first K40 should be here next week now I just need to get prototypes done so I can begin to make that money back and then upgrade to something bigger 





**"3D Laser"**

---
---
**Josh Rhodes** *January 27, 2016 04:08*

That's always my thought too.. never pulled the trigger yet. Would like to know how it goes..


---
**Jim Hatch** *January 27, 2016 18:51*

**+Corey Budwine**​interesting point. I've not really considered making money off mine. What are people doing to monetize their laser?


---
**3D Laser** *January 27, 2016 20:07*

I talked to a guy on Facebook who made 1200 dollars in three months on his k40 making ornaments 


---
**David Wakely** *February 05, 2016 22:15*

I have made a decent amount of money making slate door numbers. I've easily paid for my K40 and I've since upgraded to its bigger, bigger brother! I've now got 60w 700 x 500 machine. It's well worth the upgrade and its worlds apart in terms of performance! I'd defiantly recommend. If you need any help with any slate bits just message me, it's really easy to get into and slate costs are really low.


---
**Jim Hatch** *February 05, 2016 22:39*

**+David Wakely** Where are you getting your slate? Building supply place or something? The one you posted in your other note looks great. I'd love to do one for my house. It would look good against the light gray siding.


---
**David Wakely** *February 06, 2016 10:33*

Yeah they are just roof slates. Speak to a building yard sometimes they have some that people bring back that have chipped edges and stuff you can pick them ones up dirt cheap because normally they go in the bin!


---
**Jim Hatch** *February 06, 2016 14:23*

**+David Wakely**​ Thanks. I'll take a look today.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/SU7WfvrpsX3) &mdash; content and formatting may not be reliable*
