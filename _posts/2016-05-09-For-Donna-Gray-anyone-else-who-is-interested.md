---
layout: post
title: "For Donna Gray & anyone else who is interested"
date: May 09, 2016 21:18
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
For **+Donna Gray** & anyone else who is interested.



This is how the 3d printed air assist I've designed attaches. This is my previous revision (which is all electrical taped up because the material was flimsier than I expected & there were design flaws in my original design).



The new version, which you will see as the brass coloured one (in photos) uses the same method to attach to the laser head.



![images/616a4af9dd8ce08dc4187c3bc0761622.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/616a4af9dd8ce08dc4187c3bc0761622.jpeg)
![images/8d7fcd0b068770644d508452b28dbe00.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8d7fcd0b068770644d508452b28dbe00.jpeg)
![images/f2e3a8e59b2d8bc5420b2c51bf71c10f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f2e3a8e59b2d8bc5420b2c51bf71c10f.jpeg)
![images/205099edff7b16544a31a01ee2bf1ac5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/205099edff7b16544a31a01ee2bf1ac5.jpeg)
![images/802a51bc0dd022af53d0f77def1a7285.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/802a51bc0dd022af53d0f77def1a7285.jpeg)
![images/b5879e46a2116bac02baf033466835df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b5879e46a2116bac02baf033466835df.jpeg)
![images/390daeed6bc2e479d5f3528fc6289e44.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/390daeed6bc2e479d5f3528fc6289e44.jpeg)
![images/16e20eca3b5c574657f759c6a684f11e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16e20eca3b5c574657f759c6a684f11e.jpeg)
![images/918488a89886af6bc0b4273715d2b0e4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/918488a89886af6bc0b4273715d2b0e4.jpeg)
![images/4f2d73ed645ae73bda1187e97040e9af.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f2d73ed645ae73bda1187e97040e9af.jpeg)
![images/a5527ed81874c3eac0d5aef03883ae0e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a5527ed81874c3eac0d5aef03883ae0e.jpeg)
![images/73a410d716663f7bc89e1914614f043f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/73a410d716663f7bc89e1914614f043f.jpeg)
![images/5835d77e77b19c38b4bd3605912f9d3a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5835d77e77b19c38b4bd3605912f9d3a.jpeg)
![images/904d011400fa3e5cae9f2ecd17d62483.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/904d011400fa3e5cae9f2ecd17d62483.jpeg)
![images/8f8ffdb4d51e56c3bf2338520e59374b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8f8ffdb4d51e56c3bf2338520e59374b.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:41*

Updated version: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/7fQWizJwCgF) &mdash; content and formatting may not be reliable*
