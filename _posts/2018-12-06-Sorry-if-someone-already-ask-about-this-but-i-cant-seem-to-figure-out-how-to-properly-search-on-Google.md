---
layout: post
title: "Sorry if someone already ask about this but i cant seem to figure out how to properly search on Google +"
date: December 06, 2018 22:47
category: "Hardware and Laser settings"
author: "Tim Shepherd"
---
Sorry if someone already ask about this but i cant seem to figure out how to properly search on Google +. So im running the k40 laser with the cohesion3d board and using lightscribe to run the gcode. I cant figure out how to fix this issue of the machine cutting in a mirrored path. Can anyone help me out?

Thanks


**Video content missing for image https://lh3.googleusercontent.com/-DqlGrNhVAYY/XAmnDRabz4I/AAAAAAAAGSA/lWhNVabhQsYRBc4SPHZLZjE9Y2a5skKGgCJoC/s0/gplus212579819450591265.mp4**
![images/8c4519de1f3a570d294172bcef8ad235.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8c4519de1f3a570d294172bcef8ad235.jpeg)



**"Tim Shepherd"**

---
---
**Joe Alexander** *December 07, 2018 02:43*

are you using the preinstalled smoothieware or grbl? if smoothie you can edit the config file to invert the y pin and make it work right by adding a believe an ! to it. i would also assume that jogging the y axis currently moves backwards, this would fix that. The other way of fixing it is to swap one of the wires on the connector.


---
**Tim Shepherd** *December 07, 2018 02:51*

Im using the pre installed smoothie, but i think I'll try flipping the Y connection first.

Thank you!


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2018 15:10*

People often set the origin wrong. It should be the front left in LightBurn. Yes I know that’s not where the switches are. 


---
**Tim Shepherd** *December 07, 2018 16:59*

yeah i meant to write lightburn not lightscribe. I was able to fix it, first i tried to flip the Y connection but just caused it to crash, so I put the Y connection back to the original orientation. It ended up being a origin setting in lightburn(thank you ray). I set the origin (in device settings) to be front left, now it cuts the way it should. Is it normal that it parks to the rear left even though the origin is set to front left? 


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2018 17:10*

This is all covered in the LB docs :) 


---
**Joe Alexander** *December 07, 2018 17:11*



yea parking in the rear left is normal as that puts the gantry out of tha way and that is also where the limit switch is located.




---
**Tim Shepherd** *December 07, 2018 17:21*

apparently i missed all of this while reading the documents lol. Everything is good now thanks guys!


---
**Sotm** *December 23, 2018 22:25*

You will probably have to reverse your X axis motor direction. The K40 is set up for right to left because that is the way Chinese read. The original K40 board handled this for the cutter, the Cohesion board does not. Had to do so on another laser cutter I had. 


---
*Imported from [Google+](https://plus.google.com/104043869405329045983/posts/bXTonX5A6kj) &mdash; content and formatting may not be reliable*
