---
layout: post
title: "Can someone advise me as to the best way to extend this stepper motor cable by 300mm?"
date: April 15, 2018 14:00
category: "Modification"
author: "Duncan Caine"
---
Can someone advise me as to the best way to extend this stepper motor cable by 300mm?  Thank you.

![images/60218f9c774a8dc171a6be9eed011afc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60218f9c774a8dc171a6be9eed011afc.jpeg)



**"Duncan Caine"**

---
---
**Andy Shilling** *April 15, 2018 15:12*

Go to your local rc model shop they will have lipo balance lead extenders, you'd need a 3 cell lead which will have four poles or they will have the plug and socket for you to make it up yourself.


---
**James Rivera** *April 16, 2018 06:17*

If that doesn’t work, just cut and splice in new wires, solder and shrink wrap. These wires don’t use a lot of current so it should be easy. (At least that’s what I’ve done in the past)


---
**Duncan Caine** *April 17, 2018 08:22*

Thank you Andy and James, I have now learned that the connectors are called JST.  One other thing, the lipo balance lead extenders are generally 300mm long, can I connect 2 to make 600mm?


---
**Andy Shilling** *April 17, 2018 08:26*

**+Duncan Caine** you can but over that length I'd suggest getting the correct size made up, less connections less problems.



Better still look for RGB led wire on your chosen auction site and you can cut/ solder and heat shrink your own custom length.


---
**Duncan Caine** *April 17, 2018 08:32*

**+Andy Shilling** Andy, many thanks, I'm learning all the time, I didn't even know this existed, found on ebay.

[ebay.co.uk - Details about 5/10/20m 4 Pin 5050 3528 LED RGB Strip Extension Connector Cable Wire](https://www.ebay.co.uk/itm/5-10-20m-4-Pin-5050-3528-LED-RGB-Strip-Extension-Connector-Cable-Wire/111762387450?hash=item1a058eb1fa:m:mmIT6ITXFKifDlVDH6w72Zw)


---
**Andy Shilling** *April 17, 2018 08:39*

**+Duncan Caine** that's the stuff I came across it a while back and always keep a few metres in my spares box. I tend to use it more with Dupont connections for my rc planes though.


---
**James poulton** *May 23, 2018 06:23*

What type of jst connectors does it use , I've found mini's and loads of different pins.? 


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/FQiG43o8tsf) &mdash; content and formatting may not be reliable*
