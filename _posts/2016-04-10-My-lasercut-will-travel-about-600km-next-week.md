---
layout: post
title: "My lasercut will travel about 600km next week"
date: April 10, 2016 18:20
category: "Discussion"
author: "Lo\u00efc Verseau"
---
My lasercut will travel about 600km next week. Is there something to do ? Also how to remove all the water ?





**"Lo\u00efc Verseau"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 10, 2016 19:12*

I assume you are moving house or something? I would suggest packing some bubble wrap around the tube region. Removing the water I would just unattach the pump & just drain the water out. After that, you may need to take the tube out & angle it so any residual water drains out of the tube. I wouldn't be too concerned about a few drops in there, but depends how long it will be sitting sealed up.


---
**MNO** *April 11, 2016 21:04*

my came with some water inside (i think for testing they filed it up), so water is not such problem. Protecting it from shaking this is what i would do... So:

1)making x and y axis to not move

2) like Yuusuf told buble wrap around tube

3) mdf plate on bottom so whole lasercutter casing will hold shape

4)Buble wrap or soft styropor for on bottom



and most important:

5)remember to take USB key if You use stock software :)

 


---
**L Vers** *April 11, 2016 21:06*

I've seen a very cool and simple mod to do. Add a USB hub inside with the dongle pluged-in. So you will never  lose it ;)


---
**MNO** *April 11, 2016 21:09*

yeap... it is on my list too... And add USB web cam to it to so i don't watch on beam.


---
*Imported from [Google+](https://plus.google.com/102787032729416934676/posts/aGyDKGhCnx1) &mdash; content and formatting may not be reliable*
