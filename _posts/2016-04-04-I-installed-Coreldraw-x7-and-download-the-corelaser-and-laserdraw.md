---
layout: post
title: "I installed Coreldraw x7 and download the corelaser and laserdraw"
date: April 04, 2016 13:07
category: "Software"
author: "John von"
---
I installed Coreldraw x7 and download the corelaser and laserdraw. Now when I go to engrave with corelaser, I get a server threw an exception message. The message doesn't occur when I am using laserdraw. Also, if I can't import a file from coreldraw, there is a problem with a filter. Any help would be appreciated. John von





**"John von"**

---
---
**ben crawford** *April 04, 2016 13:13*

i just had issues with the software my self, i threw it on an windows XP virtual machine running on my laptop and it worked just fine.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 04, 2016 15:47*

You might find that CorelDraw 7 is not compatible with the CorelLaser plugin maybe (or other way around). Most of us received CorelDraw 12 with the K40, albeit a not so legit version. If you don't have the CorelDraw 12 install, I have the entire K40 disc in my Google Drive as a .rar file.



[https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwUWJwM29COUNxSUU&usp=sharing)


---
**HalfNormal** *April 04, 2016 18:34*

Running X7 in Win10 just fine.


---
**Tony Sobczak** *April 05, 2016 06:42*

Same as Half normal. 


---
**John von** *April 24, 2016 22:09*

I sincerely apologize for the late response. I bought a version of coreldraw 7 and I am running it on window xp. It is working fine.

I truly appreciate your feedback.


---
**John von** *April 24, 2016 22:14*

Hey, sorry for the mistake, I bought coreldraw 11 and it working fine


---
*Imported from [Google+](https://plus.google.com/112642592493783290997/posts/PNTNRKbaL9g) &mdash; content and formatting may not be reliable*
