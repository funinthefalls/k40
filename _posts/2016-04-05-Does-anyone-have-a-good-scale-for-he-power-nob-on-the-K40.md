---
layout: post
title: "Does anyone have a good scale for he power nob on the K40?"
date: April 05, 2016 12:16
category: "Discussion"
author: "ben crawford"
---
Does anyone have a good scale for he power nob on the K40? There are no markings around the knob and i am now sure what power setting it's at other than what the mA meter says when test firing. Is there a good scale i can use for this?





**"ben crawford"**

---
---
**Scott Marshall** *April 05, 2016 12:27*

The ma reading is where the rubber meets the road. 



As the tube ages, the setting required to drive the tube at a given ma will change, so calibrating the pot isn't really helpful.



What DOES help is a nice 10 turn pot - with a vernier scale even... hey the price is right and it looks cool.



[http://www.ebay.com/itm/111725547354?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/111725547354?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**ben crawford** *April 05, 2016 12:36*

just bought one, it will indeed help. I guess for now it's just fiddling with it. What is a good mA setting for acrylic? so i need to adjust the power when cutting vs engraving?


---
**Scott Marshall** *April 05, 2016 12:45*

It depends on your Speed, best way is to 1st align the mirrors if you havn't done that yet, and get the focus dialed in by adjusting the height of the workpiece (trash the factory spring plate and make a nail bed you can adjust with blocks or such) 

Once you've got it right with the world, take some material you plan to use and step through speeds and ma settings, making a permanent guide as to what you get for particular combinations.

Cutting 1/8" acrylic I run 18mm/sec @ 18-20ma. Don't exceed 20 as it will tend to reduce your tube life. Slow down or make 2 passes. For engraving start at 200mm/sec and 3 or 4 ma, going up if you need to.



Have Fun,

Scott


---
**ben crawford** *April 05, 2016 12:48*

I guess it will just take time and experience to get right. I plan on junking the spring plate and using a honey comb. 


---
**Jim Hatch** *April 05, 2016 12:54*

**+ben crawford** I used a sharpie to mark 2 places on the machine - where it begins to lase (about 3-4ma) and where it hits 90% as I'd rather do 2 passes than stress the tube running it at (or over) max.


---
**Scott Marshall** *April 05, 2016 12:55*

The Honeycomb plates are kind of pricy for what they do. Hardware cloth or 17ga wire brads in 1/4 ply works well for almost free, there' a lot of creations, most work quite well.



 It took me about 2 months on and off to get the feel for this particular machine, and it needs some stuff added right away. A lot of people destroy the tube right off as they forget to turn on the water. It only takes a few seconds and it's a $200 awcrap. Adding a shutdown or alarm is recommended. Read through the archives here, there is a ton if info. The software takes a while too. Then you feel like changing the controller and learn it deja-vu all over again wise. I think most people's K40 are just a working progress (NOT work in progress, you get to use it while you build it)



You have to love playing with machines or the K40s not for you.


---
**ben crawford** *April 05, 2016 13:03*

I have the water pump, fan and laser on the same surge protected power supply, they are either all on or all off. I am already planning a few upgrades. I bought a laser cross module for lining up my cuts, not sure how to wire it into power just yet. I am looking at the most cost effective cutting bed i get to replace the spring one it comes with. I have yet to align the mirrors mostly because i have not looked up how.



A work in progress indeed but i am getting results.


---
**Scott Marshall** *April 05, 2016 13:19*

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
**Anthony Bolgar** *April 05, 2016 13:24*

I wonder if running at higher mA really shortens the tube life or not. Running at low mA requires you to run the tube for a longer time, so how much tube life are you really saving? For example (just using round numbers as an example, don't think that these are real world numbers):

10mA for 30 minutes or 20mA for 15 minutes. Wouldn't that be the same in terms of tube life?


---
**Scott Marshall** *April 05, 2016 13:29*

You're not the only one. The manufacturers warn against it, then rate their tubes at absolute maximum permissible current. 



It's the cathode heating that reduces the life, and something I don't claim to understand yet with gas mix breakdown. I think if it's well cooled and you don't do it a lot, it's ok to step on it once in a while.



I go to 29ma occasionally and don't concern myself too much. I'll blow it tomorrow now that I admitted it out loud.....


---
**ben crawford** *April 05, 2016 13:31*

It all depends on if life usage of the tube is linear It could be that when you use double the mA you are taking three times as much life out of the tube. It's just a thought.


---
**Jim Hatch** *April 05, 2016 13:37*

**+Anthony Bolgar** I don't know - how long can you walk vs sprint? Running machines at max capacity is generally recognized as life-shortening relative to running them at lower capacities. I know my car & motorcycles recommend against running for long periods of time at redline but don't have a similar warning about total # of hours (or miles). Stresses at maximum are likely more than linearly increased over lower power so low power at long times is less impactful than high power for shorter periods.


---
**Anthony Bolgar** *April 05, 2016 13:43*

I do not run at max very often, but I would love to see an actual test done under real world conditions where we could compare full power vs lower power and see how may mm of cut each setup could do before tube failure. Anyone want to spend $1000 to find out? (lol)


---
**Scott Marshall** *April 05, 2016 13:49*

**+Jim Hatch**

I agree completely, and I know that the tube is wearing faster - you boil off the coating on the cathode when you run it hard, a common problem with HAM radio transmitter tubes if you load them up too hard (Used to do that too, back in the '70s, but wouldn't if I was running a commercial station).



If it wears out, I'll put a larger one in it. and probably overdrive it too.  It's all a matter of your personal way of looking at life.



I personally don't have a problem running most stuff @110%, but there's lots of people out there that drive 2mph under the speed limit, and have never produced tire smoke. Nothing wrong with that.



 I raced NHRA for 20years.....


---
**Vince Lee** *April 05, 2016 15:18*

Here is a page discussing laser hardware that touches on some lifespan considerations.  [http://www.repairfaq.org/sam/laserco2.htm](http://www.repairfaq.org/sam/laserco2.htm)


---
**Scott Marshall** *April 05, 2016 15:26*

**+Vince Lee**

Great Resource!



Thanks, Scott


---
**Don Kleinschnitz Jr.** *April 06, 2016 14:34*

I wired a digital voltmeter across the pot in my K40 that way I have a reference for the pots position accurate to .1v. 

I document that setting for whatever am cutting and it is easy to go back to a known setting of the pots rotation.

I posted the wiring diagram for my K40 that includes this hack somewhere on here.


---
**Heath Young** *April 07, 2016 01:37*

I set mine up so that the lowest setting on the pot is just at threshold, and the highest setting is 20mA. Its done by using trimpots as voltage dividers. Means that the current range is now linear across the board (and you can't overshoot).


---
*Imported from [Google+](https://plus.google.com/114995491064499005334/posts/HcSG121Jrcd) &mdash; content and formatting may not be reliable*
