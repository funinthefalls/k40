---
layout: post
title: "Hey all, been a while. Been busy busy with learning 3d printing and like the k40 I couldn't resist the need to modify stock firmware lol"
date: September 11, 2017 03:57
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
Hey all, been a while. Been busy busy with learning 3d printing and like the k40 I couldn't resist the need to modify stock firmware lol. Then summer hit, night new outside toy as seen below 😁. 



Was reading through posts seeing what all I've missed. What firmware is everyone using these days for k40? I never made the plunge to smoothieware, correction I thought I did but find I bought the wrong thing, mks 1.2 board. So I never installed it and haven't ran laser since. Still running Arduino+ramps 1.4 as of now. If printing I would still be using inkscape, gimp, or something found way back in mpcnc for images, great software. What about laserweb? I installed it way back when but never got the privilege to run it or sort it out. In the end I don't want to lose all that I currently know by not staying with the up in up.



Basically I went off in so many directions that I got lost in the world of tech, code, Arduino, rpi, Kodi, electronic components, scrap gold, transformer projects, designing and even got really really into graphene oxide synthises/uses countless. Currently I'm in the process of putting together a home brew large CNC and messing with renewable energy projects. Also still heavily involved with RC stuff flying things and driving things. 



Added pics of what all I got accomplished with laser, then what I accomplished after getting 3dprinter flashed over with images of very first prints using reptier host and cura. I'll continue reading through past posts catching up. Thanks for reading 😀



![images/9069926748df60964a5c2ac788251d2c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9069926748df60964a5c2ac788251d2c.jpeg)
![images/0c33d2f7eeb6b600c62626ec215d8e70.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0c33d2f7eeb6b600c62626ec215d8e70.jpeg)
![images/2f878819488589c4465c8868b770e963.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f878819488589c4465c8868b770e963.jpeg)
![images/e70db3ad8e4c893741dbcdcf42584b16.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e70db3ad8e4c893741dbcdcf42584b16.jpeg)
![images/989fbdcd043cc38da909e9149efbd608.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/989fbdcd043cc38da909e9149efbd608.jpeg)
![images/de25b160bfabd5543bade1551f8f26c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de25b160bfabd5543bade1551f8f26c6.jpeg)
![images/9310e99374e13f8db7140de9f4d344da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9310e99374e13f8db7140de9f4d344da.jpeg)

**"Andrew ONeal (Andy-drew)"**

---


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/3EMHRKPEThL) &mdash; content and formatting may not be reliable*
