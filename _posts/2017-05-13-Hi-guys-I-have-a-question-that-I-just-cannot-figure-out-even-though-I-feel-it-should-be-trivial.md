---
layout: post
title: "Hi guys. I have a question that I just cannot figure out even though I feel it should be trivial"
date: May 13, 2017 00:12
category: "Discussion"
author: "Jason He"
---
Hi guys. I have a question that I just cannot figure out even though I feel it should be trivial.



If I wanted Laserweb to leave a sprue in a shape that I am cutting, how should I modify the shape in the SVG?



It will normally cut the outline of the shape, but my goal is to just leave about a millimeter that isn't cut.



I have been using CorelDraw to create the SVG if that helps.





**"Jason He"**

---
---
**Martin Dillon** *May 13, 2017 01:35*

Corel draw has a break curve button.  select the shape you want to edit.  Depending on the shape you might have to convert it to curves under the arrange menu at the bottom. then click the shape tool.  select the node you want to break. then click the break curve icon.

![images/72039437986b5fb1a39b92e446edc8b4.png](https://gitlab.com/funinthefalls/k40/raw/master/images/72039437986b5fb1a39b92e446edc8b4.png)


---
**Martin Dillon** *May 13, 2017 01:37*

Then just manually move the two nodes apart the distance you want.


---
*Imported from [Google+](https://plus.google.com/109350981963273445690/posts/C4HGwtwfv3F) &mdash; content and formatting may not be reliable*
