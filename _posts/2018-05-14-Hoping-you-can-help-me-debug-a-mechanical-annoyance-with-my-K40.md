---
layout: post
title: "Hoping you can help me debug a mechanical annoyance with my K40"
date: May 14, 2018 21:11
category: "Discussion"
author: "Jake B"
---
Hoping you can help me debug a mechanical annoyance with my K40.  Please watch the attached video.  Please notice that when the Y-axis is near the front of the machine, it sounds as you'd expect.  However, when you get towards the back, it gets noisy and "clanky".



Things I've tried: (1) adjusting motor driver current limits, (2) adjusting Y-belt tension (3) snugging up the roller spacing on the right side of the Y-axis. (4) removing drag chain (just to see if it contributed)



Not sure what to check that would only affect half the travel of the Y-axis.  The machine is usable in other regards, but I'm not sure if this is a symptom of a larger issue.




**Video content missing for image https://lh3.googleusercontent.com/-B3_RzuC2MvQ/Wvn7eIup6pI/AAAAAAAAA0Q/nEANZjnlG708rDG8uFdbsRqRwHk6Pf4qgCJoC/s0/K40%252BDebug.mp4**
![images/e9ad44ab5a83e03d22338752bf90f963.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e9ad44ab5a83e03d22338752bf90f963.jpeg)



**"Jake B"**

---
---
**Don Kleinschnitz Jr.** *May 14, 2018 22:33*

If you slide it with no power applied does it bind or is there an increase in resistance in that area?


---
**Jake B** *May 14, 2018 23:05*

**+Don Kleinschnitz** lets say yes. I can detect a difference in feel. But I’m not sure where it’s coming from.  I wouldn’t call it binding, but defiantly a change in resistance.


---
**Jake B** *May 14, 2018 23:17*

Even still it is very slight. It might even be cyclic like a slightly out of round idler.  But really hard to say.    No change in sound when moving by hand unpowered.     My initial worry is that the rod might be out of true but it’s a petty beefy rod to have bent. 


---
**Joe Alexander** *May 15, 2018 01:03*

check to see if the gantry frame is bolted on straight, a little torsion on the frame could cause this kinda id imagine. or like you said a bad spot on a bearing but it didnt sound like that in the video. also check the belt tensioners in the back to make sure its not overtight or way loose.


---
**Paul de Groot** *May 15, 2018 07:21*

I get that noise when the steppers are overpowered. Check your 24v dc supply. You can dial the voltage back in the laser supply and see if that improves. Internally the 24v is used to supply a 12v regulator. So don't dial it back to far below 20v otherwise the system becomes unstable.


---
**Wolfmanjm** *May 15, 2018 10:08*

i had binding at one end, a dab of wd40 and 3in1 oil on the smooth rod fixed it for me.


---
**Jake B** *May 15, 2018 10:46*

**+Joe Alexander** I can't find my machinist square but using the best square I have otherwise, I do see the near-side left corner is a little bit to out of square.  The frame doesn't seem skewed overall, just this one corner.  Very slight, but noticeable.  I'm going to try squaring up this side (the important side) and see how it works, before I go down the rabbit hole of adjusting one corner of the frame independently.



Is it ever necessary to shim between the frame and the cabinet to account for errors in the "Z" dimension?



**+Paul de Groot** I have an separate 24v supply dialed in to exactly 24v.  I can try and backing it off into the 23v range and see if that affects, but I'm guessing I should figure out the frame first.


---
**Joe Alexander** *May 15, 2018 11:09*

for adjusting the z most of us remove the plate and use a lab jack or something similar to raise/lower the workpiece. also don't be intimidated about removing the entire gantry to check it and reinstall, its something we all do at some point :P


---
**Jake B** *May 15, 2018 11:11*

**+Joe Alexander**. I understand.  I meant washers or shims between the frame and the case when bolting down.  For the workpiece, I've build a motorized z-stage.


---
**Joe Alexander** *May 15, 2018 11:13*

ahh gotcha. I guess I never went that direction as i replaced the standard tube mounts with ones off thingiverse that are adjustable up/dwn and L/R so leveling the tube in relation was a cinch.


---
**Jake B** *May 15, 2018 22:32*

So I think I found one problem, the screw was stripped out of the right side pulley tensioner, making it impossible to tighten the belt on that side.  I jury rigged it by putting the nut inside the bracket rather than on the outside (was it welded originally, or the sheet metal bracket threaded?)



I made sure the gantry is square but haven't tried reducing the voltage yet.



After tightening the belts, the whole thing got quieter, but still a little noisy in the first 25% of the travel near the limit switch.  This is an improvement from the 50% mark where the noise started before.


---
**Jake B** *May 15, 2018 23:51*

Here’s how I fixed my stripped pulley adjustment screw hole:  a small 3D printed nut plate that holds the real nut inside. Doesn’t need to be tightened much more than the thickness of the nut anyway:__[https://plus.google.com/photos/...__](https://plus.google.com/photos/...__)


---
**Jake B** *May 16, 2018 20:46*

**+Paul de Groot** thanks Paul.  Tried reducing my power supply to around 22v (lowest I can go with the 24v supply adjustment).  Didn't really make a difference.  Honestly, none of what I've tried has really made an appreciable difference.  I think I'm just going to live with it as "Its just a budget K40"


---
*Imported from [Google+](https://plus.google.com/101176403058148207601/posts/94jpKj4QhsT) &mdash; content and formatting may not be reliable*
