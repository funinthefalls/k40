---
layout: post
title: "Ms10105/moshidraw, it's time to die. It is with grim satisfaction that I will de-solder you for parts"
date: October 09, 2016 18:08
category: "Modification"
author: "Anton Fosselius"
---
Ms10105/moshidraw, it's time to die. It is with grim satisfaction that I will de-solder you for parts. May you suffer in agony and pain for the last 2 years of pain you have put me through.



![images/2af16543686271cdfaf0f294cebe91d8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2af16543686271cdfaf0f294cebe91d8.jpeg)
![images/fcad50a6189bdf4b92eb09fe20017752.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fcad50a6189bdf4b92eb09fe20017752.jpeg)

**"Anton Fosselius"**

---
---
**Don Kleinschnitz Jr.** *October 09, 2016 18:20*

....."long live the "SMOOTHIE" :).


---
**Ray Kholodovsky (Cohesion3D)** *October 09, 2016 19:11*

L'chaim!


---
**Timo Birnschein** *October 10, 2016 18:43*

hahaha! Go for it! Just finished mine! [https://github.com/McNugget6750](https://github.com/McNugget6750)

[github.com - Timo](https://github.com/McNugget6750)


---
**Anton Fosselius** *October 10, 2016 18:45*

**+Timo Birnschein** yes, will probably go for something similar, but mine will be smaller, maybe the smallest to date? Will See how things turn out. 


---
**Timo Birnschein** *October 11, 2016 01:08*

**+Anton Fosselius** Cool! I can't wait to see it. The only question I have is why smaller... there is plenty of space and design as well as manufacturability will certainly suffer. Mine is specifically designed for single sided plus wires but can be manufactured as dual sided with the exact same dimensions as the moshi board. But go for it! I'm looking forward to your results!


---
**Anton Fosselius** *October 11, 2016 05:04*

**+Timo Birnschein** because I work with small embedded iot solutions.

So it's hard for me to build big as i got a mental constraint. 

 I am tempted to pipe the gcode through gprs just because I can ;)

I am not aiming for a factory grade production system. I am just fooling around. Right now I do not care about Latency, reliability and efficiency. 


---
**Timo Birnschein** *October 12, 2016 01:36*

**+Anton Fosselius** Sounds good! I'm looking forward to it!


---
*Imported from [Google+](https://plus.google.com/115118068134567436838/posts/FUf9Cvgcz9x) &mdash; content and formatting may not be reliable*
