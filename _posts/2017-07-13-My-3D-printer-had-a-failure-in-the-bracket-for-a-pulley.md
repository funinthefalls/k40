---
layout: post
title: "My 3D printer had a failure in the bracket for a pulley"
date: July 13, 2017 13:46
category: "Object produced with laser"
author: "Jeff Johnson"
---
My 3D printer had a failure in the bracket for a pulley. My first thought was that I could just 3D print another but then my brain slowly realized that the 3D printer didn't work anymore. The original part was made from 8mm plexiglass and I checked my sheets and found that I had 2mm thick expensive and very tough plexiglass so I decided to use it. This stuff is very strong but doesn't shatter. To cut it into 300x200mm sheets I have to score it deep with a razor and bend it at a pretty steep angle. It behaves like a really tough Kydex.



Anyway, after about half an hour of measuring I printed out 8 pieces and glued them togeter, 4 at a time. I used CA glue and clamped them for a few minutes and I'm very happy with the results. I guess now it's time for the 3D printer to make something for the laser cutter to return the favor. Maybe a cool knob or frame for the power meter.



![images/1707e68f30900baa65bf74b37f2136b1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1707e68f30900baa65bf74b37f2136b1.jpeg)
![images/1bf279ed7c0147d88383c8a8b58a19be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bf279ed7c0147d88383c8a8b58a19be.jpeg)

**"Jeff Johnson"**

---
---
**Henner Zeller** *July 13, 2017 15:55*

Plexiglass that is tough and doesn't shatter ? That sounds like polycarbonate not acrylic (Plexiglass is a brand-name for acrylic).


---
**Jeff Johnson** *July 13, 2017 16:12*

**+Henner Zeller** It could be. The label was thrown away after I bought it but it was pretty expensive compared to the cheapest stuff I have that's brittle. I bought it for making leatherworking templates. Mostly wallet designs.


---
**Madyn3D CNC, LLC** *July 14, 2017 15:09*

The ability to repair machines using other machines is probably one of my favorite things about having a CNC hobby shop. Great work! 


---
**Jeff Johnson** *July 14, 2017 15:22*

**+Madyn3D CNC, LLC** I agree. I was upset that the part broke until I realized it was an opportunity to make something I needed. I'm actually glad I got the chance to do it.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/R1SetkSeauy) &mdash; content and formatting may not be reliable*
