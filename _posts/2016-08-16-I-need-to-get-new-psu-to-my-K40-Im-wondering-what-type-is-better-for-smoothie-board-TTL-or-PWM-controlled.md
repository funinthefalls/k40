---
layout: post
title: "I need to get new psu to my K40 Im wondering what type is better for smoothie board ,TTL or PWM controlled?"
date: August 16, 2016 16:42
category: "Hardware and Laser settings"
author: "Damian Trejtowicz"
---
I need to get new psu to my K40

Im wondering what type is better for smoothie board ,TTL or PWM controlled?

Now i had PWM with separate ignition pin(1

st picture) but wondering about getting new ttl conntroled









![images/93c20e091698fadeea197fc7263e50bf.png](https://gitlab.com/funinthefalls/k40/raw/master/images/93c20e091698fadeea197fc7263e50bf.png)



**"Damian Trejtowicz"**

---
---
**Bart Libert** *August 16, 2016 17:06*

is there a differnce ? as far as I know the PWM pin is the power level and then you indeed have a shoot enable to let the laser shoot at the power level setup with the pwm signal




---
**Damian Trejtowicz** *August 16, 2016 17:07*

I read it ttl version works better because You use only one pin


---
**Bart Libert** *August 16, 2016 17:09*

you have to set power somehow ? Thats on the PWM input. If you PWM at a reasonable (fast enough frequency) or hookup a potentiometer, is doesn't make a difference but at lease you need to set power somewhere ?


---
**Damian Trejtowicz** *August 16, 2016 17:11*

With smoothie board software set up power level,you dont need potentiometr


---
**Bart Libert** *August 16, 2016 17:15*

no because the power is set with pwm, so you still need a PWM/analog combined power level input and a TTL trigger input


---
**Damian Trejtowicz** *August 16, 2016 17:17*

Well to be sure i will get same type like was.but im sure when you use ttl version is the same pin for fire and power level.


---
**Alex Krause** *August 16, 2016 18:04*

I have a single pin setup smoothie it is not necessary to have a Pot to set max power but I use mine to ensure I don't accidently overdrive my tube and allow for adjustments on the fly


---
**Stephane Buisson** *August 17, 2016 17:08*

In my setting, I do like:

A) I am able to fire a shot without Using the board (smoothie)

B) using  the smoothie to set the power for the job (the pot is used as ceilling, because at 5V  at max firing is far too much for the tube life, misshandling happen when you are tired)


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/fpXWsfHiRpZ) &mdash; content and formatting may not be reliable*
