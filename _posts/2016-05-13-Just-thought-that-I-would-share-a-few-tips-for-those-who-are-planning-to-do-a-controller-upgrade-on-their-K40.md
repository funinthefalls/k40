---
layout: post
title: "Just thought that I would share a few tips for those who are planning to do a controller upgrade on their K40"
date: May 13, 2016 02:39
category: "Modification"
author: "Anthony Bolgar"
---
Just thought that I would share a few tips for those who are planning to do a controller upgrade on their K40. These are general tips, not specific to any specific controller.



1. Plan out the upgrade - create a list of all the required parts, and break down the upgrade into simple and testable steps. This is the most important part of the upgrade process. Poor planning, or trying to do too much at once usually leads to errors and frustration.



2. Keep the wiring neat - color coded and neatly run wiring can make troubleshooting problems much easier - if it looks like a rats nest, it will be difficult to track down problems.



3. Document the upgrade as you go along. By doing this, you can undo changes much easier if necessary, especially when it comes to firmware or software issues, and will also make life easier if you decide to upgrade a second machine.



4. Take pictures and document the original configuration. That way you can always put it back to stock if necessary. Nothing worse than having one or two critical connections that you can not figure out.



5. Take your time, rushing will only increase the chance of error.



6. Use a static ground strap. The chance of destroying a component from static is a distinct possibility. It does not happen often, but it is a case of better safe than sorry, and ground straps are very inexpensive.



7. If you are unsure of how to do something, reach out to the group for help, there are some pretty smart people who are always willing to help.



8. Double check all connections before powering the machine on, especially the AC connections and the high voltage output to the tube. I recommend always using a GFCI outlet, and make sure all tools are out of the machine, and all components are secured to the case properly.



I hope these tips help, if you have any questions, please feel free to comment or contact me.





**"Anthony Bolgar"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:53*

Thanks for sharing that Anthony. Some good tips here.


---
**Anthony Bolgar** *May 13, 2016 08:35*

If anyone has any other tips, please comment.


---
**Greg Curtis (pSyONiDe)** *May 13, 2016 15:59*

Don't look at the operational end of the device. 😉


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/c5N3qiMgH3f) &mdash; content and formatting may not be reliable*
