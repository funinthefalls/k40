---
layout: post
title: "Shared on August 08, 2016 01:15...\n"
date: August 08, 2016 01:15
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---






**"Ariel Yahni (UniKpty)"**

---
---
**Jeff Kes** *August 08, 2016 17:13*

That is awesome is the rotation device your design or someone else?  My son and I are looking to make one for our K40 also.


---
**Ariel Yahni (UniKpty)** *August 08, 2016 17:18*

**+Jeff Kes**​ not my design. Here is the link Laser Rotary Attachment #Thingiverse [https://www.thingiverse.com/thing:1359528](https://www.thingiverse.com/thing:1359528)


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/Dyf1EpezPst) &mdash; content and formatting may not be reliable*
