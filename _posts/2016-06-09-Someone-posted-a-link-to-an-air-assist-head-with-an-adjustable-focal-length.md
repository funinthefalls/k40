---
layout: post
title: "Someone posted a link to an air assist head with an adjustable focal length"
date: June 09, 2016 18:03
category: "Air Assist"
author: "Bruce Garoutte"
---
Someone posted a link to an air assist head with an adjustable focal length. does anyone know which one that is?





**"Bruce Garoutte"**

---
---
**Jim Hatch** *June 09, 2016 18:21*

check ebay. Saite has an aluminum adjustable head with air assist. The one I got has to have a new base plate (that aluminum plate the laser head screws into) built for the machine due to the size of the mount.


---
**Bruce Garoutte** *June 09, 2016 18:31*

Thanks Jim, I'll look into it. My K40 arrived today and is sitting at home waiting for me to unpack it. I know that I'm going to need this to vector cut, so am researching now.


---
**Eric Rihm** *June 09, 2016 21:17*

you can just drill out your old plate for the saite cutter head


---
**Jim Hatch** *June 09, 2016 21:18*

**+Eric Rihm**​ True enough but if the new one doesn't work out you can't just swap the old head back in. 😃


---
*Imported from [Google+](https://plus.google.com/101697686569322649899/posts/PRBA7QHj4DR) &mdash; content and formatting may not be reliable*
