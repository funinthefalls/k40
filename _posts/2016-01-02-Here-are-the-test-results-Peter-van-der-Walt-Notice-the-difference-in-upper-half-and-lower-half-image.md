---
layout: post
title: "Here are the test results Peter van der Walt Notice the difference in upper half and lower half image"
date: January 02, 2016 22:45
category: "Materials and settings"
author: "ChiRag Chaudhari"
---
Here are the test results **+Peter van der Walt** Notice the difference in upper half and lower half image. upper half I removed M3 when the the head returns to x min. 



![images/4f80c78a5407e4c898a3c0d450c3ee22.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4f80c78a5407e4c898a3c0d450c3ee22.jpeg)
![images/508f89e11e7333e5b721a7fef8b06664.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/508f89e11e7333e5b721a7fef8b06664.jpeg)

**"ChiRag Chaudhari"**

---
---
**Jim Hatch** *January 03, 2016 00:17*

What were you using for speed & power settings?


---
**ChiRag Chaudhari** *January 03, 2016 00:24*

Power 10-30 in the increment of 2 and I am not so sure about which speed it picked up 500 or 5000. Here is the code.



G21 F500 <- it may homed machine at 500

G28 XY



M5 

 M3 

 G01 X0.0 Y0.0 S30.00 F5000 <-- raster speed ??!?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 03, 2016 03:50*

Even though it looks messed up, I can make out the nose, eyes, mouth in the image. So it's at least vaguely getting there. I think there are too many pixels (or dots burned) too close to each other. If you scaled the image up to a much larger size (& test on something super cheap) you might end up with a much nicer result, maybe?


---
**ChiRag Chaudhari** *January 03, 2016 04:25*

Peter has some other way to do the raster process now, he's writing the code for it. Also this was the very first code and was inverted. 



Also I need to lower the power further more and/or increase the speed. Another biggest issue with the coffee was, the laser was firing while going to next line ( cartridge return), making the image burn even more


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/aVWqqtfyMoZ) &mdash; content and formatting may not be reliable*
