---
layout: post
title: "Has anyone tried cutting vinyl records in the k40?"
date: December 24, 2016 01:26
category: "Object produced with laser"
author: "VetGifts By G"
---
Has anyone tried cutting vinyl records in the k40? Is it safe? Remember reading somewhere that it will corrode everything metal.





**"VetGifts By G"**

---
---
**Anthony Bolgar** *December 24, 2016 01:28*

Very dangerous, the vinyl will off gas chlorine, which can kill you. DO NOT ATTEMPT, please!


---
**VetGifts By G** *December 24, 2016 01:41*

Ok, I will just do it on my cnc router, thanks for the info!


---
**Ned Hill** *December 24, 2016 03:23*

Vinyl records are made from PVC or a mixture of PVC and PVA (polyvinyl acetate).  The PVC is the concern as one of the primary thermo oxidation (combustion) products is hydrogen chloride (HCl) gas which is VERY corrosive.  It doesn't produce any appreciable chlorine gas that I'm aware of, but HCl gas is a strong pulmonary and wet tissue irritant that can cause death at high levels.  Ventilation  would handle that but the HCl gas would degrade the hell out of your fan (among other things).  I think there could a concern with milling the PVC with a CNC router as the milling action will heat up the material and can cause release of HCl gas as well.  Proceed with caution.




---
**Alex Krause** *December 24, 2016 04:07*

Less off gassing with milling... I mill on a Mazak and turn all day long on a Haas lathe... The issue is chip evacuation I would use an air jet and a down cut endmill... Using a upcut endmill would pull the record up into the cutter and more than likely crack/break small detail cuts. The chips that you produce will be electrostaticly charged so evacuating the with air blast at the point of operation and pulling with a dust shoe would be strongly advised lest you want PVC shavings sticking to everything


---
**VetGifts By G** *December 24, 2016 14:17*

Thanks guys! Appreciate the info! I have only worked with wood so far and wanted to experiment with something different.


---
*Imported from [Google+](https://plus.google.com/113361677380464824789/posts/SBo6t8Wjcgy) &mdash; content and formatting may not be reliable*
