---
layout: post
title: "Hello, We have just bought our first laser engraver with the hopes of creating products"
date: December 07, 2018 10:58
category: "Discussion"
author: "B&C Productions"
---
Hello, 



We have just bought our first laser engraver with the hopes of creating products. We have downloaded all necessary programs and have gone through the steps to run Zadig and the driver it is showing under interface in device manager as running properly, but when I run K40 Whisperers an error comes up saying "USB Error: Laser USB Device not found."  I am stuck as to how to get the program to communicate with the machine and would love any advice :) 



If I somehow manage to figure this out I will post how I resolved the issue in case anyone else has this issue too. 



Thanks.



![images/1b69fedd570e7559293422d4f19ce699.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b69fedd570e7559293422d4f19ce699.jpeg)
![images/505c2a92024a8717ef8439382ccd5358.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/505c2a92024a8717ef8439382ccd5358.jpeg)

**"B&C Productions"**

---
---
**Don Kleinschnitz Jr.** *December 07, 2018 13:43*

I suggest that you tell us more about what machine you bought especially the controller that is in it?


---
**Ned Hill** *December 07, 2018 14:44*

Just to confirm, you went through all the steps of the whisper setup? [scorchworks.com - K40 Whisperer USB Driver Setup](https://www.scorchworks.com/K40whisperer/k40w_setup.html)

Also did you use option 7a or 7b?

Want version of windows are you using?


---
**James Rivera** *December 07, 2018 18:22*

Just going from memory (I'm not at my laser) but IIRC, in Windows Device Manager, it should be listed under the com port (e.g. "COM3") instead of "Interface". This makes me think you didn't install the USB drivers properly.


---
**Scorch Works** *December 07, 2018 19:22*

The driver is not installed.  You can just go through the steps again to resolve it.  When the driver is installed correctly it will look something like the attached pic.![images/db1e72a622c7712c7942a612d665b6bd.png](https://gitlab.com/funinthefalls/k40/raw/master/images/db1e72a622c7712c7942a612d665b6bd.png)


---
*Imported from [Google+](https://plus.google.com/107556817235093980815/posts/SFF3vpxz37U) &mdash; content and formatting may not be reliable*
