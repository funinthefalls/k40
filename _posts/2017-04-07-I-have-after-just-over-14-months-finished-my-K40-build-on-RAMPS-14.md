---
layout: post
title: "I have, after just over 14 months finished my K40 build on RAMPS 1.4"
date: April 07, 2017 10:08
category: "Modification"
author: "timne0"
---
I have, after just over 14 months finished my K40 build on RAMPS 1.4.



My grinding and resonance issues have all been resolved now I found a post on A4988 pololu copies.  MS3 jumper on the RAMPS board does not work and it ignores it.  Set MS1 and MS2 as the copies only support 1/8 microstepping not 1/16.  Reverted to the MK4Duo default firmware with only a couple of changes and it's smooth, quick and quiet.



New laser head from lightsource was well worth the money - the original lens corroded and I bought a cheap chinese head from ebay and new lens rather than pay excessive shipping from lightsource.  This was a false economy.  New one from lightsource arrived, transferred lens and it's working beautifully.  Just need to realign and I'm ready to use it.  It's been a long journey but I now know a ridiculous amount about RAMPS systems now.  No ghosting any more - the ghosting was caused by the cheap chinese laser head due to poor machining.  You get what you pay for...





**"timne0"**

---


---
*Imported from [Google+](https://plus.google.com/117733504408138862863/posts/CyZjcP4nDUN) &mdash; content and formatting may not be reliable*
