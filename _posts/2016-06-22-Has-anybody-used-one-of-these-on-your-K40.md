---
layout: post
title: "Has anybody used one of these on your K40?"
date: June 22, 2016 02:56
category: "Modification"
author: "Terry Taylor"
---
Has anybody used one of these on your K40?

I am curious about how it would connect and be driven.



[http://tinyurl.com/ztlvuyy](http://tinyurl.com/ztlvuyy)





**"Terry Taylor"**

---
---
**Jim Hatch** *June 22, 2016 03:07*

If you look it up on LightObject's web site they say it won't run off the standard controller and the comments say it will only do small diameter objects due to the lack of depth of the k40 case.



That means you'll need a Smoothie to drive it (and other software - not sure if LaserWeb supports rotary engraving yet) and to do meaningful stuff you'll need to cut out the bottom of the case and create some kind of extension for added machine depth.


---
**Terry Taylor** *June 22, 2016 03:12*

Kinda what I thought, thanks!


---
**Ben Walker** *June 22, 2016 14:26*

I have a new one of these willing to sell.  $100 will get it to you in the CUS.  I should have sent it back but missed return window.  If this is inappropriate let me know.


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/cooPma5TFXS) &mdash; content and formatting may not be reliable*
