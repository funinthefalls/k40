---
layout: post
title: "I am an admin on a small FB group for owners of a certain type of rare wood lathe"
date: October 26, 2018 13:27
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
I am an admin on a small FB group for owners of a certain type of rare wood lathe. We moved from Yahoo when that platform got ugly to use and maintain. We now have a small and focused group without all the glitz of the larger woodworking FB groups. 



It dawned on me, why don't we do the same with this G+ group. 



I don't know what it is specifically but I enjoy working with this group more than the current Laser engraving groups on FB. I guess this group and G+ in general seems to be more Maker/engineering oriented and there is not as much "social overhead" on here. Maybe its just that those groups are so large and diverse I find myself wading through a lot of garbage to get to interesting things. Maybe its just me ....



What do you think?





**"Don Kleinschnitz Jr."**

---
---
**Jim Hatch** *October 26, 2018 13:50*

Definitely agree with regards to the FB laser groups. This group is mostly about makers & tinkerers than FB seems to gravitate to. 


---
**James Rivera** *October 26, 2018 15:08*

I’m already on FB so most of the complaints about FB as a bad platform to join are irrelevant for me. I’d happily join an FB group with this crowd.


---
**Stephane Buisson** *October 26, 2018 16:06*

I never and still not trust FB (and all others software from FB), a no go for me.  this view is shared by many other old "Maker" with a bit of knowledge background. don't worry we will find a solution for the community.


---
**HalfNormal** *October 26, 2018 16:39*

FB is not a place for me.


---
**Jim Fong** *October 26, 2018 18:56*

I’m already subscribed to several FB laser/cnc groups so it wouldn’t bother me to much.  You are right about FB. I like the people here since it is more technical.   Wherever this group ends up, I hope it lives on with many of the same users. 


---
**Jamie Richards** *October 26, 2018 23:45*

Don't use any apps made for FB and you should be fine.  I've been there for years without issue.


---
**James Rivera** *October 27, 2018 00:31*

**+Jamie Richards** Bang on right. Some co-worker friends from Microsoft and I were considering developing an app for FB in its early days (about mid-2007). We discovered the app would have access to your list of friends (and IIRC, their friends!) and we realized this was just not something we were comfortable with and we abandoned the idea/platform. Some creepy shit, IMHO. Those early apps (Vampires/Werewolves) were little more than data harvesting s**t.


---
**Jamie Richards** *October 27, 2018 01:09*

I've been warning friends and family about the apps for years, and of course some didn't listen and next thing you know, I was getting spam emails using their name to make me open them. They just recently updated the security, but I still won't install one because they can still have access to your email and list.  Some of those apps used to access everything and people would still give them permissions and install knowing this fact.


---
**James Rivera** *October 27, 2018 01:40*

**+Jamie Richards** Yep. The metadata they exposed by default was insane. Like home phone numbers, addresses, and email addresses--of your friends. I think the defaults might be better now, but I haven't looked into it. I simply do not install ANY apps on FB.




---
**Jamie Richards** *October 27, 2018 04:22*

And now google had the same issue and kept quiet about it?  They still got caught.


---
**HalfNormal** *October 27, 2018 05:01*

Old fashioned forums do not have any hidden info mining.


---
**Don Kleinschnitz Jr.** *October 27, 2018 15:48*

Social platform discussions always raise a spectrum of  concerns.



Every platform has its security vs usability trade-offs. More usability usually exposes more security issues, especially when you want to share a single copy of information.

There is nothing I share on this community that I am concerned about anyone on the planet getting access to. 



I left forums some time ago and for me that technology, does not provide the desktop and mobile user experience I have come to expect including the built in sharing abilities.



I also have become used to much less administration overhead in fact that is a main reason why I moved to FB and G+ to start with.



Sorry, but I am not planning on going back to forums. 



If we don't find a better platform than FB when G+ dies I plan to start a FB group and see how it goes. It takes no effort on my part.



We had this exact problem with my wood turning group when Yahoo became ugly, with the same FB concerns. Now we are back to sharing and collaborating ....... 




---
**Jamie Richards** *October 27, 2018 16:03*

Yahoo definitely became ugly.  I did notice they have an app for the forums now.


---
**HalfNormal** *October 27, 2018 16:34*

**+Don Kleinschnitz Jr.**  My forum is Word Press based and very flexible for the new mobile user. What do you mean by "sharing" abilities?


---
**Jamie Richards** *October 27, 2018 16:43*

I actually miss the old forums, (especially old style Yahoo) but admit I'm a Facebook addict. lol


---
**Rob Mac** *October 27, 2018 17:07*

It's bad enough being on G+ instead of a proper forum system, some rubbish on FaceBook would be a downgrade!!!


---
**James Rivera** *October 27, 2018 17:36*

Wordpress is a great platform. But it would largely be a single interest per domain (e.g. [quilting.com](http://quilting.com) is only for those interested in quilting), whereas G+ and FB can have myriad interest groups that you can login to with a single account. Ease of use greases the gears. I created an account on [mewe.com](http://mewe.com), but I haven’t actively used it yet. [mewe.com - MeWe: The best chat & group app with privacy you trust.](http://mewe.com)


---
**James Rivera** *October 27, 2018 17:37*

Note that I needed nothing but an email address to join it. I didn’t need to post any further information about myself.


---
**HalfNormal** *October 27, 2018 17:40*

**+James Rivera** what other interests are there other than lasers?


---
**James Rivera** *October 28, 2018 02:32*

**+HalfNormal** It’s just another platform, similar to FB, but without the invasion of privacy.


---
**Stephane Buisson** *October 28, 2018 08:23*

**+HalfNormal** check my post here :

[plus.google.com - +Thomas Sanladerer, +Nils Hitze, +Anthony Bolgar, +Stephanie A, +Griffin Paqu...](https://plus.google.com/u/0/+AnthonyMorris/posts/EgwbPYh29EV)


---
**Don Kleinschnitz Jr.** *October 28, 2018 11:51*

**+HalfNormal** in the google world all the media I create whether on the laptop or mobile device is directly shareable from its source location. I take a picture in the shop I can post it from my phone to any where in any G product including all office products, blog, G+ etc . I do not have to upload and maintain other libraries, formats, sizes etc. I get alerts to my phone rather than more emails .... I could go on. 



I also do not want to access communities via web pages from my phone, to clunky to navigate.



FB does not avail me all of this but it is better than most forums I have used in this regard.  



Certainly there is no right-wrong here just preferences. My opinion does not intend to dis on your excellent forum work that is serving the community by collecting community info into one location :)


---
**Don Kleinschnitz Jr.** *October 28, 2018 11:59*

**+Stephane Buisson** I am still betting that Google will be back with something. I cannot imagine that they will exit the social networking market all together, there is to much search data there. I think that this abandonment is their excuse for an exit to another technology. Security is a problem as long as you are on a network with more than one human and all their products have this kind of problem so why give up this source of information!


---
**Don Kleinschnitz Jr.** *October 28, 2018 12:31*

**+HalfNormal** how do I get to your forum from my mobile?


---
**Don Kleinschnitz Jr.** *October 28, 2018 12:33*

I did not notice that Redditt was mentioned as an alternative anyone familiar with it in regard to what we are looking for?


---
**Stephane Buisson** *October 28, 2018 13:35*

**+Don Kleinschnitz Jr.** up to next August, we have plenty of time. it's urgent to wait ;-))

I think you are right to say, Google could bring something new in the mean time.



But it would be lovely to be able to back up all the contents at once, I haven't find a good way yet.


---
**Jamie Richards** *October 28, 2018 16:17*

I really miss the BBS days. lol


---
**HalfNormal** *October 28, 2018 16:44*

**+Don Kleinschnitz Jr.** Just go to the site from a mobile browser.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/8gSciBuheip) &mdash; content and formatting may not be reliable*
