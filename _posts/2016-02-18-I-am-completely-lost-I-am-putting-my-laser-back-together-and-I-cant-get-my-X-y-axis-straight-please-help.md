---
layout: post
title: "I am completely lost I am putting my laser back together and I can't get my X y axis straight please help"
date: February 18, 2016 23:51
category: "Discussion"
author: "3D Laser"
---
I am completely lost I am putting my laser back together and I can't get my X y axis straight please help





**"3D Laser"**

---
---
**Joseph Midjette Sr** *February 19, 2016 00:14*

For one, you need to make sure you get the exact same measurement from corner to corner, upper left corner to lower right corner, lower left to upper right...that will square it up...


---
**3D Laser** *February 19, 2016 00:28*

The axis are square but I'm more worried about it being square to the rest of the machine 


---
**Scott Marshall** *February 19, 2016 00:58*

Good timing, I just did mine last night as I had it apart..

It's still fresh in my mind.



What matters is that it's square to the beam coming off the mirror at the back corner. (there's nothing in the box accurate enough to mechanically square up to - it all depends on the laser tube and back mirror - both of which you probably didn't move?



Get it all together, but leave the 4 bolts with 10mm heads that hold the frame in loose.



Here's MY method for squaring it up, there are a lot of methods:



Put a piece of masking tape lightly over the 1st flying mirror (the one on the left side of the rail) you only want the adhesive to stick tot he perimeter, don't let it touch the mirror (it will span it - the mirror is recessed).

move the rail to the rear, set laser power - just start at zero and bump it up while hitting the test button for a very brief pulse until it just leaves a mark. Smaller is better 

Move the carriage to the front of it's travel and repeat.



The marks should me aligned horizontally (one directly above the other), you'll do the vertical adjustment later. 

Visualise or draw a sketch of the laser path and move the frame clockwise or counter clockwise about enough to compensate , snug up a couple of the easy to get to bolts, repeat. Imagine the beam traveling to the left and right to fiqure out which way to move. 



The Rules: You're just trying for PARALLEL. You have to adjust the angles for parallel, and then move the elements to center the impact point. You'll chase your tail forever if you use the angle adjustments to control centering. Centering will require loosening and moving the bases. Don't move them unless you are done getting it parallel, and it's missing the mirror (or you CAN"T get it parallel without it missing the mirror)



Keep doing this until you have the 2 dots about centered (not critical), and stacked precisely one above the other (the important part).



I find using shims (I use scraps of 1/8" plywood) between the right front corner of the frame and the case side makes it easier to get it tightened without moving. Tape them in place if tension doesn't hold them.



Once you have it adjusted, tighten the frame down, and  next adjust  the back corner mirror to get the dots right on top of each other vertically and near the center of the mirror (less critical than you'd think) continue using the same process on the X axis.



Once you get a mental visualization, this moves faster than you'd expect. It's very much like adjusting a rifle scope. 

Change tape often and clean mirrors if you get them dirty. Use Brake clean (my choice) or acetone. (no carb cleaner, it has oil in it) on cotton q-tips and wipe dry with the other end.



Once you get the Y axis done move on and do the same to the X axis. 



Good Luck!









﻿


---
**Joseph Midjette Sr** *February 19, 2016 01:00*

Can you take a picture and show me what concerns you or elaborate a little more on what is out of line?


---
**3D Laser** *February 19, 2016 02:31*

I am so frustrated I can not get the mirrors aligned to save my life I think I really screwed myself by adjusting the first mirror to much.  I was trying to get it just center and I loosened the set screws and now I can't get the beam to even touch the second mirror. I almost feel like I have a 300 dollar paper weight at this point


---
**Scott Marshall** *February 19, 2016 06:24*

Relax, it's fixable. Once you get used to it, this will be easy stuff. Just don't panic.



Just like the riflescope, if you can't hit paper move in. And use bigger paper. 



Put a sheet of printer paper in between the rear mirror (the one in the laser compartment) and the carriage. 

Take a shot.The hole will guide you.  



then, once you get it so it looks about like it ought be close to the moving mirror, put a nice big piece of 2" masking tape or a post it note on the moving mirror and slide the X rail all the way back, fire and adjust. 

Once you get it on the mirror up close, slide the carriage toward the front and keep narrowing in in. 



It WILL come together, I promise. You did not wreck your laser.



I'll stay with you until you get it.



I'm not sure, but I think you mentioned you are in Buffalo? If you are in a place where you can call from, I can talk you through the worst of it on the phone if you like.



I'm disabled and up all hours, so if you want to talk about it, email me, and I'll give you my phone number. 



But if you stay calm and work the problem, it will make sense and come together.



Some rules of thumb that may help:



Any screw turned countercloclwise will move the beam toward it.



Move the top 2 screws equally to raise/lower the beam



Move the outside 2 screws to move the beam left/right.



Keep about 1/8" gap in between the moving plate and the fixed plate - don't do all your moving by loosening or tightening, if you need to move a lot, tighten (cw) one screw  1 turn, and loosen (ccw)the opposite screw  1 turn.



Be careful, if you get it WAY out of adjustment it WILL fire the laser over the housing and hit something in the room. If in doubt, put a book or board or such where the laser (or last working mirror) is pointed ( or close the lid) - the main concern is the mirror in the laser compartment, if set very high , it can shoot you standing in front of the unit. It's a longshot, but possiible. Just keep it in mind.



If you have to start from scratch, go right back to the laser tube. put a paper in front of the tube, fire, confirm it's firing. then put tape over the hole and fire off the mirror into that. From there go back up to my previous instructions. (this is just to make sure you indeed have a beam, and will give you a feel for where it's pointed.



Relax, it's no more complicated than a erector set. You'll get it.





Scott594@aol.com



Scott






---
**3D Laser** *February 20, 2016 22:50*

Finally got the beam to hit the second mirror again.  It's about a 1/4 of an inch off as it moves down the rails so I still got some fine tuning to do but it's getting there!  






---
**Scott Marshall** *February 21, 2016 09:39*

Cool.



I figured you'd get it.



My 1st time took about  2 hours just to get it working after the shipping damage, last time was taking it apart for mods, like you are. (I just cut the floor out to allow a drop away/adjustablet table so I can engrave toolboxes and large objects) and it only took about 15 minutes to have it zeroed in again. 



It gets pretty routine once you have a feel for it.

 

The 1st most important thing is getting the beams parallel to the motion so it doesn't shift when the carriage moves, and the 2nd thing is getting the head adjusted do it's right down the middle of the air assist. you can live with it off here a tad, but it really has more power and cuts better (same on X as Y etc) if it's going through the center of the lens.



 If you get the 2 of those down, it really doesn't matter where it's hitting the mirrors as long as it gets there.



When I 1st got the laser, I was looking at all the fancy tools (stands, targets, laserpointer gadgets etc) on you-tube and figured you needed all that stuff. After the 1st session I realized a $2 roll of tape and patience is all you need. Like a lot of this semi-technical stuff people tend to make the job harder than it needs to be. 



If there's a trick to it at all it's: Start at the beginning, and go one step at a time. Don't be tempted to try to adjust it "all at once" 



(That seems obvious, but it's not while you're turning screws in a panic. It's good rule for most things)



Also, don't be afraid to go back to the beginning if you discover it's not right. It's faster than trying to work around a poorly set earlier stage.

And you cannot adjust for an improperly aimed laser tube with the mirrors. If it's the wrong height with all the beams parallel, you just have to face it and start over. 







Guess it's not for sale cheap after all?  Bummer.



Scott


---
**3D Laser** *February 21, 2016 22:12*

**+Scott Marshall** still struggling with it I might have to have your help here soon to get it all aligned 


---
**Scott Marshall** *February 22, 2016 02:52*

Sounded like you might have had it too. Bummer.

You can call me anytime you like.



Here's my breakdown another way: 

(I'm hoping I'll give you the magic piece of info you need if I keep detailing it)



#1 Check laser, confirm it's firing. Use paper at laser tube. 



#2 Apply masking tape to the mirror next to the laser tube. Don't get glue on mirror. Set power as low as will leave a mark. It should be pretty close to the center. If not adjust the laser tube mounts until it is.



#3 New target tape on the left hand moving mirror. 



{To be clear: You will be aiming AT the mirror on the moving rail by adjusting the stationary mirror in the laser compartment}



Move the target mirror in close to laser compartment mirror at 1st. (rail all the way back) Use large tape or paper to find the beam if it's way out. (Safety tip:Don't shoot yourself in groin here by standing in the line of fire)



#4 - (REPEAT)This section is repeated over until you have it done.

With the target mirror in close fire a shot. mark dot with sharpie. (limits confusion). Move target mirror to most distant point. Fire another shot, note direction of error. 



(we are only concerned with side to side error right now - as long as the hits are both on the tape)



Visualize the path laser is taking, and how you will have to move it to get it perfectly straight. Think in 2D right now. Draw a cheat sheet if yo like. 



Adjust the Laser compartment mirror with the 2 screws on the side. Move BOTH screws the same amount. Tighten them to move the beam toward the outside cabinet wall (increases angle - more open or obtuse), loosen to move it toward the cutting area (decreases angle - tighter or more acute). 1 turn is around 1/2" at full distance. Mark the screw heads with sharpie to keep track.



 Adjust a little, Change tape, Fire your 2 shots again (go to my REPEAT statement again until it's shooting the holes exactly one above the other)



#5 Ok, you have it aligned left/right. Now to adjust the up down. Same process as before EXCEPT we are now trying to get 2 dots at the exact same height (side by side) - (ideally 1 dot, but maybe not) We will now adjust the two TOP screws on the Laser compartment mirror. Repeat the aiming process until you have 2 dots side by side or (ideally) 1 dot. 



#6 If you have 1 dot, it's time to the next mirror set.. If you still have error (sometimes adjusting one throws off the other axis), go back to the begininng and "touch-up" the adjustment - it should be VERY close at this point, and only require tiny adjustments. 

DO NOT move on until it's perfect. You should fire 1 dot in close, move all the way out, fire a 2nd, and it should be right thru the 1st hit. Don't Move on until it's perfect.

Now that it's perfect, gently snug down the 3 jam nuts on the laser compartment mirror, and re-check to verify you didn't move anything. Fix it if you did.



IF you couldn't get this to work, the laser tube may be crooked.You will know if the mirror won't adjust far enough - doubtful though.

 Let me know and I'll help you get it adjusted. It's pretty forgiving though. (due to the length it's hard to get it too crooked horizontally at least)



#7 to be continued next post. (bet you can figure out what's next......






---
**Kelly Burns** *February 23, 2016 04:10*

Great work Scott.  Very nice of you to help.  Not sure if this will help visualize things a bit better, but I found this on another forum.  [http://www.filedropper.com/k40-alignment-instructions](http://www.filedropper.com/k40-alignment-instructions)


---
**Scott Marshall** *February 23, 2016 05:32*

Thanks, Kelly

And thanks also to Dr TJ Fawcett, author of the instructions. Nice work.



Been there myself, so I help when  and if I can. I'm afraid I didn't get the teacher gene.. Explaining things is tougher than doing them in many cases.



Dr Fawcett uses pretty much the exact same method as I "invented" - there's a name for that, concurrent development or something like that. 



Anyway, Hope it helps Corey - It looks good to me, and has good illustrations and pictures.



I've been there, Corey just needs the "magic" piece of info for it all click. I've been throwing a bunch at him hoping he reads one of my "hints" and says" Oh Yeah!! THAT"S how it goes!"



Sometimes it takes me a long time for the "flash of understanding" to hit me. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/G5EctjDPjXK) &mdash; content and formatting may not be reliable*
