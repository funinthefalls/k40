---
layout: post
title: "Just ordered some of this as a new cutting bed"
date: May 18, 2015 12:23
category: "Discussion"
author: "David Wakely"
---
Just ordered some of this as a new cutting bed. Ive seen similar used in Trotec lasers and I'm hoping it improves cut quality. Ill keep you posted on how I get on with it!





**"David Wakely"**

---
---
**Chris M** *May 18, 2015 20:10*

Polypropylene? Won't you just cut right through it?


---
**David Wakely** *May 18, 2015 20:55*

I did wonder about that, I was hoping that the beam would be far enough away that it would be unfocused and not cut through it. I guess we will just have to wait and see! I'm hoping I won't cut through it!


---
**Jim Root** *May 18, 2015 21:58*

Not too sure about that. I've got a hole in my fest from the laser cutting through the material and finding that hole in the bottom of the laser cabinet. 


---
**Jim Root** *May 18, 2015 21:58*

Darn auto correct. Desk not  fest


---
**David Wakely** *May 18, 2015 22:06*

I guess I'll keep my eye out for a aluminium one but they seem pretty hard to find... Hopefully I don't slice right through the plastic but it more than likely will. I consider a cutting bed as a consumable anyway!


---
**Stuart Middleton** *May 19, 2015 11:50*

Ha! I have nice burn marks in my desk too where I cut directly above the hole in the bottom :)


---
**Chris M** *May 19, 2015 20:34*

David - [http://www.easycomposites.co.uk/products/core-materials/6mm-aluminium-honeycomb.aspx](http://www.easycomposites.co.uk/products/core-materials/6mm-aluminium-honeycomb.aspx)



or look at ventilation grids.


---
**David Wakely** *May 19, 2015 22:10*

I will give it a go, I'm gonna wait until I break the plastic stuff first, I'm sure it won't be long lol.


---
**Jim Root** *May 20, 2015 09:22*

Would the aluminum screen for a kitchen range hood work?


---
**Chris M** *May 22, 2015 08:11*

I'd be concerned about reflections from the aluminium damaging the underside of the workpiece.


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/e9zY9VgidkD) &mdash; content and formatting may not be reliable*
