---
layout: post
title: "I joined this community because I am thinking about buying a K40"
date: January 16, 2017 20:33
category: "Discussion"
author: "Martin Dillon"
---
I joined this community because I am thinking about buying a K40.  I made a big mistake not doing this when I bought my 3d printer.  When joined afterwards I found out what a lemon it was.  

As I look around, I find lots of K40s that vary widely in price.  This is even true when looking at the same online store.  I have to assume that they are different revisions of the same laser cutter and they are passing off older ones at a lower price.  Does anyone have a recommendation for a place to buy one?  I was looking at this one: [https://www.amazon.com/CNCShop-Engraving-Machine-Engraver-Woodworking/dp/B012F0LKHE/ref=sr_1_4?ie=UTF8&qid=1484598256&sr=8-4&keywords=k40+laser+cutter](https://www.amazon.com/CNCShop-Engraving-Machine-Engraver-Woodworking/dp/B012F0LKHE/ref=sr_1_4?ie=UTF8&qid=1484598256&sr=8-4&keywords=k40+laser+cutter) 

Then adding glasses, air assist and honeycomb table.  If I go with air assist do I need a drag chain?





**"Martin Dillon"**

---
---
**Robin Sieders** *January 16, 2017 22:15*

I purchased that exact one from Amazon shortly after Christmas, I just received it last week, and can recommend the seller. When I got mine everything showed up in perfect shape, the exhaust hose had one or two little dents in it from the laser head sitting against it in transit, but aside from that everything was great. Took me a few evenings to align everything, and I had to shim the back right corner to bring everything into perfect alignment, but that's par for the course with these machines. I purchased the lightobjects air-assist head at the same time, so installed that as well. I don't currently have a drag chain, though I do think it would make a big difference. I'm using fuel line hose to bring air to the laser head, and it is much too thick, but with the normal clear tubing you normally see around here I think a drag chain would help keep it out of the work area, and from getting caught on anything. If you have any other questions just ask!


---
**Ned Hill** *January 16, 2017 23:12*

That's the typical base model for K40s at an average on the price range.  You can find other versions with a few more extras but the performance level would be basically the same.  I got a more recent version about a year ago for $375 shipped from an Ebay seller, but the prices fluctuates a bit depending on the time of the year.  Given the relatively small work area of the K40 you don't really have to have a drag chain for the air assist line, but it does eliminate the chance of overcutting your line on big cuts.  If you get the Light Object air assist head you will need to get a bigger diameter lens as the stock K40 lens is smaller than that needed for the air assist head.  It's worth getting the improved lens.


---
**Austin Baker** *January 17, 2017 05:22*

I just received this one last week and all is well so far. Being an Amazon Prime subscriber, it arrived in 2 days. Good luck!

[amazon.com - Amazon.com: Orion Motor Tech 12"x 8" 40W CO2 Laser Engraving Machine Engraver Cutter with Exhaust Fan USB Port](https://www.amazon.com/Engraving-Machine-Engraver-Cutter-Exhaust/dp/B01EJDH1BO/ref=sr_1_3?ie=UTF8&qid=1484630415&sr=8-3&keywords=k40+laser+cutter)


---
**Martin Dillon** *January 17, 2017 05:30*

I am curious as to how the table that comes with it works.  Do you need a honeycomb table and if I do get one, do I have to remove the existing one?


---
**Paul de Groot** *January 17, 2017 05:51*

Probably buy the cheapest one and convert it with one of the available conversion boards. I made one based on an arduino/grbl which works reliable over ine year now.


---
**Austin Baker** *January 17, 2017 06:22*

**+Martin Dillon** The table that comes with it isn't very impressive, but doesn't inhibit my learning/experimenting with the machine at this time. See here (skip to 4:21): 
{% include youtubePlayer.html id="0P1AdVUI4zU" %}
[https://www.youtube.com/watch?v=0P1AdVUI4zU&f&t=4m21s](https://www.youtube.com/watch?v=0P1AdVUI4zU&f&t=4m21s)


---
**Ned Hill** *January 17, 2017 13:02*

**+Martin Dillon** the table that comes with these machines is mostly crap.  The table height is right at the focus point and they have a small spring clamp in the middle of the table to hold your workpiece at the table height.   So if you want to laser anything bigger than the clamp will hold you have to set it on top of the table and the surface will then be out of the focus point.  For things up to 3mm in thickness it's probably fine but you will get back reflection burns on cuts.  A honeycomb bed will eliminate  the back reflections and will also not get a build up of burnt wood resin goo.  Most people end up replacing the bed with some kind of material that has holes/slots (e.g. honeycomb) coupled with something that allows the bed height to be varied.  


---
**Ned Hill** *January 17, 2017 13:32*

This was my initial setup for my laser bed.  Still using this simple setup but have since added a honeycomb bed.  [plus.google.com - Here is a quick and simple set up for a z-axis adjustable bed. This is…](https://plus.google.com/108257646900674223133/posts/hGcsG3Hav33)


---
**Robin Sieders** *January 17, 2017 15:17*

If you get the light objects head, it will also change your focal point, I have to raise 1/8" material up and extra 1/8" or so to get it into focus. The spring clamp is easy enough to remove, as it's only held in with 4 screws, then drops out of the rest of the table. You'll be left with a gap in the center of your table, but I just added a cookie cooling rack on top of the existing bed, which brings my material to the proper focal point as well as adding an air gap to cut down on flashback.


---
**Austin Baker** *January 17, 2017 18:43*

**+Ned Hill** Curious to know if the z-axis can be computer controlled, and whether any commonly-used software could take advantage of it to adjust height during a given job.


---
**Robin Sieders** *January 17, 2017 18:55*

**+Austin Baker** Not in its default configuration, you'd have to add a powered bed, and I believe a different controller in order to automate the bed.


---
**Ned Hill** *January 17, 2017 19:30*

**+Austin Baker** Like Robin said you would need a different control board (like the cohesion3d mini) since the stock boards only have 2 axis drivers.  Then you could use software like the open source LaserWeb to control all 3 axes.


---
**Austin Baker** *January 17, 2017 20:05*

**+Ned Hill** It seems like Cohesion3D is mentioned a lot in the group -- is this the current "go-to" replacement board for the K40? There seem to be a wide range of prices for replacement controllers.


---
**Paul de Groot** *January 17, 2017 20:14*

**+Austin Baker** there are a few options but most require some tech savvy insight. For tinkers thats fine and what they want. For non tech savvy its hard. I have put together a combo that's cheap and works, just plug and play the cables and work with open software. Let me know what you are after and then we can all advice you.


---
**Austin Baker** *January 17, 2017 20:55*

**+Paul de Groot** I do consider myself 'tech savvy' and enjoy tinkering/DIY, but I simply want to get the best results from the K40 as I can (accuracy, versatility, quality). Whichever controller offers the best quality is the one I'd consider first.. I'd then look at prices to see if I need to accept some limitations with a more affordable board. Thanks!


---
**Paul de Groot** *January 17, 2017 20:57*

**+Austin Baker** no problems. I'm a big fan of tinkering too. Just ordered 10 boards from pcbway to play with. Let me know if you want to play with a board too. Will set a blog for background info for those interested. Cheers 


---
**Austin Baker** *January 17, 2017 21:19*

**+Paul de Groot** blank pcbs? That's probably more 'savvy' than I'm looking for, haha. 


---
**Paul de Groot** *January 17, 2017 21:22*

**+Austin Baker** no, it is  completely assembled with grbl software installed. I'm just looking for some testers to see how it goes. 


---
**Ned Hill** *January 17, 2017 21:23*

**+Austin Baker** see the Cohesion3d mini developed by **+Ray Kholodovsky** one of the mods here.  It's basically a direct drop in replacement for the K40 board.  There is a separate G+ community here for the board.  [https://plus.google.com/u/1/communities/116261877707124667493](https://plus.google.com/u/1/communities/116261877707124667493)


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 21:27*

Thanks Ned. Austin - any questions let me know. 


---
**Austin Baker** *January 17, 2017 21:32*

**+Paul de Groot** in that case, I'm happy to test it. Will help me learn more about these controllers and the machine


---
**Austin Baker** *January 17, 2017 21:34*

**+Ned Hill** **+Ray Kholodovsky**​ thanks! I'll take a look


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2017 21:35*

This is the link for the laser bundle: [http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**Paul de Groot** *January 17, 2017 21:38*

**+Austin Baker** just drop me a pm with your address so i can mail it when i get my uno r4's. Chinese new year now so l expect these in feb. 


---
**Austin Baker** *January 17, 2017 21:43*

**+Paul de Groot** are you referring to the Elektor R4? R3 is the last official rev to my knowledge. 


---
**Paul de Groot** *January 17, 2017 21:46*

I have a simplified version that less complex. Works on the same atmel libraries. I also used the 328pb xplained from atmel but the footprint is a bit difficult. 


---
**Austin Baker** *January 19, 2017 06:16*

Howdy. Address is 721 Ash St. Vandenberg AFB,  CA 93737



Should I place an order for an R4?



-Austin


---
**Paul de Groot** *January 19, 2017 11:01*

**+Austin Baker** no need to order an elektor r4 or atmel 328pb xplained sdk kit separately . The r4 is expensive to order from elektor. 24 euros plus 24 euros postage. I ordered 10 Home designed R4s from pcbway because i don't have smd assembly tools and i get a professional assembled board. I have already xy axis driver shields for it. You have two options: i can send you an atmel xplained board plus shield now or my customized R4 plus shield in feb. i rather send a complete new set in feb so you can test it for me. Just pay me the postage cost which I believe is around the 23 usd.


---
*Imported from [Google+](https://plus.google.com/116337441104029914683/posts/BYk8nVqPSsy) &mdash; content and formatting may not be reliable*
