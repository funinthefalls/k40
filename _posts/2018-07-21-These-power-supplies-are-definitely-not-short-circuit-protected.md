---
layout: post
title: "These power supplies are definitely not short circuit protected!"
date: July 21, 2018 08:49
category: "Modification"
author: "Jamie Richards"
---
These power supplies are definitely not short circuit protected! Installing a dedicated power supply for the Cohesion3D Mini, plus rewiring the unit with # 14-awg stranded wire from the # 20-whatever-wg they had coming in so I can add some more components and accidentally fried my power supply.   



I wanted to double check the positive 24v output before wiring in the second supply and someone in front of the house quickly revved the heck out of their motorcycle. I jumped and the multimeter probes briefly shorted across the positive and negative, and it seemed like it was ok, but then smoke came pouring out a few seconds later.  



Not the best way to discover they don't have short circuit protection. 



Being an electronics nerd, I quickly found the problem. A 7812 regulator was scorched, but luckily I had some new ones in my parts bin, replaced it and the power supply seems fine now.   Old vs new pics. 



![images/c568e7f51dc11c63cdd5e7d163e7f2e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c568e7f51dc11c63cdd5e7d163e7f2e6.jpeg)
![images/252a64d64b07bbbf3668e2775b177a17.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/252a64d64b07bbbf3668e2775b177a17.jpeg)

**"Jamie Richards"**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 21, 2018 13:53*

Which psu is this that you fried? The laser psu or the separate 24v psu? 


---
**Jamie Richards** *July 21, 2018 13:55*

Laser.


---
**Jamie Richards** *July 21, 2018 13:56*

The 24 has protection, the K40 it would seem does not.


---
**Ray Kholodovsky (Cohesion3D)** *July 21, 2018 14:36*

Sounds accurate. 


---
**Jamie Richards** *July 22, 2018 04:15*

The mains wiring on my unit were not North American (110v/120v) adequate in my opinion.  I have everything connected and working now.  My secondary supply for the C3Dm is a 15 watt unit, which may be overkill, but I want to add some more things.  Old vs new mains wiring. 

![images/4d64a9c8395ca6cefb7c8895134e3594.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d64a9c8395ca6cefb7c8895134e3594.jpeg)


---
**Don Kleinschnitz Jr.** *July 22, 2018 15:07*

Its strange that you blew the 12V regulator as it is only used to power the internal Hswitch drivers for the HV, its output does not leave the supply? The input of that regulator is connected to the 24V output so maybe something blew its input. Glad that fixed it though...



Your secondary supply is 15W? If that is a 24V supply that does not sound like enough to drive a K40?


---
**Jamie Richards** *July 22, 2018 16:02*

I thought that was strange as well, but luckily nothing else went.  When the smoke started pouring out, the red LED started blinking. 


---
**Jamie Richards** *July 22, 2018 16:04*

I meant 15 amp lol.  Sorry about that.  It's just for the C3Dm and anything else I may want to add in the future like more strip lighting, Z table etc. 


---
**Jamie Richards** *July 22, 2018 21:10*

This isn't the first time I've had a situation with this supply.  I was having intermittent problems and discovered a leaking cap, so I replaced them both.  Looks like it was damaged and they used it anyway.

![images/f9136504127651c56aa101d43db30494.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9136504127651c56aa101d43db30494.jpeg)


---
**Paul de Groot** *July 25, 2018 02:28*

The big schottky diode behind the 12v regulator provides the 24v. If that blows then the 12v circuit looses power. The 12v is only used to feed the pwm chip to generate the high voltage. So definitely something strange going on here. Maybe and out off spec 7812?


---
*Imported from [Google+](https://plus.google.com/113436972918621580742/posts/fuaJWGWaqnR) &mdash; content and formatting may not be reliable*
