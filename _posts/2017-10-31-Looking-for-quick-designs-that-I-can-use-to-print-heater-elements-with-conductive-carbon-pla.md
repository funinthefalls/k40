---
layout: post
title: "Looking for quick designs that I can use to print heater elements with conductive carbon pla"
date: October 31, 2017 04:52
category: "Discussion"
author: "Andrew ONeal (Andy-drew)"
---
Looking for quick designs that I can use to print heater elements with conductive carbon pla. I've never been one good with design. Wanting to use the heater elements for hand/foot warmer circuits. Would be great to find one that follows fingers and another more rectangular. May also try activating graphene oxide with the k40 to utilize same designs and purpose. All I've found so far was the following image. Thanks in advance for any suggestions 😀

![images/a7cf6d91c91e50053bdecc4a79b383bd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a7cf6d91c91e50053bdecc4a79b383bd.jpeg)



**"Andrew ONeal (Andy-drew)"**

---


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/ezvq6Chb1Lk) &mdash; content and formatting may not be reliable*
