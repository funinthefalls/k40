---
layout: post
title: "Good Day Everyone! My name is Kris and I'm currently looking to purchase my first laser cutter"
date: September 24, 2016 15:12
category: "Discussion"
author: "Kris Sturgess"
---
Good Day Everyone!



My name is Kris and I'm currently looking to purchase my first laser cutter.



I'm looking at the K40 or possibly the 50W version.

I've been reading and reading trying to be prepared as much as possible. I like the fact that there is lots of help/info here for the K40 but not much on the 50W versions.

I understand that upgrading/tweaking and fixing will be involved, that doesn't scare me as I'm a tinkerer/maker kinda guy.



Does anyone know of a US re-seller of the K40 or 50W that ships to Canada? or better yet a Canadian seller? Figured I'd ask here first before bombarding them all with the question. So far this is the only hiccup I'm having regarding my purchase.



I can get one out of China but they also want $360 for shipping.



Anyhoo, I'll be lurking around here some more.



I look forward to spewing my newbie questions to you all!



Kris

Red Deer, AB





**"Kris Sturgess"**

---
---
**greg greene** *September 24, 2016 15:32*

Howdy !



Look for an ad on Ebay that says US Seller or Shiipped from the US



Have them deliver it to  a parcel depot on the Montana Alberta border - 

Drive down from Red Deer and stay the weekend, go out to dinner - enjoy the weekend.  you will then have stayed long enough to not pay duty/taxes/gst when you bring the unit back.  That savings, plus tanking up on cheap gas, plus the much lower shipping cost will get you your machine at the cheapest price.




---
**Scott Marshall** *September 24, 2016 17:42*

I just bought my 2nd one from Globalfreeshipping on Ebay. They have several warehouses in the US - one in California (which I believe is the incoming port) and now a new one in Nebraska. I know they operate under other names as well on ebay, and would not be surprised if they have a Canadian branch. My new laser arrived in 4 days to central NY fedex ground. $315us Freeshipping (as the name suggests)



There seems more and more people looking into the 50W and larger machines in general, as prices continue to fall. I'd imagine support for them will build as we see more users. I would have gone for the 50w one, but needed a K40 specifically as I'm using it to test accessories.



Good luck and let us know when the new addition arrives.



Scott.






---
**Kris Sturgess** *September 24, 2016 20:08*

Thanks Greg & Scott,



Greg,



I'm going to keep the cross border option as a last resort. It will still be expensive but will do it if I have to.



Scott,



I have an e-mail off to GlobalFreeShipping to see what they have to say. Maybe I'll luck out.



Kris


---
**greg greene** *September 24, 2016 21:46*

Good luck, I live in BC just North of the Idaho border - so it is a lot easier for me to nip down and get something.


---
**Kris Sturgess** *September 26, 2016 20:45*

Well, I've got the OK from the Boss (Wife) to order my K40 next week. 



In the mean time I'm reading and looking for upgrade parts. I think I'll be doing the LightObject Air Assist head w/ 18mm focus lens to start. Anything else I should order from there while i'm at it? (Table and rotary are upgrades for down the road) spare parts?



Plan is to get the machine up and running basically stock and slowly upgrade as I go.



I am looking at upgrading the controller (Smoothieboard? others?) as well  but I'm still reading up on that process for now.



I think the K40 will be a good starting point for me. I'm actually looking forward to tinkering and upgrading it.



I'm open to any suggestions/comments if you've got them.



Also looking for any Star Wars files if you care to share. I have a few off of Thingiverse.



Kris


---
**greg greene** *September 26, 2016 22:13*

The mirrors that came on my machine seem to be of good quality - so I haven't ordered new ones yet,  I got the LO head and lens - both good quality - you may wish to check aliexpress for a mount and led pointer for that - it goes on the bottom - not the top like I tried - and has a 5v led to mark the origin easily you get that off the Pwr supply - right hand green block last two pins on the right. i turfed the rediculous spring loaded work holder and got a 4X4 adjustabel lab table from Ebay for less than 15 bucks - I put a chunk of mesh on it and it works great as an adjustable z axis table to make sure you get the best focus.



An aquarium chiller rounds out the upgrades - and yes - it does make a measurable difference.





Good luck and don't forgett the daily meetings of Laser anonymous on the K40 site :)


---
*Imported from [Google+](https://plus.google.com/103787870002255592759/posts/8JURcuixLo3) &mdash; content and formatting may not be reliable*
