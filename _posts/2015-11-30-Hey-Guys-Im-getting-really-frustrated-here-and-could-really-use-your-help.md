---
layout: post
title: "Hey Guys, I'm getting really frustrated here and could really use your help.."
date: November 30, 2015 00:19
category: "Hardware and Laser settings"
author: "Tadas Mikalauskas"
---
Hey Guys, I'm getting really frustrated here and could really use your help.. So for a few days now I am having this problem with alignment.. I am pretty sure it is not mirrors that cause the problem which is that 3 out of 4 corners (top left, botom right and left) hits the same spot (in the picture it would be the one a bit higher to the right) and one last point at the top right goes off by a millimeter ar two to lower left. And of course if I try to get the laser to the lower left, the difference always stays. As I said, it's pretty obvious that at this point, the mirrors should not be the problem, it's a problem more like in the axis and I tried to lift bottom right  thinking that the top right is a bit higher and to make it in the same hight but nothing changes. Maybe I am using wrong methods to get everything in a "square", but I can't find any good tips on the internet for that. Maybe some of you could help me on this, because I'm getting out of patients on this one now.. Thanks in advance!



![images/56afa9d9aa6a500649228a4b2d782969.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/56afa9d9aa6a500649228a4b2d782969.jpeg)
![images/60901eda1536c4cedec83a4254e40a36.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/60901eda1536c4cedec83a4254e40a36.jpeg)

**"Tadas Mikalauskas"**

---
---
**Sean Cherven** *November 30, 2015 00:40*

Looks like the same thing mine was doing. I had to loosen the belts, and jump about 2 or 3 cogs.


---
**Scott Thorne** *November 30, 2015 01:09*

**+Sean Cherven** is right, mine was out of square I had to jump the right side belt 3 teeth to get the carriage square, it's aligned perfectly now.


---
**Tadas Mikalauskas** *November 30, 2015 07:57*

Will try this today, thanks!


---
**Phillip Conroy** *November 30, 2015 08:22*

Thats close enough,does  ot need to be perfect


---
**Stephane Buisson** *November 30, 2015 08:53*

not related to your problem, but I just notice the last mirror/lens assembly is not vertical (1st photo)


---
**Tadas Mikalauskas** *November 30, 2015 11:48*

Belt loseing and jumping a fewsteps, doesn't seem to be working yet.. Gonna try to work on it more..


---
**Tadas Mikalauskas** *November 30, 2015 21:11*

One more day with no success.. I had all frame out, checking, tighting, jumping the belt a bit and so on... can't get it right yet..


---
*Imported from [Google+](https://plus.google.com/113213933687083540785/posts/G2C4pfQQgSu) &mdash; content and formatting may not be reliable*
