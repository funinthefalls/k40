---
layout: post
title: "Hello to all... i am new to laserDRW 3 - i am trying to make this figure to cut in wood but i am not able to do this form..."
date: March 10, 2017 21:54
category: "Discussion"
author: "ATELIER DU DCC"
---
Hello to all...



i am new to laserDRW 3 - i am trying to make this figure to cut in wood but i am not able to do this form...



Is there any other way of do it if it is not possible in laserDRW



Can someone help me?



Best regards from Belgium,



S. Torres

![images/c4db75ca74c854aad221b2daaa009914.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c4db75ca74c854aad221b2daaa009914.jpeg)



**"ATELIER DU DCC"**

---
---
**Mark Brown** *March 11, 2017 01:57*

Well, how accurate do you need it?



This is made in CorelDraw.  The black is your sketch that I traced over in orange.

![images/50f510e05eb19068cf4f2190277ffca3.png](https://gitlab.com/funinthefalls/k40/raw/master/images/50f510e05eb19068cf4f2190277ffca3.png)


---
**Mark Brown** *March 11, 2017 01:59*

And this is in LaserDRW.  Once again I opened your sketch and used it to get the sizing.

It's a circle in each corner, a thick line on each side, and 3 shapes filling the center.



The laser will just cut around the outline.

![images/f51827c64fcd9e00679c6ebb59bd9e7c.png](https://gitlab.com/funinthefalls/k40/raw/master/images/f51827c64fcd9e00679c6ebb59bd9e7c.png)


---
**ATELIER DU DCC** *March 11, 2017 12:47*

Thank you very much for you work and explanation....

only now i realize how t do it.... Best regards and once again THANKS


---
**Mark Brown** *March 11, 2017 15:53*

No problem.  Glad I could help.


---
*Imported from [Google+](https://plus.google.com/+ATELIERDUDCC/posts/dSnTCJdJVdu) &mdash; content and formatting may not be reliable*
