---
layout: post
title: "Hi, I've just received my laser k40"
date: August 24, 2016 18:48
category: "Discussion"
author: "RAINON Herv\u00e9"
---
Hi, I've just received my laser k40.

I want to make objet with leather 1,5 mm thick. Is someone knows the settings for engraving, marking and cutting. I've only worked in Fablab with Trodec laser cut machine, I'm a little lost. For example if is it possible to cut engrave and mark at the same time.... And how to do this. Thanks a lot

Hervé





**"RAINON Herv\u00e9"**

---
---
**Alex Krause** *August 24, 2016 20:13*

**+Yuusuf Sallahuddin**​


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 25, 2016 01:17*

Hey RAINON. I have done quite a lot with leather using my K40.



In regards to 1.5mm thick leather, you will find that engraving needs to be done with very low power (I use 4mA) & very fast speed (I was using 500mm/s). Any more power or slower speed will result in it burned all the way through. However, you can increase the pixel steps (default is 1, change to 3 or 5) to reduce the density of the "dots" when engraved which will reduce the issue with burning all the way through.



I highly suggest testing on scrap first. Also, check through my posts as a lot of times I have posted the exact settings I used for my engrave/cut jobs.



I am not 100% sure what you mean for "marking". Is there a difference between engraving/marking?



In regards to cutting, 1.5mm leather should cut reasonably easy. I was using 4mA @ about 10mm/s. (I like to use low power as it reduces the width of the cut & reduces scorching). You may need 2 passes to get through.


---
*Imported from [Google+](https://plus.google.com/113663989140304495854/posts/fPdd5EfiW71) &mdash; content and formatting may not be reliable*
