---
layout: post
title: "I have had my K40 for a few months now and totally love it.."
date: December 19, 2014 22:26
category: "Discussion"
author: "Other World Explorers"
---
I have had my K40 for a few months now and totally love it.. well, that's not entirely true. I need more precise control for what I do and the stock components and software are lacking. So I am doing the DSP upgrade. I just need to sort out my PSU. I don't know if it is up to the task. I am in communications with the owner of Light Object. So far GREAT! will keep ya posted as events transpire. Pics of upgrade to follow once the PSU is sorted. 





**"Other World Explorers"**

---
---
**Stephane Buisson** *December 20, 2014 16:46*

Welcome here J Rowell, it will be a pleasure to read your post.


---
**Other World Explorers** *December 26, 2014 07:19*

Thanks for the welcome! 

Thus far still in holding pattern for the PSU. I do have a few things I have learned to save money I'd like to share. 



Instead of using honeycomb I use metal screen. dirt cheap at Lowes or Home Depot. In the section of Windows and doors. I just fold it over till I get four layers. Tape the ends down and there ya go. Works great. 



For water cooling I like to use distilled water. This helps prevent any metal parts in the pump that comes in contact with the water from rusting. To chill said water I am currently using 2 liter bottles of frozen water. I have 3 at the moment I rotate thru. These melt slower than ice. Can be refrozen and are much cheaper. I am not worried about thermal hot / cool spots as I do not change while cutting or run the laser that much to make a difference. If you are concerned just give it a few min to stabilize. 



i am currently working on a compressed air system and better fume extraction (my vent fan that was provided seems to suffer from asthma or something). I will post what ever crazy solution I come up with. 



hope these tips help or inspire you! 



James 


---
**Mauro Manco (Exilaus)** *January 01, 2015 14:13*

Guys only one question cutting area is really 300x200 or smaller? 


---
**Stephane Buisson** *January 01, 2015 14:26*

a bit more than that, fit A4 size. (flat material)

for thickest object or  material to be cut/engrave, the "windows hole" with the springs  holder the object is smaller about 9x20cm. not much use of it, this just for positionning, and would be easily hackable as for your need.



With Smoothie/visicut, the K40, will become like a inkjet printer, but being able to cut engrave A4 size.   ;-))


---
**Mauro Manco (Exilaus)** *January 01, 2015 14:53*

I know this only for understand...soon I release laser cut carriage for my wheels and openbuild enter full set on a4 a sheet for this nice to know k40 can manage it. 


---
*Imported from [Google+](https://plus.google.com/+LittleSteampunker161/posts/WuCxhTK2MgG) &mdash; content and formatting may not be reliable*
