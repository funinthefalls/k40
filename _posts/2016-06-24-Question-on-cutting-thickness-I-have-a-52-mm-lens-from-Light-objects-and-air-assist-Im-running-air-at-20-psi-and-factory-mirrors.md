---
layout: post
title: "Question on cutting thickness. I have a 52 mm lens, from Light objects and air assist, I'm running air at 20 psi, and factory mirrors"
date: June 24, 2016 13:47
category: "Hardware and Laser settings"
author: "giavonni palombo"
---
Question on cutting thickness. I have a 52 mm lens, from Light objects  and air assist, I'm running air at 20 psi, and factory mirrors. My focal distance is 52 mm to the top of my material. Is 3.5 mms and 13 Ma sounds about right to cut through 3 mm Baltic birch ply? I cleaned the mirrors and lens and no change. The edges are very dark too. This to me seems to slow or to much power.





**"giavonni palombo"**

---
---
**Jean-Baptiste Passant** *June 24, 2016 14:05*

I do 3mms and 13ma to cut through 5mm plywood. Cut are straight and edges are not dark. Did you check the mirror aligment ?


---
**giavonni palombo** *June 24, 2016 14:13*

I have checked them, they are very very close but not perfect. In the top left dead center, bottom right its off a bit but still touching the top left mark. Hope that makes sense. Are you using factory mirrors?


---
**Jean-Baptiste Passant** *June 24, 2016 14:17*

No, you are right, forgot that !



I changed mirror and lens, I did not bother with the stock one actually and kept them for spare.



I bought HQ SI for the mirror and ZnSe for the lens.



Paid ~60€ for it, they may not be what they are called (china :/) but they work.


---
**giavonni palombo** *June 24, 2016 14:47*

Im an idiot. I checked my lens is a 50.8 so I off there some. No sure if that little bit could make a huge difference.


---
**Scott Thorne** *June 24, 2016 16:22*

You should be able to cut at 10mm/s...@15mA


---
**Jason He** *June 24, 2016 23:59*

I cut 3mm baltic birch at about 6 to 7 mA @ 5 mm/s with a stock K40.



I spent a ton of time on aligning the mirrors. I also cleaned the mirrors and lens by carefully wiping them with rubbing alcohol. Finally, I also flipped my lens 

which resulted in an immense improvement.



I'm quite sure the correct orientation of the lens is supposed to be concave side down, but I think mine worked better with it up. I'm not sure why, but it might be because I'm not at the correct focal point. I'll have to look into it later.


---
**giavonni palombo** *June 25, 2016 20:14*

I've ordered new mirrors should be here next week. In the meantime I'm going to check my lens and do a ramp test. I'll wait to check the alignment after my new mirrors arrive. Thanks for letting me know what your speed and power settings are. Ill let you know what I find out.


---
**Helce David Melgarejo** *June 25, 2016 22:18*

I have the same problem, you managed to fix it?


---
**giavonni palombo** *June 27, 2016 19:14*

No not yet Melgarejo. I believe my machine is running at 100% power around 13 mA on my meter. So I might have been less power but I dont think that's completely it.


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/MQMEtQDcsZh) &mdash; content and formatting may not be reliable*
