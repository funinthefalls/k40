---
layout: post
title: "In this video i discuss my LO water sensor, talk about some random \"sticky stuff\", and my plans to offer a cost-effective, easy, manual z table kit for the k40!"
date: January 10, 2018 23:52
category: "Modification"
author: "Tech Bravo (Tech BravoTN)"
---
In this video i discuss my LO water sensor, talk about some random "sticky stuff", and my plans to offer a cost-effective, easy, manual z table kit for the k40! Be sure to like, subscribe, and share!...
{% include youtubePlayer.html id="cT4rlxPx3aU" %}
[https://youtu.be/cT4rlxPx3aU](https://youtu.be/cT4rlxPx3aU)





**"Tech Bravo (Tech BravoTN)"**

---
---
**Timothy Rothman** *January 11, 2018 06:46*

Where did you get the 3M VHB PSA for $6 and do you have a part number?  



Keep in mind that it's pressure sensitive and takes about a day for full bond strength, after which you can mount mostly anything and you cannot remove with perpendicular (normal) loading- almost have to roll it off.  You can likely get a razer to cut the foam carrier if you need to remove and can't roll it.  Also there are various grades and surface energy classes depending on what you're bonding.



I've used many similar adhesives for work projects, but I can't seem to find it reasonably priced for personal use.



On the Z-Axis, I am 80% done on a remix of a remix manual Z axis from a Thingiverse design.  The original designs lacked locking features and a good tensioning mechanism.  Also my remix2 will use an off the shelf belt (don't want to be doing belt bonding or repair).


---
**Tech Bravo (Tech BravoTN)** *January 11, 2018 06:52*

Good to know. I will have to find the invoice and see where i sourced it from. I may be interested in the z table. I like seeing better mousetraps!


---
**Tech Bravo (Tech BravoTN)** *January 11, 2018 14:46*

oh wow, i was sorely mistaken. the big roll was not $6. It was more like $13. Not bad but not a steal either. i suppose i should fact check before making videos LOL




---
**Don Kleinschnitz Jr.** *January 11, 2018 15:57*

Can you point us to the pressure switch you used?


---
**Don Kleinschnitz Jr.** *January 11, 2018 15:59*

You know that I discourage users from relying on eye protection instead of interlocks :). 

On my blog is a design for a bracket for the laser compartment interlock.


---
**Tech Bravo (Tech BravoTN)** *January 11, 2018 16:00*

[lightobject.com - Water flow/ pressure sensor. Ideal for CO2 laser water protection](http://www.lightobject.com/mobile/Water-flow-pressure-sensor-Ideal-for-CO2-laser-water-protection-P815.aspx)


---
**Don Kleinschnitz Jr.** *January 11, 2018 16:00*

Take a look at the "Clamping table" approach also on my site. We completed the design but have parts but I have not had the time to build it....


---
**Don Kleinschnitz Jr.** *January 11, 2018 16:01*

**+Tech Bravo** Thanks....


---
**Tech Bravo (Tech BravoTN)** *January 11, 2018 16:01*

**+Don Kleinschnitz** i have interlocks on both front covers. The rear cover is screwed shut. Also the laser is in a secure area and not accessable without my explicit supervision


---
**Tech Bravo (Tech BravoTN)** *January 11, 2018 16:05*

i looked at the rendering of the table. cool stuff. i likey!


---
**Timothy Rothman** *January 11, 2018 16:22*

**+Tech Bravo**  I was finding VHB for more like $36 and I frankly stopped looking.  Your roll looked fairly wide and long so I wouldn't balk at even $13.  I worked with 3M on some of the development of a version for drop test for phones/tablets.  They even have 1 flavor that holds panels on box trucks and trailers.


---
**Timothy Rothman** *January 11, 2018 16:46*

**+Don Kleinschnitz**  You sound like me.  I spend a lot of time designing lack time for building.  Do you have the K40 in cad.  I'm looking for the stage/motors mostly. 


---
**Don Kleinschnitz Jr.** *January 12, 2018 11:28*

**+Timothy Rothman**  I have a basis model of the cabinet and a model of the optical path. 



Neither have the motor but the optical path is pretty accurate and used it for my alignment bench.



The cabinet is not that accurate but it was good enough to do my mods.



You can find my 3D models by searching K40 or K40-s on the SU "3D Warehouse"



[3dwarehouse.sketchup.com - 3dwarehouse.sketchup.com/model.html?id=385d6821-536b-413b-870e-5d4502ce1e0e](https://3dwarehouse.sketchup.com/model.html?id=385d6821-536b-413b-870e-5d4502ce1e0e)



[https://3dwarehouse.sketchup.com/model/ed8e0e14-e130-4fe5-b0c1-a3843a8a2a25/K40-S-Packaging](https://3dwarehouse.sketchup.com/model/ed8e0e14-e130-4fe5-b0c1-a3843a8a2a25/K40-S-Packaging)



[https://3dwarehouse.sketchup.com/user/0078146073701371293352481/turnedoutright?nav=models](https://3dwarehouse.sketchup.com/user/0078146073701371293352481/turnedoutright?nav=models)


---
**Timothy Rothman** *January 14, 2018 18:23*

**+Don Kleinschnitz** .  Thanks for the links.  I started some prints before I left work and haven't had any more time to look at the K40 dimensions you have in your models, but I think I have an off the shelf closed loop belt that will work nicely. I'm developing a nice tensioner also.  My intention eventually is to have most of the stuff worked out in cad before doing the prototyping because it makes the work overall easier and having full cad is definitely helpful.




---
**Timothy Rothman** *January 15, 2018 16:38*

**+Tech Bravo**  Actually you got me looking and I came across this VHB (not as wide though), but the price is excellent and although it may not have been what you bought, it's a good deal at $6. Must be a one time deal...



The specs are pretty good.  I think they're marketing to the automotive market and thus need to be price competitive.



[https://multimedia.3m.com/mws/media/1235572O/vhb-4941-4952-rp-technical-data-sheet.pdf](https://multimedia.3m.com/mws/media/1235572O/vhb-4941-4952-rp-technical-data-sheet.pdf)





![images/3b9ce644cae9d8b4b56aa229eb3bee0b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b9ce644cae9d8b4b56aa229eb3bee0b.jpeg)


---
**Tech Bravo (Tech BravoTN)** *January 15, 2018 16:41*

Awesome. Thanks for the link!


---
*Imported from [Google+](https://plus.google.com/+TechBravoTN/posts/3gQJBBsC2H3) &mdash; content and formatting may not be reliable*
