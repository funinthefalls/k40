---
layout: post
title: "Made some Star Wars ornaments from the free pattern from C4Labs"
date: December 26, 2016 07:17
category: "Object produced with laser"
author: "Tev Kaber"
---
Made some Star Wars ornaments from the free pattern from C4Labs. I modified the files to make them cut out completely (they leave bits uncut to hold it together like a kit - cute but annoying, the first one I tried broke a bit because of it). I forgot to engrave a few bits, only did vector scores and cuts, but looks pretty good anyway. 



![images/c31332af153c3e2a4db988b7c27ed140.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c31332af153c3e2a4db988b7c27ed140.jpeg)
![images/bf70e0494356395658eeeefbd6fc16c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bf70e0494356395658eeeefbd6fc16c6.jpeg)

**"Tev Kaber"**

---


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/4dbNeDKdEVN) &mdash; content and formatting may not be reliable*
