---
layout: post
title: "have a question. I have a K40 and I set it up and it worked fine, now all of a sudden the parallel arm will not go to the home position and the moshidraw states machine is busy"
date: February 05, 2017 02:10
category: "Discussion"
author: "John Stevens"
---
have a question. I have a K40 and I set it up and it worked fine, now all of a sudden the parallel  arm will not go to the home position and the moshidraw states machine is busy. any iseals what could be wrong?





**"John Stevens"**

---
---
**Paul de Groot** *February 05, 2017 02:18*

I speculate that the endstop is broken. 


---
**Andy Shilling** *February 05, 2017 08:59*

Have a look at the link in **+Don Kleinschnitz** post, It shows a possible solution to the problem you are having.



[plus.google.com - Added some part #s and models of the endstop daughtercards to the post: http...](https://plus.google.com/u/0/113684285877323403487/posts/Qk8pe4r7hd1)


---
**Don Kleinschnitz Jr.** *February 05, 2017 14:06*

**+John Stevens** you also can try out this remote testing procedure that I am experimenting with. 



[https://docs.google.com/forms/d/1YprXQPBrxHWU7UQEeOjRmWfN_VVFaascUTVQ7s2vJVs/prefill](https://docs.google.com/forms/d/1YprXQPBrxHWU7UQEeOjRmWfN_VVFaascUTVQ7s2vJVs/prefill)


---
**HalfNormal** *February 05, 2017 15:48*

**+Don Kleinschnitz** The Google Doc link is showing request denied need permission.


---
**HalfNormal** *February 05, 2017 15:50*

**+John Stevens** You can try the software mentioned in this link. One of my K40's has a Moshiboard which was not working correctly and this reset it. Another poster also said it fixed their board. I checked the software and it is clean.

[dck40.blogspot.com - Fix for broken Mainboard MS10105 v4.x](http://dck40.blogspot.com/2013/03/fix-for-broken-mainboard-ms10105-v4x.html)


---
**Don Kleinschnitz Jr.** *February 05, 2017 18:33*

**+HalfNormal** settings were wrong should work now :)


---
**Paul de Groot** *February 05, 2017 20:51*

**+HalfNormal** that seems like a serious software bug. Sort of a software lock situation which might be related to the dongle?


---
*Imported from [Google+](https://plus.google.com/108124828909436986786/posts/CsuUCb5LLMQ) &mdash; content and formatting may not be reliable*
