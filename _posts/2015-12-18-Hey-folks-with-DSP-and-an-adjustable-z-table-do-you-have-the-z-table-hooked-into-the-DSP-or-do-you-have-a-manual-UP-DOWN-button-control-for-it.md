---
layout: post
title: "Hey folks with DSP and an adjustable z-table: do you have the z-table hooked into the DSP, or do you have a manual UP/DOWN button control for it?"
date: December 18, 2015 23:13
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Hey folks with DSP <b>and</b> an adjustable z-table: do you have the z-table hooked into the DSP, or do you have a manual UP/DOWN button control for it? And if it's through the DSP, how does that work? Are you manually adjusting it, in software, buttons on the DSP, automatically?



Basically I'm trying to figure out how it works with the DSP. Can I, for example, store information in the DSP about different materials and when I pick, say 1/8" acrylic, it automatically adjust the bed, then when i pick say 1/4" it automatically adjusts again?





**"Ashley M. Kirchner [Norym]"**

---
---
**Brooke Hedrick** *December 19, 2015 03:14*

I have the DSP from lightobject and the ztable.  I only just got everything working in the past couple of weeks.  I am using LaserCAD and I have no idea if it can automate the z table.



The control panel with the lightobject DSP has buttons for raising and lowering a ztable.  They work well.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/iy5Ns4tcYBZ) &mdash; content and formatting may not be reliable*
