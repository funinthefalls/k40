---
layout: post
title: "Gutted my 50 watt today...installed the light objects adjustable tube mounts in it.....it turned out great....not bad for 70 dollar investment....makes dialing in the tube a breeze!"
date: March 11, 2016 00:06
category: "Modification"
author: "Scott Thorne"
---
Gutted my 50 watt today...installed the light objects adjustable tube mounts in it.....it turned out great....not bad for 70 dollar investment....makes dialing in the tube a breeze! 



![images/e292aba7788d3e80cccfe7079b85d606.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e292aba7788d3e80cccfe7079b85d606.jpeg)
![images/b3e6025e9ecba7af512d6811723cf0a6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b3e6025e9ecba7af512d6811723cf0a6.jpeg)
![images/4e670dacc7e15d0f87fd269f85a7f956.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4e670dacc7e15d0f87fd269f85a7f956.jpeg)

**"Scott Thorne"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 11, 2016 01:48*

That's really cool. It would be useful to be able to make minor adjustments to the tube.


---
**Scott Marshall** *March 11, 2016 01:53*

Look nice. I was thinking of upgrading to a larger diameter tube, and the mounts are one issue I didn't see a easy rout around - adds up in a hurry, tube, power supply, etc. 

I like those mounts, if not the price. (although they are probably worth it)

They could be made from aluminum tubing... I feel a lathe session coming on.


---
**ED Carty** *March 11, 2016 02:14*

Bitchin photos man. You seem to have this handled bro. Shame you are not in the Dallas area. 


---
**Scott Thorne** *March 11, 2016 10:48*

Thanks guys.....it's been down for a week....I just got it up and running last night. 


---
**Scott Thorne** *March 11, 2016 16:53*

**+Scott Marshall**...you can use a 2 1/2 inch lock collar to make one.


---
**Joe Keneally** *March 11, 2016 17:32*

My tube barely fits between the wall and the mirror!  I really need to tweak things a bit!


---
**HalfNormal** *March 11, 2016 17:44*

I have been looking at these

[http://www.ebay.com/itm/Reci-Low-Laser-Tube-Fixtures-for-Dia-80mm-Laser-Tube-of-CO2-Laser-Machine-/181832492844?hash=item2a560fa72c:g:4RcAAOSw34FVASHC](http://www.ebay.com/itm/Reci-Low-Laser-Tube-Fixtures-for-Dia-80mm-Laser-Tube-of-CO2-Laser-Machine-/181832492844?hash=item2a560fa72c:g:4RcAAOSw34FVASHC)

Low profile


---
**Scott Marshall** *March 11, 2016 18:12*

**+Scott Thorne** 

Thanks Scott, I thought that's what they looked like. 



The good news is I'm going to need them, I'm just bought a 100W Puri tube, 10k Hr. also 80mmDia.



LaserDepot has a sale on, couldn't pass it up. They have the rest of the line up on sale if you're in the market, today is the last day.



 I think I'm going to scratch build a large open platform set-up, with a combined router head and fume/dust evacuation. 



Your upgrade is looking good. I think I'm going to get the controls upgrade done on the K40 then decide if it's worth going to a larger tube, seeing as I just bought myself another project



Scott


---
**Scott Thorne** *March 11, 2016 23:37*

**+Scott Marshall**....lol...you must have been reading my mind...I ordered the 50 watt puri from laser depot when I got the email about the sale....225 versus 375 is a good deal.....it gets shipped next week.


---
**Scott Marshall** *March 12, 2016 05:59*

**+Scott Thorne** I didn't end up ordering it. 

I hate to have to tell you this, but the LaserDepot deal looks like a scam site. I spoke to John right after I posted the reply to you, and he was unable to take paypal "at the time" - I had already transferred my money into my account to buy the tube, so I asked him to send me a paypal invoice. 4 hours later he called me back and told me Paypal would delay his transaction 2 weeks because it was a seldom used account. I asked if he had a personal account, and to send me the address so I could pay. While I was waiting I got thinking, then checking up on Laser Depot. The news was not good. Turns out they are the reincarnation of Hurricane laser and several other dubious firms all owned by a John K. When the paypal address came through, the names matched. 



Anyway, I passed, with it all looking too risky.

I hope you're not stung. It's possible John K has a legitimate business, but the paypal thing scared me. Paypal is listed as a form of payment on the checkout page of the LaserDepotusa website, but they have an "inactive" account?



Here's what I found:





[http://www.sawmillcreek.org/showthread.php?234901-Laser-Depot&p=2463119&posted=1](http://www.sawmillcreek.org/showthread.php?234901-Laser-Depot&p=2463119&posted=1)



[http://www.cnczone.com/forums/complaint-and-praise-discussions/199700-software-9.html](http://www.cnczone.com/forums/complaint-and-praise-discussions/199700-software-9.html)



[http://www.apa21.org/2015/08/hurricane-lasers-review/](http://www.apa21.org/2015/08/hurricane-lasers-review/)



[http://www.bizapedia.com/fl/LASER-DEPOT-USA-LLC.html](http://www.bizapedia.com/fl/LASER-DEPOT-USA-LLC.html)



Theres more, if you look around.

Sorry for the bad news, and I'm hoping you get your tube, I'd be in for $750 if the paypal hadn't red flagged me.



Scott


---
**Scott Thorne** *March 12, 2016 10:36*

I bought a tube from him around 4 months ago and it was great....arrived well packed and worked great.....hope this works out.


---
**Scott Marshall** *March 12, 2016 19:48*

**+Scott Thorne** 

Thanks, Maybe I'll bite next time he has a sale. 

 Been burned like that before, so I'm maybe over sensitive.  I just couldn't afford to take a chance on that kind of loss.






---
**Scott Thorne** *March 12, 2016 22:52*

**+Scott Marshall**...now I'm worried....I read all the links and it sounds like the hurricane laser guy was robbing people....I've dealt with laser depot before and got taste very good....I just hope this time is the same way.....I'll keep you posted brother. 


---
**Scott Marshall** *March 13, 2016 06:17*

**+Scott Thorne** Thanks Scott, I sincerely hope I'm wrong.

 I AM sure it's the same guy. Maybe he's decided to straighten up and run an honest business. 



Got my fingers crossed for you.

Scott


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/ga4rFbaf6mQ) &mdash; content and formatting may not be reliable*
