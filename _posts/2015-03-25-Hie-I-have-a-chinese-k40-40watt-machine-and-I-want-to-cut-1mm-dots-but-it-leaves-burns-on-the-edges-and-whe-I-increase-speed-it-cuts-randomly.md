---
layout: post
title: "Hie I have a chinese k40 40watt machine and I want to cut 1mm dots but it leaves burns on the edges and whe I increase speed it cuts randomly"
date: March 25, 2015 08:57
category: "Discussion"
author: "Auther Chigwerewe"
---
Hie I have a chinese k40  40watt machine and I want to cut 1mm dots but it leaves burns on the edges and whe I increase speed it cuts randomly.





**"Auther Chigwerewe"**

---
---
**Bill Parker** *March 26, 2015 06:38*

Have got the power as low as possible? Have you got air assist?


---
**Auther Chigwerewe** *March 30, 2015 08:10*

Same with mine put the speed at 400mm/s with very low power also try putting a masking tape under and over the area you want to cut it pics up most of the ash.


---
**Dan Shepherd** *April 04, 2015 00:58*

 When you set the speed that fast, the servo's cannot keep up with where the software thinks the laser is positioned. 

 If you continue having problems, there is a very active facebook group helping with hardware and software problems on the laser.  Laser cutting and  engraving [https://www.facebook.com/groups/441613082637047/](https://www.facebook.com/groups/441613082637047/)


---
*Imported from [Google+](https://plus.google.com/110035902646499338699/posts/CTBcWDpZCYB) &mdash; content and formatting may not be reliable*
