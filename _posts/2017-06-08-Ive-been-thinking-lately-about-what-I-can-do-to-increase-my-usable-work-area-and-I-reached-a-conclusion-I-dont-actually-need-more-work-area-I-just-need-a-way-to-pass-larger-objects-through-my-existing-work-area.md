---
layout: post
title: "I've been thinking lately about what I can do to increase my usable work area, and I reached a conclusion: I don't actually need more work area, I just need a way to pass larger objects through my existing work area"
date: June 08, 2017 12:57
category: "Modification"
author: "Gavin Dow"
---
I've been thinking lately about what I can do to increase my usable work area, and I reached a conclusion: I don't actually need more work area, I just need a way to pass larger objects through my existing work area.



Here's what I'm planning: Rather than creating a passthrough which would likely require me to move major components (like the power supply or fume extraction ducting), I'd like to cut the floor out of my K40. This will allow me to lift the entire enclosure up so I can pass objects underneath. I have no bed in it currently - I just use a series of metal blocks stacked up underneath my workpiece to achieve the correct focal length.



This brings me to my big question: Focal length. I have a 2" and a 4" lens, but in order to do what I'm talking about, I would need something like an 8" lens, and from my basic understanding of lasers, this would be a bad idea. Not to mention, I've yet to find anything >4" for sale for this laser. Instead, I want to make a lens holder extension tube, which will move the lens itself down the extra 4" that I need.



Can anyone provide me a good reason why this might not work? I understand the mechanics of it - I'll simply be screwing into the existing M22x1.0 thread where the lens retainer attaches, and moving that whole operation down. It will basically just be a spacer that adds 4" to the unfocused beam length. Is there any optical reason why this won't work? I plan to slow down my engraving speed, since the added mass hanging off the carriage may cause issues at max speed.



Any feedback is appreciated, and I'll be sure to post updates of my progress.





**"Gavin Dow"**

---
---
**Martin Dillon** *June 08, 2017 13:09*

the extra weight would be an issue and limit max speed.  I am thinking it wouldn't be hard to do.  If you have a LO air assist, I would think someone could fabricate a extension tube that would screw onto the existing part.


---
**Gavin Dow** *June 08, 2017 13:14*

I have a 3D-printed air-assist nozzle, but I would likely skip the air altogether as I'll only be using this for engraving for the time being. I'll be 3D printing the extension, as well, as long as I can get fine enough detail to print those threads.


---
**Martin Dillon** *June 08, 2017 16:15*

check this out...  [thingiverse.com - Adjustable lensholder with dual line pointer holder, air assist nozzle and focus leveling block for K40 Laser by DerZeitgeist](https://www.thingiverse.com/thing:1648481)


---
**Gavin Dow** *June 08, 2017 17:08*

I looked and looked for something like that and found nothing. It wasn't quite what I was envisioning, but I think that will work. Thanks!


---
**Martin Dillon** *June 08, 2017 18:06*

It should be good enough for a proof of concept but it looks a little flimsy.


---
**Abe Fouhy** *June 08, 2017 22:19*

I was thinking the same thing as **+Martin Dillon**​ but with an open build vertical strut with another mirror where the current mirror is, then you can keep your lenses and the 2" focus by moving the head up and down and locking it to the rail with butterfly bolts.


---
**Steve Clark** *June 09, 2017 05:58*

I look forward to what you come up with!


---
**Stephane Buisson** *June 09, 2017 10:38*

**+Gavin Dow** to answer your 1st paragraph :
{% include youtubePlayer.html id="0r5sdS8zqY0" %}
[youtube.com - Smooth K40 conversion 1st steps](https://youtu.be/0r5sdS8zqY0?t=25)


---
**Gavin Dow** *June 09, 2017 12:59*

I like the front passthrough! Unfortunately, I've got a friend (client?) that wants to engrave something spanning about 4' on a pine 2x4, so to do that I would need to cut a hole in the back, too, and the ducting for the fume extraction is there... It's basically more surgery than I want.

I figure I can get more use out of the machine if my workspace is infinite, even if the alignment will be tricky.

I'm planning on doing some design work today, and hopefully fire up the printer tonight. I'll update when it happens.

Thanks for all the feedback so far!


---
*Imported from [Google+](https://plus.google.com/106681075840640377295/posts/Hs3cijgJDHv) &mdash; content and formatting may not be reliable*
