---
layout: post
title: "Came up with a modified version of a file i found in this group a few post back, thought i'd share the modified layout for anyone looking for inspiration, starting point or aesthetic upgrade"
date: February 21, 2018 08:29
category: "Modification"
author: "sam cv"
---
Came up with a modified version of a file i found in this group a few post back, thought i'd share the modified layout for anyone looking for inspiration, starting point or aesthetic upgrade. NOTE: full credit goes to the original poster of this layout, i simply modified it to include the GLCD screen and UP/DOWN buttons for motorised Z-Axis.

![images/9deb44ed55949cd8a2203ff004cc7157.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9deb44ed55949cd8a2203ff004cc7157.png)



**"sam cv"**

---
---
**Don Kleinschnitz Jr.** *February 21, 2018 13:43*

Very nice design and look. 



*Hints from experience; *

Dont switch the cooling system.  Leaving it off can cause damage :(. 

Of course a flow sensor and overtemp interrupter on the temp controller can help back that up :)! I'm just paranoid!



Install interlocks and a LED to indicate the LPS enable loop is open i.e. doors open, no water flow, overtemp. Many time I pulled my hair out only to find a maladjusted interlock switch!



Also you can add a voltmeter across the pot to give you a digital value for relative power.


---
**sam cv** *February 21, 2018 15:00*

**+Don Kleinschnitz** Thank you... i was looking at it now again and was thinking of a very similar thing, such as master warning lights would be a  nice addition to the flow warning light... as in when the LPS is on/off in relation to an open lid or over temp. thank you for pointing that out :)! i'll be sure to add it into the next render when i get more time. 


---
*Imported from [Google+](https://plus.google.com/101915993108014864632/posts/aCGiDZJdcis) &mdash; content and formatting may not be reliable*
