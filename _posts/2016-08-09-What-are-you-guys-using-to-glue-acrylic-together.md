---
layout: post
title: "What are you guys using to glue acrylic together"
date: August 09, 2016 19:07
category: "Materials and settings"
author: "David Spencer"
---
What are you guys using to glue acrylic together





**"David Spencer"**

---
---
**Ariel Yahni (UniKpty)** *August 09, 2016 19:49*

Interested


---
**Michael Knox** *August 09, 2016 21:59*

Here is a link to what I have been using works great. SCIGRIP 3 10799 Acrylic Solvent Cement, Low-VOC, Water-thin, 1/4 Pint Can with Screw-on Cap, Clear [https://www.amazon.com/dp/B00466V8F0/ref=cm_sw_r_other_apa_NhLQxbN9QJDBN](https://www.amazon.com/dp/B00466V8F0/ref=cm_sw_r_other_apa_NhLQxbN9QJDBN)


---
**Cam Mayor** *August 10, 2016 02:09*

Acetone works best if you have no gaps in the material


---
**Justin Mitchell** *August 10, 2016 09:22*

Tensol 12




---
**Ian Wright** *October 16, 2016 13:43*

I use Chloroform.. it's the best solvent.


---
*Imported from [Google+](https://plus.google.com/118373815141190144064/posts/26Q5EXwtyid) &mdash; content and formatting may not be reliable*
