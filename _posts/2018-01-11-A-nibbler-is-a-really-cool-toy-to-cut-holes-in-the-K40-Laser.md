---
layout: post
title: "A nibbler is a really cool toy to cut holes in the K40 Laser"
date: January 11, 2018 22:02
category: "Modification"
author: "BEN 3D"
---
A nibbler is a really cool toy to cut holes in the K40 Laser.


{% include youtubePlayer.html id="dCD0nsWNDhk" %}
[https://youtu.be/dCD0nsWNDhk](https://youtu.be/dCD0nsWNDhk)





**"BEN 3D"**

---
---
**Don Kleinschnitz Jr.** *January 12, 2018 11:35*

Yup my favorite K40 surgery tool!



[photos.google.com - New video by Don Kleinschnitz](https://photos.app.goo.gl/D7SrgcNAeQVApbD92)


---
**HalfNormal** *January 12, 2018 22:50*

Mine too!


---
**BEN 3D** *January 14, 2018 15:33*

the result, a hole for a moveable z table

![images/86caacd8ba88fec3ca3ca3647d58abe9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/86caacd8ba88fec3ca3ca3647d58abe9.jpeg)


---
**HalfNormal** *January 14, 2018 15:44*

Holey K40 Batman!


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/Xb4KqQznZPp) &mdash; content and formatting may not be reliable*
