---
layout: post
title: "Question for the smart people lol. I still have an Ipad 1 at home and was wondering if I was to install LW onto a pi could I access it from the Ipad using Chrome or using something like teamviewer?"
date: March 10, 2017 21:28
category: "Discussion"
author: "Andy Shilling"
---
Question for the smart people lol.



I still have an Ipad 1 at home and was wondering if I was to install LW onto a pi could I access it from the Ipad using Chrome or using something like teamviewer? (if Teamviewer is available on Rpi)





**"Andy Shilling"**

---
---
**Ariel Yahni (UniKpty)** *March 10, 2017 21:42*

yes you can But gcode generation can be slow depending on the complexity of the raster or path. Havent test it in LW3 ( i do have an rpi2 conneted to my cnc ) and not sure it will work on LW4, which you should be using now


{% include youtubePlayer.html id="zJTmoou9EX8" %}
[youtube.com - LaserWeb2 - Runs job from Kindle Fire](https://youtu.be/zJTmoou9EX8)


---
**Andy Shilling** *March 10, 2017 21:46*

**+Ariel Yahni** not sure why but your reply has been removed as Spam??? I can see it but nobody else.  I haven't made the jump to LW4 yet as I haven't had long on LW3 lol


---
**Ariel Yahni (UniKpty)** *March 10, 2017 21:47*

**+Andy Shilling**​ I have updated my response above.


---
**Andy Shilling** *March 10, 2017 21:52*

I've just restored your post, that was very strange. I will have a look at the Pi/Ipad pairing over the weekend Thank you. 




---
**Andy Shilling** *March 10, 2017 22:06*

Thanks **+Peter van der Walt** I will try that. I'm just trying to limit having to use a pc the whole time. No point burning electricity on a pc if an Ipad and/or Pi can do the little jobs.




---
*Imported from [Google+](https://plus.google.com/109817634550144703062/posts/Vt1rfcXDuE9) &mdash; content and formatting may not be reliable*
