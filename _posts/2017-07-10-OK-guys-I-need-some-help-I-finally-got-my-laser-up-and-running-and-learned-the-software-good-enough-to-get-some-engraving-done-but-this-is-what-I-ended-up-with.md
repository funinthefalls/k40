---
layout: post
title: "OK guys I need some help, I finally got my laser up and running and \"learned\" the software good enough to get some engraving done, but this is what I ended up with"
date: July 10, 2017 19:37
category: "Discussion"
author: "Doug Western"
---
OK guys I need some help,  I finally got my laser up and running and "learned" the software good enough to get some engraving done, but this is what I ended up with.  Can anyone give me any ideas why the laser it not working on the left side of my design.  It is only a 5 x 8 piece of wood.

![images/5d610a4f6c44c5c526ccc2c67277b5b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5d610a4f6c44c5c526ccc2c67277b5b7.jpeg)



**"Doug Western"**

---
---
**greg greene** *July 10, 2017 19:45*

mirror and head alignment, 


---
**Doug Western** *July 10, 2017 19:50*

I did the line up stuff where you shoot the beam to make sure it is hitting at every corner, what else would you suggest




---
**greg greene** *July 10, 2017 19:56*

Do you have the air assist head?


---
**Doug Western** *July 10, 2017 20:01*

I just got one, I have not added it yet 




---
**greg greene** *July 10, 2017 20:09*

move the head to the places where the engraving is not happening and see if the beam is hitting the center of the mirror on the head


---
**Andy Shilling** *July 10, 2017 20:48*

I can't remember who it was but somebody had a similar thing, his laser was hitting the centre of the mirror but the height wasn't right on the second mirror so the laser wasn't firing at a 90° from the head. **+Don Kleinschnitz**​​ will remember as I believe he helped correct it.


---
**Don Kleinschnitz Jr.** *July 11, 2017 00:36*

**+Doug Western** if the beam hits the mirror in the center but does not reflect into the objective perpendicular to the gantry the beam can hit the side or off center on the objective lens, thus blocking the light.



Manually move the head on the gantry to the position it stops marking and use the test button to see if you can find out why it is not going down through the objective straight.



Sometimes you can take the lens holder apart and see damage inside where the beam hits anything other than the lens. 



Another test is to position and somehow lock the head, do a test mark, lower the table make another test mark and see if the two marks are in the same place. If not the beam is not perpendicular to the table and objective.



A lack of perpendicularity can be cause by anything that does not run parallel to the gantry including the beam exiting the laser ....


---
**Ned Hill** *July 11, 2017 05:03*

**+Doug Western** what procedure did you use for alignment?  It really looks like an alignment issue to me.  There are a couple of good guides on here if you search #K40alignment.








---
**Doug Western** *July 11, 2017 19:07*

I think I got it - it was the mirrors - pretty sure I had lined them up, but maybe I missed it so bad I just thought it hit the same dot - lol


---
**Don Kleinschnitz Jr.** *July 11, 2017 21:51*

**+Doug Western** can we see an after picture :)!


---
**greg greene** *July 11, 2017 22:04*

Ok - you've officially navigated the K40 Rite of Passage - now go make yourself up a reward plaque !


---
**Doug Western** *July 11, 2017 22:07*

I made this :)

![images/4c2e22427c8070be232057578c0a431d.png](https://gitlab.com/funinthefalls/k40/raw/master/images/4c2e22427c8070be232057578c0a431d.png)


---
**greg greene** *July 11, 2017 22:08*

Looks Great !!!  Good Job


---
**Doug Western** *July 11, 2017 22:09*

Thanks 


---
*Imported from [Google+](https://plus.google.com/102298503204290496961/posts/SARXLkX5b6t) &mdash; content and formatting may not be reliable*
