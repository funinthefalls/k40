---
layout: post
title: "Anyone on here with a x700 clone or a 50w machine"
date: October 19, 2016 08:15
category: "Discussion"
author: "Tony Schelts"
---
Anyone on here with a x700 clone or a 50w machine.  What are the average thicknesses of cutting ply what would you be cutting 3mm birch laser ply at???? I do have a K40 Also. Thanks







**"Tony Schelts"**

---
---
**Jim Hatch** *October 19, 2016 12:47*

I have a 60W and use 20mm/s @ 95% power.


---
**Scott Thorne** *October 19, 2016 14:27*

18mm/s at 50%power for 1/8 birch plywood


---
**Tony Schelts** *October 19, 2016 15:27*

Thanks.  is it safe to expect that the laser tube in my machine that says 60w is more likely to be 50 watt as it as no added section added to the side. ??? its a clone sorry if I have asked before. 




---
**Scott Thorne** *October 19, 2016 15:40*

Measure the tube...what is the length and manufacturer?


---
**Scott Thorne** *October 20, 2016 09:51*

If the tube is 1000mm long its a 50 watt....if its 1250mm its a 60 watt.


---
**Tony Schelts** *October 20, 2016 11:45*

It actually states on Ebay 1100mm 60w and I think it is about that size.


---
**Scott Thorne** *October 20, 2016 11:48*

Then its probably 60 watts....the machine i have came with a tube that was 800mm  long but it was only 45 watts at the tube...i bought a replacement 1000mm puri tube that puts out 65 after the first mirror.


---
**Tony Schelts** *October 20, 2016 11:51*

The machine is only 1150 long so in hindsight it must be about 1000mm becaus eof the mirroors etc.  Cant Actually measure it at the moment.


---
**Tony Schelts** *October 20, 2016 23:07*

interesting thanks scott


---
**Tony Schelts** *October 20, 2016 23:08*

with the k40 it was suggested that you didn't go abot 18ma.  Is it similar with these machines or going up to 95 100 percent okay>


---
**Scott Thorne** *October 21, 2016 00:19*

**+Tony Schelts**..i never run mine above 65%....my tube is rated at 20mA/s...anything above 65% on mine is above 20mA/s.


---
**Jim Hatch** *October 21, 2016 00:53*

Regardless of the tube manufacturer, the normal rule of thumb is not to run them over 95%. You can run them higher - even at levels higher than "100%" by over boosting them. They will work, they will cut better & faster and they will suffer reduced lifespan. The impact is not linear though - it's not twice as bad to run it at 90% than 45%, more like 10% worse. But it is way more than twice as hard on the tube to run it at 100% than 50%, perhaps 4 times worse.



It's like your car - not a lot of difference between 2500RPM and 5000RPM, but redline it for extended periods at 6500 and you're toasting your engine.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/1oxtRzBrZeP) &mdash; content and formatting may not be reliable*
