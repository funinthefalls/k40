---
layout: post
title: "Are there any tweaks for the data buffer on a factory K40?"
date: June 24, 2015 00:50
category: "Discussion"
author: "Jon Bruno"
---
Are there any tweaks for the data buffer on a factory K40? It seems that the machine pauses a lot and I can only assume it's a data buffering issue.

This is a photo converted to grey scale then to 1bit bmp and sent to the machine as an engraving job.


**Video content missing for image https://lh3.googleusercontent.com/-vBXtADDyD-Q/VYn-0JNXRNI/AAAAAAAABX0/BB-yGgZD_zQ/s0/20150623_200544.mp4.gif**
![images/90404094547ad9b62ca52271ef72e20e.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/90404094547ad9b62ca52271ef72e20e.gif)



**"Jon Bruno"**

---
---
**Joey Fitzpatrick** *June 24, 2015 14:02*

I don't believe that is a problem in your video.  It appears that the machine is speeding up for non print/laser moves. (When the laser is not being fired). When a raster image is being produced, the machine processes it one line at a time.  Look at your print speed settings.  When the laser is on, the machine will be operating at that speed.  When the laser is not on, the machine will move much faster in order to speed up the time required for the entire job.


---
**Jon Bruno** *June 24, 2015 14:04*

Actually the behavior is the opposite it's drawing during the fast movement then it overshoots with the slow motion and comes back to where it left off and continues with another fast burn movement. this behavior is the same at 400 millimeters per second or 200 millimeters per second it makes no difference it's almost as if the machine reset after every burst. it's very strange


---
**Jon Bruno** *June 24, 2015 14:38*

But perhaps there is a setting for non rastering movement speeds too? If I can bump up the speed when not rastering it woudn't be so painfully slow.


---
**Eric Parker** *June 25, 2015 17:12*

Don't use a USB hub if you can at all help it.


---
**Jon Bruno** *June 25, 2015 19:51*

Yeah no hub here.. 


---
**Jon Bruno** *June 25, 2015 19:57*

I tried countless things including switching to .BMP encoding and resizing the image.. it's probably got something to do with the amount of data being sent ot the laser per line. it is 1000 DPI so perhaps the buffer fills ..oh well I'll have to try another machine to confirm its not the workstation causing it.


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/UZU5ngjcp9g) &mdash; content and formatting may not be reliable*
