---
layout: post
title: "Does anyone know the belt pulley type and pitch for the stock K40?"
date: January 03, 2019 18:35
category: "Original software and hardware issues"
author: "Timothy Rothman"
---
Does anyone know the belt pulley type and pitch for the stock K40?  Seeing some conflicting info out there, including a belt from Lightobject listed as 2mm pitch, but I doubt it's exactly 2mm as they have a 2.3mm pulley listed for the k40.





**"Timothy Rothman"**

---
---
**Anthony Bolgar** *January 03, 2019 19:40*

Both belt and pulley are 2.3mm pitch




---
**Jim Fong** *January 03, 2019 20:42*

It’s a chinese copy of MXL pulley  0.08” pitch (2.032mm)



The lightobject k40 belt is the correct version but they have the pitch misprinted in the description.   



It is not 2mm GT 


---
**Timothy Rothman** *January 08, 2019 15:32*

ok, thanks.  Good info to know.  Next time I go by LightObject I'll let them know there's incorrect details on their description.


---
*Imported from [Google+](https://plus.google.com/107673954565837994597/posts/6fdkuPr9mC5) &mdash; content and formatting may not be reliable*
