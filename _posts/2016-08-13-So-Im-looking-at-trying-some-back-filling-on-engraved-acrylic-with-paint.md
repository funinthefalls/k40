---
layout: post
title: "So, I'm looking at trying some back filling on engraved acrylic with paint"
date: August 13, 2016 02:10
category: "Materials and settings"
author: "I Laser"
---
So, I'm looking at trying some back filling on engraved acrylic with paint. 



Up until now I've mainly cut acrylic, peeling the protective layer off one side and cutting through the other.



Now I'm looking at engraving, I've seen a couple of comments in videos about back filling regarding how toxic the the protective layer on acrylic is when burnt. :|



Being cynical I couldn't help but notice most of the people promoting this idea also sell ultra wide masking tape...



So is this something to be concerned about? To be honest not even sure how well the protective layer will engrave.





**"I Laser"**

---
---
**Alex Krause** *August 13, 2016 02:15*

Buy cast acrylic it has a paper mask


---
**I Laser** *August 13, 2016 02:53*

Ah cool, the acrylic I've got is cast. It does seem to be paper mask but there's a decent amount of adhesive holding it on.


---
**Jim Hatch** *August 13, 2016 03:35*

It's a low tack adhesive - nothing to worry about. The acrylic is what's gonna smell. Keep the lid closed and the exhaust on and you'll be fine. Just finished 55 acrylic plaques and 100 challenge coins a week ago. No issues.


---
**I Laser** *August 13, 2016 04:20*

Sweet, thanks for that guys. I figured it was probably nothing to worry about but wanted to be sure.



You sound like you've been busy Jim!


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/jdy5MRXGoKX) &mdash; content and formatting may not be reliable*
