---
layout: post
title: "A project I completed for the company...it's 3/8 thick clear acrylic...I really didn't think it would cut it out but it did a very nice job....5mm/s at 65% power...this was done using a 3 inch focal length lens"
date: February 19, 2016 13:33
category: "Discussion"
author: "Scott Thorne"
---
A project I completed for the company...it's 3/8 thick clear acrylic...I really didn't think it would cut it out but it did a very nice job....5mm/s at 65% power...this was done using a 3 inch focal length lens.

![images/0dc83e051ec17336e448e2270016015f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0dc83e051ec17336e448e2270016015f.jpeg)



**"Scott Thorne"**

---
---
**Scott Thorne** *February 19, 2016 13:45*

Yes...single pass...I don't think I could have done it with a 2 inch focal length lens.


---
**Brooke Hedrick** *February 19, 2016 18:36*

Does longer focal length translate to a longer segment of the beam that is tightly focused?


---
**Scott Thorne** *February 19, 2016 18:38*

**+Brooke Hedrick**...yes it does...with 4 inch being the best...from what I've read....have not tried one yet.


---
**Brooke Hedrick** *February 19, 2016 18:40*

**+Scott Thorne** thanks!


---
**Scott Thorne** *February 19, 2016 19:27*

**+Brooke Hedrick**...sure thing man.


---
**Todd Miller** *February 20, 2016 00:05*

How many watt laser ?


---
**Scott Marshall** *February 20, 2016 00:22*

Nice looking piece - R8 or 5C Collet rack?



The performance is good new.



 I've got a 2 1/2 x 18mm due here, I was hoping for a performance improvement. The 2.5" gets you to the bottom of the I beams so you can cutout the bottom and engrave larg(er) surfaces like toolboxes, hammerhandles etc. 



I may have to pop for the 5" - any problems getting it thru the cone on the nozzle? 


---
**Scott Thorne** *February 20, 2016 12:33*

**+Scott Marshall**...None at all...it's a vacuum head for one of our machines where I work....I'm going to get the 4 inch today...order it at least....another month to ship it here to Atlanta.


---
**Scott Thorne** *February 20, 2016 12:34*

**+Todd Miller**...it's supposed to be a 50 .....but the info on the tube says max is 48.


---
**Scott Marshall** *February 21, 2016 09:56*

Thanks for the info. Guess I'm ordering a longer one yet.

I'm not sure what size lens you need, but I've had good luck getting my lenses (18mm) from Saite cutter on ebay and they get here (upstate NY) in about 7-10 days from China. They're probably not up to the $200 American or German lenses, but I'm just playing around and they suit me for $20.

They have "regular" and ""better" for 10 bucks more. Havn't tried a "better" one yet, but the standard ones seem to work well.

 


---
**Scott Thorne** *February 21, 2016 10:45*

**+Scott Marshall**...yeah the China models work great for what I'm doing...I have a 1800.00 machine....not a 10,000.00 dollar machine...so a 40 dollar lens works great for what I'm doing with it.


---
**Scott Marshall** *February 22, 2016 03:28*

I ordered a 4" lens ($22.49 shipped).

Thanks for the info on the performance improvement.



Right now with the stock lens, I have to re-set the focus to the far surface to take a 2nd pass and cleanly cut 1/8 birch ply. You CAN just pound it with  4 or 5 passes on a median setting, but it cuts much better with the 1st pass on the top surface, and the 2nd on the far side. 



I'm looking forward to just setting it and cutting it clean with the 4" lens.



My machine is the regular K40, and I guess they're lucky to really put down a solid 30w despite the 40w rating.. 

I'm putting in about 250w, but there's no way of telling how much is going into water and how much is laser energy. I can find no efficiency specs for the tube.



I need all the utilization efficiency I can squeeze out....



PS - nice on on the ipad, I was a tad nervous putting my phone under the laser...



Scott






---
**Scott Thorne** *February 22, 2016 09:43*

**+Scott Marshall**...thanks bro. 


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/YiKYkCYDcUh) &mdash; content and formatting may not be reliable*
