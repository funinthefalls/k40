---
layout: post
title: "Shared on March 05, 2016 12:02...\n"
date: March 05, 2016 12:02
category: "Object produced with laser"
author: "alexandre auguste"
---
[http://mustermania.fr/index.php/2016/03/05/casse-tete-en-plexiglas-decoupe-laser/](http://mustermania.fr/index.php/2016/03/05/casse-tete-en-plexiglas-decoupe-laser/)





**"alexandre auguste"**

---
---
**I Laser** *March 06, 2016 01:34*

I had one of those given to me decades ago. Good brain puzzle. Not sure how many passes 10mm would take but any thinner I don't think it would be right.


---
*Imported from [Google+](https://plus.google.com/100325381232027359523/posts/hsqv6R2YmUa) &mdash; content and formatting may not be reliable*
