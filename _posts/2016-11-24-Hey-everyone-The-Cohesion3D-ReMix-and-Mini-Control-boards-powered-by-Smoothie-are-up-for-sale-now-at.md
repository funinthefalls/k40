---
layout: post
title: "Hey everyone, The Cohesion3D ReMix and Mini Control boards powered by Smoothie are up for sale now at ."
date: November 24, 2016 22:18
category: "Smoothieboard Modification"
author: "Ray Kholodovsky (Cohesion3D)"
---
Hey everyone, 

The Cohesion3D ReMix and Mini Control boards powered by Smoothie are up for sale now at [cohesion3d.com](http://cohesion3d.com).



A few of you have already tried out my Mini for Laser Bundle and more have theirs on the way. 



We've made the simplest possible way to upgrade your K40 to the 32 bit performance of Smoothie - which does PWM calculations for laser power every step and throttles them based on acceleration - meaning you get a uniform cut every time and can do amazing rasters like the work you've seen from **+Ariel Yahni**, **+Alex Krause**, **+Joe Spanier**, **+Carl Fisher** and many other members of this group.  Plus it frees you from the horrible stock CorelDraw application and lets you use open alternatives like LaserWeb to set up your cutting and engraving jobs!



The Mini is the same size and has the same mounting hole pattern as the stock M2Nano board, making the physical replacement a simple drop in.  But it also the ribbon, power, endstop, and pwm connectors that we've seen in all the K40's thus far, allowing anyone to swap some 4 or 5 wires and be up and running. 

With the Mini Board, 2 Motor Drivers, a MicroSD Card loaded with the K40 Laser Config, and PWM Cable, this $99 Laser Bundle is an extremely simple and affordable way to upgrade your machine to take full advantage of its capabilities.



[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)







**"Ray Kholodovsky (Cohesion3D)"**

---
---
**greg greene** *November 24, 2016 22:30*

Highly reccommended


---
**Anthony Bolgar** *November 24, 2016 22:34*

Good job Ray


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 22:37*

Thanks guys. Please reshare! 


---
**Stephane Buisson** *November 24, 2016 22:42*

not having this board myself, but I am sure it would be compatible with Visicut and fusion 360 (+laserweb) , like the original smoohtie. please confirm when you got yours.


---
**Anthony Bolgar** *November 24, 2016 22:44*

Don't forget LaserWeb ;)


---
**Stephane Buisson** *November 24, 2016 22:49*

post moved to Smoothieboard modification category.


---
**Jonathan Davis (Leo Lion)** *November 25, 2016 18:23*

**+Ray Kholodovsky** Your controller board is beyond useful and i do plan on buying some end stops later today  on amazon.


---
**Ned Hill** *November 28, 2016 05:49*

Ok, finally pulled the trigger on upgrading my controller and ordered one. This seems to be the easiest option.  Can't wait to start using it and laserweb :)


---
**Ray Kholodovsky (Cohesion3D)** *November 28, 2016 05:54*

Thanks for your support Ned!


---
**Jeff Johnson** *December 01, 2016 22:16*

I've seen a few upgrade boards and this looks like the easiest to implement. I currently use Corel Draw X7 and have become quite proficient with it since I've already been using it to make brochures for my company. Is it possible to still use Corel Draw? If not, how would I cut my designs? I'm ready to buy, I just want to make sure I'll be able to use the tools I'm best with.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 22:20*

Hi **+Jeff Johnson** thanks for your interest. 

I've never actually used CorelDraw (I gutted my laser on day 1), but I know it's a vector program.  So you should be able to export a DXF, an SVG, or an Image and import that into LaserWeb which will then calculate the laser movement gCode for you.  I hope this helps. Let me know if there's anything else I can answer.


---
**Jeff Johnson** *December 02, 2016 14:17*

I installed LaserWeb last night and it looks like the DXF files exported by Corel Draw won't work. I'm looking for other gCode apps now but if you have any suggestions I'd love to check them out. What application are you using to create vectors?


---
**Anthony Bolgar** *December 02, 2016 15:15*

LaserWeb requires that alll DXF files be saved with Polylines. Open your design on CorelDraw an make sure the design is polylines compliant.


---
**Jeff Johnson** *December 02, 2016 15:48*

I tried that and I suspect that the problem is CorelDraw. I've had a couple people on Thingiverse tell me that my exported DXF files look funny and when I open them in InkScape nothing looks as it should either. It's possible that I need to learn a new tool if CorelDraw can't do what I need.


---
**Ray Kholodovsky (Cohesion3D)** *December 02, 2016 15:52*

Me personally, I do 3D Cad in autodesk inventor and I can export dxf of faces I need to cut. I tend to use Vectric Aspire to create my gcode (I wrote a laser postprocessor for it) and then LW to send. 

There was an article floating around about how to convert to polylines using Draftsight. I'll have to dig it up. 


---
**giavonni palombo** *December 07, 2016 01:51*

After tripping over my usb cable and breaking my stock board Im having to up grade. What about a rotory? 


---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 02:04*

Well the mini does have support for 4 drivers so we do have people working on XY and a Z lift table and a rotary. The 4th axis (A) isn't officially supported by smoothie yet but you can easily treat is as an extruder in order to control it. 


---
**giavonni palombo** *December 07, 2016 02:17*

Software wise im a dummy.  I did read but did not save it. Someone said the did a post processer for vetric aspire. I have vcare pro that is rotory capable would that work ? 


---
**giavonni palombo** *December 07, 2016 02:19*

Ha ha i just saw it was Ray. After today im really felling dumb.




---
**Ray Kholodovsky (Cohesion3D)** *December 07, 2016 02:20*

I did write a Laser PostProcessor for Vectic.  It is only XY though.  I've never played with rotary.  LaserWeb is developing this and you may want ask around (**+Ariel Yahni**).


---
**giavonni palombo** *December 07, 2016 03:17*

Thanks Ray


---
**HP Persson** *December 21, 2016 22:04*

My board doesnt have the ribbon cable, is there a pinout so i can make my own converter without pulling my hair for weeks? :)

Thinking about ordering one :)


---
**Chris Sader** *December 23, 2016 05:45*

**+Ray Kholodovsky** just ordered a mini bundle to replace my faulty AZSMZ. Is the 4-6 weeks lead time still accurate?


---
**Ray Kholodovsky (Cohesion3D)** *December 23, 2016 05:50*

**+Chris Sader** Awesome!  It should actually be far less than that.  Check the Cohesion3D community for the latest updates: [https://plus.google.com/u/0/communities/116261877707124667493](https://plus.google.com/u/0/communities/116261877707124667493)


---
**Ray Kholodovsky (Cohesion3D)** *December 23, 2016 05:58*

**+HP Persson** sorry I didn't answer you earlier.  If your laser is anything like mine (all white JST connectors) you'll be just fine as well. The mini has all the known k40 connectors to date, as well as the individual breakouts for each motor, endstop, MOSFET, etc. already there. We should be able to rig just about anything from it. 


---
**HP Persson** *December 23, 2016 10:06*

Thanks **+Ray Kholodovsky**

This is what my current controller looks like, all white connectors.

If not 100% i can re-crimp them, i made a pre-order now :)

![images/e775e59f862302f1f03553c51093ef6d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e775e59f862302f1f03553c51093ef6d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *December 23, 2016 16:48*

**+HP Persson** so that's actually different, is this an older machine by any chance (like more than a year old)?  This is the "all jst" setup I meant. 

Anyways, it doesn't matter.  You have 2 motors, 2 endstops, 24v and Gnd in, and the L(o) to fire signal. We're all good - there's places to plug all that in on the mini. ![images/8905901e4256d9f6a8166aeb4b428196.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8905901e4256d9f6a8166aeb4b428196.jpeg)


---
**HP Persson** *December 23, 2016 17:09*

HT Master 5 nano is actually a newer board than in most k40 delivered today,  but as it's new tech not many sold today has it. (mostly digital k40). But as  you said it's all there so probably a easy upgrade :-)


---
**Ray Kholodovsky (Cohesion3D)** *December 23, 2016 17:17*

Good to know, thanks.  Check out the Cohesion3d community here on g+ someone else posted a red board as well. 


---
**jeremiah rempel** *February 20, 2017 22:19*

**+Ray Kholodovsky** I have just attempted to install the dreaded MSK Sbase 1.3 on my laser and when using the L for PWM it creates so much EMI that it resets the either the board/USB Chip/or locks the entire board up requiring a reset.  This POS doesn't have any 5v logic so I'm unsure how to drive the IN with a 5V PWM signal and use L as a TTL enable.  So I'm looking at your board and I've already cut the end off my blue wire bundle and attached male pins to the 4 wires.  Is the green terminal block in the picture to hook up those wires since I don't have the stock connector anymore?  If so I'm purchasing one today and will give the MKS to my buddy to refit his Stratasys 3D printer to something more modern.


---
**Ray Kholodovsky (Cohesion3D)** *February 20, 2017 22:23*

You're not going to drive the IN (although we do have the level shifter to bring a PWM signal to 5v so you can try, and still include the cable to do it).  

The new recommended way is to keep the pot in play and pulse the L wire. 



You can hook up loose wires to the power and MOSFET terminals so you don't need the stock connector. Remember it's a full functional board for 3DPrint/ CNC/ Laser that just happens to support the K40 entirely :)


---
**jeremiah rempel** *February 21, 2017 00:24*

Nope.  Funny..  Just after I purchased the Cohesion3D I saw post about killing the 5v power from the USB.  Never would have thought of that, but now it's working like a champ.  Something about just purchasing the real deal has made everything better. LoL.


---
**Lance Sponberg** *March 18, 2017 22:01*

Hi I just installed my new cohesion mini upgrade and it was a straight exchange, all of the connectors plugged right in. Probably took less than a half hour. I am using LaserWeb 3 and when I connect to the controller through comm 3 at 115200 I am getting an error that my controller is in alarm mode, cod screen shows a bunch of lines of alarm,mpos0.000 and wpos0.000. Please help!


---
**Ray Kholodovsky (Cohesion3D)** *March 18, 2017 22:08*

**+Lance Sponberg** please make a post in the Cohesion3D group and we will help you out: 



[Cohesion3D](https://plus.google.com/communities/116261877707124667493?iem=1)



Please check the latest instructions on our documentation site, there is a link to determine if you need drivers, etc. 



We will need screenshots and to know what operating system you are running. 


---
**L Y** *March 30, 2017 22:16*

Does use the cohesion board and laserweb have the ability to save different laser positions like corellaser? I went ahead and bought a cohesion nano and thats really only thing i havent found an answer too.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/WJRxCeJAz97) &mdash; content and formatting may not be reliable*
