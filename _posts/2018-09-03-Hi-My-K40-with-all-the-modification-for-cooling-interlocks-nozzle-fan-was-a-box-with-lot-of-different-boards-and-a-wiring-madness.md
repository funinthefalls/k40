---
layout: post
title: "Hi! My K40 with all the modification for cooling, interlocks, nozzle fan was a box with lot of different boards and a wiring madness"
date: September 03, 2018 20:04
category: "Modification"
author: "Dirk Eichel"
---
Hi!

My K40 with all the modification for cooling, interlocks, nozzle fan was a box with lot of different boards and a wiring madness. So idecided to built a combined helper board. This is work in progress. Please let me know what you think and what features are missing.



The features are: 6 interlock inputs. Interlock visualisation by LED, nozzle fan switch, cooling fan switch if e.g. a PC water coller is used to cool down the coolant, 4 relais (115/230V AC). All switching functions are controlled from the M2nano trigger output (see here: [https://www.instructables.com/id/China-Laser-Aux-Trigger-Output-K40-M2Nano-Board/](https://www.instructables.com/id/China-Laser-Aux-Trigger-Output-K40-M2Nano-Board/)).



It is on GitHub ([https://github.com/STB3/K40_helper](https://github.com/STB3/K40_helper)). If you have suggestions for further enhacement, let me know.



![images/5d31526a336f4c0f0babaac78457a1af.png](https://gitlab.com/funinthefalls/k40/raw/master/images/5d31526a336f4c0f0babaac78457a1af.png)
![images/cb4cda4249a7d138e24bc7163d59ebdb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cb4cda4249a7d138e24bc7163d59ebdb.jpeg)

**"Dirk Eichel"**

---
---
**Stephane Buisson** *September 04, 2018 10:41*

I really like the Air solenoid feature to not have the compressor on all the time.



[ebay.com - 12V DC 1/4" 2 Way Normally Closed Pneumatic Aluminum Electric Solenoid Air Valve 969843060148 &#x7c; eBay](https://www.ebay.com/itm/12V-DC-1-4-2-Way-Normally-Closed-Pneumatic-Aluminum-Electric-Solenoid-Air-Valve-/182231884237)



a feature to add on the C3D board **+Ray Kholodovsky**


---
**Aaron Grogan** *September 05, 2018 17:46*

I don't own a K40, but if any aspect of this board is safety-critical, you will need to add monitoring and redundancies to protect against welded relay contacts.  I would recommend reading about industrial safety relays and reset procedures.  Also make sure that your door switch and e-stop button are suitable for the safety function that they are performing.  Don't count on a no-name machine supplier for anything when it comes to your safety.


---
**Dirk Eichel** *September 07, 2018 12:13*

**+Aaron Grogan** Thats why it is open on GitHub




---
**Dirk Eichel** *November 02, 2018 09:09*

I have it now running on my machine. All features are working properly. I have 4 spare PCBs incl. all required components. If you are interested please send me an e-mail.


---
**Stephane Buisson** *November 02, 2018 10:29*

**+Dirk Eichel** please don't hesitate to make a new post in the community.


---
*Imported from [Google+](https://plus.google.com/+DirkEichel/posts/64igWe9GEYW) &mdash; content and formatting may not be reliable*
