---
layout: post
title: "Any tips on how to get the water tube onto the glass nipples?"
date: May 12, 2016 10:06
category: "Hardware and Laser settings"
author: "I Laser"
---
Any tips on how to get the water tube onto the glass nipples?



I've heated the tube up with a hair dryer (don't have a heat gun), which does make it more malleable though it's still not close to attaching. :|



Thanks





**"I Laser"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 10:35*

Try boiling water to heat up the tube. Not certain if it'll work, but can't hurt to try.


---
**I Laser** *May 12, 2016 10:52*

Thanks I've tried that too, it's like the tube is too small but they managed to get it on some how lol!



So annoying!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 12, 2016 10:55*

When I replaced the tubing that was on there I couldn't get it off & thought there was no way I would get it back on, so I just left about 3 inches of the original tube & joined to it with silicone. Maybe if you have it heated up you could use some conical shaped object to stretch it out a bit while it is in the boiling water.


---
**I Laser** *May 12, 2016 11:31*

Thank **+Yuusuf Sallahuddin** I'll give that a go, it is like there's no way it would go on but it obviously does lol!


---
**HalfNormal** *May 12, 2016 12:44*

You can try a water based lube. The kind you find at the drug store. 


---
**Stephane Buisson** *May 12, 2016 13:15*

washing up liquid as lubricant.


---
**I Laser** *May 15, 2016 11:04*

Thanks guys, managed to get the tube on, combo of boiling water and washing up liquid did the trick.



Now the fun starts with the silicone, I've given it an attempt but think it's shifted on the positive lead so going to wait until it's dry, pry it off and try again...


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 15, 2016 13:13*

**+I Laser** Good to hear that worked. I'll keep that in mind for future if I need it. Thanks for the update on it.


---
**I Laser** *May 17, 2016 02:08*

The next issue is there's a lot of fine bubbles in the tube. The new tube also has a ridge where the two cavities join, unlike my previous two tubes, which catches the fine bubbles, they accumulate forming larger more dangerous bubbles. :|



I'm wondering if the washing up liquid has caused this, as I've not had this issue before. I've had the pump running for 24 hours and they're still forming. Maybe something to keep in mind. I'll be leaving the pump on hoping they clear!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 17, 2016 03:20*

**+I Laser** Is your water return hose in the reservoir (so the water enters with no splash/bubbles) or above it (splashing into it)?



The washing liquid might be your culprit, but I assume you only put a little of it.



I would suggest running your pump/reservoir higher than the tube, to allow all the bubbles to flow down into the tube, then doing some lifting/rotating until you can get the bubbles out. I actually kept squeezing off the input flow to prevent water getting in, then let it go. It made the bubbles move in the tube a bit, allowing them to get to the exit pipe.


---
**I Laser** *May 17, 2016 06:02*

**+Yuusuf Sallahuddin** It's a strange one. I have two machines into the same tank.



Both have homemade flow switches, so the returns don't sit submerged and there's a little bit of splashing back into the tank. Though not many bubbles (I only used a drop of dishwashing liquid, massaged onto the glass tips.)



Both pumps sit on the floor of the tank.



The new tube has a bunch of fine bubbles, the tube itself is a different design than shown on the advert, which is causing the fine bubbles to accumulate and form larger bubbles.



The old tube, in the same tank has no fine bubbles.



The tube I received is a different design than what was advertised. So I'm considering contacting the seller, it does seem a little false advertising, though to be honest I wouldn't care if I wasn't having this issue...



The tube isn't clamped down so I have occasionally rotated it to expel the trapped air, but the bubbles start accumulating practically straight away.



I'll post up pics in a new thread.


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/jDBmpNp2UhV) &mdash; content and formatting may not be reliable*
