---
layout: post
title: "Did You Know? A 30 Watt laser can generally cut materials up to 1/8 thick"
date: February 06, 2016 06:55
category: "Discussion"
author: "Hang Rex"
---
Did You Know?

A 30 Watt laser can generally cut materials up to 1/8″ thick. Higher power systems will be able to cut thicker materials more quickly. When choosing a laser engraving machine, keep in mind that cutting will take much longer than engraving. With lower powered laser machines you may need multiple passes, as well.

‪#‎LaserMarkingMachine‬ ‪#‎LaserEngravingMachine‬ ‪#‎Co2LaserEengravingMachine‬﻿

![images/87330f520e385f18f29a721d913ade50.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/87330f520e385f18f29a721d913ade50.jpeg)



**"Hang Rex"**

---


---
*Imported from [Google+](https://plus.google.com/116872811666596467056/posts/gwZgF5NYDnz) &mdash; content and formatting may not be reliable*
