---
layout: post
title: "Hello everyone, I bought a K40 last year and through the help of this group have upgraded it to a reliable little machine"
date: March 24, 2018 18:47
category: "Hardware and Laser settings"
author: "Brian Williamson"
---
Hello everyone, I bought a K40 last year and through the help of this group have upgraded it to a reliable little machine. Thanks to everyone who have contributed to this great resource.



Is there a way to reset my home position by 4 or 5mm? I am running K40 Whisperer and my cutting area is 325mm x 228mm.  I have a project where I need just a few mm more cutting space on the Y axis. 228 is already puts me cutting very close to the Y axis motor.  However, when I unlock my rail I can move my gantry about 7-8mm up before it comes in contact with the frame.  



Thanks again!





**"Brian Williamson"**

---
---
**HalfNormal** *March 24, 2018 20:41*

The position of the limit switches dictates home.


---
**Joe Alexander** *March 24, 2018 22:13*

the only setting that comes to mind is reducing the retract after home to 1-2mm, I believe its default is 5mm.


---
**James Rivera** *March 24, 2018 23:18*

I've seen some here do a modification where they remove the partition between the main area and the control board area, then install a whole new gantry system. Voila! Larger cutting area. Not a small or trivial mod though...


---
**Scorch Works** *March 25, 2018 00:37*

Unlock the rail and manually move the head where you want to start.  K40 Whispered will start wherever the head is.


---
**Brian Williamson** *March 25, 2018 01:39*

Great, it worked and I had 3mm left over!  I assumed I had to initialize after unlocking the rail.  Thanks so much!




---
*Imported from [Google+](https://plus.google.com/112546866151836572955/posts/RZcZgLbwPHD) &mdash; content and formatting may not be reliable*
