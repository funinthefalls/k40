---
layout: post
title: "Just got K40 Any chance to update driver to 6C6879-LASER-M2:9 PCB ?"
date: January 13, 2019 12:02
category: "Hardware and Laser settings"
author: "Krasimir Neshev"
---
Just got K40

Any chance to update driver to 6C6879-LASER-M2:9  PCB  ?

Best Regards







**"Krasimir Neshev"**

---
---
**Ned Hill** *January 13, 2019 17:37*

Not sure I follow.  If you are going to use the stock corellaser or laserdraw then you will need to set the board type in the software settings.  


---
**Ned Hill** *January 13, 2019 17:40*

We actually recommend using K40 whisperer with inkscape for people using the stock control board.  [scorchworks.com - K40 Whisperer](https://www.scorchworks.com/K40whisperer/k40whisperer.html)


---
*Imported from [Google+](https://plus.google.com/+KrasimirNeshev/posts/B7NT46xi3Ui) &mdash; content and formatting may not be reliable*
