---
layout: post
title: "An impressive Laser Machine build. Dare I?"
date: February 22, 2018 13:26
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
An impressive Laser Machine build.



Dare I? Maybe just a bit smaller.



**+Steve Clark** note the cooling!




{% include youtubePlayer.html id="SzknZ3EHMp4" %}
[https://www.youtube.com/watch?v=SzknZ3EHMp4](https://www.youtube.com/watch?v=SzknZ3EHMp4)





**"Don Kleinschnitz Jr."**

---
---
**HalfNormal** *February 22, 2018 15:54*

I say it always comes down to time and money. If you have the time to play this way is wonderful. If you want to save some money, right now you can buy some amazing lasers at an affordable price.


---
**Steve Clark** *February 23, 2018 02:03*

IMO - He needed 75% more cooling (Maybe less).Nice project though.


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/N6eg6WbPiuN) &mdash; content and formatting may not be reliable*
