---
layout: post
title: "Working on my own arduino interlock to suit my needs"
date: September 30, 2017 22:10
category: "Modification"
author: "Space Ronin"
---
Working on my own arduino interlock to suit my needs. Currently on the bench and works alright. I am pretty new to arduino. using a flowmeter i bought to turn on a relay to enable my laser switch as an interlock. At a later date when i get better at arduino i want to add the cooling temp sensor to it(currently just using a display for it). Maybe later on a flame sensor/high temp sensor in the cutting area that will sound an alarm incase of a fire (to control it before it gets to the smoke alarm)



Also if anyone is decent at arduino and wants to look at my code ill post it. I'm sure someone can probably do a better job though lol.





[https://github.com/wiarde/laser-interlock](https://github.com/wiarde/laser-interlock)









**"Space Ronin"**

---
---
**Adrian Godwin** *September 30, 2017 23:46*

I've also been working on flow-measurement with a view to using it for an interlock. So far I'm measuring flow rate with an impeller sensor and two temperatures with DS1820 sensors. It doesn't actually turn off the laser yet, just displays the result.





The flow measurement was a problem - not because it's difficult, but because the sensors available work by restricting the flow to a jet and pointing that at a turbine wheel. With the low water pressure provided by a fish tank pump this unreasonably restricts the flow.



I bought an sensor initially off ebay, thinking it would be more easily available if someone wanted to copy my setup, but ended up using one with a better flow rate from my junk box.


---
**Space Ronin** *October 01, 2017 00:01*

I used flow meter from amazon with 3/4 fittings. Pretty good size and reduced to 1/4 for the water line. The pump that came with mine is pretty beefy supposed to do around 793lph I'm guessing unrestricted. I was measuring maybe 40-60 ish. Duno how accurate the flow meter is. Were you able to get your temps to print on LCD with your flow reading. Yours sounds interesting. I'm super new at arduino code haha. It took me an hour to smash two different scripts together to make that. 


---
**Don Kleinschnitz Jr.** *October 01, 2017 13:38*

**+Art D.** its fun to build your own embedded controller. I started going down that road and stopped because I found to many cases were the controller could fail and prevent safe interlock operation. 



When building embedded safety controllers its important to give special attention to their failure modes so that they do not leave interlocks inadvertently closed when the firmware hiccups, locks up resets etc. Its also a good idea to treat the water temp the same way. Insure that no possibility exists that when the controller is in an error state it inhibits turning the laser off.



For this reason I only use switches in series with the LPS interlock circuit. I also use a flow switch in the interlock circuit to interrupt the laser power-in case of over-temp.



FYI the safety interlocks and temp monitoring for the laser and LPS can be implemented without building anything.



 


---
**Adrian Godwin** *October 01, 2017 15:57*

**+Don Kleinschnitz** Yes, I mistrust software for safety handling. But a simple switch can fail too, especially the flow switches. 



I'm doing it mostly because it's fun and I want to measure flow rate and temperature rise to see how well the cooling is working. I probably wouldn't do it that way if someone's life depended on it.  It improves the protection for the laser tube but it's not nuclear-industry reliable.


---
**Adrian Godwin** *October 01, 2017 16:02*

**+Art D.** Yers, I'm displaying temperature on the LCD. Ultimately I want to display outflow temperature, difference between inflow and outflow, and estimates for how long it can continue to rise. 



The DS1820 sensors are quite useful - I've got them mounted in 'T' pipe fittings so they can sample the water temperature and you'll find plenty of examples online to read them. They're not incredibly accurate (about 0.5 degree I think) but don't need calibrating.



Flow calibration is fairly easy : just let the outflow go into a container and measure how much it's filled in a minute. I'm getting about 1.1 lpm - I'd prefer more but I think it's workable.




---
**Space Ronin** *October 01, 2017 16:45*

**+Don Kleinschnitz** i know what you mean now. When i have my flow meter unplugged the software reads super high and activates the relay which is bad. I tried a simple flow switch but the water pressure wasnt enough to activate it. What flow switch did you use to interlock it? this way i can have a way to monitor temps/flow but still keep a safe interlock in place. One way around the software hicup i think may be to set a safe zone for the flow. The failure of the sensor has always been 300-400  lpm  of flow and my normal flow is less then 60. I think for now i will keep using arduino until i find a flow switch that works with low flows. I dont plan on leaving the laser alone while it is in use anyway due to small children in my household that are curious.



**+Adrian Godwin**

 DS1820 sensors waterproof? i was thinking of using the T method as well and putting a sensor in there. Just havent found/looked for one yet.



Thanks for the advice everyone.


---
**Adrian Godwin** *October 01, 2017 17:00*

There's a variant of them that comes in a 6mm stainless cylinder. Perfect for pushfit plumbing (actually, I sleeved it to increase to 8mm).



I don't know how many of these are genuine. The ones I'm using are probably a cheap nickel-plated clone : better ones are brushed stainless steel. 



[ebay.co.uk - ds18b20 waterproof &#x7c; eBay](https://www.ebay.co.uk/sch/?_nkw=ds18b20+waterproof)






---
**Don Kleinschnitz Jr.** *October 01, 2017 18:12*

**+Art D.** I use the one below but had to dissemble and adjust the spring as well as put it in the output side of the system in downward position.



Cover interlocks are what I would worry about with any laser system. If the firmware was locked up and a cover is opened thinking the interlocks are working you could get hurt as you cannot see this laser nor the HV potential of the LPS HV until you get to near it.  Its easy in these machines to open a cover and look in without realizing the laser or LPS is on. 



You can double up the interlock switches if you want to read the cover position with a controller while insuring that it does not interfere with its safety. 

The cooling protection is not a safety issue is a "pocketbook" protection safeguard :)!



[http://donsthings.blogspot.com/search/label/K40%20Cooling](http://donsthings.blogspot.com/search/label/K40%20Cooling)

 

[amazon.com - Amazon.com: White Plastic Shell Magnetic Water Flow Switch w Inner Outer Thread: Home Improvement](https://www.amazon.com/gp/product/B00AKVEGTU/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)


---
**Space Ronin** *October 02, 2017 03:42*

Thanks for the link on the temp sensor. 



I actually have that same flow switch but I couldn't get it to work. What adjustments did u make to the spring. I was thinking of putting an oring  to seal the little bullet inside better or maybe cutting the spring and stretching it more.  Also I do have doors hardwired into the lps not controller. 

I have the controller on the flow meter as a backup interlock to protect the tube. I will replace it with with the flow switch and keep it to monitor flow and temps. 



All this and I haven't even used it or plugged it in yet haha. I figured best time to wire it is when the power supply has been off since the machine was built. Hopefully next weekend I'll get the cooling and exhaust lines piped in and start aligning mirrors .



So far I got the doors laser and main door interlocked, ma meter installed and been 3D printing all weekend vent adapters and arduino mounting cases. 


---
**Don Kleinschnitz Jr.** *October 02, 2017 04:09*

**+Art D.** mmm, I don't remember exactly what I did to the sensor, have to look at it again.

I remember it wasn't initially obvious as mine did not work either. After taking its guts out it became obvious and as I said it only worked downhill on the output.


---
**Space Ronin** *October 02, 2017 04:53*

ill try facing it down  next time and see if that works. seemed like the gap betweent he bullet and the seat was big enough to let the flow through withought depressing it. and it leeked because it was stupid metric threads and all i got over here is us sizes. ill have to superglue it if i can get it to work.


---
**Don Kleinschnitz Jr.** *October 02, 2017 12:36*

**+Art D.** something sounds different as I do not recall mine being metric. I am in Utah so I also do Imperial. I will try and open mine back up and post some pictures as I have gotten these questions before. 


---
**Space Ronin** *October 02, 2017 16:38*

**+Don Kleinschnitz** ok thank you 


---
**Don Kleinschnitz Jr.** *March 05, 2018 11:30*

**+Art D.** BTW I changed out my water sensor and like this setup much better:

[donsthings.blogspot.com - Improved K40 Cooling Circuit](http://donsthings.blogspot.com/2018/01/improved-k40-cooling-circuit.html)


---
**Space Ronin** *March 05, 2018 18:47*

**+Don Kleinschnitz** thanks for the update 


---
*Imported from [Google+](https://plus.google.com/103130365677659082439/posts/Pt3pZx5dP9G) &mdash; content and formatting may not be reliable*
