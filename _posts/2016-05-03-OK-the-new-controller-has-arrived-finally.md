---
layout: post
title: "OK, the new controller has arrived finally"
date: May 03, 2016 16:28
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
OK, the new controller has arrived finally. Now it is time to install it and see how LaserWeb2 and Smoothieware get along. Mot anticipating any major problems, hopefully there will be a performance increase!





**"Anthony Bolgar"**

---
---
**Jim Hatch** *May 03, 2016 16:40*

+1 It's the next mod for me but right now I'm in the middle of crafting a present for my son and another for my wife (mother's day is coming). My change over will have to wait until next week I think.


---
**Damian Trejtowicz** *May 03, 2016 16:50*

I will be greatfull if You show how all was connected beetwen board and psu because i have problem with pwm and laser on pins


---
**Anthony Bolgar** *May 03, 2016 19:23*

Will post my efforts as soon as I get a chance to do the mods.


---
**Jean-Baptiste Passant** *May 03, 2016 19:25*

**+Damian Trejtowicz** Please contact me, you are probably having the same problem I had.

Did you follow the blue-box guide ? 

The PWM pin goes to the IN for my laser, and I had to add a Laser Switch configured with G0 and G1 to get everything working.



I do not have it near me, but with this information you'lee be good to go in minutes ;)


---
**Anthony Bolgar** *May 03, 2016 19:29*

Mine is going into a RedSail LE400, but I have replaced the PSU with the same one that is in my K40


---
**Damian Trejtowicz** *May 03, 2016 19:32*

**+Jean-Baptiste Passant** temporary i followed thru schematics because im still waiting for my board,

what you mean laser switch configured with g0 and g1?


---
**Jean-Baptiste Passant** *May 03, 2016 20:02*

You need to add a switch config in Smoothieware config file, I've called it laser.switch, and you just tell him that the ON signal is G1 and off is G0. Look for "switch smoothieboard"


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/8Ce6WsktA63) &mdash; content and formatting may not be reliable*
