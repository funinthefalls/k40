---
layout: post
title: "So I finished my air assist upgrade and am now looking at the water flow switch I bought"
date: July 14, 2016 23:07
category: "Modification"
author: "Jeremy Hill"
---
So I finished my air assist upgrade and am now looking at the water flow switch I bought.  I bought this based on some recommendations to do so, however I can't seem to find the post(s) on it now to determine how to wire this thing in.  Any help on that would be appreciated.  Thank you.



![images/e6817d67a5876ac9d7deab19ca5b6e4f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e6817d67a5876ac9d7deab19ca5b6e4f.jpeg)
![images/b9a6006c2341067a896189deb7750065.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b9a6006c2341067a896189deb7750065.jpeg)
![images/954adbb0653967c995516222eeebd36d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/954adbb0653967c995516222eeebd36d.jpeg)

**"Jeremy Hill"**

---
---
**Derek Schuetz** *July 15, 2016 00:53*

You just wore it in between your laser on switch just like any other switch. But first hook it up to see if your pimp has the power to trigger it. Mine couldn't 


---
**Jeremy Hill** *July 15, 2016 01:03*

I tried to blow through both ends and wasn't able to push it open.  I was concerned the water pressure may not be able to do that either.  We will see.


---
**Derek Schuetz** *July 15, 2016 02:04*

There isn't 2 ends. Just the t connector that lets water push against the switch


---
**Phillip Conroy** *July 15, 2016 05:20*

To wire a water flow switch cut 1 wire going to the laser enable switch on the laser cutters control pannel ,strip back 10mm if insulation and join a wire going to the flow switch ,the flow switch should have only 2 wires [or connections]if it has more than 2 it is a water flow metering device that measures how much flow is going through it and is not usable with out more electronics added-you need a 2 wire water flow switch- then connect a wire to the othe end of the wire you cut going to the laser enable switch and run it to the flow switch ,------make sure you cover any bare wires with heat shrink tubing or electrial tape.

With the flow switch wired up in series this will alow the laser head to move ,however laser will not fire unless the flow switch has water pressure at the switch-on mine i also upgraded to a bigger water pump to make up for any pressure/flow drop going through the flow switch


---
**Tony Sobczak** *July 15, 2016 07:07*

Where did you get the switch?


---
**Phillip Conroy** *July 15, 2016 07:21*

Ebat where else,[http://www.ebay.com.au/itm/New-Plastic-Outer-Thread-Water-Flow-Switch-Water-Flow-Sensor-Magnetic-/141886766213?hash=item21091c3c85:g:tS8AAOSwCypWpeLk](http://www.ebay.com.au/itm/New-Plastic-Outer-Thread-Water-Flow-Switch-Water-Flow-Sensor-Magnetic-/141886766213?hash=item21091c3c85:g:tS8AAOSwCypWpeLk)


---
**Jeremy Hill** *July 15, 2016 12:26*

Finally found where I bought the item.  [http://www.lightobject.com/Water-flow-pressure-sensor-Ideal-for-CO2-laser-water-protection-P815.aspx](http://www.lightobject.com/Water-flow-pressure-sensor-Ideal-for-CO2-laser-water-protection-P815.aspx)


---
**Coherent** *July 18, 2016 11:33*

I bought and use the same switch. I can blow in mine and trip the switch. When I first installed it near the machine, it wouldn't always work, so I moved the switch closer to the pump and it operated fine. There is an adjustment to set the pressure required to trip the switch. Turn it to the lowest pressure setting.


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/NChohmGVqXt) &mdash; content and formatting may not be reliable*
