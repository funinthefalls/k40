---
layout: post
title: "I'm having issues with consistent cutting. I think the problem is the beam isn't hitting the middle of the lens"
date: December 12, 2015 10:15
category: "Discussion"
author: "David Wakely"
---
I'm having issues with consistent cutting. I think the problem is the beam isn't hitting the middle of the lens. Any suggestions on how I rectify this?



Cheers





**"David Wakely"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2015 11:40*

I'm also having same issue, so will watch this post too in case someone knows how to fix.


---
**Anthony Bolgar** *December 12, 2015 12:05*

Do you mean consistent during a job, or from one job to another. If the inconsistency is in different parts of the same job, it is an alignment issue. If it is from job to job, I have no clue.


---
**Stephane Buisson** *December 12, 2015 12:24*

water temp ?


---
**Scott Thorne** *December 12, 2015 12:46*

Align the cutting head mirror to your work piece with the lens out of the head, if it's lined up right the burn will be a perfect circle around 2mm in diameter, if it is not lined up right it won't travel 90 degrees downward to your work and it will be distorted, almost like a half moon, this method only works with air assist models.﻿


---
**Scott Thorne** *December 12, 2015 12:51*

This was the problem with my engraver when I had the same cutting problem and I got the above tip from another laser engraver manufacturer. Hope it helps but your issue might be something different, but something to look at.


---
**Anthony Bolgar** *December 12, 2015 12:54*

Good tip Scott


---
**Scott Thorne** *December 12, 2015 12:58*

Yeah...I learned the hard way that the only way to see if the beam is making it to your work piece unobstructed is to take the lens out, if it's not the only thing you have to do is to turn the head so that the beam travels 90 straight down...it makes sense if you think about it....wish I could say I thought it up...but I didn't...lol


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 12, 2015 14:13*

I just figured out the issue I've been having with cutting. I recently removed the normal bed & just installed my perforated gal steel piece in instead. Turns out, lowering the entire thing by 4mm. This has caused me to not be able to cut cleanly or at same settings as before. I just did a test, placing a 4mm piece of leather under the piece I wanted to cut & it cut perfectly & cleanly.



Maybe check the distance of your cutting bed to the lens.



I tested the lens alignment checking method that **+Scott Thorne** suggested too. Mine came out with a perfect ~2mm circle :)


---
*Imported from [Google+](https://plus.google.com/107463961369004045306/posts/1eXh1xeudHS) &mdash; content and formatting may not be reliable*
