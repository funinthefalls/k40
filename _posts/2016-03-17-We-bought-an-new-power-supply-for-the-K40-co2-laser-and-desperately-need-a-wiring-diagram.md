---
layout: post
title: "We bought an new power supply for the K40 co2 laser and desperately need a wiring diagram"
date: March 17, 2016 04:26
category: "Hardware and Laser settings"
author: "mwdynes"
---
We bought an new power supply for the K40 co2 laser and desperately need a wiring diagram.



Here's the new one.

[http://www.ebay.com/itm/1Pcs-New-40W-Power-Supply-CO2-Laser-Engraving-Cutting-Machine-110V-DIY-Sale-/151488364958?hash=item234569019e](http://www.ebay.com/itm/1Pcs-New-40W-Power-Supply-CO2-Laser-Engraving-Cutting-Machine-110V-DIY-Sale-/151488364958?hash=item234569019e)



This is the old one.



[http://www.cnczone.com/forums/attachment.php?attachmentid=181813&d=1389533643&thumb=1](http://www.cnczone.com/forums/attachment.php?attachmentid=181813&d=1389533643&thumb=1)



We have a moshi board and need to figure out how to wire in the new PSU. We also have the 24V psu mounted on the side.





**"mwdynes"**

---
---
**mwdynes** *March 17, 2016 04:37*

[https://plus.google.com/117100954272831632852/posts/CNPZ3oU9rXc?pid=6262875441714164946&oid=117100954272831632852](https://plus.google.com/117100954272831632852/posts/CNPZ3oU9rXc?pid=6262875441714164946&oid=117100954272831632852)


---
*Imported from [Google+](https://plus.google.com/117100954272831632852/posts/eHoTfwoiZJm) &mdash; content and formatting may not be reliable*
