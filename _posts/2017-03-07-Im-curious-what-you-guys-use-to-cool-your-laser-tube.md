---
layout: post
title: "I'm curious what you guys use to cool your laser tube"
date: March 07, 2017 19:14
category: "Discussion"
author: "Nathan Thomas"
---
I'm curious what you guys use to cool your laser tube. Just water? If so how many gallons is the bucket and what kind of pump? Or something else...I thought I saw coolant or antifreeze mentioned on a youtube video. 





**"Nathan Thomas"**

---
---
**Mark Brown** *March 07, 2017 19:24*

It's recently been determined by Don and others that antifreeze is too conductive to be used near the high voltage of the laser.  So just distilled water, with a few drops of aquarium algaecide if algae is an issue.



I'm just getting into this stuff, so I'm using the cheapo aquarium pump that came with it.  Just a gallon of water for now, probably go bigger if I start using the thing constantly.  I've got a thermometer in the tank, if it gets too warm I can throw a frozen water bottle in.



Some people run water coolers or chillers, if they're running high production.



Ideal operating temperature is 15-20c higher than that and your laser power diminishes.  Somewhere above 30c you start shortening the life of the tube.


---
**Ned Hill** *March 07, 2017 19:37*

You can see this post here for some recent disscusions on the topics [plus.google.com - COOLING WATER "IT MATTERS" After flailing for a few days with what I thought...](https://plus.google.com/113684285877323403487/posts/jDRGVhd6zqy)

At this time I believe we have come to consensus that it's best to use distilled or DI water with a small amount of bleach or aquarium algaecide to prevent algae growth.  Just make sure the algaecide is not copper based.  Adding a small amount of alcohol (ethanol or isoproply) is probably fine as well.  Car antifreeze is a definite no and RV antifreeze is still not recommend as well.  I think most people use a 5gal bucket as a cooling water reservoir and add ice packs/frozen water bottles to the bucket as needed to keep the temp down during long jobs.  


---
**Nathan Thomas** *March 07, 2017 19:39*

**+Twelve Foot** thanks that's good to know. I have a 5 gal Home Depot bucket with the stock water pump that came with it. I thought the antifreeze was a bit much. But good look on the temp...i was curious about the operating temp. I knew above 30c was too high, but I'll make sure to keep it below 20. I'm just setting it up and have only run a couple engraving tests so far. No cuts yet. It stays between 23-25 c. But I'll cool it down. 


---
**Ned Hill** *March 07, 2017 19:42*

Up to 25C is unlikely to do any damage to the tube especially for short runs.  Less than 20C is just a conservative recommendation, especially for long runs.


---
**Nathan Thomas** *March 07, 2017 20:13*

**+Ned Hill** just read the whole post, awesome...gotcha 5 gal distilled water (ice as needed) + 1 oz bleach. Thank you sir


---
**Cesar Tolentino** *March 08, 2017 17:20*

I only use water with a few drops of bleach for algae or slime control. The more volume the better. It takes more time to heat up the water when there is more volume. 5 gallons gives me 10 minutes of work from 60f to 65f. Will be doing 10g aquarium soon. Hope this helps.



And when I hit 65f. My 8ma power goes down to 6ma. Btw I have a 3 year old co2 tube


---
**HP Persson** *March 08, 2017 22:44*

I´m using distilled water, biocide made for computer water cooling and a dash of water wetter, to bring down the surface tension of the water (stopping bubbles to form in the tube).

If water wetter is better or worse than dish soap i don´t know, but seen to conductivity it´s really low.


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/UCW562ZhzdS) &mdash; content and formatting may not be reliable*
