---
layout: post
title: "Hi, I'm looking for ABS/Acyrlic that engraves to a different colour"
date: November 22, 2016 22:29
category: "Materials and settings"
author: "Gerard Mullan"
---
Hi,



I'm looking for ABS/Acyrlic that engraves to a different colour.  I'm not sure what it's called or what to look for. Any ideas?





**"Gerard Mullan"**

---
---
**Vince Lee** *November 23, 2016 00:14*

It sounds like you want laminated (sign making) plastic.  I get mine at Inventables.com, which sells sheets in 8x12 sizes.


---
**Gerard Mullan** *November 23, 2016 00:31*

Thanks Vince


---
*Imported from [Google+](https://plus.google.com/117648854578727329148/posts/5NvUgym9NE2) &mdash; content and formatting may not be reliable*
