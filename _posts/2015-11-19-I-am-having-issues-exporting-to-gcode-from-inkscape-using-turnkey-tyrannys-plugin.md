---
layout: post
title: "I am having issues exporting to gcode from inkscape using turnkey tyranny's plugin"
date: November 19, 2015 00:26
category: "Software"
author: "Anthony Bolgar"
---
I am having issues exporting to gcode from inkscape using turnkey tyranny's plugin. When I run the gcode file on the laser cutter (uses a ramps/arduino control board) the laser only cuts on straight lines, all curves, circles etc make the motion moves but the laser itself turns off (0mA on the gauge). When it gets to a straight line again, it powers the laser back up. Anyone have any ideas what may be causing this problem?





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *November 19, 2015 22:27*

Anyone?


---
**Nathaniel Swartz** *November 20, 2015 03:18*

What about diagonal lines?  


---
**Nathaniel Swartz** *November 20, 2015 12:35*

Sorry I should note curves, circles, and diagonal lines require both steppers working at once.  I'm wondering if that power draw is preventing the laser from getting enough power


---
**Anthony Bolgar** *November 20, 2015 15:31*

I will try a diagonal line, thanks for the info.


---
**Anthony Bolgar** *November 20, 2015 17:41*

It cuts diagonal lines fine. Still scratching my head on this one.


---
**Nathaniel Swartz** *November 20, 2015 23:05*

So if you just draw a circle in inkscape it'll follow the circle but the laser won't activate?  Try making a test where you have a few  circles with different line thicknesses.  


---
**Anthony Bolgar** *November 21, 2015 16:33*

Line thickness makes no difference.




---
**Nathaniel Swartz** *November 28, 2015 18:44*

So I'm not sure if you fixed this yet, but I have an idea.  Draw a circle in inkscape then select it and go to the path menu.  Select object to path.  Then try to cut that circle.  I noted that inkscape circles and curves weren't exporting to dxf files today unless I converted them to paths first.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/SbVzuVfs49c) &mdash; content and formatting may not be reliable*
