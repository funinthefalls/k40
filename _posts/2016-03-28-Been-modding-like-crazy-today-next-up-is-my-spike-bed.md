---
layout: post
title: "Been modding like crazy today, next up is my spike bed"
date: March 28, 2016 22:41
category: "Modification"
author: "HP Persson"
---
Been modding like crazy today, next up is my spike bed.

Made from 4mm acrylic with 23mm nails, 30mm spacing.

(probably need to add one more between them)



Not tried it yet, almost 1 am here in Sweden.



Tomorrow it´s time to motorize the adjustable head with a small motor and wormdrive, and threaded rod aligning and holding it in position.

Will do a analog one, digital with stepper motors is waste of time for me.



What kind of bed do you have, and what do you like best for cutting?

![images/b67fe9e218492de7d050349f590ac532.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b67fe9e218492de7d050349f590ac532.jpeg)



**"HP Persson"**

---
---
**Stephane Buisson** *March 28, 2016 23:25*

nice,

the square hole at the bottom act as air intake maybe you should make some holes into your acrylic, or you will weaken the exhaust smoke efficiency. 



I find it useful as well to brush out the bits and pieces when cleaning the k40.


---
**HP Persson** *March 28, 2016 23:56*

Ah, good idea. Will put in some 10mm holes between the pins, can´t harm anything!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 29, 2016 02:11*

The new bed looks great HP. Due to me cutting small objects at times, I would probably increase the amount of nails, but depends what size objects you generally cut. Only other thing I would consider is some way to align pieces in the same spot again. Maybe some sort of jig that gives you TopLeft corner position.


---
**HP Persson** *March 29, 2016 09:04*

I´m making a "fence" on the top left part, and i will just run the laser to create a perfect corner into it.

Always 100% location of all materials :)



Most parts i make will fall down, not sure how to prevent that. A flat bed would be best for that, but bad for flashing and beam kickback into the material.



Had a idea of making a hex-bed, like some K40 are delivered with, but cannot find any material to use here, and ordering one from china was ~$80 with shipping :)

Maybe end up bying one of them, but wanted to try this $3 solution first :)


---
**Phillip Conroy** *March 29, 2016 09:09*

I use 10mm long magnets and 55mm screws ataches to the magnets ,i have only needed to use 6 screws /magnet combos so far to hold up my work - great because i can move around to suit work peace and remove to vacuum out without inpaling myself with the screws


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 29, 2016 09:39*

**+HP Persson** I'd imagine increasing the quantity of nails & decreasing their spacing (e.g. half the current distance) would give you better support & prevent them falling. I am considering this sort of setup with sewing pins instead of nails, as they are finer tip & would be thinner than the nails. Still a great idea, maybe just requires a few tweaks.


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/CMLnB7mSvs5) &mdash; content and formatting may not be reliable*
