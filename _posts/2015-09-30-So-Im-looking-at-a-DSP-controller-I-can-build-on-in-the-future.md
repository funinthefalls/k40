---
layout: post
title: "So I'm looking at a DSP controller I can build on in the future"
date: September 30, 2015 19:00
category: "Modification"
author: "Ashley M. Kirchner [Norym]"
---
So I'm looking at a DSP controller I can build on in the future. Basically something I can put in now and later add other features, such as an adjustable bed for starters. LightObject has an X7 ([http://goo.gl/FBwV6G](http://goo.gl/FBwV6G)) listed that I really like because of the ability to add end stops. Does anyone here have any experience with it? Any caveats (other than having to figure out the various wiring bits)? One of the comments also say to make sure one gets stepper drivers ... does the DSP controller not have that built in?





**"Ashley M. Kirchner [Norym]"**

---
---
**Anthony Bolgar** *September 30, 2015 19:22*

I am in the process of updating my controller, however I chose to use a ramps 1.4 board with Visicut as the controller software. It works very well, and can be done for approximately $75 USD. That is much cheaper than a DSP controller from LightObjects. If you want to pursue this option I can post the links to what is required and a couple of really well written build logs.


---
**Stephane Buisson** *September 30, 2015 19:23*

hi **+Ashley M. Kirchner**, if you are projecting yourself in the future,  may I suggest you keep an eyes on the smoothie conversion. Smoothie board have everything you need and more, compare X7, for a fraction of the price.

Even more interesting, is the software chain to go with, a serious improvement on today. (Fusion 360 or Visicut).

keep looking here, more news in the next weeks ...


---
**Ashley M. Kirchner [Norym]** *September 30, 2015 19:48*

**+Stephane Buisson**, thanks for the suggestion. I actually did look at a Smoothie not too long ago but for a custom 3D printer. One of the things that I didn't like about it is the placement of the SD card, USB, and Ethernet ports, mainly the SD card. Right now the design puts the SD card near the Ethernet and USB ports. Having <b>easy</b> access to an SD card to me is more valuable than the USB and Ethernet ports. Generally the latter two will be at the back of a machine that doesn't normally get touched, whereas the SD card needs to be accessed on a front panel. Having the ability to move that off of the main board would make it more appealing, to me at least. Additionally, having those ports tied to the main board also limits where that will sit inside of an enclosure, near a back or side wall, so one can access them. That may not always be ideal. Personally I prefer to have control boards that are near the power supply, with user accessible ports near a panel. It avoids the main board being jiggled/moved as wires are plugged in or removed. In my own electrical designs, I keep user-accesible ports off of the main controller boards for whatever I'm making. Stuff gets connected with ribbon cables. Please note, these are all my personal opinion and no way makes Smoothie any less of a product. It's a great board, just not my flavor. :)


---
**Ashley M. Kirchner [Norym]** *September 30, 2015 19:51*

**+Anthony Bolgar**, can that board accept native Illustrator and AutoCAD files? That's my main drive for a replacement, the ability to use native formats that I'm already working with. I use AutoCAD/Inventor for my 3D Printer and Illustrator for the gigantic CNC that we have at my office.


---
**Anthony Bolgar** *September 30, 2015 20:39*

Visicut has drivers for the Ramps 1.4 boards (Marlin, just like a 3d printer). And Visicut currently supports  SVG, EPS, DXF and the VisiCut PLF (Portable Laser Format) natively. There is also a plugin for Inkscape that allows you to send the files right from Inkscape to Visicut. Also Visicut is freeware, it actually started as the authors bachelors thesis project. It is very reliable and full of features. You can learn more about the software here [https://hci.rwth-aachen.de/visicut](https://hci.rwth-aachen.de/visicut)


---
**Stephane Buisson** *September 30, 2015 22:58*

**+Ashley M. Kirchner** with Smoothieboard, you should go for a reprap screen to plug on. the SD card slot is under that screen (access sideway) , not to be confused with mini sd for firmware on main board.

the smoothieboard will fit in place of moshi (using same hole for usb/rj45), lcd should be next vu meter and pot. how to be fixed is not set yet as the flat cable could not exceed 30cm provided. I will 3D design a guide for SD card insert.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/W4ZabS5SZSy) &mdash; content and formatting may not be reliable*
