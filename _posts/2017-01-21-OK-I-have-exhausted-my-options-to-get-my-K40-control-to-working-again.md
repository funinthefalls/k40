---
layout: post
title: "OK, I have exhausted my options to get my K40 control to working again"
date: January 21, 2017 17:22
category: "Modification"
author: "timb12957"
---
OK, I have exhausted my options to get my K40 control to working again.  Anyone who wants to know the issue and what has been suggested as troubleshooting can search the forum for my old posts. I guess I will either buy a new unit, or replace the control in my malfunctioning one. I have heard discussion about replacing the control with something called a "smoothie". What are my options for replacing my control? What are the costs? Where do I order it? Is there a step by step tutorial for the swap out?  Thanks in advance for your help.





**"timb12957"**

---
---
**Alex Krause** *January 21, 2017 17:25*

**+Ray Kholodovsky**​ has a board that's a drop in replacement for the m2 nano board... Will take a little bit of work if you have a Moshi board


---
**Don Kleinschnitz Jr.** *January 21, 2017 17:31*

[http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html?m=1](http://donsthings.blogspot.com/2016/11/the-k40-total-conversion.html?m=1)


---
**timb12957** *January 21, 2017 17:36*

Alex, I don't even know what a Moshi board is, I will have to research it Don, I quickly scanned your instructions, can you estimate the cost if all I wanted to do was only the mod to restore 2d cutting to my broken machine?


---
**Don Kleinschnitz Jr.** *January 21, 2017 17:41*

Connect with **+Ray Kholodovsky**​ I think your costs are roughly the cost of that board unless something else is broke.

Keep in mind you are moving off a stock config and will use (albiet better and free) different software. 


---
**timb12957** *January 21, 2017 17:47*

Another option I have considered is if a member of this board could attempt repair, I would ship it to them (if in the US) But I realize the cost of shipping both ways, plus parts to repair it may exceed the cost of just buying a new laser.


---
**Alex Krause** *January 21, 2017 17:57*

If it is just the control board that's bad 100$ will be the approximate cost to fix


---
**Alex Krause** *January 21, 2017 17:57*

[http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)


---
**timb12957** *January 21, 2017 18:12*

During the attempts to repair it, on eBay I found a control board and digital readout someone had taken out of their unit because they were changing controls. 

I replaced the control board when it arrived, but the problem still existed. Now that replacement board could have been bad just like mine, but the seller claimed it to be good?? Unfortunately I have no way of knowing. But if my board and the replacement board had no issues, the problem lies elsewhere. 


---
**timb12957** *January 21, 2017 18:57*

Alex, Ok so now I know I have a M2 Nano board and not a Moshi. lol


---
**Alex Krause** *January 21, 2017 19:13*

**+timb12957**​ are you still having issues with it homing the machine? I am reading back on some previous post... Also does the laser fire?


---
**timb12957** *January 21, 2017 19:21*

It never homes. The laser will cut fine, it is a positioning issue only. I have manually moved the head to "home" and started a cut. When a rapid move is called, the motor oscillates during movement and after that, it looses positioning.


---
**Alex Krause** *January 21, 2017 20:05*

What do you mean oscillate? Like in missing steps or is there a grinding noise associated with it? I'm assuming you are referencing rapids for the Y axis?


---
**timb12957** *January 21, 2017 20:37*

Yes Y axis. And the reason I used the term ossilation is it seems the motor

is stuttering rapidly forward and backward. There is a grinding noise also

as it does this.


---
**Andrew Cilia** *January 22, 2017 15:18*

I had the same problem, it was a setup issue. 

1- make sure to set the correct controller type on the setup page. Look at your controller board for the board name. 

2- slow down the cutting speed. If it's too fast the motors will skip steps and lose position 


---
**Martin Dillon** *January 22, 2017 17:40*

I had a similar issue with my CNC plasma cutter and it was a bad connection to my stepper motors.  I went back and checked and tightened all connections and haven't had a problem since.


---
*Imported from [Google+](https://plus.google.com/113253507249342247018/posts/3oG3ZZoMpx4) &mdash; content and formatting may not be reliable*
