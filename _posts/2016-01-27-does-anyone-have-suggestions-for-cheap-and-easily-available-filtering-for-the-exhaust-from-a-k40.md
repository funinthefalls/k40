---
layout: post
title: "does anyone have suggestions for cheap and easily available filtering for the exhaust from a k40"
date: January 27, 2016 10:13
category: "Discussion"
author: "Justin Mitchell"
---
does anyone have suggestions for cheap and easily available filtering for the exhaust from a k40.  sticking the pipe out of the window isn't always an option, and often blows back into the room, and theres no way we would get permission for a proper external vent in this property.



I have tried a 4"  (100x250mm) carbon filter unit bought off ebay, and a much stronger extractor fan a 220m³/hr mixed flow job.  But the filter only lessen the smells not removes them and does not do very well with smoke.



Suggestions easily available from the UK/EU please. i see lots of cheap filter units sold in america but the postage is crippling.







**"Justin Mitchell"**

---
---
**Phillip Conroy** *January 27, 2016 10:25*

I cut a circle in an old board which i atached to the end of my laser cutter fan,a bit of tape seals it,i close the window on the board ,job done


---
**Justin Mitchell** *January 27, 2016 10:30*

**+Phillip Conroy** Alas the windows are all hinged not sash so that kind of trick doesn't work


---
**Phillip Conroy** *January 27, 2016 10:34*

In that case remove hole window and replace with mirine ply[water restant],the laser cutter fumes has poison niss gasses depending on what u cut,i can not see u filtering it for less than $1000


---
**Phillip Conroy** *January 27, 2016 10:42*

What r u cutting?,what about [http://www.aliexpress.com/item/Low-noise-and-effective-Fume-extractor-system-with-double-duts/1986856551.html?spm=2114.01010208.3.198.R5oE10&ws_ab_test=searchweb201556_2,searchweb201644_4_10001_10002_10005_10006_10003_10004_62_9737,searchweb201560_8,searchweb1451318400_6149,searchweb1451318411_6448&btsid=50d1af49-5205-443f-9cc5-2d4a75c1f8b2](http://www.aliexpress.com/item/Low-noise-and-effective-Fume-extractor-system-with-double-duts/1986856551.html?spm=2114.01010208.3.198.R5oE10&ws_ab_test=searchweb201556_2,searchweb201644_4_10001_10002_10005_10006_10003_10004_62_9737,searchweb201560_8,searchweb1451318400_6149,searchweb1451318411_6448&btsid=50d1af49-5205-443f-9cc5-2d4a75c1f8b2)


---
**Justin Mitchell** *January 27, 2016 10:46*

Mostly its acrylic that gets cut, but sometimes its plywood, or laserable rubber, etc We're a hackspace thats housed in rented office space, we can bodge together stuff but we have little budget and no option on building changes.




---
**Scott Marshall** *January 27, 2016 10:57*

A plywood box to hold furnace filters works, as do the aluminum filters used in grease traps. The furnace filter trick has been filtering sawdust out of shop extractors for years. If you use palin old fiberglass filters, they're not flammable and can be shaken out to clean. They won't get the fines, but they'll stop the big stuff.

Another way is to use a trap made from a large drum. Also a woodworkers trick. 

Look to the woodworkers forums like Sawmill and you'll find plans if you need them.



Scott


---
**Jarrid Kerns** *January 28, 2016 01:50*

Hinged window at the side, top, or bottom? You couldn't make accordion style side flaps to seal the sides/ends? I guess the problem would be attaching it all but I think it could be done with limited damage. 


---
*Imported from [Google+](https://plus.google.com/+JustinMitchell/posts/6qSs5ER14zq) &mdash; content and formatting may not be reliable*
