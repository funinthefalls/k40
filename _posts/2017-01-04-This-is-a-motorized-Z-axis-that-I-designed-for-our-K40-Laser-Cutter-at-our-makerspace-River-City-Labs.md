---
layout: post
title: "This is a motorized Z axis that I designed for our K40 Laser Cutter at our makerspace River City Labs"
date: January 04, 2017 16:23
category: "Modification"
author: "Ryan Branch"
---
This is a motorized Z axis that I designed for our K40 Laser Cutter at our makerspace River City Labs. I just finished the write-up of it, and I thought I would share. We are using a Cohesion 3D mini board and smoothie to control it. The goal was to create a motorized z axis with very common parts (i.e. 8mm threaded rod, 608 bearings, nema 17, etc)





**"Ryan Branch"**

---
---
**Stephane Buisson** *January 04, 2017 16:37*

Excellent job, thank's for sharing.


---
**Alex Krause** *January 04, 2017 17:09*

Awesome!


---
**Don Kleinschnitz Jr.** *January 04, 2017 18:04*

Very nicely designed. I may build one when I cut the bottom from my machine.


---
**Kostas Filosofou** *January 04, 2017 18:05*

Excellent!


---
**Ryan Branch** *January 05, 2017 03:17*

Thanks!


---
*Imported from [Google+](https://plus.google.com/101842495658501740623/posts/czwnWDg6UsB) &mdash; content and formatting may not be reliable*
