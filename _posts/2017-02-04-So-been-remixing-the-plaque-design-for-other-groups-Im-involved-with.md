---
layout: post
title: "So been remixing the plaque design for other groups I'm involved with"
date: February 04, 2017 21:55
category: "Object produced with laser"
author: "Ned Hill"
---
So been remixing the plaque design for other groups I'm involved with.  The first one is Masonic, with the working tools of Freemasonry in the background. The other is for my college fraternity (Pi Kappa Phi) with the chapter name (Gamma Theta) in the background along with the pirate because we called ourselves the Gamma Theta Raiders.



![images/2fd38cc5f3cca0828874ebc55eff09bc.png](https://gitlab.com/funinthefalls/k40/raw/master/images/2fd38cc5f3cca0828874ebc55eff09bc.png)
![images/fcbb725602a9c932e887afd0327821b5.png](https://gitlab.com/funinthefalls/k40/raw/master/images/fcbb725602a9c932e887afd0327821b5.png)

**"Ned Hill"**

---
---
**Alex Krause** *February 04, 2017 23:40*

I used the same compass and square about 6 months ago in black walnut... It turned out amazing [plus.google.com - For my grandfather #Laserweb](https://plus.google.com/109807573626040317972/posts/DpwVRnvabB5)


---
**Ned Hill** *February 04, 2017 23:55*

Beautiful work!


---
**Alex Krause** *February 05, 2017 05:11*

Are you staining the frame pre or post laser?


---
**Ned Hill** *February 05, 2017 05:16*

Pre laser.  The frame and the center are both cut from the same piece.  Stained, sealed with a couple coats of lacquer, masked, etched/cut and then the centerpiece etching painted.


---
**Alex Krause** *February 05, 2017 05:23*

Thanks brother! I'm going to make an edge lit sign for my lodge in the near future I will pass on the plans if you are interested


---
**Ned Hill** *February 05, 2017 05:26*

Would defiently be interested.  An edge lit sign is on my to make list, thanks.


---
**Alex Krause** *February 05, 2017 05:27*

What equipment do you have at your disposal? CNC router? Or just a laser? Also are you wanting a wall hung or table mounted sign?


---
**Ned Hill** *February 05, 2017 05:28*

Just a laser at this point, a cnc router is on my wish list though.  I was thinking about a table mounted sign.


---
**Alex Krause** *February 05, 2017 05:29*

Cool I already have a design for a table top light base 


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/MxaKtr1cv6N) &mdash; content and formatting may not be reliable*
