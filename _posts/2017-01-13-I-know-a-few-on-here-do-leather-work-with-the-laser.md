---
layout: post
title: "I know a few on here do leather work with the laser"
date: January 13, 2017 18:45
category: "Materials and settings"
author: "Carl Fisher"
---
I know a few on here do leather work with the laser. What is a good source for leather that is safe to laser and easily engraved and cut on a K40 style machine? 



I'm looking to make some custom pen rolls and sleeves for the pens I make so something soft that will roll easy and not stiff like the wallet leathers.



Thanks in advance.





**"Carl Fisher"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 14, 2017 03:56*

A sheep or goat hide is usually very thin & super soft. Cuts very easily on the K40 too. A good source for leather is usually Tandy Leather, although I'm not sure if they have something like this.


---
**Stephane Buisson** *January 14, 2017 09:25*

on Ebay in some country, you can find some left over from Sofa/furnitures factory by the Kg (they have tons of it) it's very cheap. I bought 4+2 Kg of it from 2 suppliers, one very cheap another more quality orientated. both aren't bad,  pieces not too small dispite funny shapes and  colors. all in all I am happy, good enought to make my first experience with my new craft experience.


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/h3bwEKK6QB2) &mdash; content and formatting may not be reliable*
