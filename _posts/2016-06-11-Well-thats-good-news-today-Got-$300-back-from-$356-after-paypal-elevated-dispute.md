---
layout: post
title: "Well thats good news today. Got $300 back from $356 after paypal elevated dispute"
date: June 11, 2016 16:07
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
Well thats good news today. Got $300 back from $356 after paypal elevated dispute. 

Note to others: document / take pictures of everything while unboxing testing. 

What to look for: 

1 - Shipping box is in good shape

2 - Inside the laser lid you will find accessories. Look for depending on what you bought: expandable exhaust pipe, blower, water pump, hoses in/out for cooling, silicon paste, usb key, cd, manual. Test pump and blower immediately.

3 - On the back is the tube. It should come with a plastic cover. Look very closely for cracks or similar

4  -  On the control compartment you will find the board and power supply. Look that all cables are connected

5 - With flashlight or similar inspect all mirrors and  focus lens. They should be without any brown spots or markings



If im missing something please include. I think its important that new buyers know what to do / expect





**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *June 11, 2016 16:17*

Did you have to send the original back? If so, why did PayPal think 85% of your money back was okay?


---
**Ariel Yahni (UniKpty)** *June 11, 2016 16:24*

**+Jim Hatch**​ did not sent it back. I claimed that amount based on what I needed to solve my issues. New tube, crushed exhaust, broken endstop, damaged mirror. I'm must say that it still open for me to request the remaining amount as I did not claim the faulty mirror at the beginning 


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2016 17:46*

Good. About time too. 


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2016 17:48*

I paid 338 shipped. Same seller/ link as **+Alex Krause** but $20 cheaper than he paid. Looks like you paid similar to him. 



I consider anything under $340 the best price so far. 


---
**Ariel Yahni (UniKpty)** *June 11, 2016 18:06*

Yes globalfreeshiping.  Mind that all prices dropped since we bought. 


---
**Ray Kholodovsky (Cohesion3D)** *June 11, 2016 18:11*

That's the one!  Bummer to hear they didn't agree to a refund themselves.


---
**Ariel Yahni (UniKpty)** *June 11, 2016 18:13*

They did bust their final answer was 200


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 12, 2016 02:00*

Great to hear that you got this resolved to a satisfactory result. Now you can get on happily with your upgrades/mods/lasering.


---
**Ben Walker** *June 12, 2016 16:03*

They requested mine back (free global)... I'm a little perplexed that they did that as they had me take it apart and put it back together.   Wires cut.  Resoldered, ETC.  I'm concerned that they are going to get it back and then refuse the refund.   I'm hoping for a full refund even though they actually contradict themselves in the listing.   But if I lose the $10 ship charge I'm fine.   If they charge me a restock or return ship fee,  I'm going ape.   Lol


---
**Jim Hatch** *June 12, 2016 16:43*

Save everything - pictures, emails, receipts, the whole ball of wax.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/9tdToZvZAhd) &mdash; content and formatting may not be reliable*
