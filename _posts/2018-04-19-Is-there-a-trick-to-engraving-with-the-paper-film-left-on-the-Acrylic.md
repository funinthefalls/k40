---
layout: post
title: "Is there a trick to engraving with the paper film left on the Acrylic?"
date: April 19, 2018 15:06
category: "Discussion"
author: "Bill Keeter"
---
Is there a trick to engraving with the paper film left on the Acrylic? If I remove the paper I get a nice white engrave. If I leave the paper on the acrylic the engraving gets a brown color. I'm assuming from the burned paper leaving a dirty color to the engrave.



I'm running a 60w with air assist with LightBurn. 30% power at 200 mm/sec. It's funny cus I didn't have this problem with the 40w using LaserWeb. <b>*scratches head*</b>









**"Bill Keeter"**

---
---
**Bill Keeter** *April 19, 2018 16:46*

hmm... Maybe instead I should start masking off my acrylic with that white transfer paper used with Vinyl cutters. I know that was a trick I had been shown at TechShop (man, I can't believe they went under)


---
**Joe Alexander** *April 19, 2018 18:52*

try using some liquid soap to leave a film before engraving without the paper. Then rinse the parts afterwards, it seems to help some.


---
**HalfNormal** *April 19, 2018 19:34*

I use blue tape.


---
**syknarf** *April 20, 2018 17:48*

**+HalfNormal** where do you buy the blue tape? I can't find it locally here in my country...


---
**HalfNormal** *April 20, 2018 19:54*

The blue tape is used to mask for painting. It's just a higher quality than regular brown masking tape. I am sure you might be able to find something where you're at and if not the 3M brand I find is the best.


---
**Bill Keeter** *April 20, 2018 20:02*

This white transfer paper came in today. Hopefully it works.  Then I’ll hunt down a cheaper supplier. 



UPDATE: deleted link. This paper is used for tracing designs from paper to a new surface. Doh. Not the tacking stuff used in vinyl. Fail. 


---
**syknarf** *April 26, 2018 10:59*

**+HalfNormal** I tryed many types of masking tapes, but all of them left so many residue on the acrylic after engraving. Blue tape is the only one that my local stores don't have ( and most of them didn't know about it...)


---
**Bill Keeter** *April 26, 2018 11:14*

Took a bit, but I found the issue. My compressor was pushing too much air at the nozzle. So now I have the air flow at the very low end and I’m engraving on cast acrylic with paper backing much much better


---
*Imported from [Google+](https://plus.google.com/113403625293456436523/posts/HsnGtWpu9B5) &mdash; content and formatting may not be reliable*
