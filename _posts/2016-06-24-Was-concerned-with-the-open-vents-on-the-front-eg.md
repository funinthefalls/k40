---
layout: post
title: "Was concerned with the open vents on the front (e.g"
date: June 24, 2016 06:04
category: "Discussion"
author: "Evan Fosmark"
---
Was concerned with the open vents on the front (e.g. stray beams making it through), so I cut some quick filters. Used thick foam double sided tape so it still gets airflow from the top and bottom. 

![images/3525b79a6dfd7fd4ae94d7b211e80b93.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3525b79a6dfd7fd4ae94d7b211e80b93.jpeg)



**"Evan Fosmark"**

---
---
**Robert Selvey** *June 24, 2016 17:19*

Did your unit come with air assist or did you add it ?


---
**Evan Fosmark** *June 24, 2016 17:20*

Added it last night :)


---
**Robert Selvey** *June 24, 2016 18:03*

what all did you have to buy to add air assist ?


---
**Evan Fosmark** *June 24, 2016 18:58*

Air Assist Head: [https://amzn.com/B0094WLPYK](https://amzn.com/B0094WLPYK)



Air Pump: [https://amzn.com/B002JPM91W](https://amzn.com/B002JPM91W)



Self-Retracting tubing: [http://www.mcmaster.com/#9148t125/=12zuegz](http://www.mcmaster.com/#9148t125/=12zuegz)



You'll also need to get a coupler to attach the self-retracting tubing (5/16" ID) to the air pump (1/4" ID). What I did was wrap the end of the self-retracting tubing and got a snug fit into the air pump, since the tubing has an OD of nearly 1/4".


---
**Robert Selvey** *June 24, 2016 23:40*

The pump link is the same link as the air assist head


---
**Evan Fosmark** *June 25, 2016 17:06*

Whoops, updated!


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/YnXfkJ7gt7S) &mdash; content and formatting may not be reliable*
