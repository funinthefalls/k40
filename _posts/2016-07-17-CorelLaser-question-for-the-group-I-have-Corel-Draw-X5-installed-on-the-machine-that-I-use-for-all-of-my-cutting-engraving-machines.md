---
layout: post
title: "CorelLaser question for the group. I have Corel Draw X5 installed on the machine that I use for all of my cutting/engraving machines"
date: July 17, 2016 04:19
category: "Discussion"
author: "Terry Taylor"
---
CorelLaser question for the group. I have Corel Draw X5 installed on the machine that I use for all of my cutting/engraving machines. As that system is WIndows 10, I have the disappearing menu bar problem. What I am THINKING of doing is to remove X5 and install the free version of X7 Home and Student that Corel gave me when I upgraded my main design computer to X7. So, does anyone know if CorelLaser will work with the Home and Student Edition?





**"Terry Taylor"**

---
---
**Anthony Bolgar** *July 17, 2016 05:54*

I do not know for sure, but I have heard rumors of problems using Home and Student versions.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 17, 2016 14:39*

Anthony's correct, from reports of a couple of other users here Home & Student versions don't work. Although I don't know from experience.


---
**Tony Schelts** *July 17, 2016 15:43*

I bought home and student. I does not work and I don't know how to get it to work.  so I design in x7 student and save as x5 and cut that way.  I like x7 nice to use.  But if you want to save money the hidden menu isn't a problem just an iritatiion.  If someone actually gets home and student working with corel laser please let me know.


---
**Jim Hatch** *July 17, 2016 16:32*

It won't work. The exe file name is different and there are differences in what it sets for the location of the executables as well as the registry entries. 



It is possible to use H&S editions but you'll need to copy & rename the exe, change the registry entries and either move the directories (and rename them) or put in some redirects at the OS. It fast became not worth it to me 😀


---
*Imported from [Google+](https://plus.google.com/101437162293551160837/posts/AUfE11zA9X5) &mdash; content and formatting may not be reliable*
