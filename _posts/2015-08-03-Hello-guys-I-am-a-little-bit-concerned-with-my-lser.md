---
layout: post
title: "Hello guys, I am a little bit concerned with my lser"
date: August 03, 2015 03:53
category: "External links&#x3a; Blog, forum, etc"
author: "Jose A. S"
---
Hello guys,



I am a little bit concerned with my láser. I remember it was easy to cut cardboard of 1mm thickness with a power of 4mA but now I have encreased the power to 10mA and nothing happened. I would like to hear your suggestions and advice. I have not used my láser moré than 10hrs and I have never increased the power to more than 10mA.



Thks



Best Regards





**"Jose A. S"**

---
---
**Joey Fitzpatrick** *August 03, 2015 14:05*

Typically this is caused by a dirty focal lens. The smoke from the cardboard or wood creates a film on the lens which reduces the effective power of the laser.  Try cleaning your lens with some alcohol or acetone and a clean Q-Tip.  Do you have Air assist on your machine?(This will help prevent the lens fro getting dirty)  You should also check your Mirror alignment.


---
**Jose A. S** *August 03, 2015 16:38*

Thank you so much for your help Joey. Do you have a mirror aligment guide?


---
**Joey Fitzpatrick** *August 03, 2015 17:21*

[http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf](http://www.floatingwombat.me.uk/wordpress/wp-content/uploads/2015/02/K40-Alignment-Instructions.pdf)


---
*Imported from [Google+](https://plus.google.com/115161571395599639070/posts/AKmtoUDLxko) &mdash; content and formatting may not be reliable*
