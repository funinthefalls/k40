---
layout: post
title: "I have seen pictures of air assist hose guides on here a few time any idea where I could get one"
date: February 23, 2016 21:54
category: "Discussion"
author: "3D Laser"
---
I have seen pictures of air assist hose guides on here a few time any idea where I could get one





**"3D Laser"**

---
---
**The Technology Channel** *February 24, 2016 09:17*

or here:



[http://www.ebay.co.uk/itm/10-x-20mm-1M-Open-On-Both-Side-Plastic-Towline-Cable-Drag-Chain-/191627463210?hash=item2c9de30a2a:g:PkoAAOSwDNdVn3w3](http://www.ebay.co.uk/itm/10-x-20mm-1M-Open-On-Both-Side-Plastic-Towline-Cable-Drag-Chain-/191627463210?hash=item2c9de30a2a:g:PkoAAOSwDNdVn3w3)






---
**Richard** *February 24, 2016 19:29*

Instead of a rigid plastic that could potentially get in the way, how about this? A self-retracting coiled hose for the air? It would constantly pull itself semi-tight. [http://www.mcmaster.com/#9148t125/](http://www.mcmaster.com/#9148t125/)


---
**Richard** *February 24, 2016 19:30*

This video shows it in action, which is where I got the idea (and the link)... 
{% include youtubePlayer.html id="jnpnJ1FvcQU" %}
[https://www.youtube.com/watch?v=jnpnJ1FvcQU](https://www.youtube.com/watch?v=jnpnJ1FvcQU)


---
**Arestotle Thapa** *February 25, 2016 14:11*

I bought this ( [http://www.amazon.com/gp/product/B00843VYGO](http://www.amazon.com/gp/product/B00843VYGO) ) and tried to install but it was too big and rigid. May be the one linked by Carl works better.


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/UqPeAtE2zMy) &mdash; content and formatting may not be reliable*
