---
layout: post
title: "I am thinking of adding in a small network range extender that has at least one ethernet port to use with my smoothieboard when it arrives"
date: April 27, 2016 07:05
category: "Smoothieboard Modification"
author: "Anthony Bolgar"
---
I am thinking of adding in a small network range extender that has at least one ethernet port to use with my smoothieboard when it arrives. I will then use an 8" windows 10 tablet as my touchscreen interface along with Laserweb/Laserweb 2, but would still like to use Pronterface if possible. Does anyone know if pronterface can access the laser via ip address or is it only usable with USB. The other option  I was considering is using an old dell mini that I could install in the case and access via Teamviewer 11along withe the network range extender. Anybody have any thoughts as to the best method of the 2 (or an option I have not considered)?





**"Anthony Bolgar"**

---
---
**Anthony Bolgar** *April 27, 2016 07:35*

Thanks, I will just use a range extender built into the case of the LE400 (It has a ton of room in the case, so easy to add stuff in.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 27, 2016 07:58*

An option I have been considering is a stick PC to run the whole lot, HDMI to monitor, then have access to USB ports on the stick comp (so can run a powered hub off it to connect the peripherals).


---
**Anthony Bolgar** *April 27, 2016 08:03*

I am pretty sure the tablet I have will run all the software needed. I like the idea of having a wireless remote touchscreen for the control interface. **+Peter van der Walt** **+Arthur Wolf** Can smoothieboard have both the ethernet and USB hooked up at the same time and have the ability to choose which one to use?


---
**Arthur Wolf** *April 27, 2016 08:05*

You can use both Ethernet and USB at the same time.

And you can use Pronterface via Network no problem too : [http://smoothieware.org/pronterface](http://smoothieware.org/pronterface)


---
**Anthony Bolgar** *April 27, 2016 08:13*

Thanks Arthur for the quick reply. I have decided on using ethernet via the range extender router, with a usb port on the case for backup in case I need the power of my laptop, or if the tablet needs charging or dies.Really looking forward to getting this LE400 conversion underway, just waiting on a bunch of parts that should be here witnin a couple of weeks. It will also be nice to keep all my files on one of my file servers (An HP media center box, just a glorified NAS device with 6 TB of storage space. That way I can work on projects from any of my work stations or laptops, even when away from home, then just pull the file off the server when it is time to cut. I will put a 12864 LCD with encoder on the controll panel as an back up of last resort using the SD card reader. But I doubt I will ever need to use it. Does smoothieware have a GUI to change the settings, or do you have to edit the config file manually?


---
**Arthur Wolf** *April 27, 2016 08:35*

Right now you still have to edit config file manually, but a GUI is coming.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/SjVdCZDgwxg) &mdash; content and formatting may not be reliable*
