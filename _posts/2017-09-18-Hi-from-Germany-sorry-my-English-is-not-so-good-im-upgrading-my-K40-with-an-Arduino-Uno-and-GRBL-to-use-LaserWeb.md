---
layout: post
title: "Hi from Germany (sorry, my English is not so good), i'm upgrading my K40 with an Arduino Uno and GRBL to use LaserWeb"
date: September 18, 2017 10:31
category: "Hardware and Laser settings"
author: "mannheimhoch2"
---
Hi from Germany (sorry, my English is not so good),



i'm upgrading my K40 with an Arduino Uno and GRBL to use LaserWeb. The steppercontroll works fine, but i'm confused about the PSU connection.



Grbl controls laser power by varying the 0-5V voltage from the spindle pin (D11).  But how can I "fire" the laser? Will it work if I only connect D11 to "IN" and GND to GND? I thought the PSU needs a "FIRE"-signal at "L"... and what´s with the potentiometer?



Here are some pictures...



I hope anyone can help me! Tanks!!



![images/eeb24c088b2ea32a61f44df9a6de6883.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eeb24c088b2ea32a61f44df9a6de6883.jpeg)
![images/8e516b5ced6b60c1e8650afb4134ccb4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8e516b5ced6b60c1e8650afb4134ccb4.jpeg)
![images/a3cca178a847ba59edb88fd0005aa7f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a3cca178a847ba59edb88fd0005aa7f2.jpeg)

**"mannheimhoch2"**

---
---
**Don Kleinschnitz Jr.** *September 18, 2017 13:13*

With a PWM signal: You need an "open drain" output from the arduino to drive "L" with a PWM signal. Leave the "IN" and the pot in tack



What kind of output are you trying to drive from? D11 on an arduino may work but if I recall it is not an open drain or collector. The L pin needs a ground to fire.



Can you point me to a schematic?



Community: Is anyone else on here already running from an arduino?



Youtube has a number of videos on arduino conversions.



You could save yourself some grief by using a C3D drop in board. **+Ray Kholodovsky**


---
**Nate Caine** *September 18, 2017 16:39*

If you leave the front panel "Current Regulation" potentiometer wired to the "IN" (Intensity) input, then it somewhat defeats the purpose of having everything under control of the Arduino.



Here's a couple of links discussing similar modifications.  (The second one is in German for your enjoyment!):



[github.com - CO2 laser · Issue #682 · grbl/grbl](https://github.com/grbl/grbl/issues/682)



[http://www.a-d-k.de/20150611_213938-Lasercutterreloaded.htm](http://www.a-d-k.de/20150611_213938-Lasercutterreloaded.htm)



They use a PWM output pin wired to "IN" to set the laser intensity.  This is controlled by the g-code Spindle speed command.  S1000



They use a separate digital output pin wired to "L" to turn the laser on and off.  This responds, roughly, to g-code G03 ("spindle on") and G05 ("spindle stop").



They discuss two approaches to match up the polarities of the control signals.  Either thru software or an add-on hardware inverter.



Recent HVPS power supplies might be able to handle it, but generally attempting to modulate the laser intensity by applying a PWM signal to the "L" input is not recommended.  Some supplies will cause this to repeatedly ignite the laser, which is not recommended by the laser tube manufacturers.  The repeated ignitions greatly diminish the laser tube's life.



--------------

(As an aside, refer to your photos.  Notice that the middle 6-pin connector and the <b>right</b> 4-pin connector BOTH have an "L" signal.  These are the same signal and are connected together right on the Power Supply circuit board.  You can wire to either; same effect.



 This is <b>NOT the same as the "L-" signal</b> on the <b>left</b> 4-pin connector.  That is for the Laser Tube Negative Return.  I think you know that, but mention it here to be complete.)


---
**Paul de Groot** *September 19, 2017 04:45*

Read my blog about connecting a grbl controller [http://awesome.tech/installing-the-k40-controller](http://awesome.tech/installing-the-k40-controller)


---
**mannheimhoch2** *September 25, 2017 10:06*

Hi again, 



thanks for your answeres. Here are my results... FYI...



I've connected the D11 (PWM) to "IN" and GND to GND. D13 pin is connected to "L" which fires the laser. That needs some modifications at the GRBL config.h:



- #define INVERT_SPINDLE_ENABLE_PIN - set to enable

- #define USE_SPINDLE_DIR_AS_ENABLE_PIN - set to enable



It works fine, but you need to start the controller first with deactivated laser because D13 will be pulled low during power up and laser burns at startup. I'm using the "Laser-Switch" connected to "WP" to switch off the tube.



One question: what´s the best frequency for PWM modulation? GRBL use 1kHz as default but with that setting my laser sounds like a old dot matrix printer. Should I change the prescaler? But the cpu_map.h only goes up to 7.8 kHz. The next step is "prescaler off" -> 62.5 kHz.



Thanks and greets from Germany!


---
**Paul de Groot** *September 25, 2017 22:35*

**+mannheimhoch2** Great to hear you got it hooked up correctly. Always start with the manual laser button on off. Once you are ready and start the process you press the laser button on the console. It's an extra safety option. The frequencies I use are able being produced by the 328PB. The standard 328 AVR cpu can not produce this and has just 8bit pwm (for the full power range 0-18mA). Engraving only works within a tiny part 0-2mA so you can only use a small part of the 256 values. Maybe you can use an opamp or resistors to reduce the pwm output. I had very little success with that and moved to the 328PB chip to get engraving going. If you use the standard uno then it works only for vector etching and engraving. If you are interested I do have some boards left from the kickstarter (see awesome.tech).


---
**Paul de Groot** *September 25, 2017 22:36*

**+mannheimhoch2** btw low frequencies only work well with a CO2 Tube(122-244 Hz)


---
**Don Kleinschnitz Jr.** *September 26, 2017 13:15*

**+Paul de Groot** "Once you are ready and start the process you press the laser button on the console. It's an extra safety option."



.... if you forget to turn off the laser button, when you power up you get a random laser fire..... <b>this is a safety option</b> ... :)

...or maybe a safety defect :)?.


---
*Imported from [Google+](https://plus.google.com/110619569135786652641/posts/BdHx1j5tAsw) &mdash; content and formatting may not be reliable*
