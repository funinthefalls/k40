---
layout: post
title: "90+ VIDEOS TAKE YOU FROM LASER CHUMP TO LASER CHAMP Article from Hackaday"
date: November 25, 2016 00:23
category: "External links&#x3a; Blog, forum, etc"
author: "HalfNormal"
---
90+ VIDEOS TAKE YOU FROM LASER CHUMP TO LASER CHAMP Article from Hackaday. Link below



Few of us document the progression of our side projects. For those who do, those docs have the chance at becoming a tome of insight, a spaceman’s “mission log” found on a faraway planet that can tell us how to tame an otherwise cruel and hostile world. With the arrival of the RDWorks Learning Lab Series, Chinese laser cutters have finally received the treatment of a thorough in-depth guide to bringing them into professional working order.



In two series, totaling just over 90 videos (and counting!) retired sheet-metal machinist [Russ] takes us on a grand tour of retrofitting, characterizing, and getting the most out of your recent Chinese laser cutter purchase.



Curious about laser physics? Look no further than part 2. Wonder how lens size affects power output? Have a go at part 39. Need a supplemental video for beam alignment? Check out part 31. For every undocumented quirk about these machines, [Russ] approaches each problem with the analytic discipline of a data-driven scientist, measuring and characterizing each quirk with his suite of tools and then engineering a solution to that quirk. In some cases, these are just minor screw adjustments. In other cases, [Russ] shows us his mechanical wizardry with a custom hardware solution (also usually laser cut). [Russ] also brings us the technical insight of a seasoned machinist, implementing classic machinist solutions like a pin table to produce parts that have a clean edge that doesn’t suffer from scatter laser marks from cutting parts on a conventional honeycomb bed.



Solid build logs are gems that are hard to come by, and [Russ’s] Chinese laser cutter introduction shines out as a reference that will stand the test of time.



YouTube Channel

[https://www.youtube.com/channel/UCqCyShJXqnElPTUnxX0mD5A](https://www.youtube.com/channel/UCqCyShJXqnElPTUnxX0mD5A)



Information from Hackaday

[http://hackaday.com/2016/11/24/90-videos-take-you-from-laser-chump-to-laser-champ/](http://hackaday.com/2016/11/24/90-videos-take-you-from-laser-chump-to-laser-champ/)





**"HalfNormal"**

---


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/eXK81kndgP4) &mdash; content and formatting may not be reliable*
