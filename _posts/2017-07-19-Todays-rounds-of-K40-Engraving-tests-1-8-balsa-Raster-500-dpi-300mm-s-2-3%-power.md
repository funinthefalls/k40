---
layout: post
title: "Todays rounds of K40 Engraving tests. 1/8\" balsa, Raster 500 dpi, 300mm/s 2-3% power"
date: July 19, 2017 05:47
category: "Object produced with laser"
author: "Kenneth White"
---
Todays rounds of K40 Engraving tests.  1/8" balsa,  Raster 500 dpi, 300mm/s 2-3% power.

the two same photos side by side were a test at the same settings, Left is Raster of Reworked Photo Increasing Brightness and contrast to get better details, Right is original photo engraved as is.  I personally feel with this particular photo that the original came out better then the touched up version.  #K40Whisper **+Scorch Works**



![images/52ef03c523817c5b0250529efa5ede6d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/52ef03c523817c5b0250529efa5ede6d.jpeg)
![images/f521d97f5b613bd0d315812486a6ace3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f521d97f5b613bd0d315812486a6ace3.jpeg)
![images/5816a3455c6855f3e0c802b94f91e229.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5816a3455c6855f3e0c802b94f91e229.jpeg)
![images/d5869475d749e07a739862062bb8d9e3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d5869475d749e07a739862062bb8d9e3.jpeg)
![images/eb7fc00821e6315826079ccdbb3385da.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb7fc00821e6315826079ccdbb3385da.jpeg)

**"Kenneth White"**

---
---
**Alex Krause** *July 19, 2017 05:52*

are these dithered images or  grayscale intrepretations?




---
**Alex Krause** *July 19, 2017 05:59*

I'll let you in on a secret :P ...95% of laser engraving is he prep work you put into an image.not all images are suitable for laser engraving as they are 

![images/988d107a86b78b0de41f10976a5794df.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/988d107a86b78b0de41f10976a5794df.jpeg)


---
**Kenneth White** *July 19, 2017 15:01*

**+Alex Krause**  Im not quite Sure, Im Using K40Whisper  and the HalfTone Function so pretty sure its Grayscale interpretations,  Wont be able to do actual dithering and fancy stuff until i stop being lazy and do the GRBL swap.


---
**Alex Krause** *July 19, 2017 15:03*

That image was the stock controller 


---
**Scorch Works** *July 19, 2017 15:58*

**+The Mad-Mapper** **+Alex Krause**​ 

I chose to use the term "halftone" instead of "dither" in K40 Whisperer because I think "halftone" is more of a household word.  I think "dither" is technically a more accurate term for what K40 Whisperer is doing since the dots are all the same size.


---
**Corey Mousseau** *July 19, 2017 20:39*

**+Alex Krause** What software do you use to prep your images?


---
**Alex Krause** *July 19, 2017 20:40*

GIMP


---
**Kenneth White** *July 19, 2017 21:15*

**+Alex Krause** anychance you would be willing to make a video of your full process Doing that minions one?  From Gimp To Whatever Software you used and the pot settings on the machine etc?




---
**Jared Roy** *July 25, 2017 19:53*

**+Alex Krause** I also could really benefit from a nice video helping with image prep ;)


---
**Alex Krause** *July 25, 2017 20:32*

There are a lot of variables to consider when engraving a photo on wood. Different species of wood engrave to varying levels of detail. The woods I have found that give the best results are maple,cherry, and alder. They tend to show a lot of contrast when engraved.



 The DPI level of the image you are using plays a big role in the outcome as well... I have found that 300-400dpi will yield very nice results.



The first thing I do when beginning to prep an image for laser engraving is to convert the image to grayscale and look for contrast between areas I want to emphasize and it's surroundings. Removing dark colored backgrounds will help to define people or objects in the engrave. Then adjust threshold and curve settings until the image is approximately 50%black and 50% light gray and white. 



 The dithering  method used can also change the way an image is engraved. Floyd sternberg, Jarvis, and stucki are the approaches I use most often. With Floyd sternberg used the most. 



Finally I sand the wood lightly with 400 grit sandpaper until it is smooth, this will reduce the darks from bleeding between pixels, because of loose wood fibers 




---
*Imported from [Google+](https://plus.google.com/100692370397257622708/posts/CTa3VqLqqV5) &mdash; content and formatting may not be reliable*
