---
layout: post
title: "I need a little bit of help making this into a modular design"
date: June 07, 2016 03:16
category: "Discussion"
author: "3D Laser"
---
I need a little bit of help making this into a modular design.  I want to make it the smaller cards can have this own tray that snap into the large ship card dial and token area could someone help with that 



![images/43152022cd58eaa98be297acf1a73c65.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43152022cd58eaa98be297acf1a73c65.jpeg)
![images/b2ef167706088c6d046de2d3ea8264fd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b2ef167706088c6d046de2d3ea8264fd.jpeg)

**"3D Laser"**

---
---
**Alex Krause** *June 07, 2016 04:08*

**+Corey Budwine**​ where did you get the original design?


---
**3D Laser** *June 07, 2016 05:07*

**+Alex Krause** I made it in laser draw


---
**Alex Krause** *June 07, 2016 05:17*

It wouldn't be too hard to make the design stackable can be done a few ways I would use a dowel locating system and expand the parameters of the two boards to accommodate the interlocking dowels 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 07, 2016 08:01*

I'd be happy to assist with ideas for redesigning for modularity however I am unfamiliar with this game & don't understand what are the "small cards" & the "large ship card dial" & "token area". Any chances of an image with each thing circled & marked where you want it to go?


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/GjKayjwPPAk) &mdash; content and formatting may not be reliable*
