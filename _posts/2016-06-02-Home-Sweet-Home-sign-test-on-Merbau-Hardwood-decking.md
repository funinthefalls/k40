---
layout: post
title: "Home Sweet Home sign test on Merbau Hardwood decking"
date: June 02, 2016 04:14
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Home Sweet Home sign test on Merbau Hardwood decking. 297 x 91 x 19mm.



Settings I used were 4mA @ 350mm/s @ 5 pixel steps. Quite a nice engrave effect. It actually doesn't seem to have gone into the wood much at all. I run my hand over the wood & across the edge of the engrave & can't feel the difference in height or anything. It almost looks like I just dyed/stained those sections (with extreme precision lol).



I'm pretty satisfied with the results, albeit it was not perfect alignment. Need to make a jig that is taller (as 1 layer of 3mm ply isn't suitable because the bottom edges of the decking are bevelled & they don't butt up against the ply properly).



Will look particularly nice with routed edges I think.



![images/f000d9fbb5f6194ea15d1fd8d1c1a88f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f000d9fbb5f6194ea15d1fd8d1c1a88f.jpeg)
![images/2993c5c3e6d651b47706309224156d29.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2993c5c3e6d651b47706309224156d29.jpeg)
![images/538b17f9f24c3af1a31493e0ebd94b79.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/538b17f9f24c3af1a31493e0ebd94b79.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Alex Krause** *June 02, 2016 05:14*

You will lose a bit of the bottom part of the S if you route the edge


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 05:19*

Yeah, I realised I've made this imagery slightly too large. Should have removed about 2mm of the height of the imagery, so there is a few mm on top & bottom to spare for routing. I think for this particular one, I will just route the left & right edges since the bottom & top are already slightly bevelled.


---
**Pippins McGee** *June 02, 2016 06:06*

looks nice! Yuusuf, i Searched and found an explanation from you on what pixel steps are.

"(basically the more pixel steps, the less it actually lasers, so the image ends up faded slightly)."



Are you able to explain technically what exactly is one changing when they are changing the pixel steps?

From a technical point of view, what is actually happening when you put a high pixel step value?

The laser usually fires and makes contact with your material once per pixel if you have it set to 1?

but if you have it set to 100 it will skip 99 pixels before firing as it goes across your image?



thanks


---
**Jeremie Francois** *June 02, 2016 06:09*

Impressive indeed! How long did it take?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 02, 2016 06:21*

**+Pippins McGee** I'm not 100% certain what the pixel steps actually does to be honest, but all I know is that increasing it to 3 or 5 speeds up the raster/engrave. In the process of doing this, the image ends up less dark (or deep, depending on material) & also seems to reduce the clarity of the image ever so slightly. I don't think you can set the value to 100, but it might be worth a shot to see what happens. From the settings page I notice that 1 pixel step is equivalent to 1000dpi (dots per inch). 2 pixel steps is 500dpi. So I assume 5 pixel steps would be about 200dpi. Still much higher than normal screen dpi (72dpi) but slightly less than high quality print (300dpi). So around the 3 pixel steps will give you 333dpi, so I just guess that means the laser fires less often than when set to 1 pixel step (1000dpi). So simply put, when changing the pixel steps you are actually changing the dpi of the final engrave.



**+Jeremie Francois** With the settings I used (350mm/s & 5 pixel steps) it took around 9.5 mins. I've uploaded a youtube video of the engraving (@ 500% speed) which is roughly 1m50s.


---
**Pippins McGee** *June 02, 2016 09:09*

**+Yuusuf Sallahuddin** thanks yuusuf thats really helpful. still getting to know these things. cheers


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/9QipvnFGsmZ) &mdash; content and formatting may not be reliable*
