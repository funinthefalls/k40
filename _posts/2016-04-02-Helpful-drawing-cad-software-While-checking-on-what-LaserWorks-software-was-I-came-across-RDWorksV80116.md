---
layout: post
title: "Helpful drawing/cad software While checking on what LaserWorks software was, I came across RDWorksV8.01.16"
date: April 02, 2016 16:09
category: "Software"
author: "HalfNormal"
---
Helpful drawing/cad software



While checking on what LaserWorks software was, I came across RDWorks—V8.01.16. I found a website that has it available for a free download (manufacture site). Out of curiosity I downloaded it and it installed just fine. It is made for a specific laser but the drawing abilities are all intact and it will import almost any format of drawing. Most important, it will export the file in .ai format. (adobe illustrator).

[http://en.rd-acs.com/down.aspx?TypeId=50075&FId=t14:50075:14](http://en.rd-acs.com/down.aspx?TypeId=50075&FId=t14:50075:14)







**"HalfNormal"**

---
---
**Scott Thorne** *April 02, 2016 16:32*

Laserworks is rdworks...

It comes with the cutter/engraver I have...the 50 watt sh350d....it's good software....20x12 work area...motorized z table, rotary attachment...all the trimmings...1800.00 on amazon...comes with the new 644xg ruida controller...of course I've made quite a few changes to the machine but I love it.


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/YhDAowmsMCr) &mdash; content and formatting may not be reliable*
