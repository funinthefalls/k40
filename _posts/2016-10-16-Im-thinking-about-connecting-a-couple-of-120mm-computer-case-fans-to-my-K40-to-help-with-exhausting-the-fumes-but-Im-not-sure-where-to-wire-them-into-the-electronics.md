---
layout: post
title: "I'm thinking about connecting a couple of 120mm computer case fans to my K40, to help with exhausting the fumes, but I'm not sure where to wire them into the electronics"
date: October 16, 2016 18:15
category: "Air Assist"
author: "Chris Kehn"
---
I'm thinking about connecting a couple of 120mm computer case fans to my K40, to help with exhausting the fumes, but I'm not sure where to wire them into the electronics. Has anyone done this before? If so, how did you plumb your electrical?



Air assist would be nice but for now my budget is timed.



Thanks!





**"Chris Kehn"**

---
---
**Anthony Bolgar** *October 16, 2016 18:54*

I would get a separate 12V power supply and run them from that with a simple on/off switch. The PSU in the K40 only puts out 24V and 5V and are only 1A. Get a cheap ATX PSU from a computer and you can plug the fans right in with no wiring required.


---
**HP Persson** *October 16, 2016 21:37*

I leave the K40 electronics alone, and as Anthony said, add everything on a different source.

If (when) you need to check something that doesnt work, you can be sure that your added lights, fans or whatnot isnt the issue :)



I use alot of extra fans, works perfect. No need for a loud big exhaust, fans move it to the back and the exhaust takes it from there.

Not using the original exhaust, but a quiet one.

The fans also helps keeping the mirrors clean, no smoke around the mirrors or lens getting burnt to the surface, less cleaning to do.

Saw a good difference when i started using the fans on cleaning.



As for air assist, go with a smaller fish tank pump and a copper/lastic pipe in the beginning, aim it on the surface where the beam hits and you will see a big difference.

When you saved some money you can grab a bigger air pump to fit your needs, if the smaller fish tank pump isnt enough :)


---
**Chris Kehn** *October 16, 2016 22:44*

Sounds good. Separate power supply for the accessories.



Thanks!


---
**Vince Lee** *October 18, 2016 04:56*

I run a small 40mm 24v fan mounted on the head with some side fins to direct the flow and it works great as cheap air assist.  I tried two fans and found one with really good flow.

![images/cce091545d2978e7700ab1740df4257d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cce091545d2978e7700ab1740df4257d.jpeg)


---
**Chris Kehn** *October 19, 2016 01:41*

Vince, good idea. So you have that fan hooked up to a separate power supply somewhere, right? 


---
**Vince Lee** *October 19, 2016 05:22*

**+Chris Kehn** I found a relatively low current fan so i added it to the existing supply.  I wouldn't necessarily recommend anyone else do this since the power supply rating is unknown but I chose to take the risk myself and it's all I added.


---
**HP Persson** *October 19, 2016 07:26*

Can also confirm good results with a laser head fan, better than i could expect :)

I do run this on a own PSU though, as i have much other stuff needing power.

![images/5836bb054d97c6a4dedb293758718f42.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5836bb054d97c6a4dedb293758718f42.jpeg)


---
*Imported from [Google+](https://plus.google.com/108027001410327769629/posts/FNgGYPpa1dA) &mdash; content and formatting may not be reliable*
