---
layout: post
title: "Current meter connection. I checked my connection and the the rings were extremely loose!!"
date: October 13, 2015 19:56
category: "Hardware and Laser settings"
author: "David Cook"
---
Current meter connection.  I checked my connection and the the rings were extremely loose!! And the nuts were tight.  I had to add the rings on the outside of the nuts and use a second set of nuts to tighten it. You can see passed the first set of nuts that the thread does not go all the way down.  Shown are antirotation ridges if the threaded stud.  These prevent the nut from tightening all the way down. 

Be sure to check this connection on your machine .

![images/03e7e0ded6acbefc362110016c5babfc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/03e7e0ded6acbefc362110016c5babfc.jpeg)



**"David Cook"**

---


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/G36PY4FkvK6) &mdash; content and formatting may not be reliable*
