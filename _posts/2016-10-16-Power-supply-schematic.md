---
layout: post
title: "Power supply schematic"
date: October 16, 2016 21:58
category: "Hardware and Laser settings"
author: "Paul de Groot"
---
Power supply schematic 





**"Paul de Groot"**

---
---
**Don Kleinschnitz Jr.** *October 16, 2016 22:10*

Awesome work, thanks TONS for this.Can we get the eagle file :).


---
**Paul de Groot** *October 16, 2016 22:11*

I have posted the eagle file as well. Could not work out with google drive to post this in one google+ post.😣


---
**Paul de Groot** *October 16, 2016 22:20*

**+Don Kleinschnitz** and you were right about sourcing the power limitation. I manage to tune the current to 1 A for the steppers and now all works fine. My issues all came from a current setting over 1 Amp and the 24v supply rectifier diode blew. There is no short protection either in the psu. Even my 12v 3amp $15 led power supply from china has current protection...


---
**Don Kleinschnitz Jr.** *October 16, 2016 22:31*

Since you by defacto are the resident LPS expert :), some questions if you don't mind:

1. Not clear on what "wired through connector to 5V" means on R18 and R2.

2. Are the "test"and "ON" switches the external switches on the control panel respectively, "Laser Test" and "Laser Switch" or are they switches on the power supply.

3. There are 2x "LO" signals one connected to R2 and one connected to IC-1 pin 2. Since there is only one LO signal which is on the DC connector (in mine its called "L"). What is the other and which is which. Is the "LO" signal on your drawing really "IN"? Or do I have this all messed up? 

4. My supply has 3 control connectors Laser Switch (-P, G); Test Switch (+K,-K); Current Regulation (G, IN, 5v) can you point me to them on this drawing? 



Thanks again ....

 


---
**Don Kleinschnitz Jr.** *October 16, 2016 22:37*

**+Paul de Groot** yes that is why in my conversion I have a fuse block :).


---
**Paul de Groot** *October 16, 2016 23:08*

**+Don Kleinschnitz**​

1. Not clear on what "wired through connector to 5V" means on R18 and R2.

The wire harness connects the opto couplers kathodes to the ground. The anodes are fixed to 5v on the pcb. Both should have been availableas connections to the outside world.  I gues i have drawn it logically what makes sense. My fault.

2. Are the "test"and "ON" switches the external switches on the control panel respectively, "Laser Test" and "Laser Switch" or are they switches on the power supply. Yes they are on the panel. The pxb has a miniature switch which duplicates the panel test switch.

3. There are 2x "LO" signals one connected to R2 and one connected to IC-1 pin 2. Since there is only one LO signal which is on the DC connector (in mine its called "L"). What is the other and which is which. Is the "LO" signal on your drawing really "IN"? Or do I have this all messed up? No the Lo signal from the opto coupler (kathode) is on the connector. the anode is on the pcb connected to 5v. They should have provided both as connector points to the outside world. 

4. My supply has 3 control connectors Laser Switch (-P, G); Test Switch (+K,-K); Current Regulation (G, IN, 5v) can you point me to them on this drawing? As per point 2 on the left side next to the opto coupler.  The potmeter next to the other opto coupler is the current regulation. I have a switch in the potmeter middle connection so i can feed pwm signals thru it and can switch between pwm and manual laser current settings. Hope this helps otherwise i will write something.  Do you have a page on a k40 wiki? Does a wiki exist?


---
**Don Kleinschnitz Jr.** *October 17, 2016 01:42*

**+Paul de Groot** thanks for the answers I see now what I am missing.

The DC connector which is X1 on your drawing has a signal "L" on pin 4. On your drawing it is empty. That is the control the nano uses to PWM and we are trying to figure out how it controls the supply. I would suspect it also goes to a optocoupler and acts like IC-1 but not sure how it is implemented. 

If you have a chance to chase that pin we will almost have the full story.


---
**Paul de Groot** *October 17, 2016 01:58*

**+Don Kleinschnitz** i see what you mean. I completely oversaw that yellow wire. I speculate that it gets AND gated via a diode straight to the optocoupler IC1 with the middle connection of the Potmeter (current setting)I will look it up this week. Did you know you can adj the current setting via the trimpot D behind R7? I had all my settings written on the panel and potmeter. After i blew it up i repaired and they were slightly shifted so retrimmed with that pcb trim pot.


---
**HalfNormal** *October 17, 2016 02:32*

**+Paul de Groot** A wiki is in the works. It needs work and volunteers. 


---
**Don Kleinschnitz Jr.** *October 17, 2016 02:56*

**+Paul de Groot** i checked and the IN and L signals are not connected even if you swap the meter probes to check for a diode in series. 

I bet there is a separate opto connected like IC-1 for L ...

It needs to be independent of the power adjustment and only fire if the ON sw is grnd.


---
**Don Kleinschnitz Jr.** *October 18, 2016 02:33*

 **+Paul de Groot**  I have been mesmerized by the LPS schematic and after more review I am puzzled by the "test" and "on" controls. 

These switches connect to D120 & D130's cathode. These diodes anodes in turn are connected together and to the cathode of IC-2's LED.

At first glance I assumed this was some form of diode AND gate to insure that both were true to allow the laser to fire.

However it seems that if EITGHER D120 or D130's cathode are grounded by these switches I would expect that current will flow in IC-1's LED and that would fire the laser??

What am I missing because I know both (AND) must be gnd to fire the laser.


---
**Paul de Groot** *October 18, 2016 02:54*

**+Don Kleinschnitz** i think the pcb mounted test button (which is tiny, next to the power led) and the test fire button are or'd via diodes 130 and 120. I speculate that the test fore panel switch is wired to the yellow x1-4 connection. We need to document the wire harness as well to get a definitive answer.  Unfortunately I am very time poor and my wife is not that happy with me spending too much time in the tree house full of 3d printers, robots and lasers. 😊 


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/3WHZLLLaqWp) &mdash; content and formatting may not be reliable*
