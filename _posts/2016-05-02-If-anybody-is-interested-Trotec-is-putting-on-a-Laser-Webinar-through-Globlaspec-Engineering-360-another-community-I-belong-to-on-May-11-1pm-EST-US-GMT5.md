---
layout: post
title: "If anybody is interested, Trotec is putting on a Laser Webinar through Globlaspec/Engineering 360 (another community I belong to) on May 11, 1pm EST US (GMT+5)"
date: May 02, 2016 18:19
category: "Discussion"
author: "Scott Marshall"
---
If anybody is interested, Trotec is putting on a Laser Webinar through Globlaspec/Engineering 360 (another community I belong to) on May 11, 1pm EST US (GMT+5).

The Subject is :

Turnkey Laser Systems for Part Marking and Batch Processing

Here's a link to their front page, and the details:

[http://www.globalspec.com/events/eventDetails?eventId=1002](http://www.globalspec.com/events/eventDetails?eventId=1002)



You may have to join the group to register, but that's not bad, they send out excellent whitepapers on industrial technology and trade specific (you choose the category) new OEM products, and don't ask for anything. (the OEM mfgrs are the advertisers)



Have fun, Scott





**"Scott Marshall"**

---


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/RtPtCrG9owh) &mdash; content and formatting may not be reliable*
