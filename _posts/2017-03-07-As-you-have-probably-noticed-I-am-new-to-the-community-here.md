---
layout: post
title: "As you have probably noticed I am new to the community here.."
date: March 07, 2017 23:07
category: "Original software and hardware issues"
author: "E Caswell"
---
As you have probably noticed I am new to the community here.. 

I have researched the laser for a couple of months with the likes of you guys on here and took the plunge in January this year. 

What I will say is thanks to everyone that has contributed especially the resident specialist that are always on hand to help.



Today after playing and looking further in to what I have traced back and found after Nathan Thomas had posted a thread on his layout I found a fault in the wiring that I didn't like so thought I should share it here. ( I am sure most won't be shocked as its common problem I believe)



When I tested the EM stop I found that the extractor had stopped, this had never happened before . after checking I realised I had plugged it in to the first socket and not the second one I had done from original install.

So after tracing back I found the first socket was wired to the EM circuit but was wired with different coloured wire. 



After tracing it back I found that there was what appeared to be a connection inside the main wiring loom that was protected with the coiled plastic stuff..



When I checked it out I found that the connection was just with a twisted wire and not very well twisted together at that.



The wire involved is the wire fro the EM stop button that goes to the PSU, both N & L wires were exactly the same.



Needles to say they have now been removed as I would not like to trust them and could cause a fire. I know they are not big loads but still wouldn't like it to cause a problem.



Worth looking at your wiring and changing it if its the same.







![images/e143811dcbc7ddf6a8bc87488d322aad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e143811dcbc7ddf6a8bc87488d322aad.jpeg)
![images/71383b1c94784da1d746fc670ea94d39.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/71383b1c94784da1d746fc670ea94d39.jpeg)

**"E Caswell"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 08, 2017 01:05*

Wow that's really shoddy. 


---
*Imported from [Google+](https://plus.google.com/106553391794996794059/posts/Xb7wca5pdnt) &mdash; content and formatting may not be reliable*
