---
layout: post
title: "WoooHoo! Just picked up a used Redsail Desktop 40W laser cutter/engraver for $240.00"
date: March 26, 2016 16:10
category: "Discussion"
author: "Anthony Bolgar"
---
WoooHoo! Just picked up a used Redsail Desktop 40W laser cutter/engraver for $240.00. And it even works!





**"Anthony Bolgar"**

---
---
**Scott Thorne** *March 26, 2016 17:13*

Great man...post some pics. 


---
**Anthony Bolgar** *March 26, 2016 17:23*

Going to pick it up from the seller in a couple of hours. Will post some pics once I have it in my hot little hands.


---
**Anthony Bolgar** *March 26, 2016 17:25*

I think I will upgrade this one with a smoothieboard.


---
**Anthony Bolgar** *March 26, 2016 23:31*

My luck continues....seller saw the advertising on my van (I own a clock repair/restoration business). Got a repair job from him that I will make $400 on. Also, gave him an R/C transmitter I paid $25 for, and he knocked $100 of in return. So I really only paid  $165. After doing the clock repair I will be $235 ahead of the game.


---
**Anthony Bolgar** *March 26, 2016 23:33*

After doing some quick measurements, the tube area will fit a 50W tube, with room to spare. It is a Redsail LE400, work area is 400mm X 280mm with an adjustable height bed (manual knob, but still nice to have). Rated as being a 40W tube right now. Just need to find the software it came with now, as the seller did not have it.


---
**I Laser** *March 28, 2016 02:26*

Nice, wish I had that sort of luck! :)


---
**Anthony Bolgar** *March 28, 2016 04:29*

Just won an ebay auction for 3 MO 20mm mirrors for the incredible price of 1.99 with free shipping. I had forgotten that I had even bid on these mirrors.


---
**I Laser** *March 28, 2016 04:40*

Buy a lotto ticket NOWWWWWW..... lol


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/aHXsye2X8Sp) &mdash; content and formatting may not be reliable*
