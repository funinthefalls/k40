---
layout: post
title: "Has anyone compiled a document with successful settings for cutting and engraving different materials and material thicknesses?"
date: May 20, 2016 20:57
category: "Materials and settings"
author: "Purple Orange"
---
Has anyone compiled a document with successful settings for cutting and engraving different materials and material thicknesses?





**"Purple Orange"**

---
---
**HP Persson** *May 20, 2016 23:45*

I have seen a document containing some data, can´t seem to find it right now in the group, but if you see it, take it as a idea of settings, not a fact of what your laser should be set to ;)

My suggestion is, try it out. Start low and slow and work your way up to catch a sweet spot for your laser.

Different mirrors/lenses, age & wear on the tube will demand different settings for lasers.



To get you started, what machine do you have, analog or digital?

Air assist?


---
**Greg Curtis (pSyONiDe)** *May 21, 2016 02:03*

Air assist helps tons, but I use 15mA 12mm/sec to cut acrylic, and 8-9mA 400mm/sec for raster engraves. Vector engraves get the same 8-9mA, but 35mm/ sec, ice had it skip belt teeth going over 40﻿



That's my most common material, I've done plywood (1/8th inch), but it's been a while and haven't reassessed powers since my tube upgrade.


---
**Stephane Buisson** *May 24, 2016 06:59*

difficult, because depend on so many parameters, lens, alignment, air assist, decrease in life tube, etc ...

ok to just give an idea, but at the end make your own.


---
**Purple Orange** *May 25, 2016 11:30*

Understood, however a tables with generic "known working values" would help you roughly tune a K40 setup for that material and thickness.  And then as you say, one must  adjust that table for your own specific setup.



Has none done this or is everyone afraid of sharing their experiences with the community?


---
**Greg Curtis (pSyONiDe)** *May 25, 2016 11:33*

There's also Google, that's where I found my base settings.



I've found charts that were close enough to start experimenting without many fires... 😉﻿



My rule of thumb, if you have a dial for power, never exceede 15mA.



Cuts on hard materials (wood, acrylic) full power, mid cutting speed (10-15mm/sec).



Cuts on softer materials (paper/poster board) 10mA, faster (15-25mm/sec)



Engraves are a tad trickier, depends on how deep you want to go, but pretty much anything would enjoy 8-10mA, 200-400mm/sec.



Slower speeds (as slow as you want) will create depth in acrylic, and darker markings in wood. I've even marked paper with engrave settings with pretty cool results.



Don't forget to play with the "steps" setting, as some materials like wood don't need every step fired (steps=1) you can skip a few (steps=3). This takes some experimenting.


---
**Purple Orange** *May 25, 2016 11:48*

**+HP Persson**

 I am currently looking in to buying me a (leaning towards the analogue) K40, and i am reading up on the stuff as much as I can. Looks like there are sort of 3 "must do´s" when i get one. Air-assist, red-dot-pointer and sort out the fumes evacuation....


---
**Purple Orange** *May 25, 2016 11:51*

**+Greg Curtis**

 Thanks for your standard values.


---
**Purple Orange** *May 25, 2016 11:56*

**+Greg Curtis**

 Thanks for your input, would you care sharing those "found charts" since i cant seem to find them on my own?



Question tough:

"if you have a dial for power, never exceede 15mA.

Cuts on hard materials (wood, acrylic) full power, mid cutting speed (10-15mm/sec)."



According to you reasoning;"full power" = 15mA even tough it might be possible to crank the power up?


---
**Stephane Buisson** *May 25, 2016 12:41*

**+Purple Orange** old post from this community

[https://plus.google.com/+JimCoogan_CoogansWorkshop/posts/4gfHdoABrEZ](https://plus.google.com/+JimCoogan_CoogansWorkshop/posts/4gfHdoABrEZ)


---
**Purple Orange** *May 25, 2016 13:06*

**+Stephane Buisson**

 Thanks for sharing, I have put together a "boundry scheme" based on the information that i have received in this thread. I will keep it updated as i get hold of more details of successfully cut and engraved materials with a K40. By collecting a lot of successful settings i would imagine that tuning in a particular K40 machine would be much quicker and easier and less stress inducing on the tube (less test fires). Please feel free to use it and give me a shout if I i shall update the boundary scheme with your "success details" for a specific material.



[https://www.dropbox.com/s/e87nti0u0szzbf9/K40_Boundry_scheme.htm?dl=0](https://www.dropbox.com/s/e87nti0u0szzbf9/K40_Boundry_scheme.htm?dl=0)


---
*Imported from [Google+](https://plus.google.com/112469922804049225807/posts/NLDcrQQAhfq) &mdash; content and formatting may not be reliable*
