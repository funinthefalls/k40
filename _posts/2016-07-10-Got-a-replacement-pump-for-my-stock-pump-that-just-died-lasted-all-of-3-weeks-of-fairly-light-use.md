---
layout: post
title: "Got a replacement pump for my stock pump that just died (lasted all of 3 weeks of fairly light use)"
date: July 10, 2016 18:04
category: "Modification"
author: "Tev Kaber"
---
Got a replacement pump for my stock pump that just died (lasted all of 3 weeks of fairly light use). 



Got a pond pump from Lowes, same approximate output as the stock one, hopefully more reliable. 



Had to get a couple fitting adapters (1/2" FIP x 3/8" FIP reducer coupling and 1/2" ID x 3/4" MIP hose barb) to size down to fit the laser inlet hose. 



It's loud compared to the stock pump, which was whisper-quiet, but if it proves reliable I can put up with a little noise. 



![images/432df54fc2f024a7e0fcc0ed16faaf87.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/432df54fc2f024a7e0fcc0ed16faaf87.jpeg)
![images/de78b52f2f9c3785bafb0b7c4624453e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/de78b52f2f9c3785bafb0b7c4624453e.jpeg)

**"Tev Kaber"**

---
---
**Scott Marshall** *July 10, 2016 20:28*

With a little noise at least you'll know it's running. Sounds like you dodged a bullet on that one, not very many people get away with loss of water flow, even for a few seconds. Maybe the pump at least held the tube full of water so that it didn't runaway thermally.



These laser tubes are about 9% efficient so about 420 watts of heat is produced in the tube.(at full power)



Knowing that you can imagine how quickly the metal electrodes heat up if you stop cooling them.



Glad your tube survived!



Scott


---
**Tev Kaber** *July 11, 2016 00:25*

Yeah, I lucked out! I was going to keep cutting that night, but my laptop battery was at 4% and it was late so I called it a night. The next morning, I noticed the pump didn't come on when I flipped the switch (first switch in my powering-on sequence).


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/LxgpUwpSCiE) &mdash; content and formatting may not be reliable*
