---
layout: post
title: "Does anyone have experience using the laser engraving sheet from ?"
date: March 28, 2017 08:44
category: "Materials and settings"
author: "John Sturgess"
---
Does anyone have experience using the laser engraving sheet from [www.gravierbedarf.de](http://www.gravierbedarf.de)? I'm trying to fine tune the settings but the engrave comes our with jagged edges. I'm using a 300dpi bitmap from illustrator into laserweb. This had 3 or 4 passes, not sure if it's better to try running high power and faster or low power and slower. IIRC this was about 6ma and 100mm/s 

![images/9cedaf5127f02bd75ac6299b47a8c692.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9cedaf5127f02bd75ac6299b47a8c692.jpeg)



**"John Sturgess"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 28, 2017 10:14*

Slower always tended to be much better clarity & less jaggedness, but can't say for this particular material.


---
**Claudio Prezzi** *March 28, 2017 19:21*

Why don't you use vector format (SVG or DXF)? This would make straight lines and is much quicker.


---
**Claudio Prezzi** *March 28, 2017 19:22*

I think quicker should be better with this material, as it should not melt during engraving.


---
**John Sturgess** *March 28, 2017 20:20*

**+Claudio Prezzi** all the cuts are generated from an svg, I'll try the whole thing as two svg files and see how I get on


---
**Pigeon FX** *May 08, 2017 08:45*

This looks a lot like the stuff I use from Trotec laser, I engrave at 200mm/s, with the power set to about 10 o'clock, and the cut with one pass at 5mm/s at about 11 o'clock....as others have said using Vectors is key, I use illustrator Ai files imported into Coral Laser. 

![images/865b50950130c2f08bb85c9bff97695b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/865b50950130c2f08bb85c9bff97695b.jpeg)


---
**John Sturgess** *March 27, 2018 05:47*

**+Pigeon FX** Sorry I must've missed this reply. Thanks, that looks realyy good, is that using the standard TroLase product?


---
*Imported from [Google+](https://plus.google.com/112856126515261932166/posts/XWLy5tiMnVo) &mdash; content and formatting may not be reliable*
