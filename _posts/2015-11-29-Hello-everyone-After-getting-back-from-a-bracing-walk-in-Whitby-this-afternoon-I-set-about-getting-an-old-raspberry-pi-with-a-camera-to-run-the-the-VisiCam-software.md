---
layout: post
title: "Hello everyone, After getting back from a bracing walk in Whitby this afternoon I set about getting an old raspberry-pi with a camera to run the the VisiCam software"
date: November 29, 2015 23:15
category: "Smoothieboard Modification"
author: "David Richards (djrm)"
---
Hello everyone,  After getting back from a bracing walk in Whitby this afternoon I set about getting an old raspberry-pi with a camera to run the the VisiCam software. I'm going to install it together with VisiCut and Smoothie-ware shortly. The targets are markers used to referance the position of the bed to the position of the object to cut, in this example you can see a little tin lid with a circle overlaid on it. Below is a photo of my new controller, almost configured ready to swap over for some testing. You can see a modification which is a level shifter for the pwm laser signal which are fed from some of the outputs of an unused stepper driver. David.﻿



![images/0a888f5ea1097d3984bdab2f6ad828d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0a888f5ea1097d3984bdab2f6ad828d3.jpeg)
![images/c53ece2c1459390970f43961ac9d1eb6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c53ece2c1459390970f43961ac9d1eb6.png)

**"David Richards (djrm)"**

---


---
*Imported from [Google+](https://plus.google.com/+davidrichards-djrm/posts/dt743fpto4H) &mdash; content and formatting may not be reliable*
