---
layout: post
title: "Abe Fouhy is considering converting his K40 to 50W in the future"
date: June 10, 2017 12:35
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+Abe Fouhy** is considering converting his K40 to 50W in the future. 



Questions for the forum:



1. Has anyone done this and what LPS did you use?

2. What tube are you using.



3. Has anyone run a 40W tube  with a 50W LPS?







**"Don Kleinschnitz Jr."**

---
---
**HP Persson** *June 11, 2017 09:19*

1: Original 40w, i just never went full power

2: Tong-Li, 50w - 800mm

Later changed to a 50w power supply after a month or two. (MYJG-50)

Not my regular K40, so never ran it much until i sold it.


---
**Abe Fouhy** *June 14, 2017 08:35*

Sounds good. I was just curious if the extra voltage kills the tube super quick. I think I'll give it a try


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/Nw5Mp4mqqFg) &mdash; content and formatting may not be reliable*
