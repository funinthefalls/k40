---
layout: post
title: "I have a bilge blower and can't seem to get it working properly as an exhaust fan"
date: July 10, 2016 20:53
category: "Modification"
author: "Zsolt Scheidler"
---
I have a bilge blower and can't seem to get it working properly as an exhaust fan. I tried a couple of different AC to DC wall adapters  however non of the 12V  adapter seem to fire it up. On some of them it will start  and then shuts off and start and shuts off .... and so on. 



Here is what I have:

fan

[https://www.amazon.com/gp/product/B003BMATFK](https://www.amazon.com/gp/product/B003BMATFK)

switching power supply

[https://www.amazon.com/gp/product/B00SRHHE02](https://www.amazon.com/gp/product/B00SRHHE02)



Any suggestions? Or do I have to bite the bullet and buy something like an Inline Fan? 

[https://www.amazon.com/gp/product/B00KWYJQYA](https://www.amazon.com/gp/product/B00KWYJQYA)



The other options I have seen floating around the web is using something like a 12v 30a Dc Universal Regulated Switching Power Supply ( [https://www.amazon.com/gp/product/B00D7CWSCG/](https://www.amazon.com/gp/product/B00D7CWSCG/) ) but I don't know much about electronics. Its on my list of things to learn about but I really want to get my K40 up and running with a working exhaust first.



Hoping someone can point me in the right direction. Thanks in advance.









**"Zsolt Scheidler"**

---
---
**Scott Marshall** *July 10, 2016 21:11*

You need a power supply that can provide 6 - 10 amps. Those blowers are power hungry beasts designed to be run 3 minutes at a time.

Wall-Wart type power supplies max out at about 2-3 amps. Totally inadequate for a bilge blower. (and will burn up if left running like that)



Those bilge blowers can work OK, but you need a much beefier power  supply.



You can get away with a laptop style supply producing 12v @ 6 amps or more, but you really should have a supply like this:

[http://www.ebay.com/itm/SUPERNIGHT-Regulated-Power-Switching-Supply-12V-DC-30A-360W-For-LED-Strip-Light-/262463174604?hash=item3d1c0637cc:g:u0cAAOSwepZXTkBA](http://www.ebay.com/itm/SUPERNIGHT-Regulated-Power-Switching-Supply-12V-DC-30A-360W-For-LED-Strip-Light-/262463174604?hash=item3d1c0637cc:g:u0cAAOSwepZXTkBA)



Laptop type that will work okhttp://[www.ebay.com/itm/Power-Supply-Adapter-AC-To-DC-12V-24V-2A-3A-5A-6A-10A-5050-3528-LED-Strip-Light-/262010038401?var=&hash=item3d0103e881:m:myz7cVlmLctVD835ROH0gyg](http://www.ebay.com/itm/Power-Supply-Adapter-AC-To-DC-12V-24V-2A-3A-5A-6A-10A-5050-3528-LED-Strip-Light-/262010038401?var=&hash=item3d0103e881:m:myz7cVlmLctVD835ROH0gyg)

(get the 10A model)



I'd go with the 30a model, then you have plenty of power for other accessories if you decide to go with a 12v water pump, lighting etc.



Scott


---
**Anthony Bolgar** *July 10, 2016 22:41*

I use a computer ATX power supply for all my 12V accessories. Works great.


---
**Greg Curtis (pSyONiDe)** *July 11, 2016 00:20*

I put together a slow start circuit because if I plugged it directly into the supply brick it would overload and shut off.﻿



Aside from that I run it off a 4a power supply.


---
**Pippins McGee** *July 11, 2016 02:24*

This is probably of no help to you, but I purchased a similar fan, and similar ac to dc power supply and mine works fine with ONE thing to note.

[http://www.ebay.com.au/itm/301913476327?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/301913476327?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



[http://www.ebay.com.au/itm/281043734565?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/281043734565?_trksid=p2060353.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



When switching on, the fan powers on for 1 second. then powers off, then 1 second wait, then it powers on and stays on.



Not sure why or if this is of any clue or help to working out what is your problem.

I am using 7amp transformer though compared to your 5amp.


---
**Greg Curtis (pSyONiDe)** *July 11, 2016 03:28*

That's what mine does, but won't start up on it's own. That's why I made the slow start. It's because if the initial draw that your supply kinda compensates for, but ours aren't quite powerful enough.


---
**Zsolt Scheidler** *July 12, 2016 03:39*

Thank you everyone for the input. I think I am going to go with a 12v regulated power supply as Scott suggested. I can see a little more tinkering in my future and this could be a good solution. 



I am curious about the slow start circuit, will look into that along with possibly getting a DC motor speed controller for the fan.


---
**Scott Marshall** *July 12, 2016 05:39*

**+Zsolt Scheidler** These  can be had quite cheap, but make sure you cut their claimed Amperage rating in half or so. Typical of Chinese equipment, if the rating is 15amps, it means it will handle 15A for 2 seconds while being sprayed with LN2.



[http://www.ebay.com/itm/6V-90V-15A-Pulse-Width-Modulator-PWM-DC-Motor-Speed-Control-Switch-Controller-VK-/252134658510?hash=item3ab46591ce:g:czoAAOSwo6lWJufG](http://www.ebay.com/itm/6V-90V-15A-Pulse-Width-Modulator-PWM-DC-Motor-Speed-Control-Switch-Controller-VK-/252134658510?hash=item3ab46591ce:g:czoAAOSwo6lWJufG)



You folks having problems with start up, it's quite common with Brushed DC motors on switching supplies, when the motor just starts turning it produces back emf that the regulator senses as over voltage. it can usually be eliminated by putting a small electrolytic capacitor across the motor leads (you'll see this cap on a lot of factory dc motors).

100uf @25V should do it, but anything from 10 uf to 10,000 ought to do the job. A reverse connected 1N4007 diode often does the trick as well. Both if you're the belt an suspenders type.



Scott


---
**Zsolt Scheidler** *July 12, 2016 07:24*

Thanks **+Scott Marshall**, as always your response is detailed and much appreciated. ( Even if I have to google much of it ). 



By the way looking forward to seeing some reviews from others on your ACR translator board since I think it's designed specifically for me. I was thinking of changing the M2Nano controller out with a Smothieboard but since I am not very electronics savvy  I figured I would hold out a bit to make sure this is the right machine for me. 



As far as the motor speed control I was looking at  a 7V-60V 10A 

[https://www.amazon.com/DROK-Controller-Regulator-Modulator-Indicator/dp/B00DVGGWC0](https://www.amazon.com/DROK-Controller-Regulator-Modulator-Indicator/dp/B00DVGGWC0)



I have to admit the K40 en devour has been interesting so far thanks to this helpful community and I haven't even etched or cut anything worth mensioning. Can't wait for the day that I can help other newbies with their K40 issues. 






---
**Scott Marshall** *July 12, 2016 08:01*

**+Zsolt Scheidler** That speed control would probably be OK, but may run a bit warm. It depends on the actual current your blower uses (that can change a lot depending on the air flow conditions) and how realistically rated that model control is rated. I've collected a few of them (I also use them in my K40 Light kit) and plan to publish testing results on them. (when, no IF I ever have some free time).



There's a lot of people like you, just because you're can't or just don't feel like doing a bunch of wiring and troubleshooting doesn't mean you shouldn't be able to do your upgrade. I heard so many people concerned they were going to toast a $150 controller, their laser, or both, I even had a few people who wanted to pay me to do the upgrade for them, so  I decided there was a niche that needed filling. 



Anyway if you'd like a catalog of the ACR kits (Switchable stuff coming very soon) just email me at ALLTEK594@aol.com.


---
*Imported from [Google+](https://plus.google.com/118045737098432264024/posts/4h42aXPyuP9) &mdash; content and formatting may not be reliable*
