---
layout: post
title: "Hey all my brand new k40 laser engraver arrived today and I could not wait to plug it all in"
date: October 01, 2016 10:48
category: "Original software and hardware issues"
author: "Patrick G"
---
Hey all my brand new k40 laser engraver arrived today and I could not wait to plug it all in. I have it all set up but now I cannot get it to power on except for a brief moment after checking all of the lines and then unplugging the switch connector and plugging it back in there was a whirl of the coolant fan and LEDS. Does anyone have any info they can  share or hot fixes that I am overlooking?





**"Patrick G"**

---
---
**Don Kleinschnitz Jr.** *October 01, 2016 12:22*

May need a little more information:

Have you proven the mains are live?


---
**HP Persson** *October 01, 2016 15:52*

Some has a fuse in the back where the connector is, check it if you have one, and if its burned or not.




---
**Patrick G** *October 02, 2016 12:31*

It's all sorted now, the emergency stop button was tripping even though it was no depressed. A quick spring replacement and rebuild and bam laser time. Thanks for the assistance


---
**Don Kleinschnitz Jr.** *October 02, 2016 14:01*

Hey that is the second time I have seen a problem with the ES button, should have asked if you had one :(.


---
*Imported from [Google+](https://plus.google.com/108727782554326629229/posts/2DqT6Yv4M7Z) &mdash; content and formatting may not be reliable*
