---
layout: post
title: "I just purchased my first K40 and never took it out of the box and returned it so I am a bit gun shy now"
date: November 16, 2017 09:45
category: "Discussion"
author: "The Best Stooge"
---
I just purchased my first K40 and never took it out of the box and returned it so I am a bit gun shy now.  The seller was horrible and the machine came to me scratched up, dented, dinged, missing parts and the final last straw was the X axis was bent by about 20-30 degrees and the seller sent me a video from their "tech" and it showed me how to grab it and bend it back.  That was the final insult and I started the refund process which just completed on Monday.



This machine was the upgraded, with wheels, version with the led light strip in the back so it looked better than the old potentiometer version but, as I said, I never took it out of the shipping box.



Is there any machines out there with a good quality seller that has not jacked up the price by 10-30% for Christmas?





**"The Best Stooge"**

---
---
**Paul de Groot** *November 16, 2017 20:38*

Yes I have read all the horror stories but the price is incredibly low. I am considering to bring out a machine on the market superior to the K40 without its drawbacks as mentioned above and fiber based instead of co2 tube. However to meet that price point i need to overcome the price of the fibre laser diodes. So will keep research the market to source these components. Others will keep telling you it's not possible to meet that price point. 


---
**The Best Stooge** *November 16, 2017 20:39*

**+Paul de Groot** I read that Fiber is not good at engraving and not all that great with wood but for cutting metal it is unsurpassed.


---
**Paul de Groot** *November 16, 2017 21:28*

**+The Best Stooge** it's probably a combination of various laser  diode types and PWM driving techniques to get the best engraving. The biggest issue that I see is that people are expecting high engraving speed while wood takes time to burn. Just physics process but hard to convince people to be patient with the engraving. I think it comes from all the promo videos on YouTube where people engrave at ridiculous speeds (double playback speed) which set unrealistic expectations with the public. I have done a kickstarter for a laser controller and the process of ordering and manufacturing those boards in China plus the fulfilment process is relatively cheap. Here you see the same phenomena: people expect to pay ridiculous low prices for the electronics. Just mailing the stuff cost me already $20.


---
**BEN 3D** *November 16, 2017 21:48*

If I could restart I would buy a kit to build my own device. The OnStock controler *1 is not nice. 



The on Stock Software is designed to work with Windows XP. 

I have not tried 40KWhisperer yet, but may is will be a better way to use the onstock controler.



I would buy a Board that is compatible with laserweb [https://github.com/LaserWeb](https://github.com/LaserWeb)



[plus.google.com - Do you have a DIY wish? https://es.aliexpress.com/item/1313-100w-DIY-assembl...](https://plus.google.com/113562432484049167641/posts/MFaWebumBA9)



Or Openbuilds



[http://openbuildspartstore.com/openbuilds-acro-system/](http://openbuildspartstore.com/openbuilds-acro-system/)



Or you search on the Webversion of this Google+ Community here are some Guy´s that want to sell there device to you.



*1 In my google Profile you could find a collection to my on stock controler M2Nano, to get some more details about.






---
**Paul de Groot** *November 16, 2017 22:14*

**+BEN 3D** My board is compatible with Laserweb. I will post the PCBs and schema on Github soon for people who want to build it themselves from scratch. Alternatively they can buy it assembled.


---
**The Best Stooge** *November 16, 2017 23:19*

**+Paul de Groot** I have Laserweb (though it never connects to the web so not sure why it is called that) and it sucks.  I am in the Lightburn beta and I got in because I hated LW.  On my box it ran like pure garbage because it is written in Java and is just far too resource hungry.  For me my first machine will be Ruida based because it is better than Grbl-lpc and Smoothie and runs beautifully well in Lightburn.  Fact is Lightburn is being created to be a drop in replacement for RDWorks #1 and many other controllers have been added (even Marlin for 3d printers he added per my request and help with).


---
**Paul de Groot** *November 17, 2017 00:25*

**+The Best Stooge** I got lightburn as well and testing it. Also I have created an easy laser toolchain in Inkscape with cutting and engraving plugins which works really well. Have a look at awesome.tech and maybe play a bit with the software. 


---
**The Best Stooge** *November 17, 2017 00:35*

**+Paul de Groot** I have never heard of awesome tech before.




---
**Paul de Groot** *November 17, 2017 00:40*

**+The Best Stooge** Dan and I set it up for our Kickstarters; the Gerbil laser controller and Dan's PPA (particle accelerator for STEM education). Have a look at the site :)




---
*Imported from [Google+](https://plus.google.com/111402888514079602827/posts/35K7aZr1a6W) &mdash; content and formatting may not be reliable*
