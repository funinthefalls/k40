---
layout: post
title: "Just thought I would show you some of the projects that I've been working over the last few weeks"
date: June 16, 2017 03:29
category: "Object produced with laser"
author: "Chris Hurley"
---
Just thought I would show you some of the projects that I've been working over the last few weeks. 



![images/2b4258b536343e4b735254a41677685b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2b4258b536343e4b735254a41677685b.jpeg)
![images/632738620376b4bb7ca211d60a8c8816.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/632738620376b4bb7ca211d60a8c8816.jpeg)
![images/cc868b0646b1ff6300e80c17bdc2ea18.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cc868b0646b1ff6300e80c17bdc2ea18.jpeg)
![images/f8fe73b09e336ee22528a44ddca6bc03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f8fe73b09e336ee22528a44ddca6bc03.jpeg)
![images/8179b53bceafb84c3de250699fa4c112.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8179b53bceafb84c3de250699fa4c112.jpeg)

**"Chris Hurley"**

---
---
**Maxime Favre** *June 16, 2017 09:48*

**+Numéro Treize**


---
**Stephen Sedgwick** *June 16, 2017 13:31*

These look awesome... 


---
**Don Kleinschnitz Jr.** *June 16, 2017 14:33*

A great example of what these machines can do. Nicely designed cut and engraved!


---
**Jerry Hartmann** *December 05, 2017 04:06*

Skoooooorne!




---
*Imported from [Google+](https://plus.google.com/103241400689819078337/posts/3ct9Eyakkbm) &mdash; content and formatting may not be reliable*
