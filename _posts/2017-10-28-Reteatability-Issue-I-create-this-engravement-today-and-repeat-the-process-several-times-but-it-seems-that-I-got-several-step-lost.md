---
layout: post
title: "Reteatability Issue I create this engravement today and repeat the process several times but it seems that I got several step lost"
date: October 28, 2017 12:49
category: "Original software and hardware issues"
author: "BEN 3D"
---
Reteatability Issue



I create this engravement today and repeat the process several times but it seems that I got several step lost. 



Wrong software setting? I used 60mm/s.



![images/b16df7d3f7f93f38a2642088fb4fc96e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b16df7d3f7f93f38a2642088fb4fc96e.jpeg)



**"BEN 3D"**

---
---
**Claudio Prezzi** *October 28, 2017 13:14*

60mm/s should not be a problem.

1. Check that the belt and pulleys are not loose. 

2. Check that the stepper driver current is ok. 

3. Check the accelleration settings. Too hight acceleration can cause lost steps on direction changes.

See some details here: [cncpro.yurl.ch - Initial Configuration - LaserWeb / CNCWeb](https://cncpro.yurl.ch/documentation/initial-configuration/31-grbl-1-1e) 


---
**BEN 3D** *October 28, 2017 14:22*

1. Good advice totally forgot to check this.

2. I have absolute no Idea how I can do that. I will google that.

3. Yes i was searching to something like this. But I use the onstock software Corel Draw 12 with LaserDWR for Windows XP in the moment. There is a value where you can set the stepp in mm, I thought it is just for manuell movement, but I am not shure. However I did not seen a Documentation for LaserDWR yet.


---
**Claudio Prezzi** *October 30, 2017 08:15*

Sorry, I don't know much about LaserDraw and the original Moshi board. I replaced that on the first day ;)


---
**Claudio Prezzi** *October 30, 2017 08:17*

In your video I saw that you run this engraving in two pathes and it looks like there was a shift in X direction between the two pathes.


---
*Imported from [Google+](https://plus.google.com/109140210290730241785/posts/1J18bRsAoiJ) &mdash; content and formatting may not be reliable*
