---
layout: post
title: "Really like these cutting boards :)"
date: June 09, 2016 21:24
category: "Object produced with laser"
author: "Alex Krause"
---
Really like these cutting boards :)

![images/9a26aaf1d401cb5d6eb8c611a427d8a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a26aaf1d401cb5d6eb8c611a427d8a9.jpeg)



**"Alex Krause"**

---
---
**Ariel Yahni (UniKpty)** *June 09, 2016 21:25*

Wowwwwwwww. Metallica rulzzzzzzzzz


---
**Alex Krause** *June 09, 2016 21:28*

It's one of the original American flags used durring the Revolutionary War called the Gadsden Flag


---
**Ariel Yahni (UniKpty)** *June 09, 2016 21:34*

Jajaja I just know it by the album cover


---
**Thom Williamson** *November 28, 2016 00:13*

I like that!  Good design, too.



Not to be picky, but there shouldn't be an apostrophe in "DONT" on the original flag.  Most places do the same thing with the First Navy Jack.  Sorry, I know it's technically correct the way you have it, but the historical flag didn't have that.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/97xTLSkYXXf) &mdash; content and formatting may not be reliable*
