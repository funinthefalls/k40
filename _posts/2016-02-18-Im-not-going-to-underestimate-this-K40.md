---
layout: post
title: "I'm not going to underestimate this K40"
date: February 18, 2016 12:48
category: "Object produced with laser"
author: "Ben Walker"
---
I'm not going to underestimate this K40.  I have been fielding orders for these little keychains.   So far I have sold enough of them to purchase several upgrades and I am just hitting the ground with it.   The quality of the cut,  along with a little tinkering with the software had learnt me many lessons.   Maybe I was lucky or something but I have few complaints from the cutting software.   I don't design in it.   I design on a more modern software and then export to wmf, do a few modifications and then it is off to engrave and cut.   These keychains take about 10 minutes to customize and convert.   Less than 10 minutes to engrave and cut.  1 minute to clean up and glue.   They are selling for $10 each.   So the K40 is the 10-10-10 robot in my book.   Much less frustrating than the 3d printer in the corner.   

![images/41c873399ca363965049ee59e3cbf2de.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/41c873399ca363965049ee59e3cbf2de.jpeg)



**"Ben Walker"**

---
---
**Jim Hatch** *February 18, 2016 13:11*

**+Ben Walker** +1 - You're spot on. We tend to see the negatives but the K40 is definitely a real machine and as long as you work within its limits (or are okay tinkering to extend those) it's great for an awful lot of stuff. I have access to a bigger $5K machine at the local Makerspace but it's a half hour from the house. Way easier sometimes to be able to fiddle in the garage with the K40 instead. This was definitely a good purchase from my point of view.


---
**Michael Schroeter (MiSc)** *February 18, 2016 13:54*

Wow, those keychains look awesome. You did the whole thing with your K40? The material is plywood I guess? ﻿


---
**Ben Walker** *February 18, 2016 14:03*

1/8 inch birch.  I cut both sides at once and glue them together using Gorilla wood glue.  Then I use a matte finish to keep that fresh wood look.   Of course this is a double engrave (different thickness) at 350mms and one cut at 10mms. 


---
**Michael Schroeter (MiSc)** *February 18, 2016 14:09*

Looks very nice, great idea. I need to try that as well. Which software do you use.


---
**Ben Walker** *February 18, 2016 14:14*

I am actually considering cancelling my Glowforge preorder.  I could have 10-11 of these k40's for what I spent on it.  I just can't imagine it being 10x better.  other than the drag and drop functioning.




---
**Michael Schroeter (MiSc)** *February 18, 2016 14:18*

Because of the glowforge I got the idea to buy a laser cutter. But after they told me that shipping to germany costs 1000 bucks, I decide to spend that big amount of money for a chinese laser cutter ;) I bought a G350 (500x300mm) from eBay and this one does a very nice job so far. Do you use RDWorks software?


---
**Ben Walker** *February 18, 2016 14:51*

**+Michael Schroeter** I design in illustrator (sometimes photoshop) but there is no hairline lines in illustrator so I have to convert all my cut lines in CorelLaser before doing the cut.  not a big problem but the software that ships with the k40 is so outdated I just can't wrap my head around.  maybe adobe has spoiled me.  And of course I got the two year warranty with the k40 - the glowforge is 6 months.  I hope it doesn't turn into a white elephant.  If it is 10x better than the k40 I will be on top of the world because I am highly impressed.  Did I mention that already?


---
**Michael Schroeter (MiSc)** *February 18, 2016 14:54*

I don't think that the glowforge is better. The big idea begin the glowforge is just the software and the camera, that's it. I don't think that it is worth so much money. BTW. I am pretty sure that the machine is made in China as well :)  Do you mind f I ask for the template, the file you use ? Thanks

Michael


---
**Ben Walker** *February 18, 2016 15:08*

**+Michael Schroeter** I will send the file when I break for lunch and can get to my studio.  I am actually at my daily grind today.  :-).  It's a really simple rectangle.  mirrored


---
**3D Laser** *February 18, 2016 15:17*

Hey Ben where are you selling your products if you don't mind me asking 


---
**3D Laser** *February 18, 2016 15:28*

Before I would by a glowforge I would get a red sail clone for less money bigger work area and nigger laser 80w vs 40w. Plus you get it now is teas of a year from now 


---
**Ben Walker** *February 18, 2016 15:40*

**+Corey Budwine** So far I am word of mouth only. I made a keychain for a friend who showed it to someone and they asked if I could design for a specific facebook group and well it was so popular I am getting orders through my personal account from all over the state.  


---
**Jim Hatch** *February 18, 2016 17:21*

**+Michael Schroeter** **+Corey Budwine** I backed the Glowforge (Pro version) and am not going to cancel the order. I think the K40 is a great machine for someone willing to tinker and at the price point is a no-brainer. But, I don't believe the GF and these types of Chinese lasers are in the same market space. The GF is being designed for out of the box use by a non-tinkerer - someone whose focus is on the output of the process and the design, not just someone who is comfortable with messing with the hardware.



The software is a huge piece of laser cutting/engraving and is a big weak spot with these Chinese lasers. Even the $5000 Chinese laser at our Makerspace (Legacy Laser 450 series, 60 watt engraver/cutter) uses a cheesey software (LaserCut 6.1) that isn't anything close to AI or Inkscape in usability. Documentation is sketchy and support isn't really easy to get to either. Although the distributor is in the US, they're made in China like the K40 and while it has a better fit/finish and it's more robust all around, it's still not something someone without a background in laser operations would be comfortable using (I help develop the Makerspace's training materials for it) and really requires multiple software packages to get anything done. 



The ebay sold Chinese lasers in the $2K range are larger versions of the K40 but more like the K40 than they are like the Legacy Laser or something like an Epilog, etc. Yeah, they'll all engrave/cut but how easy are they to use are worlds apart. 



Since I'm not a beta tester of the current GF I can't comment on how well it's hitting it's state goals but I have no expectation that I'm just getting a bigger K40 - I'm expecting an HP Laser Printer type of product, useful for non-techies. I do think if it's that kind of product it's worth 7X the K40.



But that doesn't make me less of a K40 fan. :-)


---
**Tony Sobczak** *February 18, 2016 17:51*

I'm a noobie and would really appreciate the template also. TIA **+Ben Walker**​. 


---
**Ben Walker** *February 19, 2016 01:57*

Hopefully this will work posting the file.  [https://drive.google.com/file/d/0B668t6QrK51rYlFna2RwSldDVGM/view?usp=sharing](https://drive.google.com/file/d/0B668t6QrK51rYlFna2RwSldDVGM/view?usp=sharing)


---
**Ben Walker** *February 19, 2016 02:04*

**+Jim Hatch** Hi Jim. It seems the last update on shipping indicated that there were no beta units out in the wild yet.  Which does concern me a great deal.  Beta was supposed to begin three months ago.  What really concerns me is first quarter shipments (My order sequence was within the for 1000 preorder).  Latest update has moved first quarter to the end of second quarter.  


---
**Jim Hatch** *February 19, 2016 02:23*

**+Ben Walker**​ Actually the update is that there are no "public" betas in the wild. It did not mention the status of the private beta units. It also committed to all pre-Oct 25th orders being delivered by the end of June which is the firmest date we've seen. I've backed a dozen or so Kickstarter & Indiegogos campaigns as well as a couple of private crowd funded ones like GF. I've only been stung once (in terms of never getting either product or refund). None has been on time. Based on the communication readiness GF is demonstrating I don't see it failing even if delayed.


---
**Michael Schroeter (MiSc)** *February 19, 2016 18:10*

**+Ben Walker** Thanks Ben, appreciate that.


---
**Stephane Buisson** *February 21, 2016 13:16*

**+Jim Hatch** **+Michael Schroeter** 

When I start this community it was in my mind to have a cheap funtional Lasercutter.  I quickly start to share my finding, the issue being the close HW &SW source, and the cost DSP upgrade board. I went for Smoothie board (great modern firmware) and Visicut (most advanced opensource software). the glowforge software is about the functions of visicut, from what is publicly available. But the glowforge is autofocus (less sensible to material thickness). I still think the GF is far overpriced for what you will got.

If I got half the GF budget I would go for a more powerful lasercutter with bigger table, and still change the board for a smoothie compatible (smoothiebraintz) and Visicut or Laserweb.


---
**Michael Schroeter (MiSc)** *February 21, 2016 15:26*

I am still thinking about an upgrade from the standard dsp chinese controller to a smoothieboard, but i don't know if this make sense, because I do not know whether the software works as expected. How about cutting, engraving and all that, does that work properly?


---
**Ben Walker** *February 22, 2016 14:53*

I am very interested in upgrading to the smoothieboard.  HOWEVER I tend to get myself in a heap of trouble with most electro mechanical devices.  Maybe I don't have the patience for it and don't want to learn more.  I looked over the documentation for the smoothieboard and I am reluctant to tackle it - especially since the stock k40 seems to be paying for itself right now.  Maybe I need to order another one and crash this one (which seems to fit my needs perfectly).  Yes a bigger build volume might be nice but I am stuck making keychains (not reluctantly) at the moment so the build volume is not necessarily a factor.  The Glowforge is larger but still a bit unhandy at 20".  I am weighing here the benefit of the ease of use.  

Just how difficult is it to swap out this board and allow for network control, etc?  I would much rather have parts on hand to get me on an upgrade path regardless but hate spending money for dust collectors (my makerspace is shrinking)


---
**Stephane Buisson** *February 22, 2016 17:23*

**+Ben Walker** Smoothiebrainz is not exactly ready just yet, a bit of patience ... real smoothie board 4XC will leave you with 2 unused driver, possible (I did it) but maybe better to wait a bit for peter.


---
*Imported from [Google+](https://plus.google.com/100981051586034517976/posts/978pbHvqQeF) &mdash; content and formatting may not be reliable*
