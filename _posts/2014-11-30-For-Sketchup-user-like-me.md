---
layout: post
title: "For Sketchup user (like me)"
date: November 30, 2014 15:44
category: "Software"
author: "Stephane Buisson"
---
For Sketchup user (like me)





**"Stephane Buisson"**

---
---
**Stephane Buisson** *January 06, 2016 08:40*

update :

Sketchup (2015&2016)

[https://github.com/JoakimSoderberg/sketchup-svg-outline-plugin](https://github.com/JoakimSoderberg/sketchup-svg-outline-plugin)﻿


---
**Don Kleinschnitz Jr.** *January 23, 2016 16:33*

I tried to use this, it installs the .rb in the plugin directory but does not show up anywhere in SU. It does not show up in the extensions directory inside SU either .... any ideas

 


---
**Stephane Buisson** *January 24, 2016 00:04*

take care to the path ! (not to embed into extra folder), I lose time with that, could I spare your time in search ...


---
**Don Kleinschnitz Jr.** *January 24, 2016 16:02*

Thank you, thank you. Moving the contents of  "sketchup-svg-outline-plugin-master up to the xx/SketchUp/Plugins folder worked.


---
**Don Kleinschnitz Jr.** *April 16, 2016 16:24*

I have started using this to convert SU files for laser cutting. It seems that the dimensions on the SVG file are off by a line weight. Seems that the outline is not outside the face area, more like 1/2 under the line or am I doing something wrong with setup etc. Any ideas


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/SYX3ghtvDW8) &mdash; content and formatting may not be reliable*
