---
layout: post
title: "Ok, maybe a weird one... anyone tried etching and cutting FR-4 PCB boards?"
date: March 01, 2018 10:52
category: "Discussion"
author: "Seon Rozenblum"
---
Ok, maybe a weird one... anyone tried etching and cutting FR-4 PCB boards? I am curious if a K40 can etch (eat) into the copper far enough to cut out traces, then obviously cut through it to cut out the PCB?



I've been wanting to buy a PCB mill, but they are super expensive and i have a laser cutter - surely I can cut my own PCBs?



I've seen videos on youtube of people spray painting their copper boards black and engraving the black off, so they can finish the board with Chemical Etching but that's not what I am wanting to do.



I want to actually cut far enough into the board to remove the copper and leave the fibreglass, kinda like what this video shows as an end product...


{% include youtubePlayer.html id="W-UZUwjg6y8" %}
[https://www.youtube.com/watch?v=W-UZUwjg6y8](https://www.youtube.com/watch?v=W-UZUwjg6y8)



I mean I'm happy to buy some board and try... but I am worried that hitting the copper with the laser might be dangerous :(





**"Seon Rozenblum"**

---
---
**Wolfmanjm** *March 01, 2018 11:29*

A K40 cannot burn through the copper. The only method that works is spraying with Matt black, burning that off as needed, then etching.


---
**Justin Mitchell** *March 01, 2018 12:41*

All metals reflect the infrared beam, and what little is not reflected gets quickly dissipated by the copper




---
**Don Kleinschnitz Jr.** *March 01, 2018 13:10*

Copper is pretty reflective at this wavelength. They make mirrors from copper for these wavelength lasers.



[http://www.2laser.com/yahoo_site_admin6/assets/images/figure1_laser_parameters.187194925_std.jpg](http://www.2laser.com/yahoo_site_admin6/assets/images/figure1_laser_parameters.187194925_std.jpg)



[https://fslaser.com/Applications/Metal](https://fslaser.com/Applications/Metal)








---
**Seon Rozenblum** *March 01, 2018 20:23*

Yeah, That's what I was thinking - bummer, was worth asking though. Thanks everyone for the replies!


---
**Timothy Rothman** *March 02, 2018 06:07*

I'm not an expert, but I know there is something you can place over the copper and when you raster it with the laser you create a super accurate mask and then can etch with some sort of sulfide acid or ferric chloride bath.  I used a sharpie with a Radio Shack kit 30 yrs ago.  If you're patient or a small enough board you can use a sharpie or a thin layer of paint, toner or contact paper also ablates under our 35W laser.  Your K40 can do a lot more than you think!


---
**Seon Rozenblum** *March 02, 2018 07:05*

**+Timothy Rothman** Yeah, I know that's an option, but in my original post I mentioned it wasn't something I was wanting to do. If I can't actually cut/etch FR-4 with my laser, I'll just continue to proto on perf board and send my files to China for PCB manufacture.



or invest in a PCB mill (eventually).



I was just really hoping (against hope) that someone had found a way to actually cut copper with a K40 ;)



Cheers!


---
**อมรชัย ธนธันยบูรณ์** *March 03, 2018 20:06*

My method for pcb.

[m.facebook.com - อมรชัย ธนธันยบูรณ์](https://m.facebook.com/story.php?story_fbid=1556023697799272&id=100001749698738)


---
**Abe Fouhy** *March 09, 2018 08:49*

**+Timothy Rothman** Yea used the overhead projector sheet in the laser printer trick to make pcbs. Did a pretty good job.


---
**Rene Munsch** *September 02, 2018 20:57*

**+อมรชัย ธนธันยบูรณ์** Is this a foiled copper PCB?


---
**อมรชัย ธนธันยบูรณ์** *September 03, 2018 01:44*

**+Rene Munsch** standard fr4 pcb.


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/DkdLgcgeNuQ) &mdash; content and formatting may not be reliable*
