---
layout: post
title: "I have downloaded CorelDraw X8 and this seems to be much more user friendly than v12- is there a way to get the CorelLaser plugin to work with X8?"
date: August 26, 2016 13:17
category: "Software"
author: "Afiqul Chowdhury"
---
I have downloaded CorelDraw X8 and this seems to be much more user friendly than v12- is there a way to get the CorelLaser plugin to work with X8?





**"Afiqul Chowdhury"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 14:00*

I haven't worked with x8 before, but others have reported it as working correctly with CorelLaser. Provided it is not a Home & Student version of x8, just make sure you uninstall other versions of Corel Draw.


---
**Afiqul Chowdhury** *August 26, 2016 14:34*

Ended up fixing it - had to make sure CorelLaser installed in the 64bit programme folder in Windows to make it work. Thanks!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 14:50*

**+Afiqul Chowdhury** That's a handy tip to know. Never registered that it would make an issue.


---
**Afiqul Chowdhury** *August 26, 2016 15:56*

Do you know if there is a way to make corellaser print the whole page that I have designed in CorelDraw?- at the moment, when you click engrave, it'll make a box only around the area that you have designed and then its up to you to drag the pointer around to position everything correctly- which is fairly difficult.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 16:06*

**+Afiqul Chowdhury** I think I understand what you're wanting. However correct me if I'm wrong.



What I tend to do is create a rectangle that is 300mm x 200mm (the area a stock K40 can do) & position all the elements I want to engrave or cut inside that. I make that rectangle NO FILL & NO BORDER. So the laser won't laser it, but CorelLaser recognises that it exists for alignment purposes.



So, if I want to do an engrave & later do a cut, I can just firstly select the rectangle & all the elements to engrave, then choose Engrave.



Then, to do the cut I again select the rectangle & all the elements intended to be cut, then choose Cut.



If this isn't what you're wanting, let me know.


---
**Afiqul Chowdhury** *August 26, 2016 16:10*

That works perfectly, thanks!


---
*Imported from [Google+](https://plus.google.com/115107558259884759407/posts/grvvY3FkeZH) &mdash; content and formatting may not be reliable*
