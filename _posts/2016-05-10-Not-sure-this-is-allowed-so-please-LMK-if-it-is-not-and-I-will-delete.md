---
layout: post
title: "Not sure this is allowed so please LMK if it is not and I will delete.."
date: May 10, 2016 14:01
category: "Discussion"
author: "Custom Creations"
---
Not sure this is allowed so please LMK if it is not and I will delete.. I have been using an old ramps 1.4 board that I used on my Delta printer a LONG time ago and there were some issues with the board that needed fixed. The issues were fixed to make it work again with a printer so I figured it would be good to use for the laser since it uses less of the board. Well it has been giving me little gremlins here and there and I am just getting tired of it so here is what I am trying to do.



I have a NEW (never used, just setup) 7" 1024*600 Capacitive Touch Screen w/HDMI, Mount for the screen and a Raspberry Pi attached to the back of it. All NEW, still have the boxes for it! I am looking to trade this setup for a Smoothie board so I can utilize the most of my K40. The Rasp Pi has been setup with OctoPrint and the SD card is included. 



Please LMK if you are interested. This package total without shipping is worth over $100..



![images/f856e7e02292d19cb034e3ffde2e4ec2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f856e7e02292d19cb034e3ffde2e4ec2.jpeg)
![images/1412923a05db1b2b8020e99c5af24cc2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1412923a05db1b2b8020e99c5af24cc2.jpeg)

**"Custom Creations"**

---
---
**John-Paul Hopman** *May 10, 2016 19:15*

So you can replace the default laser engraving board with a RAMPS board? Does it require special software? Are you still stuck with MoshiDraw / CoreLaser?



Can't help with the Smoothieboard but would love to learn more about re-using an old RAMPS board as an upgrade if you have any reference materials you could point me to.


---
**Custom Creations** *May 10, 2016 19:18*

**+John-Paul Hopman** [https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion](https://github.com/ExplodingLemur/presentations/wiki/K40-RAMPS-Conversion)


---
*Imported from [Google+](https://plus.google.com/+AaronWidmerPrometheus3D/posts/gNhQNj2EjdJ) &mdash; content and formatting may not be reliable*
