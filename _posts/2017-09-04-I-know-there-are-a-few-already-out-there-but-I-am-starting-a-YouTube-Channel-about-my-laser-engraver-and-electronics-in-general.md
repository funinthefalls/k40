---
layout: post
title: "I know there are a few already out there, but I am starting a YouTube Channel about my laser engraver, and electronics in general"
date: September 04, 2017 01:11
category: "Modification"
author: "Robert Buchholz"
---
I know there are a few already out there, but I am starting a YouTube Channel about my laser engraver, and electronics in general. I will be doing some Arduino projects and general electronics. I am looking for some feedback, ideas, and general constructive criticism.









**"Robert Buchholz"**

---
---
**Printin Addiction** *September 04, 2017 02:03*

Nice video for beginners, a lot of information is covered.


---
**BEN 3D** *September 04, 2017 10:14*

Hi Robert, 

that is a nice Video. I still in Progress on my unboxing Video of my k40. Is it ok for you if I share a link of your video in my Youtube Channel?



Cheers Ben from Germany


---
**Robert Buchholz** *September 05, 2017 11:35*

**+BEN 3D** Feel free to share a link.  Do you have a link for your channel?  I would be interested in seeing what you are doing as well.


---
**Robert Buchholz** *September 05, 2017 11:36*

**+Printin Addiction** Thanks for the feedback.  I tried to keep it short, but there is a lot more to these machines than it appears at first.


---
**BEN 3D** *September 05, 2017 15:59*

**+Robert Buchholz** I have not posted it in youtube yet because it is still in progress, but I will post it in google+ public if it is finished. So just follow me if you want.


---
**BEN 3D** *September 07, 2017 18:19*

hi **+Robert Buchholz** I watched youre complete Video right now. And it was very helpful to me. I have be exactly the same device, but my stepper Motors are attached with other cable to the m2nano board. I think it would be good if you attach informations to the ground Kabel and to first start and use of the device. You also should tell that the delivered corel software just work for xp and how you transfear the usb dongle key to your vm, and wich kind of vm hoster you used, because I mean that hyper v does not support usb dongles, so you use virtual pc or vmware player. I added your channel to my watchlist and will be happy to see more next time.


---
**BEN 3D** *September 14, 2017 23:37*

Hi **+Robert Buchholz**, I start to share my unboxing Video. Unfortently it is just in German at the moment and some Parts left becouse i need to learn blender to cut them usefule together. 


{% include youtubePlayer.html id="playlist" %}
[youtube.com - BEN_3D China Laser - YouTube](http://www.youtube.com/playlist?list=PLR5pZNTBz7AEuAXZNm5rOMWKBMYnCqVT0)

Cheers Ben


---
*Imported from [Google+](https://plus.google.com/117736558877962713281/posts/BT7Lr5o5oSV) &mdash; content and formatting may not be reliable*
