---
layout: post
title: "Anyone know what these are for?"
date: March 09, 2018 09:57
category: "Hardware and Laser settings"
author: "Andrew Sanders (pinsetter1991)"
---
Anyone know what these are for? 



![images/28b4e5842be7eac98a5c4726d5c323a9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/28b4e5842be7eac98a5c4726d5c323a9.jpeg)
![images/a9317c1cfb82542ce59fdc65608ba08c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9317c1cfb82542ce59fdc65608ba08c.jpeg)

**"Andrew Sanders (pinsetter1991)"**

---
---
**Gary Hangsleben** *March 09, 2018 10:34*

Crimp wire connector for small gauge wires.  Used to connect wires into screw clamp terminals.  Insert wire all.the way through and flatten aluminum around wire.


---
**Adrian Godwin** *March 09, 2018 10:37*

The red thing is a 'bootlace ferrule'. You put it over the end of a wire and crush the metal part, making a neat end that doesn't fray when it's in a clamp. You might find the wires on the power supply have them fitted - if you've got an odd one it was probably just dropped in the cutter.



The tube is silicon sealant. It can be used to improve the high voltage insulation around the tube connections. Shouldn't be needed unless you change the tube, but I have no idea why a pack is often put in the cutter - it doesn't seem consistent with cutting every penny out of the manufacturing cost.




---
**Don Kleinschnitz Jr.** *March 09, 2018 13:42*

**+Adrian Godwin** didn't know those pins are called <b>bootlace ferrule</b>. I always thought they were called PIN terminals. Bootlace searches much better on amazon :).  


---
**Andrew Sanders (pinsetter1991)** *March 09, 2018 15:48*

Thanks for the info guys! That bootlace ferrule was taped to the bed making it look like an important piece haha. 


---
**Stephane Buisson** *March 09, 2018 15:52*

**+Andrew Sanders**, In our case, those parts serve to connect the tube and isolate the hight voltage.

the tube could be delivred in separated box sometime.


---
**Laurent Peyremorte** *March 10, 2018 10:00*

Cable shoe. This is for multi-wire cable.


---
**Ned Hill** *March 11, 2018 17:14*

I guess they don't call them "aglet terminals" because people have such a hard time remembering what the plastic ends of shoelaces are called :P


---
*Imported from [Google+](https://plus.google.com/101788445554224184336/posts/PPT64HL66Cs) &mdash; content and formatting may not be reliable*
