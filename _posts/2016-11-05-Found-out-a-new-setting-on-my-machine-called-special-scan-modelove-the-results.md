---
layout: post
title: "Found out a new setting on my machine called special scan mode....love the results!"
date: November 05, 2016 23:46
category: "Object produced with laser"
author: "Scott Thorne"
---
Found out a new setting on my machine called special scan mode....love the results!

![images/fb66de2543495f0ecf4cd039a4ae7c60.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb66de2543495f0ecf4cd039a4ae7c60.jpeg)



**"Scott Thorne"**

---
---
**Ariel Yahni (UniKpty)** *November 05, 2016 23:52*

OMFG


---
**Cesar Tolentino** *November 05, 2016 23:56*

what is special scan mode? are you using stock box? or mod?


---
**Todd Miller** *November 06, 2016 00:04*

I'm Jealous ! ! !


---
**Scott Thorne** *November 06, 2016 00:07*

**+Cesar Tolentino**...i have a dsp 50 watt machine....but to answer your question special scan mode fires the laser for lighter areas at a different frequency...new to me bro...lol


---
**Scott Thorne** *November 06, 2016 00:07*

**+Ariel Yahni**...that's the same thing i said....lol


---
**Ariel Yahni (UniKpty)** *November 06, 2016 00:13*

Where did you buy that special scan mode lol


---
**Scott Thorne** *November 06, 2016 00:15*

**+Ariel Yahni**....I've had the machine for a year now and never noticed that setting..I'm an idiot. 


---
**Ariel Yahni (UniKpty)** *November 06, 2016 00:19*

They should put a large warning label outside the box " don't forget about the awesome mode inside" 


---
**Scott Thorne** *November 06, 2016 00:23*

**+Ariel Yahni**...this is true...or idiots please read owners manual...lol


---
**Scott Marshall** *November 06, 2016 02:02*

As well as it works, I'm surprised it's not plastered all over the box...



They need a new marketing guy. Like you.


---
**Scott Thorne** *November 06, 2016 02:10*

**+Scott Marshall**...lol


---
**ED Carty** *November 06, 2016 06:01*

Damn that looks nice




---
**Nick Williams** *November 06, 2016 06:35*

So how is "special scan mode" different from normal operations??

Just curious? I'll say this "looking good "


---
**Scott Thorne** *November 06, 2016 11:49*

**+ED Carty**..thank man. 


---
**Scott Thorne** *November 06, 2016 11:50*

**+Nick Williams**...if I'm reading it right it allows the tube to fire at a lower frequency...that what it says anyway....lol


---
**ED Carty** *November 06, 2016 15:02*

**+Scott Thorne** interesting. probably doesnt cut in as deep, but gives you better contrast. Nice


---
**Przemysław Wilkutowski** *November 07, 2016 10:32*

Where is this option ? Can you post a pic with big red this is the fwaking opton sign?



my gosh looks cool


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/9Fpbt5tz18g) &mdash; content and formatting may not be reliable*
