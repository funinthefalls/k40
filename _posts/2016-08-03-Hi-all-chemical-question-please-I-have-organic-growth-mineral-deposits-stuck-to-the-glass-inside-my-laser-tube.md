---
layout: post
title: "Hi all, chemical question please. I have organic growth/mineral deposits stuck to the glass inside my laser tube"
date: August 03, 2016 02:35
category: "Original software and hardware issues"
author: "Pippins McGee"
---
Hi all, chemical question please.



I have organic growth/mineral deposits stuck to the glass inside my laser tube.

it allows air bubbles to have something to attach to and so my tube is always full of miniature air bubbles.



Most likely caused by a) me not having changed the water for several weeks (not that long I wouldntve thought) after installing machine, or b) using deionised water instead of distilled water.



I've since replaced 10% of my 15 litre water tub volume, with Methylated Spirits (ie. ethanol with a little methanol) and this hasn't dissolved it.



Should I increase the methylated spirits volume to something more potent, say 30, 50%? 100%?

At what point is too strong that the rubber?/silicon water tubing / glue / connections of water tubing to the laser will dissolve away and spill water into my machine killing it and me.?



or is there a safer chemical to flush through the tube that will dissolve these deposits/growths that are seemingly stuck..



is it maybe a better idea to drain the system and let the tube dry out and then maybe use pipe-cleaners to scrub the inside of the water jacket inside the tube?

god I just don't know haha. alls I know is I want to clean the inside of my tube.





**"Pippins McGee"**

---
---
**festerND NaN** *August 03, 2016 03:11*

i would probably use the stuff for cleaning coffee pots/espresso machines, it's food and silicon safe and descales nicely.

just flush thoroughly, and obviously don't run the laser during cleaning.


---
**greg greene** *August 03, 2016 03:37*

clr


---
**Pippins McGee** *August 03, 2016 04:32*

**+greg greene** 

[https://www.bunnings.com.au/clr-1l-calcium-lime-rust-remover_p4460670](https://www.bunnings.com.au/clr-1l-calcium-lime-rust-remover_p4460670)

is that what you refer to Greg?



**+festerND NaN** Thanks, any idea what the stuff is called?

I've never cleaned a coffee pot before - wouldn't have a clue what it's called haha!


---
**Alex Krause** *August 03, 2016 06:09*

Replace all the water in reservior with a couple of litres of vinegar and tap water... let it run for 12-24 hours replace all water with tap water only let it run for a bit... place outlet tube into a waste pan and flush adding a bit more tap water as your reservoir drains... purge all water and replace with distilled water and some algaecide drops for aquariums


---
**Alex Krause** *August 03, 2016 06:11*

The vinegar will clean the bio deposits and alot safer than CLR which is muriatic ACID (diluted hydrochloric acid)


---
**greg greene** *August 03, 2016 13:40*

Vinegar works too and is cheaper than CLR


---
**Pippins McGee** *August 03, 2016 23:26*

thanks guys, Vinegar it is. time to kill those germs dead.


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/UZA4yt6mKPK) &mdash; content and formatting may not be reliable*
