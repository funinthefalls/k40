---
layout: post
title: "Cats Love lasers! ;)"
date: May 04, 2016 21:38
category: "Discussion"
author: "Alex Krause"
---
Cats Love lasers! ;)

![images/862fa9be880c643a7a0a6c19494d044e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/862fa9be880c643a7a0a6c19494d044e.jpeg)



**"Alex Krause"**

---
---
**Joe Keneally** *May 04, 2016 21:52*

I got mine last Christmas.  It was AWESOME!!!




---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 04, 2016 22:55*

**+Joe Keneally** That would have been a great Xmas present. It is indeed awesome. I got mine a few months before you & probably one of the most useful/fun gadgets I have ever purchased.


---
**Ray Kholodovsky (Cohesion3D)** *May 04, 2016 23:04*

A dog would guard the box. 


---
**Jim Hatch** *May 04, 2016 23:23*

**+Yuusuf Sallahuddin**​ I agree. I use it more than my 3D printer.


---
**Joe Keneally** *May 04, 2016 23:37*

Best gift EVER!!


---
**Jean-Baptiste Passant** *May 05, 2016 18:49*

**+Jim Hatch**  I use it more than my 3D Printer too, too bad it costed twice the price of the K40...



**+Alex Krause** Welcome to the family !


---
**I Laser** *May 07, 2016 03:23*

They certainly do................... :D


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/bHmPUFZkAmB) &mdash; content and formatting may not be reliable*
