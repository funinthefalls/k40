---
layout: post
title: "I have not had my laser working for a while"
date: December 17, 2015 17:38
category: "Discussion"
author: "george ellison"
---
I have not had my laser working for a while. The right end of the tube sparks to the casing when the test fire button is pressed and tube is not firing. Does this mean i definitely need a anew tube or is it the electronics. There is no water in the tube but i do remember lifting the water pump out of the water when it was running :( :( which may have got air into the tube?? Thanks for any advice !!





**"george ellison"**

---
---
**ChiRag Chaudhari** *December 17, 2015 18:03*

I have no idea what is the issue with your laser tube, but I just saw the video you posted earlier, and I have to say, it is TOO DAMN SCARY ! I jumped out of my chair when it made that zzzzzz sound. Stay away from it, thats for sure. Make sure you ground your machine properly.


---
**george ellison** *December 17, 2015 18:44*

Yes, it was scary, hopefully changing the tube should sort it do you think


---
**ChiRag Chaudhari** *December 17, 2015 19:01*

New tube should fix the problem, but check the connections, and post some high-res pictures of that end. Somebody here may have more insight on what may be the actual problem is. 


---
**Todd Miller** *December 18, 2015 00:47*

High Voltage takes the shortest path to ground.



It's possible that the insulation or wire at the

end of the tube is exposed or too close to the

chassis. 



Also, if the other end of the tube isn't grounded (via the power supply or ma meter) then the tube is unloaded due to the incomplete circuit.



It's gotta go somewhere. 



Don't fire it anymore, the arching will kill your PSU.


---
**ChiRag Chaudhari** *December 18, 2015 01:21*

Yeah **+Todd Miller** 's absolutely right. You really need to make sure all your connections are done right. Like I said post some high-res pics so that the problem can be diagnostic better.


---
**Jim Bilodeau** *December 20, 2015 00:50*

I have this same exact problem (ran it for the first time today); arcing from the laser tube to the chassis. I do not have the external ground terminal wired to a ground stake in the ground - is that needed? Or do I need to add insulation around that red wire?


---
**george ellison** *December 20, 2015 09:18*

I have never used the ground (in the uk) and it worked fine. I have tried adding insulation but no luck


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/Le6HUxVjUkk) &mdash; content and formatting may not be reliable*
