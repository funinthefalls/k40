---
layout: post
title: "Hi, I have recieved one of those 2.5W laser diode+ XY chassis to do some engraving"
date: April 30, 2016 15:25
category: "Discussion"
author: "Maxime Favre"
---
Hi, I have recieved one of those 2.5W laser diode+ XY chassis to do some engraving.

Everything is fine except the diode is not firing.

When I plug it directly on the 12V power supply, it fire.

When I run the machine I got 12V on the output of the board but the diode don't fire... 

The board is a laseraxe-thing with an arduino nano.

I tried with various firmwares, various controller like benbox, laserweb 1&2. Same result.



On the other hand I have a spare azteeg x5 mini lying around. Can I control this kind of diode with it ?





**"Maxime Favre"**

---
---
**Ariel Yahni (UniKpty)** *April 30, 2016 16:28*

Firing the laser will happen on a PWM output, that way you can control when on or off. If the machine came with the diode driver it must be connected to PWM. If you have a smoothie then go connect it to the Fan controlled or heater but mind that you need to configure LW2 with the correct parameter either M106/107 for fan or G0/G1 heater


---
**Ariel Yahni (UniKpty)** *April 30, 2016 16:31*

[http://imgur.com/8bIoVV0](http://imgur.com/8bIoVV0)


---
**Vince Lee** *April 30, 2016 18:02*

I suggest checking the outputs with a voltmeter.  I think its common for boards not to switch 12v but to switch the ground line instead, so you would tie 12v to the diode + and connect the diode - to the switched output of the mosfet.


---
**Maxime Favre** *April 30, 2016 18:06*

Thanks guys. I forget to change the Gcode commands to G0 G1 -_- It works now with the azteeg. Just some estep config to do and it should be fine. Thanks again !


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/US616xfpBuG) &mdash; content and formatting may not be reliable*
