---
layout: post
title: "For people with ramps in there k40 what are your sped setting so the machine doesn't sound like death while moving I have it homing nice and smooth but once it's cutting it's not good"
date: May 25, 2016 02:23
category: "Discussion"
author: "Derek Schuetz"
---
For people with ramps in there k40 what are your sped setting so the machine doesn't sound like death while moving I have it homing nice and smooth but once it's cutting it's not good. Also my raster is not going back and forth quickly it just goes at a snail's pace





**"Derek Schuetz"**

---
---
**Jon Bruno** *May 25, 2016 02:46*

What are you using for software?


---
**Derek Schuetz** *May 25, 2016 02:47*

Marlin with inkscape plugin 


---
**Jon Bruno** *May 25, 2016 03:13*

Give LaswerWeb 2 a try [https://github.com/openhardwarecoza/LaserWeb2](https://github.com/openhardwarecoza/LaserWeb2)


---
**Derek Schuetz** *May 25, 2016 05:16*

**+Peter van der Walt** I have a smoothie on the way but China is far


---
**Jon Bruno** *May 25, 2016 12:33*

Is Franco working with LW2?

It would be easier for me to diagnose my issues if the dev was onboard.


---
**Jon Bruno** *May 25, 2016 19:19*

Lol.. I can respect that, But, I had posted a link to pastebin with the beginning of the log in the chat of LW2. the paste is of a simple manual homing of X and then Y and then clicking the "Home" button. the log of the sample gcode file I generated fly's off the screen before I can capture the beginning of it but the flickering reveals that the board is hanging out. I'd love to be more proactive but since I'm learning as I'm fighting, I may just wait a few days and see if anyone else kicks the same rocks. I figure **+Anthony Bolgar** should have some experience with this soon enough.


---
**Derek Schuetz** *May 26, 2016 03:07*

Turns out my driver's were at half steps somehow now I'm at 32 and I can run low speed to high speed no vibrationissuee


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/XUDDx1qY2us) &mdash; content and formatting may not be reliable*
