---
layout: post
title: "Hello, I am having issues with a K40 I bought earlier this year"
date: June 10, 2018 00:26
category: "Discussion"
author: "Micah Riley"
---
Hello,

I am having issues with a K40 I bought earlier this year. It worked GREAT out of the box!



About 3-4 months go by and I start noticing it's getting weaker. Within a week I had to progressively increase cutting/engraving power (40-50%) until 90-100% started providing weak power and deemed unusable for any cutting/engraving. 



I figured it was the power supply and I swapped a new one in. It allowed me to cut as I did before at 40-50%, but at 80% now. I went with that (since it worked) until a week later... Over time, it ended up being so weak and unusable like before.



I went ahead and returned that power supply and bought the same type power supply from a different vendor. The power ended up being exactly how I left off with the last one... unusable and weak. 

After discovering this, I thought it was the tube. I swapped my stock tube out and discovered no cracks or anything to cause alarm. 

I soldered the wires as originally connected before: wrapped in silicone tubing. 

The same result from prior to switching the tube and last power supply!



I've tried reconnecting, resoldering, rewiring to no avail!



Does anyone have any clue on where I should start on this expensive 2+ month troubleshooting journey?



This link shows the state of machine operating now. Notice how weak it becomes (the tube's light become fainter) as I increase the power.




{% include youtubePlayer.html id="szNhrTQrDWA" %}
[https://www.youtube.com/watch?v=szNhrTQrDWA](https://www.youtube.com/watch?v=szNhrTQrDWA)





**"Micah Riley"**

---
---
**Joe Alexander** *June 10, 2018 00:39*

i would try changing your coolant water out. It can have weird effects by drawing the power to it, and only use distilled water. if your lines look like they are scummy rinse it with a little white vinegar then flush thoroughly and replace with distilled water, not tap water.


---
**Micah Riley** *June 10, 2018 00:47*

**+Joe Alexander** Thanks for offering some advice. 

I initially read this could be an issue, so I swapped out the original distilled water with brand new distilled water. Unfortunately, this seemingly had no effect. =(


---
**HalfNormal** *June 10, 2018 03:21*

Make sure you're getting your 0- 5 volts for control to the power supply from your control panel. I actually had a bad M2 Nano board that caused a similar problem.


---
**Jeff Bovee** *June 11, 2018 16:23*

This seems overly obvious but did you check and clean all your optics?  I found when i first started out that when things started getting dirty power dropped off dramatically and I've heard people have issues with the stock lens going bad as well.


---
**James Rivera** *June 11, 2018 17:33*

I'm going to ask a really dumb question: do you have water cooling on? I don't hear it in the video (I think that's your air pump I hear).




---
**leize yan** *June 12, 2018 06:44*

I think maybe is the laser tuber, the power is Weaken

Perhaps you need to inflate or change it to a new one.

Of course, this is only my guess. if necessary, you can send more detailed information to my mailbox, leizekeji @ [gmail.com](http://gmail.com), and I will ask our technical engineers to help you look at it.




---
**Don Kleinschnitz Jr.** *June 12, 2018 12:43*

If I had to guess I would suggest the HV is breaking down. HV can break down at larger power settings and not ionize the laser. 



Causes of HV breakdown:

1. Bad HVT, but this is a new supply so unlikely?

2. Arching somewhere in the new wiring.



Wacky control signals:

1. The digital panel is not controlling <i>IN</i> on the LPS correctly



<b>Check the </b><i>IN</i><b> pin</b>: to see if the voltage on that pin goes from 0-5v when you change the digital settings on the panel.



<b>Check for arcs:*Turn out the lights and see if you can see any arching inside the machine while running at high power. </b>



<b>*Check the voltages outlined in this guide looking for hints</b>



[drive.google.com - Troubleshooting A K40 Laser Power Subsystem 1.0.pdf](https://drive.google.com/open?id=1yAGfqw3VVKW91qEpCVFOuAwYoyI7PPuh)


---
**Don Kleinschnitz Jr.** *June 12, 2018 12:44*

**+leize yan** "Inflate" the tube ???


---
**leize yan** *June 15, 2018 06:36*

**+Don Kleinschnitz** Has his problem been solved?




---
**Eric Lovejoy** *June 15, 2018 10:24*

maybe your mirrors are dirty.  I would use a kim wipe, but there might be something better. you want to clean the mirrors, but not scratch them,  and not leave particles or fibers on the surface. 


---
**Barefootjunkers** *June 27, 2018 17:51*

Don’t own a k40 yet but know co2 lasers from repairing large industrial machines. This looks completely like a control issue. Don has it right by checking the control voltages. Another issue to look for that can be really bad is looking to see if the machine is arching to the cabinet or tube mounts. DC current always follows the path of least resistance and will jump arch a good distance to do so. If there is dirt or film in the compartment containing the tube it WILL cause the tube to arch. Before cleaning you must discharge residual voltage from tube to physical ground. 


---
**James Lilly** *June 28, 2018 19:14*

Stock mirrors?  Replace them.  I had one crack, and only replaced that one.  I started to lose a bit of power, not as dramatically as you, but I replaced the other two and it was back to normal.  I'm thinking about upgrading the lens as well. Perhaps a can get a bit of a boost there as well.


---
**James Rivera** *June 29, 2018 17:39*

A new ZnSe lens proved to be a huge upgrade for me!


---
**James Lilly** *June 29, 2018 23:53*

Thanks James. Recommendations for a vendor?


---
**James Rivera** *June 30, 2018 00:21*

I bought this: "Improved 18mm ZnSe Focus lens (F50.8mm)" from LightObject.com.

[lightobject.com - Improved 18mm ZnSe Focus lens (F50.8mm)](https://www.lightobject.com/18mm-ZnSe-Focus-lens-P206.aspx?afid=1)


---
**James Lilly** *June 30, 2018 05:41*

Thanks.  I need just a little more umpff from my k40 without pumping the power up and figures a better quality lens would do the trick.


---
*Imported from [Google+](https://plus.google.com/110132594998321146499/posts/EwLWM2pUgQ3) &mdash; content and formatting may not be reliable*
