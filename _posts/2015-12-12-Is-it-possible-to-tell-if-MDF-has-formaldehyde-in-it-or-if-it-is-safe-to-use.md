---
layout: post
title: "Is it possible to tell if MDF has formaldehyde in it, or if it is safe to use?"
date: December 12, 2015 13:18
category: "Materials and settings"
author: "Nathaniel Swartz"
---
Is it possible to tell if MDF has formaldehyde in it, or if it is safe to use?





**"Nathaniel Swartz"**

---
---
**Anthony Bolgar** *December 12, 2015 13:56*

MDF will give off fumes, however, if you have a good exhaust hooked up to an outside vent, you should not experience any problems. PVC is the big NO-NO, it will give off chlorine gas while being cut.


---
**Scott Thorne** *December 12, 2015 15:28*

I can tell you that some plywood have a formaldehyde based glue in them....I would imagine that Mdf would be similar.


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/3MF2BGpb7L1) &mdash; content and formatting may not be reliable*
