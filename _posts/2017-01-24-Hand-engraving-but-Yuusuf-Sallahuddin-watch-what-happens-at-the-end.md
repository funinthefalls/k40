---
layout: post
title: "Hand engraving but Yuusuf Sallahuddin watch what happens at the end"
date: January 24, 2017 15:41
category: "Discussion"
author: "Ned Hill"
---
Hand engraving but **+Yuusuf Sallahuddin** watch what happens at the end.  This is what I was thinking with phosphorescent paint. 





**"Ned Hill"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 24, 2017 23:29*

That's cool & looks great. I used a glow-in-the-dark green coloured paint a while back on a tobacco pouch design that I did. Turned out pretty cool too.


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/ezFvnyXLJXz) &mdash; content and formatting may not be reliable*
