---
layout: post
title: "What is going on here with my set-up?"
date: September 18, 2016 14:21
category: "Original software and hardware issues"
author: "Ian Dodds"
---
What is going on here with my set-up?  There must be something I am failing to understand here.  I draw something in Corel and export it as a bitmap, telling the dialogue to "Maintain original size.".  I import it into the LaserDrw system, select engrave - and - its a completely different size.  I wasted gawd knows how many hours and card. Is there someone out there that can point me in the right direction?







**"Ian Dodds"**

---
---
**Phillip Conroy** *September 18, 2016 19:05*

Try my one minute guild to file [plus.google.com - How to Convert images from bitmap to Vector image in less than 1 minute- using…](https://plus.google.com/u/0/113759618124062050643/posts/1YW6LuJWNYQ)  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 18, 2016 22:20*

If you are using Corel Draw, why are you using the LaserDrw app? You should use the CorelLaser plugin so you can laser from Corel Draw.


---
**Ian Dodds** *September 20, 2016 11:29*

Prtogress - at last!  Thanks Yusaf.  Just had to install their version of Corel (Backward Step! - Hey Ho!).  Just why is the head moving sooo slooowly.,  As far as I can tell, the settings are the same as under the LaserDraw program.  At the very least, it seems to be producing the stuff at the right size.  This is important to me as I am a scale modeller. So, I'll let you know next year, when this job will be finished.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 20, 2016 11:48*

**+Ian Dodds** The head is possibly moving slowly as you haven't entered the Device ID/board model into the Corel Settings (maybe?).



Check: [https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwWVlDR3RyTzlzemM/view?usp=sharing)



&

[https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwbERTbWdISGY5eVU/view?usp=sharing)



& get your Device ID from your controller board like in this image (don't use mine as as it won't work & yours will be unique)

[https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwZEpMVUFEd1FuZUk/view?usp=sharing)


---
**Ian Dodds** *September 20, 2016 13:42*

Yusaf, you are a star and don't let anyone tell you different. What I did not realise was that the Corel thingy was pre populated - with rubbish. Thanks for your assistance - much appreciated!


---
*Imported from [Google+](https://plus.google.com/101362168822531541453/posts/YiaXMytK7nc) &mdash; content and formatting may not be reliable*
