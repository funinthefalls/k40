---
layout: post
title: "I absolutely love my new 500mm X 400mm 50W upgraded to 60W laser"
date: October 01, 2017 19:30
category: "Discussion"
author: "Anthony Bolgar"
---
I absolutely love my new 500mm X 400mm 50W upgraded to 60W laser. Cuts through 6mm MDF like a hot knife through butter, 10mm/s at 75% power, or 5mm/s at 50% power. After getting the new tube and PSU installed, I thought that I would have to go through the painful mirror alignment , but all it took was a quarter turn of the screw on the second mirror and I was back to being dialed in across the entire work area. Put the power meter on it and it peaked out at 58W at the second mirror, so basically double the power of my K40's I think I will put the 50W tube on one of the K40's to give it a little extra kick :)





**"Anthony Bolgar"**

---
---
**Don Kleinschnitz Jr.** *October 03, 2017 14:50*

Will watch your 50W upgrade, I plan that when my tube goes dead.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/99wHr1fST4q) &mdash; content and formatting may not be reliable*
