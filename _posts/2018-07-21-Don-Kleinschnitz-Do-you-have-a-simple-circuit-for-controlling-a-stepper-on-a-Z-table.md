---
layout: post
title: "Don Kleinschnitz Do you have a simple circuit for controlling a stepper on a Z table?"
date: July 21, 2018 15:21
category: "Discussion"
author: "Anthony Bolgar"
---
**+Don Kleinschnitz** Do you have a simple circuit for controlling a stepper on a Z table? All I need are up and down buttons to control the stepper via an external driver.





**"Anthony Bolgar"**

---
---
**Justin Mitchell** *July 21, 2018 16:33*

Typical step-stick type drivers have two pins you care about, step and direction.  Simplest possible circuit would be a switch for direction forward/back-high/low, and a button to do a single step. Anything more than that and youll very quickly find its simpler to just use a cheap microcontroller to read the buttons and pulse the driver.




---
**Don Kleinschnitz Jr.** *July 21, 2018 17:34*

Here is how I did it:\

[donsthings.blogspot.com - Stand alone K40 Zaxis Table & Controller Build](http://donsthings.blogspot.com/2016/05/k40-zaxis-table-controller-build.html)


---
**Anthony Bolgar** *July 21, 2018 18:30*

Thanks **+Don Kleinschnitz** I found another way as well while I was searching that uses an Arduino Nano, 2 buttons, and an easy stepper driver board, all parts I already had kicking around. Total cost about $20 . But I will check out your method just to learn something new. :)




---
**Rob Mac** *July 21, 2018 20:57*

**+Anthony Bolgar** I'm interested in a similar thing. Can you post your version?


---
**Anthony Bolgar** *July 21, 2018 23:18*

Sure, I'll do it tonight.


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/BrQHwxmoy2h) &mdash; content and formatting may not be reliable*
