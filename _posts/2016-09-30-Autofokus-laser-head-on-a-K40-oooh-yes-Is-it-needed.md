---
layout: post
title: "Autofokus laser head on a K40? oooh yes :) Is it needed?"
date: September 30, 2016 06:24
category: "Modification"
author: "HP Persson"
---
Autofokus laser head on a K40? oooh yes :)

Is it needed? nah

Is it fun to have - of course :)



Powered by a Arduino trinket/micro and A4988 stepper driver.

Code not done yet, neither the hardware - but time to design a laser nozzle to fit this design.



If anyone has any input, let me know, driving blind right now :)

Doing the part holding the lens on my 3D printer now, will use copper or alu later when the design is done.

I´m neither a awsome arduino programmer, nor a good mechanical designer, so tips and tricks is welcome :)



As seen in the pictures below, my 3rd mirror is mounted at top, the movable part will be below this one, with a bl-touch sensor or limit switch sensing the material.



With this parts, it will have a travel of 50mm



![images/023484104b596347ae031971e83b1ce5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/023484104b596347ae031971e83b1ce5.jpeg)
![images/942aa516de3788f08bbde62a9cec7268.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/942aa516de3788f08bbde62a9cec7268.jpeg)

**"HP Persson"**

---
---
**Gunnar Stefansson** *September 30, 2016 07:04*

Wooow not bad, that looks pretty sexy, I'm thinking it will be fun, ofc this is a great solution if your bed isin't movable :) Looking forward to see this in a video. Working ofc... :D




---
**HP Persson** *September 30, 2016 07:38*

Thanks! I tried making the bed movable first, but getting 4 corners aligned at the same height in the K40 was alot of work :)

Easier to move the head, and it gives me more space below for bigger objects, as i still have the complete depth untouched.



And another truth is - i can´t do the same thing others do, i have to try new stuff, maybe there is some medication for that :)


---
**Gunnar Stefansson** *September 30, 2016 08:39*

Hehehe Well trying New stuff and inovating, cause u can, is what we are all about ;) Looking forward to you final Solution :D


---
**Ariel Yahni (UniKpty)** *September 30, 2016 11:46*

It is a cool solution. Where is the stepper going to be mounted? 


---
**greg greene** *September 30, 2016 13:01*

My only worry would be the added stress and inertia from the weight of all that and it's effect on the mechanism to move the head.  I can see it limiting the speed and requiring a settling down period of time from the extra inertia.   


---
**HP Persson** *September 30, 2016 14:04*

Probably will go with the 28-BYJ48 steppers instead of that Nema8, smaller height.

And their weight + the parts has less wight than the big nozzle that comes with the other types of K40´s, not sure it will be a issue.

On the other hand, the carriage itself is alot bulkier than the other K40´s with nylon wheels and thin metal.

But may be a issue, i´ll have to test what it can do



Everything will be mounted to the right of the mirror and the stepper on top of the carriage part, only the lens moving.



Prototype of the moving part in the printer now, the round hole holds the lead screw, and the slot is to align the lens left<->right if needed.

![images/4a053491259eb2cade66830113f33805.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4a053491259eb2cade66830113f33805.jpeg)


---
**greg greene** *September 30, 2016 14:20*

looks like a great piece of kit


---
**Don Kleinschnitz Jr.** *September 30, 2016 14:51*

Nice.....Impressive start, did you design this entire head or modifying existing one? 



Been thinking about adjustable head for a long time but I'm still buried in my smoothie conversion. 



Thinking of it for autofocus similar to way new 3d printers level.



I planned to use parts from a DVD player. You can get them cheap on amazon, eBay. The linear steppers are tiny as are the other mechanical parts. 



Planned to use adafruit feather with stepper daughter card.



Hoped to build it laser cut from acrylic.

Wanted to integrate laser diode and air assist.



One builder warned against "open" optics as contamination issue..



I think I will still need my lift table to get more travel on setup.



Where did you get the slide?


---
**HP Persson** *September 30, 2016 16:56*

**+Don Kleinschnitz** This version of K40D doesnt have a "head", its just a piece of metal holding the lens. See pic.

I´m doing it similar this time, but adding laser pointer, air assist and a fan to the head too. So i have to design it from zero, but i have been running with my own design movable head for 6 months already so i gained some knowledge how to optimize it :)



Open optics is no problem at all, as a matter of fact there is no difference at all between the open and closed one in power..

Atleast at cutting, i did a test with Light Objects nozzle and the open one i have, no differnce, it was just a pain in the rear to clean the lens :)

And taking 2 seconds to clean the lens with a open lens i throwed away the LO-nozzle.

But if i would wait the same amount of time to clean my lens as i would do with a closed one, it would be dirty yes :)



The closed nozzles mostly used for machines where you add other gasses than air to help the cut. China people looked at them machines and made own solutions without knowing why.

But if it´s already on the machine, heck i would use it too. But never in my mind i would install one :)



If you look at some of the smaller lasers like Epilog, they don´t have a closed nozzle and it works perfect :)

Much in our K40´s aren´t real science, it´s just China manufacturers guessing :P

And there´s as many opinions about it as there is users out there :)

In the end i´m happy if my machine does what i wan´t it to do.



Slider i´m using is a MR7 slider, grabbed from Aliexpress for cheap :)

Don´t go with the dvd-drive steppers/motors, i bought one and it has alot of backlash and almost no holding torque.

Check out 28-BYJ48 or smaller Nema´s instead .

![images/9663e99a057697f3931c13fd269996db.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9663e99a057697f3931c13fd269996db.jpeg)


---
**Ariel Yahni (UniKpty)** *September 30, 2016 17:09*

I do think your gantry and rail is much better that the standard K40, i also agree that this open optics is used widely in major Laser machine manufacturers. I will also go with the motorized bed aka 3d printer and will do the auto level


---
**Ariel Yahni (UniKpty)** *September 30, 2016 17:10*

I do think your gantry and rail is much better that the standard K40, i also agree that this open optics is used widely in major Laser machine manufacturers. I will also go with the motorized bed aka 3d printer and will do the auto level


---
**HP Persson** *September 30, 2016 17:12*

**+Ariel Yahni** Oh yeah, i had a machine before with the old gantry. The new one is so good i would never go back, even if i got paid to do it.

No matter how i bend it, it doesnt move, no backlash, no vibrations, just solid as heck :)



There should be a slider-upgrade for the older type of gantrys, would probably sell good.


---
**HP Persson** *September 30, 2016 17:13*

I actually "stole" the sensor from my 3D printer to this project, haha

(until a new arrives from china)




---
**Stephane Buisson** *October 01, 2016 09:44*

Look good, it could be a new category if you succeed

I'll keep an eyes on your progress.


---
**HP Persson** *October 01, 2016 18:30*

Version 0.01 :P



Re-using the original mount to get everything correct before i make one from copper or aluminium.

Step 1 is proof-of-concept, get the darn thing to work :)



Black part is 3D-printed, pretty solid and no wobble, but will not use it in the final version :)



Time to make the stepper mounting.

![images/f143a2439ab4a9a2499a91945cc5b277.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f143a2439ab4a9a2499a91945cc5b277.jpeg)


---
**Ariel Yahni (UniKpty)** *October 01, 2016 18:34*

So the motor should move the black part that's attached to the lense holder? Is that going to move up and down perpendicular? 


---
**HP Persson** *October 01, 2016 18:40*

Yeah, motor is moving the black piece up/down. The gold part holding the lens will be in a solid part with the black in final version.

Thats why i´m using the small slider, to always have it 100% vertical aligned.

Mirror is always still, never moves.

To the right of the trapezoidal screw i´ll mount the BL-touch sensor and red dot.

Air-assist will be a pipe from behind/below the "laser head".


---
**HP Persson** *October 01, 2016 18:54*

Here´s a short video of my old head, it will work similar to this one, but the new one i make now i automatic :)




{% include youtubePlayer.html id="5Id7vQNlNAo" %}
[youtube.com - Adjustable lens mod for K40 laser](https://www.youtube.com/watch?v=5Id7vQNlNAo)


---
*Imported from [Google+](https://plus.google.com/112997217141880328522/posts/L2r1V3953Kr) &mdash; content and formatting may not be reliable*
