---
layout: post
title: "Greetings from Germany, I'm looking forward of buying an k40 but after some research here and on the interwebz I'm kinda scared of the additional costs for \"required\" upgrades like AirAssists, Smoothieboard, new underground"
date: January 31, 2017 09:38
category: "Discussion"
author: "Philipp Skorpil"
---
Greetings from Germany,

I'm looking forward of buying an k40 but after some research here and on the interwebz I'm kinda scared of the additional costs for "required" upgrades like AirAssists, Smoothieboard, new underground for cutting etcetera. With these and other the k40 goes easily up to > 750+ 💰 so wouldn't it be easier to buy laser engraver/cutter with all these features already included? And are there any out there you could recommend?





Thanks in advance



Phil





**"Philipp Skorpil"**

---
---
**Anthony Bolgar** *January 31, 2017 10:55*

FabKit by FabCreator 



[FabCreator Community](https://plus.google.com/u/0/communities/110904922300345487536)


---
**Ned Hill** *January 31, 2017 13:53*

Not sure you can find an "upgraded 40W" for less than $1000 shipped unless it's a used one.  Knowing what I know now I probably would have started with a 50W laser which generally have more features in addition to a larger work area and more power for around $1500 or less (still would be stuck with a proprietary controller).  But the 40W machines are still decent entry level machines that provide you good experience.


---
**Ned Hill** *January 31, 2017 17:40*

For me the basic initial upgrades for a typical K40 are an air assist head, improved focus lens, air pump (for air assist) and a new bed.  So you are looking at about $160 for all those with maybe another $20 in misc items. You should be able to find a 40W machine for around $400 shipped, which would put you in at about $580 total.  This would get you started and you can add other upgrades like a new controller down the road. (New controllers have become much easier to install now with things like the Cohesion 3D mini board).



Edit..Just realized you are in Germany so all those prices are in USD and I'm not sure how the costs are affected by shipping/VAT on your end.


---
**Philipp Skorpil** *January 31, 2017 19:17*

**+Ned Hill** and with new controller I'm at the called ~750$€£ that's my "worries". 



On aliexpress I pay around 360 bucks but have to add many hours of work and another 400+$€£ to get this thing working kinda great. 


---
**Kostas Filosofou** *January 31, 2017 19:57*

If you don't want to spend hours in the machine you ask the wrong people :) ... I don't know now how match you willing to spend but if for example your ceiling is 700-1000€ you don't have many options..


---
**Ned Hill** *January 31, 2017 19:57*

It will generally work fine without a new controller so I don't consider it a must have.  It just means that the software side is just a bit clunky to deal with and you can't do true gray scale etching which requires PWM.  Just depends on what you want to be able to do and your budget. It's really not that much more work to do the basic upgrades I mentioned over what you would have to do for any new machine install.   If you can afford it I would seriously recommend looking at the 50W units.   In addition to more power the 50W units have bigger work areas and typically come with an adjustable  bed and air assist.  Something to consider.


---
**Austin Baker** *February 01, 2017 07:26*

**+Ned Hill** Ned, when you say "improved focus lens" -- what is the go-to recommended source for a better lens? Am interested in tightening my beam if possible.


---
**Ned Hill** *February 01, 2017 12:59*

**+Austin Baker** I bought mine from Light Object.  You might be able to find cheaper ones out there, but with Light Object you get a known quality.  I bought their air assist head so I bought the "High Quality" 18mm lens (50.8mm focus) to go with it.  They also sell a slightly cheaper "Improved" lens.  Not sure how different they are.  My beam diameter is about 0.15mm.


---
**Austin Baker** *February 01, 2017 19:27*

**+Ned Hill** I'll look into the LO one. Beam waist of 0.15mm is pretty good -- I assume your max-power diameter is larger?  


---
**Ned Hill** *February 01, 2017 19:31*

Beam diameter is not really dependent on power.  You get thicker kerf at higher power because you get more burn to the sides.


---
**Austin Baker** *February 01, 2017 20:14*

**+Ned Hill** I guess I would argue that thicker kerf is the result of a larger beam waist. Power on the outer bounds (beyond the 1/e point) is raised sufficiently to begin to have an effect on the target material. 


---
**Paul de Groot** *February 01, 2017 20:22*

**+Philipp Skorpil**​ i did my upgrade for under a $150. Just an arduino running on grbl with my own designed shield. I probably even overspend by having it made by a chinese pcb factory but i eas curious about the process 😀!


---
*Imported from [Google+](https://plus.google.com/109452418744230245109/posts/dLd3d2p1hVc) &mdash; content and formatting may not be reliable*
