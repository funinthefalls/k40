---
layout: post
title: "So, the 3d printed v1.03 has arrived"
date: May 11, 2016 01:57
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, the 3d printed v1.03 has arrived. Printed with Brassfill (80% brass & 20% PLA) at 100 microns. Unfortunately, the 100 micron gives it a not so nice quality on the print leaving it not air-tight & thus defeating the purpose. I've plugged to airpumps into it to test if it passes the air, which it does, albeit weakly. I am considering sealing the exterior with something to provide air-tightness & see if that makes a difference to the final output pressure. I feel like it is leaking a lot of the air through minute gaps.



Other than that, I'm quite satisfied with the print, just it isn't functional without dodgying it up. So I've ordered another print, from the original printer that used Resin/DLP printer for my first attempt. I am fairly certain that this time around my design is sturdy enough to cater for the strength/weakpoints (as I filleted everything & increased the attachment area for the input nozzles). So, we'll see how that goes when I get it printed.



For the meantime, I will brush the exterior of this with hot beeswax (as that will air-tight it & I have heaps laying around). Then I'll give it some tests to see functionality with my 2x weak aquarium air-pumps.



![images/7adda09675303762837f10b1969c09ba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7adda09675303762837f10b1969c09ba.jpeg)
![images/3f00f690fd91ccd8a683e35640dece68.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f00f690fd91ccd8a683e35640dece68.jpeg)
![images/486d478e2386c5d7326b5428ece2b39d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/486d478e2386c5d7326b5428ece2b39d.jpeg)
![images/7a45e3c7a80168468a309fee35c77ff3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a45e3c7a80168468a309fee35c77ff3.jpeg)
![images/d2c43c94f62e9660023280ea56fdf07b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d2c43c94f62e9660023280ea56fdf07b.jpeg)
![images/cd21433665379a5b62b34dc19612968b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd21433665379a5b62b34dc19612968b.jpeg)
![images/39aad2d9ca3e845c813e94cce47b9c24.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/39aad2d9ca3e845c813e94cce47b9c24.jpeg)
![images/d70fff5da5f9989b7866fd299c454d85.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d70fff5da5f9989b7866fd299c454d85.jpeg)
![images/280cca13788943c908044dc78550e723.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/280cca13788943c908044dc78550e723.jpeg)
![images/bc90a9d3d8f81dfcde727d2f761b6edc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc90a9d3d8f81dfcde727d2f761b6edc.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 02:16*

**+Donna Gray** So far, no joy on this piece. Next print will be with resin & much higher quality (25 micron slices instead of 100, so 4 times the resolution). Hopefully will get that by next week (4 business days after he accepts the print before I can pickup). I'll keep you informed as to the success/failure of this part.


---
**Don Kleinschnitz Jr.** *May 11, 2016 02:38*

Spray or brush it with a few coats of lacquer or polyurethane .....


---
**Ariel Yahni (UniKpty)** *May 11, 2016 02:49*

What resin is he using? If the part is not thick enough it will break unless his using a special tough resin


---
**I Laser** *May 11, 2016 03:00*

Spewing, the top actually looks quite pretty, like a seashell, but obviously no good for the purpose.



Like Don said, I thought you'd be better served laquering/varnishing it. Though not sure how the PLA would handle it. 



Look forward to seeing the next one ;)


---
**Ariel Yahni (UniKpty)** *May 11, 2016 03:07*

I'm sure a good print of PLA even at 300 micron you suffice, buy you need good thickness on the weak points


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 03:11*

**+Don Kleinschnitz** Don't have any lacquer or polyurethane laying around, so I was just planning to beeswax it to seal it for this one. Although, I do have some  lacquer/sealer for my leatherwork which may do the trick. I'll give that a go & see what happens.



**+Ariel Yahni** I am unsure the resin he was using, but I will check that later. The part done with resin did break, but I didn't fillet joins/etc so it was weak on those areas (3mm outlet pipes with 1mm walls & 6mm inlet pipe with 1 maybe 2mm walls, can't remember on that one). Both the inlet pipe & outlet pipes joins were super weak, but the rest of the part (2mm walls) was actually quite strong.



**+I Laser** Yeah I just had to sand the interior hole. It was 26mm in design to fit the lens mount section, but was just a tad too tight. Might be that 26mm is not precise measurement as I don't have vernier calipers to get it spot on, I just eyeballed it with a ruler haha. Could also be the print distortions that made it too tight. Either way, after sanding/smoothing the interior it fits the lens mount.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 03:14*

**+Ariel Yahni** It just says "Castable Resin" or "Resin" in his material choices. I've increased the thickness on most areas of joins in the design to cater for the weakness.



Anyone have a 3D printer that would like to do test prints? It is getting costly for me to keep paying $30 per print. If so, I'm happy to provide the STLs.


---
**I Laser** *May 11, 2016 03:20*

I assume the photos you previously provided were just 3D renders?


---
**Dushyant Ahuja** *May 11, 2016 03:20*

Try XTC 3D. 


---
**Anthony Bolgar** *May 11, 2016 03:24*

I have had great success using this one.



[http://www.thingiverse.com/thing:1063067/#files](http://www.thingiverse.com/thing:1063067/#files)



I even have one already printed that I can send you.


---
**Ariel Yahni (UniKpty)** *May 11, 2016 03:42*

**+Yuusuf Sallahuddin**​​ it's very expensive those tests. Castable resin is very weak. Send me the STL and I'll print them


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 09:31*

I actually chopped this one open with a knife to see what is going on inside it. I wanted to see if the air channels were blocked during the print. Surprisingly they were not, so the issue with the air coming out super weak was due to gaps between the layers when printed (which I couldn't see but could vaguely feel the air exiting them). So this one is destroyed. Getting it printed again by the guy who does it in Resin.



**+I Laser** No, they were photos from the guy who printed it. Obviously not as close up as I took them.



**+Anthony Bolgar** Thanks for that Anthony, but I'm trying to create one that has a lower profile overall & minimises the chance of hitting the air-assist with the beam. How much does that one extend below the lens itself? This version I am trying to get a test of extends only 10mm below the lens. Also, the internal beam hole is 6mm diameter.



**+Ariel Yahni** STL file here: [https://drive.google.com/file/d/0Bzi2h1k_udXwUF8xa0lQTUtMV0k/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwUF8xa0lQTUtMV0k/view?usp=sharing)

Hopefully you can get good results with it.


---
**HP Persson** *May 11, 2016 16:12*

Hope u dont mind i snag the STL too, very interested in trying to make the optimal air-nossle for my type of work :)



Bought a 3D printer yesterday, oh man, my K40 is soon more ABS/PLA than metal :P


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 11, 2016 23:05*

**+HP Persson** Yeah, go for it HP. I'm happy for anyone to use the design if it works for them. If anyone prints it & gets it working, keep us all updated on the progress.



One thing I noticed (when testing this BrassFill one yesterday) was that once you put your lens mount in, you can't get it out. Well, not easily. In order to get it out I had to use pliers (with some cloth in between the teeth to protect the lens mount) & pull it out that way.



Also, I'd suggest printing with as small a layer slices as possible. 100 microns just didn't seem to do the job to satisfaction.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 13, 2016 02:40*

Updated version: [https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/Ew93jx1Y4hv)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/GvfYhhd3Vp7) &mdash; content and formatting may not be reliable*
