---
layout: post
title: "Engraver and software have worked fine for months and now the software will not load"
date: October 04, 2016 01:52
category: "Original software and hardware issues"
author: "Jacob Hahn"
---
Engraver and software have worked fine for months and now the software will not load. Uninstalled and reinstalled and I get the same error. OS is win10. Anyone know what I'm doing wrong? Thanks.



[Window Title]

LaserDRW Main Programer



[Main Instruction]

LaserDRW Main Programer has stopped working



[Content]

A problem caused the program to stop working correctly. Windows will close the program and notify you if a solution is available.



[Close program]





**"Jacob Hahn"**

---
---
**greg greene** *October 04, 2016 02:05*

You have fallen victim to the win 10 auto update feature - try re-installing from the original cd if you have it or go back to win 8


---
**Jacob Hahn** *October 04, 2016 21:06*

Thank You Greg that worked!




---
**greg greene** *October 05, 2016 14:00*

Curse you Bill Gates !!!! :)


---
*Imported from [Google+](https://plus.google.com/112436114464277702509/posts/KK8zyWzEeu3) &mdash; content and formatting may not be reliable*
