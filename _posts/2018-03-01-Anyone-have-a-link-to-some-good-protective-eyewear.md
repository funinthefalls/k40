---
layout: post
title: "Anyone have a link to some good protective eyewear?"
date: March 01, 2018 20:52
category: "Discussion"
author: "Seon Rozenblum"
---
Anyone have a link to some good protective eyewear?



My K40 was supposed to come with some eyewear (I know, cheap stuff and useless) but it didn't, so apart from the orange perspex on the lid, I have no other eye protection.



Super hard to know whats good and what's not.



Or is the lid enough?



I find myself constantly watching the laser - it's mesmerising!





**"Seon Rozenblum"**

---
---
**greg greene** *March 01, 2018 21:14*

ebay - 29 bucks


---
**Anthony Bolgar** *March 01, 2018 21:24*

All you need is acrylic safety glasses, available at any big box home store like Homedepot. There is no need for anything more, as acrylic absorbs the wavelength of a CO2 laser (That is why it can cut acrylic)IT does not transmit any of the CO2 wavelength through the acrylic, therefor it does not get to your eye. I did some testing and an unfocused beam from a 40W laser at a distance of 12" takes 6 seconds to burn through the acrylic. More than enough protection for a stray beam reflection that may escape the enclosure. Get tinted ones if you can, they help to dull the bright visible light that is created.


---
**greg greene** *March 01, 2018 22:31*

Thanks Anthony !


---
**Seon Rozenblum** *March 01, 2018 22:42*

**+Anthony Bolgar** Thanks! My local hardware doesn't have anything suitable (believe it or not) but $29 ebay tinted ones ok?


---
**greg greene** *March 01, 2018 22:43*

they are clear


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/i7PbAPzrAGb) &mdash; content and formatting may not be reliable*
