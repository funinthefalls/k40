---
layout: post
title: "Does anyone know if there is an affordable system to control a laser cutter but which does support PPI cutting?"
date: July 25, 2016 10:45
category: "Discussion"
author: "Bart Libert"
---
Does anyone know if there is an affordable system to control a laser cutter but which does support PPI cutting? I currenly run my self built 80W cutter based on linuxcnc with modifications to do PPI cutting. All motors are nema23 so I have dedicated stepper drivers and nog pololu's or alikes. So I would like to be able to drive my machine with another controller and be able to setup pulsed cutting with aprox 3ms pulses. This gives you a soooo much better cut quality. You do have to speed down a little in most parts, but I can cut plywood with such great detail that you don't even see anything of burnt edges by using this technique (combined with some air assist of course). Any ideas are very welcome.﻿





**"Bart Libert"**

---
---
**Don Kleinschnitz Jr.** *July 25, 2016 11:18*

Long term I am also interested in PPI, sharing some of my research:



This is an interesting post on the subject: [http://hackaday.com/2014/06/11/better-lasing-with-pulses/](http://hackaday.com/2014/06/11/better-lasing-with-pulses/)



Ideally this is built into the controller (Smoothie) but it requires tracking position and pretty fast processing.

[http://smoothieware.org/forum/t-1604860/laser-pulsed-control-ppi](http://smoothieware.org/forum/t-1604860/laser-pulsed-control-ppi)



Here is a add on design that uses an external counter to track and pulse: [http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/](http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/)



Here is a arduino based controller: 
{% include youtubePlayer.html id="Ow0VGplSMiY" %}
[https://www.youtube.com/watch?v=Ow0VGplSMiY](https://www.youtube.com/watch?v=Ow0VGplSMiY)

Here is some code: 

[https://github.com/dirktheeng/SimplePPI](https://github.com/dirktheeng/SimplePPI)

[https://github.com/dirktheeng/PPI-Controller](https://github.com/dirktheeng/PPI-Controller)




---
**Bart Libert** *July 25, 2016 11:25*

Don, I know about these articles. I allready use PPI cutting and it is fantastic. I was wondering if there are any boards available that can do PPI cutting without me having to use a PC with ubuntu and linuxcnc. Getting a pc with little jitter to run linuxcnc is a pain in the $$$ . It is like a lottery. Maybe some dedicated boards can do this a lot better. I just need to be able to play a gcode file. Don't need any fancy GUI interfaces or whatever.


---
**Don Kleinschnitz Jr.** *July 25, 2016 11:45*

**+Bart Libert** I haven't found any "buildable" source yet. I have added some arduino code above (just edited) that would be cheap to build if it worked.


---
**Bart Libert** *July 25, 2016 12:10*

There is a possibility I am evaluating. It would not be that difficule, but 1 single arduino wouldn't handle it. It should be perfectly doable with 1 simple arduino running grmbl and 1 arduino or lookalike running some ppi shooting code. A standard grmbl is capable of jitter free 30kHz steprates on 3 axes. A lasercutter only uses 2 axes ;) That means that there is a way to have the laser shoot on the 3rd axis. The only thing you need to make is some circuitry which can make the laser shoot 1 pulse of 3ms each time the Z axis gets a step. And then you need a preprocessor in software to calculate how many pulses the Z should give based on the distance travelled. All very doable, but it is not 1 single arduino. I'll try this out when I'm back from holidays. LinuxCNC currently is a working solution, but..... thats it. I need a quite expensive pc which is never jitter-free and also likes to crash now and then. 




---
**Bart Libert** *July 25, 2016 12:15*

by the way I am only talking about vector cutting there, not about raster engraving. Thats another thing.




---
*Imported from [Google+](https://plus.google.com/104850277500909359562/posts/FVK46Qyozge) &mdash; content and formatting may not be reliable*
