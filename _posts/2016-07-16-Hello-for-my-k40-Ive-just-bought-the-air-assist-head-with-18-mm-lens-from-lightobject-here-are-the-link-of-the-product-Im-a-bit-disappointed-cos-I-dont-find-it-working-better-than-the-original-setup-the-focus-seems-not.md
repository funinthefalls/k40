---
layout: post
title: "Hello, for my k40 I've just bought the air assist head with 18 mm lens from lightobject, here are the link of the product: I'm a bit disappointed cos I don't find it working better than the original setup, the focus seems not"
date: July 16, 2016 12:39
category: "Modification"
author: "Giacomo Deriu"
---
Hello,

for my k40 I've just bought the air assist head with 18 mm lens from lightobject, here are the link of the product:

[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)

[http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx)



I'm a bit disappointed cos I don't find it working better than the original setup, the focus seems not good as the 12 mm lens.

After a fast mirrors allignement, I tried making some cut in paper. At firts I didn't know which side of the lens should be pointing upward (no documentation at all), at the beginning I thought that it must be the convess side, but the cut was very bad and I couldn't find where the right focus was. Then I tried to flip it (convess downward), seems working better although the focus was far from being good.



Is there anyone that has this system working that can give me advices about how to do the right setup?



thanks



Giacomo





**"Giacomo Deriu"**

---
---
**Jim Hatch** *July 16, 2016 13:15*

You're right about no documentation. However, this is one of those upgrades that it's assumed if you know enough to order one then you know what you need to do to install it.



A convex CO2 laser lens always installs with the curve (or bump) upwards. That's a pretty easy mnemonic to keep in mind too - bumps go up :-)



Make sure you're measuring your focus correctly. It will be from a different point than on your stock lens assembly. First, check the lens - you linked to the proper 50.8mm lens but they also offer a shorter focal lens. You need to match your focus distance to the lens you bought. (Both of the ones they offer will work but there are some differences in why you'd want one vs. the other. Using the same 50.8mm focal length you had originally is fine.)



Measure your focus distance from the bottom of the lens, not the bottom of the holder (the point of the cone). I have a different version of this mount but the bottom of the lens should be resting about at the bottom of the knurled ring nut that's just above the nozzle. You want to measure from there.



The other thing you may find is that the mirror alignment on your machine worked fine with the stock lens assembly but the beam is not actually perpendicular to the head. That's okay with stock because it just lands a bit to the left or right (or in front or behind) the head. With the air assist you need it to go through the center of the nozzle. If it's off just a bit part of the beam will hit the nozzle assembly which means you don't get a full beam and power to hit the material. To see if this is happening to you simply take the air nozzle off, get the material the proper distance (50.8mm) from the lens and see if your cut is better. If it is, you're blocking some of the beam with the nozzle and you need to do a very good alignment - make sure the beam hits the center of the mirror opening and that it is coming out directly below the nozzle opening.



Once you get that done you should see better performance than with the stock lens & mirror because those weren't as optically precise as the LO ones. Also, as a last step (or maybe a first one, but I assumed you did this) make sure you clean the lens and mirror and don't have any fingerprints or dust or smoke residue on them.


---
**Ned Hill** *July 16, 2016 22:21*

I second what Jim said about aligning the head.  I have this LO air assist head and lens also.  You need to put a piece of tape on the bottom of the air assist nozzle and make sure the beam is coming out of the center.  I use an inspection mirror to check after doing the test fire and rotate the head mirror as needed to center.   All this would be easier if the machines just came with adjustable  tube mounts.  Also, I found the LO 18mm lens focus to be spot on at 50.8mm. 


---
**Bart Libert** *July 23, 2016 15:27*

Maybe a strange question, but I'm used to working with higher wattage lasers and a self built 80W co2 cutter. On these smaller laser heads, how do you actually set the hight to your material ? For example I know that on my 80 watt laser, I need 12mm between the nozzle and the wood surface. Because I have a up/down Z table, I can easily set that height, but how is this done on these kind of heads ? Can you lower/rise the head somehow to set the correct distance ? A 3mm wood or a 1mm veneer would need different distances to focus correctly ?


---
**Ned Hill** *July 23, 2016 15:59*

**+Bart Libert** You still have to have some way to adjust the height of the bed.  Usually some kind of DIY manual z-table.  You can buy motorized z-tables for these smaller machines. You can search this group for z-table and find a number of different approaches.


---
**Jim Hatch** *July 23, 2016 16:25*

You'll need to make some kind of adjustable table. I have a set of spacers that fit my table and I can set the actual bed in 1/8" heights by adding or subtracting spacer plates of plywood underneath the steel plate I use as the bottom of my bed. That's good for the materials I use but isn't as convenient as a fully adjustable bed. You can get the motorized on from LightObject or build your own manual one with threaded rod & nuts. Or you can get a fairly inexpensive "lab jack" from Amazon.



I did make a spacer tool for measuring my table to lens height. It's 50.8mm and slips between the bottom of the knurled ring that's the bottom of the lens. You can also measure between the nozzle end and the material when it's set to the 50.8mm size from the lens bottom. It'll be in the 20mm-ish size. Then you can just place that between the nozzle and the material and you'll have it focused without having to measure from the lens itself.


---
**Bart Libert** *July 23, 2016 16:29*

so by default this machine is focussed for what kind of thickness then ? People use it out of the box, but different thicknesses of material get engraved. Is there a default material width used then ?


---
**Ned Hill** *July 23, 2016 16:31*

Supposedly with the stock bed it's focused at the surface since you put your work piece in the clamp.


---
**Jim Hatch** *July 23, 2016 16:33*

🙂 no clue anymore. One of the first things I did was pull out the stock bed. You have some height adjustability using the spring clamp on the OEM and it's fairly easy to measure from the bottom of the lens because the lens holder is flat to the bottom of the lens. But it's a pretty small work area that way. 


---
*Imported from [Google+](https://plus.google.com/112554405852389236666/posts/ixhKnbCN87k) &mdash; content and formatting may not be reliable*
