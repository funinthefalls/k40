---
layout: post
title: "Hi Folks Just joined to ask if there is any 'market' for a K40"
date: February 12, 2018 16:14
category: "Discussion"
author: "Lesley Davis"
---
Hi Folks Just joined to ask if there is any 'market' for a K40. I have acquired one and need to dispose of it. Don't know if it works but seems to have all the bits - fan , water pump, vent tube. Is it any good for spares etc? 

Thanks for your help.

 





**"Lesley Davis"**

---
---
**Anthony Bolgar** *February 12, 2018 16:48*

Where are you located?


---
**Lesley Davis** *February 12, 2018 17:06*

I am in Towcester, Northamptonshire


---
**Andy Shilling** *February 12, 2018 20:34*

+Lesley Davis have you got photos please.


---
**Lesley Davis** *February 12, 2018 23:23*

**+Andy Shilling** 

[https://plus.google.com/photos/...](https://profiles.google.com/photos/109055778368629187249/albums/6521812557841479457/6521812557492138034)


---
**Lesley Davis** *February 12, 2018 23:26*

Hi Andy. Hope the pics arrive. Not great with the technology

[https://plus.google.com/photos/...](https://profiles.google.com/photos/109055778368629187249/albums/6521813349300271825/6521813349592864946)


---
**Andy Shilling** *February 12, 2018 23:37*

It looks in good condition, are you not willing to try powering it?

 


---
**HalfNormal** *February 13, 2018 12:21*

**+Andy Shilling** Isn't your K40 getting lonely?


---
**Andy Shilling** *February 13, 2018 14:24*

**+HalfNormal** hhhhhhmmmmmmmm but what do you pay for a machine with no history?.



 I've just fitted new 60w and 24v psu's, new Mo mirrors, GaAs lens and modified it to exhaust out the side with an extension chamber for the new 60w tube when this old tube dies. So this one would need to be a fair price to convince me.


---
**HalfNormal** *February 13, 2018 16:29*

See, you have plenty of spare parts for this machine!


---
**HalfNormal** *February 13, 2018 16:30*

I purchased one that had an issue with the controller board for $150 USD


---
**Andy Shilling** *February 13, 2018 16:53*

**+HalfNormal** lol maybe. +Lesley Davis how much do you want for it buddy? Get a price on here and people might show an interest.


---
**James Rivera** *February 13, 2018 18:36*

Not trying to torpedo the sale, but how old is this? It has a florescent tube (mine is well over a year old and has LEDs) and the viewport window is clear color (mine is orange) which makes me wonder: is going to safely block the laser light? That being said, these are minor, but worth considering before purchasing.


---
**Andy Shilling** *February 13, 2018 18:49*

**+James Rivera** mine originally didn't have any lights, a moshi board and lps from the dark ages lol. I've got a clear lid on mine but a pair of glasses are way more reliable than an orange lid I'd suspect.




---
**James Rivera** *February 13, 2018 19:17*

**+Andy Shilling** Agreed. I bought laser glasses specifically for CO2 laser wavelength, and I wear them whenever the laser is on, even if the lid is closed.


---
**Andy Shilling** *February 14, 2018 13:54*

Has Lesley been back on here since the post of images 2 days ago? I was quite interested in this.


---
**Lesley Davis** *February 15, 2018 19:31*

Hi Andy

I followed the suggestion and turned on the beast this afternoon. Lights and action!! The power light came on and there was a loud 'grinding' noise. So yes something is happening but I know that does not mean it is a working machine. But progress. Thanks for all the comments and suggestions.




---
**Lesley Davis** *February 15, 2018 19:34*

Don't know how old it is as it came from my late brothers workshop along with a load of other stuff. It may be old as he was unwell for a while. I didn't have a clue what it was until google came to the rescue.


---
**Andy Shilling** *February 15, 2018 19:55*

Hi Lesley thanks for getting back to us. Grinding noise didn't sound good but to be honest I don't know how you would discribe the stepper motors noise. Do you have any idea how much you are looking at for it. I could be interested but did need to speak to a work mate to see if he would be willing to collect it for me.


---
**Lesley Davis** *February 15, 2018 20:34*

How about £180 as an opening bid? 


---
**Andy Shilling** *February 15, 2018 20:54*

Ok to be fair is possibly worth that but I couldn't offer any more than £100 based on the fact it's not proven to work 100% and it would only be spare parts to me but I'm happy to stand aside and let somebody else offer more to you.


---
**Lesley Davis** *February 15, 2018 21:03*

I think that is very fair of you and if you are willing to go with it and make enquiries about a pick up I would be happy. Is there a way to chat outside of the public site? Can I email you direct?


---
**Andy Shilling** *February 15, 2018 21:09*

I'm not sure if we can private message on here, I will try and find out and get back to you. If not I'll give you my email address.


---
**Andy Shilling** *February 15, 2018 21:22*

**+Lesley Davis** hopefully I've managed to message you now.




---
**HalfNormal** *February 16, 2018 06:33*

**+Andy Shilling** I knew you could not resist! Now you original K40 will have an adopted sibling!


---
**Lesley Davis** *February 16, 2018 09:50*

You know what they say - it's always nice to have a friend to keep you company.


---
*Imported from [Google+](https://plus.google.com/109055778368629187249/posts/B2f6me7K3tw) &mdash; content and formatting may not be reliable*
