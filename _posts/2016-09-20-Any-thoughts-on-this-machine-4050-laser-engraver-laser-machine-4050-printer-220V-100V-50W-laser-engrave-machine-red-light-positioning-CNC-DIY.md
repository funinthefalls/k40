---
layout: post
title: "Any thoughts on this machine? 4050 laser engraver, laser machine 4050 ,printer 220V / 100V ,50W laser engrave machine, red light positioning CNC DIY"
date: September 20, 2016 19:02
category: "Discussion"
author: "Derek golding"
---
Any thoughts on this machine? 



4050 laser engraver, laser machine 4050 ,printer 220V / 100V ,50W laser engrave machine, red light positioning CNC DIY

 [http://s.aliexpress.com/Vf6vMZVN](http://s.aliexpress.com/Vf6vMZVN) 







**"Derek golding"**

---
---
**Chris Boggs** *September 21, 2016 03:03*

I just purchased this item about 2 months ago and LOVE it..  It has been amazing.. Shipped from US and delivered in about 5 days..  Just wanted to let you know..  I'd be happy to answer any questions you may have.. 

 [ebay.com - Details about  50w CO2 USB Laser Engraving Cutting Machine Engraver Cutter 500mm X 300mm](http://www.ebay.com/itm/301754502253)


---
**Derek golding** *September 21, 2016 07:15*

Completely different machine by the looks of it?


---
**Stephen Sedgwick** *September 21, 2016 12:21*

I believe the one on E-bay has a little nicer set of controls/interface.  The other benefit is it isn't something you need to wait 3 to 4 weeks to get...




---
**Derek golding** *September 22, 2016 07:28*

 Does not ship to NZ and I'd also have to pay import duties if it did. Free trade agreement with China so no duties to pay in NZ. Need 240v as well. Has no one used Aliexpress?


---
*Imported from [Google+](https://plus.google.com/+Derekgolding-GoldingArts/posts/Qqxgzx124ed) &mdash; content and formatting may not be reliable*
