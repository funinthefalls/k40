---
layout: post
title: "How to make 124 custom dice with a k40"
date: August 26, 2016 03:46
category: "Discussion"
author: "Derek Schuetz"
---
How to make 124 custom dice with a k40

![images/ae737c144bef24b4f3f5ef1c8320ef6d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ae737c144bef24b4f3f5ef1c8320ef6d.jpeg)



**"Derek Schuetz"**

---
---
**Alex Krause** *August 26, 2016 03:59*

Nice cable management of your air assist and diode power wires :)


---
**Alex Krause** *August 26, 2016 04:00*

Please post your finished results really like the work holding you made as well


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 04:28*

Looks good. I'd also like the see the finished result (of at least 1 dice).


---
**Derek Schuetz** *August 26, 2016 04:43*

[https://imgur.com/a/VUDJ0](https://imgur.com/a/VUDJ0)



Here's from my previous 36 batch template 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 26, 2016 05:14*

Thanks for showing **+Derek Schuetz**. They turned out pretty cool. What kind of paint did you use to fill in the engraves? & did you seal with something also?


---
**Derek Schuetz** *August 26, 2016 05:37*

I used sharpie paint pen for the gold and the black is crayon. No seal on it I tried other paints and polishing but couldn't get good resulys


---
**John Austin** *August 28, 2016 21:41*

very cool



could you tell me what the dimension of the cable management chain you have?

The one I order is to big and like to but my air lines and wires in one to keep them out of the way.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/7uvCYgJwYJH) &mdash; content and formatting may not be reliable*
