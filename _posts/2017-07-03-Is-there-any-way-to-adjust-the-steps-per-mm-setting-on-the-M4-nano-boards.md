---
layout: post
title: "Is there any way to adjust the steps per mm setting on the M4 nano boards?"
date: July 03, 2017 17:52
category: "Discussion"
author: "laurence champagne"
---
Is there any way to adjust the steps per mm setting on the M4 nano boards?





**"laurence champagne"**

---
---
**Scorch Works** *July 04, 2017 01:59*

Is that a newer version of the stock M2 nano board? What software are you using?


---
**laurence champagne** *July 04, 2017 05:52*

**+Scorch Works** 

Sorry, I meant to say M2 nano, I don't know why i keep thinking it is M4.

Do you know any way at all to change it on the M2? I love that board but not being able to adjust the steps is a major flaw.




---
**Scorch Works** *July 04, 2017 15:14*

Short Answer:

From the LaserDRW engraving manager click on the "Properties" button the change the "Resolution" value.



Longer Explanation:

(This explanation is based on my understanding as of today. I may be wrong.)



Changing the "Resolution" does not change the number of motor steps to achieve a given move but it will change the smallest software step used internally by LaserDRW when generating the laser path.  When LaserDRW makes a line (that is not vertical, horizontal or at exactly 45 degrees) the line is constructed of a whole bunch of small steps.  Each step has a size equal to 1/Resolution.  The default Resolution is 1000 dpi, so each step is 0.001 inches.



I have not played with the Resolution setting very much but I did set it to 2000 dpi one time and it seemed to work.  I can only guess that the controller is using fractional stepping to achieve the small moves because the diameter of the timing pulley and the 200 step motors would indicate that the smallest motion from one motor step would be about .008 inches.



I am not sure exactly sure what you are trying to accomplish but the resolution is something you can change that may get you there.  A higher resolution setting may create a traffic jam in the data being sent to the controller.  The laser may start and stop while waiting for data.

![images/b518d6bc30610a4731717b05a937d7a6.png](https://gitlab.com/funinthefalls/k40/raw/master/images/b518d6bc30610a4731717b05a937d7a6.png)


---
**laurence champagne** *July 04, 2017 18:28*

Thanks for the detailed write up. I might try to look into that. 



I basically made a new machine from my k40. I needed a bigger cutting area. So new belts have messed with the accuracy of my machine. I think it is off .025% in the x axis and .02% in the Y axis. Doesn't sound like a lot but it screws everything up if I don't account for this when making my designs. So when I update to a new board like I have with the cohesion all of my designs now have to be updated.



Plus I only do engraving and the process and simplicity of corellaser program is vastly better than laser web and cohesion in my opinion.



For most other chinese boards you can adjust the steps/mm in rd works but that software doesn't work with the nano.



It just blows my mind that an important setting like this isn't available for the nano boards.


---
*Imported from [Google+](https://plus.google.com/107692104709768677910/posts/jhbJC1yPPD5) &mdash; content and formatting may not be reliable*
