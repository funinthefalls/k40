---
layout: post
title: "also laser welding, Sketchup extention, multilayers. another great video by HPI"
date: October 31, 2016 15:07
category: "External links&#x3a; Blog, forum, etc"
author: "Stephane Buisson"
---
also laser welding, Sketchup extention, multilayers.

another great video by HPI.





**"Stephane Buisson"**

---
---
**Don Kleinschnitz Jr.** *October 31, 2016 16:35*

very cool, on my list to try. Did you find the SU extension?


---
**ThantiK** *November 01, 2016 02:26*

Damn, it's too bad they used a worthless piece of software like sketchup


---
**HalfNormal** *November 01, 2016 03:08*

Here is an in depth description on how this works [http://stefaniemueller.org/laserstacker/](http://stefaniemueller.org/laserstacker/)

[stefaniemueller.org - Stefanie Mueller     » LaserStacker](http://stefaniemueller.org/laserstacker/)


---
**Don Kleinschnitz Jr.** *November 01, 2016 11:32*

Still can't find the extension anywhere...


---
**HalfNormal** *November 01, 2016 12:03*

**+Don Kleinschnitz**​ From what I read, it is not published. 


---
**Stephane Buisson** *November 01, 2016 13:23*

I can't see where [21] link to, but why not asking Stefanie MUELLER directly (her email is on the same site).


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/gvzVGsEiEeN) &mdash; content and formatting may not be reliable*
