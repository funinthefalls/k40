---
layout: post
title: "Does anyone have any experience with this?"
date: October 05, 2016 12:57
category: "Discussion"
author: "Bob Damato"
---
Does anyone have any experience with this? Looks like it could be handy for some of the leather work and other fine material. Im guessing its for engraving only and not cutting, as it would likely cut into this.


{% include youtubePlayer.html id="xXPqzB62Ji8" %}
[https://www.youtube.com/watch?v=xXPqzB62Ji8](https://www.youtube.com/watch?v=xXPqzB62Ji8)





**"Bob Damato"**

---
---
**Ariel Yahni (UniKpty)** *October 05, 2016 13:37*

I saw this a while back, I'm interested also


---
**Anthony Bolgar** *October 05, 2016 14:38*

Interesting...how much is it?


---
**Ariel Yahni (UniKpty)** *October 05, 2016 14:54*

[http://www.rowmark.com/seklema/seklema.asp](http://www.rowmark.com/seklema/seklema.asp) no price on the site


---
**Bob Damato** *October 05, 2016 16:48*

Its a little spendy but not terrible if it really does the job.

[http://www.laserbits.com/catalogsearch/result/?q=seklema&x=0&y=0](http://www.laserbits.com/catalogsearch/result/?q=seklema&x=0&y=0)


---
**Ariel Yahni (UniKpty)** *October 05, 2016 16:51*

Agree


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/a2D8kSas1Rh) &mdash; content and formatting may not be reliable*
