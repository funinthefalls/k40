---
layout: post
title: "To my big surprise this piece of 6\" ducting is a pressfit and works beautifully with two screws and a bit of duct tape!"
date: February 25, 2017 23:58
category: "Modification"
author: "Timo Birnschein"
---
To my big surprise this piece of 6" ducting is a pressfit and works beautifully with two screws and a bit of duct tape!

![images/1b1cd544bf392bb1a05790cded25c7f0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b1cd544bf392bb1a05790cded25c7f0.jpeg)



**"Timo Birnschein"**

---
---
**Ariel Yahni (UniKpty)** *February 26, 2017 00:03*

Nice, can you share a link to where it can be bought ?


---
**Timo Birnschein** *February 26, 2017 00:33*

Home Depot!![images/b21ec9fc8eccad062a0230bf5d0a82c1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b21ec9fc8eccad062a0230bf5d0a82c1.jpeg)


---
**Timo Birnschein** *February 26, 2017 00:34*

Sucks really well and produces good vacuum inside the laser. ![images/1331bbab1b988d84f1e84b20e088f20e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1331bbab1b988d84f1e84b20e088f20e.jpeg)


---
**Don Kleinschnitz Jr.** *February 26, 2017 00:41*

Been there done that ... just read the blog :)

[donsthings.blogspot.com - K40 Air Systems](http://donsthings.blogspot.com/2016/06/k40-air-systems.html)


---
**Steven Whitecoff** *February 26, 2017 04:08*

Sweet I just happen to have all that and lots of flex hose, and a serious fan that just needs cleaned and lube but I hadnt realized it would just fit perfectly or I'd already have done it.




---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/5GbXrxwSWGt) &mdash; content and formatting may not be reliable*
