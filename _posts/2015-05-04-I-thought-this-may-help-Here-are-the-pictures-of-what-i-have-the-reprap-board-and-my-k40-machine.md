---
layout: post
title: "I thought this may help. Here are the pictures of what i have, the reprap board and my k40 machine"
date: May 04, 2015 16:56
category: "External links&#x3a; Blog, forum, etc"
author: "Allabout kidzinflatables"
---
I thought this may help. Here are the pictures of what i have, the reprap board and my k40 machine. I am needing help with the wiring of them together. I am not the great with wiring, so help with pic or diagrams would be great.

Thank You



![images/4757e92f23e81411d03954551fb40bcb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4757e92f23e81411d03954551fb40bcb.jpeg)
![images/00e97fe8d7183b0cbbc95f4966f079c6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/00e97fe8d7183b0cbbc95f4966f079c6.jpeg)

**"Allabout kidzinflatables"**

---
---
**Allabout kidzinflatables** *May 04, 2015 17:22*

Ok thank you


---
**Allabout kidzinflatables** *May 04, 2015 17:34*

Thank you for your response, i have looked over the Lansing Makers Network Marlin for Lasercutters for a couple of days now and i cant seem to flow all of the wiring. Also i am not sure if i am needing any additional wires to make jumpers. Are there any detailed pictures out there that show the wires landing on them.


---
**David Richards (djrm)** *May 04, 2015 18:25*

Gretings, i found these notes useful to helpm me to understand how the system parts work together: [http://forum.planet-cnc.com/viewtopic.php?f=11&t=127](http://forum.planet-cnc.com/viewtopic.php?f=11&t=127) kind regards, David.


---
**Joey Fitzpatrick** *May 04, 2015 18:29*

Check out these sites.  These are where I found most of the info I needed for my K40 ramps upgrade. Be sure to read the comments at the bottom of the page. There are several other links that people have posted in their comments.  [http://weistekengineering.com/?p=2406](http://weistekengineering.com/?p=2406) . And      [http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html?m=1](http://3dprintzothar.blogspot.com/2014/08/40-watt-chinese-co2-laser-upgrade-with.html?m=1). 


---
**Allabout kidzinflatables** *May 04, 2015 18:40*

ok thank you


---
**Allabout kidzinflatables** *May 04, 2015 20:11*

These two sites are very good, my only question is, the wiring diagram shows the ribbon cable. Is the ribbon cable being shown with the blue strip up facing me or away?


---
**Allabout kidzinflatables** *May 04, 2015 20:13*

Does anyone have a picture of there own reprap upgrade they have done?


---
**Rado Racek** *May 20, 2015 18:46*

hi, here is mine [https://www.dropbox.com/s/t865fkxdf42wpit/DSC_0109.JPG?dl=0](https://www.dropbox.com/s/t865fkxdf42wpit/DSC_0109.JPG?dl=0)


---
*Imported from [Google+](https://plus.google.com/109401462473121480211/posts/15nD1BeXjw6) &mdash; content and formatting may not be reliable*
