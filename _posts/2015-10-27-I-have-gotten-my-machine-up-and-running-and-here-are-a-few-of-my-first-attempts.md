---
layout: post
title: "I have gotten my machine up and running and here are a few of my first attempts"
date: October 27, 2015 22:56
category: "Object produced with laser"
author: "Alan Platner"
---
I have gotten my machine up and running and here are a few of my first attempts. I am waiting on a new lens to add the air assist so I have just been cutting paper to work on my workflow and the software. These are a few of my early successes. 





**"Alan Platner"**

---
---
**Todd Miller** *October 27, 2015 23:20*

Your work looks great !  I wish I was just cutting paper ;-)  I tried to cut paper last night

but I'm not vented yet and still have to line up the mirrors.


---
**Alan Platner** *October 28, 2015 02:08*

I am in Maine, wrestled for Bapst once upon a time myself. The mirrors took me a bit to figure out. I would like to say I had a system but it was more of a guess and repeat process. I used painters tape so I could quickly cover a mirror and test fire and then try again.


---
**David Cook** *October 28, 2015 02:43*

Very cool Alan. Thanks for sharing. My machine is close. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 28, 2015 12:49*

**+Allready Gone** Have to agree with that. Originally took me hours to get mine aligned, then the other day I managed to vibrate it all loose. That took about 20-30mins to realign since I now know what I'm doing.

**+Alan Platner** The results look great and it's interesting to see how thin a line weight you can cut.


---
**I Laser** *October 29, 2015 05:38*

Nice. What font are you using for the Alan / Becca cut outs?


---
**Alan Platner** *October 29, 2015 18:14*

Those were done with some script font that was in LaserDraw, I just had to adjust the kerning to make sure all the letters intersected so it would come out as one piece.


---
**I Laser** *November 04, 2015 02:53*

Thanks for the reply, the fonts available in LaserDraw would be fonts installed on your system. If you could check the actual font name that would be great.


---
*Imported from [Google+](https://plus.google.com/100571861937033974608/posts/MVWfjrCJeMx) &mdash; content and formatting may not be reliable*
