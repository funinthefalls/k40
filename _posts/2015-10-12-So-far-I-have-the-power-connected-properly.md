---
layout: post
title: "So far I have the power connected properly"
date: October 12, 2015 20:15
category: "Smoothieboard Modification"
author: "David Cook"
---
So far I have the power connected properly. Moving to the laser control next. 

![images/22a0fcf55d4954ac49772049c0cf8066.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/22a0fcf55d4954ac49772049c0cf8066.jpeg)



**"David Cook"**

---
---
**Sean Cherven** *October 12, 2015 20:49*

What control board is that?


---
**David Cook** *October 12, 2015 20:51*

**+Sean Cherven** its an Azteeg X5 32-bit. It runs smoothieware code and will work with VisiCut 


---
**Stephane Buisson** *October 12, 2015 21:41*

David,

for the easy bit, XY steppers is very quick and easy, as the end stops (home well for me). in config invert X as surface area is in landscape mode and origin in top left corner.

I am on the laser pwm now, it fire all the time, need to find out the right setting, ...

More next time.


---
**Stephane Buisson** *October 12, 2015 22:00*

just found that [http://smoothieware.org/forum/t-1333802](http://smoothieware.org/forum/t-1333802)

see post arthurwolf 23 Aug 2015, 20:42

will try tomorow


---
**David Cook** *October 12, 2015 23:30*

Crap,  the power supply just crapped out lol.  I got both axis to home properly then no more 24V


---
**Ashley M. Kirchner [Norym]** *October 12, 2015 23:44*

Oopsies. Guess it's time for a new one.


---
**Ashley M. Kirchner [Norym]** *October 13, 2015 00:05*

Actually wait ... before you get the new one, or before you install it, or before you rip the old one out ... make sure you find the fault first. Otherwise the chances of you frying the new one still exists.


---
**David Cook** *October 13, 2015 19:48*

Alright,   I figured out what killed the power supply...... 

With the X5 control board I set my stepper current to 2Amps each  ( like I did on the delta 3d printer in the past)  well after looking up the replacement power supply on ebay and seeing the specs, I see the 24 volt output is only rated at 1AMP !!!  lol so I totally killed it  when it went to home both axis at 2A each lol.



The crazy irony is that I am an Engineer at XP Power.  a quite large power supply manufacturer !  lol but we do not make laser supplies  lol.


---
**Ashley M. Kirchner [Norym]** *October 13, 2015 20:16*

That's why I don't trust those Chinese supplies. Sometimes even if they say 2A, it could be across multiple ports, effectively giving you less than what you'd expect. It's worth trying to find the correct specs or replace it all together. 


---
**David Cook** *October 13, 2015 20:22*

Now I figure the stepper motors are only allocated < 1/2 AMP each


---
**David Cook** *October 14, 2015 19:34*

This is the power supply I just ordered,  it looks exactly like the one that was in my machine   [http://www.ebay.com/itm/231550508351?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/231550508351?_trksid=p2060353.m1438.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/MNfq8sQ7eVL) &mdash; content and formatting may not be reliable*
