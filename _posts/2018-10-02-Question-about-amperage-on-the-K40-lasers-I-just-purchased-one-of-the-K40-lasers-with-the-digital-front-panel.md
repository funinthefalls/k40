---
layout: post
title: "Question about amperage on the K40 lasers I just purchased one of the K40 lasers with the digital front panel"
date: October 02, 2018 04:09
category: "Discussion"
author: "Brian McClure"
---
Question about amperage on the K40 lasers



I just purchased one of the K40 lasers with the digital front panel.

It seems to be working well but I wanted to add a ammeter so I would know exactly what the current was not some almost meaningless percentage on the digital panel.



I added the mA meter and when I tested the laser,  15% was reading around 3mA and 99% was giving me just around 9.5mA.



I assumed the analog meter was bad since I have read many postings talking about 40W machines going up to 15-18mA



I hooked up my Fluke digital meter and got pretty much the same readings.



It doesn't have any problems cutting 3mm acrylic when set at 30% (didn't check the amperage) and I've used it for several hours engraving on various wood crafts.



Any thoughts on what may be going on?





Edit:  I forgot to mention that I changed out the controller and replaced it with a Cohesion3D Mini





**"Brian McClure"**

---
---
**Stephane Buisson** *October 02, 2018 07:56*

to new reader, buy directly the model with knob and analog meter.

those digital under their geeky look don't bring anything extra, quite the opposite.


---
**Anthony Bolgar** *October 02, 2018 10:45*

The C3D board is setup as a default to limit the current to 80% of what your panel is set to, so if the panel is set to 10mA, the C3D then limits it to 8mA. This would account for some, but not all of the lower current you are seeing.


---
**Ray Kholodovsky (Cohesion3D)** *October 02, 2018 12:51*

[plus.google.com - Ok so just got my new laser ordered the cohesion board for it. It all looks p...](https://plus.google.com/109920268070321490124/posts/dXdk96yFwqH)


---
*Imported from [Google+](https://plus.google.com/+BrianMcClure01/posts/V3qttPYWt2k) &mdash; content and formatting may not be reliable*
