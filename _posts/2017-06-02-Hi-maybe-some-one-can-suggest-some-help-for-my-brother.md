---
layout: post
title: "Hi maybe some one can suggest some help for my brother ."
date: June 02, 2017 22:31
category: "Software"
author: "Don Recardo"
---
Hi maybe some one can suggest some help for my brother .

We both have K40 lasers  and use corel laser

In corel if I draw a circle and a square and select both then move to the 

cutting page . it shows me both the circle and the square and lets me choose where they will cut on the material

If I draw both a cirtcle and a square but I just choose to select just one , either the circle or the square then on the cutting page just the selected one shows and just the selected one gets cut .

Now on my brothers machine if he tries the same thing , no matter if he selects one , both or even neither of the circle and square , on the cuttiung page both items show up and both items cut.

What could be the reason?





**"Don Recardo"**

---
---
**Mark Brown** *June 02, 2017 23:08*

I think I found it.  Click the green button in the CorelLaser control panel.  That opens CorelLaser settings (different than general laser settings).  Then Engraving Area.





![images/594c1aa46e663d9eb9f7f6481b530d33.png](https://gitlab.com/funinthefalls/k40/raw/master/images/594c1aa46e663d9eb9f7f6481b530d33.png)


---
**Ned Hill** *June 03, 2017 13:28*

Yes, you want to have "only selected".  Glad you figured it out.




---
**Don Recardo** *June 04, 2017 12:15*

Oh that was great thanks it worked a treat.  Thanks  for the help  both of you


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/M3N8Sg3WYgs) &mdash; content and formatting may not be reliable*
