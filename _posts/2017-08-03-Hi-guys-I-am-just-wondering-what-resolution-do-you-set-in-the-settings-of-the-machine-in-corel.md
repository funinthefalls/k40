---
layout: post
title: "Hi guys, I am just wondering what resolution do you set in the settings of the machine in corel?"
date: August 03, 2017 21:14
category: "Hardware and Laser settings"
author: "stephen graham"
---
Hi guys,

I am just wondering what resolution do you set in the settings of the machine in corel? Mine seems to be off. I set it to 1000dpi. Is this correct? Its when you hit properties in corel laser. I have tried lowering to say 300, if the image is 300 but the machine just reduces the size of the area when engraving. Just trying to set this thing up correctly. Any help greatly appreciated. Thanks





**"stephen graham"**

---
---
**Scorch Works** *August 03, 2017 22:44*

It should be set to 1000 dpi


---
*Imported from [Google+](https://plus.google.com/115813499996300207488/posts/PW1fhRb8N1X) &mdash; content and formatting may not be reliable*
