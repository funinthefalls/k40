---
layout: post
title: "So today my k40 with the M2 board suddenly started creeping very very slowly across empty space to reach the next piece to cut"
date: July 21, 2015 02:52
category: "Discussion"
author: "chad too (dns)"
---
So today my k40 with the M2 board suddenly started creeping very very slowly across empty space to reach the next piece to cut. It never did this before and no settings are changed. I verified correct board is set in settings also.



I am using CorelDraw x7 with the CorelLaser plugin. Anyone know what's going on? Maybe my board is going out?





**"chad too (dns)"**

---
---
**george ellison** *July 21, 2015 07:45*

I had this problem, it could be the stepper motors going crazy like they often do. try selecting all your parts that you are cutting and ungroup them, this solved the issue. 


---
**chad too (dns)** *July 21, 2015 07:51*

Ok I'll try that tomorrow when I cut some more acrylic. Thanks I'll post back with results.


---
**chad too (dns)** *July 22, 2015 16:24*

Ok so I ungrouped everything in the cut file and it still does it. It still cuts and engraves at the proper speed, it just creeps slowly over empty space. I cannot figure out what made it suddenly start doing this.


---
**chad too (dns)** *July 22, 2015 16:26*

It's also making two passes (one way and then the other) on single line cuts. I have a design set up that simply cuts the sheet ends off to fit my honeycomb. It was cutting the ends in a single pass and now it goes over them twice. I have the line sizes set to hairline and nothing has changed on that file either. Can't figure that one out either!


---
**george ellison** *July 23, 2015 08:28*

Hmm interesting. It would possibly be the program you are using to draw them. Are you importing them from an external program into Moshidraw as a dxf?


---
**chad too (dns)** *July 23, 2015 08:35*

I'm using Coreldraw x7. The files are created in inkscape and  exported as postscript. I then import them into Coreldraw. 



For cut settings I'm using normal Windows metafile. Engraving set to bitmap. Both set to entire page.


---
**chad too (dns)** *July 23, 2015 08:37*

But the thing is, I'm running files I haven't changed at all. Totally different machine behavior with no changes in files or software settings.


---
**Clifford Livesey** *August 09, 2015 18:57*

I have the same problem only on cutting and only going to the starting point and only on the x axis 


---
**chad too (dns)** *August 09, 2015 21:11*

Clifford yes exactly!! 


---
*Imported from [Google+](https://plus.google.com/108472782099926789468/posts/5pT3kiQ2FQz) &mdash; content and formatting may not be reliable*
