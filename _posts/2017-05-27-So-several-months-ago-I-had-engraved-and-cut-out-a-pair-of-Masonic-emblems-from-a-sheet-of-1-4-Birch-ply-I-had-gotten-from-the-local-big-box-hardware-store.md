---
layout: post
title: "So several months ago I had engraved and cut out a pair of Masonic emblems from a sheet of 1/4\" Birch ply I had gotten from the local big box hardware store"
date: May 27, 2017 01:23
category: "Discussion"
author: "Ned Hill"
---
So several months ago I had engraved and cut out a pair of Masonic emblems from a sheet of 1/4" Birch ply I had gotten from the local big box hardware store.  The coloration of engraved area came out kind of blotchy and uneven.   Really didn't like it so I just set them to the side leaving the masking on.  Since the masking was still on I decided to try the polyshade spray technique that I had shared earlier, from an OP on facebook, to darken and even out the engraved area.  It worked like a charm.  I used two moderate coats as it doesn't really build up that fast compare to something like spray paint.    #K40ShopTips



![images/e99fcd1b72b55e578eab3c5e7f3e0664.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e99fcd1b72b55e578eab3c5e7f3e0664.jpeg)
![images/a8afc06f462df2a2528a9b217df5bf89.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8afc06f462df2a2528a9b217df5bf89.jpeg)
![images/019e014ec06ea941683a7526b6c79b31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/019e014ec06ea941683a7526b6c79b31.jpeg)

**"Ned Hill"**

---
---
**Anthony Bolgar** *May 27, 2017 01:40*

That really improved the finished product. Thanks for sharing this method with us.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2017 04:54*

That's a really nice touch. Looks really good.


---
**Alex Krause** *May 27, 2017 12:18*

I'm actually surprised there is no color bleeding into the grain of the non engraved portions 


---
**Nigel Conroy** *May 30, 2017 12:46*

**+Ned Hill** What are you using to mask large areas? or how are you avoiding overlap which effects the engrave?

I know you mentioned 3M delicate previously.


---
**Ned Hill** *May 30, 2017 14:37*

**+Nigel Conroy** for large areas I'm now using RTape Conform 4075RLA ([http://www.uscutter.com/RTape-ApliTape-4075-RLA-High-Tack-Application-Transfer-Tape](http://www.uscutter.com/RTape-ApliTape-4075-RLA-High-Tack-Application-Transfer-Tape)).  In the past I used the 3M tape for large areas as well.  The pieces I showed above are actually masked with the 3M tape.  You just have to be careful in butting the pieces together.

[uscutter.com - uscutter.com](http://www.uscutter.com/RTape-ApliTape-4078-RLA-Medium-Tack-Application-Transfer-Tape)


---
**Jeff Johnson** *May 30, 2017 15:02*

Have you tried that on large areas of birch? I've given up on it because it stains so inconsistently but I've only tried rub on stains.




---
**Ned Hill** *May 30, 2017 15:18*

**+Jeff Johnson**  I haven't tried this spray on large area's yet.  Personally I use Minwax brush on stains with birch without any problems.  Typically use 2 coats.  You could also try using a wood conditioner if you're having trouble with uneven staining.  I typically have to do this with pine.


---
**bbowley2** *May 31, 2017 17:09*

Looks great. I'll give it a try.  I've been engraving the S&C on acrylic.

PV 29 Silver Trowel Phoenix, AZ


---
**Ned Hill** *June 01, 2017 15:04*

**+bbowley2** Nice to meet you Brother.  Wilmington Lodge 319 Wilmington, NC


---
**Travis Sawyer** *June 06, 2017 16:16*

I'm still getting my K40 up and running, but did the S&C on our "Pasta Paddles" for the lodge with my 2.5w engraver.  Also just got done doing some shot glasses for table lodge with the 2.5w.  I can't wait to get my K40 up and running (and build a rotation cradle/table). (Berkshire Lodge A.F. & A.M, Adams MA)




---
**Alex Krause** *June 07, 2017 05:53*

McPherson  Lodge #172 A.F.&A.M. , McPherson KS :P


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/BeWKzCdk1Qf) &mdash; content and formatting may not be reliable*
