---
layout: post
title: "I just bought a k40 laser from eBay"
date: March 02, 2016 19:08
category: "Discussion"
author: "Trinh Ta"
---
I just bought a k40 laser from eBay. I saw everyone in this group posted about  upgrade to arduino and RAMPs. Can you you tell me, what is the purpose to upgrade to arduino and RAMPs? 





**"Trinh Ta"**

---
---
**3D Laser** *March 02, 2016 19:25*

Better software


---
**Jim Hatch** *March 02, 2016 19:39*

**+Corey Budwine** And the software is generally documented in English as a primary language and usually has some sort of support available (even with the open source stuff). Not to mention the software is not a pirated copy :-)


---
**Ben Walker** *March 02, 2016 20:39*

**+Jim Hatch** is it acceptable to use the p word?   I had my suspicions but who am I to say?   I installed it onto a spare computer and accidentally registered it thinking it was legit.   No warning about piracy.  It reported that it was no longer supported and tried to get me to upgrade.   For a fee of course.   I'm concerned the upgrade won't talk to the machine anymore so I got out of there


---
**Jim Hatch** *March 02, 2016 21:27*

**+Ben Walker**​​ Well should I use "undocumented, outdated, malware infected, delivered on unmarked CD media with license key in a readme text file without a license document and unsupported by the original software development company"? :-) 



I will admit "pirated" was an assumption of mine based strictly on circumstantial evidence. I don't <b>know</b> it's not legit because I didn't actually see them copy it nor do I know they don't have some sort of underlying OEM license from Corel that allows them to copy & ship as part of their machine sale. I just <b>suspect</b> it's not legit.



I do find it interesting that LaserDRW which in my case seems to have some Chinese heritage (or at least a translation) requires a security dongle but the giant U.S. software provider's application gets a license key in a file on the CD. Seems ironic.



Of course sometimes things that smell, look and quack like ducks are actually ducks :-D﻿


---
**Ben Walker** *March 02, 2016 21:28*

**+Jim Hatch** absolutely.   I had tongue in cheek as I typed that.   


---
**Thor Johnson** *March 02, 2016 21:32*

And I'll say that if you have RAMPs in place, you can print from a SD card, so you can keep your computer away from the workshop and its dust and fumes.  OTOH, LaserDraw, et al, are easier to do custom setups (eg, I want to engrave a belt buckle, but I haven't built a jig to consistently hold it in a known location, so I can use LaserDraw to move the pointer so the red-dot indicates the top-left and punch the "Starting" button.

-- But that can (and I'm sure it will) be added to the RAMPS/Smoothie stack.  The Moshi stack is completely locked down.


---
**I Laser** *March 03, 2016 23:48*

**+Jim Hatch** Completely agree it's ironic that they've secured the plugin with a dongle, yet the prerequisite software comes on a CD-R with the license key in text file and instructions not to register it. Pretty sure it's pirated. ;)


---
**Scott Marshall** *March 04, 2016 15:12*

It's the Chinese manufacturers that are putting us in the position to use what was delivered as"legitimate", but is by all accounts at very least outdated and not supported by Corel. I have not found any comment on the subject by Corel, they may have made a deal on an old version, letting it go "semi-public domain" , but there is no documentation either way.

Te serial number used to activate it seems to be the same one for all users. Whatever that means.



Anyways, you're faced with using what they sent or nearly doubling the price of the machine to "get legit"

 

People using illegal software is one of my pet peeves (as is software manufacturers gouging users)

I don't condone either, and both are evidently in play here. 

I was sold a copy of software represented as legal with a piece of new gear by a commercial manufacturer, so I have to go with it, despite the whole affair smelling funny.



I can live with all the faults of the K40, you get what you pay for.

The software mess is one that is nearly a deal breaker. 

(thanks I laser for the help getting mine running), without you guys helping, I'd probably have switched boards by now. Probably will soon enough, even without the legal/support concerns, the included software is pretty crappy and is really only acceptable (in my opinion) as a transfer step. I still have to draw in DraftSight, PDF out and wash it through CorelLaser. It still requires backing through the entire workflow to change 1 dimension or letter.



Scott




---
*Imported from [Google+](https://plus.google.com/114886757826264619236/posts/KtksktLquS8) &mdash; content and formatting may not be reliable*
