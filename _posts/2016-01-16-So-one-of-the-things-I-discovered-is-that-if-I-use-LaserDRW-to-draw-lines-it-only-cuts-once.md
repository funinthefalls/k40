---
layout: post
title: "So one of the things I discovered is that if I use LaserDRW to draw lines, it only cuts once"
date: January 16, 2016 04:53
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
So one of the things I discovered is that if I use LaserDRW to draw lines, it only cuts once. So while I can't draw things like living hinges in Illustrator, export as an image, and have it cut once, I <i>can</i> do the lines in LaserDRW and it would work just fine. Now I just need to find a work flow that actually produces a good result. On to exploring.





**"Ashley M. Kirchner [Norym]"**

---
---
**Ashley M. Kirchner [Norym]** *January 16, 2016 20:18*

Huh, I will try that then. Thanks for the heads up. That would make my workflow a lot easier.


---
**Ashley M. Kirchner [Norym]** *January 19, 2016 00:57*

That didn't work ... was worth a try though.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/PwCV8f5doRY) &mdash; content and formatting may not be reliable*
