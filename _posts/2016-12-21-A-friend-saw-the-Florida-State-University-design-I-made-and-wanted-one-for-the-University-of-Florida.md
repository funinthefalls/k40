---
layout: post
title: "A friend saw the Florida State University design I made and wanted one for the University of Florida"
date: December 21, 2016 17:01
category: "Object produced with laser"
author: "Jeff Johnson"
---
A friend saw the Florida State University design I made and wanted one for the University of Florida. This is what I came up with. This is done on cheap hardwood plywood.

![images/4d46155751429a585885ec0f7c1236e5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/4d46155751429a585885ec0f7c1236e5.jpeg)



**"Jeff Johnson"**

---
---
**James G.** *December 21, 2016 18:01*

That looks great! 


---
**Bob Damato** *December 21, 2016 23:15*

That wasnt done on a stock system was it... <b>sigh</b>


---
**Jeff Johnson** *December 21, 2016 23:57*

I've added air assist, a honeycomb bed that's adjustable using wooden standoffs, and lighting. Otherwise it's stock. This is a very capable machine if you figure out the settings that work best and get creative with your designs.


---
**Jeff Johnson** *December 21, 2016 23:58*

This is just simple engraving and cutting. I stain the gator head and border and then used a black marker to darken the engraved areas of the Gator head.


---
**Don Kleinschnitz Jr.** *December 22, 2016 00:55*

Sweet!


---
**Bob Damato** *December 22, 2016 01:58*

It looks like the background alternates between cut and engrave, is that the case?


---
**Jeff Johnson** *December 22, 2016 04:51*

It's all engrave. I have alternating black on white and then white on black Square logos


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 22, 2016 07:31*

Looks great. Was it a time consuming piece? I'd imagine so.


---
**Ned Hill** *December 22, 2016 14:18*

Lol, **+Yuusuf Sallahuddin** that was one of my first thoughts as well ;)  Still a very nicely done piece.


---
**Jeff Johnson** *December 22, 2016 14:32*

**+Yuusuf Sallahuddin** yes, it was very time consuming but it's a gift for a friend so I don't mind.


---
*Imported from [Google+](https://plus.google.com/105896334581840652176/posts/jM9k3i3Bqsn) &mdash; content and formatting may not be reliable*
