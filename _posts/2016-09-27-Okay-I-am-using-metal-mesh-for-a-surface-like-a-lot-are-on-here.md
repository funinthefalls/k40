---
layout: post
title: "Okay, I am using metal mesh for a surface like a lot are on here"
date: September 27, 2016 19:32
category: "Discussion"
author: "J.R. Sharp"
---
Okay, I am using metal mesh for a surface like a lot are on here. Problem I am facings is when the laser penetrates, it is burning the metal somehow and creating a sticky black buildup that is getting on the back of the work. I have scrubbed the platform down and still no luck..



Thoughts?





**"J.R. Sharp"**

---
---
**Ariel Yahni (UniKpty)** *September 27, 2016 20:08*

Put some metal washer bellow the material to create a small difference to reduce reflection 


---
**Phillip Conroy** *September 27, 2016 21:37*

I only cut 3m mdf and had sa e problem until i swaped metal bed for 10mmlong magnefs with 65mm screws ontop made 12 of these that hold up my work,now underside of cut is unmarked [pin  cushion bed00,also added a removeable metal tray that i take out and runundef hot water to cleanoncea week-


---
**J.R. Sharp** *September 28, 2016 00:53*

Interesting. I would do that but some of my designs are engraved and cut, so I can't let it fall through, etc. 


---
**Scott Marshall** *September 28, 2016 03:09*

I keep a nail board around for such occasions.



 Just take a piece of any thin ply (FLAT) and predrill for 17ga x 1" wire brads (or whatever you have or can get easily) - drill so they fit tight where you have to drive them in, but not so tight as to stress or split the wood. Put them in in about a 1" grid for general purpose use, or as you need for your products.



The 1" gap defocuses the laser where it hardly scorches the plywood, and nothing of consequence bounces back, protecting the underside of your work. If your fan is working good, they offer the additional benefit of good airflow around the work, and that helps keep smoke deposits from forming underneath. As always, watch for fires underneath where they're not obvious right away.



Scott


---
**Jeff Johnson** *September 30, 2016 03:55*

This is how I handheld it.  The honeycomb does get a little glue on it but almost no noticeable marking due to reflected laser. [https://plus.google.com/105896334581840652176/posts/Kj4AKr11QVH](https://plus.google.com/105896334581840652176/posts/Kj4AKr11QVH)




---
*Imported from [Google+](https://plus.google.com/116586822526943304995/posts/45N4hm6wrcg) &mdash; content and formatting may not be reliable*
