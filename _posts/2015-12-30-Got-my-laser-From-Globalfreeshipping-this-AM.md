---
layout: post
title: "Got my laser From Globalfreeshipping this AM"
date: December 30, 2015 02:02
category: "Discussion"
author: "Scott Marshall"
---
Got my laser From Globalfreeshipping this AM. 



The fan and pump were tossed in the cutting cavity loose and battered the cutting head so hard it snapped off the hexagonal standoffs. Plate is bent. Optics condition doubtful. I sent them an ebay message asking they send a carriage/head assy and offered to fix it rather than ship it back. Waiting to hear back.



I always seem to be the one that gets the dud. 



On the upside the laser looks intact despite being full of water and riding in a FEDEX truck locally 2 days (another case of "why me") and sub freezing temps here in sunny Elbridge NY (west of Syracuse).



Tracking on my lens set and air assist show them in Flushing NY, so they'll beat the parts from Global I need to run it.



Happy New Year, (almost)﻿





**"Scott Marshall"**

---
---
**Jim Hatch** *December 30, 2015 02:20*

Mine came with the stuff loose but didn't break anything. It was badly grounded though so check on that when you fire it up. 


---
**Scott Marshall** *December 30, 2015 06:08*

Thanks, Looks like it's going to need a real close going over. I expected that, but geez, a couple of pieces of foam or tape wouldn't kill em...


---
**Jim Hatch** *December 30, 2015 13:37*

I think it's a crapshoot with these. I was surprised by how well mine was packed. It seemed like overkill - although considering it was marked "this end up" on the box and was left in my driveway with a different end up I guess more packing was better than less. In the laser tube compartment there were 3 blocks of styrofoam and 6 4x4 squares of pink squishy foam packed in around the tube. I had water in it but not a lot that I recall - I thought that was good because it meant they had likely tested it but didn't even consider the freezing potential as it was being driven around on a truck. Cross your fingers on that one. I have mine in the garage so I added some antifreeze to my cooling bucket (25%). 


---
**Scott Marshall** *December 30, 2015 15:45*

Jim, it sounds like yours was packed better than mine, with the exhaust fan and water pump thrown in the cutting compartment loose, I'd have been surprised if it didn't damage something. To add to it, Fedex bounced it around locally while putting it on and off the truck 3 times locally, because they had a "delivery exception" which I think means they realized one man couldn't handle it. When they delivered it, it was left standing on end, and there were no markings on the box, not even "fragile" or "do not Freeze".



In addition to the poor packing they left a substantial amount of water in the laser tube, a possibly fatal move if shipping to temperate climes.



GFS responded with the usual delaying platitudes...



Here is the GFS correspondence so far:



Me:

I received my laser today, the packaging was intact and undamaged.



However, inside of the cutting cavity I found packed the exhaust fan and water pump, not secured in any way. These items slid around in shipping and battered the cutting head to destruction, bending the aluminum plate and breaking off the hexagonal standoffs which mount it to the carriage. I'm not sure if the optics are damaged or not, as I elected not to disassemble it until I had your permission. I would expect judging from it's appearance, the entire carriage assembly will need replacement. I can send photos if you wish.



If you can send me a replacement cutting head/ carriage assembly, I would appreciate it, as I'd rather repair it than ship the entire unit back. I'd imagine you agree.



Please advise me as to how you would like to handle this.



GFS:

We want to apologize for the disappointment and inconvenience our product has caused.

Could you please tell us more details and send us some pictures or videos to help us understand the issue?

You can upload the video on youtube and send us a link.

We will try to resolve the problem within 24 hours after your reply.

Thank you for your understanding.



I'm sending photos. Nothing moving here, so I'm NOT doing a video production for their benefit.



Film @ 11 - so to speak....






---
**Jim Hatch** *December 30, 2015 16:48*

The optics in the main cavity are pretty simple - a couple of mirrors and a lens. They don't have any play in mounting (although the mounting moves) so they should be fine unless crushed or poked by something. Thy're likely to be out of alignment though which is no big deal. Did you check the carriage assembly for squareness? If it isn't too far out it wasn't likely damaged either (unsquare carriages are another typical flaw in general and just something you fix with these). Is there any way you can fab replacements for the broken mounts? It may be way faster to do that than wait on the seller. Then when they finally come through you have an extra (or some $ - they often offer $ rather than replacement parts). I'd have to look at mine but I don't recall the travelling head mounts were very complicated parts - something a bit of aluminum stock from Home Depot and time with a Dremel I could replicate. (I was looking at modifying it so I could move the tube vertically to allow me to set the focal point when I opened up the interior to give me more depth for materials).


---
**Scott Marshall** *December 30, 2015 17:00*

I've not taken it apart yet, but I'm guessing the lens and mirror are OK and I've got an air assist head and spare Moly mirrors/lens coming anyway. It's out of alignment for sure, the mounting plate is bent down about 15 degrees, and the standoffs are broken (at least 2) and the whole thing is loose, just hanging by the remaining bent standoffs. the XY rails look ok, and the carriage moves smoothly. I'll throw a dial indicator on in a while and see if they're Ok. I'm going out to the shop to take the pictures shortly, I'll post them here when I send them to GFS.


---
**Jim Hatch** *December 30, 2015 17:39*

Where did you get your air-assist head? I was going to print one up on the Makerbot this weekend but if someone else has done it already I might just go the easy way and not diddle around trying to get just the right fit. At 40W it's a huge benefit to have air or you need to clean the lens fairly frequently to keep the power that reaches the work piece up.


---
**Scott Marshall** *December 30, 2015 20:59*

Saite Cutter on Ebay. I got it on sale, and it's gone up a bit since, should be here pretty soon, tracking shows it in Flushing NY - About 300miles out.



[http://www.ebay.com/itm/252198712590?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/252198712590?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
*Imported from [Google+](https://plus.google.com/116005042509875749334/posts/ZaEh62gZtqF) &mdash; content and formatting may not be reliable*
