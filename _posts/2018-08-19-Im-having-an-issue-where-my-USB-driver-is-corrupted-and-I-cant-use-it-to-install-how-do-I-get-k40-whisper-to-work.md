---
layout: post
title: "I'm having an issue where my USB driver is corrupted and I cant use it to install how do I get k40 whisper to work?"
date: August 19, 2018 04:49
category: "Original software and hardware issues"
author: "james buhr"
---
I'm having an issue where my USB driver is corrupted and I cant use it to install how do I get k40 whisper to work?





**"james buhr"**

---
---
**Scorch Works** *August 19, 2018 17:38*

You could try to uninstall the "corrupted" driver and start over.


---
*Imported from [Google+](https://plus.google.com/118025258350703248321/posts/i5psBdreznu) &mdash; content and formatting may not be reliable*
