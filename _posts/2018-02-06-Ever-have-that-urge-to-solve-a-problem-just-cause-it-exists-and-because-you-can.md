---
layout: post
title: "Ever have that urge to solve a problem just cause it exists and because you can ?"
date: February 06, 2018 13:39
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Ever have that urge to solve a problem just <i>cause it exists</i> and because <i>you can</i>?



While working on my K40 I thought the fan was running and it was not. Its pretty quiet and down below the machine. Turns out I had a problem with an AC connector. 



I set off to design a sensor and this is what the results were .....

.... ya it got out of control!








**Video content missing for image https://lh3.googleusercontent.com/-Wi5TQ6AQWTg/WnmwJVKL5kI/AAAAAAAAzCw/rmeRMQUnyz4351bw077OrlCUc1VuUAAcwCJoC/s0/20180205_175153.mp4**
![images/43f9cbf53960e4856ca04f1a536fd688.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43f9cbf53960e4856ca04f1a536fd688.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-ppwOkD_Fvac/WnmwJasoo1I/AAAAAAAAzCw/EGlXShi_bSgY5UnnwgHyeyAr9g7YTH-DACJoC/s0/20180205_175753.mp4**
![images/8803f8c47a429080dfac803e0d21e8f2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8803f8c47a429080dfac803e0d21e8f2.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-LFYUfmiXuZk/WnmwJYD2rxI/AAAAAAAAzCw/he0yurV7Lvkm5IBWdVIY7CmZFnl_rKyzgCJoC/s0/20180205_180312.mp4**
![images/ceb640cdf3d5b42007020512d1400b94.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ceb640cdf3d5b42007020512d1400b94.jpeg)
![images/e2eaad13d9cdf2460e791c814d17a208.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e2eaad13d9cdf2460e791c814d17a208.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Andy Shilling** *February 06, 2018 14:00*

O my, did you actually do any planning or was it all on the fly lol.


---
**Don Kleinschnitz Jr.** *February 06, 2018 14:08*

mental planning then just hacking ....


---
**Andy Shilling** *February 06, 2018 14:11*

But you know what, it works. That's all that matters and I'm sure you will get the urge at some point to revisit and refine it. Good job I say. 👌


---
**Don Kleinschnitz Jr.** *February 06, 2018 14:24*

**+Andy Shilling** this one is done, spent to much time on it ......


---
**Steve Clark** *February 06, 2018 17:04*

**+Don Kleinschnitz**  Oh Don! We need to find you some better projects for your time and talents. I’d need to get that Peltier cooling schematic posted fast! - grin 



[ebay.com - Details about Atwood 36680 Hydro Flame Furnace Sail Switch 37716](https://www.ebay.com/itm/Atwood-36680-Hydro-Flame-Furnace-Sail-Switch-37716/231563095496?epid=1022115398&hash=item35ea3c6dc8:g:MEAAAOSwHnFVzte6&vxp=mtr)


---
**Phillip Conroy** *February 06, 2018 19:19*

Love it,I am working on air fow switch /alarm and fire alarm for My sh g350,WA set back a few weeks with other faults hope to post soon


---
**Don Kleinschnitz Jr.** *February 06, 2018 19:51*

**+Steve Clark** no kidding.... Btw wanted to use a microswitch but it took to much force and to big a sail. So went to hall effect. Side benefit is you can see the amount of air by the lever position lol. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/TZJSGZ6yF69) &mdash; content and formatting may not be reliable*
