---
layout: post
title: "Thinking out loud and sharing some info and musing on PPI and laser response : I have been thinking about PPI for some time due to its purported power efficiency for cutting"
date: November 15, 2016 14:40
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Thinking out loud and sharing some info and musing on PPI and laser response :



I have been thinking about PPI for some time due to its purported power efficiency for cutting. The K40 @30-40 watts has plenty of power for engraving but marginal power for cutting IMO. 



The PPI method purports to extend the tubes life. Then again some think it will shorten it.



I want to play with PPI [like i need another project]:.

...........................

How to implement with a smoothie:

I  imagine an Arduino placed in series with the smoothie and the Laser Powers 'L' control signal? Maybe some hardware help on a shield? Or maybe all hardware?

I would  simply "AND" the PPI pulse with the PWM signal from smoothie going to 'L' ... I think.



Simple approach:

I wonder why (haven't thought about this enough) you couldn't just "AND" a fixed 3ms pulse stream with the 'L' signal to create a cheap modulated approach to PPI. You would turn this on and off for vector cutting. I assume this will not work as you need to know the feed rate?

...........................

Whoops:

On a related note. For some time I have been wondering what the lasers dynamic response is. I have imagined it to be in ms not us as the gas has to ionize. I have it on my list of tests.

From a below reference it states that the laser will not respond fully to pulses less than 2-3ms ?? This value is useful to know in that PWM frequencies that allow a pulse of less than 3ms won't be outputting the power we expect. 



Example: of the PWM period is 20us then a 10% DF setting will result in a pulse of 2us. If I recall the typical period setting in the smoothie configuration is 20us?? 



.......Should the period be set to 20,000us (20ms)? 

That is 50hz......?

Am I missing something? 

How am I off by 3 orders of magnitude .........????

.........................

Links:



[http://www.engraversnetwork.com/support/universal-lasers/laser-how-tos/dpi-vs-ppi-laser/](http://www.engraversnetwork.com/support/universal-lasers/laser-how-tos/dpi-vs-ppi-laser/)



[http://hackaday.com/2014/06/11/better-lasing-with-pulses/](http://hackaday.com/2014/06/11/better-lasing-with-pulses/)



[http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/](http://www.buildlog.net/blog/2011/12/getting-more-power-and-cutting-accuracy-out-of-your-home-built-laser-system/)



[https://spie.org/Documents/Membership/laser_co2_demaria_hennessey.pdf](https://spie.org/Documents/Membership/laser_co2_demaria_hennessey.pdf)



[https://github.com/JoachimF/PPI](https://github.com/JoachimF/PPI)











**"Don Kleinschnitz Jr."**

---
---
**greg greene** *November 15, 2016 15:00*

Why use an Srduino for the 3ms pulse - just set up a 3ms signal and a simple and gate Don, then you get the same result - No?  I suppose it could be cheaper with Arduino?


---
**Don Kleinschnitz Jr.** *November 15, 2016 15:28*

**+greg greene** YES!, that was what I meant in the "Simple Approach" above.


---
**greg greene** *November 15, 2016 15:55*

Kewl !


---
**Don Kleinschnitz Jr.** *November 15, 2016 16:05*

**+greg greene**  the answer may be that when you change speeds the power in the cut changes with a fixed pulse? But in reality are you changing speed that dramatically when vector cutting? You accelerate at the start and then decelerate at the end of a cut.


---
**greg greene** *November 15, 2016 16:10*

Yes, vector cutting is at a set speed, with the possible exception  if the laser takes a finite time to come up to the power level - but that would be very minimal if at all. I would think, the variation in material density would cause a more notable level in cut efficiency than that.


---
**Don Kleinschnitz Jr.** *November 15, 2016 16:12*

**+greg greene** then the simple approach may work :).


---
**greg greene** *November 15, 2016 16:17*

Yeah, it would be a good place to start - simpler is better ! :) and also more easily spread throughout the community


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/QWrzvBh8bc3) &mdash; content and formatting may not be reliable*
