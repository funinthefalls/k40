---
layout: post
title: "I have (finally) started to setup my K40"
date: January 15, 2018 04:23
category: "Original software and hardware issues"
author: "James Rivera"
---
I have (finally) started to setup my K40. The current step is getting the cooling system running. I have silicone tubing coming out of the back of the K40 and I know it needs to go to this pump. My questions are:

1) does it matter which end of the laser is the inlet vs. the outlet? And,

2) how do I connect the default silicone tubing to this pump? As in, what fitting or adapter(s) do I need?

And,

3) any links to the answer to #2 would be greatly appreciated.

Thanks!

-James



![images/e878f77bb0b68817f292b76ef567d7ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e878f77bb0b68817f292b76ef567d7ad.jpeg)
![images/16775555e34cc2aaa2dd3320757b6aaa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16775555e34cc2aaa2dd3320757b6aaa.jpeg)
![images/eb34d2d3ef2c373555efb43849f55321.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb34d2d3ef2c373555efb43849f55321.jpeg)
![images/739e9d0841060f28b47e8805993513ee.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/739e9d0841060f28b47e8805993513ee.jpeg)
![images/9475a5af4947ebdef0de1793dd1776f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9475a5af4947ebdef0de1793dd1776f4.jpeg)
![images/805b217718828d20f717ad55288aaf6e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/805b217718828d20f717ad55288aaf6e.jpeg)

**"James Rivera"**

---
---
**Ned Hill** *January 15, 2018 05:41*

1.) Technically I don't think it matters, but the inlet on mine is labeled going to the end of tube where the laser does not come out.

2.) You can probably go to your local hardware store to get a barbed reducing adapter.  I that's what I did.




---
**James Rivera** *January 15, 2018 06:02*

**+Ned Hill** What size reducing adapter did you get?




---
**Ned Hill** *January 15, 2018 06:09*

I think I got one that was a 3/4" male thread to a 1/4" barb.  Since thread on the outlet of my pump was metric (20mm I think) the fitting was a little small but still thread on.  When screwed down tight it gave a good enough seal.


---
**Ned Hill** *January 15, 2018 06:10*

You can use some teflon tape to make up the difference if yours is the same.


---
**James Rivera** *January 15, 2018 06:20*

**+Ned Hill** Cool--thanks for the help!


---
**James Rivera** *January 15, 2018 06:30*

After googling for the model number and hose adapter, I saw a picture that made me slap my forehead. It came with this. It fits perfectly and the other end fits the hose. Duh. Hooray for zero documentation.![images/f981bfa52005bfc68c529817430f1bc6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f981bfa52005bfc68c529817430f1bc6.jpeg)


---
**James Rivera** *January 15, 2018 06:32*

Now I just need to get some kind of bucket to hold the water. And the cooking system will be more or less usable. Now I just need to 3D print the air assist nozzle!


---
**Ned Hill** *January 15, 2018 06:32*

**+James Rivera** Yep that's it :)


---
**Ned Hill** *January 15, 2018 06:38*

Just get a 5gal bucket with a lid from the hardware store and drill some holes in the top for the tubing and cut a notch for the power cord.  You will want to stock up on some plastic water bottles to keep in the freeze to add to the bucket to keep the temp down for longer work times.  I just use 1/2 liter bottles.


---
**Ned Hill** *January 15, 2018 06:42*

Remember to also change the water periodically.  I try and change mine every 1-2 months.  It's recommended to use distilled water and to avoid additives other than very small amount of bleach (cap full) or algaecide if you really feel you need something.  If the water becomes too conductive it can cause problems.


---
**James Rivera** *January 15, 2018 06:45*

**+Ned Hill** Those are all pretty cheap. I'll probably make a run to the hardware store for it all tomorrow. Thanks again!




---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/Ynoztcn9QwA) &mdash; content and formatting may not be reliable*
