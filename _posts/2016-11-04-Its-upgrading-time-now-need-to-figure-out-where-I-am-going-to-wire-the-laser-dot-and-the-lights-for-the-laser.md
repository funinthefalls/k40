---
layout: post
title: "It's upgrading time now need to figure out where I am going to wire the laser dot and the lights for the laser"
date: November 04, 2016 12:21
category: "Modification"
author: "Rob Turner"
---
It's upgrading time now need to figure out where I am going to wire the laser dot and the lights for the laser 



![images/c78339b66038bb1cfe294e04c09b9873.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c78339b66038bb1cfe294e04c09b9873.jpeg)
![images/7a3bfcc2d33ba355befc0b95695ad75b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7a3bfcc2d33ba355befc0b95695ad75b.jpeg)

**"Rob Turner"**

---
---
**Don Kleinschnitz Jr.** *November 04, 2016 12:50*

Some useful information: 

The LPS capacity

... 5VDC @ 1 amp-

... 24 VDC @ 1 amp-



<s>-------------------</s>

Most of the LED strips run on 12V and that is not provided by the LPS.



I would add a +12VDC supply because I do not think either of the voltages on the LPS would have extra capacity.



You can also just use a 12VDC brick and plug it into a power strip. I did that for quite a while.



<s>----------------------</s>

In case you plan a controller conversion:



For my smoothie conversion I have this power design:



LPS DC ;

5V: 

...Switch LED's-

...AC relay driver-

24VDC: 

...Z lift table stepper-



Add on supply's:

12VDC @5A: 

...Finder LED-

...Cabinet lights-

...Temperature meter/controller-

...Future 12V devices like water pump

 24VDC@5A:

...Smoothie which drives stepper and display.



.....

Lots of choices for cheap supplies on amazon and ebay.




---
**HalfNormal** *November 04, 2016 18:11*

I use old computer power supplies because I have them lying around. 


---
**Don Kleinschnitz Jr.** *November 05, 2016 12:45*

I'm sure you know, but you need to have a constant load (resistor) on the 12vdc on those supplies to get reliable power. 


---
**Kevin Oakes** *November 08, 2016 23:07*

Don the constant load should be on the 5vdc :-)


---
**Don Kleinschnitz Jr.** *November 09, 2016 01:34*

**+kevin oakes** whoa you are right on the 5V MY BAD but is the 12V that will go wacky .... at least on mine.


---
**Kevin Oakes** *November 09, 2016 01:45*

Yes that correct. I normally put a 5v fan on the 5v rail to stabilise the 12v. I make them up for 3d printers as they have a lot of protection in them that I wouldn't rely on getting from these cheap Chinese psu's. I then use 5v 5v load (fan) to cool the printer control board so the load isn't wasted :-)


---
**Kevin Oakes** *November 09, 2016 01:52*

You can check the psu v output is right apparently by testing the voltage of grey and negative. If that reads 5v then the 12v 5v and 3.3v are right. I've never tried it but read about it.


---
*Imported from [Google+](https://plus.google.com/114544098138525257938/posts/1SvoFSpgXCA) &mdash; content and formatting may not be reliable*
