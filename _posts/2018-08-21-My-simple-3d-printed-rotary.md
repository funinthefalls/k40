---
layout: post
title: "My simple 3d printed rotary"
date: August 21, 2018 14:18
category: "Hardware and Laser settings"
author: "Jim Fong"
---
My simple 3d printed rotary. 



[https://www.thingiverse.com/thing:3060428](https://www.thingiverse.com/thing:3060428)



![images/0734a6b836bc6ed6782bc1c3309719b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0734a6b836bc6ed6782bc1c3309719b3.jpeg)
![images/f7b986b5e2e03cdcc8bd6a2d59cf31c2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f7b986b5e2e03cdcc8bd6a2d59cf31c2.jpeg)

**"Jim Fong"**

---
---
**S Th** *August 21, 2018 15:06*

This is Great! I might have to build one. I haven't upgraded my controller yet, so I'm running the stock board (M2 nano I think?) Is there an easy way to implement a third stepper? I was thinking I could just order a replacement Y-axis stepper and swap the plugs whenever I wanted to use the rotary. Is it more complicated than that?


---
**Kelly Burns** *August 27, 2018 04:05*

Looks great Jim.  


---
**Jim Fong** *August 27, 2018 11:56*

**+Kelly Burns** thanks. 



Here’s a low profile version that would work better for a k40



[thingiverse.com - Laser Rotary Lower Profile by Jimf123](https://www.thingiverse.com/thing:3068792)


---
**Kelly Burns** *August 27, 2018 16:32*

My design has been working well for me.  I may play around with your design.  I have a hole in the bottom of mine so I don’t need the low-profile.  Thanks for sharing.  It’s great for the whole community to have options like this.  Especially one that can be built was 3D printer and hardware store parts.  


---
**Kelly Burns** *August 27, 2018 16:42*

**+S Th** it’s not as easy, but you can use a rotary with stock controller.  You can swap the Y-Axis stepper with the rotary one.  Depending on the controller and software, you either adjust your source image (distort) or adjust the STEPS/mm for the y axis.  


---
*Imported from [Google+](https://plus.google.com/114592551347873946745/posts/DYP9Buygsy4) &mdash; content and formatting may not be reliable*
