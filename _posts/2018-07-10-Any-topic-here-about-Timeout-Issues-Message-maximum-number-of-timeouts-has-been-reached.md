---
layout: post
title: "Any topic here about Timeout Issues. Message: maximum number of timeouts has been reached"
date: July 10, 2018 20:23
category: "Software"
author: "M\u00e1rio Santos"
---
Any topic here about Timeout Issues.

Message: maximum number of timeouts has been reached.

I have this problem, more than I wanted, I already tried to reinstall but the problem continue.









**"M\u00e1rio Santos"**

---
---
**Don Kleinschnitz Jr.** *July 10, 2018 21:02*

The community needs to know a lot more about the configuration of your machine and what software you are using!


---
**Mário Santos** *July 11, 2018 10:42*

**+Don Kleinschnitz** Sorry, I use the K40 Whisperer software [scorchworks.com](http://scorchworks.com)

And I have this problem when the k40 is engraving is stop with the error message about timeouts!

Thnks for your time Don Kleinschnitz.


---
**Scorch Works** *July 11, 2018 12:43*

**+Mário Santos** Look at the discussion here: [https://www.google.com/amp/s/amp.reddit.com/r/lasercutting/comments/7vf7j0/help_with_k40_whisperer_timeout_issues/](https://www.google.com/amp/s/amp.reddit.com/r/lasercutting/comments/7vf7j0/help_with_k40_whisperer_timeout_issues/)

[reddit.com - r/lasercutting - Help with K40 Whisperer Timeout Issue(s)](https://www.google.com/amp/s/amp.reddit.com/r/lasercutting/comments/7vf7j0/help_with_k40_whisperer_timeout_issues/)


---
**Mário Santos** *July 11, 2018 14:20*

Hi, I read this same article yesterday, tried to move the cable but did not give great result...I Will try a new USB cable, thanks a lot for your time and everything you done.


---
**Scorch Works** *July 11, 2018 16:20*

LaserDRW doesn't seem to be as sensitive to this problem so I am sure there is some programming that could help.  However, I have not yet figured out what to do to help the problem.


---
**Mário Santos** *July 12, 2018 09:16*

**+Scorch Works**  Hi, I change the USB cable and its works....no the error don't happening :) Thanks again for your time.


---
*Imported from [Google+](https://plus.google.com/111123929078847414796/posts/cg2BDk6Fv8c) &mdash; content and formatting may not be reliable*
