---
layout: post
title: "We have a K40 with Ray's excellent Mini board at work and I was wanting to cut a meARM robot for Christmas for my son: One of the things in cutting the design is the air assist blows the parts around"
date: December 15, 2016 01:28
category: "Discussion"
author: "Trampas Stern"
---
We have a K40 with Ray's excellent Mini board at work and I was wanting to cut a meARM robot for Christmas for my son:

[http://www.thingiverse.com/thing:993759](http://www.thingiverse.com/thing:993759)



One of the things in cutting the design is the air assist blows the parts around.  I was wondering how others have fixed this problem?



One idea I had was to put break away tabs (mouse bites for PCB folk) in the design.  Another idea was to make a vacuum base to hold down work, or make a open base to blow parts to bottom of machine. 



However I figure this is problem that others have solved and since my google skills are failing today I figured I would ask. 





**"Trampas Stern"**

---
---
**Don Recardo** *December 22, 2016 16:02*

If its blowing 3mm acrylic around I would say you have too much

air pressure on your air assist . It needs very little on acrylic.

Mine doesnt even blow paper around


---
**Trampas Stern** *December 23, 2016 12:16*

Thanks! I will turn it down.


---
*Imported from [Google+](https://plus.google.com/105967656691766312361/posts/7skWeLeiCgX) &mdash; content and formatting may not be reliable*
