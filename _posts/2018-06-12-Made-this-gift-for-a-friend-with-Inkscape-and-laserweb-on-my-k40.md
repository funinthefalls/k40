---
layout: post
title: "Made this gift for a friend with Inkscape and laserweb on my k40"
date: June 12, 2018 11:51
category: "Object produced with laser"
author: "Kevin Lease"
---
Made this gift for a friend with Inkscape and laserweb on my k40

![images/cf1947c41032972b88d42f031829e720.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf1947c41032972b88d42f031829e720.jpeg)



**"Kevin Lease"**

---
---
**HalfNormal** *June 12, 2018 12:24*

Sweet!


---
**Don Kleinschnitz Jr.** *June 12, 2018 12:54*

Excellent work! Can you outline for the community how you did the color infill?


---
**Kevin Lease** *June 13, 2018 01:28*

I painted after engraving with testers model paint that I had already

The red and white two coats others one coat


---
**Don Kleinschnitz Jr.** *June 13, 2018 13:13*

**+Kevin Lease** no masking?? Wow you have a steady hand :)!


---
**Frederik Deryckere** *June 20, 2018 13:19*

that is done with a red diode laser ;-)




---
*Imported from [Google+](https://plus.google.com/109387350841610126299/posts/WUFbuATJCjR) &mdash; content and formatting may not be reliable*
