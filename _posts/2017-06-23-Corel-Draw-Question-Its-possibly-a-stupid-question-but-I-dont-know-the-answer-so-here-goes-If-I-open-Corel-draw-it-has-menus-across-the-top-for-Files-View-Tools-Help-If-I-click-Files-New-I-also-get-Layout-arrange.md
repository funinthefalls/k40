---
layout: post
title: "Corel Draw Question, Its possibly a stupid question , but I dont know the answer so here goes If I open Corel draw it has menus across the top for Files View Tools Help If I click Files /New I also get Layout, arrange,,"
date: June 23, 2017 22:21
category: "Software"
author: "Don Recardo"
---
Corel Draw Question,



Its possibly a stupid question , but I dont know the answer so here goes





 If I open Corel draw it has menus across the top for

Files     View     Tools     Help

If I click  Files /New I also get 

Layout,  arrange,, effects ,, bitmap etc



Sometimes I forget to click new  and just open or import a file 

but then I dont have  the needed menus like Arrange etc



Is there any way to get the missing menus up the top  without having to close

the file , do " New" and then load the file back in





**"Don Recardo"**

---
---
**Gee Willikers** *June 23, 2017 22:44*

Try saving the import as a .cdr file or whatever file format corel uses theses days. (I'm an Illustrator guy)


---
**Don Recardo** *June 24, 2017 10:35*

Thanks for your reply but thats not my problem.

I will try to uplaod a picture to show what I mean.



The top image shows the menu bar if I open or import a file

the lower images shows the menu bar if I click file/new

as you can see the menu now has lots more options . My question was , if I forget to click new before opening or importing my file , is there any way to get back the missing menu items  short of delting the file , clicking file/new and reloading the file



 

![images/1282dce1c66436c9dc8ab3a6e154c0bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1282dce1c66436c9dc8ab3a6e154c0bc.jpeg)


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/LkSru4EBjgu) &mdash; content and formatting may not be reliable*
