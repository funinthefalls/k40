---
layout: post
title: "I could use a little assistance. I just replaced the stock control panel with a new acrylic panel so I could add my glcd and generally make a cleaner layout"
date: November 13, 2016 14:50
category: "Modification"
author: "Carl Fisher"
---
I could use a little assistance. I just replaced the stock control panel with a new acrylic panel so I could add my glcd and generally make a cleaner layout.



However now I can't get the laser to fire. I've verified that I'm sending a voltage to the trigger on the power supply but I'm not getting any current passing through the ammeter. 



I'm not sure where to check for issues and dont' want to jump the wrong pins with my multimeter looking for something and causing a bigger issue.



![images/9da3090d0a420759c0d27b9207135a95.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9da3090d0a420759c0d27b9207135a95.jpeg)
![images/e88b620541e87689146fe67bd41ce1ad.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e88b620541e87689146fe67bd41ce1ad.jpeg)

**"Carl Fisher"**

---
---
**Carl Fisher** *November 13, 2016 14:58*

And yes the laser wire switched from yellow to green. I had to extend it about 4" and green is what I had on hand.




---
**Anthony Bolgar** *November 13, 2016 15:07*

Make sure you have the interlock circuit closed (Jumper across the WP terminals, not sure how theyt are labeled on some supplies, mine is wp) Without that circuit being closed, it will not fire. The laser enable button should be in series with that loop.


---
**Carl Fisher** *November 13, 2016 15:16*

The enable switch is the only switch on that circuit and it does open/close as expected.  This was working 100% before I moved over to the new panel. I moved one switch at a time and wire for wire to be sure they all went back exactly.



I get my trigger signal from the smoothie (1 wire setup) but it's like it's just not firing the laser circuit.


---
**Carl Fisher** *November 13, 2016 15:22*

Ok, so this is messed up. To get it to fire I have to press/hold the test fire button. So what got messed up that the test fire is now required to fire under a G1 command.




---
**Anthony Bolgar** *November 13, 2016 15:28*

There were 2 buttons in the original panel, a laser enable and a ttest fire button. Laser enable is on or off, the test fire was a momentary contact switch. Sounds like you put the wrong one in your new panel. Make sure the enable switch is the on/off one and not the momentary one.


---
**Carl Fisher** *November 13, 2016 15:31*

nevermind, I'm completely brain dead this  morning. I was forgetting to issue the M3/M5 commands when I was test firing from LW3.  False alarm and everything is working perfect.


---
**Alex Krause** *November 13, 2016 16:03*

Lol :) glad you got it sorted out. NICE looking panel BTW


---
**Don Kleinschnitz Jr.** *November 13, 2016 16:09*

Very nice panel...




---
**Carl Fisher** *November 13, 2016 22:57*

Thanks on the kudos on the panel. It really does clean it up.



I need to learn not roll out of bed on a Sunday morning and immediately go into the shop to do things that require thinking. I wasted entirely too much time simply to miss enable and disable codes.


---
**Kirk Yarina** *November 14, 2016 01:09*

What did you use to design your front panel?


---
**Carl Fisher** *November 14, 2016 03:24*

**+Kirk Yarina** I drew it up in Inkscape and did the engraving and cutting with LaserWeb


---
**Bill Keeter** *November 14, 2016 17:27*

Nice design. I'm planning to make something very very close to this. I'm just going to add a SD card slot. Also did you think about using clear acrylic? So it covered the LCD? I'm just going to back paint the other areas of the acrylic.



Also how much trouble was it to remove the GLCD dial? Is there a  hidden screw or something? 


---
**Anthony Bolgar** *November 14, 2016 17:53*

Most dial knobs on the Glcd are press fit.


---
**Kirk Yarina** *November 14, 2016 18:00*

That's impressive!  My mental picture (never done one) was spray the back's base color, reverse engrave the lettering, then respray the contrasting color.  Time to rethink that...


---
**Anthony Bolgar** *November 14, 2016 18:06*

It is how I did mine **+Kirk Yarina** [https://plus.google.com/+AnthonyBolgar/posts/A7ppkR1EgBY](https://plus.google.com/+AnthonyBolgar/posts/A7ppkR1EgBY)


---
**Carl Fisher** *November 14, 2016 21:32*

**+Bill Keeter** The dial on mine is just a press fit as I imagine most of them are. Hopefully whoever assembled yours didn't glue it on since that knob really does need to come off to panel mount it.


---
**Carl Fisher** *November 14, 2016 21:34*

**+Bill Keeter** Also regarding the clear acrylic, I really wanted to use what I had on hand and it was either this or pink :)  Since it's right next to my CNC, I may consider putting a thin piece of plexi over the display and just secure it with the standoff mounting screws but we'll see how it goes.


---
**Kelly S** *November 17, 2016 22:29*

**+Carl Fisher** would you be interested in sharing your file?  I have a c3d board on order and have the same parts you do and will have to get the face-lift ready.  ;)


---
**Carl Fisher** *November 17, 2016 23:00*

Sure, let me do some tweaks to the file and I will upload it in the next day or two. You'll have to mark and hand drill a few mounting 

holes though because I don't know that every panel is the same.


---
**Kelly S** *November 17, 2016 23:27*

That is easy, I have a nice step bit for that.  :)  Thank you very much!


---
**Carl Fisher** *November 18, 2016 00:07*

[https://plus.google.com/u/0/105504973000570609199/posts/Sgn7c8W8TDK?sfc=true](https://plus.google.com/u/0/105504973000570609199/posts/Sgn7c8W8TDK?sfc=true)



Make sure to take note of the need to adjust the fonts


---
**Kelly S** *November 18, 2016 02:56*

Thank you!


---
**greg greene** *December 17, 2016 23:12*

What board did you use?  I have the Cohesion 3D board - am trying to get the GLCD Panel - I have the same one as you, but I only get the blue screen of death.  The Config file does have the GLCD portion enabled,  


---
**Carl Fisher** *December 19, 2016 16:18*

**+greg greene**​ Cohesion3d. Make sure your  ribbon cables are plugged in the right way. If so try flipping the expansion board around and make sure you're sitting correctly on the header pins. 


---
**greg greene** *December 19, 2016 16:20*

the ribbon cables are all keyed - so they can only go in one way, I have tried switching them and reversing the sxpansion board - but it's either nothign at all, or the blue screen of death.


---
**Carl Fisher** *December 19, 2016 16:26*

I remember EXP1 and EXP2 not being identified very well on the board that Ray was providing. If you have it wired right, here is my part of the config, maybe it will help you?





## Panel

panel.enable                                 true             # set to true to enable the panel code



# Example for reprap discount GLCD

# on glcd EXP1 is to left and EXP2 is to right, pin 1 is bottom left, pin 2 is top left etc.

# +5v is EXP1 pin 10, Gnd is EXP1 pin 9

panel.lcd                                   reprap_discount_glcd     #

panel.spi_channel                           0                 # spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)

panel.spi_cs_pin                            0.16              # spi chip select     ; GLCD EXP1 Pin 4

panel.encoder_a_pin                         3.26!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                         3.25!^            # encoder pin         ; GLCD EXP2 Pin 5

panel.click_button_pin                      1.30!^            # click button        ; GLCD EXP1 Pin 2

panel.buzz_pin                              1.31              # pin for buzzer      ; GLCD EXP1 Pin 1

panel.back_button_pin                       2.11!^            # back button         ; GLCD EXP2 Pin 8

panel.encoder_resolution                4                       #override the default resolution of 2 for number of steps per detent on the encoder




---
**greg greene** *December 19, 2016 16:33*

Thanks Carl - I'll give it a try


---
**Carl Fisher** *December 19, 2016 16:42*

Also if you do get it working, take note of that additional line for panel.encoder_resolution. Without that, one click on the rotary would jump like 2 or 3 lines making it impossible to use the rotary. That was a little hidden gem that **+Ray Kholodovsky** found for me in one of the smoothie guides somewhere.


---
**greg greene** *December 19, 2016 17:04*

Ray is a great source of info !


---
**greg greene** *December 19, 2016 18:32*

OK Carl, I found these two lines reversed



panel.encoder_a_pin 3.26!^ # encoder pin ; GLCD EXP2 Pin 3

panel.encoder_b_pin 3.25!^ # encoder pin ; GLCD EXP2 Pin 5

from Ray's cfg in that he has pin 3 at 3.25! and pin 5 at 3.26!



stlil doesnt work though


---
**Carl Fisher** *December 19, 2016 18:36*

OH, I forgot. There is a small trim pot on the GLCD board below the screen. Try turning that. It manually adjusts the contrast.


---
**greg greene** *December 19, 2016 18:45*

OK, Is the screen normally blue, or normally dark with blue lettering?


---
**Carl Fisher** *December 19, 2016 18:54*

It could go either way depending on how much you jack with the contrast. Mine currently is blue with light blue/white text. If you crank it too far you may get into a polarized version where the text is darker. Each board seems to behave a bit differently.


---
**greg greene** *December 19, 2016 18:59*

Ok, I get no change with the pot at all, just a bright blue screen so It's looking more and more like a dead panel strange that your cfg and Rays are different on those two pins - but he may have a different panel maker.  Nothing from China is built in any reasonably standard manner it seems !!:)


---
**Carl Fisher** *December 19, 2016 19:21*

yeah, he did mention to me originally that it was a crap shoot with these boards. Mine for example isn't nearly as bright and easy to read as some of the others I've seen no matter what I do with it. Lots of stories of dead boards which is why I bought mine from Amazon instead of someone like Bangood. I wanted ease of dispute if something was bad and through prime they'd take it back for exchange no questions asked if I had needed it.


---
**greg greene** *December 19, 2016 19:48*

Got mine through Ebay - and they're good too, so I'll try and get another one and see, always an adventure with these things isn't it !! :)


---
**Carl Fisher** *December 19, 2016 20:07*

Looking at the board in this orientation, EXP1 is to the right, closest to the power connection and obviously EXP2 is to the left. Just want to make sure you're in the correct orientation.  If that's correct and your config is right and the pot doesn't do anything, sounds like you may have a bad GLCD

![images/7d958f47de6b3f13d191a2b071a0a574.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7d958f47de6b3f13d191a2b071a0a574.jpeg)


---
**greg greene** *December 19, 2016 20:17*

Your connectors are orientated different than mine, which way do the tabs face?


---
**Carl Fisher** *December 19, 2016 20:54*

the tabs face to the outside of the board.


---
**Carl Fisher** *December 19, 2016 20:55*

Unless he changed the adapter. **+Ray Kholodovsky** ?


---
**greg greene** *December 19, 2016 20:58*

I know he's working on a new one so we can use the ethernet board along with the panel.


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2016 22:10*

Greg you and Carl have the same board and adapter. Based on the debugging I've already done with you over email I would agree that it is likely that the GLCD is bad.  

The new adapter (which **+Kelly S** tested) is electrically identical but either way that's unrelated to this. 


---
**greg greene** *December 19, 2016 22:19*

Yes, I believe you are correct - I've contacted the seller for a replacement 



Thank Ray - I'll take one of the new adapters when available.  

Still awaiting the Test fire solution


---
**Ray Kholodovsky (Cohesion3D)** *December 19, 2016 22:26*

The way we wired it up the hardware test button should not work.  

If you want to try the test fire button in LW you need to configure 2 settings in LW (settings --> gcode tab, bottom 2 values) and probably flash the cnc firmware onto the control board. 


---
**Kelly S** *December 19, 2016 23:04*

**+Ray Kholodovsky** any more info to the test fire?  Was looking to align/check my mirrors a while back.  But currently unable to test lol.  Pondering hooking back up the old panel along side the c3d board for the use of a simple test fire as it had a built in digital potentiometer.


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 00:18*

So what's going on?  Is anyone able to do an M3 and then use the fire button in LW?   The 2 wire approach to modifying the laser that we use renders the test fire button not functional because the pwm level is sitting at 0.  **+Kelly S** are you asking me regarding the physical fire button or the one in LW?


---
**Kelly S** *December 20, 2016 00:32*

I have not been able to get laser web to send a test shot yet.  What I was saying is with power going tot he original laser power control board and hooked into the pwm lead, should be able to adjust the wattage and do a test shot as it would act independent of the c3d board.  Will do some test later on when relocating the electronics to make room for a larger gantry.  Hopefully in a week or two.


---
**greg greene** *December 20, 2016 02:44*

Yeah, no luck on sending an M3 through the console then pressing the test fire button in LW - and as you say, the hardware test button isn't working either.  All I can do is setup a dot in a corner in LW then executethe Gcode - but the burn lasts so long it makes a huge mark - hard to get a good alignment that way


---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2016 02:53*

I can play with this in a few days once the sample mini's get here.  My laser is currently torn down and waiting for the new board. 


---
**greg greene** *December 20, 2016 02:57*

No problem Ray, I'm sure it'll get worked out :)


---
*Imported from [Google+](https://plus.google.com/105504973000570609199/posts/h5YGUSNXuDf) &mdash; content and formatting may not be reliable*
