---
layout: post
title: "Im hoping someone can help as l am close to taking a hammer to this machine ."
date: September 16, 2016 07:54
category: "Software"
author: "Martin McCarthy (NGB Discos)"
---
Im hoping someone can help as l am close to taking a hammer to this machine . The issue is that my laptop wont detect the machine, I know that a few people have had similar issues. I have done all the usual stuff so making sure the machine model and id is correct in corel draw etc . I have replaced the motherboard,  so im pretty sure its a driver issue but as everyone knows , instructions are a bit thin on the ground so dont know if im missing a step or im installing the files.  I have also tried it on a different laptop and it still wont detect. Any help would be very much appreciated. 





**"Martin McCarthy (NGB Discos)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 08:19*

There seems to be a folder on the original K40 Software CD (that shipped with mine) that is\The latest CDR software\Laser engraving drive.



In that folder is a Setup.exe & a couple of .sys files & an .inf file. I believe these are drivers that may be required? I don't recall installing drivers myself, but it was a long time ago when I did it.


---
**Alex Krause** *September 16, 2016 11:53*

Windows 10?




---
**Martin McCarthy (NGB Discos)** *September 16, 2016 11:58*

what about windows 10?


---
**Alex Krause** *September 16, 2016 11:59*

A new update to Windows 10 won't allow for unsigned drivers


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 12:02*

you can override this in the settings though and will allow the drivers to be installed and doesnt ecplain why when i plug the laser in , none of the laptops detect any form of device at all. even without a driver it should detect that some form of hardware has been plugged in


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 12:21*

**+Martin McCarthy** Dodgy USB cable maybe?


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 12:24*

nope have tried 3 of those as well


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 12:25*

What is the controller type? Is it the m2nano or the moshi?



You may have a faulty controller...


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 12:27*

M2nano


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 16, 2016 12:30*

Might be worthwhile contacting the seller to ask for a replacement controller (if you recently purchased it). Or see if someone on the group is semi-local & has one that you could test to see if that is the issue.


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 12:33*

By controller do you mean the motherboard?  If so this has been replaced 


---
**Joe Alexander** *September 16, 2016 15:16*

It may have already been done/said but note that for me I had issues often. plug in the usb into the computer then turn on the laser, but MAKE SURE the laser power switch is on lol(the USB power wasn't enough for the board to initialize and register). Facepalmed myself on that one a couple times .for safety i dont press laser enable until i am ready to run a job.


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 15:21*

The racket the psu makes you certainly know when it's switched on 


---
**Alex Krause** *September 16, 2016 18:05*

Did they send a replacement dongle along with the board? Not sure if the USB key is paired with the boards or not


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 19:41*

**+Alex Krause** there was no new key, I don't think they are paired to the board anyway hence why you have to put the I'd of  the board into the corel laser software 


---
**Alex Krause** *September 16, 2016 19:50*

Can you read the output of the cable that connects the psu to the nano board to make sure the PSU is giving the nano board 24v and 5v  power?


---
**Martin McCarthy (NGB Discos)** *September 16, 2016 19:52*

nope dont have anyway to test that


---
**Cherisse Nicastro** *October 10, 2016 04:59*

Did you ever get this sorted out? I just got a machine delivered Friday and seem to have the same problem. I have aligned the laser, but can't connect to Windows 10. Each time I run CorelLaser, I get an error "Engraving Machine is disconnected". I do not see the machine in my devices. I have set the ID and even tried deleting the last character and replacing the USB cable as some posts have suggested. I have checked all of the connections on the board and within the unit. I have tried running CorelLaser as an administrator. None of these have worked. Open to any suggestions....


---
**Martin McCarthy (NGB Discos)** *October 10, 2016 05:59*

**+Cherisse Nicastro** I disconnected the 5v wire coming off the power unit going to the motherboard, that fixed mine as I saw on some of the pics posted here that some had three wires and some had 4. Worked for me 


---
**Cherisse Nicastro** *October 10, 2016 07:29*

Thanks **+Martin McCarthy** I will try that tomorrow!


---
**Cherisse Nicastro** *October 13, 2016 02:26*

My issue was completely user error... Which was easily resolved with a fresh mind in the morning. Documentation (if you can call it that) said to make sure to turn on laser switch (and my unit has a button called that), but I failed to turn on the giant red button (power switch). Duh...


---
*Imported from [Google+](https://plus.google.com/108373254461031274579/posts/c5Nq7tNU3UV) &mdash; content and formatting may not be reliable*
