---
layout: post
title: "Forgive me if this is pinned somewhere as I can't seem to see pinned post from my phone I am about to buy my first laser (k40)"
date: January 13, 2016 17:33
category: "Discussion"
author: "3D Laser"
---
Forgive me if this is pinned somewhere as I can't seem to see pinned post from my phone I am about to buy my first laser (k40). As I have been looking around I have noticed a few different models.  Now I no to stay away from a Moshi board but is one model better than the others





**"3D Laser"**

---
---
**Jim Hatch** *January 13, 2016 19:10*

Non-Moshi board and get air-assist if it's within $50 of the non-air model. I'm adding air to mine - I think most people do if they do a lot of work with wood. A new laser head is $17 from lightobjects but of course I ended up getting a mirror upgrade and a better focusing lens and that plus the air pump was about $100. :-)  Could have gone cheaper on the lens and pump but still cost $50.


---
**3D Laser** *January 13, 2016 19:26*

I need to find a list of mods that can be done.  Air assist is the first thing I'm doing how bad are the lenses and mirrors on this model


---
**Jim Hatch** *January 13, 2016 19:51*

Mine was fine out of the box. I got mine on ebay from "equipmentwholesaler1" and I was engraving and cutting pretty quickly. It was even pretty well aligned when I set it up. So technically no mods were "necessary". But.....



My mod list includes:

Air assist nozzle head, mirror & upgraded 18mm ZnSE lens (Lightobjects), pump (Amazon)

Expanded build bed - removed the stock plate and added a manually variable height bed using a sheet of aluminum, spacers and expanded aluminum grille (Amazon & Home Depot)

Improved air exhaust - cut the air scoop in the back (already added rubber weatherstripping to the exhaust fan to seal that)

Powered air intake - adding 4 5V (pulling the power from the control board) 20cfm 80mm fans to bring air into the box so the exhaust can pull it out (Amazon)

AZSMZ controller board - so I can use Peter's software (on github) for better control (Ebay + stepper controls from Amazon)

Micro lid switch - kills power to the laser if the lid is open (Amazon)



I'm waiting on the new controller board but I'll be installing the fans this weekend I hope (just came in on Sunday and it's just been too cold in the garage to play). In the meantime I get to play on my MakerSpace's 60W laser so I have to split my time with the stuff I'm doing on that one (in addition to being 60W, it's twice as fast engraving and has an 18x24" bed capacity vs my K40's 8.5x14").


---
**3D Laser** *January 13, 2016 20:33*

I'm looking at getting s k40 and selling enough to upgrade to a bigger laser within a year 


---
**3D Laser** *January 14, 2016 00:53*

**+Jim Hatch**



Anyway you can posts pics of your modes k40 so I can get an idea of what it looks like


---
**Jim Hatch** *January 14, 2016 04:14*

Yep. I'll take some this weekend. 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/WDgfadFKNy9) &mdash; content and formatting may not be reliable*
