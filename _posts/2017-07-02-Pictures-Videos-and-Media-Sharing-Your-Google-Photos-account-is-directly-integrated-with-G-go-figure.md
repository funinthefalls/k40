---
layout: post
title: "Pictures, Videos and Media Sharing Your \"Google Photo's\" account is directly integrated with G+ (go figure)"
date: July 02, 2017 02:52
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40MediaSharing



<b>Pictures, Videos and Media Sharing</b>

Your "Google Photo's" account is directly integrated with G+ (go figure). 



Android: If you have an android device the pictures you take are automatically uploaded to your "photos" account and they are directly accessible from G+. When you are at  your machine you can simply take a photo and then go to G+ on your phone or Chrome and post, including that picture.



Other OS: From other devices and OS's you can upload pictures to your Google Pictures account on that PC to get easy access from G+.



Alternate sources: adding sharing links from Box, Gdrive, Dropbox etc work. You can also simply share Google: sheets, forms, documents etc. Sharing documents is a good way to  link the community to a source that might change. That way they get notices when things are changed.

Try this link: [https://docs.google.com/spreadsheets/d/1sImHCr2K4mDRnpdzep_qb5DpMAEQHVOJnwe7Dd7hmH4/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1sImHCr2K4mDRnpdzep_qb5DpMAEQHVOJnwe7Dd7hmH4/edit?usp=sharing)



Albums: Create albums to better organize your pictures and if you make the album public you can share them with the community directly by subject mater. G+ users who subscribe will get notices when  pictures are added.

Try this link:  [https://goo.gl/photos/iHXMYgDSd8BDyDyG8](https://goo.gl/photos/iHXMYgDSd8BDyDyG8)﻿





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/3JjQCBNgXGy) &mdash; content and formatting may not be reliable*
