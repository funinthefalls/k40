---
layout: post
title: "I have been staring at this problem all day and submit it here for some other eyes and ideas"
date: December 07, 2018 22:28
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
I have been staring at this problem all day and submit it here for some other eyes and ideas. Perhaps you like solving puzzles.



Recently I have been Christmas gift running jobs and going through a series of tests on my K40.



Today I decided to do a power test by cutting across the edge of a piece of .22" acrylic to see how deep it cuts.



The acrylic is set on edge in the cutting area, its edge at the correct focal point.



I designed a test in LightBurn that cuts perpendicular to the sample. It cuts 10x and for each cut the % of power is increased by 10%. These vertical cuts are .7" long and .125 apart. The speed is .5 ips.  



I expected this behavior:

1. 10 cycles of the meter going from 0 to x% of the max current

where x = x+10 % on each cycle. xmin = 10 and xmax = 100

2. cut lines space about .125 apart

3. increasing depth for each cut 



In the photo below you see the 10 cuts.

They are about .125 apart but you will notice that they are all the same depth. Actually in some cases earlier ones are deeper than later ones. Huh!

They are not very deep but that could be a lens or alignment problem.



In the video the LPS power is set to 5. On my machine that means that the LPS has 5v on the IN pin... i.e. it is wide open. The actual power is supposed to be the programmed % for each stroke * the IN setting. i.e. (.1)*5 = .5, (.2)*5=1.... etc etc.



<b>You will notice two anomalies I see in the video:</b>

<s>there are 12 transitions of the meter. Huh!</s>

 one at 11 ma then the second jumps to 21 ma and the remaining 10 are at 23 ma. It almost sounds like the first meter jump is as the gantry is moving into position. Huh! Where did the other two cuts go, they are not on the plastic?

<s>the current does not change throughout the last 10</s>



<s>BTW I tried this @ power settings of 2.0 and 3.0 volts.</s>



<s>-----------------</s>

I have been engraving moderately well for a few days but when I attempted a cut is seemed the power was low. That is why I started this test.



Soooo...



Do you see anything I missed?

Am I interpreting something wrong?

Am I using LightBurn incorrectly?



Thanks for looking!

I will be back at it tomorrow.



![images/34e81b38a8c1aefb6cb6d67182d53ba7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34e81b38a8c1aefb6cb6d67182d53ba7.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-GSlRPr7lcJs/XAr0Gpt0XBI/AAAAAAABBjA/h8fuq424ZO85O01tviPM0JehlfPLHLXOACJoC/s0/20181207_114550.mp4**
![images/125f043a0f7fc000a9fa51dfbacd83ef.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/125f043a0f7fc000a9fa51dfbacd83ef.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**LightBurn Software** *December 08, 2018 02:20*

The "easy" way to see if it's a software issue is just save the GCode for your test.  It should be simple commands, so assuming there are no surprises, you likely have a hardware issue.


---
**Don Kleinschnitz Jr.** *December 08, 2018 14:06*

Good idea I will start there....thks


---
**Don Kleinschnitz Jr.** *December 08, 2018 17:58*

**+LightBurn Software** I tried what you suggested and seems there are some extra moves & cuts in the Gcode!



I posted details here: [https://plus.google.com/+DonKleinschnitz/posts/WgZCXZbhf53](https://plus.google.com/+DonKleinschnitz/posts/WgZCXZbhf53)



 




---
**LightBurn Software** *December 08, 2018 18:44*

Two of your layers (C02 and C03) are set to use multiple passes.


---
**Don Kleinschnitz Jr.** *December 08, 2018 20:27*

**+LightBurn Software** OMG, wonder how I did that. Sorry for the bother and thanks!


---
**HalfNormal** *December 09, 2018 17:30*

**+Don Kleinschnitz Jr.** **+LightBurn Software** The reason you pay for quality software is the quality of support you receive!


---
**Don Kleinschnitz Jr.** *December 09, 2018 19:48*

**+HalfNormal** I actually <b>want</b> to pay [a reasonable price] for hobby software for that exact reason. So far as a novice with LB (that's obvious) I am impressed with how easy it is to do the "laser" essentials. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/1SJdduoFUAT) &mdash; content and formatting may not be reliable*
