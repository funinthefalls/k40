---
layout: post
title: "Could someone give a description of how to feel for the correct the tension in the X and Y timing belts?"
date: March 24, 2018 10:18
category: "Hardware and Laser settings"
author: "Duncan Caine"
---
Could someone give a description of how to feel for the correct the tension in the X and Y timing belts?





**"Duncan Caine"**

---
---
**James Rivera** *March 24, 2018 16:12*

...and how to tighten without taking the whole thing apart?


---
**James Rivera** *March 24, 2018 16:27*

As far as how to feel for it, I put my finger on the belts and found one a little bit looser than the other. I pushed up the engraving speed on some text and found that 40mm/s works perfectly, 50mm/s would just start to be a 50/50 chance of getting “squiggly” and anything above that, like 60mm/s was clearly making the steppers miss steps. Given the speeds I’ve seen others here mention, I’m pretty sure my belts could use a tightening. I haven’t had the time yet to investigate it myself, so I’m piggybacking on this question. 🤓


---
**Ned Hill** *March 24, 2018 17:09*

Kind of hard to say.  It shouldn't be too tight but still tight enough to stay engaged on the stepper pulleys.  I can give mine a little strum but still get quite a bit of deflection when I push on the belt.  I know this is kind wishy washy answer.  The belts are a bit elastic so just don't over tighten them.



**+James Rivera** if you are missing steps above 40mm/s on engraving your belts may be too loose.  I routinely engrave at 350-400 mm/s without problems and occasionally up to 550 mm/s.


---
**Ned Hill** *March 24, 2018 17:13*

**+James Rivera** to tighten the y-axis belts there are holes in the back of the machine which allow you to stick a long screwdriver through to get to the belt tensioners on either side of the bed.


---
**James Rivera** *March 24, 2018 17:24*

**+Ned Hill** Thanks!  The X-axis did seem fine, while the Y-axis seemed much looser.  I'll have to look for the holes. I only recall one though...


---
**Duncan Caine** *March 24, 2018 17:42*

**+James Rivera** Hi James, there are 2 holes, about 1cm dia and the tensioning screws are about 120mm from the hole ..... remember those ducks at the fairground you had to hook, and never could, well, finding the head of those tensioning screws is a little like that ... impossible.


---
**Ned Hill** *March 24, 2018 18:28*

I found It helps to reach around through the bed to guide the screwdriver to the screws.  Left side is easier then right but just getting a finger on the screw helps.  Or putting a light in the back of the bed should enable you to look through the hole to see the screw head.  


---
**E Caswell** *March 31, 2018 22:01*

Pics of the y axis adjustment screw that I took when I had the gantry out might help you. 

![images/cd6b33ca2e822ac1e5a9c47779def6eb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cd6b33ca2e822ac1e5a9c47779def6eb.jpeg)


---
*Imported from [Google+](https://plus.google.com/117389844208885052616/posts/G3pkwfk4tZu) &mdash; content and formatting may not be reliable*
