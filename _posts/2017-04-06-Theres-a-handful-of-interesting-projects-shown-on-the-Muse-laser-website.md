---
layout: post
title: "There's a handful of interesting projects shown on the Muse laser website"
date: April 06, 2017 08:33
category: "Repository and designs"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
[https://fslaser.com/Muse](https://fslaser.com/Muse)



There's a handful of interesting projects shown on the Muse laser website. Ranging from Intro to Intermediate to Maker skill. You'll have to scroll about 3/4 of the way down the page to find them.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Jim Hatch** *April 06, 2017 12:47*

There's no download link for them unless you buy their laser (unlike Epilog who makes theirs available to the public). They don't seem to be as challenging or novel as the categories suggest from the little pictures and descriptions that are available. Check out the Epilog ones or Ponoko's free project files (something like 650 of them!) if you're looking for inspiration or something to build. 



[ponoko.com - Laser Cut Designs Free Download. Laser Cut Patterns & Projects.](https://www.ponoko.com/showroom/product-plans/free)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 06, 2017 15:54*

**+Jim Hatch** Oh, I didn't click on them or realise that you can't. Thanks for pointing that out & sharing the ponoko ones. Will take a look.


---
**Jim Hatch** *April 06, 2017 16:01*

there's a bunch of stuff on the ponoko site that are really one-off and not generally useful but still 100 pages of them :-)



You can also go to Thingiverse and do a search on laser and see what pops up - lots of things there too.




---
**HalfNormal** *April 06, 2017 23:56*

Rabbit Lasers also have a bunch of free stuff. Scroll down to the bottom of the page to see the rest.

[rabbitlaserusa.com - Laser Engraver Downloadable Projects Examples / Laser Cutter Machines Examples / laser engraver and cutting projects](http://www.rabbitlaserusa.com/DownloadableProjects.html#CardstockBoxes)


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/JdDLATZFJGc) &mdash; content and formatting may not be reliable*
