---
layout: post
title: "Well, this week I read some posts/blog of HP Persson about his modified air assist system"
date: July 30, 2017 10:26
category: "Modification"
author: "Dennis Luinstra"
---
Well, this week I read some posts/blog of HP Persson about his modified air assist system. Because I made some improvements to my system, I thought I'll share them with you.



A half year ago I bought a new laser nozzle and a 38.1mm lens. The main thing I don't like about this nozzle was that the air intake tube (inside diameter) was too narrow. I filed it out as big as I could go without braking the part (3mm). Also the air flow was pulsing because of the pump type, I fixed that using a empty plastic bottle as an air reservoir. 



Also I found out that the air assist nozzle opening was too wide, a smaller (3mm) opening, much closer too the cutting surface gives a much firmer air stream. Because I don't have a 3d printer, I designed and cutted some wooden rings and glued them together. The modified nozzle is working great! The air stream is much more firmer and cuts are more clean at the top surface.



Also I designed some iron clamps for claming the plywood made of steel and aluminium. After all I am very pleased with the results.

First picture is before last nights modification.



![images/7204cbcde38837a72e616f70f38664d3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7204cbcde38837a72e616f70f38664d3.jpeg)
![images/1f514d0f139afbcce4d5bf45dbbc3586.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f514d0f139afbcce4d5bf45dbbc3586.jpeg)
![images/a8b072affc5c7d1c4be6538dc5bba41a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a8b072affc5c7d1c4be6538dc5bba41a.jpeg)
![images/a0eedad79d1c2ac897256e47ffa587d1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a0eedad79d1c2ac897256e47ffa587d1.jpeg)
![images/079d1ada1fccaecbebecfd822f6c47c2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/079d1ada1fccaecbebecfd822f6c47c2.jpeg)
![images/356c9d60c32c6b872af8c652e86064ff.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/356c9d60c32c6b872af8c652e86064ff.jpeg)
![images/7b9bab10a65f6cbdbee3a847b87ba316.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b9bab10a65f6cbdbee3a847b87ba316.jpeg)

**"Dennis Luinstra"**

---
---
**Don Kleinschnitz Jr.** *July 30, 2017 11:47*

#K40AirAssist


---
**Ned Hill** *July 30, 2017 17:20*

Very nice.  The only comment I have is that I would be somewhat worried about having the wood nozzle catch fire.  If the alignment slips out, the beam can hit the inside of the nozzle.  Not a lot of wood there to burn, but something to keep in mind.


---
**Chris Hurley** *July 31, 2017 00:58*

Can you take some extra pictures of the way you have you drag chain mounted and used? 


---
**Dennis Luinstra** *July 31, 2017 19:53*

And a few photos of the drag chain, hard to photograph! 

![images/b90807c9f1ecffeb75fbc335de1d04e0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b90807c9f1ecffeb75fbc335de1d04e0.jpeg)


---
**Dennis Luinstra** *July 31, 2017 19:53*

![images/82951ab7e4aa747ac1064c20dd5ccdd7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/82951ab7e4aa747ac1064c20dd5ccdd7.jpeg)


---
**Dennis Luinstra** *July 31, 2017 19:54*

![images/b756209af955d877432b3c49f76812b8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b756209af955d877432b3c49f76812b8.jpeg)


---
**Dennis Luinstra** *July 31, 2017 19:58*

And an old photo of my modified Gantry. I am able to cut 34.5cm by 27.6 cm maximum


---
**Dennis Luinstra** *July 31, 2017 20:00*

![images/81b753ab12f515bc93d08b9bd4906b01.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81b753ab12f515bc93d08b9bd4906b01.jpeg)


---
**Chris Hurley** *July 31, 2017 20:01*

Wow that's awesome! Please do share. 


---
**Dennis Luinstra** *July 31, 2017 20:03*

It was a lot of cutting and measuring even grinded a piece of the lineair ball bearing housing to give the belt a bit more space. Only have one photo (see above) and now... We are cutting only 300x210mm sheets 😀


---
**Dennis Luinstra** *July 31, 2017 20:10*

![images/5a52a41fc7d7ce61a7a9c86440b526c0.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5a52a41fc7d7ce61a7a9c86440b526c0.jpeg)


---
*Imported from [Google+](https://plus.google.com/101821475060654077048/posts/TM72EqcbPCr) &mdash; content and formatting may not be reliable*
