---
layout: post
title: "Peter van der Walt , Arthur Wolf : I have a question regarding the inversion property [!] used in the Smoothie configuration file"
date: September 02, 2016 10:50
category: "Modification"
author: "Don Kleinschnitz Jr."
---
**+Peter van der Walt**, **+Arthur Wolf** : I have a question regarding the inversion property [!] used in the Smoothie configuration file. This question relates to my relentless search for truth and understanding for K40 PWM control :). When using a MOSFET port on the smoothie such a P2.4 does the ! invert the output of the processor or does it invert the P2.4 output. To be more clear and eliminate confusion relative the outputs configuration does the ! cause the MOSFET to be turned on or off for a true condition. 





**"Don Kleinschnitz Jr."**

---
---
**Don Kleinschnitz Jr.** *September 02, 2016 11:31*

So to minimize my confusion if i am using a P2.4 output and have ! set in the configuration the PWM pulses at the gate of the N Channnel MOSFET are going from grnd to Vcc or from Vcc to gnd ?


---
**Mircea Russu** *September 02, 2016 13:58*

**+Don Kleinschnitz**  pwm 10% normal pin (no "!") means pin will be high (true) for 10%cycle and low 90% cycle at cpu and mosfet will open for 10% time which is ok for driving the L input which is active low but not ok for IN which would receive 90% duty cycle (pot as pull-up when mosfet is not opened). Now inverting ("!") means the opposite 10%pwm is 90% high and 10% low at cpu...


---
**Don Kleinschnitz Jr.** *September 02, 2016 14:45*

**+Mircea Russu** ...from above..."and mosfet will open for 10% time which is ok for driving the L input which is active low" .... I don't get how the MOSFET going open grnds the L pin? I'm missing something important?




---
**Mircea Russu** *September 02, 2016 17:53*

**+Don Kleinschnitz** Think of the mosfet as a voltage operated switch. Voltage present mosfet open. Mosfet "switch" has one lead grounded (the "source" pin of the mosfet). When powered (at the "gate" pin) the mosfet is open and the third pin (the "drain) is connected with the source pin which is at gnd level, so the drain is at gnd level. When mosfet is not powered at the base pin the drain is left floating and is driven by external circuitry.  You connect the drain of the mosfet to the L pin which has some internal pull-up. Mosfet not powered, the drain floats, is pulled up and laser does not fire. Mosfet is powered, drain is at gnd level, overcomes the pullup resistor in LPS, L becomes low and laser fires. Hope I made myself clear enough, I'm a dentist and not native English speaker. 


---
**Mircea Russu** *September 02, 2016 18:03*

Extra1: This talk is about N channel mosfets, the type present is Smoothie. P channel are the opposite but let's not bother with that. 

Extra2: The MKS Sbase has all 4 mosfets with the source pin tied to smoothie gnd. The original smoothie has some sort of vcc/gnd rails and might be different or need some extra gnd connection for the mosfets to work. 

Extra3: The MKS Sbase has some led+ resistor between drains and vcc rail which pull the drains at almost vcc level when mosfets are not open. That's why I recommend removing these leds for connecting to a LPS which might get damaged by applying 23V at L pin. 

Extra4: The MKS Sbase has a good github repo with all schematics, firmware and parts list. It IS opensource and not a direct 1:1 smoothie clone. 




---
**Don Kleinschnitz Jr.** *September 02, 2016 18:08*

The only modification I would make is that when the gate of a N channel MOSFET is positive it is closed not open and the L pin is therefore at grnd. BTW I think that the L input is the cathode of an LED in an optical isolator. Cheers.


---
**Mircea Russu** *September 02, 2016 18:12*

Yes. Language differences. I meant Open as in an "open faucet: let the flow pass" (water, electrons, whatever). Not open as in an "open switch" :)


---
**Don Kleinschnitz Jr.** *September 02, 2016 18:14*

**+Mircea Russu** I have been looking at the Smoothie schematics and wondered if there is some inversion problem we did not account for. As an EE, I am familiar with MOSFETs and in my smoothie the MOSFET source is gnded and drain is on a pin that can be configured ...


---
**Don Kleinschnitz Jr.** *September 02, 2016 18:15*

**+Mircea Russu** Ah that is what I was struggling with :)


---
**Mircea Russu** *September 02, 2016 18:26*

**+Don Kleinschnitz** The interesing part of the config file which I do not quite get is the ^ and opposite sign (which I don't have on my phone). I mean ! Inverts a signal but you can also use those signs to tell it active high/low. I haven't had the time to dig the surcecode for the exact parsing way. I've bought this laser to cut some faces for my wall switches for the new home automation but I've spent more time fiddling with it than I had initially planned. Now with smoothie and LW3 I have a pretty accurate machine, but still kind of regret not going for the G350. After finishing the automation and moving into the new house I might get a little more laser play time. 


---
**Darren Steele** *September 04, 2016 18:49*

I'm glad it's not just me struggling with this.  I've got the whole thing working using 



[switch.la - is coming soon](http://switch.la)serfire.output_pin                  2.5^



and hooking that up to L on the PSU.  This allows me to set the power using the potentiometer but I do not have a PWM signal going anywhere and simply cannot get it working.  It's incredibly frustrating.  There are so many people saying so many different things and I still haven't found a single source of relevant data.  I've been trying to solve this on and off for about 4 months and I'm getting close to regretting going for the smoothieboard upgrade :(


---
**Mircea Russu** *September 04, 2016 21:52*

**+Darren Steele**​ remove the code for the switch and go laser_module_pwm_pin 2.5! It should work without any hardware changes. 


---
**Robi Akerley-McKee** *September 06, 2016 06:37*

**+Mircea Russu** the ^ and v are only pins where you are reading for the pull up ^ to 3.3volt or v to ground resistors.  ! Just inverts either a pin reading something or setting something.  These are used to read a sensor or switch activated off the board.  A switch to ground is set to 1.23^ pull up so it reads high until you close the switch and conducts to ground pulling the pin low.  A switch to a +voltage like 3.3v is connected to a pin set with v so when the switch is open the pin read low and when you close the switch the pin reads high. The o is for output pins setting something that wants to see a ground when activated. The o! Is for a something that activates when it's not grounded.


---
**Mircea Russu** *September 06, 2016 07:17*

**+Robi Akerley-McKee** You are my hero! Thank you! Pull-down res, that's a new one for me coming from the AVR world. Now I can Wire the Test switch to the Smoothie and fire a constant time length pulse every time :)


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/VzeZGxKUAtU) &mdash; content and formatting may not be reliable*
