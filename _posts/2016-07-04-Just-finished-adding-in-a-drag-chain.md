---
layout: post
title: "Just finished adding in a drag chain!"
date: July 04, 2016 22:48
category: "Modification"
author: "Evan Fosmark"
---
Just finished adding in a drag chain! I'm really liking how clean it keeps things. Shot a quick video to share.





**"Evan Fosmark"**

---
---
**Ned Hill** *July 05, 2016 12:24*

Nicely done.  Just got a drag chain myself, but haven't mounted it yet.  What did you use for the mounting bracket on the laser head?


---
**Robert Selvey** *July 05, 2016 13:45*

Where did you get your drag chain from ?


---
**HalfNormal** *July 05, 2016 14:31*

**+Evan Fosmark** the same question Ned asked. 


---
**Evan Fosmark** *July 05, 2016 16:03*

**+Ned Hill** & **+HalfNormal**    I 3D printed `cable_ends_k40_Laser_End_bracket.stl` from [http://www.thingiverse.com/thing:1275013/#files](http://www.thingiverse.com/thing:1275013/#files)



It doesn't line up perfectly with the drag chain I have, but well enough that I could secure it down. Will probably design a better-fitting one eventually.



**+Robert Selvey** I got this one: [https://amzn.com/B00880AVL2](https://amzn.com/B00880AVL2) Took a good two weeks to get delivered, though.


---
**Pippins McGee** *July 06, 2016 04:24*

what do you suggest for people without 3d printers?

To buy and mount a drag chain what should we do - any advice please.


---
**Evan Fosmark** *July 06, 2016 07:28*

**+Pippins McGee** I'd suggest just going to Home Depot and look at their metal brackets. I bet you can find something of appropriate size that'd work. Or laser cut your own out of acrylic. You can do this by cutting two rectangles with the holes in them, and then use acrylic solvent to weld them together at a 90 degree angle, or cut one long piece with the holes in them, and then heat up & bend it into shape.


---
**Robert Selvey** *July 23, 2016 17:22*

I would like to get one what size is your drag chain ?


---
**Abe Fouhy** *April 07, 2017 07:54*

**+Evan Fosmark** Dumb question, what is the point of the drag chain?


---
**Mario Gayoso** *July 26, 2018 21:04*

**+Robert Selvey**  you can get it in Amazon, 10x15 drag chain




---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/4rW15wRa3jW) &mdash; content and formatting may not be reliable*
