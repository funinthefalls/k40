---
layout: post
title: "I am trying to understand what code is used with the stock DC-KIII CO2 machine?"
date: June 15, 2016 01:52
category: "Software"
author: "Jeremy Hill"
---
I am trying to understand what code is used with the stock DC-KIII CO2 machine?  I see a lot of folks providing guidance on how to upgrade to something else but I can't seem to find the basic code it uses out of the box.  Any insight would be appreciated.  Thank you.





**"Jeremy Hill"**

---
---
**Ariel Yahni (UniKpty)** *June 15, 2016 02:39*

**+Jeremy Hill**​ what do you mean with code? 


---
**Jeremy Hill** *June 15, 2016 02:40*

I know it isn't gcode as several have provided details on how to do mods to get to a gcode set up.  I am curious what code it using to control the movement of X and Y axis?  Thanks


---
**Ariel Yahni (UniKpty)** *June 15, 2016 02:48*

It's propietary and assuming encrypted that's why you need a USB key to run it


---
**Jeremy Hill** *June 15, 2016 02:55*

Good point.  I suspect that is why there aren't a lot of details.  Thanks for the calibration. 


---
**Scott Marshall** *June 15, 2016 03:04*



It's not been successfully decompiled or reproduced. If fact, I spent a lot of time trying to crack the M2nano and all the components are normal commercially available parts. The stepper drivers are standard Allegro chips and the communications chip is kind of non-mainstream but fully documented, and so on, all EXCEPT the microcontroller. It's either proprietary or has been intentionally mis-labeled. Searches for it show bits and piece of part numbers, most leading indeed back the the Chinese firm who produce it. No spec sheets, and what is associated with the partial number matches is in Chinese. If I had to guess, it's a short run, custom chip designed and programmed by Chinese engineering students as a class project, or as an exercise, which was then purchased or developed into a 'product' by those involved.



That's the reason so many people want to change controllers. So many, in fact, in a few weeks I'll be releasing my own product, one to allow 'normal' people (those that are not engineers, electronic hobbyists, or just plain don't wish to fool with it) to install an aftermarket controller in a few minutes without soldering.



It's a “Plug and Play” install kit for aftermarket controllers especially for Mr Art Wolfs Smoothieboard , which is by far the most respected controller currently available for K40 retrofit.



He builds a 1st class unit, and I hope to “follow in his footsteps without stepping on his toes” so to speak.


---
**Pippins McGee** *June 15, 2016 03:42*

**+Scott Marshall** interested in a plug and play solution to upgrading k40. can you elaborate on the advantages of this upgrade?

What can one do with your upgrade that I can't already do with CorelDraw/Corellaser and the existing m2 nano board for example?



I think i will be all for it, but no one has ever explained what the purpose of upgrading is.



cheers.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 06:39*

**+Pippins McGee** It's not so much Scott's upgrade that allows difference in functionality. It's **+Arthur Wolf**'s Smoothieboard that will allow you to use much better software than the CorelDraw/Laser combo. Software wise, I'll be using LaserWeb (by **+Peter van der Walt**) once I get my Smoothieboard conversion completed. LaserWeb is open-source software that has features such as setting different power levels for different items in the same job. So you can cut (engrave too?) using different settings all in the one run. Instead of having to do multiple runs with different settings. I will be using **+Scott Marshall**'s retrofit kit to bridge the Smoothieboard controller to the K40 electronics. Scott's retrofit kit allows for literally just unplugging all the motors/etc & plugging them into his board, which then you plug the Smoothieboard (or other upgraded controller boards) into that. The version I will be going with is his "switchable" board that allows for retaining the original controller/software combo. I choose this as I feel sometimes I may want to use the old stuff, or in an emergency/failure of other component I will have a "backup" option.


---
**Scott Marshall** *June 15, 2016 09:54*

Yuusuf said it.



The software is the improvement, the Smoothie board is a way to the better software, and my gadgets are an easy of getting the Smoothieboard into your K40 without soldering wires together and tracing circuits out. If you're skilled at electronic work, you could handle installing the Smoothieboard  yourself, but if you don't know how to do the work, or don't feel like fooling around with it, my kit makes it a plug in job that should be running in under an hour. The hardest part of the install with my kit is drilling the holes and mounting the power supply. I've included a template for that, and even the screws.


---
**Scott Marshall** *June 15, 2016 09:57*

I posted a fairly detailed pre-release anouncement that answers a lot of questions, but I may have it up wrong. Alex Krause Plus oned it, but that's the only response I've had, I'll look into reposting it in the discussion column and see if it shows up better.


---
**Paul de Groot** *June 15, 2016 10:10*

A simple conversion is an arduino uno with a cnc shield running grbl firmware 0.9


---
**Scott Marshall** *June 15, 2016 10:23*

**+Pippins McGee** I posted a "pre-release" announcement yesterday, but I screw it up and posted it in a place only a few people can see. Here's a link, and I re-posted it in the discussion (Here), but hear it's not sending out notices. If you don't find the info you need, hit me again, and I'll answer any questions you have.



Scott [https://plus.google.com/u/0/photos/116005042509875749334/albums/6296135268706117393/6296135271361327666?cfem=1&pid=6296135271361327666&oid=116005042509875749334](https://plus.google.com/u/0/photos/116005042509875749334/albums/6296135268706117393/6296135271361327666?cfem=1&pid=6296135271361327666&oid=116005042509875749334)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 10:28*

**+Scott Marshall** The notification just came through. 14 minutes after you posted. Must be a google server delay/lag.


---
**Stephen Sedgwick** *June 15, 2016 12:10*

Did I miss a price tag that will be going with this board and then power supply?  and/or combo?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 12:14*

**+Stephen Sedgwick** From what I've discussed with Scott, the price has not been finalised as yet. He should be updating us all with the price in the near future though.


---
**Stephen Sedgwick** *June 15, 2016 12:25*

A rough idea would be awesome as I was looking for a possible plug and play option for Arduino but depending on price (as money makes the work go around), I might just do the wiring/configuration myself.  I was thinking of taking on a similar project if nothing was designed (however if it is and is coming to a completed design I will not step on these toes).


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 15, 2016 14:41*

**+Stephen Sedgwick** I'm sure **+Scott Marshall** will be able to fill you in with a better idea than me. I don't want to suggest a price that is not in the right scope.


---
**Scott Marshall** *June 15, 2016 16:15*

**+Stephen Sedgwick** 



Stephen, I'll be releasing pricing in the next few days.



 This kind of got out ahead of me.



I mentioned my products to a brand new user a couple days ago, suggesting that in a month or so when he's ready, for him to give me a shout, and interest just exploded. Guess I was correct in my thought his was a product that was needed.



I've got prototypes of the single board, (called the ACR) and the Switchable is in final engineering (hopefully) and I will be sending my files tot he board house today or tomorrow. If all goes well the ACR will be available in 3 weeks and the Switchable a week or two after that.


---
**Jeremy Hill** *June 15, 2016 17:34*

Thank you for the feedback.  I appreciate it.


---
*Imported from [Google+](https://plus.google.com/104804111204807450805/posts/TxDUUmRcvWK) &mdash; content and formatting may not be reliable*
