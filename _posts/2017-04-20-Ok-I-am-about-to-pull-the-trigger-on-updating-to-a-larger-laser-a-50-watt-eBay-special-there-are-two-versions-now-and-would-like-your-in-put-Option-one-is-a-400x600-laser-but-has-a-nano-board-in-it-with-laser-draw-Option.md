---
layout: post
title: "Ok I am about to pull the trigger on updating to a larger laser a \"50 watt\" eBay special there are two versions now and would like your in put Option one is a 400x600 laser but has a nano board in it with laser draw Option"
date: April 20, 2017 02:35
category: "Discussion"
author: "3D Laser"
---
Ok I am about to pull the trigger on updating to a larger laser a "50 watt" eBay special there are two versions now and would like your in put



[http://m.ebay.com/itm/50W-CO2-Laser-Engraving-Machine-Engraver-Cutter-Auxiliary-Rotary-Device-600-400-/332184620856?hash=item4d57bf0f38%3Ag%3AHWAAAOSwB-1Y7ByF&_trkparms=pageci%253Afe09336f-2570-11e7-87af-74dbd180407f%257Cparentrq%253A8930310915b0a9c4a24cdf52ffffffcd%257Ciid%253A1](http://m.ebay.com/itm/50W-CO2-Laser-Engraving-Machine-Engraver-Cutter-Auxiliary-Rotary-Device-600-400-/332184620856?hash=item4d57bf0f38%3Ag%3AHWAAAOSwB-1Y7ByF&_trkparms=pageci%253Afe09336f-2570-11e7-87af-74dbd180407f%257Cparentrq%253A8930310915b0a9c4a24cdf52ffffffcd%257Ciid%253A1)



Option one is a 400x600 laser but has a nano board in it with laser draw





Option two is the 300x500 laser with the Ruida board



[http://m.ebay.com/itm/New-50W-CO2-Laser-Engraving-Machine-Engraver-Cutter-with-Auxiliary-Rotary-/281882801833?hash=item41a18636a9%3Ag%3AQmcAAOSwQupXURnj&_trkparms=pageci%253Afe09336f-2570-11e7-87af-74dbd180407f%257Cparentrq%253A8930310915b0a9c4a24cdf52ffffffcd%257Ciid%253A2](http://m.ebay.com/itm/New-50W-CO2-Laser-Engraving-Machine-Engraver-Cutter-with-Auxiliary-Rotary-/281882801833?hash=item41a18636a9%3Ag%3AQmcAAOSwQupXURnj&_trkparms=pageci%253Afe09336f-2570-11e7-87af-74dbd180407f%257Cparentrq%253A8930310915b0a9c4a24cdf52ffffffcd%257Ciid%253A2)













**"3D Laser"**

---
---
**Joe Alexander** *April 20, 2017 03:39*

are you going to keep the mainboard or upgrade it to smoothie/C3D? if upgrade then i'd go for the bigger work area


---
**Ray Kholodovsky (Cohesion3D)** *April 20, 2017 04:12*

To Joe's point I'd be curious to see the electronic guts of one of these guys. And I agree with what he said. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 20, 2017 05:06*

Personally I'd agree with the above comments. Larger work area is always going to be a bonus. I'd seriously consider getting that one & upgrading the controller.


---
**Phillip Conroy** *April 20, 2017 07:10*

I have the 500 X 300 laser cutter and I am very happy,well made ,and smooth ,controller  takes a bit of getting used to .can cut end etch in one go.tube not much more power than k40 as is only 80mm long vk40 700 ,measured 45 watts with my laser power meter,I use a plugin  for coreldraw x7 .timed cut on k40 war 6.30 min and on the gh 350 was only 3 minutes .mine has manually table and red dot pointer.


---
**Joe Alexander** *April 20, 2017 09:54*

**+Phillip Conroy** can you post some pictures of the internal electronics of your 500x300? I'm curious as to the difference between them and the K40s.


---
**Steve Clark** *April 20, 2017 16:45*

Something that seems odd to me on the first one is the width of the machine is stated at 1050 mm a 50 watt rated tube is 1000 mm? It’s not wide enough to hold a 50 watt tube. That is an alarm to me that needs an answer.

Also it still has those crappy mirror cells as the K40 and the cooling system looks also to be not much different from the K40’s.



The second example you show, though having a smaller work area, and from the pictures (pictures can be deceiving though) looks to be a little nicer machine. “Better looking” mirror cell/s and head. The width appears to be enough to hold a 50 watt tube.  Looks like the one that the guy on "RD Works Laser Lab" may have but I did not do a direct comparison.




{% include youtubePlayer.html id="GNV0JUJXdvk" %}
[https://www.youtube.com/watch?v=GNV0JUJXdvk](https://www.youtube.com/watch?v=GNV0JUJXdvk)



Just a thought, you may want to selectively watch his video’s and get more of an insight to these machines. That is what I would do before I jumped. You could also send him a note through his website.



Whichever one you get, I would seriously address the cooling system and the exhaust flue system. Both of these machines are still IMO a kit machine just like the K40 (abet some better options included) and its going to cost a few hundred bucks more to modify and improve the items of concern I mentioned. 




---
**Phillip Conroy** *April 21, 2017 00:14*

![images/92f45ad75b2b34119318183670420b03.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92f45ad75b2b34119318183670420b03.jpeg)


---
**Phillip Conroy** *April 21, 2017 00:14*

![images/82e235988f06a3050269fd72463662b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/82e235988f06a3050269fd72463662b2.jpeg)


---
**Phillip Conroy** *April 21, 2017 00:14*

![images/2433a96c4f21846362115cee7871b631.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2433a96c4f21846362115cee7871b631.jpeg)


---
**Phillip Conroy** *April 21, 2017 00:15*

![images/a33c1dc7d1d3b8c5ce024f61f0ed4e02.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a33c1dc7d1d3b8c5ce024f61f0ed4e02.jpeg)


---
**Phillip Conroy** *April 21, 2017 00:18*

controllers screen is fantastic ,color and shows whats being cut ,time job taken at end and a progress bar,very easy to use


---
**Phillip Conroy** *April 21, 2017 00:19*

also got a rotary attachment with mine 


---
**3D Laser** *April 21, 2017 14:53*

**+Phillip Conroy** hey Phil do you know off hand what the actual cutting space is on the machine I know it says 300x500 but I can push my k40 to 220x110 also I typically order ply the size of the opening could I get a 12x24 sheet of ply in the machine or would I need to trim it down a few inches 


---
**Phillip Conroy** *April 21, 2017 20:54*

Off hand no,however the laser head is adjustable height wise and you can feed in extra large items from th front.the controller is great can cut and engrave in 1 file ,rdworks software is crappie, however most people just us the plugin for coreldraw or autocad.this machine is very well made ,with the mirror mounts and slide rails much better than the [k40.it](http://k40.it) has air assist built in as well as water flow switch..The wiring of the machine is very good and the controller has spare inputs and  out puts to control air soildniod etc.machine is so much smoother than thek40.The laser head zooms from one cut in the work peace to the next at a very good speed where the k40 takes forever . So far I have been able to cut 3mm plywood at 18mm/sec and 15ma.


---
**3D Laser** *April 21, 2017 20:57*

**+Phillip Conroy** wow that is twice as fast as mine k40 leaning more towards the laser with the smaller bed but better components at the moment do recommend a seller most are saying free shipping however also state delivering to a residence cost more 


---
**Phillip Conroy** *April 21, 2017 21:13*

I got mine second hand for 881 au,they brought it to cut cardboard inserts for mobile phones, however it left burned edges so they couldn't use it.the machine sat around for a year unused so was worried about the tube being any good.have tested it with my power meter and is putting out 45 watts so all good so far.these machines have 800mm tubes so are not true 50 watts,more like 40 to 45 max.when this tube goes I will buy an extension and a longer 60 watt tube.might keep the 50wat power supply and try it on the bigger tube


---
**Stephane Buisson** *April 21, 2017 22:37*

to add more confusion to your choice with an extra 600 usd, look that one  (autofocus +100$) :



[ebay.com - Details about  RECI 100W Co2 700x500mm Laser Engraving Cutting Machine Engraver Cutter USB](http://www.ebay.com/itm/RECI-100W-Co2-700x500mm-Laser-Engraving-Cutting-Machine-Engraver-Cutter-USB/112141443625?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D2%26asc%3D40130%26meid%3D25ce814c1acc4d9b9f2e0598540bf594%26pid%3D100005%26rk%3D6%26rkt%3D6%26mehot%3Dpp%26sd%3D222343550969)


---
**3D Laser** *April 24, 2017 15:26*

**+Stephane Buisson** I don't think I could fit it through the door plus I would like to buy a laser already in the USA


---
**Nigel Conroy** *April 25, 2017 14:14*

[http://www.ebay.com/itm/60W-110V-CO2-Engraver-Cutter-Laser-Engraving-Machine-w-USB-Interface-New-/252796092487](http://www.ebay.com/itm/60W-110V-CO2-Engraver-Cutter-Laser-Engraving-Machine-w-USB-Interface-New-/252796092487)?



I've just purchased this one. Was hoping it was going to arrive today, as per the sellers estimated delivery date. However I contacted the freight provider and they hadn't even requested lift gate service, the extra charge for residential delivery ( quotes of $100-$150).



Luckily the lady I spoke to at the freight service wasn't taking any BS and got onto the other company, after they bounced me back to her and sorted it out.



I was really annoyed when I found out about the delivery date as it was one of the reasons I picked the seller. I bought and paid for it 6 days ago and the freight company said the earliest possible date I'll have it is 5/2, 5 business days later then the seller estimated delivery.



I've sent them a strongly worried email and will await their response. I was hoping to have this up and running for the weekend but it'll be much longer now. 



So my advice is get the contact info of the freight company as early as you can and communicate with them directly to confirm lift gate service and you'll need to set up an appointment for delivery. 


---
**Nigel Conroy** *April 25, 2017 14:32*

**+Corey Budwine** to the point of which machine.

I have access to the second one you posted above at work. It is a great machine and does a great job. 

The reason I went with a different machine is that your unable to pass material through it. The small front panel that can be removed is blocked by the black rail at the front of the work area. 

The table on mine is thin aluminium and for precision work you'd need to improve on that as I find it isn't very flat or even. Obviously the exhaust fan requires an upgrade. 


---
**3D Laser** *April 25, 2017 16:30*

**+Nigel Conroy** I am going with the second machine with the updated components I think it will be more than enough space for my needs plus I don't know if I can get anything bigger through my door


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/a3waHmYc67w) &mdash; content and formatting may not be reliable*
