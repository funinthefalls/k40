---
layout: post
title: "Testing my pin bed. I get this strange issue you see in the photo"
date: June 28, 2016 19:26
category: "Discussion"
author: "Mircea Russu"
---
Testing my pin bed. I get this strange issue you see in the photo. Strange because it's on the face of the material, not on the back like a reflection from the bed would be. It's towards the middle of the shape I cut, not always in the same direction. Any ideas?

![images/1cb8d013b3e5414a41890ec932392868.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1cb8d013b3e5414a41890ec932392868.jpeg)



**"Mircea Russu"**

---
---
**Alex Krause** *June 28, 2016 19:36*

Place a piece of paper in your bed and hold down test fire see if you are getting a stray beam comming out of the Head. I had that problem and was related to alignment of my mirrors and the head


---
**Mircea Russu** *June 28, 2016 19:43*

Yes. It is a stray beam. Part circle. Only in the back half of the bed. I guess the beam hits the lens margin. I have this weird alignment problem I can't get rid of it seems. It has something to do with the belts as the travel is not the same and the 2nd mirror to head adjustment has 1mm error when moving the gantry all the way front/back. 


---
**Alex Krause** *June 28, 2016 19:45*

Have you done a corner alignment test? With a piece of tape over the entry to the laser head test fire at low power in all four corners to see if the laser is hitting in the same place at all 4 extremes of the machine


---
**Thor Johnson** *June 28, 2016 20:05*

Yep.  Stray beam.  You gotta stay near the center of the circle at all times and you need to make sure the hole is perpendicular to the beam  (theoretically it doesn't matter, but in reality, it matters a lot).



I had this but it only showed up a "cutting power" -- simple tests on paper never showed it to me until I put some painters tape on top of the plywood I was trying to cut.


---
**Mircea Russu** *June 28, 2016 20:21*

**+Alex Krause**​ I did the corner test but only get 3 out of 4 corners in the exact same spot. It's like the y carriages move different by 1mm aprox. Left side always good, centered. Right side 1mm error front to back. I ended up aligning in the center of y axis so I get +/- 0.5mm at the ends. I had another post about it yesterday. Thanks. 


---
**Alex Krause** *June 28, 2016 20:25*

Your whole axis assembly may be out of alignment. Which would require removing it from the machine and making it square 


---
**Mircea Russu** *June 28, 2016 20:29*

Already tried that. It's square to about 1mm from what I can measure. Always has this 1mm error when x carriage is at right end and I move along y. The pulleys are really sloppy, to wide and a single narrow bearing inside, I'm planning on replacing them with flanged bearings on m3 screws. 


---
**Ben Walker** *June 29, 2016 16:18*

I wonder if your focused beam isnt hitting the edge of your air assist nozzle.  I have had something very similar happen and it turned out the nozzle was just slightly askew.  It also got fairly hot.  But it was engraving along my cut lines but not at the distance this shows.  maybe a mm.


---
*Imported from [Google+](https://plus.google.com/108399777534625810210/posts/SSNXZnkWrn9) &mdash; content and formatting may not be reliable*
