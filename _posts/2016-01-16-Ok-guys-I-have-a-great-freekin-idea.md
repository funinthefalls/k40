---
layout: post
title: "Ok guys I have a great freekin idea"
date: January 16, 2016 02:33
category: "Discussion"
author: "ChiRag Chaudhari"
---
Ok guys I have a great freekin idea. We all agree that G+ is really not the best way to interact here. Lot of useful posts get buried and you cant even edit your original post to add or remove some more pictures neither you can reply with pictures.



So how about we create our own Forum website? I have some experience with wordpress and we can install what ever forum theme we decide or go with what ever works. Domain name is cheap, I can totally host the website on the server I have with Hostgator. I have 3 sites hosted on it. So we can get started for nothing at all. I can at least do that much !







**"ChiRag Chaudhari"**

---
---
**Sean Cherven** *January 16, 2016 02:47*

Ehh, I'd rather just stick with Google+ and create a wiki page with all the good info on it, and we can update and add more info as needed?


---
**3D Laser** *January 16, 2016 03:35*

I personally like the idea of a forum as long as its monitored well for spammers it could do well but a wiki page could serve the same purpose to iguess 


---
**Sean Cherven** *January 16, 2016 04:33*

I like the idea of a wiki page better, but a forum would work, however somebody would have to be responsible for the cost involved, and maintain backups incase of server failure or loss of data due to other reasons.


---
**Sean Cherven** *January 16, 2016 04:34*

A wiki would be free if we went with one of those websites where you can setup a free wiki


---
**Scott Marshall** *January 16, 2016 07:26*

CNC Zone has a sub-forum dedicated to this sort of machine and it's brethren, but it's languishing for lack of activity. The most current post on the K40 is over a year old.  Maybe we could move in and liven it up a tad...



[http://www.cnczone.com/forums/](http://www.cnczone.com/forums/)



There's a variety of other Chinese laser subforums, for the Redsail etc.

 I'm sure with the number of us here, we'd rate our own K40 sub.



Maybe just start posting  in the General Laser thread and let the mod see we're a serious bunch willing to contribute. 



They have an incredible array of talent in CAD/CAM and all CNC disciplines over there. 



Another one I belong to, but caters more to scratch builders is OpenCNC.



Both are worth a look. 



I think a lot of our "horsepower" is wasted here due to the clunky format. I'd like to support Google, but the health of the group is more important.



 I'm expecting this to get big.



This machine is the 1st that's really practical to bash for your average experimenter, being basically a kit that's pre-assembled.

Additionally it appeals to the less technical crafters.



 I'm hoping it's quality will improve and cause it's popularity to jump to phenomenal levels IF/WHEN the manufacturers put on some interlocks and write a real manual. Then it could go mainstream. Especially with better software. Look at what Cricut and Silhouette have done with what amounts to an inkjet printer with a blade in it, and a very minimal (but user friendly) software package.



On the other hand, all it would take is for some inexperienced person to laser a hole in their hand, burn the house down, etc then call the laywers to pretty much ruin a good thing.



As always, sorry for rambling on. 



One last thing, Google mods, if you're out there, or if anyone knows how to contact them, be it known we would like a more mainstream format. If there's anything that can be done. I hereby volunteer my time to assist in any way if we can be accommodated through the Google family. 



I sincerely appreciate the resource Google has set up for us here, and would expect it has exploded the same way under many headings. Our brothers (and sisters) under different subjects may be asking the same question, and I'd imagine Google is already considering an improved format.



Maybe all we have to do is wait a while?



Scott










---
**Richard Vowles** *January 16, 2016 07:56*

The only reason I'm even here is because it's on plus. You can reply with a pictures link in Photos, and if you need to add more, just do another post, finding the old updated one is a hassle... 


---
**Richard Vowles** *January 16, 2016 07:58*

**+Scott Marshall**​, they keep asking for people to send their feedback using the in system feedback mechanism - has anyone tried?


---
**Scott Marshall** *January 16, 2016 08:35*

Mr Vowles, you hit the nail on the head there. Posts are not stored in an indexed fashion. That is the single biggest issue with this format. Also, The 2 most popular forum engines out there all allow you to view the forum titles and thread activity at a glance, thus often answering your question without even posting.



 I haven't been asked anything by Google. If I weer, I would most certainly express my desire for a "conventional" format. 



I see no advantage to this format, and expect it is simply an extension of the Google You-Tube comment engine. The similarity is quite obvious if you think about it. 

I'm just guessing, but think maybe the powers that be at Google may have decided to offer additional conversationalism (Ok, maybe NOT a word) after seeing what happens on a popular you-tube how-to video. They simply expanded the comment engine  and added a indexing system to allow creation of specific subject conversations.



Thank you too, to the Original poster, I saw your name here once, and you've done all of here a service. This is without a doubt "THE PLACE for K40 AFICIONADOS" 

on the internet.



Scott






---
**Stephane Buisson** *January 16, 2016 18:16*

Hi guys, 

I agree with you G+ lack of indexations (in our sense) to become a perfect tool.

But G+ is also incredibly popular (open to the world) and offer some indexation (with google search: type k40 users community  & your request) or search into the appropriate field (old G+).

Now to bounce from a link to an other, a wiki is a good idea, if somebody want to start one, I will be happy to add the link into the "about this community" top box.

I don't think an "other" forum is a good idea, it's a lot of work, repeating all time the same, for those just not wanting to search a bit.  (+Coherent try to start one last month, with a very relative success: [http://k40d40lasers.freeforums.net/](http://k40d40lasers.freeforums.net/) )



Please post relevant info using the bit of G+ indexation we have, I mean the categories (Mod, hard, soft, repo, etc...). For chatting use the comments as much as you want (no limit there). time to time I change a category in function of the replies found in the comments.


---
**ChiRag Chaudhari** *January 16, 2016 19:17*

Most definitely G+ is good place, but here are few things needed to be taken care of. 

> When some one new joins the group and asks the question about something that has been already posted answered a bunch of times.

> You can reply them with links, but if you want to post the picture you have to upload it somewhere and then post the link to it.

> Searching for something is literally PITA. And if its answered in a comment, good luck finding it.

> The new G+ seriously sucks. Its more like social media stream. 

> Even though we can post in different categories most of people does not know how to search category wise. And if you go to particular category its still the stream of the posts.



Wiki is also a good option. But someone has to maintain that or a few guys can do it. But again when someone has question or confusion then you may have to post it here and again its the same story.



Many of us had experience with XDA-Developers.com forum and we know how awesome it is. Because its really we categorized. But all the CNC forums are so damn cluttered with so much information. Now about the domain and hosting, like I said I can take care of all that as I already have 3 of my sites hosted on them. And its my online business so not planning to shut them down any time soon. 



This idea came in my head because I wanted to look up something real quick, which was luckily commented under something that I posted. But it still took me some time to find the right post and scroll through the comments. Lucky enough I was looking for a link so it was easy to spot that blue text! 



So its just a though. Or hey we can always hangout here. I am so glad we are not on facebook, well than I would not be here any way. Quit that shiat about 4 year ago! :P


---
**ChiRag Chaudhari** *January 16, 2016 19:19*

PS: we dont have to jump on any conclusion. but just as another project I will install some forum engine on sub domain and see what I can come up with. At least I will lean a few more things about wed designing! 


---
**I Laser** *January 17, 2016 04:36*

As Stephane said, Coherent has already gone to the trouble of setting up a free forum. I think the concern with any third party solution is future proofing. G+ is free and it's highly unlikely to just disappear someday.



I've switched back to classic G+, you can search the group, you can edit your posts, etc. So for the most part it's functional, though far from perfect.


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/HP8o126yEmW) &mdash; content and formatting may not be reliable*
