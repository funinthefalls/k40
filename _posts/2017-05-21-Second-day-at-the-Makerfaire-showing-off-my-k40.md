---
layout: post
title: "Second day at the Makerfaire showing off my k40"
date: May 21, 2017 13:58
category: "External links&#x3a; Blog, forum, etc"
author: "Paul de Groot"
---
Second day at the Makerfaire showing off my k40😊





**"Paul de Groot"**

---
---
**Ned Hill** *May 21, 2017 14:10*

Looks like you have been having an awesome time.  Very cool.


---
**Paul de Groot** *May 21, 2017 14:13*

**+Ned Hill** yes it has exceeded all my expectations. Totally  awesome. Met a steampunk couple that made victorian chain watches with leds electronics. Amazing 😮


---
**Cesar Tolentino** *May 21, 2017 15:14*

Oh wow. If you were close, I will be there hanging out with you.


---
*Imported from [Google+](https://plus.google.com/102073383537723054608/posts/LQGFiGL9ix9) &mdash; content and formatting may not be reliable*
