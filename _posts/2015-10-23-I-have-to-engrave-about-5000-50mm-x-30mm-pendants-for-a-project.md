---
layout: post
title: "I have to engrave about 5000 50mm x 30mm pendants for a project"
date: October 23, 2015 09:08
category: "Discussion"
author: "Ian Hayden"
---
I have to engrave about 5000 50mm x 30mm pendants for a project.  I have cut out a template to hold 30 [pieces.at](http://pieces.at) a time in the K40.  The engraving on each is a bitmap image of some Japanese text.  As such it will engrave line by line.  It would be faster to engrave each pendant individually  before moving on to the next one. Anybody know how to set up LaserDrw / WinsealXP to do this ?  Of course the software manual doesn't help.





**"Ian Hayden"**

---
---
**Phillip Conroy** *October 23, 2015 09:38*

5000 pendents will take forever,good luck,keep an eye on water temps,i would engrave all 30 at once


---
**Ian Hayden** *October 23, 2015 09:50*

**+Phillip Conroy** engraving one (for test purpose) took nearly a minute.  Engraving 30 at once took 45 minutes, ie 15 minutes wasted travelling between units.  Extrapolated to 5000 - adds approx 40 hours to the total,  So you see why i am keen to find a solution.  


---
**Phillip Conroy** *October 23, 2015 10:05*

Sometimes it is not total time  but how much operator time,ie while you wait 45 min you can be doing other things


---
**Phillip Conroy** *October 23, 2015 10:06*

When do they need to be ready


---
**Ian Hayden** *October 23, 2015 10:09*

i have plenty to do whiles the jobs are running (woodturning mainly), Problem is i have other projects in the queue, and dont have space for a second machine, yet.


---
**Phillip Conroy** *October 23, 2015 10:15*

Sub contrat out to other laser cutter owners, i mainly do  cutting and havent done any engraving yet but keen to give it a go


---
**Chris M** *October 24, 2015 08:31*

You could divide the 30 into columns, and engrave each column as a seperate task. The vertical gaps should be covered quickly. Trouble is, you  will need to click continue between tasks, I think. Don't forget to put a registration point at the top left and include it in all tasks, so that they start from the same place.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 25, 2015 11:06*

Depends on how complex the design is, you could possibly use cut instead of engrave, with low power settings. I was thinking about this myself today as I have noticed when you cut rather than engraving it actually follows edges and seems to be a quicker process. So imagine you had a square to draw, you could break that up into lots of lines; e.g. ||||. So when the laser cutter cuts (on either side of each line) it would follow the line rather than doing dot by dot in a raster setup. Unfortunately complex designs would require a fair bit more work to draw the image, but may decrease the "engrave" time (mock engrave really).


---
**Ian Hayden** *October 25, 2015 11:42*

**+Chris M** thanks - tried that. It is the best option so far, though doesnt mean i can get much other stuff done in between


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 26, 2015 08:44*

**+Ian Hayden** I did a test cut/mock engrave to show you what I was meaning. I will tag you in it.


---
*Imported from [Google+](https://plus.google.com/111645426566096956650/posts/QCAJLqMSYUQ) &mdash; content and formatting may not be reliable*
