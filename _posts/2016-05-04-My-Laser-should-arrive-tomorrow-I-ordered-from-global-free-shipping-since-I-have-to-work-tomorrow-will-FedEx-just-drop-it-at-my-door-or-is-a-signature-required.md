---
layout: post
title: "My Laser should arrive tomorrow :) I ordered from global free shipping since I have to work tomorrow will FedEx just drop it at my door or is a signature required?"
date: May 04, 2016 03:45
category: "Discussion"
author: "Alex Krause"
---
My Laser should arrive tomorrow :) I ordered from global free shipping since I have to work tomorrow will FedEx just drop it at my door or is a signature required? On a side note my Quarterly incentive bonus comes tomorrow too so I will be able to order all the upgrades ASAP ;) I look forward to adding content to the community and picking the brains of all the active members!﻿





**"Alex Krause"**

---
---
**Jim Hatch** *May 04, 2016 04:23*

Mine was left in front of the garage door. It's a big ass box. Of course it was on its side with "this end up" pointing toward the door instead of the sky. Didn't harm it any though. Packed like they knew what they were doing.


---
**varun s** *May 04, 2016 05:18*

I'm just amazed how courier companies just drop off the packages without signatures from the receiver! But here in India be it FedEx or DHL they literally call you on the phone and drop the package and take necessary signatures before leaving.


---
**Alex Krause** *May 04, 2016 05:24*

**+varun s**​ I live in a town of less than 1000 people with the next town being miles away I left a package on my door step for 3 weeks once because it was delivered to me and it wasn't mine 


---
**varun s** *May 04, 2016 05:34*

Wow! 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 04, 2016 07:25*

**+Jim Hatch** I was home when mine was delivered, but mine was upside down in the back of the truck, not secured to anything & had obviously been sliding around. Surprisingly no damage to mine.



**+Alex Krause** Looking forward to having you join the conversations more & seeing your work.


---
**Ben Walker** *May 04, 2016 12:37*

FedEx did a spectacular job delivering mine.  It even came on a Saturday.  I think the policies have changed and unless your address has a 'signature required' flag on it (you can initiate that) or the seller requested a signature (it will be on shipping detail), the carrier will just leave it.  It is a huge box and it aint no lightweight either.  I would say slightly larger than a 4.3cf door fridge freezer.  But heavier.  The faster it is off their truck the better.  Welcome to the fascinating land of cutting with light.


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/EYqxTfsyBGt) &mdash; content and formatting may not be reliable*
