---
layout: post
title: "Need some help before I put the k40 in the street and hit it with my truck"
date: May 07, 2017 01:20
category: "Discussion"
author: "Robert Selvey"
---
Need some help before I put the k40 in the street and hit it with my truck. I decided to get more precise with the focusing the mirrors and for some reason, I just can't get it right been on it for two days prob like 18 hours. If I move the head to the top left it hits dead center I move the head down the bottom left it's way off, move the head to the bottom right hits dead center again, move it to the top right it's way off again. I have been on it so long everytime I move the mirrors feel like I'm going backwards.





**"Robert Selvey"**

---
---
**Ned Hill** *May 07, 2017 01:43*

If you haven't looked at it I would suggest the floating wombat alignment guide. [plus.google.com - - Laser Alignment Guide - With all the laser alignment questions we get, here...](https://plus.google.com/108257646900674223133/posts/LGfT6SS3Tcc)



You need to start with the second mirror and make sure it's centered from the top left to the bottom left.  Once that's achieved you then work on the third mirror (head) to make sure it's centered from the bottom left to bottom right.


---
**Robert Selvey** *May 07, 2017 01:53*

**+Ned Hill** Thanks I downloaded the file will go though it in the morning.




---
**Ned Hill** *May 07, 2017 01:57*

I recommend that you also use a marker and label the screws A, B ,C as stated in the guide.  That makes it easier to follow.

![images/a106adbaccf3913c95d1af45831439a8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/a106adbaccf3913c95d1af45831439a8.png)


---
**Jim Hatch** *May 07, 2017 03:53*

Also check that the gantry is square and the X axis is parallel to the tube.


---
**Sebastian Szafran** *May 07, 2017 05:05*

If you have access to a 3d printer consider replace stock tube mounts to make tube alignment easier e.g. 40 Watt CO2 Laser Tube Clip Remix found on #Thingiverse [thingiverse.com - 40 Watt CO2 Laser Tube Clip Remix by CallJoe](https://www.thingiverse.com/thing:1581069)


---
**Joe Alexander** *May 07, 2017 23:24*

Those tube mounts made all the difference with my setup. I had the same issue as **+Robert Selvey** and ensuring my tube was level and hitting dead center was the problem. Floating wombat guide is how I dialed in and its cutting great! 3mm acrylic and birch @15ma  10mm/s 1 pass.


---
**Robert Selvey** *May 07, 2017 23:27*

Thanks you all for the help, worked on it today and got it really close.


---
**Joe Alexander** *May 07, 2017 23:52*

for a spot check make a svg with a 1mm line in each coener and in center on low power, put a sticker on your laser head(to ensure it hits center of the aperture) and run. all the dots should line up.


---
**Vince Lee** *May 09, 2017 19:02*

My recommendation when aligning is to ignore where the beams lands, at least initially.  The primary goal when adjusting a mirror is to align the beam parallel to the axis of movement so that the beam hits a consistent spot on each subsequent target no matter where the head is.  Only after this is done should you even think about trying to center where the beam hits (which needn't be that close except the final mirror).  To do this, move the target if possible, or if not, move the mirror in and out parallel to itself so the angle of reflection you just set previously doesn't change.  Doing it this way, I can align everything in under 5 minutes.  Only if something is really off should you need to resort to moving the tube itself.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/dNU2q2iurzf) &mdash; content and formatting may not be reliable*
