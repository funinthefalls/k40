---
layout: post
title: "Since I seem to have Laserweb and Ramps/Marlin setup working on the Bench, I'm ready to start the conversion of my K40 from MOSHI to Ramps"
date: March 28, 2016 12:53
category: "Modification"
author: "Kelly Burns"
---
Since I seem to have Laserweb and Ramps/Marlin setup working on the Bench, I'm ready to start the conversion of my K40 from MOSHI to Ramps.  Any input or advice would be appreciated.





**"Kelly Burns"**

---
---
**Anthony Bolgar** *March 28, 2016 13:20*

Just a few pointers that I can think of....

1. Have a new panel cut and ready to install the display and any other switches you added. That way you have a clean neat install if troubleshooting is necessary, also lessens the chance of accidental short circuits.

2.Try and standardize your wiring colours, eg. green for ground, red for positive, etc.

3.Do not connect the laser enable switch until after all the motion control work is installed and tested. I had an issue when I installed mine that the laser would randomly fire on homing moves, almost zapped my hand a couple of times.

4.Be as neat as possible, makes it easier to troubleshoot any issues, as well as making the installation more reliable.

5.If you have trimmed or removed the exhaust vent inside the case, you can set your work area size in the config to 330mm for X, and 230mm for Y.

6.Setup some type of safety system for your laser, such as [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)

Or at least make sure you put in a basic interlock for the door, so it cannot fire the laser if the door is open.

7. RELAX and DON'T PANIC!!!!!. If you have any trouble, just ask for help, enough of us have done the conversion that someone will be able to help you.




---
**Vince Lee** *March 28, 2016 14:26*

I recommended getting all the proper connectors instead of hardwiring.  Then you can swap back to the moshi if you run into any problems or have a breakdown and have to wait for replacement parts.  A middleman board is handy for the ffc cable.


---
**Kelly Burns** *March 28, 2016 18:17*

**+Peter van der Walt** I appreciate input from the expert himself...  I do plan on going to Smoothie, but I wanted start with things that I am comfortable with.  Also, when I started down the covnersion route a month ago, it looked like Smoothie Boards were not available in the U.S.A.    btw...  Love the Laserweb project.  Great stuff and Amazing progress in a very short time.   If I wanted to support the project, would it be best to do it with hardware donations or PayPal?  Feel free to contact me directly and tell me where to send whatever.


---
**Kelly Burns** *March 28, 2016 18:58*

**+Peter van der Walt** The Bench test I did seemed to work great and complete.  I'll check out the link and see if I can't help get you confirmation on the fix. I have kept the Laserweb install up-to-date, so I should have all published changes. I plan on having the Ramps controller installed in the K40 tonight or tomorrow.  That should answer whether or not its working. I'm going to build a 3W Diode setup for the Craft Room so I will always have a Ramps setup to play with so I can be a regular Ramps/Marlin tester if needed.








---
**Kelly Burns** *March 28, 2016 19:01*

**+Anthony Bolgar** Thanks for the info and advice.  I have reconfigured my K40.  Removed the exhaust and installed a semi-fixed Honeycomb bed.  Other than the crappy Moshi software, I feel like its been going pretty good.  




---
*Imported from [Google+](https://plus.google.com/112715413804098856647/posts/ZN917e76yb6) &mdash; content and formatting may not be reliable*
