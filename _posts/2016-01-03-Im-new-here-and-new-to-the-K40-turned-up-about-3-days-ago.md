---
layout: post
title: "I'm new here and new to the K40 - turned up about 3 days ago"
date: January 03, 2016 22:01
category: "Discussion"
author: "Pete OConnell"
---
I'm new here and new to the K40 - turned up about 3 days ago. All I've done so far is cut and 'etch' a couple of pieces of wood (5.5mm ply) for the kids doors however I want to start making things properly with it. I know that the hardware isn't great so I'm going to start modding it asap. I'll continue to read what everyone has done so far and more than likely start asking a shed loads of questions on what to do. :)





**"Pete OConnell"**

---
---
**ChiRag Chaudhari** *January 04, 2016 00:07*

Welcome to the club mate! We have a great community here. A lots of active members with different setups to answer most your questions if not all.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 03:22*

Welcome to the group Pete. I recommend one of the first things to do is check mirror alignment & make sure your lens is the right way up (seems a few of us had it arrive with the lens upside down, which affects the overall engrave/cut performance).


---
**ChiRag Chaudhari** *January 04, 2016 03:29*

**+Yuusuf Sallahuddin** speaking of the lens, it is  concave on one side  and convex on the other. What is the correct orientation?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 04:15*

**+Chirag Chaudhari** As far as I'm aware, concave facing downwards. This seems to be the best orientation (from my tests) for being able to cut properly. I was unable to cut through 2mm leather cleanly until Scott Thorne mentioned that he could easily. Once I fiddled around with things, changed the lens orientation to concave down (also you will notice the convex side you can see your reflection in it, put that upwards) I've been able to cut things much easier (i.e. in 1 pass instead of 10+).


---
**ChiRag Chaudhari** *January 04, 2016 16:44*

**+Yuusuf Sallahuddin** You know what, that is exactly what I did too. Few weeks ago spent hour aligning the mirrors, cleaned them, cleaned the lens but can produce enough power to to cut through at your power levels. Then I flipped the lens and suddenly everything works great.



I think we need to make a cheat sheet for K40 laser to help the new guys.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 04, 2016 17:52*

**+Chirag Chaudhari** Totally agree with the idea of a Cheat Sheet to help new guys to the K40. Because there are so many tiny little things that seem to affect the overall performance of the laser, mainly due to poor quality control on the manufacturing end & extremely poor instructions (i.e. none in English/other languages).


---
**Pete OConnell** *January 04, 2016 18:50*

I will need to get mine up and running again this weekend and check that the mirrors are aligned properly, i did it based on a bit of paper burning and is probably way out of alignment......did it without any safety in mind as well, not thinking the mirrors could be so out of whack it blinds me 3ft up. admittedly the kids weren't running around at the time :)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 05, 2016 06:03*

**+Pete OConnell** The mirrors on mine were badly out of whack to begin with. Fortunately not enough to hit me in the eyes, but the elbow still hurts.


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/9aHL5iYVUpd) &mdash; content and formatting may not be reliable*
