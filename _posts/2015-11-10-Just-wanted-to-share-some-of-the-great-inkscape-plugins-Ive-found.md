---
layout: post
title: "Just wanted to share some of the great inkscape plugins I've found"
date: November 10, 2015 00:27
category: "Software"
author: "Nathaniel Swartz"
---
Just wanted to share some of the great inkscape plugins I've found.



Lets you build boxes with interlocking tabs:

[https://github.com/Neon22/inkscape-LasercutBox](https://github.com/Neon22/inkscape-LasercutBox)



For making gears:

[https://github.com/jnweiger/inkscape-gears-dev](https://github.com/jnweiger/inkscape-gears-dev)



Making flexible (elliptical boxes):

[https://github.com/BvdP/elliptical-box-maker/](https://github.com/BvdP/elliptical-box-maker/)

(Note you really need air assist to cut the flex joints)



Jigsaw puzzle creator:

[https://github.com/Neon22/inkscape-jigsaw](https://github.com/Neon22/inkscape-jigsaw)





**"Nathaniel Swartz"**

---
---
**Nathaniel Swartz** *November 10, 2015 02:52*

Actually I meant to include this boxmaker plugin: think [https://github.com/hellerbarde/inkscape-boxmaker](https://github.com/hellerbarde/inkscape-boxmaker)


---
**I Laser** *November 11, 2015 10:27*

This one is good too. [http://boxmaker.connectionlab.org/](http://boxmaker.connectionlab.org/)


---
*Imported from [Google+](https://plus.google.com/+NathanielSwartz/posts/557ZQC4nkEi) &mdash; content and formatting may not be reliable*
