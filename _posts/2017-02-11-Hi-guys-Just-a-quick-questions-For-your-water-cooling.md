---
layout: post
title: "Hi guys, Just a quick questions. For your water cooling..."
date: February 11, 2017 13:23
category: "Discussion"
author: "1981therealfury"
---
Hi guys,



Just a quick questions.  For your water cooling... do you use full on distilled water or condensed water from a dehumidifier or just tap water?  Also, what do you guys use to stop the water going nasty.  I try to change mine every month or so... but with the cold weather my dehumidifier hasn't produced much these last couple of months and the water im using is starting to go cloudy and green/brown tinted.



Is it safe to put a bit of bleach in the water to stop any thing from growing?





**"1981therealfury"**

---
---
**1981therealfury** *February 12, 2017 14:27*

Any help on this would be greatly appreciated :)


---
**Ned Hill** *February 13, 2017 01:38*

I just use tap water, which for me is treated city water (neutral pH and low mineral content).  My 5gal bucket reservoir doesn't get any sunlight (direct or indirect) so I don't have a problem with algae.  I do get a bit of a slime build up after awhile so I change it about once a month.  If you have a problem with algae you can use an aquarium algaecide.  I wouldn't recommend using bleach, other than maybe a cap full, because it can degrade some types of tubing.  Some people also use RV antifreeze instead of straight water.  Hope this helps.


---
**1981therealfury** *February 13, 2017 08:26*

Thanks **+Ned Hill**​. I went with standard tap water and antifreeze in the end... will see how that goes. I did have some indirect sunlight getting to the bucket previously but have now moved it to another location where that shouldn't be a problem.



There is lots of conflicting information on the Internet with regards to what water to use and what to treat it with... seems like we all do it differently and none of us really have any issues so I'm guessing it's not that important really.


---
**Austin Baker** *February 17, 2017 07:27*

I'm not sure how well it works yet but I dropped an aquarium UV light in the bucket and leave it on all the time.


---
*Imported from [Google+](https://plus.google.com/+1981therealfury/posts/CkJtAiiDvoi) &mdash; content and formatting may not be reliable*
