---
layout: post
title: "What is the right way to mount the saite_cutter Air Assist nozzle?"
date: May 18, 2016 00:43
category: "Discussion"
author: "Pigeon FX"
---
What is the right way to mount the saite_cutter Air Assist nozzle?



I first fitted it the way I assumed it would go together (left hand image), but as it includes a knurled nut and the carriage itself is thicker I was getting the laser hitting far to low. So...I omitted the knurled nut, and used a rubber O-ring between the carriage and the body of the cutter head, and was able to get everything tight level and the air assist and set screw facing the direction I wanted..............but I have a feeling I just installed it incorrectly first time, so did I do something wrong?

![images/30fc744a48ffc1087fd44a851fcdf5f4.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/30fc744a48ffc1087fd44a851fcdf5f4.gif)



**"Pigeon FX"**

---
---
**Scott Marshall** *May 18, 2016 02:38*

You got it. It's the same as mine, except i used felt washers to shim it. 

Make sure it's adjusted straight down the pipe and.... 

Laser away and have fun, 

Scott


---
*Imported from [Google+](https://plus.google.com/107264773673623393202/posts/SAGBcTrqAao) &mdash; content and formatting may not be reliable*
