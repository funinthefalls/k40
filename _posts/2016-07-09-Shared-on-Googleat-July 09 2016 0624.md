---
layout: post
title: "Shared on July 09, 2016 06:24...\n"
date: July 09, 2016 06:24
category: "Software"
author: "Gee Willikers"
---


![images/9c8c7a54b1fe6854588bb81a621b46a1.png](https://gitlab.com/funinthefalls/k40/raw/master/images/9c8c7a54b1fe6854588bb81a621b46a1.png)



**"Gee Willikers"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 09, 2016 07:14*

I use 0.01mm, but close enough.


---
**Jim Hatch** *July 09, 2016 13:35*

**+Yuusuf Sallahuddin**​ 😃 I use .001mm myself. But I don't do that for every line. When I do an engrave, I'll add a 2nd outline around the first that's 2pixels away and 2 pixels thick. That way I don't get an engrave between the outside & inside bounding lines - the inside one still controls, but I get a crisp finish edge to the outside of the engrave.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 09, 2016 16:53*

**+Jim Hatch** Yeah that's a good idea placing a thin border around engraves. I occasionally do that for a crisp edge also, as otherwise it looks vaguely aliased on the curves.


---
*Imported from [Google+](https://plus.google.com/+JeffGolenia/posts/erkFWwcLzZQ) &mdash; content and formatting may not be reliable*
