---
layout: post
title: "Any woodworkers out there, I've started to make some jigs for my tablesaw using the lasercutter"
date: January 26, 2017 16:37
category: "Object produced with laser"
author: "Nigel Conroy"
---
Any woodworkers out there, I've started to make some jigs for my tablesaw using the lasercutter.

I created models in SkecthUp and exported to dxf files.



The sled is based on the segeasy sled by Jerry Bennett and has adjustable fences for creating segments and angled cuts.

 Also created the adjustable stop block from his sawstop.



The knobs I designed but the idea has been done many times.



Hope someone finds them useful or interesting.



![images/5fce77ba8b78ff7cb086c3acdafc3156.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5fce77ba8b78ff7cb086c3acdafc3156.jpeg)
![images/9ff65039a577b9579bbb1e5b714608d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9ff65039a577b9579bbb1e5b714608d7.jpeg)
![images/7c258220089314722cbe48882d40a734.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7c258220089314722cbe48882d40a734.jpeg)
![images/e79eed19bd92196a9dd1da9b1e7b34a2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e79eed19bd92196a9dd1da9b1e7b34a2.jpeg)
![images/37ec9e719a0a5e098101a6628145e590.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/37ec9e719a0a5e098101a6628145e590.jpeg)

**"Nigel Conroy"**

---
---
**Mike Meyer** *January 27, 2017 13:10*

Brilliant...I'm going to shamelessly copy your idea...what did you use for the base wood?


---
**Nigel Conroy** *January 27, 2017 14:02*

No worries, that's exactly what I did. 



I used 1/4 Luan plywood from Lowes.

Laminated sheets together to get the thickness in the fence and the base of the sled. 

I used 1/4 inch carriage bolts and the corresponding nuts are embedded into the knobs.

The square holes you can see were for aligning the boards during the glue up. 

I ran out of time to cut the pins that should go into them but I was able to do the flue up accurately enough without them so I did it anyway. 

[segeasy.com - Seg-Easy Solutions](http://segeasy.com) has the plans for the sled and the stop that I used to base my build from.



I could probably share the DXF file with you if that helps?


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/8ouY7x4fyxo) &mdash; content and formatting may not be reliable*
