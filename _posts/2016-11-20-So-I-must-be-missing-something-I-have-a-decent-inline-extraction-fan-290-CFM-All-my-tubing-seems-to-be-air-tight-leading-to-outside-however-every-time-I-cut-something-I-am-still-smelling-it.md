---
layout: post
title: "So I must be missing something, I have a decent inline extraction fan (290 CFM) All my tubing seems to be air tight leading to outside, however every time I cut something I am still smelling it"
date: November 20, 2016 04:53
category: "Discussion"
author: "Kelly S"
---
So I must be missing something, I have a decent inline extraction fan (290 CFM) All my tubing seems to be air tight leading to outside, however every time I cut something I am still smelling it.  Anyone else run into this issue, and how did you solve it?  





**"Kelly S"**

---
---
**Phillip Conroy** *November 20, 2016 05:02*

Do not forget the cut items will give off a slight smell.fan should be at end of exhaust pipe as it is very hard to seal the joints and if fan was at cutter than pipe would be under pressure and find any small gaps.if fan is at end of pipe then the pipe in in a vacuum and any small gaps would just suck in room air


---
**Jonathan Davis (Leo Lion)** *November 20, 2016 05:03*

it could be something to do with the direction the fan has been placed as it could be on in reverse side thus blowing air in instead of out.


---
**Cesar Tolentino** *November 20, 2016 05:32*

Same thing on mine that's why I covered the whole cabinet with plastic and combine it with a box fan to the window. Solved the issue. 




{% include youtubePlayer.html id="pXPAsT0reaM" %}
[https://youtu.be/pXPAsT0reaM](https://youtu.be/pXPAsT0reaM)


---
**Kelly S** *November 20, 2016 05:48*

I found part of the issue, I had the exhaust fan at the start of my line, I moved it to the end and the smell went down tons, however in the area the fan it (round 290 cfm inline style, will attach picture) I am catching fumes from acrylic, wood I do not care about, but if I stink the house up my other half will be... angry.


---
**Kelly S** *November 20, 2016 05:51*

This is the fan I have.  I do not have an option for a window in my office so it is ran down tubeing to another room basement window and out a dryer vent I put in.

![images/3b03e446af95dfa53847b314d605c410.png](https://gitlab.com/funinthefalls/k40/raw/master/images/3b03e446af95dfa53847b314d605c410.png)


---
**Kelly S** *November 20, 2016 05:53*

Here is the problem area.

![images/284408695822f1391c13f8bef5f0396a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/284408695822f1391c13f8bef5f0396a.jpeg)


---
**Kelly S** *November 20, 2016 05:54*

Yes that is square gutter pipe, but I 3d printed and silicone adapters in so it is super solid.


---
**Cesar Tolentino** *November 20, 2016 06:35*

Be very careful with cutting acrylic.  That thing you smell is cyanide.  You are not suppose to smell that. Pls research more about it. 


---
**Kelly S** *November 20, 2016 19:07*

**+Cesar Tolentino** from my understabding that is from abs and not acrylic.  And I found the source of my issues, it is cold outside and the wood it is in is not air tight and some air is creaping from the temperature difference outside.  Will be fixed later today.


---
**Jim Hatch** *November 20, 2016 19:39*

**+Cesar Tolentino** Acrylic does not have any of the chemicals that turn into cyanide gas when laser cutting. Acrylic is one of the best most consistent materials to use in a laser. Some other plastics like polycarbonates (Lexan) do release cyanide gas which will corrode your laser (in addition to being bad for your lungs). They also tend to catch fire. ABS isn't chemically bad, it just tends to melt into slaggy messes. Acrylic (also known sometimes as plexiglas) is fine and can be distinguished from polycarbonates by its clear edges vs the frosted edges of polycarbonates. 


---
**Cesar Tolentino** *November 20, 2016 19:46*

Hmmm good to know. I need to dig more about this.   I know that there is also a difference in cutting cast vs extruded.  Thanks for the info. 


---
**Jim Hatch** *November 20, 2016 20:58*

**+Cesar Tolentino**​ Not much difference between cast & extruded acrylic except that cast gives a frosted look when raster engraving and extruded can have a tendency to stress crack (called "crazing"). Cast is usually more expensive. Extruded engraving tends to be less defined - a kind of "washed out" look. Cast has more variability in thickness over a large sheet.


---
*Imported from [Google+](https://plus.google.com/102159396139212800039/posts/WL22XgQJhKR) &mdash; content and formatting may not be reliable*
