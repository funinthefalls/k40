---
layout: post
title: "So I have a box full of scrap probably about 150 pieces of birch plywood that are 3-3.5 inches by 11.5 inches does anybody have a good idea what I can use them for?"
date: July 08, 2016 02:26
category: "Discussion"
author: "3D Laser"
---
So I have a box full of scrap probably about 150 pieces of birch plywood that are 3-3.5 inches by 11.5 inches does anybody have a good idea what I can use them for?  Or have a file they would be willing to share I have been making my decks since day one and really haven't explored much else to do with my k40





**"3D Laser"**

---
---
**Anthony Bolgar** *July 08, 2016 03:33*

How thick are the pieces?


---
**Tony Schelts** *July 08, 2016 06:56*

I have been making Mini Tee Lights,  as well as a few other things. I intend taking them to a Craft fare at the end of the month.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 08, 2016 07:57*

Try bookmarks maybe if the wood isn't too thick. Could probably get 2 out of each 3.5 x 11 inches. Or alternatively you could make yourself some business cards with them & hand them out at your stall :)


---
**Scott Marshall** *July 08, 2016 09:14*

Good sign size. Doorknob hangars, the possibilities are endless...


---
**Stephen Sedgwick** *July 08, 2016 12:59*

Something you could do would be card storage box - keeping with that same theme.  Below are a couple links to what I mean... either something that slides together covering the decks... or even a deluxe box/game mat where the top could be your game field and below holds the cards, dice etc..



[https://www.etsy.com/listing/399825727/behemoth-deck-box?utm_source=google&utm_medium=cpc&utm_campaign=shopping_us_a-home_and_living-storage_and_organization-toy_storage&utm_custom1=43631e71-47a6-4a63-8fdb-e8269242cc52&gclid=Cj0KEQjwnv27BRCmuZqMg_Ddmt0BEiQAgeY1l8RRXcSza6qyVaqBL3Y12Olzz3ZucOEW61dJxnNhc-kaAoou8P8HAQ](https://www.etsy.com/listing/399825727/behemoth-deck-box?utm_source=google&utm_medium=cpc&utm_campaign=shopping_us_a-home_and_living-storage_and_organization-toy_storage&utm_custom1=43631e71-47a6-4a63-8fdb-e8269242cc52&gclid=Cj0KEQjwnv27BRCmuZqMg_Ddmt0BEiQAgeY1l8RRXcSza6qyVaqBL3Y12Olzz3ZucOEW61dJxnNhc-kaAoou8P8HAQ)



[http://www.yeggi.com/q/deck+box/5/](http://www.yeggi.com/q/deck+box/5/)


---
**3D Laser** *July 10, 2016 19:36*

**+Stephen Sedgwick** I have been trying to get a good design for a box I know how to operate this machine but I am lacking in the design area 


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/252XHciJJaj) &mdash; content and formatting may not be reliable*
