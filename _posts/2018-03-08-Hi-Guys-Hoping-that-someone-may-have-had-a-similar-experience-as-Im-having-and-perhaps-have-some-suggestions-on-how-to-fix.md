---
layout: post
title: "Hi Guys Hoping that someone may have had a similar experience as I'm having and perhaps have some suggestions on how to fix"
date: March 08, 2018 08:15
category: "Original software and hardware issues"
author: "Andrew Weatherburn"
---
Hi Guys

Hoping that someone may have had a similar experience as I'm having and perhaps have some suggestions on how to fix.



The software that came with mine basically didn’t work, I think the dongle may be a dud, so after a few hours on the net researching possible fixes/solutions, I settled on installing the K40 Whisperer software.



My question is with the Whisperer software, mine doesn’t seem to be fully talking to the K40. When I start it up and initialise, it moves slightly and settles in the top LH corner as it does on the Whisperer youtube tutorial, but it does not respond to any of the Jog commands, i.e. I can’t move the cutter head via the computer, so I have not been able to get any further and do any engraving.



Thanks in advance.......Andrew







**"Andrew Weatherburn"**

---
---
**Seon Rozenblum** *March 09, 2018 00:44*

Hey Andrew - Awesome you posted this here... Can you provide a photo of your K40, to see what the control panel etc looks like. There are so many variations, but maybe the control panel shot + a shot of the controller board to determine if it's a NANO or something else.



You mentioned the laser head moves to the top right corner. The default home for a K40 <b>should</b> be top left, so maybe you are not having a K40 whisperer issue, but rather a hardware issue.



BTW, it's me (Unexpected Maker) :-)


---
**Andrew Weatherburn** *March 09, 2018 01:37*

Oops dont know my right from my left at age 60, now edited! 



Will take pics as suggested and post later. Thanks for your help Seon


---
**Andrew Weatherburn** *March 09, 2018 03:38*

Please see pics showing the model of my K40 and of the controller

![images/cf2e9f0e776bd396a3dce3f9831c0b75.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cf2e9f0e776bd396a3dce3f9831c0b75.jpeg)


---
**Andrew Weatherburn** *March 09, 2018 03:38*

![images/34be3c727e1560e559addc259d0a4382.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/34be3c727e1560e559addc259d0a4382.jpeg)


---
**James Rivera** *March 09, 2018 06:06*

I’d have to double check to whether my board is the same, but I think the case/panel are identical.


---
**James Rivera** *March 09, 2018 06:13*

Here is a picture I found of my board, which works with K40 Whisperer:



[lh3.googleusercontent.com](https://lh3.googleusercontent.com/WY_KNmALdPb_LR-QM6308ALk2dylu_uz2SuU62EkYriq4S7D_GlmMzF1JnsNRCQmZ6V7BUxYyDwTf2Fe8yVzhI_zxXkUsC3ZGLC-9cNknPdS6xl7xTHav_LR1jeO)


---
**Andrew Weatherburn** *March 10, 2018 03:13*

Hi again Guys, I'm still having problems. I have checked all connections, plus had quite a few emails back and forth with Scorch from K4 Whisperer all to no avail as he's suggesting that the machine could be faulty and has suggested that I try and run it with the original software.......which takes me back to square 1. My original problem was that I couldn't get the original software to run in the first place, always getting the message "LaserDRW Main Programer has stopped working" see pic of screen attached. Has anyone else encountered this problem?

![images/43d48f508f5b9065eec12ca664ac27f4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/43d48f508f5b9065eec12ca664ac27f4.jpeg)


---
**James Rivera** *March 10, 2018 08:50*

“Achieving interface language packs”? Is there an error dialog underneath this one?


---
**Andrew Weatherburn** *March 10, 2018 10:40*

**+James Rivera**  You are right there is another message on the bottom of the opening splash screen of the LaserDRW software, but you can see as much as I can, any ideas what that could mean?? I've removed and reinstalled the software a few times and from different sources (download file from the facebook group). I am not experienced with Windows (as I have been Mac for the last decade or so) could there be something basic I'm missing with installing and running the program??


---
**James Rivera** *March 10, 2018 18:26*

Well, if I had to guess, that message is “engrish” meaning it is trying to install a language pack (in a non-standard custom way), probably Chinese, and failing. You could try manually installing the Chinese language packs in Windows, then when you subsequently try to install LaserDRW perhaps it would skip this step, and move on to the rest. Frankly, I don’t think much of software hacked by untrusted 3rd parties (a great root kit delivery system), so I never installed it. I use K40 Whisperer (which is freaking <b>great</b>) but I see you’ve already tried that. I would ask which USB driver method you tried, but I see you’ve already been in contact with Scorchworks, so I’m not sure that will be a useful line of inquiry. I will say I’ve seen a lot of people here having success with the #Cohesion3D board upgrade (I have one I haven’t even opened yet because K40 Whisperer works great for me at the moment). If the Chinese language pack in Windows doesn’t work and the Zadag USB drivers are working, then I have to suspect a faulty driver board, which makes the Cohesion 3D upgrade seem like the best choice. My $0.02.


---
**James Rivera** *March 10, 2018 18:30*

Not sure which version of Windows you have, but perhaps this will give you the general idea if you’re not too familiar with Windows language packs:[docs.microsoft.com - Add Language Interface Packs to Windows &#x7c; Microsoft Docs](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/add-language-interface-packs-to-windows)


---
**Andrew Weatherburn** *March 11, 2018 00:13*

Hey James

Thanks for your input and the language pack which I'll try next. As I think you get, I'm just trying to get to stage one of ensuring the machine isn't faulty, which I'm having suspicions of given that I can't get Whisperer talking properly to the K40. Once I establish whether it is or not and whether it has to go back under warranty, then yes, I'll certainly look at the Cohesion 3D upgrade.

Cheers Andrew


---
*Imported from [Google+](https://plus.google.com/107944531443311658007/posts/3vk1pe3oPr8) &mdash; content and formatting may not be reliable*
