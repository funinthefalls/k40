---
layout: post
title: "Hi guys , trying to do some picture in k40 whisperer and it is coming out like this ......"
date: March 19, 2018 13:46
category: "Object produced with laser"
author: "jindrich stehlik"
---
Hi guys , trying to do some picture in k40 whisperer and it is coming out  like this  ...... Any suggestion what is causing those horizontal lines ?  Material is birch plywood , but dont think  it is a problem of material ..... Could that be whisperer ? 

Machine working great ,  do plenty of raster engraving and those lines are not there , only now when tried some pic for first time .....

Any suggestion  ???? Thanks for help ....

Jin

![images/c874f9c1515e9c766a591fcabc2224b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c874f9c1515e9c766a591fcabc2224b2.jpeg)



**"jindrich stehlik"**

---
---
**jindrich stehlik** *March 19, 2018 13:54*

Done before photo and lines not there ...

![images/cddc64e1e1e4cc50164cc302db897aa5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cddc64e1e1e4cc50164cc302db897aa5.jpeg)


---
**Ned Hill** *March 19, 2018 17:06*

What speed and power were you using for the photo?


---
**jindrich stehlik** *March 19, 2018 17:43*

175 mm/s , i think it is recommended setting for raster emgraving in  whisperer and 25 %  power ...

Not experienced at all yet 😀 , just learn as i go 😁 ,  active relax 


---
**Ned Hill** *March 20, 2018 03:16*

Hmmm not sure then.  I was thinking that if the power was too low the laser maybe dropping out because it looks like the laser isn't firing for some of the raster lines.  But 25% should be fine.  If you up the power does it stay the same?  I would also say check the y-belt to make sure it has good tension.  I"m not well versed on k40 whisperer so I not sure what in the software could cause this if at all.  Any thoughts **+Scorch Works**


---
**Scorch Works** *March 20, 2018 14:35*

It looks semi-random so I would guess it is a mechanical issue.  I would check to see if it gets worse if you go faster or better if you go slower.  Also make sure the correct controller board is selected.  If the wrong controller board is selected the speeds will not be right.


---
**jindrich stehlik** *March 24, 2018 21:22*

Despite having experience with CNC , have to admit to my own shame that i did mistake , such a basic one that it is hard to believe 😁

I cleaned all mirrors and forgot to tight second one , so when gantry moving , mirror was moving in its housing and that was causing horizontal lines .....

Very sorry for wasting your time lads , i usually try to think first , dont know what my mind was up to 😁😁😁

And thank you for your replies . 

Jin


---
**Ned Hill** *March 24, 2018 21:53*

Glad you got it straightened out :)  Not a complete waste of time.  Because you told use the fix we can now add it to our knowledge base in case this comes up again with someone else. #K40HorizontalLines




---
**jindrich stehlik** *April 08, 2018 14:22*

Working well now , very happy about it 😁

![images/8ad7fcbe506d802e14b3a8a92711be6b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8ad7fcbe506d802e14b3a8a92711be6b.jpeg)


---
*Imported from [Google+](https://plus.google.com/118266158783995097899/posts/X741RiR8KE4) &mdash; content and formatting may not be reliable*
