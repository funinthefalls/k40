---
layout: post
title: "Formatting text Formatted text makes your information and/or questions more clear"
date: July 02, 2017 02:47
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40FormatingText



<b>Formatting text</b>

Formatted text makes your information and/or questions more clear. G+ formatting is limited but available:

 

To display text in bold, you would enclose it with the * character. This can look like this for example, which displays like this in bold.



To display text in italics, you would enclose it with the _ character. This can look like this for example, which displays like this in italics.



Last but not least, you can strike through text using the character. If you write -this and that, then this and that will show up like this on Google+.





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/eSjN4Y5175V) &mdash; content and formatting may not be reliable*
