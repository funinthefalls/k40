---
layout: post
title: "Working on a Leather Tobacco Pouch for a mate (as a test) with Laser Engraving"
date: October 22, 2015 01:34
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
Working on a Leather Tobacco Pouch for a mate (as a test) with Laser Engraving.



The main piece (bottom & largest piece) took 3 hours to engrave with settings: 6mA power, 500mm/s speed, 1pixel steps, 1 pass.



I'm pretty happy with the effect of the laser engraving on leather. Definitely a great buy to add different styles to my work.



Is there anything I can do to speed up this process? Am I doing something wrong even?

![images/333c9b3e2657f6336dc5c96e9f8b6a67.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/333c9b3e2657f6336dc5c96e9f8b6a67.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 23, 2015 07:42*

**+Allready Gone** Leatherwork is a bit of a hobby/passion of mine that I am trying to set up as a business (would love to work for myself doing this eventually). Been getting a lot of project requests lately hence lots of leatherwork haha. Got about 5 more currently in the pipeline.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/HJyeNk3K5Rw) &mdash; content and formatting may not be reliable*
