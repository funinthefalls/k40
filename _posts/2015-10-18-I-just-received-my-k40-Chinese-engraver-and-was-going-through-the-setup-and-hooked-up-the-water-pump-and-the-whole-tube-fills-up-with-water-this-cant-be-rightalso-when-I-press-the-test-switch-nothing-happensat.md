---
layout: post
title: "I just received my k40 Chinese engraver and was going through the setup and hooked up the water pump and the whole tube fills up with water, this can't be right...also when I press the test switch nothing happens...at"
date: October 18, 2015 19:42
category: "Hardware and Laser settings"
author: "Scott Thorne"
---
I just received my k40 Chinese engraver and was going through the setup and hooked up the water pump and the whole tube fills up with water, this can't be right...also when I press the test switch nothing happens...at all....can anyone please tell me how to check the power supply, I'm also pretty sure the tube id's broken on the inside.





**"Scott Thorne"**

---
---
**Alessandro Milano** *October 28, 2015 10:31*

Post a photo of the broken tube so we can check.

Pressing the test button should light up the laser...try to put a piece of paper in front of the first mirror to test the laser and to exclude a bad mirrors setting


---
**Scott Thorne** *October 28, 2015 10:34*

The tube was most certainly broken, the company that sold it to me has one on the way...thanks for your post.


---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/WEWQsEo54S1) &mdash; content and formatting may not be reliable*
