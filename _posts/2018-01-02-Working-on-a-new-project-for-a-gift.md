---
layout: post
title: "Working on a new project for a gift"
date: January 02, 2018 03:37
category: "Object produced with laser"
author: "Ned Hill"
---
Working on a new project for a gift. I just cut the cross out of some stained 3mm birch ply I had on hand and the fish from a piece of unstained 3mm ply to see how it would look. Probably going to do the actual piece from some sapele mahogany and maple. Open to other wood combo suggestions. 



![images/208ea51d67a9928b5432d404f136e902.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/208ea51d67a9928b5432d404f136e902.jpeg)
![images/12a22691e80b0a86e0b896109e97d5dd.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/12a22691e80b0a86e0b896109e97d5dd.jpeg)
![images/eb73558274013cb3e895a24e81fce520.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/eb73558274013cb3e895a24e81fce520.jpeg)

**"Ned Hill"**

---
---
**Fook INGSOC** *January 02, 2018 03:42*

Very Nice!


---
**HalfNormal** *January 02, 2018 03:47*

Never fail to impress! 


---
**David Allen Frantz** *January 02, 2018 13:17*

Looks real nice!


---
**Anthony Bolgar** *January 02, 2018 22:10*

Great work as always Ned.


---
**Cesar Tolentino** *January 05, 2018 15:10*

Your creativity always amazes me


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/czPZ7tHwP3P) &mdash; content and formatting may not be reliable*
