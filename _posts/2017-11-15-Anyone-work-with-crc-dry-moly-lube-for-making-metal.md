---
layout: post
title: "Anyone work with crc dry moly lube for making metal?"
date: November 15, 2017 03:58
category: "Discussion"
author: "Alex Raguini"
---
Anyone work with crc dry moly lube for making metal?  Does it work on aluminum?  Any suggestions for bet results?





**"Alex Raguini"**

---
---
**Gavin Dow** *November 29, 2017 14:42*

I haven't found anything that will actually allow me to mark on un-anodized aluminum, including dry moly. The heat just dissipates too quickly to do anything meaningful.

I've heard that the expensive stuff (Cermark) will do it, but I haven't seen it first hand.


---
**Alex Raguini** *November 29, 2017 18:21*

I haven't tried the expensive stuff as it is expensive.  I just wanted to mark the edge of my table with ruler marks.  I've just decided to trash aluminum marking and went with the adhesive rulers woodworkers use for their equipment.



Thanks for the response




---
*Imported from [Google+](https://plus.google.com/117031109547837062955/posts/Ldo3kJogMYh) &mdash; content and formatting may not be reliable*
