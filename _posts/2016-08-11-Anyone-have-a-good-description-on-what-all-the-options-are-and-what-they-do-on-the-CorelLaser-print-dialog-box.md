---
layout: post
title: "Anyone have a good description on what all the options are and what they do on the CorelLaser print dialog box?"
date: August 11, 2016 18:17
category: "Original software and hardware issues"
author: "Bob Damato"
---
Anyone have a good description on what all the options are and what they do on the CorelLaser print dialog box? I think I understand most of them but theres a few I have no clue on. Id like to understand the program more so I can get the results I want.

Thank you!

bob





**"Bob Damato"**

---
---
**Ariel Yahni (UniKpty)** *August 11, 2016 18:20*

i used this [http://goo.gl/jYCbuR](http://goo.gl/jYCbuR)




---
**Bob Damato** *August 11, 2016 18:50*

Oh very nice, thank you! That was probably on the CD that I never got that Im still arguing with the vendor on!!!!


---
**Eric Flynn** *August 11, 2016 18:59*

I have a very good understanding of the obscure things.  If you have anything specific, just ask.


---
**Bob Damato** *August 11, 2016 19:14*

**+Eric Flynn**  Thank you Eric. next time Im in the app and need to figure something out, Ill shoot you a note. You may regret this :)


---
**Savannah Champagne** *August 12, 2016 01:32*

Hi Eric, would you happen to know how you adjust the steps per inch setting?


---
**Eric Flynn** *August 12, 2016 02:00*

**+Savannah Champagne**  Unfortunately, with the stock controller, you cannot change steps per setting.  Is yours not moving the proper distance?


---
*Imported from [Google+](https://plus.google.com/106396592141936890049/posts/Z5GZG7xxTqM) &mdash; content and formatting may not be reliable*
