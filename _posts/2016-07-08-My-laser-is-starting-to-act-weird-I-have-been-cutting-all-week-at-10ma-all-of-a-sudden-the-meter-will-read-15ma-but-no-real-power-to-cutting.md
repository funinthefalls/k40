---
layout: post
title: "My laser is starting to act weird I have been cutting all week at 10ma all of a sudden the meter will read 15ma but no real power to cutting"
date: July 08, 2016 00:02
category: "Discussion"
author: "3D Laser"
---
My laser is starting to act weird I have been cutting all week at 10ma all of a sudden the meter will read 15ma but no real power to cutting.  If I turn the pot all the way down and back up it starts cutting fine any ideas





**"3D Laser"**

---
---
**Jim Hatch** *July 08, 2016 00:31*

My money is on a bad pot. Cheap and easy to swap out to make sure.


---
**greg greene** *July 08, 2016 01:00*

Agreed = thee are MilSpec parts !


---
**Evan Fosmark** *July 08, 2016 04:36*

If you have some contact cleaner, spray it around the wiper for the potentiometer and swing it back and forth a few times.


---
**greg greene** *July 08, 2016 13:05*

Try a product called De-Ox-it works on my old Ham Equipt - but your stuff is much newer - I suspect the Pot arm is not making contact inside the pot


---
**Evan Fosmark** *July 08, 2016 14:21*

**+greg greene** good to see another ham on here! 73, de k7fos


---
**greg greene** *July 08, 2016 14:23*

73 De Ve7gpg


---
*Imported from [Google+](https://plus.google.com/115245163342210504036/posts/2oe8Hrtpj8i) &mdash; content and formatting may not be reliable*
