---
layout: post
title: "Anyone know what codes turn laser on and off?"
date: January 09, 2016 22:44
category: "Software"
author: "Andrew ONeal (Andy-drew)"
---
Anyone know what codes turn laser on and off? I found a pic software to use but requires codes for laser on and off. After going back and forth with the guy, he's asked about possibly wiring laser to d9 to generate faster speeds for photos engraved, anyone familiar with this ? 



I'm thinking perhaps connecting a lower power laser diode to the existing spindle to offer a choice when printing a job. Any thoughts on this?

![images/e02bf82f1089e9b8c83457cc53dfe0e6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e02bf82f1089e9b8c83457cc53dfe0e6.jpeg)



**"Andrew ONeal (Andy-drew)"**

---
---
**ChiRag Chaudhari** *January 09, 2016 22:55*

Looks like you have Marlined it or Ramped it so it's M3 on and M5 to turn off


---
**Stephane Buisson** *January 09, 2016 23:21*

those lcd screen fit Smoothieboard too :-))

If you need some help, **+Andrew ONeal**, you should describe a bit better your situation, hardware, software, d9 ?



a laser diode is an idea we had, making is way slowly ... 


---
**Andrew ONeal (Andy-drew)** *January 10, 2016 00:50*

What do I gain with smoothie and what do they run? Just seems like the laser diodes do better and at such low power in comparison to this k40 co2.


---
**Andrew ONeal (Andy-drew)** *January 10, 2016 00:58*

**+Chirag Chaudhari** thanks for the codes man. Yes it's ramps, but have had so many issues with the gcode generation for projects. Tried many encoder plugins but the one software that's most promising is pic engraver and or jtech photonics both require laser on and off codes. I've learned a ton in researching all this and now think I've got a handle on the coding. Still I have yet to even print a single job, aside from a test line burnt into wood. Thanks again for all the support.


---
**ThantiK** *January 10, 2016 00:58*

**+Andrew ONeal**, Diode lasers don't "do better at low power"....they're garbage.  Where in the world did such an ignorant statement come from?  Adding a laser diode to this would be the most hilariously backwards thing ever done to a K40... like buying a Ferrari to put a go-kart engine into.


---
**ChiRag Chaudhari** *January 10, 2016 01:37*

**+Andrew ONeal**​ I did try jtech plugin too but nothing works better than Turnkey with LaserWeb for me.


---
**Ariel Yahni (UniKpty)** *January 10, 2016 01:47*

Heyyyyy my 3.8 diode is as mini John cooper work


---
**Ariel Yahni (UniKpty)** *January 10, 2016 01:53*

**+Chirag Chaudhari**​ does turnkey run on a rumba? 


---
**ChiRag Chaudhari** *January 10, 2016 02:56*

Not sure. I have to Google what's this Rumba is first. LOL. I'm guessing it's another control board.


---
**ThantiK** *January 10, 2016 03:08*

A Rumba is just an Arduino MEGA with a custom PCB layout that's meant to run a 3D printer.  Anything that will run on RAMPS, Azteeg, etc will run on the Rumba.  The Rumba can run Marlin, which TurnKey was meant to target, so the answer is yes.  It'll run on a Rumba.


---
**Andrew ONeal (Andy-drew)** *January 12, 2016 01:47*

**+ThantiK** if you are going to quote someone then include the entire statement. "Just seems like the laser diodes do better and at such low power in comparison to this k40 co2.﻿" This is all very new to me which is why I turn to those who know more than I do. Right now I have a k40 laser that I rebuilt regarding electronics and is fully functional but I've yet to use it for anything, other than a straight line test.


---
**Andrew ONeal (Andy-drew)** *January 12, 2016 01:50*

**+Chirag Chaudhari** that's awesome. Just haven't had time to mess with it here lately. I got really hung up on how to ready a image or cut in inkskape with out with out using plugins. Laserweb is that like pronterface?


---
**Juan Gianni** *January 18, 2016 18:23*

hi Andrew. I have a problem and maybe you can help me. 

buy KC40 3020 laser machine but the USB key does not work. you have the files on the USB key? Thanks very much !!


---
*Imported from [Google+](https://plus.google.com/116967390217775047304/posts/enPPsuDYRQ8) &mdash; content and formatting may not be reliable*
