---
layout: post
title: "Is this the engraving speed i should expect after upgrading to smoothie?"
date: January 09, 2018 23:11
category: "Smoothieboard Modification"
author: "Martin Larsen"
---
Is this the engraving speed i should expect after upgrading to smoothie?

Running cohesion3d mini and lw4 via usb at 150mm/s


**Video content missing for image https://lh3.googleusercontent.com/-rp7FBOX3DVM/WlVMPUWdfEI/AAAAAAAAHJo/xuwVVB51198eb9metGZTYBNYMfMul6F_ACJoC/s0/VID_20180109_235606.mp4**
![images/c5266271d55d336e1e0d59321fd8ea57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c5266271d55d336e1e0d59321fd8ea57.jpeg)



**"Martin Larsen"**

---
---
**LightBurn Software** *January 10, 2018 00:17*

Try LightBurn - the 30 day trial is free and unrestricted, and the GCode sender is <b>much</b> faster. Smoothie, due to processing speed, will run 250dpi at roughly 80 to 100 mm/sec, and GRBL-LPC will run at roughly 200mm/sec or better.


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2018 00:48*

What Lightburn said.  What you're seeing is the stutter of LW4 not sending the data fast enough.  Try that software. 



Info and Downloads available at [http://cohesion3d.com/lightburn-software](http://cohesion3d.com/lightburn-software) and at [lightburnsoftware.com](http://lightburnsoftware.com)

[cohesion3d.com - Lightburn Software](http://cohesion3d.com/lightburn-software/)


---
**Martin Larsen** *January 10, 2018 00:49*

Thanks.

Just installed and run lightburn, but cant control the Y axis..

I have had an issue with the Y axis on my cohesion 3d mini that was solved by changing the config file to use the A-axis instead, but it seems like that gets overruled when running lightburn :/ - All i get is a strange roaring when jogging the Y axis.



200mm/s seems quite well. I hope that i will get there someday :)


---
**LightBurn Software** *January 10, 2018 00:51*

That shouldn’t be the case - we have the same issue with our test board, in fact. The Smoothie config file change works for us. GRBL-LPC required rebuilding with different axis definitions, as it does not use the config file.


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2018 00:54*

What issue?  And if you remapped Y --> A socket pins in config.txt the controlling software should not change anything.






---
**LightBurn Software** *January 10, 2018 00:54*

You might need to lower the acceleration or rapid speed for in your settings. LightBurn uses those values for rapid moves, or the speed value set in the “Move” window, tabbed behind the cuts window, for click-to-move.


---
**LightBurn Software** *January 10, 2018 00:54*

It sounds like the Y axis being driven too fast and missing steps - that would make the sound you describe.


---
**Ray Kholodovsky (Cohesion3D)** *January 10, 2018 00:57*

And to be clear, I sent Lightburn dev one of our boards that failed QA where the Y socket was not working properly, and they just changed the pins over to the Z socket and everything works...  but we test everything we ship, so I do not think /hope your board has this issue. 


---
**LightBurn Software** *January 10, 2018 00:58*

Ah! Correct - should’ve stated that.


---
**Martin Larsen** *January 10, 2018 06:56*

Maybe a good nights sleep and a fresh mind will help me :)



Ray#

I had the problem with Y-axis not moving. Tried changing the X and Y drivers, but still the same, so read this forum and tried the solution with remapping to A-socket. Works like a charm now. (it also did out of the box, so nothing wrong with your factory test)

Lightburn#

I run smoothie driver. Havent really found out what it takes to run GRBL yet. 

New firmware on the board, and changed settings in lw4 or lightburn. Is that all?



I tried changing speed settings from 300mm/s down to 50mm/s when jogging, but no change. I will try again today and keep you updated with a video when i get home.

Both#

Thanks for your feedback guys :) 


---
**LightBurn Software** *January 10, 2018 07:03*

That's very strange - LightBurn, under the hood, is just emitting either rapid moves, or G1 moves. It's possible that you have your Smoothie rapid setting too high.  That would be "default_seek_rate" in the Smoothie configuration, set in mm/min. I think the shipped default is 4000, but I've probably set mine to 6000 at least, probably 9000.  If you set that too high, you could get the grinding. Maybe LaserWeb applies a feed rate to the rapid moves too.


---
**Claudio Prezzi** *January 10, 2018 15:14*

**+Ray Kholodovsky** Please be correct! It's not LW4 that can not deliver data fast enough, it's smoothiewares serial "protocoll" and the used mbed serial driver that slows down the communication. We can deliver more than 200mm/s at 250dpi with grbl-LPC!!



What some other software does is using a workaround (push the whole file to the USB driver) with the downside of loosing control to stop/pause the machine mid job immediately. We can't accept such a behaviour.


---
**LightBurn Software** *January 10, 2018 15:28*

LightBurn still allows pause, though you are correct that there is a delay as the plan and serial buffers empty. Stop is immediate, as it should always be.


---
**Wolfmanjm** *January 10, 2018 18:37*

**+Claudio Prezzi** NO you are still wrong. It is NOT smoothie serial protocol and we do not use mbed usb serial either. I have proven over and over again the issue is the HOST (PC) usb drivers. There is a delay when it reads the ok, this is not anything we can do about. the [fast-stream.py](http://fast-stream.py) proves that as it decouples the sending from the ok. Once and for all please get your facts straight. I am getting very tired of your consistent refusal to accept facts.


---
**Claudio Prezzi** *January 11, 2018 17:07*

+Wolfmanjm The send->ok is your original "protocol" and [fast-stream.py](http://fast-stream.py) is just your workaround that sacrifice immediate response. Is that wrong?



And once and for all: STOP stalking me with your FAKE FACTS!

It's FACT that the same hardware with a different firmware can deliver twice the pixel rate with the same host. You just never can accept faults on your side.


---
**LightBurn Software** *January 11, 2018 17:13*

Regardless of where the fault lies, a solution exists that is easy to implement, we’ve done so, and customers approve. For us it really is that simple.



We’ll be investigating a number of updates to our streamer to allow for a more or less instant pause on Smoothieware even with the decoupled streaming, and already have this working with GRBL and GRBL-LPC.


---
**Claudio Prezzi** *January 11, 2018 21:02*

**+LightBurn Software** Sure, if you can live with the delay, you can use the workaround (no problem for raster engraving). 

But LW4 is also used for CNC-Milling where we need a synced process and cannot accept a delay of many moves. That's the reason why we don't implement the workaround.


---
**LightBurn Software** *January 11, 2018 21:05*

I would simply provide that as an option, allowing the user to choose.


---
**Wolfmanjm** *January 12, 2018 23:00*

**+Claudio Prezzi** "fake facts" oh really, you are the one that has stated blatantly incorrect information, and if you peruse the original Issue on this that we solved with DarklyLabs, I give the "facts". One more thing the ping pong protocol is not a smoothie protocol, it is the standard reprap protocol. You really need to do some research before you go insulting other developers. The issue is clearly (and documented in the USB drivers themselves) that Macs and Windows USB serial driver waits for several milliseconds before returning a read request even if there is data in the buffer. This means the read of an OK is delayed enough to slow the ping pong protocol (again the STANDARD) in most cases. Linux drivers do not seem to have  this builtin delay, and is why things work much faster in Linux. FWIW the delay is there to try to fill the USB buffers which have a minimum size of 64bytes. I suggest you do your homework.

To be honest I cannot be bothered with you, (let alone stalk you!) but I will correct your incorrect statements when you make them.


---
**LightBurn Software** *January 12, 2018 23:14*

Does the GRBL-LPC firmware use a different USB path? The same software feeding GRBL-LPC with the same mechanism, presumably through the same drivers, does not exhibit the issue. I’m curious as to why.


---
**Claudio Prezzi** *January 13, 2018 12:28*

**+LightBurn Software** That's the right question. And if they use a different USB path, that's the next <b>why</b>.


---
**Claudio Prezzi** *January 13, 2018 12:31*

+Wolfmanjm I don't deny the reason you found for the delay with your firmware, but you deny all the other facts (2x speed with grbl-LPC, even faster with 8bit grbl). You just don't want to hear it!


---
*Imported from [Google+](https://plus.google.com/105195340721594982075/posts/UGax4K3XBNT) &mdash; content and formatting may not be reliable*
