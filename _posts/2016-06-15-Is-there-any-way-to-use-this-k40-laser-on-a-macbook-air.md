---
layout: post
title: "Is there any way to use this k40 laser on a macbook air?"
date: June 15, 2016 22:32
category: "Software"
author: "Christina Marie"
---
Is there any way to use this k40 laser on a macbook air?





**"Christina Marie"**

---
---
**Ariel Yahni (UniKpty)** *June 15, 2016 22:35*

Parallel desktop or. Boot Camp unless you have upgraded the controller


---
**Christina Marie** *June 15, 2016 22:41*

thanks for the reply but I'm not familiar with parallel desktop or boot camp?


---
**Stephane Buisson** *June 15, 2016 22:49*

original stock machine softwares is windows only.

your 2 options are :

- Run windows on your Mac, directly (bootcamp) or indirectly (virtual machine, not recommended due to usb disconnection).

- or modify the K40 with another electronic board which allow to use some Mac compatible software or via navigator like chrome.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 16, 2016 04:52*

Yeah as Ariel & Stephane mention, Bootcamp is probably your easiest/best option. I'm running Windows 10 via Bootcamp on my MacBook & it runs the laser fine. For more on bootcamp/windows install check here ([https://support.apple.com/en-au/HT204990](https://support.apple.com/en-au/HT204990)).


---
**Christina Marie** *June 16, 2016 21:14*

**+Yuusuf Sallahuddin** which version of moshi do you have?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 17, 2016 04:42*

**+Christina Sears** I don't have the moshi. I've got the m2nano board. Using CorelDraw/CorelLaser.


---
*Imported from [Google+](https://plus.google.com/106654713649021155154/posts/bdfJdkjhrGP) &mdash; content and formatting may not be reliable*
