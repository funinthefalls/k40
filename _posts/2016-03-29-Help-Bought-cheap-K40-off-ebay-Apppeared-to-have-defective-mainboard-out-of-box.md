---
layout: post
title: "Help! Bought cheap K40 off ebay. Apppeared to have defective mainboard out of box"
date: March 29, 2016 21:16
category: "Hardware and Laser settings"
author: "Wayne Herndon"
---
Help! Bought cheap K40 off ebay. Apppeared to have defective mainboard out of box. Laser head would not go to home and no communication through USB port. Laser would test fire but nothing else.



Got new NANO M2 mainboard through seller and put it in. When I turned machine on, laser head went to home position. Centered beam. Now unit behaves exactly like original. Laser test fires, will not go to home, and does not communicate through port.



Knowing that there is one common denominator in all of my failed relationships, I must ask what am I doing wrong?



I am plugged into a grounded 110V  circuit. Have separate bare copper ground wire connected to same ground from the laser case.  am running Windows 8 64 bit.



All suggestions welcome.





**"Wayne Herndon"**

---
---
**3D Laser** *March 29, 2016 21:33*

Have you checked all the connectors 


---
**Wayne Herndon** *March 29, 2016 21:41*

Yes. Have checked connections to mainboard and the connections to the power supply. Have not checked connectio to stepper mptpr because I would need to disassemble tha machine.


---
**Phillip Conroy** *March 29, 2016 22:05*

Check power supply voltages esp 24 volts going to mainboardhttps://[dl.dropboxusercontent.com/u/65700374/K40%20Modification%20improvements.pdf](http://dl.dropboxusercontent.com/u/65700374/K40%20Modification%20improvements.pdf)


---
**Wayne Herndon** *March 29, 2016 23:23*

oops.

checking voltage on the 24V line at the mainboard, discovered the 24V wire is loose in its modular plug, Embarrassing.  :) Thanks for the help.


---
**Tom Spaulding** *March 30, 2016 00:42*

Not your fault that it was loose and you got a spare nano board for your wasted time.


---
**Christina Marie** *June 17, 2016 00:34*

same problem! where is the 24v line?


---
*Imported from [Google+](https://plus.google.com/113849066359765553098/posts/SdtaHgkTeou) &mdash; content and formatting may not be reliable*
