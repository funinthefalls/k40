---
layout: post
title: "Received machine yesterday, 50w laser so k50!!!?"
date: July 13, 2016 19:15
category: "Discussion"
author: "Nigel Conroy"
---
Received machine yesterday, 50w laser so k50!!!?



Have a few questions around laser tube as it looks pretty dirty to me?



Also is this the updated machine?







![images/1f073dfef765ecba920faf216c596d9f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f073dfef765ecba920faf216c596d9f.jpeg)
![images/13f411fa75f4835487202a2574336d84.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/13f411fa75f4835487202a2574336d84.jpeg)
![images/d536751074dff1ab8df8cb7b4e31dd31.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d536751074dff1ab8df8cb7b4e31dd31.jpeg)
![images/9c5c0f2a6bcd43f34c59a46ea905508c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c5c0f2a6bcd43f34c59a46ea905508c.jpeg)
![images/5fe00f11e443118ad6562e3777360a15.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5fe00f11e443118ad6562e3777360a15.jpeg)

**"Nigel Conroy"**

---
---
**Derek Schuetz** *July 13, 2016 19:26*

Looks like any other tube


---
**Nigel Conroy** *July 13, 2016 19:32*

Thanks **+Derek Schuetz** , I was just concerned about all the yellow in the tubing. 

I know you had concerns as to whether the one I picked was the most up to date. Is there a way to figure that out?


---
**Derek Schuetz** *July 13, 2016 19:52*

If you have the motorized bed I'd say that is a good sign but as far as anything else I'm unsure I don't think the board really maters all that much


---
**Nigel Conroy** *July 13, 2016 20:02*

Thanks


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 13, 2016 21:43*

I was also concerned about the yellowing in the tubing on mine, so I replaced it. Not sure if it really made any difference other than peace of mind.


---
**Fauzip** *July 14, 2016 01:27*

Where did you buy it


---
**Nigel Conroy** *July 14, 2016 01:31*

Here is a link to the one I purchased 



[http://www.ebay.com/itm/391317039073](http://www.ebay.com/itm/391317039073)﻿


---
**I Laser** *July 14, 2016 02:07*

Probably yellowed by left over coolant/water when they tested it. Like most of them, they seem to sit in factories for a while so the shelf life of the tube is somewhat reduced.


---
**Fauzip** *July 14, 2016 04:19*

that machine looks cool. thank you Nigel for the link, definitely helpful.


---
**Pippins McGee** *July 14, 2016 06:31*

**+Yuusuf Sallahuddin** hi yuusuf, what inner/outer diameter size tubing is used to replace and what material if you dont mind me asking assuming you know off the top of your head - i consider replacing tubing too - dont want it to be loose and fall  off the laser though.. did you use sealent to glue it on? if so what type of sealant? sorry for the questions...

the current tubing sitting in water seem to attract the growth of algae - becomes very slimy. do you add anything to your water by the way to keep water from discolouring tube / growing algae?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *July 14, 2016 07:27*

**+Pippins McGee** Honestly, I don't remember what I got but I think I got 6mm ID (so probably 8mm OD). However, I was concerned of getting it onto the nipples on the tube without breaking them, so I left about 2 inches of the existing tubing attached to the tube (at both ends) & joined my tubing onto those & siliconed the joins (quite thoroughly). I just picked it up in Bunnings in the sprinkler aisle, the clear tubing they have there. Not sure what material it is, but if you purchased a larger ID than I got you should be able to get it over the nipples by heating the end of the tube in a cup of boiling water for a bit (as it becomes quite pliable then). Then maybe heat it with heatgun/hair-dryer once you've got it suitably on to allow it to shrink on a bit & cool it with a damp rag to set it.



In regards to my water, I just used plain rain water (from the tank) with no additives. I've got it in a lidded container (about 30 L) that is sealed, although not air/water-tight. I've yet to have any issue with slimy/algae or anything like that, although my work area never sees the light of day (in garage).


---
**Nigel Conroy** *July 14, 2016 12:20*

I've also found the tubing supplied with mine is very smelly.... I've contacted ebayer with images of yellowing and will see what they say also. 


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/9W9G6ff9kyu) &mdash; content and formatting may not be reliable*
