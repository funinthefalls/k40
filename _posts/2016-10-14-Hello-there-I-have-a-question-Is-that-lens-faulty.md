---
layout: post
title: "Hello there. I have a question. Is that lens faulty?"
date: October 14, 2016 18:10
category: "Original software and hardware issues"
author: "Patryk Hebel"
---
Hello there. I have a question. Is that lens faulty? Can it cause a problems like breaks in between engraving?

![images/6c2efd01d0255e0825d2cec72e451d63.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/6c2efd01d0255e0825d2cec72e451d63.jpeg)



**"Patryk Hebel"**

---
---
**greg greene** *October 14, 2016 18:23*

If you can't clean that spot off - then you need a new one


---
**Patryk Hebel** *October 14, 2016 18:24*

**+greg greene** Yeah it won't come off. Could it cause breaks when engraving?


---
**Ariel Yahni (UniKpty)** *October 14, 2016 18:27*

**+Patryk Hebel**​​ yes﻿.  Is it only on engraving? Do you se that behavior all over the bed? 


---
**greg greene** *October 14, 2016 18:29*

Yup - it needs replacing


---
**Patryk Hebel** *October 14, 2016 18:30*

**+Ariel Yahni** Only when engraving. It breaks at some point. :/


---
**greg greene** *October 14, 2016 18:42*

I suspect the darker area heats up absorbing some energy from the beam - and since you use less power when engraving - it is enough of an absorption to affect the outcome.  You still get it when cutting - but likely there is enough strength left over to continue the cut - and cutting is usually at a slower speed.


---
**Scott Marshall** *October 14, 2016 18:45*

I'd guess your alignment is off as well, and that causes the transmission point of the lens to move around with the head, and THAT will cause it to cut in and out as the transmission point moves into and out of the bad portion of the lens.



Add a good mirror inspection, cleaning & alignment to the tune-up.

It will be like a new Laser.



Scott


---
*Imported from [Google+](https://plus.google.com/102509166997588398801/posts/C7V1MuKnyxb) &mdash; content and formatting may not be reliable*
