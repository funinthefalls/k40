---
layout: post
title: "First Fire shoot, Success !!!"
date: November 10, 2014 14:31
category: "Discussion"
author: "Stephane Buisson"
---
First Fire shoot, Success !!!



![images/19b120c36ed3c9b7ebe5f9ecfd329d71.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/19b120c36ed3c9b7ebe5f9ecfd329d71.jpeg)
![images/9fbcc66f1fb3c0591634dfb5eb8fdf06.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9fbcc66f1fb3c0591634dfb5eb8fdf06.jpeg)
![images/1690d9cb82b0c19d85f9e51838c573b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1690d9cb82b0c19d85f9e51838c573b7.jpeg)
![images/bc283f73acf1a359e2d2d16246b4363a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/bc283f73acf1a359e2d2d16246b4363a.jpeg)
![images/5bfb0fe0e7ed0571d9285c07917aef7a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5bfb0fe0e7ed0571d9285c07917aef7a.jpeg)
![images/d640130ed57084b82cecc2cf81292f0b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d640130ed57084b82cecc2cf81292f0b.jpeg)
![images/cecd89d8b8b2e531b9f7b189bd1bc3dc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cecd89d8b8b2e531b9f7b189bd1bc3dc.jpeg)
![images/891dd1c66d485264aea3de56358f05cf.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/891dd1c66d485264aea3de56358f05cf.jpeg)
![images/81cf0c0b35b34655efd1a21821e9df96.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/81cf0c0b35b34655efd1a21821e9df96.jpeg)

**"Stephane Buisson"**

---
---
**Stephane Buisson** *November 10, 2014 14:34*

after fire shooting on the papers, mirror need cleaning with ear buds.


---
**Stephane Buisson** *November 10, 2014 14:52*

read comments from photos


---
**Imko Beckhoven van** *November 10, 2014 19:36*

Just so you know.. 12 mA is quite high. The maximum i used was 14 maby 15 mA


---
**Stephane Buisson** *November 11, 2014 00:52*

thx, I will try with less, but I am not sure about the focal and distance yet, slowly taking control of all the parameters.


---
**Imko Beckhoven van** *November 11, 2014 09:48*

Focus is only important at the nozzle. Aiming for the center on all mirrors is the most important. This will result in a nice sharp focus.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/cKdqeHgukBF) &mdash; content and formatting may not be reliable*
