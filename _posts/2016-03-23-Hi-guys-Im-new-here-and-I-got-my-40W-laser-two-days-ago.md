---
layout: post
title: "Hi guys, I'm new here, and I got my 40W laser two days ago"
date: March 23, 2016 12:59
category: "Discussion"
author: "Mishko Mishko"
---
Hi guys,

I'm new here, and I got my 40W laser two days ago. It's the exact model as on the community image on the left.

I've tried cutting and everything worked fine until today, when X and Y ratio suddenly changed to around 1:0.3 , so if I try to cut the 10mm square i get a rectangle 10X3mm. Does anybody have an idea what could cause this change? I've checked if I unintentionally checked or unchecked some option, but I don't see anything.

I'm using Corel Laser with Corel Draw X7, as that's what I got in the package.



Any ideas? Thanks in advance,



Miso





**"Mishko Mishko"**

---
---
**Mishko Mishko** *March 23, 2016 13:47*

OK, I have solved this problem, I just reinstalled Corel Laser. This softwatre is awful, and from what I could read, others are no better. 

So, before I take the machine apart and make it work with MACH3, do you have any recommendations which software I could try?

Thanks,

Miso


---
**Stephane Buisson** *March 23, 2016 14:30*

**+Mishko Mishko** why MACH3 ?, please have a look to open source software.

Laserweb and Visicut is a good start. then think about your electronic board.


---
**Mishko Mishko** *March 23, 2016 21:07*

Hi Stephane,

Thanks for your suggestions.

MACH3 because I normally make CNC machines myself, and I'm familiar with the software, and I rarely use CorelDraw for creating input files for CNC. Most often, I start with DXF or Gcode. I use Inventor (with InventorCam), Vectric Aspire, Mastercam etc. depending on the job at hand, and I have tons of stepper drivers and electronics lying around, so this, at least for me, seems a logical shortcut if there's no easy way to obtain software that does a decent job, which Corel Laser certainly doesn't. It's user interface vanishes in the middle of the job, changes x/y ration and what not...

I've downloaded Visicut yesterday, and didn't find the option for K40 laser. Is there an option to use it with this machine without hardware modifications?

I'll check the Laserweb, too, thanks for the tip, but the question is the same, does it work without any mod?

Thanks,



Miso


---
**Stephane Buisson** *March 23, 2016 23:21*

**+Mishko Mishko** unfortunately not, you will need to mod your electronic. this is because the stock electronic is proprietary and closed. but if you are convinced by the open source softwares ( and plenty cool options coming with it), changing the hardware will be the easy part. Smoothie is the best hardware choice now.


---
**Mishko Mishko** *March 24, 2016 06:39*

I thought that will be the case.

Thanks anyway, I will have a look into both softwares and decide how to proceed.

The thing is, I don't really need this laser for any serious job, but it was so cheap that I decided to buy it anyway. I was more interested in making one from scratch, but, not having as much experience with lasers, I thought this one would be functional and simple enough as a means of "training" before I dive into something more complicated, and indeed, for the money, I think it does quite a decent job.

I've removed the funky bed, cleaned all the mirrors, lens and fixtures, which were so dirty I have trouble imagining in what environment these machines are made, my guess is not some garage, more probably some back yard with chicken jumping around...

I haven't read many posts here fo far, and I don't know what results can be expected, but it did cut 6mm Perspex without any problems, and just barely failed to do the same with 8mm (with one pass, it was maybe 0.2-0.3mm short on full power), and that's more than I expected from it.

I couldn't make it engrave, though, and did not try after the Corel Laser reinstallation, so I'll try again today. When I started the job, it just moved dead slow and occasionaly fired a dot or two, althouh the speed was set to 320 mm/s.

Thanks again for your help.

Miso


---
**Stephane Buisson** *March 24, 2016 12:14*

**+Mishko Mishko** you are right to take the K40 for what it is. I would say the main limit is the gantry. if you are happy enought with it, a couple hundred bucks of mods would be enought to make a  very decent machine (check my 2 youtube and posts). for me going open source software was the light at the end of the tunnel. I do have now a reliable machine.


---
**Coherent** *March 25, 2016 18:33*

I too have built a number of CNC machines and use Mach3 on a couple mills and a CNC router. Mach is fine for these type machines or a plasma cutter, but you won't be able to engrave with a laser. Setting it up to turn on and off the laser for cutting is possible and I've read accounts of folks who have done so using a G540 to control the steppers and Mach3 and Gcode, but the speed of the "on/off" signals etc to "pulse" for engraving are another issue and my understanding is the Mach software isn't capable of doing so. This is because engraving text or a photo with a laser isn't done via GCode (which is basically lines and arcs) like cutting can be. The laser software interprets the drawing and pulses on and off the laser quite differently. 


---
**Mishko Mishko** *March 25, 2016 23:31*

I'm still just gathering information and trying not to get overly excited with any option before I'm sure what exactly I intend to use the machine for, or else I'll end up piling software and hardware like a madman, as I always do;)

Cutting is definitely of much more interest to me than engraving, as a segment of a workflow for rapid prototyping, but of course, throwing half a functionality away wouldn't be the most clever decision either.

There is some sort of plugin for engraving on MACH3 website ([http://www.machsupport.com/software/plugins/](http://www.machsupport.com/software/plugins/)), but of course, I don't have an idea how well it works in comparison to other stuff. It's called "Engraving trigger for laser or impact".

The laser sits on my bench right next to my router, so connecting the motors to its drivers and the laser trigger shouldn't take much time and is also non-destructive, so guess I'll give it a go when I get back home from a trip I'm currently on, will post the results if anyone is interested...

I have checked briefly both Smoothieboard and Arduino&Ramps options. Smoothieboard looks good to me, and I have all the parts for the second option, so, more reading for me in the near future;)

But, before doing anything else, I want to do the proper optics alignment and basic hardware stuff, air assist etc...

I do have some experience with lasers, had been working with a Trumph laser for several years, so doing this shouldn't be a problem once I find time and get going...

I'm really glad I found this community, so many people with an interest in exactly the same machine...


---
**Coherent** *March 27, 2016 14:54*

There is some sort of plugin for engraving on MACH3 website ([http://www.machsupport.com/software/plugins/](http://www.machsupport.com/software/plugins/)),



That's promising looking, I had never seen it. I initially looked at using Mach3 after reading all the negative comments about the software that comes with the small Chinese lasers.  It may be an alternative then. Especially if your controller goes bad and you already have another controller and Mach. Things also look very promising going the smoothie board route. DSP seemed like the only real "best" best controller replacement for these lasers, but the cost was prohibitive... no so much now. 


---
**Mishko Mishko** *March 27, 2016 18:09*

If the laser is the only machine, I'd say other options, like Smoothieboard, absolutely makes much more sense. Even if someone, like me, has the complete MACH3 setup on the same bench, this is still something I'd do for testing purposes only, and not as a permanent solution, as to switch the machines, I have to disconnect the router motors, connect laser motors, change motor current settings, connect other signal cables (at least the one to switch laser on and off) and so on. Doable, but really not fun for everyday use. So, if I decided to use MACH3 for laser, I'd have to setup at least one more control box with drivers with relays or something for laser switching. And if I want to use both machines at the same time, add another PC to that setup, if nothing else, there's no way to run two instances of MACH3 simultaneously. So I guess besides testing I will also opt for Smoothieboard.

For someone with laser only, MACH3 makes even less sense given the other options. PC must be of a desktop variety, LPT connection is the only option, and software itself is not free (costs $ 175,00, more than a Smoothieboard)


---
**Coherent** *March 29, 2016 12:44*

**+Mishko Mishko**

 Agree with your comments. I do use Mach3 and the same controller with 3 different machines though. I just swap stepper, limit and probe cables using quick connects and load a different machine profile in Mach3... of course you have to have all the machines in the same shop/area to keep it a simple alternative. Like you said though, only one at a time which is fine in my case. If you do test Mach and the plugin out on the laser, please pass on your results.  I'm sure there are others who would be interested also.


---
**Mishko Mishko** *March 29, 2016 16:22*

I hope I'll find some time this weekend, will post the results, of course.


---
*Imported from [Google+](https://plus.google.com/114169453703152974799/posts/XchdhwqWEix) &mdash; content and formatting may not be reliable*
