---
layout: post
title: "Hey all, I'm finishing up connecting my newest machine, which has been bit of a nightmare, guess you get what you pay for lol..."
date: February 14, 2016 06:37
category: "Modification"
author: "I Laser"
---
Hey all,



I'm finishing up connecting my newest machine, which has been bit of a nightmare, guess you get what you pay for lol... I've installed a mA meter on it, thanks for the help guys, but not sure how to connect a flow switch.



The old machine is easy, the laser enable is a standard switch so can hook the flow switch off there but the new machine's laser enable is connected directly to a PCB. :\ 



Any ideas? Happy to post up a pic of the internals if that would help! Thanks :)





**"I Laser"**

---
---
**Scott Marshall** *February 14, 2016 09:12*

If it's a standard K40, you can just open the circuit that goes through the "LASER SWITCH" as it's marked on the front panel, ie, The Laser Enable circuit. Use a normally open flow switch, and it will prevent the laser from firing until flow is confirmed. Use a low restriction flow switch, some of the sliding weight type  (rotameter) can be pretty restrictive for the stock aquarium pump.

A Photohelic or similar Industrial solution is a pricey affair. (World Magnetics makes a nice little low pressure switch cheap, but they're hard to find, I got mine thru IBM as a contractor, they use them for tank level 'bubblers")



 The 2 buck solution:

I designed a zero restriction flow switch by installing  a cheap 2 dollar polyethylene float switch in a pill bottle.

Mount the float switch into the lid, and drill several large holes (3/8") around the top lip of the Pill Bottle. Make sure the holes are high enough to allow the float to lift with the bottle full. Run the return line straight into the side of it as low as possible (just drill a hole and press fit the tubing into it).



The water in the bottle lifts the float, closing the circuit. Drill an strategically sized drain hole in the bottom of the pill bottle, so that it drains down on low flow, but cascades out the top holes normally. 1/8 - 3/16" works with the stock K40 cooling gear.



 Mount the whole affair above the water line in your tank so the water cascades into the tank (Screw to the top lip of your 5 gal bucket or equal) Wire to a SAFE isolated low voltage circuit (NOT FOR SWITCHING LINE VOLTAGE - (or any non isolated loads)

Please don't electrocute yourself!!!



I plan on publishing my low buck safety and convenience setup for the K40 here in the next week or 2. Pictures and wiring diagrams.



 If you have a standard K40, it should have already had a ma meter on it, so I guess I'd need to know for sure exactly what model you have to be sure.﻿ Pics would help.


---
**I Laser** *February 15, 2016 00:03*

It's one of the 'digital' k40 designs, so no mA meter, here's a fuzzy pic (my phone camera won't focus anymore :\) [http://s17.postimg.org/v2k548w1r/k40.jpg](http://s17.postimg.org/v2k548w1r/k40.jpg)



I've got two types of flow sensor, one like this (coffee flow swtich) [http://bit.ly/1ThEu5N](http://bit.ly/1ThEu5N) and also one like this [http://bit.ly/1mBtQK5](http://bit.ly/1mBtQK5) 



The second one should be okay as I bought it based off a  'how to' youtube video. 



I do like you're '2 Buck' solution though, but will ask for more detail prior to attempting that!



I'll try and get my wife's phone and take some focused shots of the internals later.




---
**Scott Marshall** *February 15, 2016 04:50*

**+I Laser** 

Nice looking unit!



I've been meaning to get some pictures up. I still haven't posted the ones of "the building of the billet aluminum carriage". 



Think I may have misplaced my cmera, now that I think about it, I haven't seen it lately...





Guess I'm i the sale boat. Will post pix soon, I promise.


---
**I Laser** *February 15, 2016 05:53*

To be honest **+Scott Marshall** I wish I had of stuck to the analog design, it was much better engineered. This one is cramped and hard to work with, not to mention every mirror and the lens was damaged, the gantry out of whack and the tubing not connected to the tube correctly. Thank goodness the wiring seems okay!



Anyway once I get the flow switch on it should be okay. I'm quite sure it was you that helped me with connecting the mA meter, it works a charm. Still need to compare it against others, or find my multimeter, but seems to be working well.



Don't stress, I'm always behind schedule others thing come up and take precedence. But do look forward to the pics. ;)


---
**Scott Marshall** *February 16, 2016 04:56*

**+I Laser** 

I do remember that, glad you got her working. 



So, This is your 2nd Laser? Uh-huh.  Want to cop to any more hid in the basement?



You're as bad as I am, 1 is not enough.... already thinking I may sell this once I get it user friendly and build a larger C rail model maybe something that will cut 1/4" ply in 1 pass...

Then it will be "I just need to cut thin steel...



they are fun toys, less work than the Mill, and I haven't crashed any $35 end mills yet...

At a buck a square foot, the 1/8 ply allows a lot of playing for your money.

Lost out on the scrap bin of acrylic at Lowes. They threw it out before I could catch up with the manager. 

Next time...



Got to tinker some tonight, camera IS awol. Around here somewhere....



Scott


---
**I Laser** *February 16, 2016 08:30*

Only the two, but I do understand what you mean. It starts with one, then two, then a bigger one, then a more powerful one, then a.... lol



So I've managed to pry my wife's phone from her for long enough to take a few close up pics.



[http://s30.postimg.org/4ipt7heht/k40a.jpg](http://s30.postimg.org/4ipt7heht/k40a.jpg)



The PSU



[http://s22.postimg.org/lzjj5gash/k40b.jpg](http://s22.postimg.org/lzjj5gash/k40b.jpg)



Looking at the logic board



[http://s29.postimg.org/ok2jxtkvr/k40c.jpg](http://s29.postimg.org/ok2jxtkvr/k40c.jpg)



That pesky PCB that is the top panel. ;)



I'm pretty sure now that David Richards has the same machine and has connected a flow switch so will post a pic into that thread too. :)


---
**Scott Marshall** *February 17, 2016 04:33*

Sure is different. Is the panduit wire duct yours or theirs? 



Is it the same running gear as the 'regular' (V3) K40 but with fancier controls?



Software?



Scott


---
**I Laser** *February 17, 2016 05:27*

Yeah it's a standard k40 design with the pot swapped out for the LCD screen and power buttons. Software the same also ;)



The panduit is their doing, the cabling is neat, it was more the hardware side of it which was woeful.


---
**I Laser** *March 09, 2016 21:15*

**+Scott Marshall** how's the 'low buck safety and convenience setup' coming on? :)


---
**Scott Marshall** *March 09, 2016 22:19*

**+I Laser** 

Haven't forgot you. I have some parts coming, I also remembered I have a dozen or so multi function conversational displays (like you see on commercial CNC machinery, copiers etc) and I'm considering integrating one of those. Right now, I'm setting up a drop away base, and have mounted the motion control from the top of the enclosure, leaving the bottom completely open (I have it cantilevered from the right side) 

Anyway, between being sick, and waiting on parts, it's moving slower than planned (as usual). I Do have a flow chart drawn up and will post it here when I figure out how to.

Oh, yeah, I DID get the lighting, 12v supply and safety door switches installed. Dimmer for lights and speed for exhaust fan are mounted Still working out details on flowmeter, got one of those “coffee maker” flowswitches, and they're way too restrictive for the factory pump, which I tested and it's only putting out about 2LPM with the factory plumbing. I'm adding a heat exchanger (transmission cooler), flowmeter (visual paddle wheel type- ebay – THAT works well. It's coming together, I promise to share as soon as It's documentable.

 


---
**I Laser** *March 10, 2016 00:38*

Like my setup, it's never quite finished lol! 



All good, don't stress, if anything I was interested in your flow idea and if you had any pics but seems you've changed that so no worries. ;)


---
*Imported from [Google+](https://plus.google.com/103592788609016594341/posts/KpjZqnKfZxb) &mdash; content and formatting may not be reliable*
