---
layout: post
title: "One of the many awesome designs on made out of 1/8\" Birch (it measured more like 2.89mm, which is less tham 1/8\")"
date: December 17, 2015 21:48
category: "Object produced with laser"
author: "Ashley M. Kirchner [Norym]"
---
One of the many awesome designs on [makecnc.com](http://makecnc.com) made out of 1/8" Birch (it measured more like 2.89mm, which is less tham 1/8"). I ended up redrawing this completely because their DXF files have some issues like open paths, misalignments, edges that should be straight and are not, generally the usual imperfection you get when you export from one format to another. So I spent a couple of days in Inventor recreating the pieces, making them 3D, and actually assembling the thing virtually. Matching up all the pieces, adjusting what needed adjusting, before I exported all the faces out to Illustrator for final kerf adjustment, changing the fill of holes versus body panels, and then cut them out. Doing it all in 3D helped a great deal in fixing some of the alignment issues. In this picture, the outer layer of the wheels aren't attached yet, they're just sitting in the foreground.



I did make some small changes and there is still more to come. The bucket edge has been changed to a pointy edge instead of straight. Fixed some of the holes that would've been made with a router instead and overall got things a bit more snug.



Next step now is taking it all apart again (it's just held together with friction at this point), and start making minor adjustments. The wheels are too wobbly because the whole is a tad too large for the axle part. The 'hub plates' are a bit loose, an even smaller kerf offset will fix that. The bucket and arm are too loose and I would love to figure out a way to add some friction so it can actually be held in a particular position other than just laying down. The pivoting front needs to be made tighter. And then there's some engraving work. The 'AJ Construction' sign was a test (it's being gifted to a boy who's name is AJ). That will actually get engraved on the side panel under the window frame. And there are some other engravings I want to do.



All in all though, I'm really happy with this. I'm debating leaving the edges and light burn marks as is, not sand them off. It gives it character I think.

![images/9c88d8c78e7d5702d116fa74160c673b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9c88d8c78e7d5702d116fa74160c673b.jpeg)



**"Ashley M. Kirchner [Norym]"**

---
---
**Tony Schelts** *December 17, 2015 22:08*

Thats brilliant, it there a file avialable??


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 17, 2015 22:18*

I think the edges/light burn marks do indeed give it character. Personally I like that look, although some people may not.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 17, 2015 22:23*

I just had a thought regarding the bucket/arm. I am not 100% sure, but the rectangular section that passes through the little circle (at the top of the bucket arm), is that an axle? If so, couldn't you make the axle a touch wider so it only just fits into the holes in the body, therefore having a touch more friction to hold it in place? Or, even where the axle passes through the body, you could maybe change the shape of the hole to something that has slots for the bucket to sit in predefined positions. I will draw something & post for you for my idea.


---
**Ashley M. Kirchner [Norym]** *December 17, 2015 22:37*

**+Tony Schelts**, it's sold by [makecnc.com](http://makecnc.com).



**+Yuusuf Sallahuddin**, it is an axle yes, and I do plan on making the hole a tad smaller (as opposed to adjusting the axle which then requires me to also adjust the outer plates as well.) You can't make the hole too small though because too much friction on the axle and it will break due to the twisting motion. I also plan on making the outer plates fit tighter so they can be pressed tighter against the arm which would then provide more friction.



The idea of having notches did come to mind, however since I suspect this will be played with, I'm afraid having the notches will result in it breaking more easily (think about a little kid's patience trying to move something that's stuck.)


---
**Ashley M. Kirchner [Norym]** *December 17, 2015 22:38*

There are other bits and pieces that I need to address. For example, the plate onto which the front pivots originally did not have 'end stops' to prevent it from rotating too far that the wheels start to rub against the frame, so I added that to it. But the same could also be said about the bucket and bucket arm. Right now you can lift that arm all the way up and INTO the cab. Poor low wage worker would not survive that. <grin> So yeah, just some minor improvements.



And I'm considering adding a piece of acrylic for the back and front windows, leaving the sides open ... haven't decided yet.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 17, 2015 22:47*

It's a great project to tinker with at least. I have posted a pic for what I was thinking to allow positioning, however just noticed your comment about the kid might break it. That would very possibly be the case with a notched system like what I was thinking.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/9ANPtE6Jd4K) &mdash; content and formatting may not be reliable*
