---
layout: post
title: "Thanks to everyone, finally got K40 laser functioning"
date: April 09, 2016 21:38
category: "Hardware and Laser settings"
author: "Wayne Herndon"
---
Thanks to everyone, finally got K40 laser functioning. After about 4 hrs of use, the laser can barely mark a piece of foam board never mind cutting it. Cleaned mirrors/lens, centered mirrors, no help. Checked power supply. At full power it is giving 19.6 mA. Is this the problem?



Thanks





**"Wayne Herndon"**

---
---
**Jim Hatch** *April 09, 2016 22:07*

Make sure you have the material at the right focal length distance for your lens (might be too low) and if that's OK, flip your lens over.


---
**Joe Spanier** *April 09, 2016 22:29*

19.6 is way too much. You shouldn't push that tube much past 16mA. 



Is your water flowing ok?


---
**Scott Thorne** *April 09, 2016 22:40*

It's either the alignment or as **+Jim Hatch** has stated the focal length is way off. 


---
**Wayne Herndon** *April 09, 2016 22:45*

Thanks for the responses. 19.6 mA is what the PS ran at when I tested it. I usually run 10-15 mA. Focal length is 50.7 mm. According to Lightobject lens is supposed to be 50.8. Water is flowing fine. I flipped lens. no result. Night before this happened it was cutting 1/16 maple like paper at 5 mm/sec.


---
**Wayne Herndon** *April 09, 2016 22:50*

One other thing. In the past, when centering the mirrors, the beam would burn a relatively round hole in the paper. Now it is burning a "reverse L" when I align the mirrors.


---
**Joe Spanier** *April 09, 2016 22:51*

Is it burning that right out of the tube?


---
**Wayne Herndon** *April 09, 2016 23:00*

It's burning that at the cutter head. I will check right at the tube.


---
**Wayne Herndon** *April 09, 2016 23:02*

Right at the tube it is burning a round hole.


---
**Wayne Herndon** *April 09, 2016 23:03*

Cracked/scorched mirror? if so, what would be the cause?


---
**Scott Thorne** *April 09, 2016 23:06*

Dirty mirrors...when dirt builds up on them they tend to get burn marks on them. 


---
**Jim Hatch** *April 09, 2016 23:10*

**+Scott Thorne**​ Yep. The L shape says something is disrupting the beam. Dirt or a crack.


---
**Wayne Herndon** *April 09, 2016 23:17*

Holding both mirrors in my hand. Neither looks cracked or dirty. Later tonight I will super clean and reinstall them. Right now, SWMBO says that if I don't take her to dinner, she will stop functioning. :) Thanks for the help. Will keep you posted.


---
**Jim Hatch** *April 09, 2016 23:25*

**+Wayne Herndon**​ Check the lens too. Is there any water on it (that will mess it up)? You can see if there is an internal crack by repositioning the mirror so the beam hits off center. If the L spot is now a circle again that points to a lens flaw.


---
**HalfNormal** *April 10, 2016 03:13*

**+Wayne Herndon** You have 3 not 2 mirrors. 2 external and one in the head.


---
**Phil Willis** *April 10, 2016 13:00*

Standard K40 has a 50.8 mm or 2 inch focal length. Ideally the lens to the target should be that length. 


---
**Wayne Herndon** *April 11, 2016 03:34*

Finally back. All 3 mirrors have been cleaned. The beam is centered. The lens has been flipped and flipped again. Now it will not even mark foam board. I am running out of ideas except for the tube.


---
**Jim Hatch** *April 11, 2016 13:02*

**+Wayne Herndon**​ Did the reverse L shaped spot go back to a circle? Does it burn a hole in paper or cardboard placed in front of the final mirror? If not at 5ma, what about 10 or 15ma? 


---
**Jim Hatch** *April 11, 2016 13:08*

**+Wayne Herndon**​ also, you said it was a LightObjects lens. Are you sure it's a 50.7mm focal length? They have other lenses with different focal lengths. I'd put a ramped piece of material in there with the base as low as possible and the top edge up near maximum height that still clears the lens - maybe 10mm. Fire the laser at 10 or 15ma the whole length. If it does mark/cut somewhere in there, measure that to find out what the focal length is.


---
**Scott Thorne** *April 11, 2016 13:24*

**+Wayne Herndon**....make sure that the beam is not hitting the side of the head before it gets to the lens...I've had that problem before. 


---
**Wayne Herndon** *April 11, 2016 19:02*

And the prize goes to ...........Scott Thorne. This was a new 18 mm air assist head from Lightobject. I worked great the first night. I took the mirror out to clean it on Friday and hasn't worked since. Put the original head back in and it works like a champ. My brain is now mush and I can't think of a way to adjust the mirror in the LO head. I put a Post-it where the lens should be on the LO head and the beam isn't touching it at all.


---
**Jim Hatch** *April 11, 2016 19:25*

**+Wayne Herndon**​ Bingo. Same thing happened when I put in my LO. Mine was as hitting the inside of the nozzle and not getting out the bottom. Took the nozzle off and it hit the material about 2" in back of the head.



There are some decent instructions on how to align mirrors including how you need to adjust the head to get it to hit the mirror correctly. There's a document somewhere here hosts on it that was linked in a recent thread about aligning mirrors. If you don't have it it I'll dig it out.


---
**Wayne Herndon** *April 11, 2016 19:37*

What really had me confused is that Thursday I put the LO in and it worked fine until I took the mirror out and cleaned it. Guess I don't know my own strength. :) If you would dig the document out I would really appreciate it. 


---
**Scott Thorne** *April 11, 2016 20:21*

**+Wayne Herndon**...it's a matter of turning the head to get the mirror to shoot the beam at a 90 degree angle brother...it has a sweet spot.


---
**Wayne Herndon** *April 12, 2016 05:43*

All is good. Thanks for the help.


---
*Imported from [Google+](https://plus.google.com/113849066359765553098/posts/XN8exP75cfN) &mdash; content and formatting may not be reliable*
