---
layout: post
title: "I was having a problem blowing my fuse after installing a new 220V/10A DPST switch on my control panel"
date: March 03, 2017 21:34
category: "Repository and designs"
author: "Madyn3D CNC, LLC"
---
I was having a problem blowing my fuse after installing a new 220V/10A DPST switch on my control panel. I ended up having a faulty switch but it cost me a lot of aggravation coming to that conclusion. During the troubleshooting, I had to make myself a quick diagram for reference, because I learned my lesson a long time ago when it comes to trusting color codes and wiring standards from China. I went through checking continuity with the DMM while making this, just to be sure. I hope this helps someone in the future.  

![images/360def8dc63faeff3bee5e2d7ff16642.png](https://gitlab.com/funinthefalls/k40/raw/master/images/360def8dc63faeff3bee5e2d7ff16642.png)



**"Madyn3D CNC, LLC"**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 03, 2017 21:44*

Wiring standards and China in the same sentence? Sober up my friend. :)


---
**Madyn3D CNC, LLC** *March 03, 2017 22:09*

well, at least they wear gloves.... LOL!

![images/35a83b0a06a1e4f199a132fbbdab073b.png](https://gitlab.com/funinthefalls/k40/raw/master/images/35a83b0a06a1e4f199a132fbbdab073b.png)


---
**Mark Brown** *March 03, 2017 22:16*

On mine there's a cooling fan (and two power outlets) wired parallel between the "red" wires.  Which confused me for a moment because my multimeter was picking up 150Ω between the two.



Did you add the fuse, or is there one in there somewhere that I didn't notice?


---
**Don Kleinschnitz Jr.** *March 03, 2017 22:25*

**+Twelve Foot** my AC plug has a fuse inside of it.


---
**Madyn3D CNC, LLC** *March 04, 2017 00:19*

The fuse in the back of mine came stock.. I'm glad it's there. A cheap $3.00 switch off ebay almost cost me another hundred bucks. I just fished around on digikey for a good one, should be here monday :) 


---
*Imported from [Google+](https://plus.google.com/+vapetech/posts/XEkKmCb8AmC) &mdash; content and formatting may not be reliable*
