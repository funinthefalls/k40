---
layout: post
title: "Focal Lens Needs Cleaning Every Hour Of Cutting.Even with 3 water traps my focal lens gets water on it .if you look closely at the 2nd photo you will see that it do not shine like the cleaned lens in photo 3.To see the water"
date: March 31, 2016 21:40
category: "Discussion"
author: "Phillip Conroy"
---
Focal Lens Needs Cleaning Every Hour Of Cutting.Even with 3 water traps my focal lens gets water on it .if you look closely at the 2nd photo you will see that it do not shine like the cleaned lens in photo 3.To see the water on the lens you have to angle the lens so that the light shines on it at just the right angle.The focal lens shrinks the laser beam from about 5mm round to 0.1mm round so its job is very important,any scattering and you loose cutting power.I have ordered disposable motor guard filters that i hope will prevent this water getting to my lens.on the plus side with my air assist laser Head it only takes me 5 seconds to removed the focal lens so cleaning the lens in no real [hassle.ps](http://hassle.ps) my focal lens is a 20mm one



![images/fbd01273be80053fb675604894b66428.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fbd01273be80053fb675604894b66428.jpeg)
![images/a354d5a65268bb4551e709e67044d12a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a354d5a65268bb4551e709e67044d12a.jpeg)
![images/fda12090d13cfab136a0cd25e6b11a07.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fda12090d13cfab136a0cd25e6b11a07.jpeg)
![images/d6830b048cac92deae5d032b287942fa.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d6830b048cac92deae5d032b287942fa.jpeg)

**"Phillip Conroy"**

---
---
**Josh Rhodes** *March 31, 2016 23:44*

I would start thinking about how air flows. At **+Mobile Makerspace**​ we have a different kind of laser but it never has this kinda problem. I mean maybe eventually. 



I suspect our Venus laser was designed to have fresh air sucks from outside hitting the mirrors and polluted air runs out the exhaust and never lingers on glass bits. 


---
**Phillip Conroy** *April 01, 2016 01:15*

it is so bad if cutting 3mm mdf and any of the lines are 1mm thick i have to clean focal lens every 20 minutes ,i have on order a motor guard filter ,it can not get here soon enough.

car spray painters have this problem as well ,it shows up as spots in the finished paint.They use expensive air dryers $1000


---
**Scott Marshall** *April 01, 2016 01:22*

If you are seeing that much water from shop air, your system has some huge problems. it's not hard to get good clean dry air in most climates (the closer to the equator the higher the average temp/humidity).



 If your tank is large enough, properly drained (each day minimum) and is sized properly, a simple Filter/regulator and disposable painting filter before your air head should do the job quite well.



 If your compressor is undersized, and runs more than 30% of the time the air won't cool in the tank, and the temp will drop at the nozzle. Another possible problem could be if you are not regulating the line pressure down to 10-15 PSI before using a flow control ( the large pressure drop causes condensation). 



You want the air to have cooled before your filters, and any large pressure drops to occur before your filters, Cooling causes condensation, and pressure drops cause cooling. If you do indeed have to live with them, make sure they happen before your water traps.



To give you any specific advice will require details on your air system.



Hope there's something there you can use. You can also check the Auto painting forums/sites for detailed info, painters hate wet air more than anybody. 



Scott


---
**Phillip Conroy** *April 01, 2016 01:32*

thanks for that ,i have been thinking of adding cooling to the air compressor between the pump and tank -running a coil of copper pipe and a fan to cool the air before it gets to the tank.

my problem is that i run the cutter for the full 3 hours and i am pushing the limits of the k40 laser cutter cutting 1mm wide mdf.


---
**Scott Marshall** *April 01, 2016 01:40*

Long run times push your air compressor so that the hot air makes it to the nozzle, which will cause your issue for sure.

 A cooler of sorts may help, (keep it close to the compressor) but if you are running your compressor that hard, the real problem is that it's too small (or tankless) thus running too much, carrying heat through to the outlet.



An air assist head doesn't use much air, you may want to consider a larger compressor. A 1 or 2 hp tank type compressor will do the job easily and not cycle too frequently. (you'll want it remotly located, they are loud, but that's a plus for eliminating water anyway)



Some people are using pumps and airbrush compressors, which are tankless and would be a water problem for sure in a humid climate.



Scott


---
**Josh Rhodes** *April 01, 2016 02:03*

**+Scott Marshall**​​ .. 

I don't mean to be a naysayer..



But **+Mobile Makerspace**​​ operates in the dampest, wettest city in the U.S. 



Yet our laser uses a small airbrush style compressor..﻿


---
**Scott Marshall** *April 01, 2016 02:16*

I'm not discounting the usefulness of them, but I spent my life in industry and have only industrial equipment as a reference, and an airbrush compressor isn't going to live long with 3 hour runs.



Diaphram compressors by design wear quickly, generate dirt and even the better ones of tilting piston design and don't tolerate 3 hour runs (for long).



They may work well for light duty work, but for professional/industrial use, a few hundred dollars more for a decent quality piston compressor, tank and delivery system is cheap insurance against it going down in the middle of a run.

You also have the advantage of a useful shop upgrade instead of a dedicated piece of equipment. Good shop air is basic to most production enviroments, and I've build dozens of systems up to 50hp. I do have less experience with airbrush compressors, but have repaired a few for friends and wasn't impressed with the design or quality.


---
**Phillip Conroy** *April 02, 2016 06:02*

i am getting 99.999 % of the water out of the line ,however the last little bit is a bugger ,when  turn on air assist for a split second i can see water vaper in the air water traps water bowel,that i think is getting past the 2 water traps.

on larger lasers i have seen the air assist as a small hose next to the laser head,so air does not go anywhere near the lasers lens


---
**Scott Marshall** *April 02, 2016 09:25*

If it's visible, it means your air is saturated (holding all the water it can) just like fog, and will condense on the lens if the lens is even 1 degree cooler than the air.

Getting the air away from the lens would leave the lens unprotected from smoke, ash and floating resins, which pollute the lens just as bad if not worse. Even when you don't NEED air assist, it's normal practice to run a low flow, just enough to keep the air pushing the smoke away from the lens.



What's your air system made up of? It may be a simple fix to "get the water out".


---
**Phillip Conroy** *April 02, 2016 09:49*

It is only visable for a split fraction of a second in the water trap,air asisst is made up of 2hp 40 liter cheap air compressor outside then 6 meter rubber air line a water trap/regulator set to .4 bar than another regulator /water trap set to .2 bar,than 1/8 inch platic hose of 1 meter,than the air assist [http://www.ebay.com.au/itm/252273200569?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com.au/itm/252273200569?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Phillip Conroy** *April 02, 2016 10:00*

[https://www.google.com.au/imgres?imgurl=http://thingiverse-production-new.s3.amazonaws.com/renders/7c/75/0e/27/eb/IMG_20151210_110717_preview_featured.jpg&imgrefurl=http://www.thingiverse.com/jimustanguitar/collections/laser-cutter&h=472&w=628&tbnid=Jjz4dTgf-cQekM:&docid=f19lSGo35_SW1M&ei=FJf_VuDAFcbNmwX5p5SYBA&tbm=isch&client=ms-opera-mobile&ved=0ahUKEwjgmqn41-_LAhXG5qYKHfkTBUMQMwheKDgwOA](https://www.google.com.au/imgres?imgurl=http://thingiverse-production-new.s3.amazonaws.com/renders/7c/75/0e/27/eb/IMG_20151210_110717_preview_featured.jpg&imgrefurl=http://www.thingiverse.com/jimustanguitar/collections/laser-cutter&h=472&w=628&tbnid=Jjz4dTgf-cQekM:&docid=f19lSGo35_SW1M&ei=FJf_VuDAFcbNmwX5p5SYBA&tbm=isch&client=ms-opera-mobile&ved=0ahUKEwjgmqn41-_LAhXG5qYKHfkTBUMQMwheKDgwOA)

I had this type of air assist nozzel and had no problens as the air never goes near the lens all my problems started when i upgraded


---
**Scott Marshall** *April 02, 2016 10:38*

That sounds like a pretty decent system. The double regulators are redundant, but shouldn't be a problem. (I'm guessing the 1st one in part of the compressor) How long are the cycles and run time?

>.2 bar is a lot of air if you don't have a flow control valve after it to limit flow. 

I'm thinking you're moving way more volume than needed. A good air assist flow is minimal, about what you could provide by blowing in the 1/8" tube, not much more.

If you are using too much air, and live in a very humid/warm area, that could do it. If you can put a vise-grip or clamp  on the 1/8 tube  to limit the flow (or if the last regulator will go way down (some won't) try setting the flow to where it just blows away the smoke, and no more. See if that helps.


---
**Phillip Conroy** *April 02, 2016 16:41*

Both regulators/water traps are at thr laer cutter,compressor run tumes are-1nin running 4 min rest.first watertrap captures 30 percent of the water and 2nd water trap captures 69.999.the amount of water that covers the laser fiocal lens is 1 water drop per week,a very small amount.


---
**Scott Marshall** *April 02, 2016 18:57*

You got me. That ought to be OK. Sure it's water?, not oil from the compressor?



 Unless you live in a rainforest, Your air shouldn't be that wet. I did a system in PuertoRico for Zimmer years ago that acted like that, and we had to resort to a refrigerated dryer and dessicant bed.



There's a painters trick yo may want to try. Fill your filter bowel with dessicant beads.



 You can harvest silca gel packs from medicine bottles or electronics packaging in a pinch, and use the beads. 



Link:[http://www.ebay.com/itm/1-Quart-Jug-Replacement-Desiccant-Blue-Indicating-Silica-Gel-Bead-2-LBS-/271869636578?hash=item3f4cb16fe2:g:j44AAOSwpDdVVSX7](http://www.ebay.com/itm/1-Quart-Jug-Replacement-Desiccant-Blue-Indicating-Silica-Gel-Bead-2-LBS-/271869636578?hash=item3f4cb16fe2:g:j44AAOSwpDdVVSX7)



You can dry the beads in the oven and reuse. 


---
**Phillip Conroy** *April 02, 2016 21:55*

Thanks for the painters tip will try,it is water not oil,been trying to think of a cheap way to setup desiccant container after the water traps,maybe use a under sink water filter housing and replace the filter with the desiccant,or an old franz car toilet roll oil filter.


---
**Scott Marshall** *April 02, 2016 23:10*

[http://www.ebay.com/itm/10x2-5-Clear-Filter-Housing-3-4-NPT-W-Brass-Insert-Whole-House-RO-/361337105514?hash=item54215eac6a:g:tlMAAOSw9r1V8Yul](http://www.ebay.com/itm/10x2-5-Clear-Filter-Housing-3-4-NPT-W-Brass-Insert-Whole-House-RO-/361337105514?hash=item54215eac6a:g:tlMAAOSw9r1V8Yul)



Here's a residential water filter housing. (10")



Inside on the outlet side (center) you will find a male nipple intended to engage the filter element. It fits well with a 1/2" schedule 40 pvc coupling. Use a length of the pipe to make a standpipe which nearly reaches the bottom. Cap the pipe on the bottom end and drill lots of 3/32" holes in the bottom 3". (you can also use larger holes and tywrap on nylon stocking material)

Another option is take a wound element and unwind all but a couple layers. Like this one:[http://www.ebay.com/itm/Watts-SF50-978-String-Wound-Sediment-Filter-10-x-2-5-50-micron-/252088505483?hash=item3ab1a5548b:g:5nUAAOSwuTxV8yOf](http://www.ebay.com/itm/Watts-SF50-978-String-Wound-Sediment-Filter-10-x-2-5-50-micron-/252088505483?hash=item3ab1a5548b:g:5nUAAOSwuTxV8yOf)



 Fill it up with deccicant beads and you have a nice dryer. When the beads turn red, spread them on a cookie sheet and bake @ 240F until blue again. 



Use it only on the low pressure side of the regulator, they can handle the pressure, but if mechanically damaged can fragement if under high pressure. (PVC pipe is a no-no in a factory for the same reason) Not acceptable for code use,(factory)but for a private shop, they work great.



If you want less work, just put in a 1um spun polypro filter element and pour beads around the outside, easier but it cuts the bead volume a lot.. I've made quite a few through the years.



All up you can do this with a load of beads for around forty bucks US.



A copper tubing cooler can only help as well. A simple 50" roll of 3/8 copper would do it, but has gotten darned expensive lately. 15' in a cooler of cold water will do it too.



 Old automotive air conditioning condensers work great and are nearly free in a scrapyard (scrap price for aluminum usually) just rinse the oil out with some solvent, cut off the A/C connectors and put a couple of compression fittings on the ends. Throw a fan on it and you're in business. Put it in an old fridge and you have a low buck referigerated dryer....  Geez, I love low buck engineering!


---
**Phillip Conroy** *April 03, 2016 02:56*

Thank you sound good


---
**Scott Marshall** *April 03, 2016 03:25*

Hope you get it working. tough problem.


---
**Phillip Conroy** *April 03, 2016 07:40*

Made a tank for deccicat beads out of 3 feet of 1 inch round pvc pipe have installed on laser cutter at the back on a 45deg angle,total cost was $20 including the beads so far it is working only cutvfor 1 hour so far and focal lens is still clean============================================i still have the 2 water traps/regulators in line before the pvc pipe as they still need water emptying every 3 hours of cutting .



===== i am so happy it looks like i have beat the water problem, because the water traps are removing 99.999999% of the water i estamate that i will only have to change the deccicant beads in the pvc tank every 6 months ,time will tell tho=====================+==+==========


---
**Phillip Conroy** *April 03, 2016 13:25*

Thank you all the people that added comemts ,it all helped


---
**Scott Marshall** *April 03, 2016 13:41*

Nice work!


---
**Phillip Conroy** *April 05, 2016 00:57*

Used the laser cutter cutting 3mm mdf for 3 hours now and lens is still clean i am soo happy ,will start a new discussion so that i can add photos 


---
*Imported from [Google+](https://plus.google.com/113759618124062050643/posts/e1qu1tywsTc) &mdash; content and formatting may not be reliable*
