---
layout: post
title: "He there, recently i am thinking about upgrading my control board"
date: January 08, 2017 18:42
category: "Modification"
author: "Krist\u00f3f Gilicze"
---
He there,  recently i am thinking about upgrading my control board. I was looking at the budget solutions. Woud you guys recommend a GRBL based board or something like a Ramps(marlin)?. I think i would use LaserWeb. 





**"Krist\u00f3f Gilicze"**

---
---
**Kristóf Gilicze** *January 08, 2017 21:16*

Thanks :) And one more thing! Do i need this middleman board, does it actually does anything or it just makes the wiring easier?


---
**K** *January 09, 2017 02:55*

One of the cheapest GRBL options out there is the CNCShield V3. It's a nice little board. Just be aware that it was designed with the pinouts for GRBL 0.8 so you have to plug Spindle Enable into the Z+ spot (I think... double check that..) but the point is that the pins changed ever so slightly with GRBL 0.9 and on. 


---
**Paul de Groot** *January 09, 2017 05:37*

I actually developed a driver shield for an Arduino uno r4 aka 328pb avr controller. This allows high res engraving. Will post some pictures when i get home 😊 grbl 1.1e has a laser mode to prevent burned corners when cutting. Sofar I'm very happy with it. 


---
*Imported from [Google+](https://plus.google.com/107969900550483335330/posts/bVEZJin8jKw) &mdash; content and formatting may not be reliable*
