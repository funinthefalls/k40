---
layout: post
title: "Simple method I used to make a single pen display"
date: November 24, 2018 17:10
category: "Object produced with laser"
author: "Ariel Yahni (UniKpty)"
---
Simple method I used to make a single pen display


{% include youtubePlayer.html id="ek9eFORMcnM" %}
[https://youtu.be/ek9eFORMcnM](https://youtu.be/ek9eFORMcnM)





**"Ariel Yahni (UniKpty)"**

---
---
**James Rivera** *November 24, 2018 21:47*

Cool! I could see doing something like this to make a tablet stand, too.


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/i6hALtwrUx5) &mdash; content and formatting may not be reliable*
