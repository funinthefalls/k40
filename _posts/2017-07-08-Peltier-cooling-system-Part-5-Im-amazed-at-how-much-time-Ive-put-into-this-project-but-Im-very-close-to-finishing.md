---
layout: post
title: "Peltier cooling system Part 5. Im amazed at how much time Ive put into this project but Im very close to finishing"
date: July 08, 2017 18:54
category: "Modification"
author: "Steve Clark"
---
Peltier cooling system Part 5.



I’m amazed at how much time I’ve put into this project  but I’m very close to finishing.



Since I‘m a amateur at the electrical end,  I approached it from the simplest way I knew. Others with a better background may have done it more efficiently and differently.  I moved everything I could out of the laser to an the electrical box I made from  the metal key box I got from Harbor Freight (See post 3 for link). Here is what is in it.



I have four power supplies:



1)	 Two 12 volt 30amp PS for the Peltiers. I could not locate a 50 to 60 amp 12 volt and wanted one  or two that were 20 or 30% over the amps needed. This for a couple of reasons, they don’t have to work so hard, plus can be used for other devices and the price difference gave me a fan in each for cooling.



2)	One 24 volt 5 amps for the Z axis and a A or B (Rotary axis)  I have a C3D so a future project will be building a rotary.



3)	The last is I have a little 12v to 3v step down that I’m using for my laser crosshairs. (I just moved this to my control panel above the laser.) 





I needed  a place to mount two relays with their heat sinks that go with the Peltier control.





An area for the terminal blocks and fuses.





Wiring for power in and out for the:



1.	12v DC

a.	Panel lights 

b.	 A little 3v step down transformer  for laser diode crosshair 

c.	Ambient temperature gauge

d.	The JLD812 (DC 12v) Dual Display F and C PID Temperature Controller

e.	Peltier cooling fans

f.	Peltiers (360 watts, I've rated them at a functional 240 watts)

g.	Electrical panel cooling fan

 

2.	24v DC

a.	Z axis table power

3.	 K40 PS 120v

4.	120v plugs (power on with main)

a.	Blower vent (Also has a variable speed switch)

b.	Water pump



When I’m finished with this project I’ll do a wiring diagram and post it.



The reserve tank and housing is built out of PVC, plywood and Popular. That tank box is lined with Styrofoam. Where the tubes come out of the box another there is another housing which contains the Peltiers, tubing connections, and the a thermocouple that attaches to the insulated tank box. 



The inside of this last box is aluminum taped as where there may be condensation so it can drain out without soaking into the wood. Closed foam insulation will be added along with insulation around the water tubing coming out of the whole unit to the laser tube.



I’m real close to finishing. The electrical box has been mounted and then powered up to test the wiring and voltages. I’ve fit checked everything… so now I’m figuring out how to run the wires out from the Peltiers and the cooling fans to get power from terminal blocks mounted in a small electrical box on the cooler housing.  



A note here… when the main power is turned on the Peltiers fans will also turn on and stay on.  The Peltiers themselves are regulated through the Temperature Controller  and relays. 



When I’m finished with this I’ll do a wiring diagram and post it.





![images/40b9670688024a4921948409e04dc134.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40b9670688024a4921948409e04dc134.jpeg)
![images/f1460448c069498f2e1f2fea36981219.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1460448c069498f2e1f2fea36981219.jpeg)
![images/f63a51bea9985ee81bef0626339470c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f63a51bea9985ee81bef0626339470c8.jpeg)
![images/30c500e27dc70dc4bdba0bf0e2d66bb8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/30c500e27dc70dc4bdba0bf0e2d66bb8.jpeg)
![images/387e3052f223943b23722ba724dbae7d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/387e3052f223943b23722ba724dbae7d.jpeg)
![images/f9681ab1bef4e567646fe0a287c34ac1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f9681ab1bef4e567646fe0a287c34ac1.jpeg)
![images/3b1c5511994750b070c7c9d47f51ac80.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3b1c5511994750b070c7c9d47f51ac80.jpeg)

**"Steve Clark"**

---
---
**Don Kleinschnitz Jr.** *July 08, 2017 19:30*

I am not a peltier expert. How are you controlling the current to these. I vaguely remember that they need to be current driven to get max efficiency. 

Something like applying full voltage creates internal heating and loss of efficiency.


---
**Steve Clark** *July 08, 2017 19:54*

Interesting comment. I was under the impression that they were self regulating and that they are on at the full wattage rated until turned off by the Temperature Controller. I have not read anything that talks about needing to control the current. 



I did read something about making sure to rid of the heat produce on the hot side with radiators and fans ect or you would have cooling problems. Other than that no writings about voltage or current regulation.



You have me wondering. Got any ideas on how I would  know? Sigh…I thought I had this all figured out.


---
**Steve Clark** *July 08, 2017 23:47*

**+Don Kleinschnitz** 

Spent  the day looking into this with my poor electrical knowledge here is what I’ve learned… right or wrong.





A 60watt 12v DC Peltier assuming  the 12 volts (maximum) is available will pull up to its rated current. You can just turn them on and off to control your coolant temperature. However it is better to control them by voltage. (Ohm’s Law?)



PWM is one option  but there needs to be some precautions to be observed… high enough frequency to minimize thermal stresses in the TE and EMI problems require shielding to the power wiring. (see page 10 para 41 of attachment) (page 9 para 40 of attachment is also of interest)





a linear drive system, done by placing a bipolar junction transistor (BJT) in series with the TE. (see page 11 para 43 of attachment)



Just turning it on and off with the temperature controller (see page 11 para 43 of attachment)





Skip to page 8 or 9 to start reading…



[tellurex.com - www.tellurex.com/media/uploads/PDFs/peltier-faq.pdf](https://www.tellurex.com/media/uploads/PDFs/peltier-faq.pdf)



I’d like to regulate the voltage but not sure I’m smart enough to come up with the right method. 




---
**Don Kleinschnitz Jr.** *July 09, 2017 05:13*

#K40cooling



Some links I have been treading through....



I keep coming up with lots of coolers and exchangers (6-10) needed to handle 100F ambient down to acceptable cooling temp?



I wonder what we don't know as these TEC controllers are expensive.



[https://www.meerstetter.ch/compendium/pwm-vs-direct-current?gclid=CjwKEAjwhYLLBRDIjoCu0te4niASJAC0V4QPPt8SpnQRN0r_I611ffKQTyGlMeiPnyrifpOQ_mSxdhoCqhfw_wcB](https://www.meerstetter.ch/compendium/pwm-vs-direct-current?gclid=CjwKEAjwhYLLBRDIjoCu0te4niASJAC0V4QPPt8SpnQRN0r_I611ffKQTyGlMeiPnyrifpOQ_mSxdhoCqhfw_wcB)





[https://www.meerstetter.ch/compendium/tec-peltier-element-design-guide](https://www.meerstetter.ch/compendium/tec-peltier-element-design-guide)



[https://tetech.com/cooling-assembly-and-heat-load-calculator/](https://tetech.com/cooling-assembly-and-heat-load-calculator/)



[maximintegrated.com - MAX1978 Integrated Temperature Controllers for Peltier Modules - Maxim](https://www.maximintegrated.com/en/products/power/switching-regulators/MAX1978.html)


---
**Steve Clark** *July 09, 2017 17:14*

Yes those controllers are very expensive but I'm wondering if our need to control is so basic we don't need to have the extent of control and sophistication they are selling. 



The things I've read so far says we need to have available Peltier capacities greater than required for our system and to do this efficiently, you need to keep the operating current somewhere within 20 to 60% of the Imax of the Peltier elements . 



So IMO (such that it is) there is a need to be able to adjust the PS voltage/current down. Take a look at this video if you haven't already. (Don, one of your links turns out to be the source this same video)




{% include youtubePlayer.html id="bp3Ox7H4dwA" %}
[youtube.com - 1.2.1 Thermoelectric Cooling Design](https://www.youtube.com/watch?v=bp3Ox7H4dwA)



At this point if I can get something to control the voltage I'm willing to turn mine on. 




---
**Don Kleinschnitz Jr.** *July 09, 2017 18:18*

Certainly turn it on....only way to learn :).



If I understood correctly Imax is the ideal operating point not an absolute max so you want to run at Imax not above not below?



Try on/off as it's cheap and maybe a wider temp range and some inneficiency won't matter. 



If that's not good I would try PWM control, there are a few cheap drivers out there used for CNC spindles and the like which could be combined with a temp controller that also monitors voltage...or something like that.


---
**Joe Alexander** *December 27, 2017 06:36*

heres a question I haven't seen yet: where are you locating the temp probe for the TEC controller: in contact with the peltier itself or in the reservoir tank? Been toying with 1 or several 12v6a peltiers for a K40 cooling circuit.


---
**Steve Clark** *December 27, 2017 17:29*

**+Joe Alexander**  I have two three Peltier banks in series 60watt units so 360 watts. That is about 240 watts functional. I was in a hurry at the time so I just ordered two of these shown below. 



Oh, almost forgot my temp probe is after the tube. I went back and forth on this finally decide to put it there. I never have seen more than one or two degree variance using the JLD812 Temp controller. 



[ebay.com - Details about Semiconductor Refrigeration Radiator Thermoelectric Peltier Water Cooling Device](https://www.ebay.com/itm/Semiconductor-Refrigeration-Radiator-Thermoelectric-Peltier-Water-Cooling-Device/322618169332?hash=item4b1d8a97f4:g:h7gAAOSwuaFZeVUt)


---
**Joe Alexander** *December 27, 2017 19:23*

just curious on the effect on power usage were the temp probe closer to the peltier plate, thus hopefully preventing it from running hotter than needed? i have a small circuit i set up and uses a closed loop cpu water cooler block with radiator for the hot side and it seems to work ok.


---
*Imported from [Google+](https://plus.google.com/102499375157076031052/posts/j9R6Z6xL19d) &mdash; content and formatting may not be reliable*
