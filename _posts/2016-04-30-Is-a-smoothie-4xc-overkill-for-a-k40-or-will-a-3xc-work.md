---
layout: post
title: "Is a smoothie 4xc overkill for a k40 or will a 3xc work?"
date: April 30, 2016 01:32
category: "Hardware and Laser settings"
author: "Alex Krause"
---
Is a smoothie 4xc overkill for a k40 or will a 3xc work?





**"Alex Krause"**

---
---
**Jim Hatch** *April 30, 2016 01:53*

A 3XC is enough but may be hard to find. Seems like the 4 & 5s are the ones that are available now. The 4 or 5 will work, just has extra control capacity (for additional stepper motors, etc).


---
**Ray Kholodovsky (Cohesion3D)** *April 30, 2016 01:59*

The 4 has the added benefit of the Ethernet being onboard. 


---
*Imported from [Google+](https://plus.google.com/109807573626040317972/posts/MJGTAeqqTLG) &mdash; content and formatting may not be reliable*
