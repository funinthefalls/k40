---
layout: post
title: "Hey guys, I'm a beginner. Quite confused with this: What is actually the difference between K40 and K50 in term of cutting?"
date: August 07, 2018 15:25
category: "Discussion"
author: "Nurul Rafatin"
---
Hey guys,

I'm a beginner. Quite confused with this: What is actually the difference between K40 and K50 in term of cutting? I wanted to cut wood and acrylic as well, so which one is better option. Does K40 good enough. 



Thanks 





**"Nurul Rafatin"**

---
---
**James Rivera** *August 08, 2018 02:37*

K40 vs. K50 is mostly just power level (40 watts vs. 50 watts). The K40 will cut 3mm plywood in a single pass, and I believe it can also cut 3mm acrylic in a single pass (I haven't tried that yet).


---
**Nurul Rafatin** *August 08, 2018 07:22*

**+James Rivera** thank you for your response! 


---
**Ned Hill** *August 08, 2018 23:07*

You get more power with a K50 over a K40, but also typically a larger cutting area since tube length scales directly with power.  I've cut upto 1/4" (6mm) ply in one pass with my K40.   Just depends on what you want to do as far as what thickness of materials you want to cut and how big of a work area do you need.  If you can afford the K50 you can never go wrong with more power and more work area IMHO.


---
*Imported from [Google+](https://plus.google.com/105448582291063021512/posts/ZnQudC5DCUD) &mdash; content and formatting may not be reliable*
