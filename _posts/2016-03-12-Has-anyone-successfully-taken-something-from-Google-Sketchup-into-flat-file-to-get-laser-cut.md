---
layout: post
title: "Has anyone successfully taken something from Google Sketchup into flat file to get laser cut?"
date: March 12, 2016 08:07
category: "Software"
author: "Pete OConnell"
---
Has anyone successfully taken something from Google Sketchup into flat file to get laser cut? I've got a design in sketch up that im wanting to laser cut.





**"Pete OConnell"**

---
---
**HP Persson** *March 12, 2016 10:25*

There is some addons for sketchup, like "flattery" to make a 3D model into flat pieces.

Then export it as DXF and into Corel or whatever software you are using.


---
**HP Persson** *March 12, 2016 12:02*

**+Peter van der Walt** exactly that plugin I use too. Works awesome. Sketchup default export is not good. 


---
**Pete OConnell** *March 19, 2016 10:02*

I've tried using this plugin and it adds in random lines - any ideas? The face I'm trying to export to STL is [https://drive.google.com/file/d/0Bx6BqUBR4VS9SHFwZ2ttSG0zLUk/view?usp=sharing](https://drive.google.com/file/d/0Bx6BqUBR4VS9SHFwZ2ttSG0zLUk/view?usp=sharing)


---
**Pete OConnell** *March 19, 2016 10:05*

in fact ignore me, if i had followed what you said and did polyline dxf and it works :P


---
*Imported from [Google+](https://plus.google.com/115354437352450054826/posts/Hh7ixzrAoCe) &mdash; content and formatting may not be reliable*
