---
layout: post
title: "Today I installed the Cohesion 3D pcb in my K40 lasercutter"
date: April 21, 2017 13:39
category: "Smoothieboard Modification"
author: "Andy Knuts"
---
Today I installed the Cohesion 3D pcb in my K40 lasercutter. Now I'm having horizontal banding issues while engraving. Anyone has an idea what could be the cause? 

Have a look at this picture. Just a test on cardboard. On the right it was engraved with the original K40 pcb + LaserDRW and on the left Cohesion 3D pcb + Laserweb 4.. As you can see on the left side, there's a banding issue that I can't seem to figure out...

![images/d74b310aa851f8767abf638c8119f106.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d74b310aa851f8767abf638c8119f106.jpeg)



**"Andy Knuts"**

---
---
**Claudio Prezzi** *April 21, 2017 14:39*

Check settings/machine/beam diameter. That value ist copied as default to every laser raster operation and is used as the distance between two lines.


---
**Mark Brown** *April 21, 2017 18:33*

Or (shot in the dark) is there an option for motor steps per mm? 


---
**Andy Shilling** *April 21, 2017 20:17*

**+Andy Knuts** Yes that is the laser diameter, I had the same issue with mine, just change the laser diameter in settings and you should be good to go.


---
**John Revill** *May 11, 2017 01:57*

I don't have my Cohesion 3D board yet, but it uses the same drivers as my RAMPS board. I had the same problems and found it was the stepper current setting on the driver was incorrect. I had to manually adjust the trim pot until the banding disappeared. That's only if your laser is moving from left to right in your photo. If it's top to bottom, then it's something else.


---
*Imported from [Google+](https://plus.google.com/101382109936100724325/posts/Jfj9He8VZMh) &mdash; content and formatting may not be reliable*
