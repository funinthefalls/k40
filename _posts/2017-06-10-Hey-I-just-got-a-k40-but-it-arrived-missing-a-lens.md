---
layout: post
title: "Hey, I just got a k40 but it arrived missing a lens"
date: June 10, 2017 21:29
category: "Original software and hardware issues"
author: "Nathan Thomas"
---
Hey,



I just got a k40 but it arrived missing a lens. Its the one on the stock laser head.

 Can anyone tell me where I can get a replacement? Also anything else I need to know like what size, length etc. Just want to make sure I don't get the wrong thing.

I've emailed the eBay seller but I don't want to wait weeks for the part to arrive. 



Thanks,





**"Nathan Thomas"**

---
---
**Joe Alexander** *June 10, 2017 21:45*

stock focus lens is 18mm i believe, can get off ebay or [lightobject.com/](http://lightobject.com/) most eventually upsize to the 20mm/air assist nozzle. Helps tremendously with cutting.


---
**Nathan Thomas** *June 10, 2017 21:49*

**+Joe Alexander** I'm sorry I've been trying to post a pic but couldn't until now. I think it's the mirror that I'm missing. 

Would you happen to have the size etc for that as well?

![images/e115fd9366c6ce61fb23c9db3265961d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e115fd9366c6ce61fb23c9db3265961d.jpeg)


---
**Joe Alexander** *June 10, 2017 22:17*

ahh that is a 18mm reflection mirror if i recall correctly. I replaced my stock mirrors pretty quickly.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2017 22:25*

Isn't the stock lens smaller than 18mm? I recall that when people purchased the LightObject head that they had to get an 18mm lens also (or some spacer) due to the fact the stock lens is not big enough. If my guesstimation is correct, I think the stock lens is about 12mm in size. Mirror I would have guesstimated is 20mm... You can check one of the other 2 mirrors (located on the left Y carriage section or up the back near the tube). Only issue is it's all going to be out of alignment now & require aligning once you replace everything.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 10, 2017 22:27*

Oh, you can try [lightobject.com](http://lightobject.com) for parts too. Might be quicker than waiting for ebay seller.

[lightobject.com - LightObject](http://www.lightobject.com/)


---
**Joe Alexander** *June 10, 2017 23:06*

your probably right **+Yuusuf Sallahuddin**, i think i used the stock head/mirrors for a total of 1 day heh.


---
**Nathan Thomas** *June 11, 2017 00:23*

**+Joe Alexander** **+Yuusuf Sallahuddin**​

I just found the mirror. It was at the bottom of the box so it's all scratched up anyways.  Still have to replace it. 

I did measure it, approx .75" so that's about 19mm.


---
**Nathan Thomas** *June 11, 2017 00:37*

If I upgrade the damaged mirror, but keep the other two stock mirrors that's not damaged, would it change the laser effect as far as usage? Not talking about a full upgrade as far as size...but just the material....going with Si or Mo.

How hard is it to change the other 3 mirrors?


---
**Joe Alexander** *June 11, 2017 00:39*

pretty easy, just unscrew the retaining collars and swap. and as long as your mirrors are clean and relatively scratch-free you should be fine.


---
**Abe Fouhy** *June 11, 2017 00:50*

Are the stock optics that bad? It's only $45 to replace the head and mirrors, worth it if the head already is air assisted?


---
**Joe Alexander** *June 11, 2017 01:22*

its not terribly critical the stock ones can last a long time. if you have air assist already then start with that and see how it goes.


---
**Ned Hill** *June 11, 2017 02:41*

I upgraded all my mirrors to Moly from the start and added a lightobjects air assist head with upgraded lens.  Been very happy with those upgrades.


---
**Abe Fouhy** *June 11, 2017 06:31*

**+Ned Hill**​ how much for the moly lens pack?


---
*Imported from [Google+](https://plus.google.com/106608075082031214915/posts/ZmyEkBeBvU2) &mdash; content and formatting may not be reliable*
