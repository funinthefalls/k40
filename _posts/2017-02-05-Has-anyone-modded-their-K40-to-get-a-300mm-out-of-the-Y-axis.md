---
layout: post
title: "Has anyone modded their K40 to get a 300mm out of the Y axis?"
date: February 05, 2017 04:07
category: "Modification"
author: "Ray Kholodovsky (Cohesion3D)"
---
Has anyone modded their K40 to get a 300mm out of the Y axis?   I removed the sleeve on the left shaft at the bottom of Y which got me closer to 250mm.  



**+Ariel Yahni** what's the cut size of your openbuilds mod? 



I'm hoping for something less extreme than that though. 





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**Alex Krause** *February 05, 2017 05:32*

**+Kim Stroman**​


---
**Ariel Yahni (UniKpty)** *February 05, 2017 12:25*

Anything will involve changing the rails 


---
**K** *February 05, 2017 16:33*

Even with changing out the rails I wasn't able to get 300 because the of the mirror mount.


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/MY84ypzKhyT) &mdash; content and formatting may not be reliable*
