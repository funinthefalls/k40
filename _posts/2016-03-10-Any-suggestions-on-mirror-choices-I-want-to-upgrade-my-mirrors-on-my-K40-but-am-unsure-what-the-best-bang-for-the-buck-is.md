---
layout: post
title: "Any suggestions on mirror choices? I want to upgrade my mirrors on my K40, but am unsure what the best bang for the buck is"
date: March 10, 2016 21:34
category: "Hardware and Laser settings"
author: "Anthony Bolgar"
---
Any suggestions on mirror choices? I want to upgrade my mirrors on my K40, but am unsure what the best bang for the buck is.





**"Anthony Bolgar"**

---
---
**Richard Taylor** *March 10, 2016 22:08*

Someone recommended hard disc platters to me... I've not tried it, but if you can cut them to size would be very cheap if it works!


---
**I Laser** *March 10, 2016 23:18*

Just about to try that, will report back if it works. 



If you don't have the hardware it's a bit of an outlay, more than a set of mirrors anyway. You'll need hole saw, arbor, drill capable of taking arbor as they are larger than standard chuck size.



I wanted a couple of sets and spares and already had most of the hardware except 20mm hole saw. so thought I'd give it a crack.



Anyway for some reading:

[http://diylaser.blogspot.com.au/2012/01/get-your-co2-laser-mirrors-for-free.html](http://diylaser.blogspot.com.au/2012/01/get-your-co2-laser-mirrors-for-free.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 10, 2016 23:36*

**+I Laser** That's an interesting idea to use hard-disc platters for mirrors. I might have to give it a go sometime with some old hard-drives.


---
**Scott Marshall** *March 11, 2016 02:26*

Not worth making for 12 bucks

[http://www.ebay.com/itm/Dia-25mm-HQ-Silicon-SI-Reflection-Mirrors-10600nm-CO2-Laser-Engraving-Cutting-/252295527930](http://www.ebay.com/itm/Dia-25mm-HQ-Silicon-SI-Reflection-Mirrors-10600nm-CO2-Laser-Engraving-Cutting-/252295527930)



The set is cheaper. They have a set with lens of your choice(FL), and 3 moly (more rugged) mirrors for about $52.


---
**I Laser** *March 11, 2016 02:46*

It's about $18 AUD per mirror, each machine needs 3, so roughly $50 AUD.



$25 for 20mm hole saw and endless supply of mirrors lol. I need two sets, so prefer to save $75 ;)




---
**Scott Marshall** *March 11, 2016 02:54*

I forget you guys get hit hard on the shipping. Maybe it IS worth turn in out a couple sets. Performancewise, I hear they are nearly identical to the factory ones at low powers like we're running. You have to adjust for the thinner material, but once done, you're good to go.



When I 1st got mine, I got the set with moly mirrors and 18mm x 50.2mm lens for $52 US delivered. That puts you almost double the cost, assuming the AUD is close to the USD. 



Ouch.


---
**I Laser** *March 11, 2016 23:14*

It's what we like to call "The Australia Tax". Way pay more for pretty much most things, apparently having a relatively high minimum wage ($17.29 AUD/ $13 USD per hour) means we can afford it. :)


---
**I Laser** *March 12, 2016 03:49*

So boy genius bought a 20mm hole saw for 20mm mirrors. <b>facepalm</b> Will be getting a 25mm version which should supply 21mm mirrors which do fit the holder.



To be continued....


---
**Scott Marshall** *March 12, 2016 20:06*

DoDo happens. Never done that myself (uh huh)



Another option may be to put on the goggles and have at a platter with the dremel and a 1/8" carbide diamond bit.  






---
**I Laser** *March 13, 2016 22:45*

Ha, I've never screwed up prior to this either <b>cough cough cough</b>... :)



Okay so I've had time to test the them out. I can confirm they are as good as the original set. And unsurprisingly better than the damaged set I received on my second machine.



Not sure I'd agree they are easier to clean, a claim made on the blog I previously linked. Whilst I wasn't as careful handling them as I am with the legit mirrors, my finger prints were quite the mission to remove!



I've been fairly rough on them, so would agree they'll take a bit more punishment than the originals which even though I've been quite careful cleaning (isopropyl alcohol / cotton q tips and light pressure) still show some faint hairline scratching. 



Now do I think it's worth the effort? For sure! They aren't as 'finished' as the originals, even with burs removed the edges are still a bit rough looking but the important part is they work, they're much cheaper and not much effort to make. :D






---
**Scott Marshall** *March 13, 2016 23:01*

Cosmetics aren't a category for this sort of part, if the laser bounces off them, nobody will ever know their cool laser "object de art" was made by mirrors with rough edges.



Maybe tape them up before cutting next time - use some frisket film or lo-tack if you got it, masking tape ought to do it too. Easier to clean off tape adhesive than all the oils etc from handling/cutting them out.



Glad it worked out, may try it myself sometime when I have a little time.



Scott




---
**Anthony Bolgar** *March 14, 2016 07:42*

I wonder how well they would cut with a cnc router. I will take some platters to a buddy who has a nice little desktop cnc and see how they cut. If they work out good, I will offer them for free to anyone who wants them (Just pay postage)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/duASAYcbds5) &mdash; content and formatting may not be reliable*
