---
layout: post
title: "I made my wife and new clock face...what do you guy think?"
date: March 29, 2016 21:54
category: "Object produced with laser"
author: "Scott Thorne"
---
I made my wife and new clock face...what do you guy think? 

![images/7b878249c6cd5e60918fb2fb27a12558.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7b878249c6cd5e60918fb2fb27a12558.jpeg)



**"Scott Thorne"**

---
---
**Damian Trejtowicz** *March 29, 2016 22:00*

Wish to do one for my wife as well if its possible to get file


---
**Imko Beckhoven van** *March 29, 2016 22:13*

Youre Missing a hole in the Center ? 


---
**ED Carty** *March 30, 2016 02:10*

Bad @ss


---
**I Laser** *March 30, 2016 02:11*

The twelve is not at the top!















Kidding :) Very nice job, what are the dimensions and what's it cut from?


---
**Coherent** *March 30, 2016 02:23*

Now that's cool. Neat design!.


---
**Jim Hatch** *March 30, 2016 03:33*

Nice. Almost no burn/smoke marking in the plywood. Could only see it when I zoomed in the picture. Should clean off with alcohol right?


---
**Jarrid Kerns** *March 30, 2016 04:00*

Very nice


---
**Yuusuf Sallahuddin (Y.S. Creations)** *March 30, 2016 07:19*

Nice design Scott. Looks good.


---
**Scott Thorne** *March 30, 2016 09:41*

Thanks guys...it's 1/8 birch ply....the file is from free dfx files...cool designs on the site...all are free. 


---
**Scott Thorne** *March 30, 2016 09:42*

**+Jim Hatch**...fine grit emery cloth.


---
**Scott Thorne** *March 30, 2016 09:43*

**+I Laser**....if you download the file you can resize it to what size you néed...this one is 8 inchs


---
**I Laser** *March 30, 2016 09:58*

**+Scott Thorne** Cool, could you please link to site a quick Google search didn't provide results. 


---
**Scott Thorne** *March 30, 2016 10:02*

I think this is where I got it from...if not,  when I get off today I'll put the file on Google for you. 


---
**Scott Thorne** *March 30, 2016 10:02*

[http://vectorink.com/holiday/christmas-stocking-0185/](http://vectorink.com/holiday/christmas-stocking-0185/)


---
**The Technology Channel** *April 06, 2016 12:10*

I think it might be this one....

[http://www.readytocut.com/community/threads/clock-face.411/](http://www.readytocut.com/community/threads/clock-face.411/)








---
*Imported from [Google+](https://plus.google.com/101614147726304724026/posts/MDhkEDhrmEe) &mdash; content and formatting may not be reliable*
