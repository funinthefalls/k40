---
layout: post
title: "Accidental discovery: through an oversight of my own, I had forgotten to turn on the air assist while cutting some wood"
date: December 22, 2015 20:13
category: "Discussion"
author: "Ashley M. Kirchner [Norym]"
---
Accidental discovery: through an oversight of my own, I had forgotten to turn on the air assist while cutting some wood. Constant flame lit the 3D printed nozzle on fire. I caught it just as it was starting to get  deformed and blew it out. I really didn't want to deal with it at that point, and just fired it up again. Much to my surprise, the deformation on the nozzle is resulting in a way better cut now. Not even a little burn mark on the surface, nothing. Before I was getting very little burn mark which I was using as a design element. But now .... it's as clean as the wood itself.



I need to take the nozzle off and investigate the deformation and see about reprinting a new one with the same changes. Heh.





**"Ashley M. Kirchner [Norym]"**

---
---
**Rocky Choi** *December 22, 2015 20:26*

Happy accident! Glad that it didn't do any damage. Photos would be awesome when available!!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 22, 2015 20:43*

That's great & hopefully it is able to be replicated :)


---
**Scott Marshall** *December 22, 2015 23:19*

"I meant to do that"


---
**Ashley M. Kirchner [Norym]** *December 23, 2015 04:04*

I hope this works. This was yesterday, before the nozzle "mishap":

[https://goo.gl/photos/3HMpEZKZ685fRKrx7](https://goo.gl/photos/3HMpEZKZ685fRKrx7)



And this is today:

[https://goo.gl/photos/KK3tFfVZxWKVsqvy5](https://goo.gl/photos/KK3tFfVZxWKVsqvy5)


---
**Ashley M. Kirchner [Norym]** *December 23, 2015 04:07*

I haven't pulled the burnt nozzle off yet because I want to do some more cutting/engraving of some pieces that I want done. However, just looking at it, one of the things I <i>think</i> happened is that when the ABS melted, it stretched a bit, bringing the tip of the nozzle closer to the material. It used to float about 5mm above it, now it's within 3mm. I also think internally it changes as the 'hissing' pressure coming out of it is louder, which indicated a more restricted airflow. We'll see when I yank it off eventually.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 23, 2015 07:20*

**+Ashley M. Kirchner** Wow, that is actually a massive improvement on the scorch marks. I think you're onto a winner :)


---
**Scott Marshall** *December 23, 2015 15:51*

We (I) would sure like to know what made the difference, because it sure is a big step forward.



When you get it apart, I'll be paying attention to your findings.



My machine arrives tomorrow, and I ordered an aluminum air assist head from place in China on ebay, so it will be a while getting here.


---
*Imported from [Google+](https://plus.google.com/+AshleyMKirchner/posts/QT9usi57BzA) &mdash; content and formatting may not be reliable*
