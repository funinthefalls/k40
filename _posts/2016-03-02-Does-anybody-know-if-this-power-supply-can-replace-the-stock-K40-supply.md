---
layout: post
title: "Does anybody know if this power supply can replace the stock K40 supply?"
date: March 02, 2016 03:24
category: "Discussion"
author: "Tim barker"
---
Does anybody know if this power supply can replace the stock K40 supply? It doesn't have any real documentation listed and I can't seem to read the labels from the picture so I figured I'd see if anybody here has used a similar one as a replacement.





[http://www.ebay.com/itm/New-High-Quality-40W-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-110V-/261845993572?hash=item3cf73cc864:g:7d8AAOSwrklVKAMK](http://www.ebay.com/itm/New-High-Quality-40W-Power-Supply-for-CO2-Laser-Engraving-Cutting-Machine-110V-/261845993572?hash=item3cf73cc864:g:7d8AAOSwrklVKAMK)





**"Tim barker"**

---
---
**Scott Marshall** *March 02, 2016 08:22*

Not really any specs with the listing, or for that matter decent photos of the connections. 



I'm reaching here due to the lack of info, but my best guess it that it will work, but you will have to add a 5v supply to power the onboard controller. The "factory" k40 supply is a basic 15kv 30ma supply, but has the extra output. It's no big deal, it's a cheap fix - I'll link you to a suitable supply below.



Be warned the pin out IS NOT the same, so you won't be just plugging it in. It looks like the larger  4 pin  will be a plug in, but the control lines are different than MY k40 (I guess there are variations)



In mine, the leftmost connector is 2 pin ( P+ Gnd), Next is a 3 pin(K-,K+), and a 3 pin - (GND, LN, 5v)Note-this is the 5v supply mentioned. This connector also has the gnd to pair with the K pins.



I'm pretty sure I've seen exact replacements for $100-$125, these will plug right in. By the time you retrofit the 5v supply and repin the connectors, it may be well worth the extra 20 or 30 bucks. 



Good luck

Scott



Drop in K40 supply $100.

[http://www.ebay.com/itm/40W-Power-Supply-CO2-Laser-Engraving-Cutting-Machine-110V-220V-Switch-white-port-/331518580795?hash=item4d300c183b:g:UpMAAOSwD0lUcpRn](http://www.ebay.com/itm/40W-Power-Supply-CO2-Laser-Engraving-Cutting-Machine-110V-220V-Switch-white-port-/331518580795?hash=item4d300c183b:g:UpMAAOSwD0lUcpRn)



5v 6a power supply. Smaller ones are available, but this is better quality. There are many.

[http://www.ebay.com/itm/Switch-Power-Supply-Driver-adapter-AC110-220V-TO-DC-5V-12V-24V-48V-For-LED-Strip-/321999561547?var=&hash=item4af8ab634b:m:mRikYq1huBwLnIAepDqOrgA](http://www.ebay.com/itm/Switch-Power-Supply-Driver-adapter-AC110-220V-TO-DC-5V-12V-24V-48V-For-LED-Strip-/321999561547?var=&hash=item4af8ab634b:m:mRikYq1huBwLnIAepDqOrgA)




---
**Tim barker** *March 05, 2016 22:32*

Awesome! Thank you for the info.


---
**Scott Marshall** *March 08, 2016 18:47*

Glad to help, hope you get it running soon.

Scott


---
*Imported from [Google+](https://plus.google.com/113646430521733366133/posts/LwMUDiLJTHw) &mdash; content and formatting may not be reliable*
