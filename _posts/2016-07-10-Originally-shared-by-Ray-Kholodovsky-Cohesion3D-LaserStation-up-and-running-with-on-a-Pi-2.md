---
layout: post
title: "Originally shared by Ray Kholodovsky (Cohesion3D) LaserStation up and running with on a Pi 2"
date: July 10, 2016 18:57
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
<b>Originally shared by Ray Kholodovsky (Cohesion3D)</b>



LaserStation up and running with #LaserWeb3 on a Pi 2. Still need to network this corner of the basement before I can actually do anything. 

![images/31571a42266975e1af354a43dcabedf9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/31571a42266975e1af354a43dcabedf9.jpeg)



**"Ray Kholodovsky (Cohesion3D)"**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/eYT2J8ENTGj) &mdash; content and formatting may not be reliable*
