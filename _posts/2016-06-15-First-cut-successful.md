---
layout: post
title: "First cut successful!"
date: June 15, 2016 14:49
category: "Discussion"
author: "Ben Marshall"
---
First cut successful! 

![images/e83d4fdcc398f88d063bb51c3ece32c2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e83d4fdcc398f88d063bb51c3ece32c2.jpeg)



**"Ben Marshall"**

---
---
**Ariel Yahni (UniKpty)** *June 15, 2016 14:58*

Nice. Is that 5mm? What's setting did you use? 


---
**Ben Marshall** *June 15, 2016 15:00*

3mm oak ply I believe. Set at 15mA at 20mm/s. CoreIlaser plug in with trial version of corelx8. think I could lower the power. 


---
**Ariel Yahni (UniKpty)** *June 15, 2016 15:02*

Cool, im  building a database, when you get the lowest power setting on one cut please let me know


---
**Jim Hatch** *June 15, 2016 15:10*

**+Ben Marshall**​ Doing the happy dance? My first cut I was all excited about being able to cut something I drew with an invisible laser beam 😃


---
**Ben Marshall** *June 15, 2016 15:46*

**+Jim Hatch** I was pretty stoked when it finished. I drew this up over the weekend and couldn't wait to send it to the printer.


---
**Ben Marshall** *June 15, 2016 15:48*

**+Ariel Yahni** I'll send you the specs when I get home


---
**Stephane Buisson** *June 15, 2016 15:50*

Good feeling isn't it !!! (welcome here **+Ben Marshall** by the way)


---
**Ben Marshall** *June 15, 2016 19:59*

**+Ariel Yahni** 5mm ply (5.95 according to caliper)


---
**Ben Marshall** *June 15, 2016 19:59*

**+Stephane Buisson** thanks!


---
*Imported from [Google+](https://plus.google.com/100548165068880479823/posts/gX37TwdhRNu) &mdash; content and formatting may not be reliable*
