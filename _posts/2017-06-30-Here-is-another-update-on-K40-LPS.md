---
layout: post
title: "Here is another update on K40 LPS"
date: June 30, 2017 03:22
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40LPS



<b>Here is another update on K40 LPS.</b>



I took **+Nate Caine**'s advice and autopsy'd another High Voltage Transformer (HVT). I am glad I did because I was surprised at the alternate results I got.



I think that now we have a pretty good idea what is in a K40 LPS including the HVT.



The detailed post: [http://donsthings.blogspot.com/2017/06/k40-high-voltage-transformer-autopsy-2.html](http://donsthings.blogspot.com/2017/06/k40-high-voltage-transformer-autopsy-2.html)



From the post:



<b>Learning's</b>

A K40 HVT contains a high current primary and a multi-section secondary. A high turns ratio secondary in combination with a voltage double'r  creates approximately 11,300 volts per 100 volts of primary voltage.

This autopsy provides a model of the HVT that more completely characterizes a key component of a K40 LPS... its HVT.



If the above analysis holds true then the following has been verified:



..K40 LPS are easily capable of voltages in excess of 23,000 volts

..A K40 HVT's include a voltage doubl-er in its output stage

..A K40 HVT cannot be tested using a standard DVM because it cannot forward bias the internal HV diodes. Each HV diode is actually a serially connected stack of 20 or more diodes. Voltages that exceed 120 volts might be necessary to forward bias both the diodes in this double'r configuration.

..K40 HVT's are not repairable



<b>Suspicions of K40 LPS Failure Modes</b>

..I suspect that LPS failures fall into these categories:

..AC plug swapped with the DC plug damaging the supply's enabling circuits

..The low voltage PWM controllers output shorting, blowing itself and the bridge rectifier.

..The Bridge rectifier failing under load. 

..Arc's causing excessive secondary current, opening the HVT's diodes.



<b>What's Next To Do On the K40 LPS Quest</b>

..Map the actual voltages in the LPS including the primary's HV buss to further verify the above model.

..Scope and capture dynamic views of the internal circuitry's operation.

..Scope and capture dynamic views of the Lasers current and voltage while marking.

..Review the component specifications and verify that specifications are not being exceeded in actual operation.

..Noodle a safe HVT DIY HVT tester. 

..Noodle a safe and DIY HV tester so you can tell if tube or LPS is bad.

..A safe how-to- repair your LPS



Thanks again for all the help and support...





![images/9a0937bc316a8154c5f2d3a59a45983f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9a0937bc316a8154c5f2d3a59a45983f.jpeg)
![images/27b7edf00ffb56ac9f45cfd6249ad76f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/27b7edf00ffb56ac9f45cfd6249ad76f.jpeg)
![images/2f71adf77923d6a28ba0ee6fbe9fc92b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2f71adf77923d6a28ba0ee6fbe9fc92b.jpeg)
![images/f4b6e6d8eddcea46f604a2456fa34fe9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f4b6e6d8eddcea46f604a2456fa34fe9.jpeg)
![images/c1e848c98d56a56e1b1ba5cc4ade92c8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c1e848c98d56a56e1b1ba5cc4ade92c8.jpeg)

**"Don Kleinschnitz Jr."**

---
---
**Paul de Groot** *June 30, 2017 04:02*

Amazing photography, well done. I expected an array of diodes and capacitors instead of just two diodes. Learned something new here :)


---
**Steve Clark** *June 30, 2017 17:00*

Nice work Don!


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/3W6gHQxQjGp) &mdash; content and formatting may not be reliable*
