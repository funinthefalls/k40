---
layout: post
title: "I remade my last laser cut but this time at 45% size so now I have Father & Son the big one is 200mm sq and the small one 90mm sq"
date: June 11, 2017 15:23
category: "Object produced with laser"
author: "Don Recardo"
---
I remade my last laser cut but this time at 45% size so now I have Father & Son 

the big one is 200mm sq and the small one 90mm sq







![images/7dbe1eeb2ae104faa9d8ff09ac92c589.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7dbe1eeb2ae104faa9d8ff09ac92c589.jpeg)
![images/f1ce643ffbb7c0f4d04509bb18d52e9d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f1ce643ffbb7c0f4d04509bb18d52e9d.jpeg)

**"Don Recardo"**

---
---
**greg greene** *June 11, 2017 15:34*

very cool


---
*Imported from [Google+](https://plus.google.com/106290033771296978518/posts/FqfZ6JWL4FS) &mdash; content and formatting may not be reliable*
