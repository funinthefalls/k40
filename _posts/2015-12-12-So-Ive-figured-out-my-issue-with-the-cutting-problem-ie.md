---
layout: post
title: "So, I've figured out my issue with the cutting problem (i.e"
date: December 12, 2015 20:17
category: "Materials and settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So, I've figured out my issue with the cutting problem (i.e. not cutting cleanly). Turns out when I removed the default cutting area from beneath my perforated steel area, it dropped the steel ~4mm lower. So I raised up the legs ~4mm & now it's cutting like a beauty.



Here's some tests I did on leather.



From left to right:

4mm Natural Vegetable Tanned Leather

Tests: 3, 4 & 5mm/s

Power: 7mA

Conclusion: 3mm/s worked, but only just. Might be better to drop to 2.5mm/s if possible.



2mm Natural Vegetable Tanned Leather

Tests: 6, 7 & 8 mm/s

Power: 7mA

Conclusion: All three speeds worked. So, I'd recommend going with the faster speed.



1mm Natural Vegetable Tanned Kangaroo Hide

Tests: 10, 11, 12, 13, 14, 15mm/s

Power: 7mA

Conclusion: All five speeds worked. So again, I'd recommend going with the faster speed.

![images/f329d0b7d564a4c18d09e110e7fe4cba.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f329d0b7d564a4c18d09e110e7fe4cba.jpeg)



**"Yuusuf Sallahuddin (Y.S. Creations)"**

---


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/3qry6hwMvEj) &mdash; content and formatting may not be reliable*
