---
layout: post
title: "Does the K40 cut better through ply or is it the same for hardwood.???"
date: February 26, 2016 10:54
category: "Discussion"
author: "Tony Schelts"
---
Does the K40 cut better through ply or is it the same for hardwood.???  How many passes is it needed to cut 6mm hardwood?  I can cut 1/8 ply at 8mm sec @ 7ma any thoughts thanks





**"Tony Schelts"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 27, 2016 13:10*

I have never tried, but would be interested to know if it is possible too. When I cut leather, I notice that thicker stuff (like 3-5mm thick) requires either a lot slower, a lot more power, or many more passes in order to cut through it. You may find it to be similar with the hardwood.


---
**Ben Walker** *February 29, 2016 12:36*

I did 1/4 inch oak at 5mms with power at 15 or so.  1/8 inch birch cuts easy at 15mms at 11-13 power.  I really like cutting the birch and poplar the best.  Poplar gives a nicer contrast on the engrave.  Avoid knots in solid woods.  I have some black walnut I plan to test sometime today.  Will update.


---
*Imported from [Google+](https://plus.google.com/103582753975119848936/posts/39a96JLhF75) &mdash; content and formatting may not be reliable*
