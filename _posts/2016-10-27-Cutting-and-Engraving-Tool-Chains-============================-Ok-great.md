---
layout: post
title: "Cutting and Engraving Tool Chains ============================ Ok great!"
date: October 27, 2016 02:48
category: "Software"
author: "Timo Birnschein"
---
Cutting and Engraving Tool Chains

============================



Ok great! I have a laser that can cut 6mm wood! I can engrave at 2500mm/min and rising! How can I utilize all that power?



The question of software is none that is easy to answer. I have been searching for a toolchain that works reliably(!!) but I could not find any. That's why I'm writing this. Not because I have all the answers.



Goal: be able to design an X-Wing with all the vector engravings and 2.5D engravings for the pumps outlets and little details and then cut the parts out using (DXF) vector cuts - in one file.



I use Solidworks for the design of parts. Anything else also works well, if it's 123 or OpenSCAD or InkScape. Export the vector cuts as DXF is no problem in all those apps and professional software specialized on laser cutting handles all those DXF files with ease!



How can I make dxf to gcode? I currently prefer dxf2gcode. Unfortunately, it is NOT reliable. If (!! - not when) it loads the dxf file, no problem. Export to gcode works without any issue and is freely configurable. But if it doesn't load the dxf file because.... I guess reasons... I can't use this brilliant software that stitches all the nodes automatically and flawlessly into a single path with definable laser power and speed per path. What then?



Inkscape is, surprisingly, pretty dump and very slow and stalls many times a day when handling dxf files. There seems to be NO universal way to weld the separate lines from a dxf file into exportable single paths, so none of the plug-in gCode exporters work with previously imported dxf files! It only works well if the part was initially designed in Inkscape in the first place! So, hundreds of people are crying for years that dxf welding doesn't work but... no help in sight.



Then, there is the engraving problem: I can generate a 2.5D engraving and convert it to gCode myself. I have found img2gco, I modified it and it works well. But it does only that! I can't mix it properly with a dxf file for later cutting around the engraved parts!



To not write a book: Let us collect tool chains that work (if that has never been done before somewhere).



- What are you using to import and handle dxf (or similar) files for vector cutting that works reliably?

- What are you using to generate engravings?

- What are you using to mix engravings and dxf cutting?



I use 



1. Solidworks -> dxf export -> dxf2gcode -> any gcode sender including my own

2. Photo -> img2gco with my mods -> my own gCode sender because it's fast enough

3. Solidworks and some 2.5D heightmap greyscale image for engraving -> what...? CamBam maybe? Laserweb? But I have issues with LaserWeb because some of the vectors it exports are so small that my machine simply crawls along, even with the new grbl 1.1. 



What else? Is there a software that costs an acceptable amount of money that handles dxf and engraving and exports gCode for our lasers?





**"Timo Birnschein"**

---
---
**Stephane Buisson** *October 27, 2016 12:20*

the tools chain, will be dependant of your laser cutter controller for the last link.



**+Timo Birnschein** did you also check Visicut ?




{% include youtubePlayer.html id="lbTTPkDEhOg" %}
[https://www.youtube.com/watch?v=lbTTPkDEhOg](https://www.youtube.com/watch?v=lbTTPkDEhOg)


---
**HalfNormal** *October 27, 2016 15:23*

**+Timo Birnschein** search the Inventables forum and the Shapeoko wiki and forum. They have been fighting this issue also for years.


---
**Timo Birnschein** *October 27, 2016 18:39*

**+Stephane Buisson** Visicut!! I have not! Thanks for this, I'll certainly test this tonight as soon as I get home. It looks like ppl are actually using it successfully and it support gcode using a general gcode driver. It is also written in Java which makes it maintainable from a C++ developer point of view. I'm looking forward to it!



Anyone using Visicut with grbl?



Anything else?



btw. I'll try Laserweb 3 again tonight as well. I have this weird phenomenon that even 3mm holes use hundreds of mini vectors in the gcode file. If I find a method to reduce the detail in the gcode output it has most of the stuff I'm searching for. Images and cuts in one file, positioning of individual objects. Only the serial sender is too slow and a bit unstable but maybe I can have a look at that and help. We'll see...


---
*Imported from [Google+](https://plus.google.com/+TimoBirnschein/posts/EW2tihVz4hV) &mdash; content and formatting may not be reliable*
