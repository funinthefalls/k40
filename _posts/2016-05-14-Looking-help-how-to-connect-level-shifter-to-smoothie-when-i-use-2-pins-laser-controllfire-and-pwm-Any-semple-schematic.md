---
layout: post
title: "Looking help how to connect level shifter to smoothie when i use 2 pins laser controll(fire and pwm) Any semple schematic?"
date: May 14, 2016 13:44
category: "Smoothieboard Modification"
author: "Damian Trejtowicz"
---
Looking help how to connect level shifter to smoothie when i use 2 pins laser controll(fire and pwm)

Any semple schematic?





**"Damian Trejtowicz"**

---
---
**Stephane Buisson** *May 14, 2016 17:45*

look here for inspiration (and in comments): [https://plus.google.com/114978037514571097180/posts/MxAWWe4jmzS](https://plus.google.com/114978037514571097180/posts/MxAWWe4jmzS)


---
**Damian Trejtowicz** *May 14, 2016 17:58*

I connected without level shifter,all work expect that i have only about 50 % of power.

Just still not grab how to put level shifter in line


---
**Damian Trejtowicz** *May 14, 2016 20:27*

Yupiii,all sort it out.i have full spectrum of laser power now

PWM signal is going thru level shifter,FIRE pin is going straight from smoothie board

I had problem because reference 3.3v from my board was missing(my jumper was set up to use 5v for endstops,and this is point where i took reference voltage( i use mks sbase 1.2 board)

Im so happy now how everything work,everything mive more quiet and smooth,visicut is briliant and at last laser web working right for me

I still seen laserweb is nit working with smoothie or am i wrong?


---
**Stephane Buisson** *May 15, 2016 08:21*

**+Damian Trejtowicz** move on laserweb2 (I update the link).

If you have the time you can also try Visicut (should work with usb, not only ethernet), and Fusion 360.

So a large choice to enjoy ...

Please describe your convertion somewhere, and share the link with the community. (did you kept the pot? (act as ceiling and save your tube lifespan).


---
**Damian Trejtowicz** *May 15, 2016 11:19*

I removed the pot(its 2 seconds to connect back)

I tried VisiCut and Laserweb1 and they work fine.when i do full documentation ,included schematics and firmware and config file for mks sbase 1.2 i will publish this


---
**Athul S Nair** *May 24, 2016 12:58*

**+Damian Trejtowicz** Can yo give the link of logic level converter you used?


---
*Imported from [Google+](https://plus.google.com/115612955145035237158/posts/AT4k5TZNYuB) &mdash; content and formatting may not be reliable*
