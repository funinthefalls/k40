---
layout: post
title: "When cutting wood or MDF should the ends be very dark and scorched looking ?"
date: February 08, 2017 21:54
category: "Discussion"
author: "Robert Selvey"
---
When cutting wood or MDF should the ends be very dark and scorched looking ?

![images/b937b69a16a29e075a3ed34e0cb9a6d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b937b69a16a29e075a3ed34e0cb9a6d7.jpeg)



**"Robert Selvey"**

---
---
**greg greene** *February 08, 2017 22:04*

Well, you are cutting wood with fire aren't you?  So I would expect that.


---
**Hani Chahine** *February 08, 2017 22:41*

It's that char that actually allows you to cut through the wood using the laser. I personally like the effect.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 08, 2017 22:42*

Unfortunately for us who don't like the effect it is going to happen. There's pretty much nothing you can do about it (to my knowledge) other than sanding it off (or painting it) after the job is finished.


---
**Jim Hatch** *February 08, 2017 23:06*

You can reduce char by more finely tuning your power/speed settings to be just enough to cut but no more than that. Then you can get a lighter smoke vs dark char that rubs off at the overpowered/too slow extreme.



You can clean it with orange hand cleaner (GoJo) and a brush. 


---
**Ned Hill** *February 09, 2017 00:21*

Having a higher and directed air flow on the air assist can also help reduce charring by lowering the power/unit time dynamic like Jim mentioned.  


---
**Paul de Groot** *February 09, 2017 01:46*

Or just cut acrylic material ☺


---
**Eldar Khaliullin** *February 10, 2017 05:16*

You can try to clean it up with hydrogen proxide. 


---
**Jim Hatch** *February 14, 2017 14:09*

**+brandon klevence** You can make the "weeding" of the painters tape easier by grabbing it with a higher tack tape like duct or gorilla tape after you're done lasering. Press the gorilla tape on the blue tape bits and then peel it off. The gorilla tape sitcks better to the blue tape than the blue tape sticks to the wood.


---
**New cannabis grower** *February 22, 2017 18:02*

No its not ment to be dark and come of on your fingers but unless you have air assit it's going to happen 


---
**Jim Hatch** *February 22, 2017 18:25*

**+Stuart Hands** You can also minimize the charring by using just enough power to cut but no more. That takes a bit of practice to dial in. Lower power or higher speeds can get it so it's "toasted" but not blackened. Worth putting together a little calibration tool that cuts out small squares at different power/speed combos so you can test a particular sample before doing the final engrave/cut. Cleanup is pretty easy for wood with orange pumice soap (like GoJo) or for MDF I tend to use a random orbit sander with 220 grit as it's quick and doesn't have a chance to swell the material since it's a finer composition than ply or hardwood.


---
*Imported from [Google+](https://plus.google.com/109246920739575756003/posts/7sqzrLg99m7) &mdash; content and formatting may not be reliable*
