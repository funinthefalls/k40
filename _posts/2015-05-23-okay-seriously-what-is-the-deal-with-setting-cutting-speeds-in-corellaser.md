---
layout: post
title: "okay, seriously, what is the deal with setting cutting speeds in corellaser?"
date: May 23, 2015 03:38
category: "Discussion"
author: "Eric Parker"
---
okay, seriously, what is the deal with setting cutting speeds in corellaser?  the speed shown is comepletely NOT the speed that it runs at.





**"Eric Parker"**

---
---
**Chris M** *May 23, 2015 08:13*

Make sure that the board type and serial number are set correctly in the properties dialog.




---
**Eric Parker** *May 23, 2015 17:43*

I definitely checked that first.  It's just that lower mm/sec speeds seem to make it run faster, and higher runs slower.


---
**Stuart Middleton** *May 24, 2015 00:52*

I've found that it tops out at a certain speed (although I'm sure it didn't before) but small numbers are slower cuts than big numbers for me.


---
**Eric Parker** *May 25, 2015 04:25*

okay, so after some testing, the number is accutare for engraving, yet not cutting.  More testing is needed.


---
**John Karbassi** *July 09, 2015 16:30*

Eric - I just went through this today.  Had to change both the device ID as well as the controller type in CorelLaser.  Neither was set correctly.  Both were printed on the control board.


---
**Eric Parker** *July 09, 2015 19:12*

I've done that.  Still not as expected, so at this point I'm just writing down what works and what doesn't


---
*Imported from [Google+](https://plus.google.com/+EricParkerX/posts/eRRAQH3R2Gd) &mdash; content and formatting may not be reliable*
