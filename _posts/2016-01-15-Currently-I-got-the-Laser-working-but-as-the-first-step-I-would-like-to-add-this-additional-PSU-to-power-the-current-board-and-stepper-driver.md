---
layout: post
title: "Currently I got the Laser working but as the first step I would like to add this additional PSU to power the current board and stepper driver"
date: January 15, 2016 07:19
category: "Modification"
author: "Sunny Koh"
---
Currently I got the Laser working but as the first step I would like to add this additional PSU to power the current board and stepper driver. Can someone point me in the correct direction where I can see a wiring diagram for the 4 pins as I found that 3 of the wires are  24V, 5V and GND while the 4th is the Laser On. 



![images/1f4968a02d6af9973151b7797faef869.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1f4968a02d6af9973151b7797faef869.jpeg)
![images/fb0708446a795ad49a28c3fdfba9f279.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fb0708446a795ad49a28c3fdfba9f279.jpeg)
![images/cdfce3bb8f69231d438a7757ae820502.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cdfce3bb8f69231d438a7757ae820502.jpeg)
![images/c23aa2545d31d0ebbe211fdc3c3c1a3f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c23aa2545d31d0ebbe211fdc3c3c1a3f.jpeg)

**"Sunny Koh"**

---
---
**Anthony Bolgar** *January 15, 2016 16:50*

Why do you want to add another PSU?...the stock setup has enough power.


---
**Sunny Koh** *January 15, 2016 17:33*

**+Anthony Bolgar**  A few reasons, firstly to isolate the board from the laser PSU. Second is to hopefully improve stability of the LiHuiYu board as I am getting USB disconnections running LaserDrw and CorelLaser. (Read about the laos board and some stablity of the laser PSU output)  And finally, first step in converting the LIHUIYU board to an open source board. 


---
*Imported from [Google+](https://plus.google.com/107483714711922583550/posts/jmU58Sgg3HH) &mdash; content and formatting may not be reliable*
