---
layout: post
title: "Sorry for silly question but do either of these fit on the lightobject laser head?"
date: June 08, 2016 15:29
category: "Modification"
author: "Pippins McGee"
---
Sorry for silly question but do either of these fit on the lightobject laser head?

22mm diameter ([http://www.ebay.com.au/itm/K40-Co2-Laser-Head-Focus-Diode-Module-Red-Dot-Position-Engraver-Cutter-22mm-Dia-/252338844927](http://www.ebay.com.au/itm/K40-Co2-Laser-Head-Focus-Diode-Module-Red-Dot-Position-Engraver-Cutter-22mm-Dia-/252338844927))



24.5mm diameter ([http://www.ebay.com.au/itm/Co2-Laser-Head-Adjust-Focus-Diode-Module-Red-Dot-Position-DIY-Engraver-Cutter-/252214823938](http://www.ebay.com.au/itm/Co2-Laser-Head-Adjust-Focus-Diode-Module-Red-Dot-Position-DIY-Engraver-Cutter-/252214823938))



or do neither of them fit the LO nozzle at all (if not - which type are people buying for LO head)



Cheers, just wanted to check before buying.





**"Pippins McGee"**

---
---
**Ned Hill** *June 08, 2016 22:29*

I was actually looking at buying one myself.  The LO air assist head measures 25mm just above the K40 mounting plate and 20mm  below.  So the 24.5mm one looks like it would work mounted above the plate.


---
**Ned Hill** *June 08, 2016 22:45*

Then again I'm not totally sure how well it would clear the edge of the mounting plate.




---
**Pippins McGee** *June 09, 2016 00:26*

**+Ned Hill** thanks Ned, i was wondering on the measurements of the LO head. Mine is still in the mail so I don't have it yet to measure.



Is this what you were thinking? (ignoring the fact the image shows a stock head instead of an LO heaD)

[http://i.imgur.com/ZYXN6Av.jpg](http://i.imgur.com/ZYXN6Av.jpg)

If so, if mounted here it looks as if it would clear the mounting plate OK.


---
**Ned Hill** *June 09, 2016 00:30*

Yes that is what I was thinking, but you need to keep in mind that the LO head has a smaller diameter then the stock K40 head so there will be a bigger gap between the head and the edge of the plate.


---
**Pippins McGee** *June 09, 2016 03:24*

**+Ned Hill** ah good point. I may email lightobject and see what they recommend. 

Else I'll just have to take my chances and buy one and hope it fits.



I'm sure some other people here have done red dot upgrade with LO head, feel free to chime in


---
**Richard Wallace** *June 09, 2016 06:28*

Does anyone have any info on wiring one of these up?  Can I get 5V from the power supply or M2 Nano board?  (Is it possible to have it turn on at the times when the laser is meant to be firing, even when the laser is turned off?)


---
**Pippins McGee** *June 09, 2016 06:57*

**+Richard Wallace** Hi Richard, I found a post that may be answer your question.

[https://plus.google.com/104424492303580972567/posts/i5sueP4gp3e](https://plus.google.com/104424492303580972567/posts/i5sueP4gp3e)



Re: tapping 5v from the stock boards. seems a few people have tapped 5v from a specific connector on the m2 nano.



also, I would just wire in a simple toggle switch along the wire so you can control when to turn it on / off yourself


---
**Richard Wallace** *June 09, 2016 23:54*

**+Pippins McGee** Thanks for the info.

Also, can anyone tell me (a totally non-electronics person) what is the best/proper way to connect the wires for the red dot laser to the pins on the M2 Nano board?  I assume that the red dot laser will just come with bare wires and I will have to handle the connection myself.   I was thinking that something like [http://www.ebay.com.au/itm/162038052220](http://www.ebay.com.au/itm/162038052220) or [http://www.ebay.com.au/itm/162014613300](http://www.ebay.com.au/itm/162014613300) would work.


---
*Imported from [Google+](https://plus.google.com/100825332636790645093/posts/3L65Buadgzs) &mdash; content and formatting may not be reliable*
