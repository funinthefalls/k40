---
layout: post
title: "One of my hobbies is board games, the K40 will be great for making deluxe enhancements for games"
date: July 02, 2016 20:18
category: "Object produced with laser"
author: "Tev Kaber"
---
One of my hobbies is board games, the K40 will be great for making deluxe enhancements for games. 



Here is a fan-made printable playmat for Dungeon Dice (downloaded from BoardGameGeek), and my lasercut version of the same thing. 



Two layers of 3mm plywood, glued together. 

![images/134d571b0520c54c87e200b81f4a2c2b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/134d571b0520c54c87e200b81f4a2c2b.jpeg)



**"Tev Kaber"**

---
---
**Jim Hatch** *July 02, 2016 21:35*

Very nice. All you need are custom dice 😃


---
*Imported from [Google+](https://plus.google.com/100576662332619772799/posts/9CJ9quSEo2G) &mdash; content and formatting may not be reliable*
