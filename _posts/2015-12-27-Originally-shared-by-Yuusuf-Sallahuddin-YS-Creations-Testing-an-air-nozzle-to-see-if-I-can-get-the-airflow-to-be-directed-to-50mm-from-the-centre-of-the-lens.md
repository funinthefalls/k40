---
layout: post
title: "Originally shared by Yuusuf Sallahuddin (Y.S. Creations) Testing an air-nozzle to see if I can get the airflow to be directed to 50mm from the centre of the lens"
date: December 27, 2015 10:30
category: "Modification"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
<b>Originally shared by Yuusuf Sallahuddin (Y.S. Creations)</b>



Testing an air-nozzle to see if I can get the airflow to be directed to 50mm from the centre of the lens. My thoughts are that this is the focal point of the stock lens on the K40 & that hitting directly at the area being cut/engraved may minimise the charring/soot. Can't test on the machine at the moment as I've replaced water hoses & silicone I used requires 72hrs to dry before I can run water through it. So won't be able to use the laser until Wednesday night at earliest.





**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 27, 2015 10:32*

Here are some photos of the air-nozzle thing so you can see inside it if you'd like.

[https://drive.google.com/folderview?id=0Bzi2h1k_udXwNnFqSDJuYU1JaGc&usp=sharing](https://drive.google.com/folderview?id=0Bzi2h1k_udXwNnFqSDJuYU1JaGc&usp=sharing)


---
**Bruce Williams** *December 27, 2015 20:59*

Hi Yuusuf, it's an interesting idea but I can't see how the air is focused to the cutting area from the video.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 28, 2015 03:22*

**+Bruce Williams** Oh, there is a link above (in my comment) that links to some photos of where the air is coming from. In the video, the paper on the string was my test to see if the air is hitting the right spot.


---
**Bruce Williams** *December 31, 2015 02:18*

Hi Yuusuf, What are you using for an air pump? I saw your earlier post with the ball inflation needle which I thought was a clever idea. Did that work for you?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 31, 2015 02:32*

Hey **+Bruce Williams**. Yeah it actually works reasonably well. This video is of the ball inflation needle too (except I chopped it shorter). For the air pump I am using a very basic air pump that came with some Tanning Airbrush Kits I purchased (for $5-6 ea. at a local auction site). Basically it is the same as an airpump for a fishtank, with about the same output. When you use the ball inflation needle though, it focuses it a bit better & gives more pressure on a smaller area. I am however considering a small airbrushing air-compressor (as I could dual use it for airbrushing also).


---
**Bruce Williams** *December 31, 2015 03:22*

Thanks Yuusuf! I really appreciate the quick response. I just got my laser for Christmas and managed to set the cardboard on fire lol.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *December 31, 2015 05:16*

**+Bruce Williams** Haha, I did some tests on cardboard stuff & also set it on fire when I first played with it. Once I included some form of air to blow out the flames, it worked nicer.



Great Xmas present by the way. You'll get many hours of fun (& annoyance) out of the K40.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XLgKSPh6JJy) &mdash; content and formatting may not be reliable*
