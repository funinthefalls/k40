---
layout: post
title: "Any Seattle or Puget Sound laser users"
date: July 01, 2018 19:10
category: "Object produced with laser"
author: "James Lilly"
---
Any Seattle or Puget Sound laser users.  Here's a information card about an upcoming art show with my laser engraved/cut wall scultures.

![images/b675bb0f90d3498173222270e3da9ab2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b675bb0f90d3498173222270e3da9ab2.jpeg)



**"James Lilly"**

---
---
**James Rivera** *July 02, 2018 06:46*

I live in Bellevue. No guarantees I can make it, but more information would be helpful.


---
**James Lilly** *July 02, 2018 20:49*

Here's a link to the gallery website:[http://www.lynnhansongallery.com/](http://www.lynnhansongallery.com/)

[lynnhansongallery.com - LYNN HANSON GALLERY](http://www.lynnhansongallery.com/)


---
**James Lilly** *July 02, 2018 20:54*

312 S Washington St

Seattle, WA 98104

(Pioneer Square, Tashiro Kaplan Building)



You can park on Yesler and walk down, if you're early enough there is free parking in the garage on 3rd between Washington and Yesler.


---
*Imported from [Google+](https://plus.google.com/114642009939368433927/posts/BQAtJv4Rjqe) &mdash; content and formatting may not be reliable*
