---
layout: post
title: "Decided to go with the assist nozzle, and it'll be here in a couple days"
date: August 29, 2015 14:19
category: "Modification"
author: "Kirk Yarina"
---
Decided to go with the [lightobjects.com](http://lightobjects.com) assist nozzle, and it'll be here in a couple days.  Any suggestions on what to use for the air line so it won't interfere with the moving carriage?   The only coiled air line I've found is pretty large.



How does the factory lens holder come apart?  If it's screwed on it's pretty tight.  I couldn't see a setscrew, but it might be hidden on the back.  I'd like to see what size lens it is so I can cut an adapter.





**"Kirk Yarina"**

---
---
**ThantiK** *August 29, 2015 15:11*

If you have access to a 3D printer you can print a drag chain: [https://lh5.googleusercontent.com/-ye53AYsQL_Q/VJZRAq7ZqYI/AAAAAAAANLw/fu1Kh5uZyt8/w1596-h898-no/20141220_204702.jpg](https://lh5.googleusercontent.com/-ye53AYsQL_Q/VJZRAq7ZqYI/AAAAAAAANLw/fu1Kh5uZyt8/w1596-h898-no/20141220_204702.jpg)


---
**David Wakely** *August 29, 2015 15:35*

The size of the lens on these is 12mm I believe. And drag chain would work but be careful you don't get one that's too large.


---
**Kirk Yarina** *August 29, 2015 23:53*

I thought about drag chain, even have some on hand, but there's nowhere to attach it that doesn't interfere with the carriage.  The McMaster Carr tubing looks best.  Thanks!


---
**Kirk Yarina** *August 30, 2015 15:40*

And just how does the factory head/lens holder come apart to get to the mirror?  Is mine just reefed on too tight?


---
**David Wakely** *August 30, 2015 17:18*

The head assembly should just unscrew. It shouldn't be that tight. I am not sure on his but I think on mine it is backward threaded to it unscrews the opposite way to a normal screw 


---
**Joey Fitzpatrick** *August 31, 2015 03:06*

I made my own coil hose from regular 6mm pneumatic hose.  wrap the hose tightly around a pipe and heat with a heat gun. after it is heated, cool it immediately with cold water.  It will hold the coil shape indefinitely.


---
**Kirk Yarina** *August 31, 2015 19:45*

Lens holder finally unscrewed, with a normal right hand thread.  It's been sitting since yesterday, so perhaps it had warmed up from use the last time I tried it.  Mine is a 12.0 lens.



+Joey Fitzpatrick , thanks for the tip, and pictures in the other topic.  Nice looking installation!


---
**Kirk Yarina** *September 09, 2015 01:13*

Finally installed the LO nozzle/head assembly over the weekend (took a pair of channelocks to get the old one apart) and jury rigged about 2'/600mm of silicone aquarium tubing to my 65lpm air pump.  The tubing is about the same 4mm ID as the linked mcmaster carr coiled hose, and the airflow out of the nozzle was pretty unimpressive.  I think a larger ID hose is going to be required.  Was that 6mm hose ID or OD?


---
**Joey Fitzpatrick** *September 09, 2015 03:31*

Most of the air assist hose used on these K40 machines are 4mm I.D. 6mm O.D.  If you want a little more volume through the Light object air assist head, you can drill the nipple out a little larger.  The one I bought from Light Object only had a 1.5mm nipple hole.  I was able to open it two drill sizes.  I could have gone three, but I didn't want to chance drilling through the side wall.  4mm I.D. hose should be plenty big enough


---
**Kirk Yarina** *September 17, 2015 23:26*

**+Joey Fitzpatrick**  Mine was about 2mm id with burrs at the ends, so I drilled it out to 3.18mm/1/8" (good excuse to fire up the Unimat) and used a countersink to deburr both ends.  It made a noticeable difference in air flow.  Thanks!  Used a couple rubber bands to rig up the aquarium tubing, still need to follow up with something better.


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/3jAJgkWMhu5) &mdash; content and formatting may not be reliable*
