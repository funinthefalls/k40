---
layout: post
title: "Ok so can someone explain to me how I can engrave the inside of a project but then cut out the outside of the object all in one process in corelLaser every time my corelLaser finishes a job the tool bar where you select to"
date: May 03, 2016 20:20
category: "Discussion"
author: "Donna Gray"
---
Ok so can someone explain to me how I can engrave the inside of a project but then cut out the outside of the object all in one process in corelLaser every time my corelLaser finishes a job the tool bar where you select to cut engrave etc disappears I cannot work out how to find the tool bar again I thought I could do all the engraving first then set it to cut the outline next but cant find the tool bar again???





**"Donna Gray"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 03, 2016 21:15*

Ah, what you want to do is not check the "starting" tickbox. So firstly, add the engrave to the queue (making sure that "starting" is not enabled). Then your toolbar may disappear, mine does too all the time. If so, you want to go down near the clock in windows & there is a red icon that is for the laser cutter toolbar. Right click it, then choose cut, add to queue (again make sure that "starting" is not enabled). Once both have been added to the queue (or more if you wanted) you can right click the icon & choose the one that says "1 task" & has what looks to be a play button. It should then do the engrave first (as you added it to queue first) then the cut next.


---
**Jim Hatch** *May 03, 2016 21:20*

What Yuusuf said :-) As he notes you need to send them as separate steps so it helps if they're separate layers in Corel. Some lasers and software allow you to do both cutting and engraving in the same job by using line colors to distinguish the different operations (but even there sometimes will only do that if all of it is vector based).


---
**Mishko Mishko** *May 03, 2016 21:57*

You can also check this clip: 
{% include youtubePlayer.html id="MR7967VHKnI" %}
[https://www.youtube.com/watch?v=MR7967VHKnI](https://www.youtube.com/watch?v=MR7967VHKnI)


---
*Imported from [Google+](https://plus.google.com/103145403582371195560/posts/WinjA6eXdNa) &mdash; content and formatting may not be reliable*
