---
layout: post
title: "So finally I have the new tube"
date: May 27, 2016 01:48
category: "Discussion"
author: "Ariel Yahni (UniKpty)"
---
So finally I have the new tube. I don't think it's venue possible to get a better pack than this, it the to keep the free wood tool box. 



![images/fa7835d57463d6bd51f53e1b8e850ccc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fa7835d57463d6bd51f53e1b8e850ccc.jpeg)
![images/ce8f90f45e6e1258cf85063beda89c62.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ce8f90f45e6e1258cf85063beda89c62.jpeg)
![images/cbbc7df11e722a9aca460d7b4e7fd3be.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/cbbc7df11e722a9aca460d7b4e7fd3be.jpeg)
![images/056083aabf4c355c3fc6e106923b29bc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/056083aabf4c355c3fc6e106923b29bc.jpeg)
![images/9d797d26647549b546d19f1f11f76524.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/9d797d26647549b546d19f1f11f76524.jpeg)
![images/2a47f267993175c93617cd37006e45b7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/2a47f267993175c93617cd37006e45b7.jpeg)

**"Ariel Yahni (UniKpty)"**

---
---
**Jim Hatch** *May 27, 2016 02:11*

Nice looking tube. Good on the box too. Handy for stuff in the garage 😃


---
**Ray Kholodovsky (Cohesion3D)** *May 27, 2016 02:16*

Any update on the original seller's response? Did you do a dispute?


---
**Ariel Yahni (UniKpty)** *May 27, 2016 02:21*

yes they offered me 200 but just yesterday they noticed i filed a dispute on paypal, so imagine if i need to wait something from them. I have not responded yet until i have them machine operational. This tube cost me 250 landed


---
**Alex Krause** *May 27, 2016 03:26*

What brand of tube did you buy?


---
**Ariel Yahni (UniKpty)** *May 27, 2016 03:31*

It's from AliExpress. Will post the link once it works. 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2016 07:01*

This is a really nice looking tube. Can't wait til you're up & running. Looks like the tube supplier took a lot of care in their packaging of the tube.


---
**Ben Walker** *May 27, 2016 14:12*

The lead filament and brass couplings.  WOW.  I did not expect to see such a nice looking replacement.  I look forward to you getting it to work and posting the seller details.  The one I ordered from Amazon Prime (at $286!) looked like a found object and didn't have any markings on it or nothing.  Looked very cheap.  Came in a paper box with bubble wrap.  I ended up just ordering a new machine for $40 more and sending it back.


---
**Ariel Yahni (UniKpty)** *May 27, 2016 15:09*

Will post seller soon,  trying to broker a discount code for the community, but as this guy's is only the reseller not sure how much I can get


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2016 17:06*

**+Ariel Yahni** That's a great idea, if the seller wants to make definite sales a discount for this community would be an ideal way for them to go & some free advertising at the same time.


---
**I Laser** *July 14, 2016 02:56*

**+Ariel Yahni** That looks way better than the replacement I received. Can you please post a link?


---
**Ariel Yahni (UniKpty)** *July 14, 2016 11:39*

**+I Laser**​ 700x50 size 40W CO2 laser tube warranty 3 monthes 2500-3000 Hours life span for 40W CO2 carving engraving machine DHL fast ship

 [http://s.aliexpress.com/U73iI7V3](http://s.aliexpress.com/U73iI7V3) 




---
**I Laser** *July 14, 2016 12:32*

Thanks **+Ariel Yahni**


---
**I Laser** *August 09, 2016 07:42*

**+Ariel Yahni** So hows your tube going a couple of months on? In the market for a replacement and will be purchasing one of yours if it's still going strong.


---
**Ariel Yahni (UniKpty)** *August 09, 2016 11:24*

**+I Laser**​ no issues at all till now


---
*Imported from [Google+](https://plus.google.com/+ArielYahni/posts/fhvY1RkhCo4) &mdash; content and formatting may not be reliable*
