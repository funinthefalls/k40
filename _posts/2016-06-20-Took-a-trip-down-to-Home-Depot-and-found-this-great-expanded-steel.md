---
layout: post
title: "Took a trip down to Home Depot and found this great expanded steel"
date: June 20, 2016 01:52
category: "Discussion"
author: "Evan Fosmark"
---
Took a trip down to Home Depot and found this great expanded steel. So much better than the terrible bed that comes with these machines! Only cost $10 for the material. Then I just needed to cut it down to size. Still need to find better washers to better hold it in place, though.

![images/3274ab52e02ec5b312f28bb2bf658833.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3274ab52e02ec5b312f28bb2bf658833.jpeg)



**"Evan Fosmark"**

---
---
**3D Laser** *June 20, 2016 02:08*

Exactly what I did to start from there I made a manual z bed at Home Depot they have one inch washers with 1/8 inch openings that I used two per peg to hold it all together 


---
**Corey Renner** *June 20, 2016 02:49*

How big is the piece of steel that you get for $10?


---
**Evan Fosmark** *June 20, 2016 02:50*

12"x24"


---
**Evan Fosmark** *June 20, 2016 04:28*

Well, with this new bed I can't seem to cut with the same settings as before. Do I need to do some sort of adjustment due to the difference in height?


---
**HalfNormal** *June 20, 2016 04:30*

50 mm from lens to work


---
**Evan Fosmark** *June 20, 2016 04:37*

Ah, right now I'm about 58mm from the lens to the bed. Makes sense that it wouldn't be optimal then, because the original bed had an extra ~9mm of vertical height compared to the extended steel.



So I'm guessing I just need to make up that height with some spacers below the bed?


---
**HalfNormal** *June 20, 2016 04:40*

**+Evan Fosmark**​ I use slim wood pieces under items to raise them to the correct hight.


---
**Robert Selvey** *June 20, 2016 06:21*

Hey Evan,  I was wondering where you purchased your laser from for $380 all I can find is the older version without the digital display for around 350 and I would rather get the newer version.


---
**Evan Fosmark** *June 20, 2016 06:25*

**+Robert Selvey** It was a seller on ebay with the username "de-direktvertrieb". I think they're sold out, but when they get more in stock you can negotiate down to ~ $363. It shipped from California and only took a few days to be delivered.


---
**Anthony Bolgar** *June 20, 2016 07:27*

I use the same steel from Home depot


---
**Michael Audette** *June 20, 2016 14:16*

Just make sure it's not galvanized.  If it is you might want to find something else.


---
**Corey Renner** *June 20, 2016 18:11*

Michael, what's your concern with galvanized?  I know the zinc makes it dangerous to weld, but I wouldnt expect that the laser would be able to get it hot enough for anything bad to happen


---
**Michael Audette** *June 20, 2016 18:34*

Off gassing when heating/welding...the laser burns., the Galvanized off-gasses.  There is an exhaust but I'd rather err on the side of caution rather than get the shakes.  Note that they do make this stuff that isn't galvanized or even made out of aluminum.  I'd opt for that if it's available over galvanized - even if it's a couple bucks more.


---
**Anthony Bolgar** *June 20, 2016 18:39*

The one I got is just mild steel. I wrapped it in an aluminum frame. It is big enough to make 2 cutting beds from it.


---
**Evan Fosmark** *June 20, 2016 18:44*

I doubt it's galvanized. One of the advertized uses of the extended-steel was for use in outdoor grilling, and I can't imagine something labeled for food would be galvanized. I'll double-check when I get home, but I'm fairly certain it's not.


---
**Michael Audette** *June 20, 2016 18:46*

Likely the case...I haven't checked my local HD lately but you setup is looking pretty nice.


---
**Dave M** *June 20, 2016 20:05*

**+Evan Fosmark** you might know this already, but the washers you might want to try are called "fender washers".


---
**Evan Fosmark** *June 21, 2016 07:14*

Got the z height set up again properly this evening. Had to cut back the intake vent considerably, in order to support the height. 



Seems to be working really well so far.


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/PM8Mx3WhNHF) &mdash; content and formatting may not be reliable*
