---
layout: post
title: "Hello. I have been playing around with my K40 for a little while, learning the \"ins and outs\" of using it"
date: June 23, 2016 18:14
category: "Discussion"
author: "Richard"
---
Hello. I have been playing around with my K40 for a little while, learning the "ins and outs" of using it. I think I have a pretty good grasp of the engraving part, but I cannot for the life of me figure out how to make it cut anything. I am still using the stock CorelLaser/CorelDraw that came with the machine. I have no clue how to use InkScape although I do have it installed. I was trying to use the attached image to test out cutting a silhouette of some sort that has an inside cutout, but no matter what I try it never even shows up on the preview when I click the cut button, and if I hit Start anyway it instantly says it's done. Can somebody please explain (or give a link to) how I can make this work? Thank you in advance.

![images/467bf7b31a3d95229b9796475bc3fd81.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/467bf7b31a3d95229b9796475bc3fd81.jpeg)



**"Richard"**

---
---
**3D Laser** *June 23, 2016 19:00*

What type of image is it


---
**Alex Krause** *June 23, 2016 19:06*

Are you wanting to leave the black areas and cut out the white?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 23, 2016 19:13*

I have had the same issue. It's usually when I do not have the object selected in CorelDraw when I click "cut" & then it auto-finishes & won't show up in the cut window either. Have you got the object selected?


---
**Jim Hatch** *June 23, 2016 19:42*

Assuming you want the black to be solid and the white to be cut out (the reverse would work too using the same concepts that follow), you'll want to put a little horizontal or vertical rectangle in the O so when you cut it out it doesn't drop the black center out too.



Then you need to create a outline around the black portions - you can do that by using Corel's trace tool. Copy those lines to a different layer. 



Send the cutline layer to the laser. You'll end up with a solid set of birds on a branch with the words cut out inside.



If you want it the other way where the black is the negative space (perhaps so you could use the stencil to spray paint a bunch of birds onto some surface) it will be a little more complicated because you'll need to create thin bridges from all the interior letters to the outside edges or they'll fall out of the stencil when it's cut. 


---
**Richard** *June 23, 2016 20:33*

Thank you. That worked great. I didn't even realize CorelTrace was there, it counts as a different program and I was trying to do everything inside CorelDraw.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 24, 2016 00:17*

**+Jim Hatch** That's interesting to know. I didn't realise that it had to be vector in order for it to cut (as I've never tried to cut a raster). I also would have overlooked the inside of the O.


---
**Phillip Conroy** *June 25, 2016 01:15*

try my 1 minute file conversion   [https://plus.google.com/113759618124062050643/posts/1YW6LuJWNYQ](https://plus.google.com/113759618124062050643/posts/1YW6LuJWNYQ)


---
**Craig** *August 24, 2016 09:47*

I've never been able to cut a line or vector drawind in Corel laser , only solid objects(will vector cut the outline and inside if any white area).  If I try to do a line it will treat it as a skinny rectangle and double cut.  Does anyone know if Crel laser/laserdrw will let you vector a line drawing?  For example, cut out a circle - the only way now is to draw a filled (black) circle and it will cut it out.  But what if I want to vector cut a smiley face? the outer circle keeps cutting tice(inside and outside the line)


---
**Phillip Conroy** *August 24, 2016 10:11*

Make sure the lines are very fine off hand 0.01mm if any thicker laser will cut both sides of line


---
**Jim Hatch** *August 24, 2016 12:27*

+1 I use a .001mm stroke thickness for lines in Corel. I think others use .01 or "hairline". I'm not sure where the threshold is for the laser to go from vector to double cut or treat the line as a box that gets rastered. I expect it's somewhere in the 0.1mm area because that's about the laser cut kerf. 



I use a couple of lasers and the .001 setting works for all of them so I don't have to mess with the file when using it for a different laser.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 24, 2016 14:00*

I used 0.01mm stroke thickness in Corel without it doing double lines. I guess anything around there or smaller will suffice.


---
*Imported from [Google+](https://plus.google.com/117522165219520060657/posts/Ck3Ky2RHsVY) &mdash; content and formatting may not be reliable*
