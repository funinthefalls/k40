---
layout: post
title: "Water cooling pumps, Standard pump is 2000L/H, is this sufficient?"
date: February 18, 2017 14:01
category: "Modification"
author: "Rob Kingston"
---
Water cooling pumps, 



Standard pump is 2000L/H, is this sufficient? what have people upgraded to flow rate wise?



Thanks,



Rob.





**"Rob Kingston"**

---
---
**Dave Durant** *February 21, 2017 17:45*

I've been wondering about this, too. 



My stock pump doesn't seem to have much oomph and I still have good-sized bubbles in the tube, even if I raise one end up and have the machine at 45 degrees or so. That's got me thinking that maybe a replacement would be good.



Tips on what what to look for in a replacement would be appreciated!


---
*Imported from [Google+](https://plus.google.com/104868032632666700277/posts/E9T8Bdej6uc) &mdash; content and formatting may not be reliable*
