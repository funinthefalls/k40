---
layout: post
title: "Guys, i cut 3mm/sec at 30% power"
date: April 13, 2017 02:20
category: "Discussion"
author: "Marvin Pagaran"
---
Guys, i cut 3mm/sec at 30% power. I'm getting circle on top and oblong in the bottom. Any advise?



![images/fdd5ad76a715cc567e386cdf6c85c2c3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fdd5ad76a715cc567e386cdf6c85c2c3.jpeg)
![images/b566df906369cf063c3cc6a3cbd2a40d.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/b566df906369cf063c3cc6a3cbd2a40d.jpeg)
![images/3e2c64fe1a90c2bf0c1243ce7ff14163.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3e2c64fe1a90c2bf0c1243ce7ff14163.jpeg)

**"Marvin Pagaran"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *April 13, 2017 05:50*

How thick is the material? You want to try focus the lens to 1/2 way through the thickness of your material. So from the base of the lens to the centre of the material thickness it should be 50.8mm (2") if you are using the original lens.



Also, another option to check is whether the moving mirror (Mirror #3) is causing the beam to hit the bed on an angle. You could check this by placing some masking tape on the bed, drawing an outline the shape of your wood scrap, then place a scrap of wood in the outline you drew & test fire. Test fire again with the wood removed. Compare where the 2 dots hit in the outline (on bed & on wood) to see if there is a significant difference. If so, the beam is not hitting the bed perpendicular & may need some adjustment.


---
**Marvin Pagaran** *April 14, 2017 02:08*

Thanjs will check on that


---
*Imported from [Google+](https://plus.google.com/107482131084712765082/posts/eNmtUbK9L5i) &mdash; content and formatting may not be reliable*
