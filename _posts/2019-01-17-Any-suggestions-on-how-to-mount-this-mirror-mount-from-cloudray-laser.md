---
layout: post
title: "Any suggestions on how to mount this mirror mount from cloudray laser?"
date: January 17, 2019 16:21
category: "Modification"
author: "Sean Cherven"
---
Any suggestions on how to mount this mirror mount from cloudray laser?



I'm not sure I feel comfortable drilling holes...



![images/dfb734a072f3c6e683905e5f13438fd1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/dfb734a072f3c6e683905e5f13438fd1.jpeg)
![images/24ea50560f2e832794ffb65535c51c47.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/24ea50560f2e832794ffb65535c51c47.jpeg)

**"Sean Cherven"**

---
---
**Tony Sobczak** *January 17, 2019 18:16*

Following




---
**Anthony Bolgar** *January 17, 2019 18:36*

U can always use some good two sided tape like the 3M brand stuff? But seriously, it is only 2 holes. If you don't feel that comfortable drilling them, maybe a friend or relative could do it for you?




---
**Sean Cherven** *January 17, 2019 18:39*

Actually, it's 4 holes. The original two holes do not align.


---
**Sean Cherven** *January 17, 2019 19:28*

And I don't mind drilling the holes, I'm just worried about getting them aligned correctly.


---
**James Rivera** *January 17, 2019 22:35*

One idea: Measure the existing 2 holes. Measure the holes on the new mirror mount.  Draw them up in Inkscape so you can center the new mount over the old position. Print it on paper. Position the existing holes on the paper over the real ones, then use the new holes on the paper as a drilling guide.


---
**Stephane Buisson** *January 18, 2019 08:38*

Keep in mind you want to be parralell to the tube/box, so you can use a block or a square to align the drill bit.


---
**Don Kleinschnitz Jr.** *January 18, 2019 12:09*

I concluded that I would not get the holes in the right place to enable the adjustments I wanted. Therefore I elected to use the existing holes to mount a subassy that contains the mirror on a subplate that is adjustable. 



To get the beam in the center you need to consider the range of adjustment  in 3 axis;

..coarse rotation, 

..front to back of machine,

 and elevation of the mirror. 

(I set the elevation precisely with the material thickness. The new mirror was to low relative to the lasers center line when mounted on the floor like the stock mirror)



The screws that hold the stock mirror assy. are a bugger to get to [under the laser compartment] so I used a nut plate to make it easier to install. 



[https://photos.app.goo.gl/nZqhUPECDXmuSMP38](https://photos.app.goo.gl/nZqhUPECDXmuSMP38)



[https://photos.app.goo.gl/K4ncdcENRfMJXKA1A](https://photos.app.goo.gl/K4ncdcENRfMJXKA1A)



[https://photos.app.goo.gl/m1ojeNStr8YBHKxB7](https://photos.app.goo.gl/m1ojeNStr8YBHKxB7)



[donsthings.blogspot.com - Improving the K40 Optics: Mirror #1 replacement](https://donsthings.blogspot.com/2018/11/improving-k40-optics-mirror-1.html)


---
*Imported from [Google+](https://plus.google.com/+SeanCherven/posts/6jeryeLfAQX) &mdash; content and formatting may not be reliable*
