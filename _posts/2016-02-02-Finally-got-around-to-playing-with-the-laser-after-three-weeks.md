---
layout: post
title: "Finally got around to playing with the laser after three weeks..."
date: February 02, 2016 00:27
category: "Materials and settings"
author: "Todd Miller"
---
Finally got around to playing with the laser after three weeks...



Scott Thorne shared an image, so I gave it a try on my system.



The left image is using 3.05mm hobby plywood

and the right image (larger) using 3.15mm Bass wood.



The Bass wood is softer,  an burns darker so maybe I'll

stick with the plywood, but it was all I had for material.



Also, the Bass wood image was etched on it's side

(portrait) with the grain as opposed to the smaller

image where I was etching against to grain.

![images/51214b5cc7336ff77c9408957d49e85e.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/51214b5cc7336ff77c9408957d49e85e.jpeg)



**"Todd Miller"**

---
---
**I Laser** *February 02, 2016 00:52*

Both look pretty sweet to me :)


---
**Joseph Midjette Sr** *February 02, 2016 02:29*

Both are awesome but I am leaning towards the Bass wood. It is more defined. Great Work!


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 02, 2016 06:29*

I agree with Joseph. The Bass wood seems to be a much more defined and crisp image. Could possibly be related to etching with the grain. Nice work on both pieces.


---
**Scott Thorne** *February 02, 2016 11:00*

Looking good man....sweet job.


---
**Justin Nguyen** *February 07, 2016 22:48*

how do you measure the size on CorelLaser corresponded with actual size of plywood. I often get engraving/ cut out of the plywood. thanks


---
*Imported from [Google+](https://plus.google.com/104261497719116989180/posts/c5FNZuCCuy2) &mdash; content and formatting may not be reliable*
