---
layout: post
title: "I just got my K40 today. It came with a USB thumb drive but its empty..."
date: June 15, 2015 17:49
category: "Discussion"
author: "Bruce D (Cam & Dad)"
---
I just got my K40 today.

It came with a USB thumb drive but its empty... Any idea what was suppose to be on it?





**"Bruce D (Cam & Dad)"**

---
---
**Jon Bruno** *June 15, 2015 17:50*

That is a software key.

It's not a storage device but is required to use the laserdrw suite.

There should be a disk hidden somewhere in the machine.


---
**Bruce D (Cam & Dad)** *June 15, 2015 17:59*

Thank you I found the disc and  laser Drw seems to open and work. I left the Thumb drive plugged in


---
**Jon Bruno** *June 15, 2015 18:01*

Laserdrw will open and you'll be able to poke around but the dongle is needed if you try to send anything to the laser controller.

Glad you found it. Have fun with your new toy!﻿


---
**Bruce D (Cam & Dad)** *June 15, 2015 18:02*

Thank you :)


---
**Eric Parker** *June 15, 2015 20:38*

Yup, that little USB device does 2 way cryptographic hashing to secure the program.  DONT LOSE IT.



I used one of the red ribbons that came in the laser to put through the keyring hole on top, so it has nice visible streamers if I ever decide to put it in a drawer.  My advice is to tie it to the usb cable.


---
**Jim Root** *June 15, 2015 22:39*

I leave my dongle in a usb slot on the back of the computer. Out of sight out of mind. 


---
**Eric Parker** *June 19, 2015 23:40*

**+Josef de Joanelli** True, if it were a storage device, but in this case, it is an HID device.


---
**Josh Smith** *June 01, 2016 15:35*

I just bought on on Gumtree - it came with the dongle but no CD! Im so confused :(


---
*Imported from [Google+](https://plus.google.com/104630767939271001198/posts/NzPyn8rW6WU) &mdash; content and formatting may not be reliable*
