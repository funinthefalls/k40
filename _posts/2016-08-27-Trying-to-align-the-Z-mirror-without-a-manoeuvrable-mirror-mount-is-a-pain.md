---
layout: post
title: "Trying to align the Z mirror without a manoeuvrable mirror mount is a pain!"
date: August 27, 2016 15:25
category: "Original software and hardware issues"
author: "Anthony Santoro"
---
Trying to align the Z mirror without a manoeuvrable mirror mount is a pain! 

Currently I am packing the edges of the Z mirror mount with tiny pieces of cardboard to align the beam, this can't be the best option. What do you guys use? What is recommended to be the least time consuming and most simple way to align the Z mirror? 





**"Anthony Santoro"**

---
---
**Ariel Yahni (UniKpty)** *August 27, 2016 15:35*

If you have such a big difference then I would say your trouble is elsewhere. I did have to shim it to make it square but that's it 


---
**Anthony Santoro** *August 27, 2016 15:38*

The X and Y mirrors are aligned perfectly, the beam is hitting the Z mirror right in the centre but the beam is directed to the very edge of the lens. 

My rough packing method has fixed the issue, but I'm sure there is a better method of aligning this mirror out there in this community. 


---
**Eric Flynn** *August 27, 2016 15:49*

Get some longer screws for the mounting plate , and put nuts on the screws under the plate.  This allows you to captivate the screws to the plate, and adjust using the screws.  This essentially makes a 3 point adjustment system for the head.  Work great.


---
**Anthony Santoro** *August 27, 2016 16:01*

I removed the lens and replaced it with some tape. From this I can see that the beam is hitting what would be the edge of the lens rather than the centre. The final mirror should be exactly 45° to the lens, but I found this not be to the case on this machine (my previous machine was fine). To adjust the alignment of the Z mirror I have manipulated the mirrors holder with some cardboard so the mirror is now 45 °, but there should be a better way to achieve this alignment than cardboard packing.

The 100w Chinese machines have adjustable Z mirrors, the k40 unfortunately does not have this luxury. (K40D may though) 


---
**Eric Flynn** *August 27, 2016 16:20*

Anthony, I just posted a good example of how you can do it without cardboard.


---
**Anthony Santoro** *August 27, 2016 16:24*

Thanks for that **+Eric Flynn**​! I appreciate your advice. 

I will give this a try. 


---
**Anthony Santoro** *August 28, 2016 05:49*

Your method worked great! Thanks for the advice **+Eric Flynn**​


---
*Imported from [Google+](https://plus.google.com/106649411786258336023/posts/M4kyAwmHopB) &mdash; content and formatting may not be reliable*
