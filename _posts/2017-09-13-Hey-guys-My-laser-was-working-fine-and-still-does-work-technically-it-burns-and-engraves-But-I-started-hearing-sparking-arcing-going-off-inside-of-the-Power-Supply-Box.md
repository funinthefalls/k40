---
layout: post
title: "Hey guys! My laser was working fine and still does work technically (it burns and engraves) But I started hearing sparking/arcing going off inside of the Power Supply Box"
date: September 13, 2017 23:46
category: "Original software and hardware issues"
author: "Marc Beckwith"
---
Hey guys! My laser was working fine and still does work technically (it burns and engraves) But I started hearing sparking/arcing going off inside of the Power Supply Box. I took off the top (after I disconnected it and took a picture of where the burn marks. 



Any help at all would be awesome! I wanna know what causes this and how i can fix it because it isnt a once in a while type of thing, it happens every time and continuously as the laser is running.



the burn mark is on the Red wire. Where the three lines connect to the main board

![images/aa99ff96fad6bfa1fa9d31f65b1a03d3.png](https://gitlab.com/funinthefalls/k40/raw/master/images/aa99ff96fad6bfa1fa9d31f65b1a03d3.png)



**"Marc Beckwith"**

---
---
**Don Kleinschnitz Jr.** *September 14, 2017 04:14*

Be advised that the LPS creates <b>LETHAL</b> voltages and exposing the internals or running without interlocks and/or proper mounting can be <b>life threatening</b>



.........

That plug is the drive connection to the HVT and should not be arcing. 



There are many potential reasons for this condition including a failing HVT or tube.



Its hard to see even zooming in the picture what the actual wire looks like. 

1. How old is the power supply?

2. How old is the tube?

3. When you test fire does the laser current fluctuate?

4. Does it arc if the LPS is properly mounted with its screws?


---
**Joe Alexander** *September 14, 2017 09:54*

that's some serious arcing, I second don's note about being safe. Side note: that a duracell battery just rollin' around bottom left? :P


---
**Don Kleinschnitz Jr.** *September 14, 2017 11:47*

I also just noticed some "black" potting or rtv around the HVT coil. Was this added.....?


---
**Marc Beckwith** *September 14, 2017 13:18*

I am definitely aware of the high voltage! I have not operated it without it being in the correct place and all covers on!



The laser does not fluctuate in voltage when I hold the test button down. However, the longer I am holding it, the more I can see/hear it arcing inside of the power supply. The last time it arced I took the cover off and noticed this burn mark. 



Here is a better picture 

![images/679dcf0698e3ff807b752f90525b9a82.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/679dcf0698e3ff807b752f90525b9a82.jpeg)


---
**Don Kleinschnitz Jr.** *September 14, 2017 13:38*

I am totally guessing.... that the HVT is going bad. Are there any chared or stressed (heat) components on the board. Look carefully on the left side.



What about the potting question See last post?


---
**HP Persson** *September 14, 2017 13:47*

Seen this before on many machines, it has been the bad HVT causing it. Replacing that one will fix the issue.

On aliexpress you can search for "40w laser flyback" to find some of the listings, just make sure it is visually identical to yours, as there is different models.


---
**Marc Beckwith** *September 14, 2017 13:54*

**+Don Kleinschnitz** 

The tube and the power supply are about 6 months old, not that much use on them. 



It does arc when everything is connected and properly placed.



The laser does not fluctuate in voltage






---
**Bill Parker** *September 14, 2017 16:46*

To me the burnt part is on the primary side so where the above post says look for the flyback cable I feel that is wrong. The connections are on the low voltage side of the unit and could it not just be the crimps on the cables are not crimped tight enough and allowing it to arc in the crimps?. Just my thoughts on it I'm no expert on these units but having been an industrial electrician for 40 years I have seen many crimped cables failed just due to not being crimped tight enough. 


---
**Don Kleinschnitz Jr.** *September 14, 2017 20:00*

**+Bill Parker** it could be a crimp but they actually look fine from what the picture shows. Yes this is the primary side and although it is lower voltage than the secondary it is still 400-600V and it will arc. 



**+Marc Beckwith** let the supply rest with the input power unplugged for 30 min. Then unplug that plug and inspect it for any damage, poor crimps or missing insulation. Plug it back in and separate the wires as much as possible from each other or put some silicon on them. 



However, I suspect that there is a HV leak between the secondary and primary.   


---
**Marc Beckwith** *September 14, 2017 21:03*

**+Don Kleinschnitz** 



Thank you Don and Thanks everyone else for all of the help and support!



I checked the crimps, the only wire that seems to be a little loose is the black one (but not very). 



What exactly is this "insulation that I should be checking, and what should i be checking it for?


---
**Don Kleinschnitz Jr.** *September 14, 2017 22:46*

**+Marc Beckwith** I meant like nicks in any of those wires.


---
**Marc Beckwith** *September 15, 2017 07:25*

**+Don Kleinschnitz** Ohh ok, damn. I checked that at first, and the insulation was good




---
**Marc Beckwith** *September 15, 2017 07:26*

I ordered a new flyback for it! lets see if this fixes the problem




---
**jonathon Hendrickson** *January 11, 2018 04:26*

New to the group hello everyone I have a 40 watt in a 50 watt power supply that both failed the 40 watt will only provide 3 milliamp the 50 does nothing is there anyway to diagnose the problem with the power supply other than throwing a new flyback at it I would like to troubleshoot it before just replacing a part and hope so that's the problem


---
**Don Kleinschnitz Jr.** *January 11, 2018 15:49*

**+jonathon Hendrickson** there really isn't a good way to test the supply other than creating an arc from the output to gnd. 



However this is a scary and dangerous thing to do that can even damage the supply if its good. Unfortunately there isn't other ways we have found to test them as the voltage is very high 20,000V.



If you can draw a strong arc then the problem is probably the tube not the LPS. If not its likely the LPS HVT.



Search the forum for other posts on the subject, look at my blog and then decide your approach. If you need help setting up an arc let me know. 



BE VERY CAUTIOUS AS THESE ARE LETHAL VOLTAGES and as always unless you know how to troubleshoot HV and associated safety, I do not recommend attempting an arc.



If you need more help let us know ...

My bet is that it is the supply's HVT which isn't to expensive to try.The good news is if it turns out not to be the supply you will eventually need an HVT anyway.


---
**jonathon Hendrickson** *January 11, 2018 15:53*

I'm looking at testing the low-power side of the power supply so before it gets to the flyback I know the flyback can get very dangerous but there's got to be a way to test everything up to the flyback. The problem is timing I have 13 Arrow of Light awards that I need to do for Boy Scouts I just ordered one power supply that I waited 10 days for it to arrive and they sent me a dud in or trying to claim that solder fell off in shipping

![images/80a492ae4091ebce06d85c0820616b57.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/80a492ae4091ebce06d85c0820616b57.jpeg)


---
**jonathon Hendrickson** *January 11, 2018 15:53*

![images/c29ec4521c09a6afe4be4bd92b3da0b3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/c29ec4521c09a6afe4be4bd92b3da0b3.jpeg)


---
*Imported from [Google+](https://plus.google.com/105917640032798246851/posts/7g8Xsxv6jQ4) &mdash; content and formatting may not be reliable*
