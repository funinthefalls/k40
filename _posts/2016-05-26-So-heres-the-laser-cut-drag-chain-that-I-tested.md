---
layout: post
title: "So here's the laser cut drag-chain that I tested"
date: May 26, 2016 20:03
category: "Object produced with laser"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So here's the laser cut drag-chain that I tested. I mounted it differently to how I've seen most drag chains mounted as this one has too much slack & it droops when mounted in the usual sideways position. I just created a plywood mount that attaches to the far end of the Y-rail (which was useful to hold my webcam also).



Here's a youtube video of it in action: 
{% include youtubePlayer.html id="Dn5xL4tKNd8" %}
[https://youtu.be/Dn5xL4tKNd8](https://youtu.be/Dn5xL4tKNd8)



Video might not be live for a little bit as it is still uploading. If it's not working when you check, try again a bit later.



![images/5db7ac6269276f05611cdf4078070855.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5db7ac6269276f05611cdf4078070855.jpeg)
![images/40ba3c288c960398f7f787741a3d1bb3.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/40ba3c288c960398f7f787741a3d1bb3.jpeg)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Tony Sobczak** *May 26, 2016 22:25*

Will you share the cdr?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 22:37*

**+Tony Sobczak** I used the svg that was on the thingiverse files, but I can create CDR that will have the mounts as well. Although I am assuming for the mount that goes on the rail (where I have webcam) you may not need the webcam bit so you could just make it a single rectangle rather than an L shape. I'll post link here shortly for you.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 26, 2016 23:04*

**+Tony Sobczak** I modified the mount that holds the cam so it is just to hold the drag chain. You may need to do extras of the links depending on how long you need. Keep in mind this is more a temporary solution than a permanent one due to the slackness in the chain.



[https://drive.google.com/file/d/0Bzi2h1k_udXwQUY3a2pnamtJOTA/view?usp=sharing](https://drive.google.com/file/d/0Bzi2h1k_udXwQUY3a2pnamtJOTA/view?usp=sharing)


---
**HalfNormal** *May 27, 2016 03:47*

Here is one I have thought about making

[http://www.cnczone.com/forums/videos/172959-cnc.html#post1244105](http://www.cnczone.com/forums/videos/172959-cnc.html#post1244105)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *May 27, 2016 06:56*

**+HalfNormal** That's a pretty cool design. It looks like it has a really wide radius for a full 180 degree bend, so maybe could do with scaling down for inside the K40. As it is, it's probably good for CNC machine where you don't have space restrictions from the case/frame/lid.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 03, 2016 01:35*

This version turns out to be a failure. I had a major catastrophe with it yesterday while engraving where it must have locked up or something & the the laser head was getting caught & jumping all over the place, totally ruining the piece I was working on. Need to get a proper solution asap I think.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/hrAVh7KRc1c) &mdash; content and formatting may not be reliable*
