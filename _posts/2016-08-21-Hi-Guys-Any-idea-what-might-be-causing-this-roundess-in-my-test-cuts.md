---
layout: post
title: "Hi Guys Any idea what might be causing this roundess in my test cuts?"
date: August 21, 2016 16:57
category: "Discussion"
author: "josh hatfield"
---
Hi Guys



Any idea what might be causing this roundess in my test cuts?



The two axises have been calibrated to 0.1mm accuracy 



I am using grbl 0.9j with a G-shield--Laserwbe3 is used to generate the G-Code.



In the attached photo you can see that the corners are rounded and that there appears to be a convex line on each edge where their should be a straight line



The G-Code File is here

[https://drive.google.com/open?id=0B8pvPiWhMwS9YXQ5VzRCRHhYdVk](https://drive.google.com/open?id=0B8pvPiWhMwS9YXQ5VzRCRHhYdVk)



The photo is here

[https://drive.google.com/file/d/0B8pvPiWhMwS9VEl4LTctdmJaSmM/view?usp=sharing](https://drive.google.com/file/d/0B8pvPiWhMwS9VEl4LTctdmJaSmM/view?usp=sharing)



I am interested to here what you guys think!





**"josh hatfield"**

---
---
**Ariel Yahni (UniKpty)** *August 21, 2016 17:06*

Can you share the file used to generate the gcode and a screen capture of the preview in Laserweb? 


---
**Ariel Yahni (UniKpty)** *August 21, 2016 17:13*

gcode is fine, just tested it. At what speed did you run the cut? 




---
**josh hatfield** *August 21, 2016 17:33*

10 mm/s I belive


---
**Ariel Yahni (UniKpty)** *August 21, 2016 17:41*

My only thougth at this point would be to much heat at the corner due to very slow speed.  File and gcode seems fine


---
**josh hatfield** *August 21, 2016 17:41*

Interesting point! . I shall a faster cut


---
**josh hatfield** *August 21, 2016 18:10*

Just did a test cut at 20mm's and I notice, that when viewed from the side the beam appears to have cut at an angle.. The bottom of the wood had the same curved edges but when I looked at the top of the cut the edge where straight.



Initial problem ==Solved! =P



New Problem!



How do I get the beam to cut straight...Could this prehaps be an issue with bed height..Maybe an more unfocused beam is hitting the wood in the wrong place


---
**Ariel Yahni (UniKpty)** *August 21, 2016 18:25*

ok that means the head itself is at an angle. The gantry / head must be as close as perpendicular to the cutting surface. Inspect and measure at different point to find whats causing the angle


---
**josh hatfield** *August 21, 2016 18:30*

Ok Will Do.. Should I make test cuts at differents points on the Y axis to see if the angle changes?


---
**Ariel Yahni (UniKpty)** *August 21, 2016 18:53*

Really not needed, if the angle is very steep then you should easily see where the problem is. At the end the gantry/head and the bed/produxt being cut must be parallel to each other


---
**josh hatfield** *August 21, 2016 18:54*

Ah I understand now. Thanks for your help!


---
**josh hatfield** *August 22, 2016 20:01*

Just did some more tests. Cuts weren't nearly as angled. Curves are still there. Did the g-code file cut straight on your machine?


---
**Ariel Yahni (UniKpty)** *August 22, 2016 20:16*

I did not run the actual cut since by just inspecting the gcode I see straight movement with no arcs


---
**josh hatfield** *August 22, 2016 20:19*

Ah shame. I might try different materials to see if its the plywood causing an issue



Also I will try making the cut over several passes maybe that will reduce the warping (probably not =P)


---
**Ariel Yahni (UniKpty)** *August 22, 2016 20:25*

If you don't have air assist then to much heat will burn the wood faster, corners at slow speed can easily cause that.  Try the same at much faster speed but multiple passes it should not see change in tone on the surface of the wood. I can try your file if it's makes you happy :) 


---
**josh hatfield** *August 22, 2016 20:25*

Nah it's fine. Thanks though. It is probably like you said due to the material burning too fast


---
*Imported from [Google+](https://plus.google.com/100555832645554213708/posts/M7MAhyn3u7z) &mdash; content and formatting may not be reliable*
