---
layout: post
title: "Almost ! I can home the X axis but the Y gets stuck and I can tell the settings for the X are not correct still"
date: October 12, 2015 23:01
category: "Smoothieboard Modification"
author: "David Cook"
---
Almost !   I can home the X axis  but the Y gets stuck  and I can tell the settings for the X are not correct still.   I do not have any jumpers installed for the step settings,  so Ill find some jumpers and retest in a moment.  getting close to having full control over this laser tube  ( well motion control at least,  still need to figure out the fire and PWM control settings.





**"David Cook"**

---
---
**Ashley M. Kirchner [Norym]** *October 13, 2015 00:42*

Until you can have it make coffee in the morning, drive you to the office, get you lunch, make dinner, bring you home after work ... you don't have full control. :)


---
*Imported from [Google+](https://plus.google.com/+DavidCook42/posts/DpLBekSgdFa) &mdash; content and formatting may not be reliable*
