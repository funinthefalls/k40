---
layout: post
title: "Here's my issue. Used it for around 6 months, absolutely fine"
date: October 11, 2015 13:50
category: "Hardware and Laser settings"
author: "george ellison"
---
Here's my issue. Used it for around 6 months, absolutely fine. Woke up this morning, pressed the test button....


**Video content missing for image https://lh3.googleusercontent.com/-8TXeXZio45A/VhpoXNu7QWI/AAAAAAAAAIg/uzI1wxE38L4/s0/video-2015-10-11-14-19-40.mp4.gif**
![images/238758ce865202e3566ecdf3192bc531.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/238758ce865202e3566ecdf3192bc531.gif)



**"george ellison"**

---
---
**george ellison** *October 11, 2015 15:11*

Going to take a look under the silicone seal, it may have work out and just need re-wiring, do you think think is the issue?


---
**george ellison** *October 11, 2015 16:45*

Hope so, getting it check out by an expect on Wednesday. Thanks again to everyone for their support ;)


---
**Joey Fitzpatrick** *October 11, 2015 16:48*

Usually this is caused by a crack in the tube.  The crack may be small and hard to see.  Look closely at the back side of the tube.  Usually happens at the spot where the anode wire sticks through the glass or at the inner cooling/water tube seam .  Sometimes water will leak into the outer area of the tube.  Hopefully it is just a bad connection


---
**george ellison** *October 11, 2015 17:59*

Does it matter if water leaks? can i do anything about this?


---
**Joey Fitzpatrick** *October 12, 2015 06:41*

If the tube is cracked, there is no fix that I know of.  You will need a replacement tube unfortunately 


---
**george ellison** *October 12, 2015 11:30*

Thanks, will update you.


---
**Maldenarious** *October 13, 2015 18:33*

That looks very similar to what mine was doing, for me it turned out that the rubber tube insulation that should be covering the connection was missing (the wiring was fine), i just siliconed some rubber tubing in place and all was well.


---
**Jon Bruno** *October 13, 2015 22:04*

Mine was a lightning storm when the tube cracked internally where the cooler tubes pass through each envelope.




---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/Aw9sXwJ1wW6) &mdash; content and formatting may not be reliable*
