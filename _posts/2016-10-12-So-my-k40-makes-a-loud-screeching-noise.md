---
layout: post
title: "So my k40 makes a loud screeching noise"
date: October 12, 2016 00:09
category: "Original software and hardware issues"
author: "giavonni palombo"
---
So my k40 makes a loud screeching noise. When It does I see that the beam is going toward the metal part in the tube towards the small brown wire. It does this sometimes, off on on. When it doesn't make that noise it cuts great, when it does it cant hardly cut 2mm cardboard at 6ma and 20mms.  Is the tube trashed?    





**"giavonni palombo"**

---
---
**giavonni palombo** *October 12, 2016 00:12*

![images/8b70a774fdfafd2aa521446c472615d7.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8b70a774fdfafd2aa521446c472615d7.jpeg)


---
**greg greene** *October 12, 2016 00:20*

I would say so, I suspect an intermittent short in the tube - causing the beam to deflect


---
**giavonni palombo** *October 14, 2016 13:41*

Are any of the ebay replacement tubes any good?



 I dont think I will replace with L.O. tube. I noticed now the 1 meter tube says "Note: This laser tube is not designed for the K40 machine. I know it use to say it was a replacement or upgrade. 


---
**greg greene** *October 14, 2016 15:02*

Let us know how it works out


---
*Imported from [Google+](https://plus.google.com/112184431791021587103/posts/U3wGvseiK3H) &mdash; content and formatting may not be reliable*
