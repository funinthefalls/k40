---
layout: post
title: "I have a 40w Chinese laser engraver and I am using Corel Laser"
date: December 31, 2016 23:40
category: "Discussion"
author: "Ronald Jansen"
---
I have a 40w Chinese laser engraver and I am using Corel Laser.  I am having problems engraving in all the areas of the available 10x8 work area.  If I engrave a 4x4 image in the upper left quadrant it works fine.  If I move the image to the upper right quadrant only part of the image is engraved.  Please see the attached picture.  I have checked the focal length in all areas and they are equal.  Any ideas?  Thank for any help......Ron



![images/3f8d4893489fac58d5838b07d14a24f6.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/3f8d4893489fac58d5838b07d14a24f6.jpeg)
![images/ca58e28986eedee414e38fdd0272a3b2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/ca58e28986eedee414e38fdd0272a3b2.jpeg)

**"Ronald Jansen"**

---
---
**Ariel Yahni (UniKpty)** *December 31, 2016 23:53*

**+Ronald Jansen**​ most surely the distance from the laser lens to the work surface is not the same at all points thus making the laser defocus and weak at the point of burn or you don't have a good alignment of the laser at point meaning is not hiting he last mirror in the same spot 



[http://not.hiting](http://not.hiting)


---
**3D Laser** *December 31, 2016 23:58*

When I first got my laser I spend hours trying to align the thing and it was the absolute best thing I could have done now I can engrave and cut along the entire working area of my k40.  Just make sure once you get it aligned you tighten back up those screws otherwise it will fall back out of alignment 


---
**Yuusuf Sallahuddin (Y.S. Creations)** *January 01, 2017 02:03*

I'd second (or third really) what Ariel & Corey have mentioned. I'd attribute this issue to poor alignment in all quadrants. Take your time & get it aligned as best as possible. You will benefit greatly for both engraving & cutting.


---
**3D Laser** *January 01, 2017 02:27*

You might have to square up the rails to get aligned right as well.  I had to take mine all apart to get it right but again well worth it 


---
**Ronald Jansen** *January 01, 2017 07:37*

Thank you, Ariel, Corey and Yuusuf for your posts.  I will go back to the beginning and align the mirrors again.  I will post the results here in a day or two.....thanks again.


---
*Imported from [Google+](https://plus.google.com/115654891619984544388/posts/JBxW9oQDHQi) &mdash; content and formatting may not be reliable*
