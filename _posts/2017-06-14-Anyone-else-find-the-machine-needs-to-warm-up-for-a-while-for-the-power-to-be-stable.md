---
layout: post
title: "Anyone else find the machine needs to warm up for a while for the power to be stable?"
date: June 14, 2017 18:53
category: "Original software and hardware issues"
author: "Frank Fisher"
---
Anyone else find the machine needs to warm up for a while for the power to be stable?  I have been riding the dial up and down for the first few minutes but after that it has been reasonably consistent.  I ordered a new pot for the dial a while back, still waiting.  Not sure if this is connected though.





**"Frank Fisher"**

---
---
**Frank Fisher** *June 15, 2017 16:02*

I have held the test fire button for nearly a minute with it fluctuating wildly between 2mA and 12mA on a whim, making little hisses as it drops to 2 then flying back up to 12 or more.  Is this normal?  By the second minute I had managed to get it down to 3.5mA fairly steadily.  I thought.  Then I turned my back and it flicked back up and set light to a piece of card I was cutting.  Seems solid 3.5mA now though. 


---
**Frank Fisher** *July 06, 2017 16:32*

<b>finally</b> got a replacement 10 turn pot.  It allows very fine control which I need.  BUT still needs a warm up.  I have to let it draw boxes very slowly on a tile for 10 mins or so then it bursts the power up and down and finally comes under control of the pot.  Any ideas?




---
**Frank Fisher** *July 06, 2017 16:36*

If it helps I finally achieve cuts at just 3mA, when some on here say that is very low,  but during the warm up it makes a lot of gentle hissing rather than getting a laser. Am I asking too much to reliably cut at that level?




---
*Imported from [Google+](https://plus.google.com/114228670405979897634/posts/31o6b6fKnG2) &mdash; content and formatting may not be reliable*
