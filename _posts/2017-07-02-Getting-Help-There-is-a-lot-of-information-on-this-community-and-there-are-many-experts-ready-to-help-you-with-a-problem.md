---
layout: post
title: "Getting Help There is a lot of information on this community and there are many experts ready to help you with a problem"
date: July 02, 2017 02:50
category: "Discussion"
author: "Don Kleinschnitz Jr."
---
#K40GettingHelp



<b>Getting Help</b>

There is a lot of information on this community and there are many experts ready to help you with a problem. Its courteous to provide enough information to enable the community to help you with the minimum of effort. When you post about a problem think of your problems definition from the perspective of those giving help. The troubleshooter has to gain a clear understanding of what you see and are experiencing before "help is on the way".



<i>See the problem description form in the "About Community" links section in the left hand panel of the community page. Fill out this form to help you provide complete information about your problem.</i>





Here are some hints on getting help:



1. Search the community and G+ with # and keywords for your problems before you post for help. 

_If you are someone that is trying to help it is frustrating when a problem's answer is easily searchable in the community and the asking member didn't bother to do any research.



2. Describe your environment using a structured format

Use a helpful format for communicating your problem. Use this on the first post of a thread and link to the first thread if you create additional threads on the same problem.



Make sure your first post has these:

.. #K40broke[description]  where "description" is a short term that describes the problem. Example #K40brokepot

.. Machine type: [K40, K50, DIY etc]

.. Controller type: [Smoothie, C3D, Arduino, etc]

.. Firmware: [Smoothieware, grbl, etc]

.. Firmware config: [link to config file]

.. Problem tenacity: [new problem, returning problem]

.. Suspected source of problem:[power transition, came with machine]

.. Symptoms: [what did you observe or test]

.. Photos/video:[photos of the subject part, engraving, motion, arc, layout, symptoms]. 

Hint: Videos and photos allow experts to notice things that you may not see. Example: " ...it looks like that power cable is wired backward"

 

3. In the dialog of a problem thread sequentially number questions. Respond with answers that are identified by the same number.

This eliminates confusion and the need to repeat questions

.....

Example question:

1. What is your mains power

......

Example answer:

1. 240 VAC



3. When the problem is fixed post one last time, in the same thread, with the solution summarized. 

It is frustrating to someone that is helping when the final resolution to a problem is unknown and the requester just moves on. It also inhibits our ability to link cause and effect for the rest of the community.

Example: There was an arc between the anode of my laser tube and the case. I removed the anode connection and repaired it using this procedure [link] and that fixed the problem.





**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/6BdGctNgza3) &mdash; content and formatting may not be reliable*
