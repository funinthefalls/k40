---
layout: post
title: "Hello, I just ordered my K40, and planning on upgrading it on day one"
date: February 25, 2016 13:04
category: "Discussion"
author: "Jean-Baptiste Passant"
---
Hello,



I just ordered my K40, and planning on upgrading it on day one.



Which upgrades should I order ?



[lightobject.com](http://lightobject.com) have a lot of parts, so I prepared my cart :



[http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)



[http://www.lightobject.com/40W-CO2-laser-Mirror-Lens-DIY-bundle-P551.aspx](http://www.lightobject.com/40W-CO2-laser-Mirror-Lens-DIY-bundle-P551.aspx)





I also ordered :



- Ramps board

- Heatshrink

- Endstop

- Honeycomb bed



Is there anything I should add or change ?



Thank you !





**"Jean-Baptiste Passant"**

---
---
**3D Laser** *February 25, 2016 13:14*

You will need a air compressor for the air assist and a 18mm lens for it


---
**3D Laser** *February 25, 2016 13:21*

Also think about upgrading the exhaust as soon as possible that is what I am working on now


---
**Jim Hatch** *February 25, 2016 13:25*

I would caution against upgrading it "day one". Get it working out of the box first. Make sure it works so if there are any problems you can go back to the seller for a fix (or $ return). Once you modify, you can't ask them to fix anything that doesn't work.



Once it's all working in stock form, then go ahead and modify away. I would also suggest modifying one thing at once - e.g. add air assist get it working and then swap boards. If you do a bunch of things at once and it doesn't work then it's going to be much harder to troubleshoot.


---
**Jean-Baptiste Passant** *February 25, 2016 13:43*

**+Jim Hatch** You are right, will do that, once I have checked everything works fine, is there any upgrade I should do first ?


---
**Jean-Baptiste Passant** *February 25, 2016 13:44*

**+Corey Budwine** The 18mm lens is in the kit from lightobject. I will order an air compressor though, how could I forget ... Beside the exhaust, is there any upgrade I should do first ?﻿


---
**Jean-Baptiste Passant** *February 25, 2016 13:49*

Looks like the bundle is not available, would that be ok ? :

2 * [http://www.lightobject.com/Laser-Reflection-Mirror-Mount-P205.aspx](http://www.lightobject.com/Laser-Reflection-Mirror-Mount-P205.aspx)

3 * [http://www.lightobject.com/20mm-Gold-Plated-Reflection-Mirror-P116.aspx](http://www.lightobject.com/20mm-Gold-Plated-Reflection-Mirror-P116.aspx)

1 * [http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx](http://www.lightobject.com/Improved-18mm-ZnSe-Focus-lens-F508mm-P206.aspx)

1 * [http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx](http://www.lightobject.com/18mm-Laser-head-w-air-assisted-Ideal-for-K40-machine-P701.aspx)

Total comes to 110$ + shipping.


---
**Ben Walker** *February 25, 2016 17:27*

Fairly new to the K40 myself.  But the air assist and new lens will mark a huge improvement.  As far as a new board I am doing well with the LaerDRW board for the time being.  Thinking about the Smootie but have no committed yet.  If it aint broke.....




---
**3D Laser** *February 25, 2016 18:27*

My lens and my honey comb are on their way I can't wait I have been having problems getting all the way through the material


---
**Ben Walker** *February 25, 2016 19:04*

Check your lens Cory.  Mine had a fingerprint on it that I could not get off.  It was burned into the lens somehow - not mine mind you.  It arrived like that.  Then I learned that the lens must be concaved toward the top and the flat side down.  Check these areas.  I cut through 1/4 wood like butter at 12mms.  It's astonishing how much you will enjoy air assist.  The honeycomb bed is great, too.  But it seems to me it creates more smoke.  I think it is all the holes in the case.


---
**3D Laser** *February 25, 2016 19:23*

Yea I don't think I have mine completely focused.  I even jacked the power up to 25ma (then was told that was a really bad idea). And tried to cut through it and I can't get through 


---
**Tony Sobczak** *February 26, 2016 03:41*

I'm also looking at what I need to upgrade. Anyone have an air assisted with dual laser. Where did you get it from? 


---
**Stephane Buisson** *February 26, 2016 12:46*

**+Jean-Baptiste Passant** si tu prend le probleme dans l'autre sense, je veux dire par la, sotfware to hardware, choisie toi le logiciel qui te conviens le mieux, je dois dire que pour le moment c'est a mon gout Visicut, Laserweb arrive derriere a grand pas. pour cela il te faudra une smoothieboard, and cocorico tu peux l'avoir aupres de **+Arthur Wolf** direct de BREST. apres tu suis le tuto que j'ai mis en lien en haut de la page.

bienvenue a toi sur cette communaute.


---
**Arthur Wolf** *February 26, 2016 12:56*

**+Stephane Buisson** Merci pour le ping. **+Jean-Baptiste Passant** si tu as besoin d'aide avec quoi que ce soit en rapport avec Smoothieboard, je suis la pour ca.


---
**Jean-Baptiste Passant** *February 27, 2016 17:57*

**+Arthur Wolf** **+Stephane Buisson** Merci pour l'info ! Je vais chercher les infos pour la SmoothieBoard bretonne ;)


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/C91cWci5zWe) &mdash; content and formatting may not be reliable*
