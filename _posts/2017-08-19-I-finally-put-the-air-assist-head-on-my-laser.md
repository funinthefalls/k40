---
layout: post
title: "I finally put the air assist head on my laser"
date: August 19, 2017 20:01
category: "Air Assist"
author: "Doug Western"
---
I finally put the air assist head on my laser.  I am hoping to avoid the massive set up of an air compressor to blow this air.  Has anyone found an aquarium air pump or other simple set up that works and if so, do you mind sharing what you did.





**"Doug Western"**

---
---
**Don Kleinschnitz Jr.** *August 19, 2017 21:31*

**+Doug Western** this is a common subject and lots of recommendations can be found by searching the community.



[donsthings.blogspot.com - Don's Things](http://donsthings.blogspot.com/search/label/K40%20Air%20Systems)


---
**Steve Clark** *August 20, 2017 03:54*

They are all a little pricey…A lot of postings I’ve seen say they use this one:



[https://www.harborfreight.com/16-hp-58-psi-oilless-airbrush-compressor-60329.html](https://www.harborfreight.com/16-hp-58-psi-oilless-airbrush-compressor-60329.html)



I have no idea how well they work but others seem happy with them.



For the price I decided to buy this Porter-Cable instead as I found one reconditioned  on this site for 65 bucks including free ship. Don’t see any for that price now though.



[http://www.ebay.com/itm/Porter-Cable-0-8-HP-6-Gallon-Oil-Free-Pancake-Air-Compressor-C2002-Reconditioned-/282425425560?hash=item41c1ddfe98:g:8iAAAOSwJS5ZgoLA](http://www.ebay.com/itm/Porter-Cable-0-8-HP-6-Gallon-Oil-Free-Pancake-Air-Compressor-C2002-Reconditioned-/282425425560?hash=item41c1ddfe98:g:8iAAAOSwJS5ZgoLA)



One of these cheapo’s for 39 bucks from HF might work…they are 100psi compared to the one I have which is 150psi (I use it for my air stapler too)




---
**Paul de Groot** *August 20, 2017 05:43*

I bought an aquarium airpump on eBay for $15 and it works really well. The airflow is really tiny but enough to keep the lens and work piece clear of smoke. Not tried acrylic yet. It is meant to push the molten plastic out of the kerf.


---
**Jim Fong** *August 20, 2017 08:20*

**+Steve Clark** that will work but it is super noisy to run. I have a larger Porter Cable compressor  for my air nail guns.  I use a HF airbrush compressor and barely hear it run.  My exhaust fan is louder.  The laser is sometime run late at night and there is no way I would want to run a noisy compressor for several hours.  


---
**Don Kleinschnitz Jr.** *August 20, 2017 15:21*

**+Steve Clark** I had that air compressor and it did not work to well for me.


---
*Imported from [Google+](https://plus.google.com/102298503204290496961/posts/2ajB1jyJqbW) &mdash; content and formatting may not be reliable*
