---
layout: post
title: "I Just entered the world of laser engraving after buying a K40"
date: September 04, 2016 23:30
category: "Software"
author: "Jamie Lowe"
---
I Just entered the world of laser engraving after buying a K40. I just did the software install and I'm curious about CorelDraw, as I thought it was included. My software is asking for a Key to finish the install. Sounds like I would need to purchase a key to make it work. Is this the case?







**"Jamie Lowe"**

---
---
**Mike Mauter** *September 05, 2016 00:15*

There should be a file in the Corel12 folder called CDKEY.txt with the serial number. 


---
**Jamie Lowe** *September 05, 2016 00:36*

Thank you!


---
**HalfNormal** *September 05, 2016 00:49*

**+Jamie Lowe** You will need to block Corel from phoning home with your firewall. It will eventually stop working.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 05, 2016 02:24*

Not the most legit software I've ever been given with a product, but as HalfNormal says, it will try phone home & eventually tell you it is non-genuine. If you intend to continue using your best bet is to purchase a key at some point. The CorelLaser plugin works with most versions of Corel Draw (up to x8 from what I've read here) although some have had issues with Home & Student versions, so don't waste your $ on them until you verify with someone else here if it actually works.


---
**Brent Crosby** *September 05, 2016 14:19*

So basically the hardware vendor is shipping stolen software. 


---
**HalfNormal** *September 05, 2016 15:23*

**+Brent Crosby** Yep! It was duly noted in another post that they ship pirated software but have their software protected by a USB dongle!


---
**Vince Lee** *September 05, 2016 19:33*

**+HalfNormal** again i think we are talking about different parties here.  The people who make the oem controller boards and software (moshi or lihanyu?) are the ones who added the dongles but I think it is the no-name sellers that assemble the boxes that include the pirated software (tho mine did not).  


---
**HalfNormal** *September 05, 2016 21:41*

**+Vince Lee** You are correct. Two different parties involved. 


---
*Imported from [Google+](https://plus.google.com/101114189070867293080/posts/95QwthXhfue) &mdash; content and formatting may not be reliable*
