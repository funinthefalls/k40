---
layout: post
title: "Newbie question here. Got my machine set up, hopefully correctly, if there is a fine tune let me know"
date: October 09, 2017 08:38
category: "Hardware and Laser settings"
author: "Nik Green"
---
Newbie question here.

Got my machine set up, hopefully correctly, if there is a fine tune let me know.

Using K40 Whisper. Made artwork in Inkscape, small rectangle 0.313 thick line in RGB red.

Artwork in place in Whisper.

Press cut button, laser power set at 10mA and speed set at 5mms. Four passes and I get an engrave but not terribly deep.

Material is 3mm MDF, which on a GCC Spirit 60w is not a problem.

Next try laser power set at 20mA and speed set at 2mms. Three passes.

Again not very deep but this time the burn line is spreading.

Now I am assuming the fault is mine and I have missed something basic.

Does anyone have any ideas to help point me in the right direction?

Many thanks





**"Nik Green"**

---
---
**Printin Addiction** *October 09, 2017 14:29*

Check the mirrors, mine were very dirty to start with... 


---
**Ned Hill** *October 09, 2017 17:46*

Things to check.

1.) Make sure your have good alignment on all your mirrors.  There are a couple of alignment guides on here.  Search # K40alignment (without the space after the hashtag).

2.) Determine the correct focal height for your laser.  For sharp engrave and cutting the piece you are working on needs to be close to the focal point of your lens.  For sharp engraving the focus need to be at the surface and for cutting you typically set the focus half way through your material.  Most K40s come with a lens with a 50.8mm focal length.  

3.) Make sure your lens is oriented with the curved (bump) side up with the flat side on the bottom (toward workpiece). 


---
*Imported from [Google+](https://plus.google.com/107953065246056901111/posts/e8E9bgWYGTc) &mdash; content and formatting may not be reliable*
