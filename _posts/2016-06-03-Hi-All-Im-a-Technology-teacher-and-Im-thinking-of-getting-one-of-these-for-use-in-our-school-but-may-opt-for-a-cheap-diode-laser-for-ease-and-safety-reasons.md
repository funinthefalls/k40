---
layout: post
title: "Hi All, I'm a Technology teacher and I'm thinking of getting one of these for use in our school but may opt for a cheap diode laser for ease and safety reasons"
date: June 03, 2016 17:44
category: "Discussion"
author: "Nigel Conroy"
---
Hi All,

I'm a Technology teacher and I'm thinking of getting one of these for use in our school but may opt for a cheap diode laser for ease and safety reasons.



I've been reading up a bit on these, especially here.

One mod mentioned has been an adjustable height table and I thought of a video I saw that may give others some inspiration.




{% include youtubePlayer.html id="2RQcClMWeh4" %}
[https://www.youtube.com/watch?v=2RQcClMWeh4](https://www.youtube.com/watch?v=2RQcClMWeh4)



It's not for a laser cutter but the technique used to build the adjustable table is easy and clever. 



Hope it helps someone.



Any thoughts or input on a K40 versus a diode laser in a school setting are also welcome.





**"Nigel Conroy"**

---
---
**Lance Robaldo** *June 03, 2016 19:24*

One problem that immediately comes to mind with Diode lasers is eye protection, especially in a classroom setting.

 

Glass, Acryllic, lexan, etc.. are transparent to the frequencies that diode laser operate so they go right through it.  When they strike an eye, they can retinal damage. 



These same materials are opaque to C02 lasers.  So simple lab goggles will protect your students with C02 lasers whereas you need specialized eye protection for the diode lasers. 



If somehow the C02 laser does strike an eye, it damages the cornea, not the retina.  Obviously NEITHER is good.  But given the choice between the two...corneas can be replaced...


---
**Lance Robaldo** *June 03, 2016 19:29*

It is also very easy to wire in a shutoff switch so the C02 laser cannot fire if the lid is lifted.  



Most ( or all?) diode lasers that I've seen are "open" and have nothing to stop the beam from bouncing around the class.  For safety you'd want to build some sort of opaque enclosure.  Unfortunately that would also mean you couldn't see inside.


---
**Nigel Conroy** *June 03, 2016 19:54*

Thank **+Lance Robaldo** , I have obviously been thinking about safety. I was wondering about the enclosure and how I could make one for the diode laser. I was hoping I would be able to construct an enclosure with a sheet of material that could be used as a viewing window. 



The diode lasers I've seen come with the protective glasses. Does anyone know what material the lenses are made from and if its available in sheet format?



If I had a choice I would definitely go for a CO2 laser but I'm thinking this cheap option might take a lot more tinkering then initially thought to get classroom ready.


---
**Lance Robaldo** *June 03, 2016 21:24*

Most diode lasers would be around 980nm, but there are other diodes out there so you would need to know which one you get.  Plastic sheets for diode lasers are   e x p e n s i  v e.



Just do a Google search on "laser diode blocking plastic" and you'll find several suppliers.


---
**Nigel Conroy** *June 03, 2016 21:40*

Would the same be true for the k40 to place a blocking sheet on the viewing window?


---
**Lance Robaldo** *June 03, 2016 22:34*

The viewing window on a K40 is transparent to visible light, but totally opaque to the C02 laser.  So it's safe with no need for further protection.


---
**Lance Robaldo** *June 03, 2016 22:35*

That's why C02 lasers can cut and engrave acryllic, but diode lasers cannot, because the diode laser beam simply passess through without damaging.


---
**Christoph E.** *June 06, 2016 09:52*

Here you go: [http://dck40.blogspot.de/2013/01/cover-safety-switch.html](http://dck40.blogspot.de/2013/01/cover-safety-switch.html)



For a classroom I'd just setup a camera on top of it project it on the wall so everyone can sit down and watch. Nice Idea anyway. <3


---
**Nigel Conroy** *June 06, 2016 13:17*

Thanks for sharing **+Chris E** 


---
*Imported from [Google+](https://plus.google.com/+NigelConroy/posts/RrS9XqAd9H5) &mdash; content and formatting may not be reliable*
