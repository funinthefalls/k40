---
layout: post
title: "Made a couple of Christmas ornaments for my nephews and their girlfriends"
date: December 13, 2015 02:52
category: "Object produced with laser"
author: "Anthony Bolgar"
---
Made a couple of Christmas ornaments for my nephews and their girlfriends. It is engraved on some porcelain tags I found at a liquidation store. It engraved quite well at 40% power and 200mm/min speed.



![images/a9688d850f1ed081db1b38be7208cff1.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a9688d850f1ed081db1b38be7208cff1.jpeg)
![images/d1aa4180e39ef83f4252ab3f769679d4.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d1aa4180e39ef83f4252ab3f769679d4.jpeg)

**"Anthony Bolgar"**

---
---
**Scott Thorne** *December 13, 2015 10:50*

That turned out quite nice.


---
**Anthony Bolgar** *December 13, 2015 12:37*

Thank you, now that I know porcelain engraves well, I will be making some personalized coasters using 4" X 4" porcelain tiles of various colours.


---
**I Laser** *December 14, 2015 05:05*

Looks very nice. Is that silver paint infill?


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/cRwC4kCmuWb) &mdash; content and formatting may not be reliable*
