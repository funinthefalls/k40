---
layout: post
title: "Hi please help I brought my k40 laser cutter but it didnt come with the cd just the dongle"
date: October 21, 2017 16:21
category: "Software"
author: "sam johnson"
---
Hi please help

I brought my k40 laser cutter but it didn’t come with the cd just the dongle. I have downloaded K40 Whisperer and Inkscape. I can design on Inkscape but then an error code comes up when I try opening it up in K40 Whisperer 

Has anyone got any ideas on how to get this working so I can start using my cutter 

Thank you in advance 





**"sam johnson"**

---
---
**Printin Addiction** *October 22, 2017 00:40*

What is the error? Did you load the driver?


---
**Printin Addiction** *October 22, 2017 18:12*

Was it an error running k40 whisperer, or when trying to load the file?


---
**Scorch Works** *October 22, 2017 18:49*

I worked to resolve this with Sam on YouTube and e-mail.  In the end reinstalling Inkscape may have solved the issue.  It was not clear what the specific issue was but Sam is up and running now.  


---
*Imported from [Google+](https://plus.google.com/101831409148787022120/posts/SuGyWkLCG6m) &mdash; content and formatting may not be reliable*
