---
layout: post
title: "BUMMER! ( I apologize to all my non USA friends for the following statement.) I received a call today that my new 60 watt laser could be delivered tomorrow"
date: December 13, 2017 22:56
category: "Discussion"
author: "HalfNormal"
---
BUMMER!

( I apologize to all my non USA friends for the following statement.)



I received a call today that my new 60 watt laser could be delivered tomorrow. I placed my order on Ebay Sunday night 3 days ago! I was excited and said of course you can deliver it tomorrow, my wife will be home to receive it. Well she then reminded me that she would not be home at all tomorrow since she was going out of town for the day. "How about Friday?" I asked the dispatcher. Sorry, we only deliver in your area Tuesday and Thursday. Verifying that my wife would indeed be home next Tuesday, I scheduled the delivery of my new laser.



I just had to vent a little and of course I feel guilty about all the  people who have to deal with issues of customs, taxes ect when ordering anything not local. 



I feel better, thanks!.





**"HalfNormal"**

---
---
**greg greene** *December 14, 2017 17:41*

Glad we could help ! :)


---
*Imported from [Google+](https://plus.google.com/+HalfNormal/posts/CGinfQRg15z) &mdash; content and formatting may not be reliable*
