---
layout: post
title: "So noob here, I have a Kehui KH-3020 K40 machine"
date: February 23, 2017 16:01
category: "Discussion"
author: "Steven Whitecoff"
---
So noob here, I have a Kehui KH-3020 K40 machine. It came with the Nano controller board and I've found it seems to cut correctly with Corel setup for HP/GL2 data vs WMF, the suggested settings(found on here) which does not cut  correctly.  So that makes me wonder if it doesnt support other software or could at least if we had drivers.  Reading about the Smoothie boards I see where they have advantages but still seems the software situation is just trading one partial solution for another. Having to use 2-4 programs to get results seems a more of a cludge than the maching is :D.  Educate me please





**"Steven Whitecoff"**

---
---
**Steve Anken** *February 23, 2017 16:13*

It's a no brainer to me to use Smoothie and LaswerWeb, best workflow and active team improving it all the time. YMMV.


---
**Don Kleinschnitz Jr.** *February 23, 2017 16:24*

The key difference that got me here was the challenges associated with the closed nature of the stock K40's software/hardware tool path. With the stock controller you are locked to a proprietary controller and Corel. Try and get support or improvements for either .... :(



As long as you stay with the stock K40 software you can do SOME stuff ok. For example if all you are going to do is engrave dithered images that setup may work fine.



Corel is not a CAD tool and I found myself constantly tweaking the design or playing with formats to make it cut dimension-ally correct. 

  

As soon as you try and do things that come from other software's it gets even more clumsy, difficult or impossible. By other software's I mean CAD/CAM packages that offer much better design and fabrication interfaces. 



To make matters worse you have no way of knowing what is not working, the original design, the design software or the machines controller because Corel communicates with the controller via proprietary means and you cannot see what the controller is seeing.



Moving to a G-code based controller opens up many options to use open source tools in your tool chain. Any given project may use more than one tool but not because of format problems rather to take advantage of various functionality.



That's my story and I am sticking with it ...:)




---
**Ray Kholodovsky (Cohesion3D)** *February 23, 2017 18:26*

Well it's not quite the same thing as hitting print from Microsoft Word.  Pretend that your paper printer could print print your picture, or it could cut it out.  Doesn't it make sense that you'd need some way of telling the printer which you thing you want it to do?  Now add the fact that it can print on paper, cardboard, wood, tile and anything else... More settings that it needs to be told... And to some extent this is already the case with the dialog box that pops up when you hit print.   

ANYWAYS.  The right way to do this is you have an image or a CAD file (DXF, SVG, vectors) that you import into LaserWeb and you tell the machine exactly what you want it to do (engrave this picture, cut around here at this power level, cut this at some other power level, etc...) This is called CAM - essentially the part that takes the file and defines what the machine should actually do with each feature.  That's how the industry operates and how you get some real powerful performance out of your machine. 



Oh, as far as Smoothie controllers go, I make this one, which is a direct upgrade bundle for the K40 Laser and quite inexpensive compared to the others: [cohesion3d.com - Cohesion3D Mini Laser Upgrade Bundle](http://cohesion3d.com/cohesion3d-mini-laser-upgrade-bundle/)



-Ray


---
**Steven Whitecoff** *February 24, 2017 04:32*

Ok Ray, I only asked about the Nano board software path to see if corels data being HP/GL to get proper cutting meant the controller used it or if CorelLaser was changing HP/Gl to propietary stuff. 

Sounds kinda like Corel will take you to the finish line only to dump you inches short. Anyway, I'm not resisting the Cohesion board because I've been told the Nano is a dead end(and actually the entire machine, which IS BS) as far as using its parts in a large format  machine. I intend to go to 1600x1000 workspace, the Smoothie will handle that I presume(along with the larger steppers?). Steppers is another topic, I presume the K40 sized ones wont handle the masses involved with a 1600x1000 setup. So then the question will the Smoothie handle more powerful steppers. 


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 04:41*

Ooh, larger build, cool. 

The A4988 drivers will run Nema 17's. And to run a flying head you should be just fine with those. I'm talking about something like a 48mm long Nema 17 motor rated for roughly 84 oz/ in. That's close to what openbuilds carries.

You can also get Trinamic StepStick drivers - almost silent motor performance! 



We also have an external stepper adapter, if you want to go larger than that you can get a black box external stepper driver to run big Nema 23's or higher, but again it depends on the mechanics of your build. 


---
**Steven Whitecoff** *February 24, 2017 13:28*

This is the recommended stepper setup, NEMA 23



[aliexpress.com - Aliexpress.com : Buy Leadshine Stepping Motor 573S15 and Motor Driver 3ND583 for Laser Engraving/Cutting Machine Stepper Motor from Reliable motor stepper driver suppliers on MASTER LASER](https://www.aliexpress.com/store/product/Leadshine-Stepping-Motor-and-Motor-Driver-for-Laser-Engraving-Cutting-Machine-Stepper-Motor/1017060_1917723308.html?spm=2114.12010612.0.0.c4tqo9)


---
**Steven Whitecoff** *February 24, 2017 13:43*

The next big question is can a closed loop system be used? I'm guessing yes as I'd think the input would say go X number of steps and the driver detects the stepper failing to go X steps and drives it to X steps till the feedback confirms proper postion


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 15:15*

Yep, same answer as before, use the external stepper adapters to break out the signals for the closed loop driver. Plenty of people do this. 


---
**Don Kleinschnitz Jr.** *February 24, 2017 16:08*

**+Ray Kholodovsky** I'm interested in how steppers can be closed loop. What is used as feedback sensor?


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 16:11*

**+Don Kleinschnitz** the product is a combined stepper motor with encoder and control box (like an external stepper driver) that handles all the thinking for it. Such a box typically takes the step dir signals as input and runs the motor system. 



I don't know what exactly the internal sensor is. It's an encoder, might be an optical one. We just feed the system the step and dir signals. 


---
**Steve Anken** *February 24, 2017 16:25*

Here's one way to close the loop on steppers but there are also servos once you go that direction. 

[https://www.kickstarter.com/projects/tropicallabs/mechaduino-powerful-open-source-industrial-servo-m](https://www.kickstarter.com/projects/tropicallabs/mechaduino-powerful-open-source-industrial-servo-m)


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 16:27*

And here's an improved version of that mechaduino. I have these. [misfittech.net - Misfit Tech – Island of Misfit Technology and Electronics](http://misfittech.net/)


---
**Don Kleinschnitz Jr.** *February 24, 2017 16:44*

**+Ray Kholodovsky** I am glad I asked I did not know about the AS5047D. Very cool part and nice control system. The sensor is an on axis magnet and an AS5047 on the back side of the board.


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 16:51*

Yes, I know how it works :) Here's the NZS:![images/96b890e5d31f3ff205be8edaa039430b.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/96b890e5d31f3ff205be8edaa039430b.jpeg)


---
**Don Kleinschnitz Jr.** *February 24, 2017 17:28*

**+Ray Kholodovsky** You posted; "I don't know what exactly the internal sensor is. It's an encoder, might be an optical one." I was just passing on the answer :)


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2017 17:32*

We were talking about 2 different things. 

The mechaduino/ NZS uses the magnetic encoder, yes. 

I was referring to the actual closed loop stepper systems, not these "workaround" upgrade units.  The real systems might use an optical encoder I think, but it might be mechanical, something else, etc. 


---
**Steve Anken** *February 24, 2017 20:42*

Closing the loop is one thing with various levels of accuracy. It's just a feedback loop and can be implemented many ways. The servos I have sitting in front of me have both a plastic optical disc and three hall sensors and the controller is a DSP board. On a small micron level machine we have they used Renishaw linear encoders for absolute position.



I have not tried the closed steppers and am just going with servos for our 5'x9' table.


---
**Steven Whitecoff** *February 25, 2017 14:09*

Since the cost of a closed stepper system is only about 10-15% more at a $125 price point it seemed like a no brainer as a "bolt on" mod. If you look at high end lasers they all use closed loop for accuracy

[ebay.com - Details about  LCDA357H+LC57H<wbr/>3100 NEMA23 3NM 3PHASE Closed-Loop Stepper Motor Driver Kit CNC](http://www.ebay.com/itm/LCDA357H-LC57H3100-NEMA23-3NM-3PHASE-Closed-Loop-Stepper-Motor-Driver-Kit-CNC-/191908107316?hash=item2cae9d5434:g:o3YAAOSwRQlXceNB)

 

next I have to figure out how resolution works in terms of phases, minimum angle, pulley size etc.


---
*Imported from [Google+](https://plus.google.com/112665583308541262569/posts/RcP3FCyNMqR) &mdash; content and formatting may not be reliable*
