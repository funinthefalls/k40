---
layout: post
title: "Well I got my laser in the other day and everything looks in good shape"
date: September 08, 2016 15:26
category: "Discussion"
author: "Randy Draves"
---
Well I got my laser in the other day and everything looks in good shape.  Was suprised to see it had lights and end stops built in.  Started doing some upgrades.  Bilge blower exhaust, grill bed and air assist.  But when I was cleaning the mirrors after alignment (pita) I noticed the fixed mirror looked messed up.  The Y mirror looked similar but not as bad.  Will this effect the performance?   Should I contact the ebay seller for replacements?  



So far I cut/burned a couple of things and had some frustrating times lining things up in corel laser.  Probably a loose nut on the keyboard.  What are some advantages to upgrading to aurduino/smoothie?



![images/7999a71ecca4058879e1d882bcfdec71.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/7999a71ecca4058879e1d882bcfdec71.jpeg)
![images/5494316df1fa091d4501d79ee45c7196.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5494316df1fa091d4501d79ee45c7196.jpeg)
![images/070c76bab91117c01042e6161015443c.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/070c76bab91117c01042e6161015443c.jpeg)
![images/5158d61c63e899f80b0f8102f8b69052.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5158d61c63e899f80b0f8102f8b69052.jpeg)
![images/411142f5b1e553093d71034e3c5e7209.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/411142f5b1e553093d71034e3c5e7209.jpeg)

**"Randy Draves"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 08, 2016 15:56*

I've only been using Smoothie for a few weeks now (as opposed to almost 12 months on Corel). So far I've noticed the improvements are firstly the workflow, secondly I can do gradients of power in the one engrave (so shades of grey instead of everything being 100% black), & every time I want to run a 2nd pass I don't have to stuff around adding another task to the queue or pressing "Okay" between each pass. So, depending on your plans for the usage of your machine, you could stick with Corel if just wanting to cut. If you intend to engrave, having Smoothie would be a good option as it allows for much better results (check the LaserWeb/CNC Web community or examples of **+Alex Krause**'s recent engrave tests with the new version of Smoothie firmware).


---
**Randy Draves** *September 08, 2016 17:07*

Right now it's just the basics but eventually I'll would like to do some photos/gradients.  So it sound like I'll have to start researching the smoothie upgrade.    



What do you think about the mirror?     Is it worth asking for a replacement?


---
**Anthony Bolgar** *September 08, 2016 17:10*

I would ask for a replacement, worst thing that can happen is they say no. No harm in asking.


---
**Carl Fisher** *September 08, 2016 18:06*

I upgraded to a smoothie compatible board from **+Ray Kholodovsky** and I can't believe I even bothered with the stock nano board. I'm still dialing in settings and experimenting but being able to accurately control the laser through PWM and feed it real gCode is a huge improvement. I can now to gray scale engravings and I have a lot more control over the machine in general. This also gives you a Z and A axis driver for upgrades to a motorized bed and rotary. Add a compatible glcd board (RepRapDiscount Controller for example) and you can control the machine from a control panel.



As for your mirrors and lenses, I would ask for replacements but in the end you'll probably want to order upgrades anyway from someone like Laserbits.




---
**Robi Akerley-McKee** *September 08, 2016 19:19*

using the RAMPS 1.4 Turnkey laser extension for inkscape can be tweaked to allow repeat passes added into the layer names.  I hacked turnkey laser to allow 15 [feed=300,ppm=4,repeat=3] for a 15 power 300mmpm 4 pulses per mm, and cut 3 times into the gcode file.  then used pronterface to upload it to the arduino/ramps combo.


---
**Randy Draves** *September 08, 2016 21:00*

Since this post I've been checking out the smoothieboard and mks sbase boards.  After looking through the posts here it's definitely  the next upgrade I'll be doing.  I'll have to check out Ray.



I saw that you can get a touch screen for the mks sbase board.  I wonder if that will work with the laser.  



I figured I'll have to upgrade the mirrors and lenses at some point.  I'll give Laserbits a look.





I also saw a video on youtube about a arduino safety system.  The parts were only about $20 so I ordered them out.  Now I just gotta try to figure out how to put it together.  : )






---
**Anthony Bolgar** *September 09, 2016 00:03*

Safety system is on github at [https://github.com/funinthefalls/LaserSafetySystem](https://github.com/funinthefalls/LaserSafetySystem)


---
**Randy Draves** *September 09, 2016 14:29*

Thanks for the link Anthony.  



The ebay seller wants me to buy the mirrors locally and they will reimburse me.  Any suggestions on where to buy?


---
**Anthony Bolgar** *September 09, 2016 15:40*

LightObjects.com is a trusted reputable U.S. supplier. They sell high quality products and stand behind their products.


---
**Randy Draves** *September 09, 2016 17:25*

Mirror size is 20mm correct?  I'm not at home to measure.


---
**Anthony Bolgar** *September 09, 2016 17:32*

Yup 20mm, get the MO mirrors, they are the best.


---
**Randy Draves** *September 09, 2016 17:34*

Great thanks 


---
**Randy Draves** *September 09, 2016 18:03*

Since the ebay seller is picking up the cost I decided to be fair and order them from saite_cutter on ebay.  They were only 24.29.  Granted they are going to take a few weeks to get here.  But lightobjects wanted 27 for each mirror.  I know they are probably better quality but since I'm not paying for them I can't expect the ebay seller to.  


---
**Yuusuf Sallahuddin (Y.S. Creations)** *September 09, 2016 20:06*

**+Randy Draves** You probably could have got the eBay seller to pay the 27 each, a lot of people have received near full refunds for their machines due to issues on arrival.


---
**Randy Draves** *September 09, 2016 21:14*

Yuusuf your probably right but besides the mirrors nothing else has been bad (yet)  So far it's been a good experience with this seller.  And after all the horror stories I've read with others I'm happy so far with the purchase.  I'm not looking for the seller to upgrade my machine for free,  just to make it right.  Which the seller is trying to do.  Also I read here that saite cutter sells some decent mirrors.  Don't know how they compare to lightobjects mirrors. 


---
*Imported from [Google+](https://plus.google.com/110537209659823059360/posts/fWYQXpJq66B) &mdash; content and formatting may not be reliable*
