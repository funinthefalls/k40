---
layout: post
title: "Dealing with measurement units in the U.S"
date: February 24, 2017 23:46
category: "Discussion"
author: "Ned Hill"
---
Dealing with measurement units in the U.S. is a pain in the ass.  We buy our milk by the gallon but our soft drink/pop by the liter (smh). My normal job is in science were I exclusively deal with the metric system but when I go to the store to buy plywood, it's by the foot.  Everybody with these lasers pretty much sticks with metric, as far as I can tell, because it's easier (duh) and I've been getting tired of switching between the two, like when I need to set up my table saw to get a specific size piece of wood cut for the laser.  So I finally decided it was time to get a combo measuring tape.  Got a 3.5m (12ft -Arrgh!) tape from amazon and it works like a dream.  Metric system RULES/ERS! :)  [https://www.amazon.com/gp/product/B000CFJB08/](https://www.amazon.com/gp/product/B000CFJB08/)

![images/7dbd164dc60c86f44f2522f4a8583d43.png](https://gitlab.com/funinthefalls/k40/raw/master/images/7dbd164dc60c86f44f2522f4a8583d43.png)



**"Ned Hill"**

---
---
**Jim Root** *February 25, 2017 01:02*

I can feel your pain. But I'm in the construction industry in the US and everything is in feet and inches. I buy my milk by the gallon but pop by the ounce. Unfortunately there is a bunch of mixed measurements here. What really sucks is when we get on a Federal job with "Soft Metric" requirements. What that means is that we are expected to "Buy US" materials, which are in SAE measure, and apply them to specs that are all metric. SMH




---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2017 03:15*

Wow, the worst I have to deal with (in general) is people talking in millimetres, centimetres, or metres. Sometimes you get the odd person talking in inches, feet, yards, etc, but that is quite rare.


---
**Alex Krause** *February 25, 2017 03:29*

My biggest Irk is when I grab a print from the archive at work and some silly engineer has put fractions of inches and decimals on the same part but different feature... Like a groove width is represented as decimal and a chamfer is listed in fractional.... But we measure everything with calipers :(


---
**Andy Shilling** *February 25, 2017 12:10*

It's the whole world over, I work as a foam convert in the uk and we have people coming in asking for pieces of foam that are 120mm x 1mtr x 5" thick. Milk used to be in pints and now it's in litres; and when ever I look for a recipe online a lot of them ask for cup measurements.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2017 13:06*

**+Andy Shilling** Haha, that's just ridiculous. Totally made me laugh thinking 120mm x 1m x 5" hahaha.



The whole cup measurements is even worse considering there is US cups & everyone else cups. Same with teaspoons etc, seems there is multiple variations of how much fits in a teaspoon.


---
**Coherent** *February 25, 2017 14:51*

I guess after years of playing with CAD, CAM and CNC machines, I don't even think about it any more and often measure and draw using both at the same time. That may sound weird, but many of parts and hardware I use are imported from China, so they are obviously metric, but when I build or design, I use inches and or inch fractions .  



"Yuusuf Sallahuddin  Haha, that's just ridiculous. Totally made me laugh thinking 120mm x 1m x 5" hahaha" 



I do exactly that lol! I use Inventor quite a bit and whats nice about it is that I can enter any value in its type of measurement standard... for example as inch like "1.00 in" or metric as "25.4" mm and the software knows what I mean. You can also measure or annotate and it will show one or the other or both! Kinda like Ned's tape measure within the cad software on steroids. 






---
**Anthony Bolgar** *February 25, 2017 15:06*

In Canada, we have been metric for about 40 years now, and we still can't shake the imperial system in construction and industry, because of our close economic and geographical ties to the US. Drives me crazy!!!!!!


---
**Don Kleinschnitz Jr.** *February 25, 2017 15:09*

My frustration is..... fractions, the most useless, most complex, most inaccurate system of measures ever conceived...IMO.



The worst is when you have a decimal dimension-ed drawing and all you have in your hand is a fractional ruler. 



The Egyptians were master builders why in hell did they invent fractions.

............

I converted my shop to decimal and metric OMG no more dividing 1/8 by 3/32?



The problem I have is that I was a maker from a young age and I learned to build with fractions. Fractional sizes are indelibly marring my brains sense of size. Its hard for me to get a "feel" for metric sizes like.... 1/8 is about 3mm etc. 

I need to just stop thinking in fractions. As soon as I do I have to go to home depot and buy a 2x4. Try asking for a 1.5x3.5 or better yet ask for a 38.1x88.9 ... doesn't even come off the tongue right.

............

There are certain indelible technologies that we have burdened ourselves with even though we know they are inefficient, for social reasons we will be stuck with them forever. The English system of measures and the Querty keyboard come to mind. 

[http://computer.howstuffworks.com/question458.htm](http://computer.howstuffworks.com/question458.htm)

........

I have two helpers: a few sets of fractional/decimal/metric calipers and I found this ruler. 



Definitely going for that metric-fraction version, cause its a tool I do not have and that is just not right ....



![images/967565a498138f805d410f85215d3c79.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/967565a498138f805d410f85215d3c79.jpeg)


---
**Anthony Bolgar** *February 25, 2017 15:12*

Qwerty was designed to SLOW down typists on mechanical typewriters so the keys would not jam.


---
**Don Kleinschnitz Jr.** *February 25, 2017 15:21*

**+Anthony Bolgar** that is my point, now we all have this purposely inefficient means of input that we cannot get rid of  <:) in front of screaming fast computers.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 25, 2017 16:33*

**+Don Kleinschnitz** I was fortunate enough that growing up in Australia, my entire life has been spent using decimal & metric systems for everything. So for me, it all comes 1st nature. Only when speaking with older generations (such as my parents or grandparents) did I ever really come across imperial measurements, weights, currencies & the likes. For me, I have the opposite of what you say regarding the 1/8". I always have to convert it into its nearest decimal equivalent so I can visualise how big that really is. Another fortunate thing for me is here in Australia majority of our material supplies come in metric sizing. I can go to my local Bunnings hardware store & get a piece of 81 x 19mm. I guess whichever system you utilise doesn't really matter as per se, it's only when you starting using multiple systems that things become a bit convoluted.


---
**Jonathan Davis (Leo Lion)** *February 25, 2017 23:37*

For me usually when measuring things I find inches to be a easier system to understand. 


---
**Bill Keeter** *February 26, 2017 04:20*

I switched to Metric the minute I decided to build my first printer. Why? Cus RepRap is open source and everyone was using metric. Learning to building a printer and converting everything to inches in the process just sounded like another way to fail. 



Best. Decision. Ever. I'm a complete convert. 3mm is just easier to deal with than 1/8".


---
**Alex Krause** *February 26, 2017 06:21*

**+Ned Hill**​​ I still have some stick on metric tape measures... I sent some to **+Anthony Bolgar**​​ for his lasers... I use them in mine to mark my bed boundaries for quick and easy alignment here is an example photo... If you want one let me know I will send it for postage only 



![images/d84cc3e4ef04fb989ff2ae8ebfc303cb.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/d84cc3e4ef04fb989ff2ae8ebfc303cb.jpeg)


---
**Alex Krause** *February 26, 2017 06:23*

![images/a549c9dd64a892baa8a95694fe89bd33.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/a549c9dd64a892baa8a95694fe89bd33.jpeg)


---
**Ned Hill** *February 26, 2017 14:30*

Thanks **+Alex Krause**, I sent you a PM.


---
**Don Kleinschnitz Jr.** *February 26, 2017 16:29*

**+Alex Krause** That's a great idea gonna do that. 

Btw I was wondering how do you align an engraving accurately on the bed especially if you are going to make multiples.


---
**Bill Keeter** *February 26, 2017 18:49*

**+Don Kleinschnitz** here's the trick I learned at TechShop. Cut out a piece on the laser from from a sheet of wood or acrylic. Attaching sheet to the bed (tape whatever). After the part is cut, remove it. Now you have a cut out you know the location of. Drop in the part you want to engrave. And in LW use the same alignment you used to cut to now engrave. 



Engrave and repeat. :)


---
*Imported from [Google+](https://plus.google.com/+NedHill1/posts/Wvo4XHp62r3) &mdash; content and formatting may not be reliable*
