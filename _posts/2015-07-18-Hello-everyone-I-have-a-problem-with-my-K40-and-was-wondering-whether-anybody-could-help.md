---
layout: post
title: "Hello everyone, I have a problem with my K40 and was wondering whether anybody could help"
date: July 18, 2015 14:15
category: "Discussion"
author: "george ellison"
---
Hello everyone, I have a problem with my K40 and was wondering whether anybody could help. When i got the machine i aligned the mirrors, now about 6 months later from vibrations etc, the mirrors need re-aligning. I carried out the same procedure as before but this time i cannot get a consistent focus throughout. I moved the gantry to the bottom and the cutting head to the right and aligned the mirror so the spot was hitting the middle of the cutting head. I then kept the cutting head to the right but moved the gantry to the top. Now, the beam is to the left and is not entering the hole. Any help would be much appreciated. Thanks. 





**"george ellison"**

---
---
**Steve Moraski** *July 18, 2015 16:38*

George, I ran into similar issues when trying to align the mirrors on my k40.  What I did was to start with getting the tube lined up in the center of the first mirror.  After that starting with the second mirror at top left center the dot on mirror 2.  After that instead of moving the second mirror all the way to bottom left I moved only about an inch or so down and got the dot centered. I then repeat this step every inch or so until I get to the end,  making sure to go back to the start every so often to verify I was still aligned.   After getting the second mirror aligned go back to the start and follow a similar adjustment pattern for the third mirror. Let me know if that helps or you have further questions. 


---
**David Richards (djrm)** *July 18, 2015 17:52*

Thats right Steve, the calibration needs to be right at the outset. You can't  properly compensate for the first mirror by adjusting any subsequnt ones. George, you mention focus in your post, better check the hidden lens and mirror are not dirty too. I was suprised how much dirt the lens had accumulated before I added air assist. Kind regards david.


---
**george ellison** *July 19, 2015 18:28*

This is fantastic help, will give it a try when I can. Have cleaned all mirrors and lens with acetone, very dirty indeed! Thanks again, will let you know how I get on.


---
**george ellison** *July 20, 2015 14:38*

Hello guys, had a go today  and found that i needed a spacer on the fixed mirror. I added this and am now trying to align. I have found a handy manual online to help me but its so frustrating i cant get it right. 


---
**David Richards (djrm)** *July 20, 2015 17:16*

Hi George, I found an online manual when I needed to calibrate my light path. Although the general procedure given was correct the details about which screws to move to correct particular errors were  just plain wrong. You will probably find you need to re-do over and over the get the desired results - I did but I got an acceptable setup eventually. It was a trial by fire for myself, the machine had arrived quite badly damaged and I opted to take a huge discount instead or returning it to the supplier. The problem was that the case was badly bent and the jolt had also bent one of the mirror mounts. The I did the repair calibration after checking out the vital pieces were ok. David.


---
**george ellison** *July 21, 2015 07:33*

Thanks for those comments. I have now spent about five hours last night altering mirrors etc. and the dot seems to be landing in the same place in all four corners, will do a test cut today, thanks again


---
*Imported from [Google+](https://plus.google.com/104170451866294872772/posts/TPLCDaYdnUH) &mdash; content and formatting may not be reliable*
