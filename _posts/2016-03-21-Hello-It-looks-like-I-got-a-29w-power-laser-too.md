---
layout: post
title: "Hello, It looks like I got a 29w power laser too ( )"
date: March 21, 2016 11:36
category: "Hardware and Laser settings"
author: "Jean-Baptiste Passant"
---
Hello,



It looks like I got a 29w power laser too ([https://plus.google.com/109462644991158070594/posts/hAfMBR5eofd](https://plus.google.com/109462644991158070594/posts/hAfMBR5eofd)).



I cannot take off the upper label, as it tear appart both labels.



Do any of you have any idea on how I could peel the upper label off without damaging the label under so I canget a clear photo and open a dispute ?



Thank you





**"Jean-Baptiste Passant"**

---
---
**Stephane Buisson** *March 21, 2016 12:10*

You should compare your tube with Chris photos, if same brand (KH) and same seller, then opt for a free new 40W certified tube. referring to the seller offer in that post. 



Do you try steam ? (or hot water to melt the glue)


---
**Stephane Buisson** *March 21, 2016 12:16*

once could be a mistake.

But If KH does that deliberately, it's not a mistake but theft.

40W being stated in the product description.


---
**Jim Hatch** *March 21, 2016 12:56*

I believe the 40W rating comes from driving the tube at maximum possible power. If you check the length of the various CO2 laser tubes - certified 40W tubes are longer than our machines (same for 50s which is why those machines have an extension grafted on them to accommodate the tube). It's not exactly theft - you can run the power all the way up and get 40W output but it will drastically shorten the life of the tube. It's not all that different from over clocking PC CPU chips.


---
**Stephane Buisson** *March 21, 2016 13:21*

**+Jim Hatch**  I do agree on the fact you shouldn't use it on full power, but a 29w peak tube relabelled as a 40W (peak) is theft. at 75% you never have more than 21.75 W useable.

(about 2mm woodcut at best)


---
**Jim Hatch** *March 21, 2016 14:20*

**+Stephane Buisson** Physics is physics though. A true 40W CO2 laser tube is 850mm and the K40s ship with 700mm tubes. A 700mm tube is 32W max so the 29W of the OP's tube is probably spot on. The width of the K40s I've seen on Ebay don't allow for an 850mm tube so they're limited to the 30W tubes. I agree it's not optimal - I'd prefer a "real" 40W, but I'd also like 2x4 lumber not to be 3 1/2 x 2 1/2" and 3/4" plywood not to be 18mm (or .70 inches). It is what it is - it's not uncommon to have "nominal" sizing of things. We like to think we're dealing with engineering precision here but that's likely too much to expect from these Chinese laser suppliers. Assuming they'd even agree that they're not selling a 40W laser, they'd likely just change it to a 30W laser to charge the same $. If we threw a hissy fit and refused to buy the 30W ones they'd likely raise the price to accommodate the larger tube and the larger sized box to fit it all in. (Or the cynic in me says they'd manufacture a 40W certification sticker and apply that to the laser instead - after all, how many of us have the technology to measure the power output of our lasers.)



If we want truth in power ratings we need to buy from mainstream and accredited suppliers like Epilog and pay the resulting difference. If you really need 40W of power, you need to step up to a 50W Chinese laser which will take you into the $1000 range. I do okay with the K40 for wood engraving and acrylic engraving & cutting. When I need to do serious wood cutting (over 1/4" which I can handle with multiple slow speed passes with the K40) I use my MakerSpace's 60W laser but most stuff I can handle with the K40.



Heck - I'd be happy if clothes were real sizes. It'd make my life so much easier but I can't keep track of my wife's sizes in clothes because the size numbers are getting smaller for the same size clothes. (Men's clothing used to be "real" inches like 34" waists being 34" around but even that's changing where I've seen pants with 34" inch waists more like 35 1/2" which seems an awful lot like an old 36" waist.) :-)


---
**Stephane Buisson** *March 21, 2016 14:39*

K40 accept up to  72 cm



branded tube PURI 40W 72cm

[http://www.ebay.com/itm/Puri-40W-CO2-Laser-Tube-For-Laser-Engraving-Machine/131730557959?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D35624%26meid%3De896dacff71840b18209cc7b0c2f5e0c%26pid%3D100005%26rk%3D5%26rkt%3D6%26sd%3D161422135003](http://www.ebay.com/itm/Puri-40W-CO2-Laser-Tube-For-Laser-Engraving-Machine/131730557959?_trksid=p2047675.c100005.m1851&_trkparms=aid%3D222007%26algo%3DSIC.MBE%26ao%3D1%26asc%3D35624%26meid%3De896dacff71840b18209cc7b0c2f5e0c%26pid%3D100005%26rk%3D5%26rkt%3D6%26sd%3D161422135003)



if 70 cm most likely 35w (plenty on ebay)

[http://www.ebay.com/itm/HOT-Selling-35W-CO2-Laser-Tube-Cooling-for-CNC-Laser-Engraving-Machine/301321475127?_trksid=p2047675.c100009.m1982&_trkparms=aid%3D777000%26algo%3DABA.MBE%26ao%3D1%26asc%3D35624%26meid%3Dd32d8e3fa2f84d029b1b79a156b20c59%26pid%3D100009%26rk%3D1%26rkt%3D1%26sd%3D151196052806](http://www.ebay.com/itm/HOT-Selling-35W-CO2-Laser-Tube-Cooling-for-CNC-Laser-Engraving-Machine/301321475127?_trksid=p2047675.c100009.m1982&_trkparms=aid%3D777000%26algo%3DABA.MBE%26ao%3D1%26asc%3D35624%26meid%3Dd32d8e3fa2f84d029b1b79a156b20c59%26pid%3D100009%26rk%3D1%26rkt%3D1%26sd%3D151196052806)


---
**Jean-Baptiste Passant** *March 21, 2016 19:10*

Brand is KH, laser is 29w... I will negotiate with the seller.


---
**Heath Young** *March 24, 2016 03:36*

Mine is the same - 29W tube. Hardly surprising - its probably 40 Chinese watts. (Like PMPO). I actually think that's the real rating of these tubes.


---
**Jean-Baptiste Passant** *March 24, 2016 07:59*

Seller asked me how much I wanted as a refund, I will ask the price of a certified laser, I paid for a 40w not a 29w...


---
**Stephane Buisson** *March 24, 2016 12:02*

**+Jean-Baptiste Passant** tu porurrais entrer dans un dialogue de sourd, fais attention, le mieux est de ne pas donner de prix, mais une reference (un lien pointant vers un tube 40W), libre a ton vendeur de trouver une meilleur solution pour un produit similaire.

(pense qu'il y a de forte chance qu'il a une relation particuliere avec les sources en chine, mais cela pourrait prendre du temps).

l'autre moyen de sortir du probleme est d'accepter du cash, mais se sera sans doute moins que ton prix d'achat pour un 40W. gardes-tu ton 29W? (on fait quand meme pas mal de chose avec 29W).

PS: autre solution j'ai vu des tube 50W moins cher que des 40W ebay Allemagne, cela necessiterai de changer l'alim, mais a une solution seduisante a regarder. attention plus long il te faudra modifier le boitier (light objet vend un truc pour cela, tu peut aussi t'en inspirer)



[http://www.ebay.de/itm/Water-Cooling-50W-Sealed-Laser-Tube-for-CO2-Laser-Engraver-machine-3000-4500-H-/111801328950?hash=item1a07e0e536:g:qJwAAOSwAYtWJJkx](http://www.ebay.de/itm/Water-Cooling-50W-Sealed-Laser-Tube-for-CO2-Laser-Engraver-machine-3000-4500-H-/111801328950?hash=item1a07e0e536:g:qJwAAOSwAYtWJJkx)


---
**Jean-Baptiste Passant** *March 24, 2016 14:48*

**+Stephane Buisson** 

Oui, je lui ait envoyé le lien d'un 40w PURI, pour lui expliquer que ses 30€ je ne les accepterais pas. J'ai donc demandé un tube certifié 40w ou l’équivalent en cash pour acheter du vrai 40w.



Il est vrai que passer à 50w peut-être intéressant, je vais y songer très fortement



J'attends sa réponse, et je mettrais le déroulement ici ;)



Je ne sais pas si je garderais le 29w, cela dépendras du déroulement des négociations.


---
**Jean-Baptiste Passant** *March 30, 2016 11:38*

Seller accepted to refund me enough for me to buy a 50w laser. Now I have to get a power supply and extension for it :)


---
*Imported from [Google+](https://plus.google.com/+JeanBaptistePassant/posts/Bx2Zkvb6R37) &mdash; content and formatting may not be reliable*
