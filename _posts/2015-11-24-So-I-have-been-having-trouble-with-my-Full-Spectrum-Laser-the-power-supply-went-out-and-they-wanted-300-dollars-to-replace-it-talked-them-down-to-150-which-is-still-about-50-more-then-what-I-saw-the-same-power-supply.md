---
layout: post
title: "So I have been having trouble with my Full Spectrum Laser the power supply went out and they wanted 300 dollars to replace it :-( talked them down to 150 which is still about 50 more then what I saw the same power supply"
date: November 24, 2015 21:53
category: "Hardware and Laser settings"
author: "Nick Williams"
---
So I have been having trouble with my Full Spectrum Laser the power supply went out and they wanted 300 dollars to replace it :-( talked them down to 150 which is still about 50 more then what I saw the same power supply going for from china. OK got it. My laser was now firing and low and behold the controller would stop in the middle of a job, REALLY:-(



But wait I have been working with grbl and laser diodes for the better part of two years so why not give the K40 a try.

For 366 off EBay coming out of California and free shipping how could I go wrong. Oh ya Moshi:-( well I gave the Moshi software about a day and it was as bad as every one says and my background is in software:-( 



Now time for the upgrade I spent exactly 21/2 hours on this build. Have not wired in the homing switches but every thing is working great,



So I bought a Raspberry Pi CNC shield for $30

[http://blog.protoneer.co.nz/raspberry-pi-cnc-board-hat/raspberry-pi-cnc-board-gpio/](http://blog.protoneer.co.nz/raspberry-pi-cnc-board-hat/raspberry-pi-cnc-board-gpio/)



This is designed to work with the Rasp Pi but the Rasp is not needed you can just use the shield and plug in the usb. I have purchased a Rasp Pi 2 as I want to get functionality working on the pi such as part libs and walk away cutting but currently just using my laptop and a USB connection.



The hardest thing was building cables to connect the stepper motors a chose to connect directly to the K40  steppers and not use the ribbon cable that is part of the Moshi board



Her is the config for the grbl 



Grbl (LaserInk 1.1)0.9g ['$' for help]



Grbl 0.9g ['$' for help]

$0=25 (step pulse, usec)

$1=25 (step idle delay, msec)

$2=7 (step port invert mask)

$3=0 (dir port invert mask)

$4=0 (step enable invert, bool)

$5=0 (limit pins invert, bool)

$6=0 (probe pin invert, bool)

$10=3 (status report mask)

$11=0.010 (junction deviation, mm)

$12=0.002 (arc tolerance, mm)

$13=0 (report inches, bool)

$14=1 (auto start, bool)

$20=0 (soft limits, bool)

$21=0 (hard limits, bool)

$22=0 (homing cycle, bool)

$23=0 (homing dir invert mask)

$24=25.000 (homing feed, mm/min)

$25=500.000 (homing seek, mm/min)

$26=250 (homing debounce, msec)

$27=1.000 (homing pull-off, mm)

$100=78.000 (x, step/mm)

$101=78.000 (y, step/mm)

$102=250.000 (z, step/mm)

$110=5000.000 (x max rate, mm/min)

$111=5000.000 (y max rate, mm/min)

$112=500.000 (z max rate, mm/min)

$120=225.000 (x accel, mm/sec^2)

$121=225.000 (y accel, mm/sec^2)

$122=10.000 (z accel, mm/sec^2)

$130=200.000 (x max travel, mm)

$131=200.000 (y max travel, mm)

$132=200.000 (z max travel, mm)

ok



Note that this is the Laserink variation of grbl based off 9g

It allow for PWM image rastering.

[https://github.com/nickw89509/LaserInk](https://github.com/nickw89509/LaserInk)

 Full disclaimer I am the the writer of this firmware.



I couldn't be happier and I am doing image engraving.



![images/1ba8258e07d887cab5233acdd440bcb2.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1ba8258e07d887cab5233acdd440bcb2.jpeg)
![images/1fd2f6674c38b7f552c4453d6a7acac5.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1fd2f6674c38b7f552c4453d6a7acac5.jpeg)
![images/da13caf241eda0235850841ce09b797a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/da13caf241eda0235850841ce09b797a.jpeg)
![images/1bfc6fb5ca072646373201c94417e498.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1bfc6fb5ca072646373201c94417e498.jpeg)

**"Nick Williams"**

---
---
**Nick Williams** *November 25, 2015 09:01*

**+Peter van der Walt** 

I looked at the laser grbl. It looks like it is a port to a different cpu which means that it would require new electronics. How does the laser grbl handle image engraving?  


---
**DIY3DTECH.com** *November 26, 2015 12:31*

Very cool, this is what I have been looking for!  Have worked a lot with GRBL and appreciate the fact you also included the $$ ! 


---
**DIY3DTECH.com** *November 26, 2015 12:37*

P.S. Would also be interested to see how you handle the end-stops since there are only two assume the G28 would have to set to only seek two rather than four.  Do keep us posed as over Xmass my laser too will have new a brain :-)




---
**Nick Williams** *November 27, 2015 06:17*

I have not yet wired in the end-stops. I use the laser like a CNC if you read the grbl file you will noticed I disabled Homing. Additionally have set the de-energize delay so the steppers disable after a job. This allows me to manually position the laser. Every job I send assumes the current position is (0,0) and sends G92X0Y0.  This simplifies positioning issues. It also made the controller conversion supper simple cause I just needed to wire in the steppers. I actually ran  a cable from the grbl controller X axes directly to the X Stepper motor and disconnected from the board that is connected to the ribbon cable.  


---
**Nick Williams** *November 27, 2015 06:20*

When the time comes I will enable homing, This is a simple matter of recompiling the grbl Code base. There is a constant in the config.h file that allow you to do homing only on the XandY axis ignoring the Z axes.


---
**DIY3DTECH.com** *November 28, 2015 01:44*

Nick have myself talked into doing the brain transplant  as I have an extra Arduino UNO and CNC Ramp board ([http://blog.protoneer.co.nz/arduino-cnc-shield/](http://blog.protoneer.co.nz/arduino-cnc-shield/)) laying around as I keep spares as I have several machines I built (using GRBL) using this config.  As I also have the M2 Nano board the steppers are already 4 wire (non-ribbon cable) so I can just crimp on duponts and be ready to go.  However with your fork of GRBL, how do I tie in the laser trigger?


---
**Nick Williams** *November 28, 2015 07:41*

I have made a small change to my code base to support both power intensity and on off. The original codebase was part of my Kickstarter which were diode lasers and did not use the on/off

Her is a link to my dropbox with the hex code you will need

[https://www.dropbox.com/s/zgi2gpjxuho3tz2/grbl.hex?dl=0](https://www.dropbox.com/s/zgi2gpjxuho3tz2/grbl.hex?dl=0)

Pin 11 will control laser intensity and pin 13 will toggle the laser on and off with m3/m5



If you use the laser intensity you can wire pin 11 directly to the power supply were the pot signal is currently coming in. I am using the same software that I use for my laserink Kickstarter but the there are plugins for inkscape that should work for doing vector drawing.



Wiring pin 11 should only be needed if you plan to do grey scale image rastering like what I have shown in the images above.


---
**Nick Williams** *November 28, 2015 07:46*

Oh,  yah if you do wire in the intensity then the laser is controlled with the spindle command. The range is 0-255 and don't forget to wire the ground of the power supply to the arduino but I am probably stating the obvious here.

Good luck let me know how it gos:-)


---
**Nick Williams** *November 28, 2015 07:54*

I am also working on another modification to grbl to support high detailed image rastering. For the image that was shown I had to throttle the max power of the laser and slow down the machine do to the fact that our software generates a GCode block for every pixel in the original image. This works fine for a 2-3 watt diode laser running at 1200 mm/min but for a 40 watt laser you have the opportunity to go 10 times as fast at these rates the grbl can not process the blocks fast enough. So I am adding a custom streaming mode which should get me to about 12000 mm/ min.  


---
**DIY3DTECH.com** *November 28, 2015 12:42*

Interesting, I downloaded the hex, however I have "digital" version not the pot.  Do you know the lead from the power supply goes to pin 13? Or are you saying one has to pick either 11 or 13?  Many thank again!


---
**Nick Williams** *November 28, 2015 18:37*

If you could send me a picture of your power supply I could probably figure it out. I have seen a few of these.



My laser is at my office or I would send  you a picture if we don't figure it out I will get you images Monday.



On my power supply:

To the right of the power supply there are is a 4 pin connector.

24+

5+

Grnd

Laser Enable

I am not sure of the order as I am doing this from memory but on the power supply circuit board there is some tiny lettering.

(5,24,G,?) can't remember the symbol for the laser enable but I am sure about the other 3 so the one left will be the enable. On my laser this active high i.e. 5 volts will turn the laser on 0 will turn the laser off. typically these power supplies have an active low usually this is used for the test fire button on the panel either has the ability to energize the laser.   



One important safety issue is that on the grbl pin 13 will default to high when grbl is booted. I will be changing this but on the version I gave you this is true. This means that when grbl is booted if the laser is powered up it will turn on immediately. On my current laser this is not a problem because I have the pwn on pin 11 wired to the power control so this will default to zero effectively leaving the laser off.



From what I have gathered you are going to get your laser to just work in on off mode so this will be an issue. This state will be cleared by adding an M5 command to the grbl startup script. This will cause the laser to turn off immediately it may still come on for a brief moment.  



Nick


---
**Nick Williams** *October 19, 2016 18:56*

**+DIY3DTECH.com** Its been a few weeks figured I would check in and see if you made any progress or need any help??


---
*Imported from [Google+](https://plus.google.com/107520953345127905250/posts/HdpNeABNVoz) &mdash; content and formatting may not be reliable*
