---
layout: post
title: "Hi all I was wondering if anyone had a link or some info on where to get oem parts for the K40?"
date: July 12, 2016 03:06
category: "Discussion"
author: "Pat B"
---
Hi all I was wondering if anyone had a link or some info on where to get oem parts for the K40? I have a bad stepper motor cable and im having troubles finding one.Also I would like to have some spare parts on hand for when this kind of thing happens.

TIA

![images/f5e1d9325983f94c1dcdef4e4d3e9342.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f5e1d9325983f94c1dcdef4e4d3e9342.jpeg)



**"Pat B"**

---
---
**Jim Hatch** *July 12, 2016 03:20*

LightObject has parts specifically for the K40. Most of what a K40 uses isn't specific - lots of general purpose electronics so Mouser or Digikey are good sources too.


---
**Alex Krause** *July 12, 2016 03:49*

If you have the m2nano board you can get some new connectors and plug into the other Axis and endstop connector ports after running a set of plain cables... is your end stops optical or physical limit switch?


---
**Don Kleinschnitz Jr.** *July 12, 2016 04:07*

Good question: the closest I could find was here but this is way to short:

[http://www.mouser.com/ProductDetail/Molex/15167-0288/?qs=sGAEpiMZZMvyL%252b65tEJ%2fL8uh7MrRs2hyeCgdlM6OibY%3d](http://www.mouser.com/ProductDetail/Molex/15167-0288/?qs=sGAEpiMZZMvyL%252b65tEJ%2fL8uh7MrRs2hyeCgdlM6OibY%3d)



The cables may be made by AXON. 



Can you repair the unit shown in the picture?


---
**Pat B** *July 12, 2016 12:03*

Thanks for the input everyone! I was able to flip the strip and resume work but it's still glitching on bigger files and certain parts of the table. But it has been doing it sense day one so I learned how to work around it. Lol


---
**Pat B** *July 12, 2016 16:54*

**+Jim Hatch**  I called digikey they were very nice to help. Seems like they have that connector in stock ! just ordered and I'll let you know the outcome. Thanks again! 


---
**Don Kleinschnitz Jr.** *July 13, 2016 13:22*

**+Pat B** the connector or the cable? I thought your cable was bad? Re-looked at the picture your are looking for the smaller flex cable?


---
**Pat B** *July 13, 2016 18:50*

**+Don Kleinschnitz** it was the small cable from the small board behind the stepper motor to the small board under the belt bar. I was able to flip it and keep working till the new one comes. 


---
**Don Kleinschnitz Jr.** *July 14, 2016 10:47*

**+Pat B** do you have the part no of that cable? I would like to post it on [http://donsthings.blogspot.com/](http://donsthings.blogspot.com/)

Also, did you ask them if they have the  longer one?


---
**Pat B** *July 15, 2016 02:23*

**+Don Kleinschnitz**  the cable # 

WFT E244685 AWM 20798 80C 60V VW-1. 

 I just received the new cables and they are too small I have to grab a caliper and be more precise on the sizes and try again 


---
**Pat B** *July 15, 2016 02:28*

I didn't ask them if they had a longer one (I could use a shorter one if anything) but they seem to have a bunch of sizes . 


---
*Imported from [Google+](https://plus.google.com/104544141227137983020/posts/34SzcrxnvJw) &mdash; content and formatting may not be reliable*
