---
layout: post
title: "Where can I find safety glasses for the 5030 size CO2 50w laser?"
date: March 22, 2018 12:37
category: "Discussion"
author: "Rob Mitchell"
---
Where can I find safety glasses for the 5030 size CO2 50w laser?



I’m in CANADA.





**"Rob Mitchell"**

---
---
**Joe Alexander** *March 22, 2018 13:24*

the size of the machine is irrelevant, the beam wavelength is what matters. Look for glasses rated for 10.6 um, and I wouldn't recommend buying the cheapest pair you can find. You only get 2 eyes..


---
**Anthony Bolgar** *March 22, 2018 14:02*

ANy pair of acrylic safety glasses will block the wavelength of a C02 laser. An unfocused beam at 18" away takes approx 6 seconds to burn through the acrylic. If you are staring into the beam for longer than that then Darwin will take care of things for you. ;)




---
**BEN 3D** *March 22, 2018 19:53*

I bought my from here. [https://www.uvex-laservision.de/en/laser-safety-eyewear/](https://www.uvex-laservision.de/en/laser-safety-eyewear/)



I bought R02.P1D01.1001 for 200 Bugs

[uvex-laservision.de - Laservision Entwickler, Hersteller und Vertrieb von Laserschutzprodukten &#x7c; uvex laservision](https://www.uvex-laservision.de/)


---
**Paul Mott** *March 24, 2018 13:40*

As AB says any pair of acrylic safety glasses will work BUT…



My brain told me that my eyes were precious so I purchased the proper goggles with a 5+ OD rating at the 10.6um wavelength. Are your eyes worth it ?




---
**Anthony Bolgar** *March 24, 2018 14:18*

If you follow proper safety proceduyres such has having lid interlocks in place, the standard glasses should be fine. But as others have said, it is your only pair of eyes, treat them nicely.




---
*Imported from [Google+](https://plus.google.com/105696631925164417072/posts/LEryx7N27QF) &mdash; content and formatting may not be reliable*
