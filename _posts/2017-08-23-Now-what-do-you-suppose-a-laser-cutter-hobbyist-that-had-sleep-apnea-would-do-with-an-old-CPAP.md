---
layout: post
title: "Now what do you suppose a laser cutter hobbyist that had sleep apnea would do with an old CPAP?"
date: August 23, 2017 21:01
category: "Air Assist"
author: "Don Kleinschnitz Jr."
---
Now what do you suppose a laser cutter hobbyist that had sleep apnea would do with an old CPAP?

??? :)





![images/e14431115897808fa3922373a92f62fc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e14431115897808fa3922373a92f62fc.jpeg)



**"Don Kleinschnitz Jr."**

---
---
**Adrian Godwin** *August 23, 2017 21:24*

Ha. I was just looking at an ex-hospital pump for air assist :). 


---
**Adrian Godwin** *August 23, 2017 21:28*

I love re-purposing things, especially when they're robustly built like medical stuff. 



I started with a cooker hood for fume extraction, have just swapped it for a solder fume filter, I'm using an old aquarium chiller for cooling and I'm trying to find a cheap aquarium external pump-filter to replace the  pump and reservoir.



Not sure if the CPAP has enough pressure  to blow down a thin tube but I'll try it if I can't find something better.




---
**Don Kleinschnitz Jr.** *August 23, 2017 21:58*

I found out how to adjust this one and :

...It puts out 20 psi

....Its adjustable

...It is whisper quiet

... it does not pulse 



we will see!


---
**Steve Clark** *August 23, 2017 22:33*

NICE


---
**Martin Dillon** *August 23, 2017 22:48*

genius 


---
**Phillip Conroy** *August 25, 2017 12:28*

Rember to turn off humidifier, last thing you want is moisture on focal lens,or hang on have I go my terms wrong and you want it on ?,good reuse


---
**Steve Clark** *August 25, 2017 15:47*

Nope... off in this case...It is an option on most machines, mostly likely just disconnect it and don't fill the reservoir. 


---
**Don Kleinschnitz Jr.** *August 25, 2017 16:34*

**+Steve Clark** **+Phillip Conroy** **+Steve Clark** np on this model the humidifier is a separate assy. I just did not connect it up :).


---
**Awesome Coder** *September 13, 2017 11:19*

Fantastic idea. I have a couple older CPAP laying around. I'll see how high their pressures can go. 


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/bg1RWAPNj3y) &mdash; content and formatting may not be reliable*
