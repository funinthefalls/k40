---
layout: post
title: "So, I finally got my K40 setup.."
date: February 24, 2018 08:23
category: "Hardware and Laser settings"
author: "Seon Rozenblum"
---
So, I finally got my K40 setup.. yay, and while I am waiting on my C3D Mini to arrive, I thought I better check it works BEFORE I pull it apart... and it does, but not quite like I was hoping for my first cut...



So I downloaded K40 Whisperer & Inkscape - couldn't make heads or tails out of the Corel install files I was provided.



I got them setup and installed the driver (using the replace method) and everything worked fine.



So I download a test SVG and asked the laser to cut it at 10mm/s with laser % at 60% - and it marked the card quite darkly, but didn't cut it.



So from everything I have read 10mm/s is quite slow, and 60% for strength should be ample to cut standard thickness cardboard right?



The laser seems to be focused ok, because the test fire dot is tiny, and the lines it's making on the card seems pretty thin.



I am using a new LightObjects laser head (air assist model) with a new 18mm F50.8 lense.



Any ideas? Maybe 60% is just too low? Or 10,,/s is too fast?



Thanks in advance!



For reference, that card is the size of the clamp area in the K40.

![images/e187abbf45b62c6c77fd961b1d5c1229.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e187abbf45b62c6c77fd961b1d5c1229.jpeg)



**"Seon Rozenblum"**

---
---
**Andy Shilling** *February 24, 2018 08:38*

+ Seon Rozenblum  Those lines look way to big for it to be focused, a cutting line would look more like the lines to the right on your card. do yourself a favour and do i little ramp test.



place a bit of wood or card in the machine propped up at one end and make the machine run a straight line over it, as the distance gets greater from the head to the card you will see the focus change.



it looks to me you might be measuring from the bottom of the head rather than where the lens is halfway up in the head.


---
**Seon Rozenblum** *February 24, 2018 08:45*

**+Andy Shilling** Ahh, ok, that makes sense. Yeah I think I did measure from the bottom. Good call!



Will give it another go after dinner and see if I can do a better job.



Assuming the laser is focused better, is 10mm/s slow enough? and 60% should be way more than enough for cardboard right?


---
**Seon Rozenblum** *February 24, 2018 08:48*

**+Andy Shilling** Sorry, one more thing... I see conflicting info on this... lense curve down or curve up?



I have it curve up.


---
**Andy Shilling** *February 24, 2018 08:55*

I would say that's a little to slow and runs the risk of setting light to it. I run at mm/m rather than mm/s so my speeds are different to yours.



10mm/s = 600mm/m for me. I cut 4mm acrylic @ 300mm/m running 15mA so you should be able to cut cardboard faster than you are.



Try the focus test and then adjust your power from there, it takes time to get things right as all these machines are different but you will get there and have to start all over again when you get the C3D board.


---
**Andy Shilling** *February 24, 2018 08:59*

**+Seon Rozenblum** I'm not 100% sure, I believe mine is curve up, quite bad really as I only replaced it last month but can't remember. **+Don Kleinschnitz** would be able to confirm that but I would like to believe I've got mine right.




---
**Andy Shilling** *February 24, 2018 09:02*

Also if you did measure from the bottom of the head try measuring from the bottom of the milled grip part where the lens is unscrewed from just above the nozzle retaining screw.


---
**Seon Rozenblum** *February 24, 2018 09:49*

**+Andy Shilling** Ok, I re-measured, The distance from the "lense" to the cutting area on my K40 > 60mm, so I am going to have to pad out the plate to bring the item being cut higher to match the 50.8mm.



I didn't do the ramp test yet... will try tomorrow, but I did manage to get it to cut about 1/3 through 7mm ply at 60%, 10mm/s in one pass, so maybe that's progress.



I'll report back tomorrow after the ramp test :-)





![images/16cd22e75f6c20f332e6baff33473262.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/16cd22e75f6c20f332e6baff33473262.jpeg)


---
**Andy Shilling** *February 24, 2018 09:55*

**+Seon Rozenblum** that sounds better, remember if you are cutting ideally you want the focus to be set half way through what you are cutting 

ie 

7mm ply = 54.3m focal length 

3mm ply =52.3 focal length



good luck ;)


---
**Don Kleinschnitz Jr.** *February 24, 2018 13:29*

CURVE SIDE UP FLAT SIDE DOWN....



[donsthings.blogspot.com - K40 Lens Specifcations, Orientation & Cleaning](http://donsthings.blogspot.com/2017/05/k40-lens-specifcations-orientation.html)


---
**Seon Rozenblum** *February 24, 2018 21:57*

**+Don Kleinschnitz** Thanks! I've seen too many people talking about reflective & dull side side up or down and my K40 actually came with the 12mm lense curve down in the laser head :(


---
*Imported from [Google+](https://plus.google.com/115451901608092229647/posts/MZG2G8BdsdA) &mdash; content and formatting may not be reliable*
