---
layout: post
title: "Ive seen people here adding a (touch?) screen to a modified/upgraded custom control panel, presumably after a main board upgrade"
date: February 21, 2018 18:52
category: "Smoothieboard Modification"
author: "James Rivera"
---
I’ve seen people here adding a (touch?) screen to a modified/upgraded custom control panel, presumably after a #Cohesion3d main board upgrade. My question is: is this necessary to use it after the board upgrade, or is this just a kind of “UI sugar” or for something else? (E.g. SD card interface?)





**"James Rivera"**

---
---
**BEN 3D** *February 21, 2018 19:04*

I do not now, but I planning to add a edit: (sorry) rasperry pi to my laser and may use octopi (if it works?) or raspi os with k40 whisperer. Second should work as I know...


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 20:01*

It's a graphic LCD, useful for seeing status, jogging/ controlling the machine, and running jobs without having your computer tethered all the time. 



The GLCD Adapter and GLCD Screen are available on our website. 



[store-kq4bjok.mybigcommerce.com](https://store-kq4bjok.mybigcommerce.com/product_images/uploaded_images/c3d-glcd.jpg)


---
**James Rivera** *February 21, 2018 20:45*

**+BEN 3D** when you say, "add an Arduino" do you mean a basic RAMPS  1.4 board? If so, and if it is running Marlin, then OctoPrint should be able to communicate with it. If you mean an Uno plus a grblShield, then I'm not so sure.


---
**James Rivera** *February 21, 2018 20:49*

**+Ray Kholodovsky** What do "WCS" and "MCS" mean in that image? (Also, while I'm sure I can find it, a direct link to the GLCD item for sale on your website might be more helpful than just the image of it).


---
**Ray Kholodovsky (Cohesion3D)** *February 21, 2018 20:52*

World vs Machine coordinates.  As long as you haven't applied a G54 offset (complex CNC stuff) then they're the same.  I just pay attention to the left half :)



Adapter: 

[cohesion3d.com - GLCD Adapter for Cohesion3D Mini](http://cohesion3d.com/glcd-adapter-for-cohesion3d-mini/)



Screen: 

[http://cohesion3d.com/graphic-lcd-control-panel/](http://cohesion3d.com/graphic-lcd-control-panel/)


---
**BEN 3D** *February 21, 2018 21:19*

**+James Rivera**  sory i thinking to rapsperry pi and had have written arduino. I editited it at the top. I think an arduino would be to slow for this job, but the 40k whisperer is written in phyton and so it should possible to add it to the octopi and/ or compile it to the raspi os.



And it should also able to add sensors for water temperture, water flow .... live cam ...


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/5AcDde1w7K4) &mdash; content and formatting may not be reliable*
