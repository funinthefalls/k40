---
layout: post
title: "Doed nyone know if corel laser will work with corel draw x8 home version?"
date: February 15, 2017 18:27
category: "Software"
author: "Lewis Harris"
---
Doed nyone know if corel laser will work with corel draw x8 home version?





**"Lewis Harris"**

---
---
**Jim Hatch** *February 15, 2017 19:41*

No. It works with all other X8 versions. Several of us on here are using X8. You'll have to reinstall CorelLaser after installing X8 so you get the add-in icons enabled.


---
**Lewis Harris** *February 16, 2017 19:01*

Thanks Bummer!!!


---
*Imported from [Google+](https://plus.google.com/100166516438769339774/posts/QDG8uFFwjLJ) &mdash; content and formatting may not be reliable*
