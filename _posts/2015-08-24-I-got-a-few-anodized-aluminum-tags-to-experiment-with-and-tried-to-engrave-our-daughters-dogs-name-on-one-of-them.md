---
layout: post
title: "I got a few anodized aluminum tags to experiment with, and tried to engrave our daughter's dog's name on one of them"
date: August 24, 2015 17:53
category: "Materials and settings"
author: "Kirk Yarina"
---
I got a few anodized aluminum tags to experiment with, and tried to engrave our daughter's dog's name on one of them.  This one has been gone over a number of times at different speed and power settings, and I tried both engraving and  cutting (trying for a vector outline to get time missed area).  I'm just not able to get the complete letter area removed, and the top of the "R" isn't completely outlined.  This hasn't happened before on wood, silicone, or two layer acrylic tags.  Am I missing something, or is this just how the K40 does anodized Al?



This is with the LaserDRW that came with my machine, using the default font.



After uploading I just noticed the writing on the fixture, engraved the same day, did the same thing.  Looks like I might have accidentally messed up a setting, but I've got no idea what it might have been.  Any ideas?

![images/92a72cad229f0ca3464b2b144a963ba8.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/92a72cad229f0ca3464b2b144a963ba8.jpeg)



**"Kirk Yarina"**

---
---
**Joey Fitzpatrick** *August 28, 2015 14:26*

I gave up on trying to get laserDRW to behave. It never gave consistent results.  I bet if you use corellaser you will get better results.  Did you ever get corellaser to work properly? (I saw one of your other posts about the problem you were having). If you need a good copy of corellaser let me know, I can email you a link to it.  What version of coreldraw do you have and what operating system are you using?


---
**Kirk Yarina** *August 28, 2015 21:12*

It came with CorelDraw version 12, which I realize is pretty old.  CorelLASER, with the plugin loader, ID's itself as 2013.02, but still uses V12.  I suspect it's just a preloader for the plugin.


---
**Kirk Yarina** *August 28, 2015 21:15*

Today's project is to see if I can reinstall the plug in.  I'm using Windows 10 Pro from the insider preview, which just updated to build 10525 on an i5-5200U Lenovo G50..


---
**Jim Hatch** *January 02, 2016 23:00*

**+Joey Fitzpatrick** I'm reinvigorating an old thread :) 



Do you have a clean version of CorelLaser? The one I got with my K40 gets an Antivirus flag on a file it tries to install (assembly.dll). Of course the chinese folks would like me to turn off AV checking but I'd feel more comfortable installing it if I wasn't getting the AV warning. Like Kirk I'm also running Win10.



TIA


---
*Imported from [Google+](https://plus.google.com/+KirkYarina/posts/aooPfMMi3L6) &mdash; content and formatting may not be reliable*
