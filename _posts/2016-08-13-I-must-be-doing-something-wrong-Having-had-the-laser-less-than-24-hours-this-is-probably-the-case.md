---
layout: post
title: "I must be doing something wrong? Having had the laser less than 24 hours, this is probably the case"
date: August 13, 2016 18:46
category: "Original software and hardware issues"
author: "Scott Pollmann"
---
I must be doing something wrong?  Having had the laser less than 24 hours, this is probably the case.  I was able to successfully cut paper using an svg from [http://monicascreativeroom.se/category/cutting-files/cupcake-wrappers](http://monicascreativeroom.se/category/cutting-files/cupcake-wrappers).  That file, however appears jagged, I was expecting something more smooth.



I was able to successfully cut through cardboard, but it took about 7 passes.  I was expecting to be able to easily cut through cardboard.  Granted, I haven't yet installed an air assist, but I assumed slightly better.



I was engraving and atempting to cut some old MDF laminate flooring.  Learning how to recenter the laser each pass.  I was expecting to be able to cut through this as well.  How many passes is "normal" to cut through various materials.  I've read three, so maybe a setting is wrong?



Using CorelDraw 12 and the corel laser plug in.

![images/8b06d980700061b356dbc8c5b451c140.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/8b06d980700061b356dbc8c5b451c140.jpeg)



**"Scott Pollmann"**

---
---
**Eric Flynn** *August 13, 2016 18:59*

You likely need to square up, and align the optics/bed, and tram in the head.


---
**Jim Hatch** *August 13, 2016 19:03*

It'll cut 1/4" ply in a single pass with the optics  aligned and the lens installed correctly (they're often installed wrong way up at the factory).﻿


---
**Eric Flynn** *August 13, 2016 19:18*

Yes indeed.   A well aligned and set up K40 is quite capable.  Much more capable than the majority of these you see on the web.  But, you have to put in the effort to do it, rather than plowing in for instant gratification.  It doesnt take that long really.


---
**Eric Flynn** *August 13, 2016 19:20*

BTW, Mine will do 1/8 ply, at 12mm/s at 5mA tube current in a single pass, with air assist, after setting up properly.  I also changed the current meter for a nice digital uA meter, so the current readings are far more accurate than the stock analog meter, which was way off to begin with.


---
**greg greene** *August 13, 2016 19:33*

Align it,  align it and then align it some more.  - then you will be very pleased !


---
**Scott Pollmann** *August 13, 2016 19:37*

Will do.  I ordered an air assist head from LO.  I might just keep playing with the software, trying to determine how it works.  I sure wish I could use my mac, the PC isn't as stable.  



Once I learn how to use the laser in its current configuration, I might upgrade the board so I can use the mac.


---
**greg greene** *August 13, 2016 19:40*

That is a plus with this unit, that there are lots of inexpensive upgrades available, don't forget to upgrade the mirrors too - they make a significant difference.


---
**Eric Flynn** *August 13, 2016 19:58*

Also, keep in mind, when cutting thicker materials, focusing at the top of the material isnt always ideal.  Focusing into the material a degree, may give better cut results


---
**Scott Pollmann** *August 13, 2016 19:59*

Should have asked sooner.  I ordered the lens to fit properly in the new air assist head.  Didn't order the mirrors.


---
**Eric Flynn** *August 13, 2016 20:03*

Your stock mirrors may be fine, They vary quite a bit.  Mine came with nice mirrors.  Some do not.  You will have to post a pic of them for us to tell you which ones you have.


---
**Alex Krause** *August 13, 2016 21:01*

Looks like your are trying to cut to fast... the jagged edges of your cuts on paper


---
**Scott Pollmann** *August 14, 2016 00:35*

Eric,



I'll take out the mirrors and take a photo in a few days.



Alex,



I was cutting at 7mm/sec  I'm going to try an alignment soon.


---
**greg greene** *August 14, 2016 00:38*

7mm sec is way to slow for paper - you might be burning the edges.


---
**Scott Pollmann** *August 14, 2016 00:53*

Learning this tool is going to take some time.  :)  I'm glad this group is here.


---
**greg greene** *August 14, 2016 00:56*

They have helped me a lot - don't forget to check out the repository for some designs and stuff.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 14, 2016 02:10*

Yeah, I'd say Eric is right that alignment is necessary. Also sometimes the lens ships upside down. It should be with the bump side facing up.


---
**Eric Flynn** *August 14, 2016 05:24*

Yuusuf is correct. Convex side up, flat side down.  It really doesnt matter all that much, but the beam quality will be slightly better with the flat side toward the material.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *August 14, 2016 17:06*

I personally found that 20 passes through 3mm thick leather was ridiculous & then switching the lens orientation I was able to reduce that to about 2 maximum with the same power settings I had been using (7mA).


---
**Bob Damato** *August 15, 2016 13:32*

Those jagged edges almost look like a small bmp file being enlarged too mnuch and losing resolution.


---
*Imported from [Google+](https://plus.google.com/103382513064902122956/posts/dzRA5xn7Jmf) &mdash; content and formatting may not be reliable*
