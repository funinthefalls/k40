---
layout: post
title: "So I just went & tested again"
date: October 15, 2015 13:51
category: "Hardware and Laser settings"
author: "Yuusuf Sallahuddin (Y.S. Creations)"
---
So I just went & tested again. I tried reducing the power from about 6mW to 4-5 mW. I don't know whether that makes a difference but it seems that my first attempt this time around was fairly straight in the horizontal scale, albeit way out on the vertical scale.



So with some fiddling around, I've managed to get it fairly straight horizontally, and closer vertically, however still cannot get it spot on.



Thought I'd post a few pics to give a bit better idea of what I've done & where I am at. Might assist with any assistance you can offer me.



So what I'm wondering now is do I keep adjusting little bit by little bit or am I still looking at needing to either adjust the laser up/down at one end or adjusting the y-axis rail up/down at one end?



**+Phillip Conroy** **+Allready Gone** 



![images/1a6359c78839a079d041941d1e2b0d28.png](https://gitlab.com/funinthefalls/k40/raw/master/images/1a6359c78839a079d041941d1e2b0d28.png)
![images/c07a6d9e326374826a34b6c737034719.png](https://gitlab.com/funinthefalls/k40/raw/master/images/c07a6d9e326374826a34b6c737034719.png)
![images/26f276cddc25c043946963aa8c843ca8.png](https://gitlab.com/funinthefalls/k40/raw/master/images/26f276cddc25c043946963aa8c843ca8.png)

**"Yuusuf Sallahuddin (Y.S. Creations)"**

---
---
**Joey Fitzpatrick** *October 15, 2015 17:05*

A tip I found useful when aligning mirrors was to always turn two adjusting screws together.  Either both top screws or both side screws.  There is a guide that betters explains why somewhere in this group.  I will see if I can find it and repost it


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 17:20*

**+Joey Fitzpatrick** Thanks Joey. Let me know if you find the guide :)


---
**Joey Fitzpatrick** *October 15, 2015 17:21*

[http://www.floatingwombat.me.uk/wordpress/?attachment_id=30](http://www.floatingwombat.me.uk/wordpress/?attachment_id=30)


---
**Joey Fitzpatrick** *October 15, 2015 17:27*

This is the most comprehensive guide I have found yet.  I keep a printed copy of it near my laser engraver for reference.  I have aligned it so many times now that I can dial it in within 5 minutes.  The first time I attempted, it took me 3 hours to align it.


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 17:37*

**+Joey Fitzpatrick** Thanks again Joey.


---
**Coherent** *October 15, 2015 19:47*

I feel your pain, but it looks like you're getting closer. The guide referenced was what I used. Once you figure out which screw moves it which direction it gets easier. Mostly it was a lot of trial and error for me. It seemed every time I tightened the screw nut locks, things moved a bit. Just stick with it. It can get frustrating, but you'll get it eventually. The lower you can keep the power with short test fires for smaller "dots" the better. Remember that it doesn't matter so much where on the mirror it hits, just as long as it hits in the same place when moved closer/further.  


---
**Coherent** *October 15, 2015 20:37*

Also, here is another mirror alignment guide with some diagrams etc that may help.

[http://dck40.blogspot.com/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html](http://dck40.blogspot.com/2013/02/post-by-nycon-how-to-adjust-mirrors-for.html)


---
**Yuusuf Sallahuddin (Y.S. Creations)** *October 15, 2015 22:03*

**+Marc G** Thanks Marc. Someone shared that link with me on my original post, but thanks anyway. I'll have another go at it this afternoon when I return home. I have a feeling that I may need to pad up the 1st mirror at the front just fractionally like in that guide suggests sometimes the level of the 1st mirror may need adjusting.


---
*Imported from [Google+](https://plus.google.com/+YuusufSallahuddinYSCreations/posts/XBNLxXgBvgw) &mdash; content and formatting may not be reliable*
