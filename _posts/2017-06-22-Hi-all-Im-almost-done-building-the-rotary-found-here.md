---
layout: post
title: "Hi all. I'm almost done building the rotary found here ."
date: June 22, 2017 21:57
category: "Modification"
author: "Arion McCartney"
---
Hi all. I'm almost done building the rotary found here [https://www.thingiverse.com/thing:1359528](https://www.thingiverse.com/thing:1359528). One hurdle that I can't seem to get past is how to adjust for focal distance. This design is height adjustable only on the ass end, but the driven side is not height adjustable. I am thinking I can make only one wheel drivable by the stepper motor and the other wheel adjustable left to right to raise or lower the work, but that doesn't solve my problem completely as I don't think it will give me enough adjustment. I'd love to hear your thoughts and suggestions. I could also shim the bottom to raise or lower the whole rotary assembly, but that would be a pain. Thanks in advance for any input!





**"Arion McCartney"**

---
---
**Jim Fong** *June 22, 2017 22:11*

I just raise/lower with several pieces 1/4" ply until focal length is correct.  One day I'll make a adjustable bed.  


---
**Ariel Yahni (UniKpty)** *June 22, 2017 22:12*

Shim it. The adjustable end is for wine glasses and similar


---
**Arion McCartney** *June 22, 2017 22:13*

Thanks guys. I figured that might be the easiest solution. 


---
**Joe Alexander** *June 23, 2017 02:49*

one side is adjustable to account for the odd bottles, etc. I would assume if you set it level on your bed then it should be fine. Coincidentally I also have made this, how'd your build go with only those photos to go off of? I took some liberties tweaking the drive train myself. I'll post a pic in a few.


---
**Arion McCartney** *June 23, 2017 03:01*

**+Joe Alexander** My build is going ok.  I had to change some parts due to alignment issues as pointed out by **+Ariel Yahni**.  I ended up changing the bearing features to fit bearings I have as well as lower the height of the bearings/drive wheels to maximize the size of bottle I can fit on it with correct focal length.  I am still working on it, but it is mostly done.  3D printed different drive wheels also.  The laser cut ones seemed more of a pain.  I will post pics when I am done, but it may not be fore a couple weeks as I just got bad news that my dad has cancer.  Looking forward to your pic.


---
**Joe Alexander** *June 23, 2017 04:19*

ouch, sorry to hear that. Lost my mom to lymphoma 3 years ago myself so I get it. I used the same wheels that were in the print but had to tweak an end cap as the tabs didnt line up. got it to rotate with LW but im not fully confident with the reliability of those wheels atm as they are mostly friction fit(very snugly)


---
**Ariel Yahni (UniKpty)** *June 23, 2017 04:22*

**+Arion McCartney**​ I hope he gets well soon


---
**Arion McCartney** *June 24, 2017 04:24*

+Joe Alexander Sorry to hear that buddy.  I can't even imagine the feeling right now.  +Ariel Yahni Thanks.  Anyway, I had time to finish it today and got it running GRBL-LPC and LW for a while until my crappy driver board failed.

![images/5ad699589e3134d3e1bf0157f16118e9.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/5ad699589e3134d3e1bf0157f16118e9.jpeg)


---
**Joe Alexander** *June 24, 2017 05:16*

did you see the collection i tagged you in **+Arion McCartney**? figured it was better than spamming this post with pics and keeps it organized.


---
**Arion McCartney** *June 24, 2017 05:24*

**+Joe Alexander**​ I did not and I can't find any notification of it... Don't know why


---
**Joe Alexander** *June 24, 2017 05:41*

[plus.google.com - Laser Cut/Engraved Projects](https://plus.google.com/u/0/collection/s49SGE) look in there


---
**Arion McCartney** *June 24, 2017 05:49*

**+Joe Alexander** Awesome, thanks for sharing that!




---
**Helcio Xavier** *June 27, 2017 16:07*

Dear Sirs, I would like to know how I can acquire the Rim-Drive Rotary Attachment, for laser

Thank you very much,

Helcio Xavier




---
*Imported from [Google+](https://plus.google.com/110543831642314750018/posts/ewept65VU2c) &mdash; content and formatting may not be reliable*
