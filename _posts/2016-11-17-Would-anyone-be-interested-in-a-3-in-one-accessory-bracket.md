---
layout: post
title: "Would anyone be interested in a 3 in one accessory bracket?"
date: November 17, 2016 16:38
category: "Modification"
author: "Matthew Prater"
---
Would anyone be interested in a 3 in one accessory bracket? I'm currently designing a bracket for the K40 that includes laser assist, a 40mm led light ring which will help see your work, and air assist (I will include the option of mounting your existing hose, as well as a version of my "poor mans air assist" which is a must to keep the lens clean." The "poor man's air assist" will essentially be a 40mm fan which funnels just enough air into the lens housing to keep it clean.  I'm still in the design phase, and am awaiting some electronic components. Pictures and videos (once complete) to follow. 





**"Matthew Prater"**

---


---
*Imported from [Google+](https://plus.google.com/113895077196814551282/posts/4AJaPvKFvxp) &mdash; content and formatting may not be reliable*
