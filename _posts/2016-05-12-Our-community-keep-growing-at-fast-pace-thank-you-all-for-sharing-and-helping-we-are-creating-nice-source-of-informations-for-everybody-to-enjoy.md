---
layout: post
title: "Our community keep growing at fast pace, thank you all for sharing and helping, we are creating nice source of informations for everybody to enjoy"
date: May 12, 2016 14:32
category: "Discussion"
author: "Stephane Buisson"
---
Our community keep growing  at fast pace, thank you all for sharing and helping, we are creating nice source of informations for everybody to enjoy. (+500 members in the last 3 months)

Today I create new categories, please select the appropriate one  when posting. (I was away for the last month, but i will try to read all posts and comments to sort the post into the right categories).

![images/0425b8dc85bef480337660d0b12b5d45.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/0425b8dc85bef480337660d0b12b5d45.jpeg)



**"Stephane Buisson"**

---
---
**Stephane Buisson** *May 12, 2016 14:32*

comment field for stat purpose:

creation date, birthday 7/11/2014

community reach 100 members on 12/2/2015

community reach 200 members on 11/5/2015

community reach 300 members on 16/7/2015

community reach 400 members on 9/9/2015

community reach 500 members on 16/10/2015

community reach 800 members on 5/1/2016

community reach 900 members on 25/1/2016

community reach 1000 members on 7/2/2016

community reach 1250 members on 25/3/2016

community reach 1500 members on 6/5/2016

community reach 1750 members on 17/6/2016

community reach 2000 members on 3/8/2016

community reach 2250 members on 15/9/2016




---
**Anthony Bolgar** *May 19, 2016 05:20*

general Relativity


---
**Scott Marshall** *July 10, 2016 08:54*

Wonder how many of these have been sold?



Figuring 25% lurkers, we've got about 1875 K40 owners here. I'd expect 90% PLUS go looking for help online, most will land here eventually, because none of the other forums compare as far as K40 knowledge goes. 



Not to mention that the  people are as friendly here as any forum I have ever visited.



The K40 sellers should put our site address on the "instructions manual"


---
**Frank Fisher** *August 03, 2016 09:57*

On the other hand I have looked through here before turning mine on, and 3 days later still haven't dared turn it on in case I have not checked some wire yet, somewhere ;)


---
**Scott Marshall** *August 03, 2016 12:12*

**+Frank Fisher**

But when you do finally fire it up, chances are good you won't blow your tube in the 1st 15 minutes as as happened to many owners less safety and detail conscious than you.



The time you are spending now making sure things are right will be repaid with years of productive lasing.



You're are already a success story, and what this place is all about. Soon enough. YOU will be explaining how it's done to a new K40 owner.



Welcome to the best group on the internet.



Scott


---
**stephen whitman** *October 10, 2016 14:43*

Can anyone help me with my k40 lazer engraver machine cannot install software that came with it 


---
**Scott Marshall** *October 10, 2016 22:45*

**+stephen whitman** 

What specifically is the problem?

Be detailed, the more we know, the easier the fix.


---
**Anthony Bolgar** *October 11, 2016 11:27*

This should probably moved to a thread of it's own.


---
*Imported from [Google+](https://plus.google.com/117750252531506832328/posts/2KPdivwNSSZ) &mdash; content and formatting may not be reliable*
