---
layout: post
title: "the machine is engraving offset, i tried to engrave 2 concentric circles and the secong circle (the small one) is out of center ,moved to +y , anyone solved this problem??"
date: August 22, 2018 16:06
category: "Discussion"
author: "enrique turner"
---
the machine is engraving offset, i tried to engrave 2 concentric circles and the secong circle (the small one) is out of center ,moved to +y ,  anyone solved this problem??  enriqueturner123@gmail.com



![images/1e8fabac22cd270ac4e76a1bb3f84f2a.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1e8fabac22cd270ac4e76a1bb3f84f2a.jpeg)
![images/06e57b72479f2c5fedde298d5a808efc.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/06e57b72479f2c5fedde298d5a808efc.jpeg)

**"enrique turner"**

---
---
**Djinn per se** *August 22, 2018 16:16*

Hi!! you need to do a reference pixel or square (using out bounds) for keep the reference, look the next article for more info:  [instructables.com - K40 Laser Cutter: How to Cut and Engrave in One Job](https://www.instructables.com/id/How-to-Cut-and-Engrave-Using-a-K40-Laser-Cutter/)




---
**Michael Audette** *August 22, 2018 17:31*

My best guess your travel feedrate is beyond what the machine/steppers can support and it's missing steps during moves.  Try turning down your travel/move speed in the GCODE generation to something closer to your cutting speed and see if it cleans it up.


---
**Sebastian Szafran** *August 23, 2018 23:25*

Is the offset the same when you run the same job multiple times? Loosing steps during engraving would possibly not allow to repeat on exactly same path. Is each circle a separate job or both are part of the same job? Do other jobs run correct?



Michael might be right, if your feedrate is set beyond machine limits when laser is not fired and you just move to another location, steps will be lost, but engraved shape will still look like a circle, as runtime feedrate is usually slower. Following this logic, is it possible that you overspeed on Y axis only?






---
**Joe Alexander** *August 24, 2018 10:23*

I agree with **+Djinn per se** as this issue has been seen many times before. So make sure you have that reference pixel in each layer at the same spot so everything from layer to layer lines up appropriately. Alternately you could use another program like K40 Whisperer or Lightburn.


---
*Imported from [Google+](https://plus.google.com/102539676010028236167/posts/F87AEedwio2) &mdash; content and formatting may not be reliable*
