---
layout: post
title: "YEAY!!! My Smoothie is here!!!"
date: July 14, 2016 18:53
category: "Smoothieboard Modification"
author: "Jon Bruno"
---
YEAY!!! My Smoothie is here!!!





**"Jon Bruno"**

---
---
**greg greene** *July 14, 2016 19:02*

Please let us know how it installed and works for you.  What can you do with it, that you can't do without it?


---
**Ariel Yahni (UniKpty)** *July 14, 2016 19:33*

Changing the stock board will among other things: 1- bring platform independence ( use other software to control your laser) / 2- recognize Grey as depth. 


---
**greg greene** *July 14, 2016 19:37*

What else do you need beside the board?  I see other stuff on the Uberclock web site.


---
**Ariel Yahni (UniKpty)** *July 14, 2016 19:48*

Well they sell a bunch of other stuff that's 3d printer related. For a K40 laser just the board but other connectors could  E needed depending on  your current psu and stock board


---
**greg greene** *July 14, 2016 19:51*

Thanks - what software do you plan to use - I don't have reliable internet connection at the machine site.


---
**Ariel Yahni (UniKpty)** *July 14, 2016 19:53*

I already did my convertion and i use Laserweb3 to control it on a daily basis.


---
**greg greene** *July 14, 2016 19:55*

You need an internet connection for laserweb though - correct?


---
**Ariel Yahni (UniKpty)** *July 14, 2016 19:56*

Nope. It runs locally on your computer


---
**greg greene** *July 14, 2016 20:02*

Awesome - one last question - what version of the board did you get - I will only use it for the K4


---
**Ariel Yahni (UniKpty)** *July 14, 2016 21:00*

I don't have an original smoothie but the desicion is also based if you would like network cable to connect the laser, if not go with the smaller board for 3 axis


---
**Jon Bruno** *July 14, 2016 23:38*

Same here.. I wasn't able to actually get a "real" smoothie.. but it's my hope that it'll do the job until I can.

Ariel I'll have questions for you about the pwm power setup.. but I think I'll figure it out with your config file you sent me.


---
**Ariel Yahni (UniKpty)** *July 14, 2016 23:40*

**+Jon Bruno**​ no need to actually change the smoothie, but it's important to support the developer.  PWM is just decided on the pin but the actual hookup depends in exactly what machine you have


---
**Jon Bruno** *July 14, 2016 23:46*

I know there was some writeups referring to level shifters and such.. 

that area seems a bit grey in my research.

I have an LPSU with the smaller connectors. I'm not sure how that  makes the difference between the models mentoned in some of the writeups..


---
**Ariel Yahni (UniKpty)** *July 14, 2016 23:52*

Place an image of both psu and stock board


---
**Jon Bruno** *July 15, 2016 00:36*

The stock board was removed when I went RAMPS and the factory LPSU has been changed long ago. I'll post pics anyway.. FWIW the stock board worked with the second LPSU the same as the first which ironically had the larger green connectors. this is why I'm skeptical that there is a difference in the way they work﻿.


---
**Jon Bruno** *July 15, 2016 11:44*

[https://drive.google.com/folderview?id=0B36nra1ArP-Vdi16YnFVTHJ2Sms](https://drive.google.com/folderview?id=0B36nra1ArP-Vdi16YnFVTHJ2Sms)


---
**Ariel Yahni (UniKpty)** *July 15, 2016 12:14*

**+Jon Bruno** Do you have to use a ribbon cable to connect to the stock board? For running the laser with smoothie look at the PSU from R to L. The first 4 pin connector will read 24v/gns/5v/L . You need the 24v - GND to power the smoothie and the L will be connected to a mosfet ( where you would connect a heated bed or 3d printer extruder heater ).


---
**Jon Bruno** *July 15, 2016 12:55*

The factory board was connected to the X axis and home switches with a ribbon. I have since terminated that cable to more managable termination with wires.

My question is more for the power level control. I understand that L is pwm'd but what is to be done with the power (IN) pin on the LPSU? Do we just feed it 5v solid and let the L manage the duty cycle or are both modulated?﻿ also the output of the mosfet, wouldn't that be at 12V or whatever the input voltage rail is at? I suppose I wouldn't want to send 12v to the L pin if it's only 5v tolerant.﻿


---
**Ariel Yahni (UniKpty)** *July 15, 2016 13:05*

There is no need to tap into any other line at all. The basic reason is the original board wasent connected to any other source either. Note that with this method the you still keep the pot and is used for security or to cap the max output. If you would like to control it also then you will need a 2 pin configuration. Depending on your board then you add the level shifter as smoothie will only output 3.3v and the PSU expect 5v for max power


---
**Jon Bruno** *July 15, 2016 13:15*

I know there are only a handful of smoothie modded lasers running, and they are mostly here. I guess my question then is, which method is preferred?

I'm perfectly ok with the one wire solution, that is Unless power regulation is also becoming the rave.


---
**Ariel Yahni (UniKpty)** *July 15, 2016 13:23*

I mus say that i expressed myself wrong before. I meant a pwm capable pin. There is still much debate but lately many are going for this one wire solutions are there is no good reason to go any other way


---
**Jon Bruno** *July 15, 2016 13:25*

PWM capable power IN pin?

I know that on my ramps I have D6 connected to IN on the LPSU for PWM intensity control.


---
**Ariel Yahni (UniKpty)** *July 15, 2016 13:39*

Don't let me confuse you. Where I said Mosfet I meant pwm pin


---
**Jon Bruno** *July 15, 2016 13:42*

Why you rascal ! Now you've got me totally buggered up.


---
**Jon Bruno** *July 15, 2016 13:43*

There is so much info, and so little time to study. I try to prevent myself from immersing 100% into it until I'm sure I've found the right path to follow. and now you go and mess me up!.. lol


---
**Jon Bruno** *July 15, 2016 13:55*

Ok, I was under the impression that the L line was used to cycle the beam on and off and the IN on the LPSU was PWM'd to control the current (mA) of the beam. If I understand you correctly the smoothie now controls the burn by mixing duty cycle of the laser beam with rate of beam travel across the workpiece?

If so, the max mA is preset on the POT and if the controller wanted to burn through the piece it would just fire the beam longer and travel slower? as opposed to the stock way of having a set rate of travel and a set intensity combination where only vector work could be achieved,


---
**Ariel Yahni (UniKpty)** *July 15, 2016 13:58*

Pot will stay in place and be used as a hardware max mA . Whatever you put on POT will be 100% . The L will be pwm by smoothie to control intensity in that range. Also note that one of the great features of smoothie is that it also controls power on acceleration and deceleration.


---
**Jon Bruno** *July 15, 2016 14:01*

well, I thank you for all of your help.. Im sure Ill have more questions later, but for now I'll just watch the clock until I can get back to the lab..


---
*Imported from [Google+](https://plus.google.com/105850439698187626520/posts/bVM1np3ucdN) &mdash; content and formatting may not be reliable*
