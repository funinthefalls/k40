---
layout: post
title: "I need help with cutting from downloaded DXF's"
date: May 10, 2015 21:28
category: "Software"
author: "Stuart Middleton"
---
I need help with cutting from downloaded DXF's. Lots of test pieces I download are a bunch of un-joined lines. When I use the "inside first" option it doesn't work because the lines aren't joined into paths. This means the shapes can cut out and fall out before the inner detail has cut.

I'm mainly using InkScape and I can't seem to find a simple way to join all nodes/end points/ vertices (whatever you call them) so that the lines make closed paths. Any ideas how to fix this?





**"Stuart Middleton"**

---
---
**David Wakely** *May 10, 2015 21:55*

What board does your machine have, is it Moshi or LaserDRW/CorelLASER? I'm interested also how DXF's cut with CorelLASER


---
**Stuart Middleton** *May 10, 2015 22:53*

It is LaserDraw/CorelLaser




---
**Sean Cherven** *May 10, 2015 23:43*

I've also been trying to figure this out. I hope someone has the answer. 




---
**Stephane Buisson** *May 11, 2015 06:34*

Hi,

unclean dxf are unfortunately common, not special to lasercutting, I did have the problem with dxf import into sketchup.

you will find a lot of video tutorial on that matter with sketchup, a good start to understand the problem. it exist some sketchup plugin to complete the job for you (personally untested but in my to do list).


---
**Stuart Middleton** *May 11, 2015 09:03*

I've found the solution to this with inkscape. It turns out I need to read the manual!!! If you select all of the lines in the DXF using the node tool, then select all again (CTRL-A) you will then select all of the nodes, the standard "join selected nodes" tool when works correctly! :)


---
**David Wakely** *May 11, 2015 11:18*

How do you get yours to cut a single line. Mine will only cut the outline of a solid black object. I.e a Dxf file of a square with a single black line would cut twice, one on each side of the line. Does yours do this too?


---
**Stuart Middleton** *May 11, 2015 11:20*

I don't think I've tried that, but the files I have that are disconnected do only cut each line once. Is it to do with the line width maybe? I'll try tonight.


---
**David Wakely** *May 11, 2015 11:23*

I dunno, I'm using the CorelLASER plugin and i have set the lines to hairline and it still cuts twice. If you could let me know how yours goes I would be most grateful!


---
**Jim Root** *May 11, 2015 11:49*

I'm very new at this but I think you have to set the line width to "0". Otherwise it tries to cut the line out. 


---
**David Wakely** *May 11, 2015 11:51*

Ok I'll give it ago tonight and let you know how I get on. Thanks all


---
**Bee 3D Gifts** *May 16, 2015 18:40*

Would Illustrator work? I know some of my lines were not joined and it would stop and go and stop and go and stop...etc..lol. I h ad to "Join lines" in illustrator. but I may be thinking of something different


---
*Imported from [Google+](https://plus.google.com/+StuartMiddleton1/posts/CMHXP8TLRzh) &mdash; content and formatting may not be reliable*
