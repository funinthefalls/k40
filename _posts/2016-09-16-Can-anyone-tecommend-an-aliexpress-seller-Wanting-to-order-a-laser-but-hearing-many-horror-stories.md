---
layout: post
title: "Can anyone tecommend an aliexpress seller? Wanting to order a laser but hearing many horror stories!"
date: September 16, 2016 19:42
category: "Original software and hardware issues"
author: "Derek golding"
---
Can anyone tecommend an aliexpress seller? Wanting to order a laser but hearing many horror stories! 





**"Derek golding"**

---
---
**Tiago Vale** *September 16, 2016 19:46*

Are you from europe?


---
**Derek golding** *September 16, 2016 22:19*

New Zealand


---
**Scott Marshall** *September 17, 2016 05:41*

You can buy direct from them. Search for the main site, I get emails form them all the time, next time I get one, I'll post the site. I'm pretty sure it's just [aliexpress.com - Find Quality Wholesalers, Suppliers, Manufacturers, Buyers and Products from Our Award-Winning International Trade Site. Wholesale Products from China Wholesalers at Aliexpress.com.](http://aliexpress.com) 


---
**Scott Marshall** *September 17, 2016 05:45*

Sure enough, I typed the address and it popped up as shown. Google added that feature not to long ago, I guess I like it. My site came up the other day and I thought it was the work of my webmaster. Guess not.



I followed the link, and sure enough, you can buy direct (I got some RC parts a while back) and you even get points toward free stuff if you spend a lot (laser ought to get you a free something)



Good luck and happy K40ing



Scott


---
**Derek golding** *September 19, 2016 07:27*

I use aliexpress every day. Looking for a trustworth seller. It's the Chinese equivalent of Ebay so some good and some bad sellers.


---
*Imported from [Google+](https://plus.google.com/+Derekgolding-GoldingArts/posts/XdjRiWqNY1t) &mdash; content and formatting may not be reliable*
