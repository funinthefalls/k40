---
layout: post
title: "So I was having trouble cutting things earlier, and decided to go through the alignment process"
date: June 30, 2016 08:20
category: "Discussion"
author: "Evan Fosmark"
---
So I was having trouble cutting things earlier, and decided to go through the alignment process. Got through all of that, and the laset is acting better, BUT the laser seems to be cutting at a slight bevel,  and this wasn't happening when I first got the machine. Any ideas? I did upgrade to the LO air assist head, and got the improved lens too. Is something with the head misaligned?





**"Evan Fosmark"**

---
---
**Yuusuf Sallahuddin (Y.S. Creations)** *June 30, 2016 09:00*

If it's cutting on a bevel then it's more than likely related to the beam not bouncing directly perpendicular on the 3rd mirror. It must be coming off at an angle. You'll need to tweak the 2nd mirror, I'd imagine, in order to get the beam coming into the 3rd mirror parallel to the work area (since the 3rd mirror is fixed).


---
**Phillip Conroy** *June 30, 2016 09:56*

Check focal clens is 50mm from center of what u are cutting.cut some thing and check air assist is not heating up[beam not clearing bottom of air assist]put some masking tap on bottom of air assist and fire laser at low power -check beam is in center,clean and  check focal lens is rised curve up.


---
**Evan Fosmark** *June 30, 2016 15:10*

**+Phillip Conroy** I originally had the lens curve up, but switched it last night to have it curve down. The reason being that when I did the ramp test, it was creating a "ghost" of the cut above the real cut.


---
**Ned Hill** *June 30, 2016 17:09*

With the curved or convex side down you are going to have a less focused beam.  Make sure you are hitting near the center of the laser head mirror so the beam passes near the center of the lens for best focusing.


---
**Evan Fosmark** *July 01, 2016 04:00*

Yep, got it working! Convex side up now. I had to raise the bed by another 6mm to get it to the proper ~50.8mm distance.


---
**Evan Fosmark** *July 01, 2016 07:17*

Also now I'm able to cut much faster and with lower power. Cutting cleanly through 3mm birch plywood at 30% 10mm/s.


---
*Imported from [Google+](https://plus.google.com/102300524326099031212/posts/dW8SNYDMdV8) &mdash; content and formatting may not be reliable*
