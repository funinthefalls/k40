---
layout: post
title: "good afternoon Someone could help me with a problem I generated my k40 , was working normally and suddenly turn off the laser , but the light of the normal continuous tube but nothing comes of it"
date: July 12, 2015 23:25
category: "Discussion"
author: "tony alexander rico cera"
---
good afternoon

Someone could help me with a problem I generated my k40 , was working normally and suddenly turn off the laser , but the light of the normal continuous tube but nothing comes of it.

Thanks in advance


**Video content missing for image https://lh3.googleusercontent.com/-OcY0_XlyZ6A/VaL3UlnXONI/AAAAAAAASlU/ggEnBmeI0fM/s0/VIDEO0004.mp4.gif**
![images/00ce6c6bbcbb3d63cd494a7afa752aa9.gif](https://gitlab.com/funinthefalls/k40/raw/master/images/00ce6c6bbcbb3d63cd494a7afa752aa9.gif)



**"tony alexander rico cera"**

---
---
**Lars Mohler** *July 13, 2015 15:21*

I am assuming that this is the cathode end of your tube? If so, there is a lot of arcing going on. Arcing is a bad thing. It acts as a low resistance path to a place where the hi-V should not be going. If your power is set high, turn it down. Also keep the tube and its area clean in order to reduce the chance of arcing. Soot is a great path for arcs.


---
**tony alexander rico cera** *July 13, 2015 21:36*

The machine was working normally and is leading rubber callus on the tip of the laser . I myself stick it in the same position where it was. But I'll take that mistake and tube do not think q 's because this practically new and has little use


---
**Dingo D** *July 14, 2015 18:36*

Swap out the tube..fix all it will (yoda voice)


---
*Imported from [Google+](https://plus.google.com/111896216284635092101/posts/815gw39eAxr) &mdash; content and formatting may not be reliable*
