---
layout: post
title: "Hello all, I've ported the Turkney Tyranny patches for Marlin to MarlinKimbra, and then other things are added: - support for coolers (i use a TEC ( peltier cell ) to lower water temperature ) - support for flow meters"
date: May 16, 2016 20:56
category: "Software"
author: "Franco \u201cnextime\u201d Lanza"
---
Hello all, I've ported the Turkney Tyranny patches for Marlin to MarlinKimbra, and then other things are added:

 - support for coolers (i use a TEC ( peltier cell ) to lower water temperature )

 - support for flow meters



MarlinKimbra is a modern and powerful fork of Marlin, well mantained and selected as the new "marlin for due".



Here my fork patched: [https://github.com/nextime/MarlinKimbra/tree/dev](https://github.com/nextime/MarlinKimbra/tree/dev)



Here the pull request to the main MarlinKimbra: [https://github.com/MagoKimbra/MarlinKimbra/pull/113](https://github.com/MagoKimbra/MarlinKimbra/pull/113)



And then my new panel for the K40 machine:[http://www.nexlab.net/2016/05/16/how-to-build-a-perfect-acrylic-panel/](http://www.nexlab.net/2016/05/16/how-to-build-a-perfect-acrylic-panel/)





**"Franco \u201cnextime\u201d Lanza"**

---
---
**Anthony Bolgar** *May 17, 2016 04:20*

I am very interested in this fork.


---
**Jon Bruno** *May 19, 2016 13:33*

yes please.. because it just sits there and laughs at me when I try to use it with LW2


---
*Imported from [Google+](https://plus.google.com/+FrancoLanzanextime/posts/iUQDKKw7PUT) &mdash; content and formatting may not be reliable*
