---
layout: post
title: "HI, I just finished wiring a RAMPS 1.4 board to a ten High laser engraver...the M2Nano board was giving me problems"
date: March 01, 2016 18:14
category: "Software"
author: "Francis de Rege"
---
HI,

I just finished wiring a RAMPS 1.4 board to a ten High laser engraver...the M2Nano board was giving me problems. I'm trying to setup Marlin from [github.com/TurnkeyTyranny](http://github.com/TurnkeyTyranny) etc.. as suggested here and in other places. I just went through all the versions of Arduino I found at the Arduino site (1.6.X, 1.5X and 1.0X). None seemed to allow the program to compile properly. Maybe I'm missing one are I'm doing something else that is stupid. Anyhow, does anyone know which version works? Or is there another program I should flash to the board? Appreciate any help.





**"Francis de Rege"**

---
---
**Roberto Fernandez** *March 01, 2016 22:45*

Try to copy the arduino add ons, and library carpets to arduino directory in your c disk.

And compile again.




---
**Francis de Rege** *March 02, 2016 13:13*

Thanks for answering. Sorry, not at all a computer guy...got marlin to work on a 3D printer I built with a RUMBA board but the arduino software is mostly mysterious to me.

There are a bunch addons at [github.com/TurnkeyTyranny/buildlog-lasercutter-marlin](http://github.com/TurnkeyTyranny/buildlog-lasercutter-marlin), are those the ones you mean? How do I actually download them, there doesn't seem to be an option to do that.

Should I also use the latest version of arduino (1.67)?


---
**Francis de Rege** *March 02, 2016 13:38*

Hey, for my edification if you feel like answering I'd still like to understand more...however...you might be rolling your eyes and I was able to get things to work using the instructables post "Goodby Moshi or how to run your laser printer on arduino". He (sorry can't seem to find the authors name...life saver) provides a dowloadable file that seems to compile just fine...he put all the addon files in the correct place so it seems to work (yippy!). Will be going back to that post no doubt for oncoming problems (there always are).


---
**Heath Young** *March 13, 2016 23:28*

Let me know if you decide to get rid of the nano board - mine is going senile and the seller is being rather unhelpful in replacing it.


---
*Imported from [Google+](https://plus.google.com/113477512464872718508/posts/LDC2VG5MFZK) &mdash; content and formatting may not be reliable*
