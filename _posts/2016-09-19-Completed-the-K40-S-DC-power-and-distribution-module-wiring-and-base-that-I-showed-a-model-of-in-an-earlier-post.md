---
layout: post
title: "Completed the K40-S DC power and distribution module wiring and base that I showed a model of in an earlier post"
date: September 19, 2016 00:05
category: "Modification"
author: "Don Kleinschnitz Jr."
---
Completed the K40-S DC power and distribution module wiring and base that I showed a model of in an earlier post.

Next UP:  the control panel and smoothie enclosure.



Before it is mentioned I could etch the labels if I had a working laser thingie .....



![images/f2360d335c79ccb90ee67db0c91e0f90.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/f2360d335c79ccb90ee67db0c91e0f90.jpeg)
![images/e334c76571ac8e265c614d604d7fb930.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/e334c76571ac8e265c614d604d7fb930.jpeg)
![images/57266a739bb7e7ba62f2cf11f9869a0f.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/57266a739bb7e7ba62f2cf11f9869a0f.jpeg)
![images/fd54d12db5fdcf18a0a1312ae3581514.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/fd54d12db5fdcf18a0a1312ae3581514.jpeg)

**"Don Kleinschnitz Jr."**

---


---
*Imported from [Google+](https://plus.google.com/+DonKleinschnitz/posts/bXUuTfT4VcU) &mdash; content and formatting may not be reliable*
