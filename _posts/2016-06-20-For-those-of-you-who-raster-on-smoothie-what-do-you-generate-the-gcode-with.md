---
layout: post
title: "For those of you who raster on smoothie, what do you generate the gcode with?"
date: June 20, 2016 18:10
category: "Discussion"
author: "Ray Kholodovsky (Cohesion3D)"
---
For those of you who raster on smoothie, what do you generate the gcode with?  Does LW2 or LW3 support the creation of gcode from a DXF?  I need to get into solder stencils for pcbs, I have vinyl and may get some acetate (transparency/ drafting film) from Staples, and that's how it works - I export a dxf with the paths already "deflated" of the cream layer from eagle.





**"Ray Kholodovsky (Cohesion3D)"**

---
---
**dstevens lv** *June 21, 2016 09:17*

If you need them cut hit me with the file(s) via email.  More than happy to help.  



I've got .003 mylar in the shop ([http://www.dickblick.com/products/grafix-dura-lar-matte/](http://www.dickblick.com/products/grafix-dura-lar-matte/)) but can order some polyimide. ([http://www.mcmaster.com/#2271K2](http://www.mcmaster.com/#2271K2))


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/MVpBPPkaWML) &mdash; content and formatting may not be reliable*
