---
layout: post
title: "Hi Everyone, Ive had my K40 up and running for a couple of days, Day 1 - started out well apart from the right side of the material didn't cut/engrave at the same intensity, easy i thought just need aligning, but before i had"
date: April 05, 2015 15:03
category: "Discussion"
author: "grahame dale"
---
Hi Everyone, Ive had my K40 up and running for a couple of days, 



Day 1 - started out well apart from the right side of the material didn't cut/engrave at the same intensity, easy i thought just need aligning, but before i had done that it stopped cutting through 3mm ply :(



Day 2 i have aligned all mirrors adn it engraves like a dream but still

wont cut like it did to start with, i have it set at 28A and speed at 10 (Same as yesterday) just need to cut 3mm PLY wood,



any advice for me please ,





**"grahame dale"**

---
---
**Tim Fawcett** *April 15, 2015 18:00*

Have you got water cooling? If not you risk the laser tube. If the water cooling is not adequate and  gets to more than about 25C the laser efficiency drops.

The maximum current for the 40W laser is 18mA. Overdriving it will shorten the laser life and may reduce laser efficiency


---
**grahame dale** *April 15, 2015 18:44*

Thanks for replying Tim. I did mean 18a not 28 and water is consistant at 17c it is baffeling. 


---
**Tim Fawcett** *April 17, 2015 12:20*

The other thing to do is to check how clean the mirrors and the lens are - if you have not got air assist, it is easy to gunk up the lens dependin on what you are cutting.


---
*Imported from [Google+](https://plus.google.com/110693493789999414301/posts/A3yuW3Tzztn) &mdash; content and formatting may not be reliable*
