---
layout: post
title: "Finally have all the parts to do a Ramps 1.4 conversion on my K40"
date: November 03, 2015 14:56
category: "External links&#x3a; Blog, forum, etc"
author: "Anthony Bolgar"
---
Finally have all the parts to do a Ramps 1.4 conversion on my K40. I hope to have it finished in 2 or 3 days. Will post some videos and pictures when it is complete. Looking forward to being able to run Visicut.





**"Anthony Bolgar"**

---
---
**Paul Dumke** *November 03, 2015 15:24*

I'll be interested in the Visicut part. I converted my K40 to Ramps/Marlin something ago but still use Repetier host and would like to use Visicut but don't know where to sta


---
**Anthony Bolgar** *November 04, 2015 00:04*

Here is a link to the visicut version that has the marlin driver built in. Hope this helps.  [https://groups.google.com/forum/#!topic/opensource-laser/ioBiT5glDSQ](https://groups.google.com/forum/#!topic/opensource-laser/ioBiT5glDSQ)


---
*Imported from [Google+](https://plus.google.com/+AnthonyBolgar/posts/eUT4LsWPm7X) &mdash; content and formatting may not be reliable*
