---
layout: post
title: "Used Bit too much power. But it turn out to be great"
date: February 20, 2016 00:10
category: "Object produced with laser"
author: "ChiRag Chaudhari"
---
Used Bit too much power. But it turn out to be great. 





![images/1b7c593c415c69940a8a08d9d0d72c58.jpeg](https://gitlab.com/funinthefalls/k40/raw/master/images/1b7c593c415c69940a8a08d9d0d72c58.jpeg)



**"ChiRag Chaudhari"**

---
---
**Sean Cherven** *February 20, 2016 00:19*

Oh wow, that really does still look nice. Is that your daughter?


---
**Yuusuf Sallahuddin (Y.S. Creations)** *February 20, 2016 01:02*

Very nice contrast in this image. Turned out pretty good.


---
**ChiRag Chaudhari** *February 20, 2016 01:24*

That's cuteness my buddy's daughter. No kids yet! 


---
**Sean Cherven** *February 20, 2016 01:42*

Oh I see lol


---
**Scott Thorne** *February 20, 2016 12:50*

Nice bro....very nice indeed.


---
**Maldenarious** *February 20, 2016 22:59*

What is it you engraved on? (looks great btw)


---
**ChiRag Chaudhari** *February 23, 2016 06:19*

**+Michael Black** its 1/8" hobby plywood from Menards. comes in 12x24" for about $4


---
**Andrew ONeal (Andy-drew)** *March 26, 2016 06:21*

Hey do you have size issues between whats designed in inkscape vs what is printed through inkscape plugin? My prints come out at half the size of what the scale says in inksckape example 8"x4" comes out more inline with 4"x4". Any ideas?


---
*Imported from [Google+](https://plus.google.com/+ChiragChaudhari/posts/4SSJsR4JQYQ) &mdash; content and formatting may not be reliable*
