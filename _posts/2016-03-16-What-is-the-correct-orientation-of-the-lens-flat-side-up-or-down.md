---
layout: post
title: "What is the correct orientation of the lens, flat side up or down?"
date: March 16, 2016 04:16
category: "Discussion"
author: "Tony Sobczak"
---
What is the correct orientation of the lens,  flat side up or down?





**"Tony Sobczak"**

---
---
**Gee Willikers** *March 16, 2016 04:19*

Flat down crown up.


---
**Tony Sobczak** *March 16, 2016 04:20*

 Thanks. 


---
**Jim Hatch** *March 16, 2016 12:27*

Plano-convex lenses (flat on one side, curved on the other)  are used on the K40 and the flat side goes towards the table (convex side toward the incoming beam -  or the beam won't be bent/focused). 



The easy way is to look at the lens and the side that you can see your reflection in is the "up" side of the lens.


---
**Pippins McGee** *June 06, 2016 06:00*

**+Jim Hatch** hi Jim,

can you please confirm

for k40 12mm stock lens. one side is reflective and the other isn't so much.

You said above 'the side that you can see your reflection should be 'up'.



Doesn't that reflect the laser back up then?

Shouldn't the NON-reflective side be on the 'up' side of the lens?



I am confused now - please clarify



thanks.


---
**Jim Hatch** *June 06, 2016 11:35*

Bumps go up - easy way to remember.


---
*Imported from [Google+](https://plus.google.com/104479750532385612568/posts/RgUbCLaWWiL) &mdash; content and formatting may not be reliable*
